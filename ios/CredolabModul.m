//
//  CredolabModul.m
//  bafmobilenew
//
//  Created by IT BAF on 26/08/20.
//

#import <Foundation/Foundation.h>

// CredolabModulBridge.m
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(CredolabModul, NSObject)

RCT_EXTERN_METHOD(setCollectCredolab:(NSString *)ReferenNumber Autkey:(NSString *)Autkey Url:(NSString *)Url)

@end
