//
//  PrivyModul.m
//  bafmobilenew
//
//  Created by Hasbi CL on 09/04/21.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(PrivyModul, NSObject)

  RCT_EXTERN_METHOD(doLiveness: (RCTResponseSenderBlock)callback)

@end
