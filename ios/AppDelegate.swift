//
//  AppDelegate.swift
//  bafmobilenew
//
//  Created by Hasbi CL on 08/03/21.
//

import Foundation
import UIKit
//import SafariServices
import Firebase
import UserNotifications
import MarketingCloudSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var bridge: RCTBridge!
    
    // MobilePush SDK: REQUIRED IMPLEMENTATION
    // The appID, accessToken and appEndpoint are required values for MobilePush SDK configuration and are obtained from your MobilePush app.
    // See https://salesforce-marketingcloud.github.io/MarketingCloudSDK-iOS/get-started/get-started-setupapps.html for more information.
    // Use the builder method to configure the SDK for usage. This gives you the maximum flexibility in SDK configurationz
    // The builder lets you configure the SDK parameters at runtime.
      
      #if DEBUG
        let appID       = "d9b2f43e-da5c-4423-b78b-12cfcd931bbc"
        let accessToken = "ZjiHtz5vR4owWQohMLN96aCQ"
        let appEndpoint = "https://mc2zlbrlfvq87rzwn7hctn4tc-ty.device.marketingcloudapis.com/"
        let mid         = "514011371"
      #else
        let appID       = "d9b2f43e-da5c-4423-b78b-12cfcd931bbc"
        let accessToken = "ZjiHtz5vR4owWQohMLN96aCQ"
        let appEndpoint = "https://mc2zlbrlfvq87rzwn7hctn4tc-ty.device.marketingcloudapis.com/"
        let mid         = "514011371"
      #endif
  
    // Define features of MobilePush your app will use.
        let inbox       = false
        let location    = false
        let analytics   = true
  
    // MobilePush SDK: REQUIRED IMPLEMENTATION
      @discardableResult
      func configureMarketingCloudSDK() -> Bool {
          // Use the builder method to configure the SDK for usage. This gives you the maximum flexibility in SDK configuration.
          // The builder lets you configure the SDK parameters at runtime.
          let builder = MarketingCloudSDKConfigBuilder()
              .sfmc_setApplicationId(appID)
              .sfmc_setAccessToken(accessToken)
              .sfmc_setMarketingCloudServerUrl(appEndpoint)
              .sfmc_setMid(mid)
              .sfmc_setDelayRegistration(untilContactKeyIsSet: true)
              .sfmc_setInboxEnabled(inbox as NSNumber)
              .sfmc_setLocationEnabled(location as NSNumber)
              .sfmc_setAnalyticsEnabled(analytics as NSNumber)
              .sfmc_build()!
          
          var success = false
          
          // Once you've created the builder, pass it to the sfmc_configure method.
          do {
              try MarketingCloudSDK.sharedInstance().sfmc_configure(with:builder)
              success = true
          } catch let error as NSError {
              // Errors returned from configuration will be in the NSError parameter and can be used to determine
              // if you've implemented the SDK correctly.
              
              let configErrorString = String(format: "MarketingCloudSDK sfmc_configure failed with error = %@", error)
              print(configErrorString)
          }
          
          if success == true {
              // The SDK has been fully configured and is ready for use!
              // Enable logging for debugging. Not recommended for production apps, as significant data
              // about MobilePush will be logged to the console.
              #if DEBUG
              MarketingCloudSDK.sharedInstance().sfmc_setDebugLoggingEnabled(true)
              #endif
          }
          
          return success
      }
  
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      FirebaseApp.configure()
      Firebase.Messaging.messaging().isAutoInitEnabled = true
      self.configureMarketingCloudSDK()
      
      
      if #available(iOS 10.0, *) {
        // For iOS 10 display notification (sent via APNS)
        UNUserNotificationCenter.current().delegate = self

        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
          options: authOptions,
          completionHandler: {_, _ in })
      } else {
        let settings: UIUserNotificationSettings =
        UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        application.registerUserNotificationSettings(settings)
      }

      application.registerForRemoteNotifications()
      
      Firebase.Messaging.messaging().token { token, error in
        if let error = error {
          print("Error fetching FCM registration token: \(error)")
        } else if let token = token {
          print("FCM registration token: \(token)")
        }
      }
      
      let jsCodeLocation: URL

      jsCodeLocation = RCTBundleURLProvider.sharedSettings().jsBundleURL(forBundleRoot: "index", fallbackResource:nil)
      let rootView = RCTRootView(bundleURL: jsCodeLocation, moduleName: "bussanautofinance", initialProperties: nil, launchOptions: launchOptions)
      let rootViewController = UIViewController()
      rootViewController.view = rootView

      self.window = UIWindow(frame: UIScreen.main.bounds)
      self.window?.rootViewController = rootViewController
      self.window?.makeKeyAndVisible()

      return true
    }
  
  
    // MobilePush SDK: REQUIRED IMPLEMENTATION
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      MarketingCloudSDK.sharedInstance().sfmc_setDeviceToken(deviceToken)
      print("Get Device Token: \(String(describing: deviceToken))")

    }

    // MobilePush SDK: REQUIRED IMPLEMENTATION
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Get Fail to RegisterRemoteToken : \(String(describing: error))")
    }
  
    // MobilePush SDK: REQUIRED IMPLEMENTATION
    /** This delegate method offers an opportunity for applications with the "remote-notification" background mode to fetch appropriate new data in response to an incoming remote notification. You should call the fetchCompletionHandler as soon as you're finished performing that operation, so the system can accurately estimate its power and data cost.
     This method will be invoked even if the application was launched or resumed because of the remote notification. The respective delegate methods will be invoked first. Note that this behavior is in contrast to application:didReceiveRemoteNotification:, which is not called in those cases, and which will not be invoked if this method is implemented. **/
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        MarketingCloudSDK.sharedInstance().sfmc_setNotificationUserInfo(userInfo)
        completionHandler(.newData)
        print("Get Receive Remote Notification: \(String(describing: userInfo))")
    }
  
    // MobilePush SDK: REQUIRED IMPLEMENTATION
    // The method will be called on the delegate when the user responded to the notification by opening the application, dismissing the notification or choosing a UNNotificationAction. The delegate must be set before the application returns from applicationDidFinishLaunching:.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // Required: tell the MarketingCloudSDK about the notification. This will collect MobilePush analytics
        // and process the notification on behalf of your application.
        MarketingCloudSDK.sharedInstance().sfmc_setNotificationRequest(response.notification.request)
      print("Get Receive Remote Notification IOS 10.0* : \(String(describing: response.notification.request))")
        completionHandler()
      
    }

    // MobilePush SDK: REQUIRED IMPLEMENTATION
    // The method will be called on the delegate only if the application is in the foreground. If the method is not implemented or the handler is not called in a timely manner then the notification will not be presented. The application can choose to have the notification presented as a sound, badge, alert and/or in the notification list. This decision should be based on whether the information in the notification is otherwise visible to the user.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(.alert)
    }
  
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
      print("Firebase registration token: \(String(describing: fcmToken))")

      let dataDict:[String: String] = ["token": fcmToken ]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
  
   
}

