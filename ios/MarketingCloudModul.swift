//
//  MarketingCloudModul.swift
//  bafmobilenew
//
//  Created by Hasbi CL on 08/03/21.
//

import Foundation
import MarketingCloudSDK

@objc(MarketingCloudModul)
class MarketingCloudModul: MarketingCloudSDK {
  
  @objc func setContactKey(_ PhoneNo: String) {
    
    MarketingCloudSDK.sharedInstance().sfmc_setContactKey(PhoneNo)
    let contactKey = MarketingCloudSDK.sharedInstance().sfmc_contactKey()
    print("Contact Key Marketing Cloud NEW: \(String(describing: contactKey))")
  }
}


