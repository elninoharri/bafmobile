//
//  CredolabModul.swift
//  bafmobilenew
//
//  Created by IT BAF on 26/08/20.
//

import Foundation
import credoappsdk

// CredolabModul.swift

@objc(CredolabModul)
class CredolabModul: RCTEventEmitter {
  
  @objc override func supportedEvents() -> [String]! {
    return ["EventReminder"];
  }
  
  @objc func setCollectCredolab(_ ReferenNumber: String, Autkey: String, Url: String) {
    // Date is ready to use!
    NSLog("%@ %@ %@", Autkey, ReferenNumber, Url)
    let ret:[String:Any] =  ["ReferenNumber": ReferenNumber, "Autkey": Autkey, "Url" : Url]
    self.sendEvent(withName: "EventReminder", body: ret)
    
    DispatchQueue.global().async {
        
         do{
             let credoAppService = try CredoAppService(url: Url, authKey: Autkey)
          _ = try credoAppService.collectData(recordNumber:ReferenNumber)
             
             
         } catch let error as CredoAppError{
             let errorType = error.getErrorType()
             let errorMessage = error.getErrorMessage()
             let eFinal:String = String(errorType.rawValue) + String(":") + String(errorMessage ?? "")
  
             
         } catch let e {
             
         }
         
    }
    
  }
}
