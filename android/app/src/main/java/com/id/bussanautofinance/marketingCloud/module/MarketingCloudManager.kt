package com.id.bussanautofinance.marketingCloud.module

import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.id.bussanautofinance.MainApplication


class MarketingCloudManager(reactContext: ReactApplicationContext?) : ReactContextBaseJavaModule(reactContext) {


    override fun getName(): String {
        return "MarketingCloudManager"
    }

    @ReactMethod
    fun salesforceSetContactKey(response: String?, contactKey: String?) {

        val application = MainApplication()
        application.setContactKey(response, contactKey)

    }


}