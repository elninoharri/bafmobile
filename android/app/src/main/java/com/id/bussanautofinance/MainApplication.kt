package com.id.bussanautofinance

import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.TextUtils
import android.util.Log
import com.facebook.react.*
import com.facebook.soloader.SoLoader
import com.google.android.gms.common.GoogleApiAvailability
import com.salesforce.marketingcloud.InitializationStatus
import com.salesforce.marketingcloud.MCLogListener
import com.salesforce.marketingcloud.MarketingCloudConfig
import com.salesforce.marketingcloud.MarketingCloudSdk
import com.salesforce.marketingcloud.notifications.NotificationCustomizationOptions
import com.salesforce.marketingcloud.notifications.NotificationManager
import io.branch.rnbranch.RNBranchModule
import java.lang.reflect.InvocationTargetException
import java.util.*

// i add this to library to material top nav
// import com.swmansion.reanimated.ReanimatedPackage;
class MainApplication : Application(), ReactApplication {
    private val mReactNativeHost: ReactNativeHost = object : ReactNativeHost(this) {
        override fun getUseDeveloperSupport(): Boolean {
            return BuildConfig.DEBUG
        }

        override fun getPackages(): List<ReactPackage> {
            val packages: MutableList<ReactPackage> = PackageList(this).packages
            // Packages that cannot be autolinked yet can be added manually here, for example:
            // packages.add(new MyReactNativePackage());
            packages.add(CustomPackage())

            //i add this  
            // packages.add( new ReanimatedPackage());
            return packages
        }

        override fun getJSMainModuleName(): String {
            return "index"
        }
    }

    override fun getReactNativeHost(): ReactNativeHost {
        return mReactNativeHost
    }

    override fun onCreate() {
        super.onCreate()
        RNBranchModule.getAutoInstance(this)
        SoLoader.init(this,  /* native exopackage */false)
        initializeFlipper(this, reactNativeHost.reactInstanceManager)

        //Saleforce

        MarketingCloudSdk.setLogLevel(MCLogListener.VERBOSE)
        MarketingCloudSdk.setLogListener(MCLogListener.AndroidLogListener())

        MarketingCloudSdk.init(this, MarketingCloudConfig.builder().apply {
            setApplicationId("d9b2f43e-da5c-4423-b78b-12cfcd931bbc")
            setAccessToken("ZjiHtz5vR4owWQohMLN96aCQ")
            setSenderId("77876047149")
            setMarketingCloudServerUrl("https://mc2zlbrlfvq87rzwn7hctn4tc-ty.device.marketingcloudapis.com/")
            setMid("514011371")
            setNotificationCustomizationOptions(
                    NotificationCustomizationOptions.create(R.drawable.ic_launcher_new)
            )
            setDelayRegistrationUntilContactKeyIsSet(true) // default = false

            // Other configuration values
            setNotificationCustomizationOptions(
                    NotificationCustomizationOptions.create(R.drawable.ic_launcher_new,
                            { context, notificationMessage ->
                                val requestCode = Random().nextInt()
                                val url = notificationMessage.url()
                                when {
                                    url.isNullOrEmpty() ->
                                        PendingIntent.getActivity(
                                                context,
                                                requestCode,
                                                Intent(context, MainActivity::class.java),
                                                PendingIntent.FLAG_UPDATE_CURRENT
                                        )
                                    else ->
                                        PendingIntent.getActivity(
                                                context,
                                                requestCode,
                                                Intent(Intent.ACTION_VIEW, Uri.parse(url)),
                                                PendingIntent.FLAG_UPDATE_CURRENT
                                        )
                                }
                            },
                            { context, notificationMessage ->
                                if (TextUtils.isEmpty(notificationMessage.url())) {
                                    NotificationManager.createDefaultNotificationChannel(context)
                                } else {
                                    "UrlNotification"
                                }
                            }
                    )
            )

        }.build(this as Context)) { status ->
            when (status.status()) {
                InitializationStatus.Status.COMPLETED_WITH_DEGRADED_FUNCTIONALITY -> {
                    if (status.locationsError()) {
                        //Handle Google Play Services issues.
                        if (GoogleApiAvailability.getInstance().isUserResolvableError(status.playServicesStatus())) {
                            // User will likely need to update GooglePlayServices through the Play Store.
                            // Call GoogleApiAvailability.getInstance().showErrorDialogFragment(...) from Activity.
                            Log.v("MyApp", "Status GoogleApiAvailability.getInstance()")
                        } else if (status.messagingPermissionError()) {
                            // User disabled location permission.
                            // Re-request permission and if granted enable desired messaging type
                            Log.e("MyApp", "Status Error")
                        }
                    }
                }
                InitializationStatus.Status.SUCCESS -> Log.v("MyApp", "Marketing Cloud init was successful")
                InitializationStatus.Status.FAILED -> Log.e(
                        "MyApp",
                        "Marketing Cloud failed to initialize.  Status: $status",
                        status.unrecoverableException()
                )
            }
        }


    }

    fun setContactKey(responseLogin: String?, contactKey: String?) {

        val setDefaultNullContactKey = "000000000"

        MarketingCloudSdk.requestSdk { sdk ->
            val registrationManager = sdk.registrationManager

            // Set Contact Key
            registrationManager.edit().run {
                if (contactKey != null && responseLogin == "200") {
                    setContactKey(contactKey)
                }else{
                    setContactKey(setDefaultNullContactKey)
                }
                commit()
            }

            // Get Contact Key
            val contactKey = registrationManager.contactKey

        }
    }

    companion object {
        /**
         * Loads Flipper in React Native templates. Call this in the onCreate method with something like
         * initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
         *
         * @param context
         * @param reactInstanceManager
         */
        private fun initializeFlipper(
                context: Context, reactInstanceManager: ReactInstanceManager
        ) {
            if (BuildConfig.DEBUG) {
                try {
                    /*
         We use reflection here to pick up the class that initializes Flipper,
        since Flipper library is not available in release mode
        */
                    val aClass = Class.forName("com.id.bussanautofinance.ReactNativeFlipper")
                    aClass
                            .getMethod("initializeFlipper", Context::class.java, ReactInstanceManager::class.java)
                            .invoke(null, context, reactInstanceManager)
                } catch (e: ClassNotFoundException) {
                    e.printStackTrace()
                } catch (e: NoSuchMethodException) {
                    e.printStackTrace()
                } catch (e: IllegalAccessException) {
                    e.printStackTrace()
                } catch (e: InvocationTargetException) {
                    e.printStackTrace()
                }
            }
        }
    }
}