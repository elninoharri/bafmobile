package com.id.bussanautofinance.privy.network

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}