package com.id.bussanautofinance;

import android.widget.Toast;

import com.facebook.react.bridge.ReactContextBaseJavaModule;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;

public class AndroidBridgingExample extends ReactContextBaseJavaModule {

    public AndroidBridgingExample(ReactApplicationContext reactContext) {
        super(reactContext); 
    }

    @Override
    //getName akan digunakan seabgai modules yang direpresentasikan dalam javascript
    public String getName() {
        return "AndroidBridgingExample";
    }

    @ReactMethod
    public void sayHi(String input, Promise promise) {
        try {
            promise.resolve("Testing input: " + input + " Callback : Greetings from Java");
        } catch (IllegalViewOperationException e) {
            promise.reject(e.getMessage());
        }
    }

    //CustomFunction
    @ReactMethod
    public void showToast(String message){
        Toast.makeText(getReactApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}
