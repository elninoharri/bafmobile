package com.id.bussanautofinance.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class PreferenceModel {

    static final String KEY_Authentication ="Auth", KEY_Url ="Url";

    private static SharedPreferences getSharedPreference(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }



    public static void setKEY_Url(Context context, String Url) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_Url,Url);
        editor.apply();
    }

    public static void setKEY_Authentication(Context context, String Authentification){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_Authentication,Authentification);
        editor.apply();
    }

    public static String getKEY_Authentication(Context context){
        return getSharedPreference(context).getString(KEY_Authentication,"");
    }

    public static String getKEY_Url(Context context) {
        return getSharedPreference(context).getString(KEY_Url,"");
    }

}
