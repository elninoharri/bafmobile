package com.id.bussanautofinance;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;
import com.id.bussanautofinance.model.AuthKeyModel;
import com.id.bussanautofinance.model.CallbackAuth;
import com.id.bussanautofinance.model.PreferenceModel;

import androidx.annotation.NonNull;

public class CredolabModul extends ReactContextBaseJavaModule implements CallbackAuth, ActivityEventListener {

    private String result = "Success";
    public CredolabModul(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @NonNull
    @Override
    public String getName() {
        return "CredolabModul";
    }


    @ReactMethod
    public void setAuthUrl(Promise promise){
        final Context context = getCurrentActivity().getApplicationContext();
        try {
              CollectCredolab collectCredolab= new CollectCredolab();
              collectCredolab.callEndpoints(context);
              collectCredolab.callback = this;
              Log.d("getAuth ", PreferenceModel.getKEY_Authentication(context));
        } catch (IllegalViewOperationException e) {
              promise.reject(e.getMessage());
        }

    }


    @ReactMethod
    public void setCollectCredolab(String ReferenNumber,String Auth , String Url,Promise promise){
        final Context context = getCurrentActivity().getApplicationContext();
        try{
            if (context == null) {
                result="activity kosong";
            }else{
                CollectCredolab collectCredolab= new CollectCredolab();
                result = collectCredolab.getCollectCredolab(context,Auth,ReferenNumber,Url);
                // result = collectCredolab.getCollectCredolab(context,PreferenceModel.getKEY_Authentication(context),ReferenNumber,PreferenceModel.getKEY_Url(context));
            }
        }catch (IllegalViewOperationException e) {
            result=e.getMessage();
        }

        promise.resolve(result);

    }


    @Override
    public void onSucess(AuthKeyModel authKeyModel) {

    }

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void onNewIntent(Intent intent) {

    }
}
