package com.id.bussanautofinance.privy.activity

import android.Manifest
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.id.bussanautofinance.R
import com.id.bussanautofinance.privy.network.ResponseApi
import com.id.bussanautofinance.privy.utils.PermissionUtils
import com.id.bussanautofinance.privy.viewmodel.LivenessViewModel
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream


class PrivyLivenessCheckActivity : AppCompatActivity() , LivenessViewModel.CallBackLivenessViewModel{

    private val REQUEST_CODE_PERMISSION = 100
    private lateinit var livenessViewModel: LivenessViewModel
    private var idCardFace: String? = ""
    private var pathSelfie: String? = "null"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initLoadData()
        initLoadView()
    }

    private fun initLoadData() {
        livenessViewModel = ViewModelProviders.of(this).get(LivenessViewModel::class.java)
        livenessViewModel?.init(this, this)

        getPhotoLiveness()
    }

    private fun initLoadView() {
            if (PermissionUtils.requestPermission(this, REQUEST_CODE_PERMISSION, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                openLiveness()
            }
    }

    private fun openLiveness() {
        if (PermissionUtils.with(this).isCameraReadyGranted) {
            livenessViewModel?.getLivenessBizToken(File(idCardFace)).observe(this, Observer {
                livenessViewModel?.processGenerateBizToken(it!!)
            })
        }
    }

    private fun getPhotoLiveness() {
        val imageBitmap = BitmapFactory.decodeResource(resources, R.drawable.photo_liveness)
        val imageOpts: BitmapFactory.Options = BitmapFactory.Options()
        imageOpts.inSampleSize = 2
        val bitmap = Bitmap.createScaledBitmap(imageBitmap,
                (imageBitmap.width / 1.5).toInt(), (imageBitmap.height / 1.5).toInt(),
                false)
        idCardFace = saveFilePhotoLiveness(bitmap).absolutePath
    }

    private fun saveFilePhotoLiveness(imageBitmap: Bitmap): File {
        val contextWrapper = ContextWrapper(getApplicationContext())
        val dir = contextWrapper.getDir("liveness", Context.MODE_PRIVATE)
        if (!dir.exists()) dir.mkdirs()

        val file = File(dir, "photo.jpg")
        val fileOutput = FileOutputStream(file)

        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutput)
        fileOutput.flush()
        fileOutput.close()
        return file
    }

    //Don't Deleted
    override fun loading() {
    }

    override fun success(data: ResponseApi) {
        val bizToken = JSONObject(data.responseString).optString("biz_token")

        if (bizToken != null) {
            livenessViewModel?.preDetectLiveness(bizToken)
        }
    }

    override fun error(data: ResponseApi) {
        finishCurrentActivityCanceled()
    }

    override fun onResult(detectToken: String, detectErrorCode: Int, detectErrorMessage: String, data: String) {
        if (detectErrorCode == 1000) {
            livenessViewModel?.verifyLivenessData(detectToken, data, object : AsyncHttpResponseHandler() {
                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray) {
                    val livenessResult = JSONObject(String(responseBody))
                    val images = livenessResult.getJSONObject("images").getString("image_best")
                    stringToBitmapSelfie(images)

                    if (pathSelfie != null) {
                        finishCurrentActivityOK()
                    } else {
                        finishCurrentActivityCanceled()
                    }
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                    finishCurrentActivityCanceled()
                }

            })
        } else {
            finishCurrentActivityCanceled()
        }
    }

    override fun onCancel() {
        finishCurrentActivityCanceled()
    }

    override fun onBackPressed() {
        finishCurrentActivityCanceled()
    }

    private fun stringToBitmapSelfie(livenessResult: String) {
        val imgBytes = com.loopj.android.http.Base64.decode(livenessResult.toByteArray(), com.loopj.android.http.Base64.DEFAULT)
        val imageBitmap = BitmapFactory.decodeByteArray(imgBytes, 0, imgBytes.size)
        val imageOpts: BitmapFactory.Options = BitmapFactory.Options()
        imageOpts.inSampleSize = 2
        val bitmap = Bitmap.createScaledBitmap(imageBitmap,
                (imageBitmap.width / 1.5).toInt(), (imageBitmap.height / 1.5).toInt(),
                false)
        pathSelfie = saveFileSelfie(bitmap).absolutePath
    }

    private fun saveFileSelfie(imageBitmap: Bitmap): File {
        var dir: File? = null
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
            dir = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "PrivyID")
        } else {
            dir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "PrivyID")
        }

        if (!dir.exists()) dir.mkdirs()

        val file = File(dir, "Selfie.jpg")
        val fileOutput = FileOutputStream(file)

        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutput)
        fileOutput.flush()
        fileOutput.close()
        return file
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (PermissionUtils.permissionGranted(requestCode, REQUEST_CODE_PERMISSION, grantResults)) {
            openLiveness()
        }
    }

    fun finishCurrentActivityCanceled(){
        val intent = Intent()
        intent.putExtra("result", pathSelfie)
        setResult(RESULT_CANCELED, intent)
        finish()
    }

    fun finishCurrentActivityOK(){
        val intent = Intent()
        intent.putExtra("result", pathSelfie)
        setResult(RESULT_OK, intent)
        finish()
    }


}