package com.id.bussanautofinance;

import androidx.annotation.NonNull;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.id.bussanautofinance.marketingCloud.module.MarketingCloudManager;
import com.id.bussanautofinance.privy.service.PrivyLivenessCheckActivityManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomPackage implements ReactPackage {
    @NonNull
    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }

    @NonNull
    @Override
    public List<NativeModule> createNativeModules(
            ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();
        //this is where you register the module
        modules.add(new CredolabModul(reactContext));
        //testing
        modules.add(new AndroidBridgingExample(reactContext));
        //privy
        modules.add(new PrivyLivenessCheckActivityManager(reactContext));
        //marketingcloud
        modules.add(new MarketingCloudManager(reactContext));
        return modules;
    }
}
