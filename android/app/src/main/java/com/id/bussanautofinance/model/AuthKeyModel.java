package com.id.bussanautofinance.model;

import com.google.gson.annotations.SerializedName;

public class AuthKeyModel {


        @SerializedName("Auth")
        public String Auth;
        @SerializedName("Url")
        public String Url;

        public String getAuth() {
            return Auth;
        }

        public void setAuth(String auth) {
            Auth = auth;
        }

        public String getUrl() {
            return Url;
        }

        public void setUrl(String url) {
            Url = url;
        }

}
