package com.id.bussanautofinance.api;

import com.id.bussanautofinance.model.AuthKeyModel;

import retrofit2.http.GET;
import rx.Observable;

public interface ApiInterfaceCredolab {
    @GET("/bafmobile/mobile/credolabs/AuthKey")
    Observable<AuthKeyModel> getAutKey();
}
