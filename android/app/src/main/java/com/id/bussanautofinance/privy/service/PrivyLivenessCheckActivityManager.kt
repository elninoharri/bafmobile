package com.id.bussanautofinance.privy.service

import android.app.Activity
import android.content.Intent
import android.widget.Toast
import com.facebook.react.bridge.*
import com.id.bussanautofinance.privy.activity.PrivyLivenessCheckActivity


class PrivyLivenessCheckActivityManager : ReactContextBaseJavaModule{

    constructor(reactContext: ReactApplicationContext?) : super(reactContext) {
        reactApplicationContext.addActivityEventListener(mActivityEventListener)
    }

    override fun getName(): String {
        return "PrivyLivenessCheckActivityManager"
    }

    private var mDoneCallback: Callback? = null
    private var mCancelCallback: Callback? = null

    //Implement callback
    private val mActivityEventListener: ActivityEventListener = object : BaseActivityEventListener() {
        override fun onActivityResult(activity: Activity?, requestCode: Int, resultCode: Int, intent: Intent?) {
            super.onActivityResult(activity, requestCode, resultCode, intent)
            if (requestCode == REQUEST_CODE) {
                if (mDoneCallback != null) {
                    if (resultCode == Activity.RESULT_CANCELED) {
                        mCancelCallback?.invoke("error")
                    } else {
                        val map = Arguments.createMap()
                        map.putString("result", intent?.extras?.getString("result"))
                        mDoneCallback?.invoke(map)
                    }
                }
                mCancelCallback = null
                mDoneCallback = null
            }else{
                // Toast.makeText(reactApplicationContext, resultCode.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }

    @ReactMethod
    fun switchToAnotherActivity(map: ReadableMap?, onDone: Callback?, onCancel: Callback?) {
        val strData = if (map?.hasKey("result") == true) map?.getString("strData") else "0"
        val currentActivity = currentActivity
        val intent = Intent(currentActivity, PrivyLivenessCheckActivity::class.java)
        mCancelCallback = onCancel
        mDoneCallback = onDone
        intent.putExtra("strData", strData)
        currentActivity?.startActivityForResult(intent, REQUEST_CODE)
    }

    companion object {
        private const val REQUEST_CODE = 1
    }

}