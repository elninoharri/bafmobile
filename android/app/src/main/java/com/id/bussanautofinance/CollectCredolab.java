package com.id.bussanautofinance;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


import com.id.bussanautofinance.api.ApiService;
import com.id.bussanautofinance.model.AuthKeyModel;
import com.id.bussanautofinance.model.CallbackAuth;
import com.id.bussanautofinance.model.PreferenceModel;

import java.util.Collection;

import credoapp.CredoAppException;
import credoapp.CredoAppService;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CollectCredolab extends AppCompatActivity {

    public String Message = "Success";

    public CallbackAuth callback;
    public void callEndpoints(Context context){
        Observable<AuthKeyModel> authKeyModelObservable = ApiService.getInternalClient().getAutKey();
        authKeyModelObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AuthKeyModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(AuthKeyModel authKeyModel) {
                        PreferenceModel.setKEY_Authentication(context,authKeyModel.getAuth());
                        PreferenceModel.setKEY_Url(context,authKeyModel.getUrl());
                        callback.onSucess(authKeyModel);
                    }
                });
    }


    public String getCollectCredolab(Context context,String Autkey, String ReferenNumber,String url){
        CredoAppService credoAppService = null;

        try{
            credoAppService = new CredoAppService(context, url, Autkey);
            Boolean ignoreDeniedPermissions = true;
            credoAppService.setIgnorePermission(ignoreDeniedPermissions);

            if(!ignoreDeniedPermissions){
                final Collection<String> ungrantedPermissions = credoAppService.getUngrantedPermissions();
                if (ungrantedPermissions.contains("android.permission.PACKAGE_USAGE_STATS")) {
                    showUsageStatsDialog(ungrantedPermissions,context);
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !ungrantedPermissions.isEmpty()) {
                    requestPermissions(ungrantedPermissions.toArray(new String[ungrantedPermissions.size()]), 0);
                }
            }

            final String rn = credoAppService.collectData(ReferenNumber);
            Message=rn+ "Success";
        }catch (CredoAppException e) {
            e.printStackTrace();
            Message = "Error" + e.getMessage();
        }

        return Message;
    }




    protected void showUsageStatsDialog(Collection<String> ungrantedPermissions, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle("Allow Usage Data");
        builder.setMessage("For the better verifying process please follow these steps:" +
                "\n1. Click 'settings' button" +
                "\n2. Select  application" +
                "\n3. Switch allow user indicator to state enable" +
                "\n4. Click 'Back' button on the top left of the screen");

        builder.setCancelable(true);
        builder.setPositiveButton(
                "settings",
                (dialog, id) -> {
                    dialog.dismiss();
                    goToUsageStatsSettings();
                });
        builder.setNegativeButton(
                "cancel",
                ((dialog, which) -> {
                    dialog.dismiss();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ungrantedPermissions.size() > 1) {
                        requestPermissions(ungrantedPermissions.toArray(new String[ungrantedPermissions.size()]), 0);
                    }
                })
        );
        builder.create().show();
    }

    private void goToUsageStatsSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_USAGE_ACCESS_SETTINGS);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(intent);
    }

}
