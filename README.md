# BAF Mobile New

Repositori untuk development aplikasi mobile SANDIA Multiproduk menggunakan React-Native CLI dengan middleware Redux Saga.

Konfigurasi git :
1. git init
2. git remote add origin http://hqdockerdev.bussan.co.id/devbussan/mobilesandiamultiproduk.git
3. git checkout [branch]

Setup untuk running untuk android :
1. Setup react-native CLI environment seperti di website https://reactnative.dev/docs/environment-setup
2. Jalankan command npm install
3. Buat emulator di android studio
4. Jika sudah terinstall react-native-cli di device, jalankan command react-native run-android. Jika belum command npx react-native run-android.
5. Jika emulator tidak terbuka, Buka android studio, buka bagian AVD dan jalankan emulator yang tersedia, kemudian kembali ke step 4.

Setup untuk running untuk ios :
1. Setup react-native CLI environment seperti di website https://reactnative.dev/docs/environment-setup
2. Jalankan command npm install
3. Jalankan command cd ios
4. Jalankan command pod install
5. Jika sudah terinstall react-native-cli di device, jalankan command react-native run-ios. Jika belum command npx react-native run-ios.
6. Untuk dijalankan di specifik simulator yang tersedia, command nya react-native run-ios --simulator="iPhone 8"

Command untuk build release android :
1. Jalankan command react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle
2. Jalankan command cd android
3. Untuk windows jalankan command gradlew assembleRelease, untuk unix jalankan command ./gradlew assembleRelease
4. Hasil release apk nya ada di path android/app/build/outputs/apk/release/[namaAplikasi].apk

Command untuk release aab :
1. sudo keytool -genkey -v -keystore baf-mobile-key.keystore -alias baf-mobile-alias -keyalg RSA -keysize 2048 -validity 10000
2. copy file keystore -> android/app

3. Add code in gradle.properties
MYAPP_UPLOAD_STORE_FILE=baf-mobile-key.keystore
MYAPP_UPLOAD_KEY_ALIAS=baf-mobile-alias
MYAPP_UPLOAD_STORE_PASSWORD=Bussan100
MYAPP_UPLOAD_KEY_PASSWORD=Bussan100

4. Add code in build.gradle
    signingConfigs {
        release {
            if (project.hasProperty('MYAPP_UPLOAD_STORE_FILE')) {
                storeFile file(MYAPP_UPLOAD_STORE_FILE)
                storePassword MYAPP_UPLOAD_STORE_PASSWORD
                keyAlias MYAPP_UPLOAD_KEY_ALIAS
                keyPassword MYAPP_UPLOAD_KEY_PASSWORD
            }
        }
        debug {
            storeFile file('debug.keystore')
            storePassword 'android'
            keyAlias 'androiddebugkey'
            keyPassword 'android'
        }
    }

    buildTypes {
        debug {
            signingConfig signingConfigs.debug
        }
        release {
            signingConfig signingConfigs.release
            minifyEnabled enableProguardInReleaseBuilds
            proguardFiles getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro"
        }
    }

5. ./gradlew bundleRelease
6. npx react-native run-android --variant=release


Disable Privy Module for IOS

1. Comment all (PrivyModul.swift)
2. Comment all (PrivyModul.m)
3. Delete PrivyLiveness_Objc.framework from (General -> Framework,Libraries, and Embedded Content)
4. Delete MGFaceIDLiveCustomDetect.bundle from (Build Phases -> Copy Bundle Resources)
5. Comment IOS callLivenessCheck on (src -> screen -> sandiaFeature -> FdeAddMp -> index -> callLivenessCheck method)
6. app ready to launch on simulator 

# Selamat Mencoba



