import * as React from 'react';
import {
  createStackNavigator,
  TransitionSpecs,
  HeaderStyleInterpolators,
} from '@react-navigation/stack';
import Welcome from '../screens/welcome';
import BAFmitra from '../screens/bafService/bafMitra';
import Home from '../screens/home';
import AuthLoading from '../screens/authentication/authLoading';
import ForgotPassword from '../screens/authentication/forgotPassword';
import SyaratPenggunaan from '../screens/authentication/syaratPenggunaan';
import TermCondition from '../screens/authentication/termCondition';

import Register from '../screens/authentication/register';
import Login from '../screens/authentication/login';
import Verifikasi from '../screens/authentication/verifikasi';
import RequestBPKPTC from '../screens/account/requestBPKB/termsCondition';
import RequestBPKP from '../screens/account/requestBPKB';

import changeProfile from '../screens/account/changeProfile';
import changePassword from '../screens/account/changePassword';

import FdeAddMp from '../screens/sandiaFeature/fdeAddMp';
import OrderList from '../screens/sandiaFeature/orderList';

import BafCare from '../screens/bafService/bafCare';
import BafPay from '../screens/bafService/bafPay';

import NoConnection from '../screens/noConnection';

import FdeDetailMp from '../screens/sandiaFeature/fdeDetailMp';

import FilterPromo from '../screens/promo/filterPromo';
import PromoDetail from '../screens/promo/promoDetail';

import UploadKtpAndroid from '../screens/camera/android/uploadKtp';
import UploadFotoKonsumenAndroid from '../screens/camera/android/uploadFotoKonsumen';
import UploadPpbkAndroid from '../screens/camera/android/uploadPpbk';
import UploadFapAndroid from '../screens/camera/android/uploadFap';
import UploadSimAndroid from '../screens/camera/android/uploadSim';
import UploadKartuKeluargaAndroid from '../screens/camera/android/uploadKartuKeluarga';
import UploadSuratKeteranganAndroid from '../screens/camera/android/uploadSuratKeterangan';

import UploadKtpIos from '../screens/camera/ios/uploadKtp';
import UploadFotoKonsumenIos from '../screens/camera/ios/uploadFotoKonsumen';
import UploadPpbkIos from '../screens/camera/ios/uploadPpbk';
import UploadFapIos from '../screens/camera/ios/uploadFap';
import UploadSimIos from '../screens/camera/ios/uploadSim';
import UploadKartuKeluargaIos from '../screens/camera/ios/uploadKartuKeluarga';
import UploadSuratKeteranganIos from '../screens/camera/ios/uploadSuratKeterangan';

import PaymentHistory from '../screens/paymentHistory';

import Information from '../screens/sandiaFeature/information';

import Summary from '../screens/sandiaFeature/fdeAddMp/form/summaryWizard';
import MitraDetail from '../screens/bafService/bafMitra/cardDetail';

import lob from '../screens/lob';
import PpbkApprovalPage from '../screens/sandiaFeature/ppbkApprovalPage';
import DetailPesan from '../components/multiPlatform/tabPesan/detailPesan';

import FilterBafMitra from '../screens/bafService/bafMitra/filterBafMitra';

//BPKB
import Bpkb from '../screens/bpkb';
import SuccessRequestBPKB from '../screens/account/requestBPKB/component/successRequestBPKB';
import BafPoin from '../screens/bafService/bafPoinTermCondition';
import BafPointsInfo from "../screens/bafService/bafPointInfo";

const Stack = createStackNavigator();

const MyTransition = {
  gestureDirection: 'horizontal',
  transitionSpec: {
    open: TransitionSpecs.TransitionIOSSpec,
    close: TransitionSpecs.TransitionIOSSpec,
  },
  headerStyleInterpolator: HeaderStyleInterpolators.forFade,
};

export function AppStack() {
  return (
    <Stack.Navigator headerMode="none" initialRouteName="AuthLoading">
      <Stack.Screen name="AuthLoading" component={AuthLoading} />
      <Stack.Screen name="Welcome" component={Welcome} />
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="BafMitra" component={BAFmitra} />
      <Stack.Screen name="MitraDetail" component={MitraDetail} />

      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="TermCondition" component={TermCondition} />
      <Stack.Screen name="SyaratPenggunaan" component={SyaratPenggunaan} />
      <Stack.Screen name="Login" component={Login} />

      <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
      <Stack.Screen name="Verifikasi" component={Verifikasi} />
      <Stack.Screen name="RequestBPKP" component={RequestBPKP} />
      <Stack.Screen name="RequestBPKPTC" component={RequestBPKPTC} />

      <Stack.Screen name="changePassword" component={changePassword} />
      <Stack.Screen name="changeProfile" component={changeProfile} />

      <Stack.Screen name="FdeAddMp" component={FdeAddMp} />
      <Stack.Screen name="OrderList" component={OrderList} />
      <Stack.Screen name="Summary" component={Summary} />
      <Stack.Screen name="Information" component={Information} />
      <Stack.Screen name="ppbkApproval" component={PpbkApprovalPage} />

      <Stack.Screen name="BafCare" component={BafCare} />
      <Stack.Screen name="BafPay" component={BafPay} />

      <Stack.Screen name="NoConnection" component={NoConnection} />
      <Stack.Screen name="FdeDetailMp" component={FdeDetailMp} />

      <Stack.Screen name="FilterPromo" component={FilterPromo} />
      <Stack.Screen name="PromoDetail" component={PromoDetail} />

      <Stack.Screen name="UploadKtpAndroid" component={UploadKtpAndroid} />
      <Stack.Screen name="UploadFapAndroid" component={UploadFapAndroid} />
      <Stack.Screen name="UploadPpbkAndroid" component={UploadPpbkAndroid} />
      <Stack.Screen
        name="UploadFotoKonsumenAndroid"
        component={UploadFotoKonsumenAndroid}
      />
      <Stack.Screen name="UploadSimAndroid" component={UploadSimAndroid} />
      <Stack.Screen
        name="UploadSuratKeteranganAndroid"
        component={UploadSuratKeteranganAndroid}
      />
      <Stack.Screen
        name="UploadKartuKeluargaAndroid"
        component={UploadKartuKeluargaAndroid}
      />

      <Stack.Screen name="UploadKtpIos" component={UploadKtpIos} />
      <Stack.Screen name="UploadFapIos" component={UploadFapIos} />
      <Stack.Screen name="UploadPpbkIos" component={UploadPpbkIos} />
      <Stack.Screen
        name="UploadFotoKonsumenIos"
        component={UploadFotoKonsumenIos}
      />
      <Stack.Screen name="UploadSimIos" component={UploadSimIos} />
      <Stack.Screen
        name="UploadSuratKeteranganIos"
        component={UploadSuratKeteranganIos}
      />
      <Stack.Screen
        name="UploadKartuKeluargaIos"
        component={UploadKartuKeluargaIos}
      />

      <Stack.Screen name="PaymentHistory" component={PaymentHistory} />

      <Stack.Screen name="lob" component={lob} />
      <Stack.Screen name="DetailPesan" component={DetailPesan} />

      <Stack.Screen name = "FilterBafMitra" component={FilterBafMitra}/>

      <Stack.Screen name = "bpkb" component={Bpkb}/>
      <Stack.Screen name = "successRequestBPKB" component={SuccessRequestBPKB}/>

      <Stack.Screen name = "BafPoin" component={BafPoin}/>
      <Stack.Screen name = "BafPointInfo" component={BafPointsInfo}/>

    </Stack.Navigator>
  );
}
