import React, {Component, useEffect, useState} from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {Linking, Platform} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {Root} from 'native-base';
import {Provider} from 'react-redux';
import {store} from './store/store';
import {AppStack} from './navigations/AppNavigator';
import {fcmService} from './pushnotif/FCMService';
import {localNotificationService} from './pushnotif/LocalNotificationService';
import {substringCircum, substringSecondCircum} from './utils/utilization';
import {
  Jumlah_Payment,
  Status_Order,
  Install_Remainder,
  Urlandroid_Home,
  Urlandroid_OrderList,
  Urlios_Home,
  Urlios_OrderList,
} from './utils/constant';

export let navigateScreen = '';

export default function App() {
  useEffect(() => {
    setTimeout(function () {
      SplashScreen.hide();
    }, 1500);

    fcmService.register(onNotification, onOpenNotification);
    localNotificationService.configure(onOpenNotification);

    function onNotification(notify) {
      console.log('onNotif : ', notify);
      const options = {
        soundName: 'default',
        playSound: true,
      };
      localNotificationService.showNotification(
        0,
        notify.notification.title,
        notify.notification.body,
        notify.data.title,
        notify.data.body,
        options,
      );
    }

    // add function to switch navigate screen
    // add if (notify) to prevent function execute double
    async function onOpenNotification(notify) {
      if (notify) {
        if (Platform.OS === 'android') {
          this.androidNavigate(notify.data.title);
        } else {
          this.iosNavigate(notify.data.title);
        }
        console.log('[App] onOpenNotification: ', notify);
      }
    }
  }, []);

  //add function android switch in here
  androidNavigate = (nav) => {
    console.log('data from android Navigate ' + nav);
    switch (nav) {
      case Status_Order:
        console.log('go to order list from app.js');
        Linking.openURL(Urlandroid_OrderList);
        break;

      default:
        Linking.openURL(Urlandroid_Home);
        break;
    }
  };

  //add function ios switch in here
  iosNavigate = (nav) => {
    switch (nav) {
      case Status_Order:
        Linking.openURL(Urlios_OrderList);
        break;

      default:
        Linking.openURL(Urlios_Home);
        break;
    }
  };

  return (
    <Provider store={store}>
      <Root>
        <NavigationContainer>
          <AppStack />
        </NavigationContainer>
      </Root>
    </Provider>
  );
}
