import {openDatabase} from 'react-native-sqlite-storage';
let db = openDatabase({name: 'sqlite_bafmobile.db', createFromLocation: 1});

export const createTableBanner = () => {
  db.transaction(function (txn) {
    txn.executeSql(
      "SELECT name FROM sqlite_master WHERE type='table' AND name='BANNER'",
      [],
      (tx, res) => {
        if (res.rows.length == 0) {
          txn.executeSql('DROP TABLE IF EXISTS BANNER', []);
          txn.executeSql(
            'CREATE TABLE IF NOT EXISTS BANNER (BANNER_NAME VARCHAR(50), BANNER_DESC VARCHAR(255))',
            [],
          );
        }
      },
    );
  });
};
