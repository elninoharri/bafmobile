import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
  BackHandler,
  Image,
  Dimensions,
  PixelRatio,
  Platform,
} from 'react-native';
import {
  Spinner,
  Container,
  View,
  Icon,
  Content,
  Button,
  Form,
  Input,
  Item,
  Header,
  Label,
} from 'native-base';
import {BAF_COLOR_BLUE} from '../../../utils/constant';
import Modal from 'react-native-modal';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {FormSubmitNikValidate} from '../../../validates/FormSubmitNikValidate';
import {styles} from './style';

export const renderField = ({
  input,
  type,
  label,
  icon,
  iconright,
  placeholder,
  editable,
  keyboardType,
  maxLength,
  secureTextEntry,
  onPressIcon,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '100%', alignItems: 'center', marginTop: 15}}>
      <Text
        style={{
          alignSelf: 'flex-start',
          marginLeft: '5.5%',
          marginBottom: '-4%',
          fontSize: 14,
          fontWeight: '500',
        }}>
        Nomor KTP
      </Text>
      <Item
        stackedLabel
        last
        style={{width: '90%', height: '15%', marginBottom: '11%'}}>
        <Label />
        <Input
          {...input}
          editable={editable}
          placeholder={placeholder}
          keyboardType={keyboardType}
          maxLength={maxLength}
          secureTextEntry={secureTextEntry}
          style={{
            fontSize: 14,
            paddingLeft: '5%',
            justifyContent: 'center',
            borderWidth: 1,
            borderColor: hasError ? 'red' : BAF_COLOR_BLUE,
            borderTopRightRadius: 5,
            borderBottomRightRadius: 5,
            borderTopLeftRadius: 5,
            borderBottomLeftRadius: 5,
            marginBottom: 2,
          }}
        />
        {hasError ? <Text style={styles.errorDesc}>{error}</Text> : null}
      </Item>
    </View>
  );
};

class ModalNik extends Component {
  render() {
    const {
      handleSubmit,
      submitting,
      isModalVisible,
      onSubmit,
      onClose,
      pristine,
    } = this.props;
    return (
      <Modal isVisible={isModalVisible}>
        <View
          style={
            Platform.OS == 'ios'
              ? Dimensions.get('window').width * PixelRatio.get() > 750
                ? {
                    marginTop: '15%',
                    width: '100%',
                    height: '24%',
                    backgroundColor: 'white',
                    borderRadius: 10,
                  }
                : {
                    marginTop: '15%',
                    width: '100%',
                    height: '32%',
                    backgroundColor: 'white',
                    borderRadius: 10,
                  }
              : Dimensions.get('window').width > 360
              ? {
                  marginTop: '15%',
                  width: '100%',
                  height: '28%',
                  backgroundColor: 'white',
                  borderRadius: 10,
                }
              : {
                  marginTop: '15%',
                  width: '100%',
                  height: '42%',
                  backgroundColor: 'white',
                  borderRadius: 10,
                }
          }>
          <TouchableOpacity
            style={{position: 'absolute', right: '2%', top: '3%', zIndex: 2000}}
            onPress={onClose}>
            <Icon style={{fontSize: 20}} type="FontAwesome" name="times" />
          </TouchableOpacity>
          <View style={{alignItems: 'center', marginTop: '5%'}}>
            <Text style={{fontSize: 14, color: BAF_COLOR_BLUE}}>
              Masukkan Nomor KTP kamu untuk mendapatkan
            </Text>
            <Text style={{fontSize: 14, color: BAF_COLOR_BLUE}}>
              informasi angsuran kamu
            </Text>
          </View>
          <Form style={{width: '100%', height: '100%'}}>
            <Field
              name="NIK"
              type="text"
              component={renderField}
              placeholder="Isi Nomor KTP Anda"
              icon="phone"
              iconright={false}
              maxLength={16}
              keyboardType="number-pad"
              editable={true}
            />
            <TouchableOpacity
              style={{
                bottom: '-6%',
                left: '35%',
                width: '27%',
                height: '20%',
                backgroundColor: pristine ? 'grey' : BAF_COLOR_BLUE,
                borderRadius: 5,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={handleSubmit(onSubmit)}
              disabled={submitting || pristine}>
              <Text style={{color: 'white', fontSize: 16, fontWeight: 'bold'}}>
                OK
              </Text>
            </TouchableOpacity>
          </Form>
        </View>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

ModalNik = reduxForm({
  form: 'formSubmitNik',
  enableReinitialize: true,
  validate: FormSubmitNikValidate,
})(ModalNik);

export default connect(mapStateToProps, matchDispatchToProps)(ModalNik);
