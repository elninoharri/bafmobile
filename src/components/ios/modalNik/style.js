import { StyleSheet, Dimensions } from 'react-native'

export const styles = StyleSheet.create({
    errorDesc : { 
        color:'red',
        alignSelf:'stretch',
        textAlign:'right',
        fontSize:10,
        fontFamily:'arial',
        marginBottom:-12
    }
});
