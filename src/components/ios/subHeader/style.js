import { StyleSheet } from 'react-native';
import {BAF_COLOR_BLUE} from '../../../utils/constant';
import {fonts} from '../../../utils/fonts';

export default StyleSheet.create({
    Icon : {
        fontSize: 16,
        marginTop: '-5%',
        marginLeft: 7,
        color:BAF_COLOR_BLUE
    },
    WifiIcon : {
        fontSize: 18,
        color:'green'
    },
    Text : {
        marginLeft:-3,
        color:BAF_COLOR_BLUE
    },
    Header : {
        backgroundColor:'white',
        borderBottomWidth:1
    },
    title : {
        fontSize:16,
        fontFamily: fonts.primary.bold,
        color:BAF_COLOR_BLUE
    }
})