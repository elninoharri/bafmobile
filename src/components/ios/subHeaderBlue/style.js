import {StyleSheet} from 'react-native';
import {BAF_COLOR_BLUE} from '../../../utils/constant';
import {fonts} from '../../../utils/fonts';

export default StyleSheet.create({
  Icon: {
    fontSize: 16,
    marginTop: '-5%',
    marginLeft: 7,
    color: 'white',
  },
  WifiIcon: {
    fontSize: 18,
    color: 'green',
  },
  Text: {
    marginLeft: -3,
    color: 'white',
  },
  Header: {
    backgroundColor: BAF_COLOR_BLUE,
  },
  title: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: 'white',
    textAlign: 'center',
  },
});
