import React, {Component} from 'react';
import {Platform, StatusBar} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
  Text,
  Icon,
  View,
} from 'native-base';
import styles from './style';
import {BAF_COLOR_BLUE} from '../../../utils/constant';

export default class SubHeaderBlue extends Component {
  render() {
    return (
      <View>
        <Header style={this.props.styleHeader || styles.Header}>
          <StatusBar
            backgroundColor={BAF_COLOR_BLUE}
            barStyle="light-content"
          />
          {/* <Left>
            {this.props.goBack ? 
              <Button hasText transparent onPress={this.props.goBack}>
                <Icon type="FontAwesome" name="arrow-left" style={styles.Icon} />
              </Button> : null
            }
          </Left>
          <Body style={{position: 'absolute'}}>
            <Text style={styles.title}>{this.props.title}</Text>
          </Body>
          <Right style={{maxWidth: 100}}>
            <Button hasText transparent>
              <Icon type="FontAwesome" name={this.props.nameIcon} style={styles.Icon} />
            </Button>
          </Right> */}
          <Left style={{maxWidth: 100}}>
            {this.props.goBack ? (
              <Button hasText transparent onPress={this.props.goBack}>
                <Icon
                  type="FontAwesome"
                  name="arrow-left"
                  style={styles.Icon}
                />
              </Button>
            ) : null}
          </Left>
          <Body>
            <Text style={styles.title}>{this.props.title}</Text>
          </Body>
          <Right style={{maxWidth: 100}}>
            <Button
              hasText
              transparent
              transparent
              onPress={this.props.goNavScreen}>
              <Icon
                type="FontAwesome"
                name={this.props.nameIcon}
                style={styles.Icon}
              />
            </Button>
          </Right>
        </Header>
      </View>
    );
  }
}
