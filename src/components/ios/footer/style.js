import { StyleSheet, Platform, StatusBar } from 'react-native';
import {BAF_COLOR_BLUE,BAF_COLOR_YELLOW} from '../../../utils/constant';
import {fonts} from '../../../utils/fonts';

export default StyleSheet.create({
    container : {
      height:'9%',
      padding:0,
      flexDirection:'row',
      backgroundColor:'white',
      shadowColor: "rgba(0,0,0,0.1)",
      shadowOpacity: 1,
      shadowRadius: 2,
      shadowOffset: {
        height: -1  ,
        width: 1
      },
      elevation:20  
    },
    topContent : {
      flex:1,
      alignItems:'center',
      justifyContent:'space-evenly',

    },
    bottomContent : {
      flex:1,
    },
    footerIcon : {
      width:"30%",
      marginTop:3,
      marginBottom:-10
    },
    footerTitleActive : {
      fontSize:10,
      fontFamily:'arial',
      marginTop:5,
      textAlign:'center',
      color:BAF_COLOR_BLUE,
      fontFamily: fonts.primary.normal,
    },
    footerTitle : {
      fontSize:10,
      fontFamily:'arial',
      marginTop:5,
      textAlign:'center',
      opacity:0.7,
      color:"grey",
      fontFamily: fonts.primary.normal,
    }

})