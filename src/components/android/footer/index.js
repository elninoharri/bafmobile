import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import styles from './style';

export default class FooterAndroid extends Component {

  render() {
    const {tabHome, tabPromo, tabPesan, tabProduk, tabAkun, tabState} = this.props
    return (
      <View style={styles.container}>
        <TouchableOpacity style={{flex: 1}} onPress={tabHome}>
          <View style={styles.topContent}>
            {tabState=='home'? 
              <Image
                resizeMode="contain"
                style={styles.footerIcon}
                source={require('../../../../assets/img/home/footer/tab-home-active.png')}
              />
            :
              <Image
                resizeMode="contain"
                style={styles.footerIcon}
                source={require('../../../../assets/img/home/footer/tab-home.png')}
              />
            }
          </View>
          <View style={styles.bottomContent}>
            <Text style={tabState=='home' ? styles.footerTitleActive : styles.footerTitle}>Beranda</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={{flex: 1}} onPress={tabPromo}>
          <View style={styles.topContent}>
            {tabState=='promo'? 
              <Image
                resizeMode="contain"
                style={styles.footerIcon}
                source={require('../../../../assets/img/home/footer/tab-promo-active.png')}
              />
            :
              <Image
                resizeMode="contain"
                style={styles.footerIcon}
                source={require('../../../../assets/img/home/footer/tab-promo.png')}
              />
            }
          </View>
          <View style={styles.bottomContent}>
            <Text style={tabState=='promo' ? styles.footerTitleActive : styles.footerTitle}>Promo</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={{flex: 1}} onPress={tabPesan}>
          <View style={styles.topContent}>
            {tabState=='pesan'? 
              <Image
                resizeMode="contain"
                style={styles.footerIcon}
                source={require('../../../../assets/img/home/footer/tab-pesan-active.png')}
              />
            :
              <Image
                resizeMode="contain"
                style={styles.footerIcon}
                source={require('../../../../assets/img/home/footer/tab-pesan.png')}
              />
            }
          </View>
          <View style={styles.bottomContent}>
            <Text style={tabState=='pesan' ? styles.footerTitleActive : styles.footerTitle}>Pesan</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={{flex: 1}} onPress={tabProduk}>
          <View style={styles.topContent}>
            {tabState=='produk'? 
              <Image
                resizeMode="contain"
                style={styles.footerIcon}
                source={require('../../../../assets/img/home/footer/tab-produk-active.png')}
              />
            :
              <Image
                resizeMode="contain"
                style={styles.footerIcon}
                source={require('../../../../assets/img/home/footer/tab-produk.png')}
              />
            }
          </View>
          <View style={styles.bottomContent}>
            <Text style={tabState=='produk' ? styles.footerTitleActive : styles.footerTitle}>Produk</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={{flex: 1}} onPress={tabAkun}>
          <View style={styles.topContent}>
            {tabState=='akun'? 
              <Image
                resizeMode="contain"
                style={styles.footerIcon}
                source={require('../../../../assets/img/home/footer/tab-akun-active.png')}
              />
            :
              <Image
                resizeMode="contain"
                style={styles.footerIcon}
                source={require('../../../../assets/img/home/footer/tab-akun.png')}
              />
            }
          </View>
          <View style={styles.bottomContent}>
            <Text style={tabState=='akun' ? styles.footerTitleActive : styles.footerTitle}>Akun</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
