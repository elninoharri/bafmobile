import { StyleSheet, Platform, StatusBar } from 'react-native';
import {BAF_COLOR_BLUE,BAF_COLOR_YELLOW} from '../../../utils/constant';
import {fonts} from '../../../utils/fonts';

export default StyleSheet.create({
    container : {
      height:55,
      padding:0,
      flexDirection:'row',
      backgroundColor:'white',
      shadowColor: "black",
      shadowOpacity: 1,
      shadowRadius: 2,
      shadowOffset: {
        height: -20,
        width: 1
      },
      elevation:20,
    },
    topContent : {
      flex:1,
      alignItems:'center',
      justifyContent:'space-evenly'
    },
    bottomContent : {
      flex:1,
    },
    footerIcon : {
      width:"30%",
      marginBottom:-15
    },
    footerTitleActive : {
      fontSize:10,
      fontFamily:'arial',
      marginTop:'10%',
      textAlign:'center',
      color:BAF_COLOR_BLUE,
      fontFamily: fonts.primary.normal,
    },
    footerTitle : {
      fontSize:10,
      fontFamily:'arial',
      marginTop:'10%',
      textAlign:'center',
      opacity:0.7,
      color:"grey",
      fontFamily: fonts.primary.normal,
    }

})