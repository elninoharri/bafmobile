import { StyleSheet, Platform, StatusBar,Dimensions } from 'react-native';
import {BAF_COLOR_BLUE} from '../../../utils/constant';

export default StyleSheet.create({
    modalContainerLarge : {
        height:210,
        backgroundColor:'white',
        borderRadius:10
    },
    modalContainerSmall : {
        height:185,
        backgroundColor:'white',
        borderRadius:10
    },
    modalHeaderTextTopLarge : {
        fontSize:14,
        color:BAF_COLOR_BLUE,
        fontFamily:'arial'
    },
    modalHeaderTextTopSmall : {
        fontSize:12,
        color:BAF_COLOR_BLUE,
        fontFamily:'arial'
    },
    modalHeaderTextBottomLarge : {
        fontSize:14,
        color:BAF_COLOR_BLUE,
        fontFamily:'arial'
    },
    modalHeaderTextBottomSmall : {
        fontSize:12,
        color:BAF_COLOR_BLUE,
        fontFamily:'arial'
    },
    modalButtonLarge :  {
        marginTop:13,
        alignSelf:'center',
        width:'27%',
        height:40,
        backgroundColor:BAF_COLOR_BLUE,
        borderRadius:4,
        alignItems:'center',
        justifyContent:'center'
    },
    modalButtonSmall : {
        marginTop:10,
        alignSelf:'center',
        width:'25%',
        height:40,
        backgroundColor:BAF_COLOR_BLUE,
        borderRadius:4,
        alignItems:'center',
        justifyContent:'center'
    },
    modalButtonLargeDisabled :  {
        marginTop:13,
        alignSelf:'center',
        width:'27%',
        height:40,
        backgroundColor:'grey',
        borderRadius:4,
        alignItems:'center',
        justifyContent:'center'
    },
    modalButtonSmallDisabled : {
        marginTop:10,
        alignSelf:'center',
        width:'25%',
        height:40,
        backgroundColor:'grey',
        borderRadius:4,
        alignItems:'center',
        justifyContent:'center'
    },
    modalButtonTextLarge : {
        color:'white',
        fontSize:16,
        fontWeight:'bold',
        fontFamily:'arial'
    },
    modalButtonTextSmall : {
        color:'white',
        fontSize:14,
        fontWeight:'bold',
        fontFamily:'arial'
    },
    errorDesc : { 
        color:'red',
        alignSelf:'stretch',
        textAlign:'right',
        fontSize:10,
        fontFamily:'arial',
        marginBottom:-14
    }
});