import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
  BackHandler,
  Image,
  Dimensions,
  PixelRatio,
  Platform,
  KeyboardAvoidingView,
} from 'react-native';
import {
  Spinner,
  Container,
  View,
  Icon,
  Content,
  Button,
  Form,
  Input,
  Item,
  Header,
  Label,
} from 'native-base';
import {BAF_COLOR_BLUE} from '../../../utils/constant';
import Modal from 'react-native-modal';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import styles from './style';
import {FormSubmitNikValidate} from '../../../validates/FormSubmitNikValidate';

export const renderField = ({
  input,
  type,
  label,
  icon,
  iconright,
  placeholder,
  editable,
  keyboardType,
  maxLength,
  secureTextEntry,
  onPressIcon,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '100%', alignItems: 'center', marginTop: 15}}>
      <Text
        style={{
          alignSelf: 'flex-start',
          marginLeft: '5.5%',
          marginBottom: '-5%',
          fontSize: 14,
          fontWeight: '500',
        }}>
        NIK
      </Text>
      <Item
        stackedLabel
        last
        style={{width: '90%', height: '15%', marginBottom: '11%'}}>
        <Label />
        <Input
          {...input}
          editable={editable}
          placeholder={placeholder}
          keyboardType={keyboardType}
          maxLength={maxLength}
          secureTextEntry={secureTextEntry}
          style={{
            fontSize: 14,
            paddingLeft: '5%',
            justifyContent: 'center',
            borderWidth: 1,
            borderColor: hasError ? 'red' : BAF_COLOR_BLUE,
            borderTopRightRadius: 5,
            borderBottomRightRadius: 5,
            borderTopLeftRadius: 5,
            borderBottomLeftRadius: 5,
          }}
        />
        {hasError ? <Text style={styles.errorDesc}>{error}</Text> : null}
      </Item>
    </View>
  );
};

class ModalNik extends Component {
  render() {
    const {
      handleSubmit,
      submitting,
      isModalVisible,
      onSubmit,
      onClose,
      pristine,
    } = this.props;
    return (
      <Modal isVisible={isModalVisible}>
        <KeyboardAvoidingView
          behavior="height"
          style={
            Dimensions.get('window').width > 360
              ? styles.modalContainerLarge
              : styles.modalContainerSmall
          }>
          <TouchableOpacity
            style={{position: 'absolute', right: '3%', top: '5%'}}
            onPress={onClose}>
            <Icon style={{fontSize: 24}} type="FontAwesome" name="times" />
          </TouchableOpacity>
          <View style={{alignItems: 'center', marginTop: '5%'}}>
            <Text
              style={
                Dimensions.get('window').width > 360
                  ? styles.modalHeaderTextTopLarge
                  : styles.modalHeaderTextTopSmall
              }>
              Masukkan Nomor KTP kamu untuk mendapatkan
            </Text>
            <Text
              style={
                Dimensions.get('window').width > 360
                  ? styles.modalHeaderTextBottomLarge
                  : styles.modalHeaderTextBottomSmall
              }>
              informasi angsuran kamu
            </Text>
          </View>
          <Form style={{width: '100%', height: '100%'}}>
            <Field
              name="NIK"
              type="text"
              component={renderField}
              placeholder="Isi Nomor KTP Anda"
              icon="phone"
              iconright={false}
              maxLength={16}
              keyboardType="number-pad"
              editable={true}
            />
            <TouchableOpacity
              style={
                Dimensions.get('window').width > 360
                  ? pristine
                    ? styles.modalButtonLargeDisabled
                    : styles.modalButtonLarge
                  : pristine
                  ? styles.modalButtonSmallDisabled
                  : styles.modalButtonSmall
              }
              onPress={handleSubmit(onSubmit)}
              disabled={submitting || pristine}>
              <Text
                style={
                  Dimensions.get('window').width > 360
                    ? styles.modalButtonTextLarge
                    : styles.modalButtonTextSmall
                }>
                OK
              </Text>
            </TouchableOpacity>
          </Form>
        </KeyboardAvoidingView>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

ModalNik = reduxForm({
  form: 'formSubmitNik',
  enableReinitialize: true,
  validate: FormSubmitNikValidate,
})(ModalNik);

export default connect(mapStateToProps, matchDispatchToProps)(ModalNik);
