import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
  BackHandler,
  Keyboard,
  Image,
  Dimensions,
  PixelRatio,
  Platform,
  KeyboardAvoidingView,
} from 'react-native';
import {
  Spinner,
  Container,
  View,
  Icon,
  Content,
  Button,
  Form,
  Input,
  Item,
  Header,
  Label,
} from 'native-base';
import CardSlider from 'react-native-cards-slider';
import styles from './style';
import CustomAlertComponent from '../../../multiPlatform/customAlert/CustomAlertComponent';
import NetInfo from '@react-native-community/netinfo';
import {BlurView, VibrancyView} from '@react-native-community/blur';
import {BAF_COLOR_BLUE} from '../../../../utils/constant';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {substringDot, fdeFormatCurrency} from '../../../../utils/utilization';
import {reduxForm, Field, change, reset} from 'redux-form';
import moment from 'moment';
import ModalNik from '../../modalNik';
import {submitNik} from '../../../../actions/home';
import AsyncStorage from '@react-native-community/async-storage';
import {fonts} from '../../../../utils/fonts';

export const renderField = ({
  input,
  placeholder,
  editable,
  keyboardType,
  maxLength,
  secureTextEntry,
  onPressIcon,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '100%', alignItems: 'center', marginTop: 15}}>
      <Text
        style={
          Dimensions.get('window').width > 360
            ? {
                alignSelf: 'flex-start',
                marginLeft: '5.5%',
                marginBottom: '-6%',
                fontFamily: fonts.primary.normal,
                fontSize: 14,
                fontWeight: 'bold',
                fontFamily: 'arial',
              }
            : {
                alignSelf: 'flex-start',
                marginLeft: '5.5%',
                marginBottom: '-8%',
                fontFamily: fonts.primary.normal,
                fontSize: 12,
                fontWeight: 'bold',
                fontFamily: 'arial',
              }
        }>
        NIK
      </Text>
      <Item
        stackedLabel
        last
        style={{width: '90%', height: 40, marginBottom: 10}}>
        <Label />
        <Input
          {...input}
          editable={editable}
          placeholder={placeholder}
          keyboardType={keyboardType}
          maxLength={maxLength}
          secureTextEntry={secureTextEntry}
          style={{
            fontFamily: fonts.primary.normal,
            fontSize: 14,
            paddingLeft: '5%',
            justifyContent: 'center',
            borderWidth: 1,
            borderColor: BAF_COLOR_BLUE,
            borderRadius: 4,
          }}
        />
      </Item>
    </View>
  );
};

class InstallmentsCard extends Component {
  constructor(props) {
    super(props);
    this.onSubmitNik = this.onSubmitNik.bind(this);
    this.statusHandling = this.statusHandling.bind(this);
    this.dueDateHandling = this.dueDateHandling.bind(this);
    this.amountInstallment = this.amountInstallment.bind(this);
    this.navigateHistoryPembayaran = this.navigateHistoryPembayaran.bind(this);
    this.state = {
      isConnected: false,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      isModalVisible: false,
      isDetailInstallmentModalVisible: false,
      detailData: false,
      isKeyboardShow: false,
      InstAmt: false,
      NextInstDt: false,
      statusInstallment: 'TIDAK AKTIF',
      isShowButtonHistory: 'none',
    };
  }

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.refs.toast.show(
          <View style={{flexDirection: 'row'}}>
            <Icon
              type="FontAwesome"
              name="exclamation-circle"
              style={{color: 'red'}}
            />
            <Text style={styles.checkInternet}>Tidak Ada Koneksi Internet</Text>
          </View>,
          DURATION.LENGTH_LONG,
        );
      }
    });
  };

  componentDidMount = async () => {
    this.checkInternet();
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {submitNikError, submitNikResult} = this.props;

    if (submitNikResult && prevProps.submitNikResult !== submitNikResult) {
      this.props.resetForm('formSubmitNik');
    }

    // if (submitNikError && prevProps.submitNikError !== submitNikError) {
    //   if (submitNikError.message === ERROR_AUTH) {
    //     await AsyncStorage.removeItem('userToken');
    //     await AsyncStorage.removeItem('userData');
    //     await AsyncStorage.removeItem('detailUser');
    //     await AsyncStorage.removeItem('activeAgreement');
    //   }
    //   this.props.resetForm('formSubmitNik');
    // }
  };

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    this.setState({
      isKeyboardShow: true,
    });
  };

  _keyboardDidHide = () => {
    this.setState({
      isKeyboardShow: false,
    });
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  navigateHistoryPembayaran = (params) => {
    const {goPaymentHistory} = this.props;
    this.setState({
      isModalVisible: false,
      isDetailInstallmentModalVisible: false,
    });
    goPaymentHistory(params);
  };

  navigateCaraPembayaran = () => {
    const {goBafPay} = this.props;
    this.setState({
      isModalVisible: false,
      isDetailInstallmentModalVisible: false,
    });
    goBafPay();
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  showModal = () => {
    this.setState({isModalVisible: true});
  };

  showDetailInstallmentModal = (data) => {
    this.setState({
      isDetailInstallmentModalVisible: true,
      detailData: data,
      InstAmt: fdeFormatCurrency(substringDot(data.InstAmt)),
      NextInstDt:
        parseFloat(data.NextInstSum) / parseFloat(data.Tenor) === 1
          ? '-'
          : moment(data.NextInstDt).format('DD MMM YYYY'),
      statusInstallment:
        parseFloat(data.NextInstSum) / parseFloat(data.Tenor) === 1
          ? 'TIDAK AKTIF'
          : 'AKTIF',
      isShowButtonHistory:
        parseFloat(data.NextInstSum) / parseFloat(data.Tenor) === 1
          ? 'none'
          : 'flex',
    });
  };

  toggleDetailInstallmentModal = () => {
    this.setState({
      isDetailInstallmentModalVisible: false,
    });
  };

  toggleModal = () => {
    this.setState({isModalVisible: false});
    this.props.resetForm('formSubmitNik');
  };

  showLoginUnregisteredCard = () => {
    return (
      <View
        style={
          Dimensions.get('window').width <= 360
            ? styles.installmentsCardContainer360
            : styles.installmentsCardContainer
        }>
        <View style={styles.ICImage}>
          <Image
            source={require('../../../../../assets/img/home/kontrak.png')}
            style={
              Dimensions.get('window').width <= 360
                ? styles.icon360
                : styles.icon
            }
          />
        </View>
        <View style={styles.topCardContainer}>
          <View style={styles.noContainer}>
            <View style={styles.noTextContainer}>
              <Text
                style={
                  Dimensions.get('window').width <= 360
                    ? styles.installmentsNo360
                    : styles.installmentsNo
                }>
                No.
              </Text>
            </View>
            <View style={styles.rightSideNoContainer}></View>
          </View>

          {Dimensions.get('window').width <= 360 ? (
            <View style={styles.totalInstallmentContainerSmall}>
              <View style={{flex: 1, alignItems: 'flex-start'}}>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 10,
                    color: 'black',
                  }}>
                  Jumlah Angsuran
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 10,
                    color: 'black',
                    fontWeight: 'bold',
                  }}>
                  -
                </Text>
              </View>
              <View style={{flex: 1}}>
                <View style={{flex: 1, alignItems: 'flex-start'}}>
                  <Text
                    style={{
                      fontFamily: fonts.primary.normal,
                      fontSize: 10,
                      color: 'black',
                    }}>
                    Jatuh Tempo
                  </Text>
                  <Text
                    style={{
                      fontFamily: fonts.primary.normal,
                      fontSize: 10,
                      color: 'black',
                      fontWeight: 'bold',
                    }}>
                    -
                  </Text>
                </View>
              </View>
            </View>
          ) : (
            <View style={styles.totalInstallmentContainerLarge}>
              <View style={{flex: 1, alignItems: 'flex-start'}}>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 12,
                    color: 'black',
                  }}>
                  Jumlah Angsuran
                </Text>
              </View>
              <View style={{flex: 1}}>
                <View style={{flex: 1, alignItems: 'flex-start'}}>
                  <Text
                    style={{
                      fontFamily: fonts.primary.normal,
                      fontSize: 12,
                      color: 'black',
                    }}>
                    Jatuh Tempo
                  </Text>
                </View>
              </View>
            </View>
          )}
          <BlurView
            style={styles.absolute}
            viewRef={this.state.viewRef}
            blurType="light"
            blurAmount={Platform.OS == 'android' ? 1 : 2}
          />
        </View>
        <TouchableOpacity
          style={styles.registerYourNikButtonContainer}
          onPress={() => this.showModal()}>
          <Text
            style={
              Dimensions.get('window').width * PixelRatio.get() > 750
                ? styles.buttonSubmitNikLarge
                : styles.buttonSubmitNikSmall
            }>
            Daftarkan Nomor KTP Kamu{' '}
          </Text>
        </TouchableOpacity>
        <View
          style={
            Dimensions.get('window').width <= 400
              ? styles.activeDesc360
              : styles.activeDesc
          }>
          <Text
            style={
              Dimensions.get('window').width <= 400
                ? styles.activeDescText360
                : styles.activeDescText
            }>
            {this.state.statusInstallment}
          </Text>
        </View>
      </View>
    );
  };

  showNoInstallmentCard = () => {
    const {goPaymentHistory = async () => {}, goSubmitOrder} = this.props;
    return (
      <TouchableOpacity
        style={
          Dimensions.get('window').width <= 360
            ? styles.installmentsCardContainer360
            : styles.installmentsCardContainer
        }>
        {this.showAlert()}
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: '2%',
          }}>
          <Image
            source={require('../../../../../assets/img/home/kontrak.png')}
            style={
              Dimensions.get('window').width <= 360
                ? styles.icon360
                : styles.icon
            }
          />
        </View>
        <View style={styles.guestTopCardContainer}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 1, marginTop: '4%'}}>
              <Text
                style={
                  Dimensions.get('window').width <= 360
                    ? styles.installmentsNo360
                    : styles.installmentsNo
                }>
                No. -
              </Text>
            </View>
            {/* <View style={{flex:2,flexDirection:'row',paddingTop:4, paddingRight:10, justifyContent:'flex-end'}}>
                                    <Text style={Dimensions.get('window').width <= 360 ? styles.noContractText360 : styles.noContractText}>Dapatkan No Kontrak,</Text>
                                    <TouchableOpacity onPress={this.props.userData? goSubmitOrder : goPaymentHistory = async () => {

                                    }}><Text style={Dimensions.get('window').width <= 360 ? styles.claimText360 : styles.claimText}> Ajukan</Text></TouchableOpacity>
                                </View> */}
          </View>
          {Dimensions.get('window').width <= 360 ? (
            <View style={{flex: 1, flexDirection: 'row', marginTop: '-2%'}}>
              <View style={{flex: 1, alignItems: 'flex-start'}}>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 10,
                    color: 'black',
                  }}>
                  Jumlah Angsuran
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 10,
                    color: 'black',
                    fontWeight: 'bold',
                  }}>
                  -
                </Text>
              </View>
              <View style={{flex: 1}}>
                <View style={{flex: 1, alignItems: 'flex-start'}}>
                  <Text
                    style={{
                      fontFamily: fonts.primary.normal,
                      fontSize: 10,
                      color: 'black',
                    }}>
                    Jatuh Tempo
                  </Text>
                  <Text
                    style={{
                      fontFamily: fonts.primary.normal,
                      fontSize: 10,
                      color: 'black',
                      fontWeight: 'bold',
                    }}>
                    -
                  </Text>
                </View>
              </View>
            </View>
          ) : (
            <View style={{flex: 1, flexDirection: 'row', marginTop: '-3%'}}>
              <View style={{flex: 1, alignItems: 'flex-start'}}>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 12,
                    color: 'black',
                  }}>
                  Jumlah Angsuran
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 12,
                    color: 'black',
                    fontWeight: 'bold',
                  }}>
                  -
                </Text>
              </View>
              <View style={{flex: 1}}>
                <View style={{flex: 1, alignItems: 'flex-start'}}>
                  <Text
                    style={{
                      fontFamily: fonts.primary.normal,
                      fontSize: 12,
                      color: 'black',
                    }}>
                    Jatuh Tempo
                  </Text>
                  <Text
                    style={{
                      fontFamily: fonts.primary.normal,
                      fontSize: 12,
                      color: 'black',
                      fontWeight: 'bold',
                    }}>
                    -
                  </Text>
                </View>
              </View>
            </View>
          )}
        </View>
        <View
          style={
            Dimensions.get('window').width <= 400
              ? styles.activeDesc360
              : styles.activeDesc
          }>
          <Text
            style={
              Dimensions.get('window').width <= 400
                ? styles.activeDescText360
                : styles.activeDescText
            }>
            {this.state.statusInstallment}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  statusHandling = (data) => {
    let statusInstallment =
      parseFloat(data.NextInstSum) / parseFloat(data.Tenor) === 1
        ? 'TIDAK AKTIF'
        : 'AKTIF';
    return statusInstallment;
  };

  dueDateHandling = (data) => {
    let dueDate =
      parseFloat(data.NextInstSum) / parseFloat(data.Tenor) === 1
        ? '-'
        : moment(data.NextInstDt).format('DD MMM YYYY');
    return dueDate;
  };

  amountInstallment = (data) => {
    let amount =
      parseFloat(data.NextInstSum) / parseFloat(data.Tenor) === 1
        ? '-'
        : fdeFormatCurrency(substringDot(data.InstAmt));
    return amount;
  };

  showSlideCard = (data) => {
    return (
      <TouchableOpacity
        style={
          Dimensions.get('window').width <= 360
            ? styles.installmentsCardContainer360
            : styles.installmentsCardContainer
        }
        onPress={() => {
          this.showDetailInstallmentModal(data);
        }}>
        {this.showAlert()}
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: '2%',
          }}>
          <Image
            source={require('../../../../../assets/img/home/kontrak.png')}
            style={
              Dimensions.get('window').width <= 360
                ? styles.icon360
                : styles.icon
            }
          />
        </View>
        <View
          style={{
            flex: 6,
            borderTopRightRadius: 10,
            borderBottomRightRadius: 10,
          }}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 1, marginTop: '4%'}}>
              <Text
                style={
                  Dimensions.get('window').width <= 360
                    ? styles.installmentsNo360
                    : styles.installmentsNo
                }>
                No. {data.AgrNo}
              </Text>
            </View>
          </View>
          {Dimensions.get('window').width <= 360 ? (
            <View style={{flex: 1, flexDirection: 'row', marginTop: '-2%'}}>
              <View style={{flex: 1, alignItems: 'flex-start'}}>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 10,
                    color: 'black',
                  }}>
                  Jumlah Angsuran
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 10,
                    color: 'black',
                    fontWeight: 'bold',
                  }}>
                  {fdeFormatCurrency(substringDot(data.InstAmt))}
                </Text>
              </View>
              <View style={{flex: 1}}>
                <View
                  style={{marginLeft: '25%', position: 'absolute', top: -10}}>
                  <Text
                    style={{
                      fontFamily: fonts.primary.normal,
                      fontSize: 10,
                      color: 'black',
                    }}>
                    Jatuh Tempo
                  </Text>
                  <Text
                    style={{
                      fontFamily: fonts.primary.normal,
                      fontSize: 10,
                      color: 'black',
                      fontWeight: 'bold',
                    }}>
                    {this.dueDateHandling(data)}
                  </Text>
                </View>
              </View>
            </View>
          ) : (
            <View style={{flex: 1, flexDirection: 'row', marginTop: '-3%'}}>
              <View style={{flex: 1, alignItems: 'flex-start'}}>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 12,
                    color: 'black',
                  }}>
                  Jumlah Angsuran
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 12,
                    color: BAF_COLOR_BLUE,
                    fontWeight: 'bold',
                    marginTop: '2%',
                  }}>
                  {fdeFormatCurrency(substringDot(data.InstAmt))}
                </Text>
              </View>
              <View style={{flex: 1}}>
                <View
                  style={{marginLeft: '25%', position: 'absolute', top: -10}}>
                  <Text
                    style={{
                      fontFamily: fonts.primary.normal,
                      fontSize: 12,
                      color: 'black',
                    }}>
                    Jatuh Tempo
                  </Text>
                  <Text
                    style={{
                      fontFamily: fonts.primary.normal,
                      fontSize: 12,
                      color: BAF_COLOR_BLUE,
                      fontWeight: 'bold',
                    }}>
                    {this.dueDateHandling(data)}
                  </Text>
                </View>
              </View>
            </View>
          )}
        </View>
        <View
          style={
            Dimensions.get('window').width <= 400
              ? styles.activeDesc360
              : styles.activeDesc
          }>
          <Text
            style={
              Dimensions.get('window').width <= 400
                ? styles.activeDescText360
                : styles.activeDescText
            }>
            {this.statusHandling(data)}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  loadingCard = (data) => {
    return (
      <TouchableOpacity
        style={
          Dimensions.get('window').width <= 360
            ? styles.installmentsCardContainer360
            : styles.installmentsCardContainer
        }>
        <Spinner color="#002f5f" />
      </TouchableOpacity>
    );
  };

  onSubmitNik = async (data) => {
    this.props.submitNIK(data.NIK);
    this.setState({isModalVisible: false});
  };

  render() {
    const {
      userData,
      activeAgreement,
      getActiveAgreementLoading,
      submitting,
    } = this.props;
    const {detailData, InstAmt, NextInstDt, isModalVisible} = this.state;

    return (
      <View style={{flex: 1}}>
        <ModalNik
          isModalVisible={isModalVisible}
          onSubmit={this.onSubmitNik}
          onClose={() => {
            this.toggleModal();
          }}
        />

        <Modal isVisible={this.state.isDetailInstallmentModalVisible}>
          <View
            style={
              Dimensions.get('window').width > 360
                ? {
                    marginTop: '-6%',
                    width: '100%',
                    height: '40%',
                    backgroundColor: 'white',
                    borderRadius: 10,
                    borderLeftWidth: 10,
                    borderLeftColor: BAF_COLOR_BLUE,
                  }
                : {
                    marginTop: '15%',
                    width: '100%',
                    height: '45%',
                    backgroundColor: 'white',
                    borderRadius: 10,
                    borderLeftWidth: 10,
                    borderLeftColor: BAF_COLOR_BLUE,
                  }
            }>
            <View
              style={{
                width: '18%',
                height: '10%',
                backgroundColor: BAF_COLOR_BLUE,
                alignItems: 'center',
                justifyContent: 'center',
                borderTopRightRadius: 10,
                borderBottomRightRadius: 10,
              }}>
              <Text style={{color: 'white', fontWeight: 'bold', fontSize: 9}}>
                {this.state.statusInstallment}
              </Text>
            </View>
            <TouchableOpacity
              style={{
                position: 'absolute',
                right: '3%',
                top: '5%',
                zIndex: 2000,
              }}
              onPress={() => this.toggleDetailInstallmentModal()}>
              <Icon
                style={{fontFamily: fonts.primary.normal, fontSize: 24}}
                type="FontAwesome"
                name="times"
              />
            </TouchableOpacity>
            <View style={{position: 'absolute', top: '15%', left: '3%'}}>
              <Image
                source={require('../../../../../assets/img/home/kontrak.png')}
                style={
                  Dimensions.get('window').width <= 360
                    ? styles.icon360
                    : styles.iconDetail
                }
              />
            </View>
            <View style={{alignItems: 'center'}}>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontFamily: fonts.primary.normal,
                  fontSize: 16,
                }}>
                No. {detailData.AgrNo}
              </Text>
            </View>
            <View style={{flexDirection: 'row', marginTop: '3%'}}>
              <View style={{flex: 1, paddingLeft: '18%'}}>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 12,
                    color: BAF_COLOR_BLUE,
                  }}>
                  Jumlah Angsuran
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 14,
                    fontWeight: '700',
                    color: BAF_COLOR_BLUE,
                    marginTop: '5%',
                  }}>
                  {InstAmt}
                </Text>
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 12,
                    color: BAF_COLOR_BLUE,
                  }}>
                  Jatuh Tempo
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 14,
                    fontWeight: '700',
                    color: BAF_COLOR_BLUE,
                    marginTop: '5%',
                  }}>
                  {parseFloat(detailData.NextInstSum) /
                    parseFloat(detailData.Tenor) ===
                  1
                    ? '-'
                    : NextInstDt}
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: '5%'}}>
              <View style={{flex: 1, alignItems: 'center'}}>
                <Text style={{fontFamily: fonts.primary.normal, fontSize: 12}}>
                  Tenor
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 13,
                    fontWeight: '700',
                    color: BAF_COLOR_BLUE,
                    marginTop: '15%',
                  }}>
                  {detailData.Tenor} bulan
                </Text>
              </View>
              <View style={{flex: 1, alignItems: 'center'}}>
                <Text style={{fontFamily: fonts.primary.normal, fontSize: 12}}>
                  Produk
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 13,
                    fontWeight: '700',
                    color: BAF_COLOR_BLUE,
                    marginTop: '15%',
                    textAlign: 'center',
                  }}>
                  {detailData.LOBCode == 'C01'
                    ? 'Motor Yamaha'
                    : detailData.LOBCode == 'C03'
                    ? 'Flexy'
                    : detailData.LOBCode == 'C05'
                    ? 'Motor Bekas'
                    : detailData.LOBCode == 'C08'
                    ? 'Mobil'
                    : detailData.LOBCode == 'C09'
                    ? 'Peralatan Berat'
                    : detailData.LOBCode == 'C15'
                    ? 'Multiproduct'
                    : detailData.LOBCode == 'S01'
                    ? 'Motor Syariah'
                    : detailData.LOBCode == 'S05'
                    ? 'Motor Bekas Syariah'
                    : detailData.LOBCode == 'S15'
                    ? 'Dana Syariah'
                    : '-'}
                </Text>
              </View>
              <View style={{flex: 1, alignItems: 'center'}}>
                <Text style={{fontFamily: fonts.primary.normal, fontSize: 12}}>
                  Angsuran
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.primary.normal,
                    fontSize: 13,
                    fontWeight: '700',
                    color: BAF_COLOR_BLUE,
                    marginTop: '15%',
                    textAlign: 'center',
                  }}>
                  Ke-{detailData.NextInstSum}/{detailData.Tenor}
                </Text>
              </View>
            </View>
            {/* <View style={{alignItems: 'center', display: this.state.isShowButtonHistory}}> */}
            <View
              style={{
                alignItems: 'center',
                flexDirection: 'row',
                // justifyContent: 'space-between',
              }}>
              <TouchableOpacity
                onPress={() => this.navigateHistoryPembayaran(detailData)}
                disabled={submitting}
                style={styles.buttonHistoryPembayaran}>
                <Text style={styles.buttonTextDown}>
                  <Icon
                    style={{
                      fontFamily: fonts.primary.normal,
                      fontSize: 18,
                      color: 'white',
                    }}
                    type="FontAwesome"
                    name="retweet"
                  />
                  History Pembayaran
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.navigateCaraPembayaran()}
                disabled={submitting}
                style={styles.buttonBayarAngsuran}>
                <Text style={styles.buttonTextDown}>Bayar Angsuran</Text>
              </TouchableOpacity>
            </View>
            <View style={{alignItems: 'center'}}></View>
          </View>
        </Modal>

        <CardSlider>
          {getActiveAgreementLoading
            ? this.loadingCard()
            : userData == false
            ? this.showNoInstallmentCard()
            : userData.Usergroupid == '1'
            ? this.showLoginUnregisteredCard()
            : activeAgreement
            ? activeAgreement.length
              ? activeAgreement.map((data, index) =>
                  this.showSlideCard(data, index),
                )
              : null
            : this.showNoInstallmentCard()}
        </CardSlider>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    submitNikResult: state.submitNik.result,
    submitNikLoading: state.submitNik.loading,
    submitNikError: state.submitNik.error,
    getActiveAgreementLoading: state.getActiveAgreement.loading,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      submitNik,
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

export default connect(mapStateToProps, matchDispatchToProps)(InstallmentsCard);
