import { StyleSheet } from 'react-native';
import {fonts} from '../../../utils/fonts';

export default StyleSheet.create({
    Icon : {
        fontSize: 14,
        color:'#002f5f'
    },
    WifiIcon : {
        fontSize: 18,
        color:'green'
    },
    Text : {
        marginLeft:-3,
        color:'#002f5f'
    },
    Header : {
        backgroundColor:'white',
        borderBottomWidth:0,
    },
    title : {
        fontSize:16,
        fontFamily: fonts.primary.normal,
        color:'#002f5f'
    }
})