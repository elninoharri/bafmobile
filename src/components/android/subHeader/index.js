import React, { Component } from "react";
import { Platform, StatusBar } from "react-native";
import { Container, Header, Left, Body, Right, Button, Title, Text,Icon, View } from 'native-base';
import styles from './style';
import {BAF_COLOR_BLUE} from '../../../utils/constant'

export default class SubHeader extends Component {
  render() {
    return (
    <View>
        <Header style={styles.Header}>
          <StatusBar backgroundColor={BAF_COLOR_BLUE} barStyle="light-content"/>
          <Left>
            <Button hasText transparent onPress={this.props.goBack}>
              <Icon type="FontAwesome" name="chevron-left" style={styles.Icon} />
            </Button>
          </Left>
          <Body style={{position:'absolute'}}>
            <Text style={styles.title}>{this.props.title}</Text>
          </Body>
          <Right>
            <Button hasText transparent>
              {/* <Icon type="FontAwesome" name="wifi" style={styles.WifiIcon} /> */}
            </Button>
          </Right>
        </Header>
        
        </View>
    );
  }
}