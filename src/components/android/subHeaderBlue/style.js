import {StyleSheet} from 'react-native';
import {BAF_COLOR_BLUE} from '../../../utils/constant';
import {fonts} from '../../../utils/fonts';
export default StyleSheet.create({
  Icon: {
    fontSize: 16,
    color: 'white',
  },
  Text: {
    marginLeft: -3,
    color: '#002f5f',
    fontFamily: fonts.primary.normal,
  },
  Header: {
    backgroundColor: BAF_COLOR_BLUE,
    borderBottomWidth: 0,
  },
  title: {
    fontSize: 16,
    fontFamily: fonts.primary.normal,
    color: 'white',
  },
});
