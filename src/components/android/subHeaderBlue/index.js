import React, {Component} from 'react';
import {Platform, StatusBar} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
  Text,
  Icon,
  View,
} from 'native-base';
import styles from './style';
import {BAF_COLOR_BLUE} from '../../../utils/constant';

export default class SubHeaderBlue extends Component {
  render() {
    const {goNavScreen} = this.props;
    return (
      // <View>
      //   <Header style={this.props.styleHeader || styles.Header}>
      //     <StatusBar
      //       backgroundColor={BAF_COLOR_BLUE}
      //       barStyle="light-content"
      //     />
      //     <Left>
      //       {this.props.goBack ? (
      //         <Button hasText transparent onPress={this.props.goBack}>
      //           <Icon
      //             type="FontAwesome"
      //             name="arrow-left"
      //             style={styles.Icon}
      //           />
      //         </Button>
      //       ) : null}
      //     </Left>
      //     <Body style={{position: 'absolute'}}>
      //       <Text style={styles.title}>{this.props.title}</Text>
      //     </Body>
      //     <Right>
      //       <Button hasText transparent onPress={goNavScreen}>
      //         <Icon
      //           type="FontAwesome"
      //           name={this.props.nameIcon}
      //           style={styles.Icon}
      //         />
      //       </Button>
      //     </Right>
      //   </Header>
      // </View>

      <View>
        <Header style={this.props.styleHeader || styles.Header}>
          <StatusBar
            backgroundColor={BAF_COLOR_BLUE}
            barStyle="light-content"
          />
          <Left style={{flex: 1}}>
            {this.props.goBack ? (
              <Button hasText transparent onPress={this.props.goBack}>
                <Icon
                  type="FontAwesome"
                  name="arrow-left"
                  style={styles.Icon}
                />
              </Button>
            ) : null}
          </Left>
          <Body style={{alignItems:'center', flex: 4.9}}>
            <Text numberOfLines={1} style={styles.title}>{this.props.title}</Text>
          </Body>
          <Right style={{flex: 1}}>
            <Button hasText transparent onPress={goNavScreen}>
              <Icon
                type="FontAwesome"
                name={this.props.nameIcon}
                style={styles.Icon}
              />
            </Button>
          </Right>
        </Header>
      </View>
      
    );
  }
}
