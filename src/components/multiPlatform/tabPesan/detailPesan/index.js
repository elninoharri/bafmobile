import React, {Component} from 'react';
import {
  TouchableOpacity,
  TouchableHighlight,
  Text,
  Alert,
  View,
  Image,
  BackHandler,
  Platform,
  Keyboard,
  Dimensions,
  PixelRatio,
} from 'react-native';
import {
  Container,
  Form,
  Label,
  Input,
  Item,
  Card,
  Left,
  CardItem,
  Right,
  Body,
  Row,
  Icon,
  Spinner,
  Content,
} from 'native-base';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import SubHeaderAndroid from '../../../android/subHeaderBlue';
import SubHeaderIos from '../../../ios/subHeaderBlue';
import {styles} from '../style';
import {fonts} from '../../../../utils/fonts';
import HTML from 'react-native-render-html';
import Inbox from '../../../../../assets/img/inbox/inbox.png';
import pic from '../../../../../assets/img/inbox/pic.png';
import moment from 'moment';
import {readNotif} from '../../../../actions/home';
export default class DetailPesan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Pesan',
      detailPesan: [],
    };
  }

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  htmlConvert = (data) => {
    let dataMod = data;
    const flag = data.NotifBody.includes('<p>');
    if (flag) {
      return data;
    } else {
      dataMod.NotifBody = `<p>${data.NotifBody}</p>`;
      return dataMod;
    }
  };

  componentDidMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    const data = this.props.route.params;
    const dataMod = this.htmlConvert(data.item);
    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) {
      this.props.readNotif({ InboxNotifID :  data.item.InboxNotifID}, userToken);
    }


    this.setState({detailPesan: dataMod});
  }

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      readNotifError,
    } = this.props;

    if (
      readNotifError &&
      prevProps.readNotifError !== readNotifError
    ) {
      console.log('readNotifError ', readNotifError);
    }

    
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  render() {
    const {detailPesan} = this.state;
    return (
      <Container>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}
        <Content style={{backgroundColor: 'white'}}>
          <View style={styles.header}>
            <Image source={Inbox} style={styles.imgInbox} />
            <View style={styles.headText}>
              <Text style={styles.titleHead}>{detailPesan.NotifTitle}</Text>
              <Text style={{marginLeft: -4}}>
                {' '}
                {moment(detailPesan.DtmCrt)
                  .utc()
                  .locale('en')
                  .format('YYYY-MM-DD, h:mm a')}
              </Text>
            </View>
          </View>

          {/* ini gambar mocking untuk styling */}
          {/* <Image
            source={pic}
            style={styles.imgContent}
          /> */}
          <View style={styles.detailContent}>
            <HTML
              html={detailPesan.NotifBody}
              tagsStyles={{
                p: {
                  textAlign: 'justify',

                  fontFamily: fonts.primary.normal,
                  lineHeight: 20,
                },
              }}
              containerStyle={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              baseFontStyle={styles.detailFont}
            />
          </View>
        </Content>
      </Container>
    );
  }
}



function mapStateToProps(state) {
  return {
    readNotifError: state.readNotif.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      readNotif,
    },
    dispatch,
  );
}
// export default connect(mapStateToProps, matchDispatchToProps)(DetailPesan);
