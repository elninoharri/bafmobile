import {StyleSheet, Dimensions, PixelRatio} from 'react-native';
import {BAF_COLOR_BLUE} from '../../../utils/constant';
const {width: WIDTH} = Dimensions.get('window');
import {fonts} from '../../../utils/fonts';

export const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  productImg: {
    width: WIDTH,
  },
  categoryButtonWrapper: {
    backgroundColor: 'white',
    height: 115,
    paddingVertical: 10,
    marginLeft: 8,
  },
  imageWrapper: {
    width: '100%',
  },

  toast: {
    marginTop:
      Platform.OS != 'android'
        ? Dimensions.get('window').width * PixelRatio.get() > 750
          ? 44
          : 20
        : 0,
    backgroundColor: '#FFE6E9',
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'red',
    justifyContent: 'center',
    paddingLeft: 50,
    height: 50,
    borderRadius: 0,
  },
  inbox: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    marginLeft: 26,
    marginRight: 20,
    marginTop: 10,
    // backgroundColor: "black"
  },
  inboxIos: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    // backgroundColor: "black"
  },

  inboxSmall: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    // backgroundColor: "black"
  },

  noInbox: {
    height: 180,
    alignSelf: 'center',
    resizeMode: 'contain',
    marginVertical: '10%',
    // backgroundColor:"yellow"
  },
  title: {
    fontFamily: fonts.primary.bold,
    color: 'black',
    fontSize: Dimensions.get('window').width > 360 ? 18 : 16,
    // backgroundColor: "grey"
  },
  titleIos: {
    fontFamily: fonts.primary.bold,
    color: 'black',
    fontSize: 18,
    marginTop: 10,
    // backgroundColor: "grey"
  },
  subtitle: {
    width: Dimensions.get('window').width * 0.7,
    maxHeight: 100,
    fontFamily: fonts.primary.normal,
    fontSize: 16,
    lineHeight: 20,
    textAlign: 'left',
  },
  subtitleIos: {
    marginLeft: 5,
    marginTop: 5,
    fontFamily: fonts.primary.normal,
    fontSize: 16,
    lineHeight: 20,
    textAlign: 'left',
  },

  wrapIcon: {
    marginLeft: Dimensions.get('window').width > 360 ? 10 : 15,
  },
  subtitleFont: {
    fontSize: Dimensions.get('window').width > 360 ? 17 : 14,
    fontFamily: fonts.primary.normal,
    lineHeight: Dimensions.get('window').width > 360 ? 21 : 21,
    // backgroundColor: "pink",
  },

  detailFont: {
    fontSize: Dimensions.get('window').width > 360 ? 18 : 16,
    fontFamily: fonts.primary.normal,
    color: 'rgba(0, 0, 0, 0.6)',
    lineHeight: 20,
  },

  wrapSubtitle: {
    marginLeft: 5,
    // backgroundColor: "pink",
  },

  trash: {
    width: 35,
    height: 45,
    marginLeft: 20,
    resizeMode: 'contain',
    // backgroundColor:"yellow"
  },

  undelete: {
    color: 'white',
    fontFamily: fonts.primary.normal,
    fontSize: 18,
    marginRight: 20,
  },

  date: {
    marginRight: 10,
    alignSelf: 'flex-end',
    marginTop: 6,
    marginBottom: 5,
    fontFamily: fonts.primary.bold,
    color: BAF_COLOR_BLUE,
  },
  dateIos: {
    alignSelf: 'flex-end',
    marginTop: '3%',
    fontFamily: fonts.primary.bold,
    color: BAF_COLOR_BLUE,
  },

  dateSmall: {
    marginLeft: 100,
    fontFamily: fonts.primary.bold,
    color: BAF_COLOR_BLUE,
  },

  textnoInbox: {
    fontSize: 16,
    color: 'grey',
    alignSelf: 'center',
    fontFamily: fonts.primary.normal,
  },

  rowFront: {
    backgroundColor: '#FFF',
    paddingTop: 10,
    paddingBottom: 15,
    shadowColor: '#999',
    elevation: 5,
  },

  rowFrontIos: {
    backgroundColor: '#FFF',
    borderColor: 'rgba(0, 0, 0, 0.2)',
    height: 120,
    shadowColor: '#999',
    elevation: 5,
    borderTopWidth: 1,
    flexDirection: 'row',
    // marginBottom: 10,
  },

  rowBack: {
    alignItems: 'center',
    backgroundColor: 'red',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
    margin: 5,
    marginBottom: 15,
    borderRadius: 5,
  },
  // ___________________
  // style detail pesan
  headText: {
    marginLeft: 20,
    flexDirection: 'column',
    // backgroundColor: "pink"
  },
  titleHead: {
    fontSize: 20,
    color: 'black',
    // alignItems: 'flex-start',
    fontFamily: fonts.primary.normal,
  },
  details: {
    fontSize: 18,
    // color:"grey",
    lineHeight: 20,
    fontFamily: fonts.primary.normal,
  },
  detailContent: {
    // alignSelf:'center',
    marginLeft: 25,
    marginRight: 25,
  },
  imgInbox: {
    width: Dimensions.get('window').width > 360 ? 45 : 35,
    height: Dimensions.get('window').width > 360 ? 45 : 35,
    resizeMode: 'contain',
  },
  imgContent: {
    marginTop: 35,
    marginBottom: 30,
    width: 414,
    height: 212,
    resizeMode: 'contain',
  },

  header: {
    marginTop: 30,
    marginBottom: 30,
    marginLeft: 20,
    flexDirection: 'row',
    // backgroundColor: "grey",
  },
});
