import React, {Component} from 'react';
import {
  TouchableOpacity,
  TouchableHighlight,
  Text,
  Alert,
  View,
  Image,
  BackHandler,
  Platform,
  Keyboard,
  Dimensions,
  PixelRatio,
  RefreshControl,
} from 'react-native';
import {
  Container,
  Form,
  Label,
  Input,
  Item,
  Card,
  Left,
  CardItem,
  Right,
  Body,
  Row,
  Icon,
  Content,
} from 'native-base';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {change, reset} from 'redux-form';
import SubHeaderAndroid from '../../android/subHeaderBlue';
import SubHeaderIos from '../../ios/subHeaderBlue';
import {styles} from './style';
import {fonts} from '../../../utils/fonts';
import noInbox from '../../../../assets/img/inbox/no-inbox.png';
import trash from '../../../../assets/img/inbox/trash-2.png';
import unreadInbox from '../../../../assets/img/inbox/unread-inbox.png';
import iconInbox from '../../../../assets/img/inbox/icon-inbox.png';
import readInbox from '../../../../assets/img/inbox/read-inbox.png';
import {SwipeListView} from 'react-native-swipe-list-view';
import {deleteNotif, getListNotif} from '../../../actions/home';
import {refreshToken} from '../../../actions';
import HTML from 'react-native-render-html';
import {getFormValues} from 'redux-form';
import moment from 'moment';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Spinner from 'react-native-loading-spinner-overlay';
import {
  BAF_COLOR_BLUE, TOKEN_EXP,
  MESSAGE_TOKEN_EXP,
  MESSAGE_TOKEN_INVALID,
} from '../../../utils/constant';
import { jsonParse, handleRefreshToken } from '../../../utils/utilization';

class TabPesan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Pesan',
      // ini nanti buat mocking data nya
      listData: [
        {
          InboxNotifID: 1,
          NotifTitle: 'Status Order',
          NotifBody:
            'Status order Anda dengan Nomor Order BAFMP2020060014481 telah.',
          DataTitle: 'Status order Anda ',
          DataBody:
            'Miliki barang incaran dengan harga terbaik pasti jadi impian setiap orang dan termasukkamu kan ? Kalau gitu memang paling pas carinya yacuma di aplikasi BAF Mobile. Kemudahan yang bisa menjalin kamu dengan pilihan harga terbaik untuk barang yang kamu cari. Gak cuma itu, saat transaksi kamu bisa manfaatin pembiayaan kredit pada BAF Mobile juga loh !',
          IsSeen: 1,
          DtmCrt: '12 Mar 2010',
        },
      ],

      DataList: [],
      setlistData: [],
      userToken: false,
      prevIndex: null,
      alertShowed: false,
    };
  }

  reloadScreen = async () =>{
    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) {
      this.setState({userToken: userToken});
      this.props.getListNotif({NotifCategory: ''}, userToken);
    }
  } 

  componentDidMount = async () => {
   this.reloadScreen();
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      deleteNotifResult,
      deleteNotifError,
      getListNotifResult,
      getListNotifError,
      readNotifResult,
      refreshTokenResult,
      refreshTokenError,
    } = this.props;

    if (
      getListNotifError &&
      prevProps.getListNotifError !== getListNotifError
    ) {
      const data = await jsonParse(getListNotifError.message); //di sini ngeparse sekaligus ngecek apakah data kiriman error berupa json
      if (data) {
        if (data.message == MESSAGE_TOKEN_EXP) {
          this.props.refreshToken(this.state.userToken);
        } else if(data.message == MESSAGE_TOKEN_INVALID) {
          this.handleInvalid();
        }
      } else {
        console.log('getListNotifError ', getListNotifError);
      }
    }

    if (
      getListNotifResult &&
      prevProps.getListNotifResult !== getListNotifResult
    ) {
      this.setState({DataList: getListNotifResult.AllData});
    }
    if (
      readNotifResult &&
      prevProps.readNotifResult !== readNotifResult
    ) {
      this.props.getListNotif({NotifCategory: ''}, this.state.userToken);
    }

    if (deleteNotifError && prevProps.deleteNotifError !== deleteNotifError) {
      const data = await jsonParse(deleteNotifError.message); //di sini ngeparse sekaligus ngecek apakah data kiriman error berupa json
      if (data) {
        if (data.message == MESSAGE_TOKEN_EXP) {
          this.props.refreshToken(this.state.userToken);
        } else if(data.message == MESSAGE_TOKEN_INVALID) {
          this.handleInvalid();
        }
      } else {
        this.setState({
          alertShowed: true,
          alertMessage: 'Error Internal Server',
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
     
    }

    if (
      deleteNotifResult &&
      prevProps.deleteNotifResult !== deleteNotifResult
    ) {
      const newData = this.state.DataList;
      newData.splice(this.state.prevIndex, 1);
      this.setState({setlistData: newData});
    }

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      console.log('Error Refresh Token  ', refreshTokenError.message);
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      await handleRefreshToken(refreshTokenResult.Refreshtoken);
      this.setState({ userToken: refreshTokenResult.Refreshtoken });
      this.props.getListNotif({NotifCategory: ''},  refreshTokenResult.Refreshtoken);
    }
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        testIdPositiveButton="OK"
        testIdNegativeButton="Cancel"
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
    if (this.state.alertFor === 'invalidToken'){
      this.props.goLogin();
      this.setState({  alertFor: null });
    }
  };

  handleInvalid = async () => {
    await AsyncStorage.removeItem('userToken');
    await AsyncStorage.removeItem('userData');
    await AsyncStorage.removeItem('detailUser');
    await AsyncStorage.removeItem('activeAgreement');
    this.setState({
      alertFor: 'invalidToken',
      alertShowed: true,
      alertMessage: 'Sesi login Anda sudah habis',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  //event ketika sudah diswipe dan row nya hilang
  onRowDidOpen = () => {
    // console.log("onRowDidOpen");
  };

  onLeftAction = () => {
    console.log('onLeftAction');
  };
  onRightAction = () => {
    console.log('onRightAction');
  };
  onLeftActionStatusChange = () => {
    console.log('onLeftActionStatusChange');
  };
  onRightActionStatusChange = () => {
    console.log('onRightActionStatusChange');
  };

  closeRow = (rowMap, rowKey) => {
    if (rowMap[rowKey]) {
      rowMap[rowKey].closeRow();
    }
  };

  // readNotif = (data) =>{
  //   this.props.readNotif({ InboxNotifID : data}, this.state.userToken);
  //   console.log("data read notif ", data );
  // }

  deleteRow = (rowMap, rowKey) => {
    this.closeRow(rowMap, rowKey);
    this.props.deleteNotif({InboxNotifID: rowKey}, this.state.userToken);
    const PrevIndex = this.state.DataList.findIndex(
      (item) => item.InboxNotifID === rowKey,
    );
    this.setState({prevIndex: PrevIndex});
  };

  onRowOpen = () => {
    console.log('onrowOpen');
  };

  renderImage = (data) => {
    if (data == 1) {
      return (
        <Image
          style={
            Platform.OS === 'android'
              ? Dimensions.get('window').width > 360
                ? styles.inbox
                : styles.inboxSmall
              : styles.inboxIos
          }
          source={readInbox}
        />
      );
    } else {
      return (
        <Image
          style={
            Platform.OS === 'android'
              ? Dimensions.get('window').width > 360
                ? styles.inbox
                : styles.inboxSmall
              : styles.inboxIos
          }
          source={unreadInbox}
        />
      );
    }
  };

  //render content view
  renderItem = (data, rowMap) => {
    const {goDetailPesan} = this.props;
    return Platform.OS === 'android' ? (
      <View style={styles.rowFront}>
        <Text style={styles.date}>
          {' '}
          {moment(data.item.DtmCrt).format('DD MMM YYYY')}{' '}
        </Text>
        <TouchableOpacity onPress={() => goDetailPesan(data) }>
          <View style={{flexDirection: 'row'}}>
            {this.renderImage(data.item.IsSeen)}
            <View>
              <Text numberOfLines={1} style={styles.title}>
                {' '}
                {data.item.NotifTitle}{' '}
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={styles.wrapSubtitle}>
                  <Text
                    numberOfLines={2}
                    style={styles.subtitle}>
                    {data.item.NotifBody}
                  </Text>
                </View>
                <View style={styles.wrapIcon}>
                  <Icon
                    style={{ fontSize: 20 }}
                    type="FontAwesome"
                    name="angle-right"
                  />
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    ) : (
      <View style={styles.rowFrontIos}>
        <View
          style={{flex: 1.2, justifyContent: 'center', alignItems: 'center'}}>
          {this.renderImage(data.item.IsSeen)}
        </View>
        <TouchableOpacity style={{flex: 4}} onPress={() => goDetailPesan(data)}>
          <Text style={styles.dateIos}>
            {' '}
            {moment(data.item.DtmCrt).format('DD MMM YYYY')}{' '}
          </Text>
          <Text numberOfLines={1} style={styles.titleIos}>
            {' '}
            {data.item.NotifTitle}{' '}
          </Text>
          <Text numberOfLines={2} style={styles.subtitleIos}>
            {' '}
            {data.item.NotifBody}{' '}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{flex: 0.4, alignItems: 'center', justifyContent: 'center'}}
          onPress={() => goDetailPesan(data)}>
          <Icon
            style={{fontSize: 25, color: BAF_COLOR_BLUE}}
            type="FontAwesome"
            name="angle-right"
          />
        </TouchableOpacity>
      </View>
    );
  };

  //hidden content after swipe left
  renderHidden = (data, rowMap) => {
    return (
      <View style={styles.rowBack}>
        <TouchableHighlight
          onPress={() => this.deleteRow(rowMap, data.item.InboxNotifID)}>
          <View style={{backgroundColor: 'red'}}>
            <Image source={trash} style={styles.trash} />
          </View>
        </TouchableHighlight>

        <TouchableHighlight
          onPress={() => this.closeRow(rowMap, data.item.InboxNotifID)}>
          <View style={{backgroundColor: 'red'}}>
            <Text style={styles.undelete}>Tap to Undeleted</Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  };

  render() {
    const {deleteNotifLoading} = this.props;
    return (
      <Container style={styles.page}>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid title={this.state.screenTitle} />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} />
        )}

        <Spinner
          visible={deleteNotifLoading ? true : false}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />

        {this.showAlert()}
        {this.state.DataList === false || this.state.DataList.length === 0 ? (
          <>
            <Content
              style={{backgroundColor: 'white'}}
              refreshControl={
                <RefreshControl
                  refreshing={false}
                  onRefresh={() => this.reloadScreen()}
                />
              }>
              <View>
                <Image source={noInbox} style={styles.noInbox} />
                <Text style={styles.textnoInbox}>Belum ada pesan</Text>
              </View>
            </Content>
          </>
        ) : (
          <>
            <SwipeListView
              data={this.state.DataList}
              refreshControl={
                <RefreshControl
                  refreshing={false}
                  onRefresh={this.props.reloadScreen}
                />
              }
              renderItem={this.renderItem}
              keyExtractor={(item) => item.InboxNotifID}
              renderHiddenItem={this.renderHidden}
              leftOpenValue={75}
              // ini fungsi dia biar gk kebuka sedikit padding di sebelah kanan nya
              rightOpenValue={-450}
              disableRightSwipe
              onRowDidOpen={this.onRowDidOpen}
              // onRowOpen={this.onRowOpen}
              leftActivationValue={100}
              // ini buat balikin card nya ketika sudah di swipe
              rightActivationValue={-500}
              leftActionValue={0}
              rightActionValue={-500}
              onLeftAction={this.onLeftAction}
              onRightAction={this.onRightAction}
              onLeftActionStatusChange={this.onLeftActionStatusChange}
              onRightActionStatusChange={this.onRightActionStatusChange}
            />
          </>
        )}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    getListNotifResult: state.getListNotif.result,
    getListNotifError: state.getListNotif.error,
    deleteNotifResult: state.deleteNotif.result,
    deleteNotifError: state.deleteNotif.error,
    deleteNotifLoading: state.deleteNotif.loading,
    readNotifResult: state.readNotif.result,
    refreshTokenResult: state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getListNotif,
      deleteNotif,
      refreshToken
    },
    dispatch,
  );
}
export default connect(mapStateToProps, matchDispatchToProps)(TabPesan);
