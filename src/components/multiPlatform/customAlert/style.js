import {Dimensions, StyleSheet} from 'react-native';
import {fonts} from '../../../utils/fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  mainOuterComponent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'#00000088'
  },
  mainContainer: {
    flexDirection: 'column',
    height: 270,
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 4,
  },
  topPart: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent:'center',
    paddingTop:20
  },
  middlePart: {
    flex: 1,
    width: '100%',
    justifyContent:'center',
    paddingVertical: 10,
    paddingHorizontal: 10
  },
  bottomPart: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  alertIconStyle: {
    marginTop:15,
    height: '100%',
    resizeMode: 'contain',
  },
  alertTitleTextStyle: {
    textAlign: 'center',
    color: '#232363',
    fontSize: 16,
    fontFamily: fonts.primary.normal,
    fontWeight: 'bold',
    marginBottom: 10
  },
  alertMessageTextStyle: {
    color: '#666',
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.primary.normal,
  },
  alertMessageButtonPositiveStyle: {
    height: 40,
    width: '30%',
    borderRadius: 4,
    backgroundColor: '#002F5F',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 5,
  },
  alertMessageButtonNegativeStyle: {
    height: 40,
    width: '30%',
    borderRadius: 4,
    backgroundColor: '#d9534f',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 5,
  },
  alertMessageButtonNegativeStyleWhite: {
    height: 40,
    width: '30%',
    borderRadius: 4,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    borderColor : 'rgba(86, 103, 137, 0.26)',
    borderWidth: 1,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 5,
  },
  alertMessageButtonTextStyle: {
    fontSize: 14,
    fontFamily: fonts.primary.normal,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  alertMessageButtonTextStyleGray: {
    fontSize: 14,
    fontFamily: fonts.primary.normal,
    fontWeight: 'bold',
    color: 'rgba(21, 25, 32, 0.5)',
  },
  IconSuccess : {
    fontSize: 80,
    color:'#5cb85c'
  },
  IconError : {
    fontSize: 80,
    color:'#d9534f'
  },
  IconAlert : {
    fontSize: 80,
    color:'#428bca'
  },
  IconInfo : {
    fontSize: 80,
    color:'#5bc0de'
  },
});
