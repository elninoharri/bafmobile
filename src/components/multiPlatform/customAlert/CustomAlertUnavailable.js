import React from 'react';
import {StyleSheet, Modal, TouchableOpacity, Image} from 'react-native';
import {Text, Icon, View} from 'native-base';
import styles from './style';
import PropTypes from 'prop-types';

export default class CustomAlertComponent extends React.Component {
  onNegativeButtonPress = () => {
    this.props.onPressNegativeButton();
  };

  onPositiveButtonPress = () => {
    this.props.onPressPositiveButton();
  };

  render() {
    return (
      <Modal
        visible={this.props.displayAlert}
        transparent={true}
        animationType={'fade'}>
        <View style={styles.mainOuterComponent}>
          <View style={styles.mainContainer}>
            {/* First Row - Alert Icon and Title */}
            <View style={styles.topPart}>
              {/* {
                this.props.alertType === 'success' ? <Icon name="checkmark-circle-outline" style={styles.IconSuccess}/> :
                  this.props.alertType === 'confirm' ? <Icon name="help-circle-outline" style={styles.IconAlert}/> :
                    this.props.alertType === 'info' ? <Icon name="information-circle-outline" style={styles.IconInfo}/> :
                    <Icon name="close-circle-outline" style={styles.IconError}/>
              } */}

              {this.props.displayAlertIcon && (
                <Image
                  source={
                    this.props.alertType === 'success'
                      ? require('../../../../assets/img/alert_success.jpg')
                      : this.props.alertType === 'confirm'
                      ? require('../../../../assets/img/alert_question.jpg')
                      : this.props.alertType === 'info'
                      ? require('../../../../assets/img/alert_info.jpg')
                      : require('../../../../assets/img/alert_failed.jpg')
                  }
                  resizeMode={'contain'}
                  style={styles.alertIconStyle}
                />
              )}
            </View>

            {/* Second Row - Alert Message Text */}
            <View style={styles.middlePart}>
              <Text style={styles.alertTitleTextStyle}>
                {`${this.props.alertTitleText}`}
              </Text>
            </View>

            {/* Third Row - Positive and Negative Button */}
            <View style={styles.bottomPart}>
              {this.props.displayNegativeButton && (
                <TouchableOpacity
                  onPress={this.onNegativeButtonPress}
                  style={styles.alertMessageButtonNegativeStyle}>
                  <Text style={styles.alertMessageButtonTextStyle}>
                    {this.props.negativeButtonText}
                  </Text>
                </TouchableOpacity>
              )}
              
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

// export default CustomAlertComponent;
