import React, { Component } from 'react';
import { Image, BackHandler, TouchableOpacity, Dimensions } from 'react-native';
import { Toast, View, Right, Text } from 'native-base';
import {
  Container,
  Left,
  Icon,
  CardItem,
  Row,
  Col,
  Item,
  Card,
  Content,
} from 'native-base';
import SubHeaderAndroid from '../../android/subHeaderBlue';
import SubHeaderIos from '../../ios/subHeaderBlue';
import { styles } from './style';
import { connect } from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import { BAF_COLOR_BLUE } from '../../../utils/constant';
import FingerprintPopup from '../fingerprint/componentPopup';
import AsyncStorage from '@react-native-community/async-storage';
import { APP_VERSION } from '../../../utils/constant';
import moment from 'moment';
import 'moment/locale/id';
import Spinner from 'react-native-loading-spinner-overlay';

class TabAkun extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
    };
  }

  logOut = () => {
    const { goLogout } = this.props;
    this.setState({ spinner: true });
    setTimeout(() => {
      goLogout();
      this.setState({ spinner: false });
    }, 1000);
  };

  render() {
    const {
      userData,
      goLogin,
      goPesananSaya,
      goChangePassword,
      goRequestBPKB,
      goRegister,
      goChangeProfile,
      detailUser,
      fingerPrint,
      handleFingerprintShowed,
      handleFingerprintDismissed,
      popupShowed,
      onAuthenticate,
    } = this.props;



    const { height } = Dimensions.get('window');

    return (
      <Container style={{ flex: 1 }}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{ color: '#FFF' }}
        />
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title="Akun"
          />
        ) : (
            <SubHeaderIos
              title="Akun"
            />
          )}
        <Content style={{ marginTop: -2 }}>
          <View style={{ height: '7%', width: '100%', backgroundColor: BAF_COLOR_BLUE }} />
          <View
            disableKBDismissScroll={false}
            style={{
              paddingHorizontal: '3%',
              flex: 1,
              alignItems: 'center',
            }}>
            <Card style={styles.cardStyle}>
              <TouchableOpacity>
                <CardItem
                  style={{ alignSelf: 'center', backgroundColor: '#F9F9F9' }}>
                  <Image
                    style={{
                      borderRadius: 50,
                      backgroundColor: '#F9F9F9',
                      width: 80,
                      height: 80,
                    }}
                    source={
                      detailUser.UsrImg
                        ? { uri: detailUser.UsrImg }
                        : require('../../../../assets/img/thumb_profile.png')
                    }
                  />
                </CardItem>
              </TouchableOpacity>
              {detailUser && (
                <TouchableOpacity
                  onPress={goChangeProfile}
                  style={styles.editIcon}>
                  <CardItem style={{ backgroundColor: '#F9F9F9' }}>
                    <Icon
                      type="FontAwesome"
                      name="edit"
                      style={{
                        fontSize: 20,
                        color: '#002f5f',
                        backgroundColor: '#F9F9F9',
                      }}
                    />
                  </CardItem>
                </TouchableOpacity>
              )}
              <CardItem cardBody style={styles.cardItemStyle}>
                <Text
                  style={!userData ? styles.headerText : styles.headerText2}>
                  {!detailUser
                    ? `Jadilah member dan nikmati \n keuntungan BAF`
                    : detailUser.CustID === '' || detailUser.CustID === '-'
                      ? `BAF Friends`
                      : `BAF Loyal`}
                </Text>
              </CardItem>
              <CardItem
                style={{
                  marginBottom: -18,
                  justifyContent: 'center',
                  backgroundColor: '#f9f9f9',
                }}>
                <View style={styles.lineStyle}></View>
              </CardItem>
              {!detailUser ? (
                <CardItem style={styles.cardItem}>
                  <TouchableOpacity onPress={goLogin}>
                    <Text style={styles.textOption}>Masuk</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={goRegister}>
                    <Text style={styles.textOption}>
                      {` `}/{` `}Daftar
                    </Text>
                  </TouchableOpacity>
                </CardItem>
              ) : (
                  <CardItem style={styles.cardItem}>
                    <Text style={styles.textTag}>{detailUser.Username}</Text>
                  </CardItem>
                )}
              {detailUser.Email && (
                <CardItem style={styles.cardItem2}>
                  <Text style={styles.textTag2}>{detailUser.Email}</Text>
                </CardItem>
              )}
              {detailUser.CustID &&
                (detailUser.CustID !== '-' || detailUser.CustID !== '') ? (
                  <CardItem style={styles.cardItem2}>
                    <Text style={styles.textTag2}>{`Customer ID `}</Text>
                    <Text style={styles.textTag3}>{detailUser.CustID}</Text>
                  </CardItem>
                ) : null}
            </Card>
            <View style={{ marginTop: 20, marginLeft: -8 }}>
              <Item style={styles.itemStyle}>
                <Icon name="card" style={styles.iconStyle2} />
                <Text style={styles.textDetail}>
                  {detailUser.NIK ? detailUser.NIK : `-`}
                </Text>
              </Item>
              <Item style={styles.itemStyle}>
                <Icon name="call" style={styles.iconStyle2} />
                <Text style={styles.textDetail}>
                  {detailUser.Phoneno ? detailUser.Phoneno : `-`}
                </Text>
              </Item>
              <Item style={styles.itemStyle}>
                <Icon
                  type="FontAwesome"
                  name="birthday-cake"
                  style={styles.iconStyle2}
                />
                <Text style={styles.textDetail}>
                  {detailUser.BirthDate
                    ? moment(detailUser.BirthDate).format('LL')
                    : `-`}
                </Text>
              </Item>
              <Item style={styles.itemStyleEnd}>
                <Icon name="home" style={styles.iconStyle3} />
                <Text style={styles.textDetail}>
                  {detailUser.UsrAdr ? detailUser.UsrAdr : `-`}
                </Text>
              </Item>
            </View>
            <Card transparent style={styles.bottomMenuTop}>
              <TouchableOpacity
                onPress={detailUser ? goPesananSaya : goLogin}>
                <CardItem>
                  <Left>
                    <Text style={styles.textMenu}>Pesanan Saya</Text>
                  </Left>
                  <Right>
                    <Icon type="Ionicons" name="ios-arrow-forward" />
                  </Right>
                </CardItem>
              </TouchableOpacity>
            </Card>
            <Card transparent style={styles.bottomMenu}>
              <TouchableOpacity onPress={detailUser ? goRequestBPKB : goLogin}>
                <CardItem>
                  <Left>
                    <Text style={styles.textMenu}>Pengambilan BPKB</Text>
                  </Left>
                  <Right>
                    <Icon type="Ionicons" name="ios-arrow-forward" />
                  </Right>
                </CardItem>
              </TouchableOpacity>
              {/* <TouchableOpacity>
                <CardItem>
                  <Left>
                    <Text style={styles.textMenu}>Riwayat Kontrak</Text>
                  </Left>
                  <Right>
                    <Icon type="Ionicons" name="ios-arrow-forward" />
                  </Right>
                </CardItem>
              </TouchableOpacity>
            </Card>
            <Card transparent style={styles.bottomMenu}> */}
            </Card>
            <Card transparent style={styles.bottomMenu}>
              <TouchableOpacity
                onPress={detailUser ? goChangePassword : goLogin}>
                <CardItem>
                  <Left>
                    <Text style={styles.textMenu}>Ubah Password</Text>
                  </Left>
                  <Right>
                    <Icon type="Ionicons" name="ios-arrow-forward" />
                  </Right>
                </CardItem>
              </TouchableOpacity>
            </Card>
            <Card transparent style={styles.bottomMenu}>
              <TouchableOpacity
                onPress={detailUser ? handleFingerprintShowed : goLogin}>
                <CardItem>
                  <Left>
                    <Text style={styles.textMenu}>
                      Aktifkan Fingerprint Login
                    </Text>
                  </Left>
                  <Right>
                    <Icon
                      type="FontAwesome"
                      name={fingerPrint ? 'toggle-on' : 'toggle-off'}
                    />
                  </Right>
                </CardItem>
              </TouchableOpacity>
            </Card>
            <Card transparent style={styles.bottomMenu}>
              <CardItem>
                <Left>
                  <Text style={styles.textMenu}>BAF Mobile</Text>
                </Left>
                <Right>
                  <Text style={styles.textMenu}>{`v` + APP_VERSION}</Text>
                </Right>
              </CardItem>
            </Card>
            {detailUser ? (
              <Card transparent style={styles.bottomMenu}>
                <TouchableOpacity onPress={this.logOut}>
                  <CardItem style={{ paddingLeft: 26 }}>
                    <Left>
                      <Icon
                        type="Ionicons"
                        name="md-exit"
                        style={{ fontSize: 20, color: '#646464' }}
                      />
                      <Text style={styles.textMenu}>
                        {`  `}Keluar dari aplikasi
                      </Text>
                    </Left>
                  </CardItem>
                </TouchableOpacity>
              </Card>
            ) : null}
            <View style={{ height: height * 0.15 }} />
          </View>
        </Content>
        {popupShowed && (
          <FingerprintPopup
            style={styles.popup}
            handlePopupDismissed={handleFingerprintDismissed}
            onAuthenticate={onAuthenticate}
          />
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(TabAkun);
