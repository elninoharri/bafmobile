import {StyleSheet, Dimensions, Platform, PixelRatio} from 'react-native';
import {fonts} from '../../../utils/fonts';

const {width} = Dimensions.get('window');

export const styles = StyleSheet.create({
  containerContent: {
    paddingHorizontal: '3%',
  },

  cardStyle: {
    borderRadius: 8,
    backgroundColor: '#f9f9f9',
    marginTop: '-10%',
    width: '85%',
    paddingVertical: 5,
  },
  itemStyle: {
    width: width * 0.8,
    height: 35,
  },
  cardItemStyle: {
    justifyContent: 'center',
    backgroundColor: '#f9f9f9',
  },
  textOption: {
    color: '#002f5f',
    fontSize:
      Platform.OS == 'android'
        ? Dimensions.get('window').width > 360
          ? 16
          : 14
        : Dimensions.get('window').width * PixelRatio.get() > 750
        ? 14
        : 16,

    fontFamily: fonts.primary.bold,
  },
  textTag: {
    color: 'black',
    fontSize: 18,
    marginVertical: -5,

    alignSelf: 'center',
    textAlign: 'center',
    fontFamily: fonts.primary.bold,
  },
  textTag2: {
    color: 'black',
    fontSize:
      Platform.OS == 'android'
        ? Dimensions.get('window').width > 360
          ? 14
          : 12
        : Dimensions.get('window').width * PixelRatio.get() > 750
        ? 14
        : 12,
    alignSelf: 'center',
    textAlign: 'center',

    fontFamily: fonts.primary.bold,
  },
  textTag3: {
    color: 'black',
    fontSize:
      Platform.OS == 'android'
        ? Dimensions.get('window').width > 360
          ? 14
          : 12
        : Dimensions.get('window').width * PixelRatio.get() > 750
        ? 14
        : 12,
    alignSelf: 'center',
    textAlign: 'center',
  },
  cardItem: {
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: '#f9f9f9',
  },
  cardItem2: {
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: '#f9f9f9',
    paddingTop: 0,
    paddingBottom: 3,
  },
  textDetail: {
    width: width * 0.65,
    lineHeight: 20,
    letterSpacing: -0.5,
    fontSize:
      Platform.OS == 'android'
        ? Dimensions.get('window').width > 360
          ? 14
          : 12
        : Dimensions.get('window').width * PixelRatio.get() > 750
        ? 14
        : 12,

    color: '#646464',
    flexDirection: 'row',
    flexWrap: 'wrap',
    fontFamily: fonts.primary.normal,
  },
  editIcon: {
    width: 55,
    alignSelf: 'flex-end',
    marginTop: -100,
    marginRight: 0,
    marginBottom: 55,
    backgroundColor: 'transparent',
  },
  headerText: {
    fontSize:
      Platform.OS == 'android'
        ? Dimensions.get('window').width > 360
          ? 14
          : 12
        : Dimensions.get('window').width * PixelRatio.get() > 750
        ? 14
        : 12,
    alignSelf: 'center',
    textAlign: 'center',

    fontFamily: fonts.primary.normal,
  },
  lineStyle: {
    width: '70%',
    height: Platform.OS == 'android' ? 7 : 9,
    backgroundColor: '#A6AAB4',
  },
  headerText2: {
    alignSelf: 'center',
    textAlign: 'center',
    fontSize:
      Platform.OS == 'android'
        ? Dimensions.get('window').width > 360
          ? 20
          : 18
        : Dimensions.get('window').width * PixelRatio.get() > 750
        ? 20
        : 18,
    color: '#002f5f',

    fontFamily: fonts.primary.bold,
  },
  iconStyle2: {
    color: '#002f5f',
    fontSize:
      Platform.OS == 'android'
        ? Dimensions.get('window').width > 360
          ? 20
          : 18
        : Dimensions.get('window').width * PixelRatio.get() > 750
        ? 20
        : 18,
    marginHorizontal: 5,
    width: width * 0.09,
  },
  iconStyle3: {
    color: '#002f5f',
    fontSize:
      Platform.OS == 'android'
        ? Dimensions.get('window').width > 360
          ? 20
          : 18
        : Dimensions.get('window').width * PixelRatio.get() > 750
        ? 20
        : 18,
    marginHorizontal: 5,
    alignSelf: 'flex-start',
    width: width * 0.09,
  },
  itemStyle: {
    width: width * 0.8,
    height: 35,
  },
  itemStyleEnd: {
    paddingTop: 6,
    width: 0,
  },
  textHeader: {
    textAlign: 'left',
    fontSize: 14,
    color: 'black',
    fontFamily: fonts.primary.bold,
  },
  textUp: {
    fontSize: 14,
    color: '#002f5f',
    fontFamily: fonts.primary.bold,
    paddingLeft: '3%',
    paddingVertical: '3%',
  },
  textBelow: {
    fontSize: 12,
    color: 'black',
    marginTop: -25,
  },
  imagePromo: {
    height: 140,
    width: 0,
    borderRadius: 10,
    flex: 1,
  },
  iconStyle: {
    fontSize: 14,
    paddingRight: 10,
    marginTop: -25,
  },
  bottomMenu: {
    marginTop: -6,
    marginLeft: -10,
    borderBottomWidth: 2,
    borderColor: '#eeeeee',
    paddingHorizontal: 5,
    width: '108%',
  },

  bottomMenuTop: {
    marginTop: 40,
    marginLeft: -10,
    borderBottomWidth: 2,
    borderColor: '#eeeeee',
    paddingHorizontal: 5,
    width: '108%',
  },

  textMenu: {
    color: '#002f5f',
    fontSize:
      Platform.OS == 'android'
        ? Dimensions.get('window').width > 360
          ? 14
          : 12
        : Dimensions.get('window').width * PixelRatio.get() > 750
        ? 14
        : 12,
    lineHeight: 28,

    fontFamily: fonts.primary.bold,
  },

  popup: {
    width: width * 0.8,
  },

  btnSubmit: {
    backgroundColor: '#002F5F',
    width: '100%',
    height: 40,
    marginTop: 10,
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
});
