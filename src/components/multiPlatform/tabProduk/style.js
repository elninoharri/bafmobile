import {StyleSheet, Dimensions, PixelRatio} from 'react-native';
const {width: WIDTH} = Dimensions.get('window');

export const styles = StyleSheet.create({
  page: {
    flex: 1
  },
  productImg: {
    width: WIDTH,
    height: WIDTH * 0.44
  },
  categoryButtonWrapper: {
    backgroundColor: 'white',
    height: 115,
    paddingVertical:10,
    marginLeft: 8
  },
  imageWrapper: {
    width: '100%',
  },

  toast: {
    marginTop:
      Platform.OS != 'android'
        ? Dimensions.get('window').width * PixelRatio.get() > 750
          ? 44
          : 20
        : 0,
    backgroundColor: '#FFE6E9',
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'red',
    justifyContent: 'center',
    paddingLeft: 50,
    height: 50,
    borderRadius: 0,
  },
});
