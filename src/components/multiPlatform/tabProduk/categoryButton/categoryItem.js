import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import styles from './style';

const categoryItem = ({image, active, label, onPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={0}
      style={styles.button(active === label ? true : false)}
      onPress={() => onPress(label)}>
      <Image resizeMode="contain" style={styles.buttonLogo} source={image} />

      <View style={styles.textWrapper}>
        <Text style={styles.label}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default categoryItem;
