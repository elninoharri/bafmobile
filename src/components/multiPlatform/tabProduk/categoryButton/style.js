import {StyleSheet, Platform, StatusBar, Dimensions} from 'react-native';
import {BAF_COLOR_BLUE} from '../../../../utils/constant';
import {fonts} from '../../../../utils/fonts';

const styles = StyleSheet.create({
  button: (active) => ({
    height: 90,
    width: 90,
    borderRadius: 10,
    paddingTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: active ? BAF_COLOR_BLUE : 'white',
    borderWidth: 1,
    marginRight: 10,
    shadowColor: '#000',
    shadowOffset: {height: 1, width: 1}, // IOS
    shadowOpacity: 0.3,
    shadowRadius: 3,
    backgroundColor: '#fff',
    elevation: 5, // Android
  }),
  buttonLogo: {
    width: '50%',
    height: 50,
  },
  label: {
    maxWidth: '80%',
    marginBottom: 5,
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.primary.normal,
    color: BAF_COLOR_BLUE,
  },
  textWrapper: {
    justifyContent: 'center',
    height: 40,
  },
});

export default styles;
