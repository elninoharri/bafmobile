import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, ScrollView} from 'react-native';
import styles from './style';
import {Content} from 'native-base';
import CategoryItem from './categoryItem';
import ILmotor from '../../../../../assets/img/home/example-motoryamaha.png';
import ILmobil from '../../../../../assets/img/home/example-mobilbaru.png';
import ILmultiproduk from '../../../../../assets/img/home/example-multiproduk.png';
import ILpertanian from '../../../../../assets/img/home/example-pertanian.png';
import ILdanaSyariah from '../../../../../assets/img/home/example-danasyariah.png';
import ILmotorBekas from '../../../../../assets/img/home/example-motorbekas.png';

export default class CategoryButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: null,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
    };
  }

  render() {
    const {pressButton, active} = this.props;
    return (
      <Content
        showsHorizontalScrollIndicator={false}
        horizontal
        style={styles.page}>
        <CategoryItem
          image={ILmotor}
          label="Motor"
          active={active}
          onPress={pressButton}
        />
        <CategoryItem
          image={ILmobil}
          label="Mobil"
          active={active}
          onPress={pressButton}
        />
        <CategoryItem
          image={ILmultiproduk}
          label="Multi Produk"
          active={active}
          onPress={pressButton}
        />
        <CategoryItem
          image={ILdanaSyariah}
          label="Dana Syariah"
          active={active}
          onPress={pressButton}
        />
        <CategoryItem
          image={ILpertanian}
          label="Pertanian"
          active={active}
          onPress={pressButton}
        />
        <CategoryItem
          image={ILmotorBekas}
          label="Motor Bekas"
          active={active}
          onPress={pressButton}
        />
      </Content>
    );
  }
}
