import React, {Component} from 'react';
import {
  TouchableOpacity,
  Text,
  Alert,
  View,
  Image,
  BackHandler,
  Platform,
  Keyboard,
  Dimensions,
  PixelRatio,
} from 'react-native';
import {
  Container,
  Form,
  Label,
  Input,
  Item,
  Card,
  Left,
  CardItem,
  Right,
  Body,
  Row,
  Icon,
  Spinner,
  Content,
} from 'native-base';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import Toast, {DURATION} from 'react-native-easy-toast';

import ListCard from '../../multiPlatform/listCard';
import CategoryButton from './categoryButton';
import {styles} from './style';
import ILMotor from '../../../../assets/img/home/product/product-tabnmc.jpg';
import ILMobil from '../../../../assets/img/home/product/product-tabmobilbaru.jpg';
import ILMotorBekas from '../../../../assets/img/home/product/product-tabmotorbekas.jpg';
import ILPertanian from '../../../../assets/img/home/product/product-tabpertanian.jpg';
import ILMultiProduct from '../../../../assets/img/home/product/product-tabmultiproduct.jpg';
import ILDanaSyariah from '../../../../assets/img/home/product/product-tabdanasyariah.jpg';
import Gap from './categoryButton/gap';
import data from './products.json';

class   TabProduk extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Produk',
      active: 'Motor',
    };
  }

  pressButton = (product) => {
    this.setState({active: product});
  };

  componentDidMount() {
    // console.log('ini data nya ' + data.motor[0]);
  }

  goBack = async () => {
    this.props.navigation.goBack();
    this.props.resetForm('Product');
  };

  imageSource = () => {
    switch (this.state.active) {
      case 'Motor':
        return ILMotor;
      case 'Mobil':
        return ILMobil;
      case 'Multi Produk':
        return ILMultiProduct;
      case 'Dana Syariah':
        return ILDanaSyariah;
      case 'Pertanian':
        return ILPertanian;
      case 'Motor Bekas':
        return ILMotorBekas;
      default:
        return ILMotor;
    }
  };

  dataSource = () => {
    switch (this.state.active) {
      case 'Motor':
        return data.motor;
      case 'Mobil':
        return data.mobil;
      case 'Multi Produk':
        return data.multiproduk;
      case 'Dana Syariah':
        return data.danasyariah;
      case 'Pertanian':
        return data.pertanian;
      case 'Motor Bekas':
        return data.motorbekas;
      default:
        return data.motor;
    }
  };

  render() {
    const list = this.dataSource().map((result, key) => {
      if (result.lenght !== 0) {
        return (
          <ListCard
            title={result.title}
            text={result.subtitle}
            type={result.type}
            content={result.content}
            iconRight="arrow-forward"
          />
        );
      } else {
        <View>
          <Text>data not found</Text>
        </View>;
      }
    });

    return (
      <Container style={styles.page}>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid title={this.state.screenTitle} />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} />
        )}
        <View style={styles.categoryButtonWrapper}>
          <CategoryButton
            pressButton={this.pressButton}
            active={this.state.active}
          />
        </View>
        <Content style={{backgroundColor: 'white'}}>
          <View style={{height: 10, backgroundColor: '#eee'}}></View>
          <View style={styles.imageWrapper}>
            <Image
              resizeMode="stretch"
              style={styles.productImg}
              source={this.imageSource()}
            />
          </View>

          <View style={{marginTop: 1}}>{list}</View>
          <Gap height={10} />

          <Toast
            ref="toast"
            style={styles.toast}
            position="top"
            positionValue={0}
            fadeInDuration={2000}
            fadeOutDuration={1000}
            opacity={0.9}
          />

          <Content disableKBDismissScroll={false}></Content>
        </Content>
      </Container>
    );
  }
}

export default TabProduk;
