import {StyleSheet, Dimensions} from 'react-native';
// import {BAF_COLOR_BLUE} from '../../utils/constant';

export const styles = StyleSheet.create({
  containerContent: {
    paddingHorizontal: '3%',
  },
  textHeader: {
    textAlign: 'left',
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold',
  },
  textUp: {
    fontSize: 14,
    color: '#002f5f',
    fontWeight: 'bold',
    paddingLeft: '3%',
    paddingVertical: '3%',
  },
  textBelow: {
    fontSize: 12,
    color: 'black',
    marginTop: -25,
  },
  imagePromo: {
    height: 140,
    width: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    flex: 1,
  },
  iconStyle: {
    fontSize: 14,
    paddingRight: 10,
    marginTop: -25,
  },
  cardStyle: {
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  cardImage: {
    borderRadius: 10,
    backgroundColor: 'white',
  },
  carItemImage: {
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
});
