import {StyleSheet, Dimensions, PixelRatio} from 'react-native';
const {width: WIDTH} = Dimensions.get('window');
import {fonts} from '../../../../utils/fonts';

export const styles = StyleSheet.create({
  textUp: {
    color: 'black',
    fontFamily: fonts.primary.bold,
    fontSize: 15,
    paddingRight: 20,
  },
  textParagraph: {
    lineHeight: 18,
    marginBottom: 10,
    textAlign: 'justify',
    color: 'rgba(0, 0, 0, 0.8)',
    width: WIDTH * 0.8,
    fontFamily: fonts.primary.normal,
    fontSize: 14,
  },

  subContent: {
    width: 20,

    alignItems: 'center',

    marginRight: 5,
  },

  row: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginVertical: 1,
  },

  pointText: {
    width: WIDTH * 0.71,
    color: 'rgba(0, 0, 0, 0.8)',
    fontFamily: fonts.primary.normal,
  },

  pointTextSub: {
    width: WIDTH * 0.61,
    color: 'rgba(0, 0, 0, 0.8)',
    fontFamily: fonts.primary.normal,
  },

  textDefault: {
    color: 'rgba(0, 0, 0, 0.8)',
    textAlign: 'justify',
    fontFamily: fonts.primary.normal,
  },

  bodyCard: {
    marginRight: '-60%',
  },
  // scrolable modal
  view: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  scrollableModal: {
    height: 400,
  },

  scrollableModalContent1: {
    // marginTop: 345,
    height: 400,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },

  modalHeight: {
    height: 760,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },

  titleModal: {
    color: 'black',
    fontSize: 18,
    fontFamily: fonts.primary.bold,
    marginTop: 0,
    marginBottom: 15,
  },
  subtitleModal: {
    fontSize: 14,
    fontFamily: fonts.primary.bold,
    marginBottom: 10,
    color: 'black',
  },
});
