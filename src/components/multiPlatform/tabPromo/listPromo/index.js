import React, {Component} from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions,
} from 'react-native';
import {Left, CardItem, Right, Body, Row, Icon} from 'native-base';

import {styles} from './style';
import Modal from 'react-native-modal';
import ILrectangle from '../../../../../assets/img/home/product/icon-rectangle.png';
import ICdeskripsi from '../../../../../assets/img/home/product/icon-deskripsi.png';
import ICpersyaratan from '../../../../../assets/img/home/product/icon-persyaratan.png';
import ICsyarat from '../../../../../assets/img/home/product/icon-syarat.png';

import HTML from 'react-native-render-html';
import {fonts} from '../../../../utils/fonts';

const HTMLexample =
  '<ol><li>Promo berlaku hingga 31 December 2020.</li><li>Promo berlaku untuk pengajuan khusus pembiayaan mobil baru dengan pengajuan hanya melalui aplikasi BAF Mobile.</li><li>Promo berupa cashback voucher belanja hingga Rp 5 juta.</li><li>Syarat dan ketentuan dapat berubah sewaktu-waktu tanpa pemberitahuan sebelumnya. &nbsp;</li></ol>';

class ListPromo extends Component {
  constructor(props) {
    super();
    this.state = {
      visible: '',
      showModal: false,
      scrollOffset: null,
      title: '',
      HTML: '',
    };
    this.scrollViewRef = React.createRef();
  }

  componentDidMount = async () => {
    const title = this.props.title.trim();
    this.setState({title: title});
    const html =
      `<h><strong>${this.props.title}:</strong></h>` + this.props.content;
    this.setState({HTML: html});
  };

  componentDidUpdate = async () => {};

  closeModal = () => {
    this.setState({showModal: false});
  };

  pressHandle = () => {
    this.setState({showModal: true});
  };

  handleOnScroll(event) {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    });
  }

  handleScrollTo = (p) => {
    if (this.scrollViewRef.current) {
      this.scrollViewRef.current.scrollTo(p);
    }
  };

  renderModal = () => {
    return (
      <Modal
        testID={'modal'}
        isVisible={this.state.showModal}
        onSwipeComplete={() => this.closeModal()}
        onBackdropPress={() => this.closeModal()}
        swipeDirection={['down']}
        scrollTo={this.handleScrollTo}
        scrollOffset={this.state.scrollOffset}
        scrollOffsetMax={400 - 300}
        propagateSwipe={true}
        style={styles.modal}>
        <View style={{height: '60%'}}>
          <ScrollView
            ref={this.scrollViewRef}
            onScroll={this.handleOnScroll.bind(this)}
            scrollEventThrottle={16}>
            <View style={styles.modalHeight}>
              {/* icon modal */}
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                }}>
                <Image
                  style={{marginTop: 20, marginBottom: 17}}
                  source={ILrectangle}
                />
                {/* isi content */}
                <View>
                  <View
                    style={{
                      // alignItems: 'flex-start',
                      marginLeft: -5,
                      marginRight: -8,
                    }}>
                    {/* {this.renderTextModal(this.props.content)} */}
                    <Row>
                      {console.log(this.props.content)}
                      <View
                        style={{
                          width: '100%',
                          paddingHorizontal: 30,
                        }}>
                        <HTML
                          html={this.state.HTML}
                          imagesMaxWidth={Dimensions.get('screen').width}
                          tagsStyles={{
                            p: {
                              color: 'black',
                              textAlign: 'justify',
                              paddingHorizontal: 20,
                              fontFamily: fonts.primary.normal,
                              lineHeight: 20,
                            },
                            li: {
                              color: 'rgba(0, 0, 0, 0.8)',
                              textAlign: 'justify',
                              paddingRight: 30,
                              fontFamily: fonts.primary.normal,
                            },
                            h: {
                              color: 'black',
                              textAlign: 'justify',
                              paddingHorizontal: 16,
                              fontFamily: fonts.primary.normal,
                              lineHeight: 30,
                              marginBottom: 10,
                              fontSize: 18,
                            },
                          }}
                          listsPrefixesRenderers={{
                            ol: (
                              htmlAttribs,
                              children,
                              convertedCSSStyles,
                              passProps,
                            ) => {
                              const {
                                allowFontScaling,
                                baseFontStyle,
                                index,
                              } = passProps;
                              const baseFontSize = baseFontStyle.fontSize || 14;

                              const bulletNumber = htmlAttribs.start
                                ? htmlAttribs.start
                                : index + 1;
                              return (
                                <View style={styles.subContent}>
                                  <Text style={[styles.textDefault]}>
                                    {bulletNumber + '. '}
                                  </Text>
                                </View>
                              );
                            },
                          }}
                        />
                      </View>
                    </Row>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </Modal>
    );
  };

  renderArray = (result) => {
    let number = 0;
    return result.map((res, key) => {
      number++;
      if (res.lenght !== 0) {
        return (
          <View style={{marginLeft: 10}}>
            <View style={styles.row}>
              <View style={{width: 20}}>
                <Text style={styles.textDefault}>{number + '. '}</Text>
              </View>
              <View>
                <Text style={styles.pointText}>{res.trim()}</Text>
              </View>
            </View>
          </View>
        );
      }
    });
  };

  renderSubArray = (result) => {
    let number = 0;
    return result.map((res, key) => {
      number++;
      if (res.lenght !== 0 && number < 3) {
        return (
          <View style={styles.row}>
            <View style={{width: 20}}>
              <Text numberOfLines={2} style={styles.pointTextSub}>
                {number + '. '}
              </Text>
            </View>
            <View>
              <Text numberOfLines={2} style={styles.pointTextSub}>
                {res.trim()}
              </Text>
            </View>
          </View>
        );
      }
    });
  };

  renderSubPage = (result) => {
    {
      if (typeof result === 'string') {
        return (
          <View>
            <Text numberOfLines={3} style={styles.textDefault}>
              {result.trim()}
            </Text>
          </View>
        );
      }

      if (typeof result === 'object') {
        {
          return <>{this.renderSubArray(result)}</>;
        }
      }
    }
  };

  renderTextModal = (result) => {
    {
      if (typeof result === 'string') {
        return (
          <View style={{marginBottom: 15}}>
            <Text style={styles.titleModal}>{this.props.title}</Text>
            <Text style={styles.textParagraph}>{result.trim()}</Text>
          </View>
        );
      }

      if (typeof result === 'object') {
        {
          return (
            <View style={{marginBottom: 15}}>
              <Text style={styles.titleModal}>{this.props.title}</Text>
              {this.renderArray(result)}
            </View>
          );
        }
      }
    }
  };

  sourceImage = () => {
    const {title} = this.state;

    if (title.includes('Cara')) {
      return ICsyarat;
    } else if (title.includes('Syarat')) {
      return ICpersyaratan;
    } else if (title.includes('Deskripsi')) {
      return ICdeskripsi;
    } else {
      return ICsyarat;
    }
  };

  render() {
    const {title} = this.state;
    console.log(title);

    return (
      <View>
        <CardItem button onPress={() => this.pressHandle()}>
          <Left>
            <Row>
              <Image source={this.sourceImage()} style={styles.imageBelow} />
              <Body style={styles.bodyCard}>
                <Text style={styles.textUp}>{this.props.title}</Text>
                {/* {this.renderSubPage(this.props.content)} */}
              </Body>
            </Row>
          </Left>
          <Right>
            <Icon size={25} name="arrow-forward" style={{marginRight: '5%'}} />
          </Right>
        </CardItem>
        {this.renderModal()}
      </View>
    );
  }
}

export default ListPromo;
