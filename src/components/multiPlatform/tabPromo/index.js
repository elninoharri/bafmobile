import React, {Component} from 'react';
import {
  Text,
  Image,
  BackHandler,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import {Toast} from 'native-base';
import {Container, Left, Icon, CardItem, Card, Content} from 'native-base';
import SubHeaderAndroid from '../../android/subHeaderBlue';
import SubHeaderIos from '../../ios/subHeaderBlue';
import {styles} from './style';
import {connect} from 'react-redux';

import {fonts} from '../../../utils/fonts';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import moment from 'moment';

class TabPromo extends Component {
  constructor(props) {
    super(props);
    // this.goNavScreen = this.goNavScreen.bind(this);
    this.state = {
      isConnected: false,
    };
  }

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  componentDidMount = async () => {
    this.props.reloadScreen();
    console.log(this.props.detailPromo);
  };

  render() {
    const {detailPromo, goPromoDetail, detailUser} = this.props;

    const listPromo = detailPromo.map((result, key) => {
      if (result.length !== 0) {
        return (
          <TouchableOpacity
            onPress={() =>
              goPromoDetail(
                result.RefBannerImg,
                result.ContentHName,
                result.StartDtTo,
                result.ContentHID,
                result.RefBannerCode,
              )
            }>
            <Card style={styles.cardImage}>
              <CardItem cardBody style={styles.cardItemImage}>
                <Image
                  source={{uri: result.RefBannerImg}}
                  style={styles.imagePromo}
                />
              </CardItem>

              <CardItem header>
                <Text style={styles.textHeader}>{result.ContentHName} </Text>
              </CardItem>

              <CardItem style={styles.cardStyle}>
                <Left>
                  <Icon style={styles.iconStyle} name="calendar" />
                  <Text style={styles.textBelow}>
                    Berlaku hingga{' '}
                    {moment(result.StartDtTo).format('DD MMM YYYY')}
                  </Text>
                </Left>
              </CardItem>
            </Card>
          </TouchableOpacity>
        );
      } else {
        return (
          <View>
            <Text>data not found</Text>
          </View>
        );
      }
    });

    return (
      <Container style={{flex: 1}}>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title="Promo"
            nameIcon="filter"
            goNavScreen={this.props.goNavScreen}
          />
        ) : (
          <SubHeaderIos
            title="Promo"
            nameIcon="filter"
            goNavScreen={this.props.goNavScreen}
          />
        )}

        {detailPromo.length !== 0 ? (
          <Content
            refreshControl={
              <RefreshControl
                refreshing={false}
                onRefresh={this.props.reloadScreen}
              />
            }
            disableKBDismissScroll={false}
            style={{paddingHorizontal: '3%'}}>
            <Text style={styles.textUp}>Semua Promo</Text>
            {listPromo}
          </Content>
        ) : (
          <Content style={{backgroundColor: 'white', paddingTop: '30%'}}>
            <Icon
              type="FontAwesome"
              name="search"
              style={{
                color: 'grey',
                marginBottom: 10,
                fontSize: 40,
                alignSelf: 'center',
              }}
            />
            <Text
              style={{
                fontSize: 12,
                color: 'grey',
                alignSelf: 'center',
                fontFamily: fonts.primary.normal,
              }}>
              Promo tidak ditemukan
            </Text>
          </Content>
        )}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function matchDispatchToProps(dispatch) {
  return {};
}

export default connect(mapStateToProps, matchDispatchToProps)(TabPromo);
