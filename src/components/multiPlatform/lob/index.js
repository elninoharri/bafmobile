import React from 'react';
import {View, TouchableOpacity, Image} from 'react-native';
import Animated from 'react-native-reanimated';
import mobil from '../../../../assets/img/home/example-mobilbaru.png';
import motor from '../../../../assets/img/home/example-motoryamaha.png';
import syariah from '../../../../assets/img/home/example-danasyariah.png';
import mp from '../../../../assets/img/home/example-multiproduk.png';

export default function MyTabBar({state, descriptors, navigation, position}) {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 44,
        backgroundColor: '#F5F5F5',
        height: 68,
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        const inputRange = state.routes.map((_, i) => i);
        const opacity = Animated.interpolate(position, {
          inputRange,
          outputRange: inputRange.map((i) => (i === index ? 1 : 0)),
        });

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{
              backgroundColor: isFocused ? 'rgba(0, 0, 0, 0.1)' : '#FFFFFF',
              width: 50,
              height: 45,
              borderColor: '#002F5F',
              borderWidth: 0.5,
              borderRadius: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              resizeMode="contain"
              style={{width: '90%', height: undefined, aspectRatio: 1}}
              source={
                label == 'motor'
                  ? motor
                  : label == 'mobil'
                  ? mobil
                  : label == 'MP'
                  ? mp
                  : label == 'Syariah'
                  ? syariah
                  : null
              }
            />
          </TouchableOpacity>
        );
      })}
    </View>
  );
}
