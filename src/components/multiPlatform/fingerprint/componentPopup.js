import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Alert,
  Image,
  Text,
  TouchableOpacity,
  View,
  ViewPropTypes,
  Platform,
} from 'react-native';

import FingerprintScanner from 'react-native-fingerprint-scanner';
import styles from './popupStyle';
import ShakingText from './ShakingTextComponent';

class BiometricPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessageLegacy: undefined,
      biometricLegacy: undefined,
      errorMessageAuthenticate: undefined,


    };

    this.description = null;
  }

  componentDidMount() {
    if (Platform == 'android' && this.requiresLegacyAuthentication()) {
      this.authLegacy();
    } else {
      this.authCurrent();
    }
  }

  componentWillUnmount = () => {
    FingerprintScanner.release();
  };

  requiresLegacyAuthentication() {
    return Platform.Version < 23;
  }

  authCurrent() {
    FingerprintScanner.authenticate({
      subTitle: 'Sentuh sensor sidik jari untuk masuk ke BAF mobile',
      title: 'Masuk dengan sensor sidik jari',
    })
      .then(() => {
        this.props.onAuthenticate();
      })
      .catch((error) => {
        if (error == "UserCancel: Authentication was canceled by the user - e.g. the user tapped Cancel in the dialog.") {
          this.props.handlePopupDismissed();
        } else {
          this.props.handleErrorFinger(error);
          this.props.handlePopupDismissed();
        }
      });
  }

  authLegacy() {
    FingerprintScanner.authenticate({
      onAttempt: this.handleAuthenticationAttemptedLegacy,
    })
      .then(() => {
        this.props.onAuthenticate();
      })
      .catch((error) => {
        this.props.handlePopupDismissed();
        this.setState({
          errorMessageLegacy: error.message,
          biometricLegacy: error.biometric,
        });
        this.description.shake();
      });
  }


  handleAuthenticationAttemptedLegacy = (error) => {
    this.setState({ errorMessageLegacy: error.message });
    this.description.shake();
  };

  renderLegacy() {
    const { errorMessageLegacy, biometricLegacy } = this.state;
    const { style, handlePopupDismissedLegacy } = this.props;
    ('../../../../assets/img/fingerprint.png');
    return (
      <View style={styles.container}>
        <View style={[styles.contentContainer, style]}>

          <Image
            style={styles.logo}
            source={require('../../../../assets/img/fingerprint.png')}
          />

          <Text style={styles.heading}>Biometric{'\n'}Authentication</Text>
          <ShakingText
            ref={(instance) => {
              this.description = instance;
            }}
            style={styles.description(!!errorMessageLegacy)}>
            {errorMessageLegacy ||
              `Scan your ${biometricLegacy} on the\ndevice scanner to continue`}
          </ShakingText>

          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={handlePopupDismissedLegacy}>
            <Text style={styles.buttonText}>Cancel</Text>
          </TouchableOpacity>
        </View>

      </View>
    );
  }

  render = () => {
    if (Platform == 'android' && this.requiresLegacyAuthentication()) {
      return this.renderLegacy();
    }
    return null;
  };
}

BiometricPopup.propTypes = {
  description: PropTypes.string,
  onAuthenticate: PropTypes.func.isRequired,
  handlePopupDismissedLegacy: PropTypes.func,
  style: ViewPropTypes.style,
};

export default BiometricPopup;
