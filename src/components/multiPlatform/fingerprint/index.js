import React, { Component } from 'react';
import {
  Image,
  Text,
  TouchableOpacity,
  View,
  AppState,
  Alert,
} from 'react-native';
import FingerprintScanner from 'react-native-fingerprint-scanner';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';

import styles from './style';
import FingerprintPopup from './componentPopup';

class FinterPrintContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: undefined,
      biometric: undefined,
      popupShowed: false,
      fingerDetected: true,

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
    };
  }

  handleFingerprintShowed = () => {
    // this.setState({
    //   alertShowed: true,
    //   alertMessage: 'Fitur ini sedang dalam tahap pengembangan.',
    //   alertTitle: 'Informasi',
    //   alertType: 'info',
    //   alertDoneText: 'OK'
    // })

    if (!this.state.fingerDetected && this.state.errorMessage) {
      this.setState({
        alertShowed: true,
        alertMessage: 'Device Anda tidak support fitur ini.',
        alertTitle: 'Informasi',
        alertType: 'info',
        alertDoneText: 'OK',
      });
    }
    if (!this.props.fingerPrintExist) {
      this.setState({
        alertShowed: true,
        alertMessage: 'Aktifkan fitur fingerprint pada akun anda.',
        alertTitle: 'Informasi',
        alertType: 'info',
        alertDoneText: 'OK',
      });
    } else {
      this.setState({ popupShowed: true });
    }


  };

  handleFingerprintDismissed = () => {
    this.setState({ popupShowed: false });
  };

  handleErrorLoginFinger = (error) => {
    if (
      error ==
      'UserFallback: Authentication was canceled because the user tapped the fallback button (Enter Password).'
    ) {
    } else {
      this.setState({
        alertShowed: true,
        alertMessage:
          'Tunggu fingerPrint 30 detik atau masukkan username dan password',
        alertTitle: 'Informasi',
        alertType: 'info',
        alertDoneText: 'OK',
      });
    }
  };

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
    // Get initial fingerprint enrolled
    this.detectFingerprintAvailable();
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  detectFingerprintAvailable = () => {
    FingerprintScanner.isSensorAvailable().catch((error) =>
      this.setState({
        errorMessage: error.message,
        biometric: error.biometric,
        fingerDetected: false,
      }),
    );
  };

  handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState &&
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      FingerprintScanner.release();
      this.detectFingerprintAvailable();
    }
    this.setState({ appState: nextAppState });
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handlePositiveButtonAlert = () => {
    this.setState({ alertShowed: false });
  };

  handleNegativeButtonAlert = () => {
    this.setState({ alertShowed: false });
  };

  render() {
    const { errorMessage, biometric, popupShowed } = this.state;

    return (
      <View>
        <TouchableOpacity
          style={styles.fingerprint}
          onPress={this.handleFingerprintShowed}>
          <Image
            testID="FingerprintImage"
            source={require('../../../../assets/img/fingerprint.png')}
            style={styles.fingerLogo}
            resizeMode="contain"
          />
        </TouchableOpacity>

        {/* {errorMessage && (
          <Text style={styles.errorMessage}>
            {errorMessage} {biometric}
          </Text>
        )} */}

        {popupShowed && (
          <FingerprintPopup
            style={styles.popup}
            handlePopupDismissed={this.handleFingerprintDismissed}
            onAuthenticate={this.props.onAuthenticate}
            handleErrorFinger={this.handleErrorLoginFinger}

          />
        )}

        {this.showAlert()}
      </View>
    );
  }
}

export default FinterPrintContainer;
