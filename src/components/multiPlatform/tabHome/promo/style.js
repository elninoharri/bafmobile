import {StyleSheet, Platform, StatusBar} from 'react-native';
import {BAF_COLOR_BLUE, BAF_COLOR_YELLOW} from '../../../../utils/constant';

export default StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: '5%',
  },
  promoBanner: {
    width: '100%',
    height: '100%',
  },
  customDot: {
    width: 20,
    height: 10,
    backgroundColor: BAF_COLOR_BLUE,
    borderRadius: 10,
    marginHorizontal: 1,
  },
  inactiveCustomDot: {
    width: 10,
    height: 10,
    backgroundColor: 'rgba(0,0,0,0.1)',
    borderRadius: 10,
    marginHorizontal: 1,
  },
  separator: {
    width: 5,
    height: 17,
    backgroundColor: BAF_COLOR_YELLOW,
    borderRadius: 20,
  },
  promoTitleContainer: {
    flex: 1,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  promoText: {
    fontSize: 14,
    fontFamily: 'arial',
    color: BAF_COLOR_BLUE,
    fontWeight: 'bold',
  },
  showAllText: {
    fontSize: 11,
    fontFamily: 'arial',
    color: BAF_COLOR_BLUE,
    fontWeight: 'bold',
  },
  leftPromoTitleContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 5,
  },
  middlePromoTitleContent: {
    flex: 7,
    justifyContent: 'center',
  },
  rightPromoTitleContent: {
    flex: 7,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 20,
  },
});
