import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  BackHandler,
  Dimensions,
  Image,
  SafeAreaView,
  PixelRatio,
  Platform,
} from 'react-native';
import {Spinner, Container, View, Icon, Content} from 'native-base';
import Carousel from 'react-native-snap-carousel';
import {scrollInterpolator, animatedStyles} from './utils/animations';
import NetInfo from '@react-native-community/netinfo';
import styles from './style';
import CustomAlertComponent from '../../../multiPlatform/customAlert/CustomAlertComponent';

const SLIDER_WIDTH = Dimensions.get('window').width;

const ITEM_WIDTH =
  Dimensions.get('window').width * PixelRatio.get() > 750
    ? Math.round(SLIDER_WIDTH * 0.8)
    : Math.round(SLIDER_WIDTH * 0.88);

const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 0.48);

class Promo extends Component {
  constructor(props) {
    super(props);
    this._renderItem = this._renderItem.bind(this);
    this.state = {
      index: 0,
      isConnected: true,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
    };
  }

  customDot = (data, index) => {
    return (
      <View>
        {this.state.index == index ? (
          <View style={styles.customDot} />
        ) : (
          <View style={styles.inactiveCustomDot} />
        )}
      </View>
    );
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  promoPress = () => {
    NetInfo.fetch().then((state) => {
      if (!state.isConnected) {
        this.props.goNoConnection();
      } else {
        this.setState({
          alertShowed: true,
          alertMessage: 'Fitur ini sedang dalam tahap pengembangan.',
          alertTitle: 'Informasi',
          alertType: 'info',
          alertDoneText: 'OK',
        });
      }
    });
  };

  _renderItem({item}) {
    return (
      <TouchableOpacity
        style={carouselStyle.itemContainer}
        onPress={() =>
          this.props.promoPress(
            item.RefBannerImg,
            item.ContentHName,
            item.StartDtTo,
            item.ContentHID,
            item.RefBannerCode,
          )
        }>
        <Image
          source={{uri: item.RefBannerImg}}
          resizeMode={'stretch'}
          style={{height: ITEM_HEIGHT, width: '90%', borderRadius: 7}}></Image>
      </TouchableOpacity>
    );
  }

  render() {
    const {tabPromo, data, touchHandler} = this.props;
    return (
      <>
        {data.length ? (
          <View style={styles.container}>
            {this.showAlert()}
            <View style={styles.promoTitleContainer}>
              <View style={styles.leftPromoTitleContent}>
                <View style={styles.separator} />
              </View>
              <View style={styles.middlePromoTitleContent}>
                <Text style={styles.promoText}>PROMO MENARIK</Text>
              </View>
              <TouchableOpacity
                style={styles.rightPromoTitleContent}
                onPress={tabPromo}>
                <Text style={styles.showAllText}>Lihat Semua</Text>
              </TouchableOpacity>
            </View>
            <View>
              <View>
                <Carousel
                  ref={(c) => (this.carousel = c)}
                  data={data}
                  renderItem={this._renderItem}
                  sliderWidth={SLIDER_WIDTH}
                  itemWidth={ITEM_WIDTH}
                  containerCustomStyle={carouselStyle.carouselContainer}
                  inactiveSlideShift={0}
                  onSnapToItem={(index) => this.setState({index})}
                  scrollInterpolator={scrollInterpolator}
                  slideInterpolatedStyle={animatedStyles}
                  useScrollView={true}
                  onPress={(value) => touchHandler(value)}
                />
                <View style={carouselStyle.counter}>
                  {data.map((data, index) => this.customDot(data, index))}
                </View>
              </View>
            </View>
          </View>
        ) : null}
      </>
    );
  }
}

export default Promo;

const carouselStyle = StyleSheet.create({
  carouselContainer: {
    marginTop: -12,
  },
  itemContainer: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  itemLabel: {
    color: 'white',
    fontSize: 24,
  },
  counter: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
});
