import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
  BackHandler,
  Image,
  Platform,
  Dimensions,
  PixelRatio,
} from 'react-native';
import {Spinner, Container, View, Icon, Content} from 'native-base';
import styles from './style';
import CustomAlertComponent from '../../../multiPlatform/customAlert/CustomAlertComponent';
import NetInfo from '@react-native-community/netinfo';

class InstallmentsButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: null,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
    };
  }

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  pressButton = (Lob) => {
    this.props.goProduct(Lob);
  };

  render() {
    return (
      <View style={{flex: 1}}>
        {this.showAlert()}
        <View style={styles.installmentsButtonContainer}>
          <TouchableOpacity
            onPress={() => {
              this.pressButton('NMC');
            }}
            style={
              Platform.OS == 'android'
                ? Dimensions.get('window').width > 360
                  ? styles.installmentsButtonAndroidLarge
                  : styles.installmentsButtonAndroidSmall
                : Dimensions.get('window').width * PixelRatio.get() > 750
                ? styles.installmentsButtonIosLarge
                : styles.installmentsButtonIosSmall
            }>
            <Image
              source={require('../../../../../assets/img/home/example-motoryamaha.png')}
              style={styles.installmentsButtonLogo}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.pressButton('CAR');
            }}
            style={
              Platform.OS == 'android'
                ? Dimensions.get('window').width > 360
                  ? styles.installmentsButtonAndroidLarge
                  : styles.installmentsButtonAndroidSmall
                : Dimensions.get('window').width * PixelRatio.get() > 750
                ? styles.installmentsButtonIosLarge
                : styles.installmentsButtonIosSmall
            }>
            <Image
              source={require('../../../../../assets/img/home/example-mobilbaru.png')}
              style={styles.installmentsButtonLogo}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.pressButton('MP');
            }}
            style={
              Platform.OS == 'android'
                ? Dimensions.get('window').width > 360
                  ? styles.installmentsButtonAndroidLarge
                  : styles.installmentsButtonAndroidSmall
                : Dimensions.get('window').width * PixelRatio.get() > 750
                ? styles.installmentsButtonIosLarge
                : styles.installmentsButtonIosSmall
            }>
            <Image
              source={require('../../../../../assets/img/home/example-multiproduk.png')}
              style={styles.installmentsButtonLogo}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.pressButton('SYANA');
            }}
            style={
              Platform.OS == 'android'
                ? Dimensions.get('window').width > 360
                  ? styles.installmentsButtonAndroidLarge
                  : styles.installmentsButtonAndroidSmall
                : Dimensions.get('window').width * PixelRatio.get() > 750
                ? styles.installmentsButtonIosLarge
                : styles.installmentsButtonIosSmall
            }>
            <Image
              source={require('../../../../../assets/img/home/example-danasyariah.png')}
              style={styles.installmentsButtonLogo}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
        <View style={styles.installmentsTitleContainer}>
          {Platform.OS == 'android' ? (
            Dimensions.get('window').width > 360 ? (
              <Text style={styles.installmentsTitleAndroidLarge}>
                Kredit Motor Yamaha
              </Text>
            ) : (
              <Text style={styles.installmentsTitleAndroidSmall}>
                Kredit Motor Yamaha
              </Text>
            )
          ) : Dimensions.get('window').width * PixelRatio.get() > 750 ? (
            <Text style={styles.installmentsTitleIosLarge}>
              Kredit Motor Yamaha
            </Text>
          ) : (
            <Text style={styles.installmentsTitleIosSmall}>
              Kredit Motor Yamaha
            </Text>
          )}
          {Platform.OS == 'android' ? (
            Dimensions.get('window').width > 360 ? (
              <Text style={styles.installmentsTitleAndroidLarge}>
                Kredit Mobil Baru
              </Text>
            ) : (
              <Text style={styles.installmentsTitleAndroidSmall}>
                Kredit Mobil Baru
              </Text>
            )
          ) : Dimensions.get('window').width * PixelRatio.get() > 750 ? (
            <Text style={styles.installmentsTitleIosLarge}>
              Kredit Mobil Baru
            </Text>
          ) : (
            <Text style={styles.installmentsTitleIosSmall}>
              Kredit Mobil Baru
            </Text>
          )}
          {Platform.OS == 'android' ? (
            Dimensions.get('window').width > 360 ? (
              <Text style={styles.installmentsTitleAndroidLarge}>
                Multi Produk
              </Text>
            ) : (
              <Text style={styles.installmentsTitleAndroidSmall}>
                Multi Produk
              </Text>
            )
          ) : Dimensions.get('window').width * PixelRatio.get() > 750 ? (
            <Text style={styles.installmentsTitleIosLarge}>Multi Produk</Text>
          ) : (
            <Text style={styles.installmentsTitleIosSmall}>Multi Produk</Text>
          )}
          {Platform.OS == 'android' ? (
            Dimensions.get('window').width > 360 ? (
              <Text style={styles.installmentsTitleAndroidLarge}>
                Dana Syariah
              </Text>
            ) : (
              <Text style={styles.installmentsTitleAndroidSmall}>
                Dana Syariah
              </Text>
            )
          ) : Dimensions.get('window').width * PixelRatio.get() > 750 ? (
            <Text style={styles.installmentsTitleIosLarge}>Dana Syariah</Text>
          ) : (
            <Text style={styles.installmentsTitleIosSmall}>Dana Syariah</Text>
          )}
        </View>
      </View>
    );
  }
}

export default InstallmentsButton;
