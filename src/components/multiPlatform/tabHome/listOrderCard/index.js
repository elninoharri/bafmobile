import React, {Component} from 'react';
import {Text, TouchableOpacity, Image, Dimensions} from 'react-native';
import {View, Icon, Spinner, Row} from 'native-base';
import CardSlider from 'react-native-cards-slider';
import styles from './style';
import CustomAlertComponent from '../../../multiPlatform/customAlert/CustomAlertComponent';
import NetInfo from '@react-native-community/netinfo';
import {BAF_COLOR_BLUE} from '../../../../utils/constant';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {fonts} from '../../../../utils/fonts';

class ListOrderCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isConnected: false,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      isModalVisible: false,
    };
  }

  componentDidMount = async () => {};

  componentDidUpdate = async () => {};

  uncompleteCard = (navigation) => {
    const {orderCountLoading} = this.props;
    return (
      <TouchableOpacity
        onPress={navigation}
        style={
          Dimensions.get('window').width <= 360
            ? styles.installmentsCardContainer360
            : styles.installmentsCardContainer
        }>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={require('../../../../../assets/img/home/bell.png')}
            style={styles.icon}
          />
        </View>
        <View style={styles.guestTopCardContainer}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: BAF_COLOR_BLUE,
                fontSize: 12,
                width: '93%',
                fontFamily: fonts.primary.normal,
              }}>
              Cek status order Anda disini !
            </Text>
            <Icon
              style={{fontSize: 12, color: BAF_COLOR_BLUE}}
              type="FontAwesome"
              name="chevron-right"
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  completedCard = (navigation) => {
    const {countOrder, orderCountLoading} = this.props;
    return (
      <TouchableOpacity
        onPress={navigation}
        style={
          Dimensions.get('window').width <= 360
            ? styles.installmentsCardContainer360
            : styles.installmentsCardContainer
        }>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={require('../../../../../assets/img/home/bell.png')}
            style={styles.icon}
          />
        </View>
        <View style={styles.guestTopCardContainer}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: BAF_COLOR_BLUE,
                fontSize: 12,
                width: '93%',
                fontFamily: fonts.primary.normal,
              }}>
              kamu memiliki {countOrder} order yang harus di lengkapi
            </Text>
            <Icon
              style={{fontSize: 12, color: BAF_COLOR_BLUE}}
              type="FontAwesome"
              name="chevron-right"
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  loadingCard = () => {
    return (
      <TouchableOpacity
        style={
          Dimensions.get('window').width <= 360
            ? styles.installmentsCardContainer360
            : styles.installmentsCardContainer
        }>
        <Spinner color="#002f5f" />
      </TouchableOpacity>
    );
  };

  render() {
    const {
      userDetail,
      countOrder,
      goLogin,
      userData,
      goStatusOrder,
      orderCountLoading,
    } = this.props;

    return (
      <View style={{flex: 1}}>
        <CardSlider style={{marginTop: '2%'}}>
          {orderCountLoading
            ? this.loadingCard()
            : !userData
            ? this.uncompleteCard(goLogin)
            : userDetail.NIK == '' || userDetail.UserGroupID == 1
            ? this.uncompleteCard(goStatusOrder)
            : countOrder
            ? this.completedCard(goStatusOrder)
            : this.uncompleteCard(goStatusOrder)}
        </CardSlider>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    orderCountLoading: state.orderCount.loading,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(ListOrderCard);
