import {StyleSheet, Platform, Dimensions, StatusBar} from 'react-native';
import {BAF_COLOR_BLUE, BAF_COLOR_YELLOW} from '../../../../utils/constant';
import {fonts} from '../../../../utils/fonts';
const win = Dimensions.get('window');
// const { width: WIDTH } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    marginBottom: '5%',
    flex: 1,
  },

  promoBanner: {
    flex: 1,
    // resizeMode: "stretch",
    width: 120,
    borderRadius: 10,
  },
  customDot: {
    width: 20,
    height: 10,
    backgroundColor: BAF_COLOR_BLUE,
    borderRadius: 10,
    marginHorizontal: 1,
  },
  inactiveCustomDot: {
    width: 10,
    height: 10,
    backgroundColor: 'rgba(0,0,0,0.1)',
    borderRadius: 10,
    marginHorizontal: 1,
  },
  separator: {
    width: 5,
    height: 17,
    backgroundColor: BAF_COLOR_YELLOW,
    borderRadius: 20,
  },
  promoTitleContainer: {
    flex: 1,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  promoText: {
    fontSize: 14,
    fontFamily: fonts.primary.bold,
    color: BAF_COLOR_BLUE,
  },
  showAllText: {
    fontSize: 11,
    fontFamily: fonts.primary.bold,
    color: BAF_COLOR_BLUE,
  },
  leftPromoTitleContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 5,
  },
  middlePromoTitleContent: {
    flex: 15,
    justifyContent: 'center',
  },
});
