import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  BackHandler,
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import {Spinner, Container, View, Icon, Content} from 'native-base';
import styles from './style';
import CustomAlertComponent from '../../../multiPlatform/customAlert/CustomAlertComponent';
import NetInfo from '@react-native-community/netinfo';
import {connect} from 'react-redux';
const win = Dimensions.get('window');
const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.3);

class BafService extends Component {
  constructor(props) {
    super(props);
    this.pressButton = this.pressButton.bind(this);
    this.state = {
      index: 0,
      isConnected: null,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
    };
  }

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  pressButton() {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (this.state.isConnected) {
        this.setState({
          alertShowed: true,
          alertMessage: 'Fitur ini sedang dalam tahap pengembangan.',
          alertTitle: 'Informasi',
          alertType: 'info',
          alertDoneText: 'OK',
        });
      } else {
        this.props.goNoConnection();
      }
    });
  }

  renderItem = (data, index, length) => {
    return (
      <TouchableOpacity
        style={{
          marginLeft: index == 0 ? 20 : 10,
          marginRight: index == length - 1 ? 20 : 10,
          width: 120,
          height: 100,
          borderRadius: 10,
          shadowColor: '#000',
          shadowOffset: {width: 0, height: 2},
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,
          marginBottom: '2%',
        }}
        onPress={
          data.nav
            ? () => {
                this.props.nav(data.nav);
              }
            : () => {
                this.pressButton();
              }
        }>
        <View
          style={{
            flex: 2.5,
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
            alignItems: 'center',
            justifyContent: 'center',
            height: 55,
          }}>
          <Image source={data.image} style={styles.promoBanner}></Image>
        </View>

        {/* <View
          style={{
            flex: 2.5,
            backgroundColor: data.background,
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
            alignItems: 'center',
            justifyContent: 'center',
            height: 45,

          }} >
          <Image source={data.image} style={styles.promoBanner}></Image>
        </View> */}

        <View
          style={{
            flex: 1,
            backgroundColor: 'white',
            borderBottomLeftRadius: 10,
            borderBottomRightRadius: 10,
            alignItems: 'center',
            position: 'absolute',
            width: '100%',
            height: '20%',
            bottom: 0,
            justifyContent: 'center',
          }}>
          <Text
            style={{
              color: 'black',
              fontSize: 10,
              fontFamily: 'arial',
              fontWeight: 'bold',
            }}>
            {data.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const data = [
      {
        title: 'BAF CARE',
        // background: '#A9D9EC',
        image: require('../../../../../assets/img/home/services/BAF-CARE.png'),
        nav: this.props.detailUser ? 'BafCare' : 'Login',
      },
      {
        title: 'REQUEST BPKB',
        // background: '#A9D9EC',
        image: require('../../../../../assets/img/home/services/REQUEST-BPKB.png'),
        // nav: 'RequestBPKP',
        nav: this.props.detailUser ? 'RequestBPKPTC' : 'Login',
      },
      // {
      //   title: 'SURVEY',
      //   background: '#8AC2B7',
      //   image: require('../../../../../assets/img/home/services/service-survey.png'),
      // },
      // {
      //   title: 'RATE APP',
      //   background: '#0C7DBC',
      //   image: require('../../../../../assets/img/home/services/service-rate-app.png'),
      // },
      {
        title: 'KEBIJAKAN PRIVASI',
        // background: '#DBE7F5',
        image: require('../../../../../assets/img/home/services/KEBIJAKAN-PRIVASI.png'),
        nav: 'TermCondition',
      },
      {
        title: 'CARA PEMBAYARAN',
        background: '#0C7DBC',
        image: require('../../../../../assets/img/home/services/BAF-PAY.png'),
        nav: 'BafPay',
      },
      {
        title: 'BAF POIN',
        background: '#0C7DBC',
        image: require('../../../../../assets/img/home/services/BAF-POINT.png'),
        nav: 'BafPointInfo',
      },
      {
        title: 'DEALER/MITRA',
        background: '#0C7DBC',
        image: require('../../../../../assets/img/home/services/BAF-MITRA.png'),
        nav: 'BafMitra',
      },
    ];

    var length = data.length;

    return (
      <View style={styles.container}>
        {this.showAlert()}
        <View style={styles.promoTitleContainer}>
          <View style={styles.leftPromoTitleContent}>
            <View style={styles.separator} />
          </View>
          <View style={styles.middlePromoTitleContent}>
            <Text style={styles.promoText}>BAF SERVICE</Text>
          </View>
        </View>
        <View>
          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={{flex: 1, paddingBottom: '5%'}}>
            {length
              ? data.map((data, index) => this.renderItem(data, index, length))
              : null}
          </ScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(BafService);
