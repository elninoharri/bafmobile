import React, {Component} from 'react';
import {Text, Image} from 'react-native';
import {Container, Left, Icon, CardItem, Card, Content} from 'native-base';
import SubHeaderAndroid from '../../../android/subHeaderBlue';
import SubHeaderIos from '../../../ios/subHeaderBlue';
import {styles} from './style';
import {connect} from 'react-redux';

class PromoDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
     
    };
  }
  render() {
    return (
      <Container style={{flex: 1}}>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid title="Promo" nameIcon="filter"
          />
        ) : (
          <SubHeaderIos title="Promo" nameIcon="filter"/>
        )}
        <Text style={styles.textUp}>Detail Promo</Text>
        

      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    deleteCart: (event) => dispatch({type: 'DELETE_CART', value: event}),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PromoDetail);
