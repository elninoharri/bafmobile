import React, {Component} from 'react';
import {Text, TouchableOpacity, BackHandler,Image,Dimensions} from 'react-native';
import {Spinner, Container, View,Icon, Content} from 'native-base';
import styles from "./style";
import CustomAlertComponent from '../../customAlert/CustomAlertComponent';
import NetInfo from '@react-native-community/netinfo';

class ActivationCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected:null,
            alertFor: null,
            alertShowed: false,
            alertMessage: '',
            alertTitle: '',
            alertType: 'success',
            alertDoneText: '',
            alertCancelText: ''
        }
    }

    componentDidMount = async () => {
    
    };

    handlePositiveButtonAlert = () => {
        this.setState({alertShowed: false});
    }

    handleNegativeButtonAlert = () => {
        this.setState({alertShowed: false});
    }

    showAlert = () => {
        return(
            <CustomAlertComponent
                displayAlert={this.state.alertShowed}
                displayAlertIcon={true}
                alertType={this.state.alertType}
                alertTitleText={this.state.alertTitle}
                alertMessageText={this.state.alertMessage}
                displayPositiveButton={true}
                positiveButtonText={this.state.alertDoneText}
                displayNegativeButton={
                this.state.alertType === 'confirm' ? true : false
                }
                onPressNegativeButton={this.handleNegativeButtonAlert}
                negativeButtonText={this.state.alertCancelText}
                onPressPositiveButton={this.handlePositiveButtonAlert}
            />
        )
      };

    displayAlert = () => {
        NetInfo.fetch().then((state) => {
            this.setState({isConnected: state.isConnected}); // set state isConnected based on result
            if (this.state.isConnected) {
                this.setState({
                    alertShowed: true,
                    alertMessage: 'Fitur ini sedang dalam tahap pengembangan.',
                    alertTitle: 'Informasi',
                    alertType: 'info',
                    alertDoneText: 'OK',
                });
                 
            }else{
                this.props.goNoConnection();
            }
          })
    }

    render(){
        
        return(
            <TouchableOpacity style={styles.guestContainer} onPress={() => {this.displayAlert()}}>
                {this.showAlert()}
                    <Image
                        style = {styles.guestBafKilatIcon}
                        resizeMode="contain"
                        source={require('../../../../../assets/img/home/bafkilat.png')}
                    />
                    <Text style={Dimensions.get('window').width <= 360 ? styles.guestIconDescription360 : styles.guestIconDescription}>Aktivasi Sekarang</Text> 
            </TouchableOpacity>
        )
    }
}

export default ActivationCard;