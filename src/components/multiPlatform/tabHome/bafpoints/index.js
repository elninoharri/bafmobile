import React, {Component} from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import style from './style';

class BafPoints extends Component {
    constructor(props) {
        super(props);

    }
    
    render(){
        return(
            <View style={{flex: 1}}>
                <TouchableOpacity
                onPress={() => console.log('BAF POINTS')}
                style={style.cardContainer}
                >
                    <View style={style.viewContainer}>
                        <Image
                            source={require('../../../../../assets/img/home/bafpoint.png')}
                            style={style.icon}
                        />
                        <View style={style.viewMiddle}>
                            <View style={style.viewBafCoin}>
                                <Image
                                    source={require('../../../../../assets/img/home/bafCoin.png')}
                                    style={style.iconCoin}
                                />
                                <Text style={style.fontPoin}>BAF POIN</Text>
                            </View>
                            <Text style={style.font}>1.012.000</Text>
                        </View>
                        <TouchableOpacity
                            onPress={() => {console.log('REDEEM')}}
                            style={style.btnRedeem}
                        >
                            <Text style={style.fontRedeem}>
                            Redeem Poin
                            </Text>
                        </TouchableOpacity>

                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

export default BafPoints;