import {StyleSheet} from 'react-native';
import {BAF_COLOR_BLUE} from '../../../../utils/constant';
import {fonts} from '../../../../utils/fonts';

export default StyleSheet.create({
    cardContainer: {
        justifyContent: 'center',
        width: '80%',
        marginBottom: '5%',
        height: 65,
        backgroundColor: 'white',
        alignSelf: 'center',
        marginTop: '1%',
        borderRadius: 10,
        elevation: 5,
        padding: 7
      },
    font: {
        color: BAF_COLOR_BLUE,
        fontSize: 12,
        fontWeight: 'bold',
        fontFamily: fonts.primary.normal,
    },
    fontRedeem: {
        color: 'white',
        fontSize: 12,
        fontWeight: 'bold',
        fontFamily: fonts.primary.normal,
    },
    fontPoin: {
        color: '#EEAF30',
        fontSize: 12,
        fontWeight: 'bold',
        fontFamily: fonts.primary.normal,
    },
    icon: {
        width: '30%',
        height: '150%',
        resizeMode: 'contain',
        // backgroundColor: 'black'
    },
    iconCoin: {
        width: '20%',
        height:'100%',
        resizeMode: 'contain',
        // backgroundColor: 'black'
    },
    viewContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    viewMiddle: {
        // alignItems: 'center',
        width: '30%',
        // backgroundColor: 'grey'
    },
    viewBafCoin: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 3,
    },
    btnRedeem: {
        backgroundColor: BAF_COLOR_BLUE,
        borderRadius: 4,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 3,
        padding: 5,
    },
})