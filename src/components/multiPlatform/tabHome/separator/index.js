import React, {Component} from 'react';
import {Text, TouchableOpacity, BackHandler,View} from 'react-native';

export default class Separator extends Component{
    render(){
        return(
            <View style={{width:'100%',borderWidth:3,borderColor:'rgba(0, 0, 0, 0.05)'}}/>
        )
    }
}
