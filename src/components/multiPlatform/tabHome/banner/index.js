import React, {Component} from 'react';
import {Text, TouchableOpacity,StyleSheet, BackHandler,Dimensions,Image,SafeAreaView,ScrollView, Platform} from 'react-native';
import {Spinner, Container, View,Icon, Content} from 'native-base';
import styles from "./style";
import {SliderBox} from 'react-native-image-slider-box';
import ImageView from 'react-native-image-view';
import {connect} from 'react-redux';

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.3);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 0.8);



class Banner extends Component {
    constructor(props) {
        super(props);
        this.onPressImage = this.onPressImage.bind(this)
        this.state = {
            images : [
                {
                    source: require('../../../../../assets/img/home/banner/default-banner1.png'),
                    title: 'Paris',
                    width: 500,
                    height: 250
                },
            ],
            isImageViewVisible: false,
        }
      }

    onPressImage(index,images){
        this.setState({
            images:[{
                source: {uri: images},
                title: 'Paris',
                width: 600,
                height: 314
            }],
            isImageViewVisible: true,
        });
    }

    render(){
        const {images} = this.props;
        
        return(
            <View>
                 <View>
                    <SliderBox
                        images={images}
                        resizeMode={'stretch'}
                        autoplay
                        circleLoop
                        dotColor='#012855'
                        inactiveDotColor="#90A4AE"
                        // onCurrentImagePressed={index => this.onPressImage(index,images[index])}
                        sliderBoxHeight={Platform.OS == 'android' ? 200 : 210}
                        inactiveDotStyle={styles.inactiveDotStyle}
                        dotStyle={styles.activeDotStyle}
                    />
                 </View>
                    <ImageView
                        glideAlways
                        images={this.state.images}
                        isVisible={this.state.isImageViewVisible}
                        onClose={() => this.setState({isImageViewVisible: false})}
                    />
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
    };
  };
  
  const mapDispatchToProps = (dispatch) => {
    return {
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(Banner);


