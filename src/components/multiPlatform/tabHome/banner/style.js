import { StyleSheet, Platform, StatusBar } from 'react-native';
import {BAF_COLOR_BLUE,BAF_COLOR_YELLOW} from '../../../../utils/constant';

export default StyleSheet.create({
    inactiveDotStyle:{
        width: 10,
        height: 10,
        borderRadius: 15,
        marginHorizontal: -9,
        padding: 0,
        margin: 0
    },
    activeDotStyle:{
        width: 20,
        height: 10,
        borderRadius: 15,
        marginHorizontal: -9,
        padding: 0,
        margin: 0,
    }
})
