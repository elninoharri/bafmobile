import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {
  Content,
  Button,
  Icon,
  Right,
  Left,
  Row,
  Spinner,
  Body,
  Header,
} from 'native-base';
import moment from 'moment';
import styles from './style';
import {
  resJsonParser,
  substringSecondCircum,
  formatCurrency,
} from '../../../../utils/utilization';

export default class ListOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  dataBarang = (data, index) => {
    var dataBarang = false;
    if (data.QUESTION || data.QUESTION !== '') {
      dataBarang = resJsonParser(data.QUESTION);
    }
    return (
      <Row
        style={{
          alignSelf: 'center',
          height: 35,
          paddingHorizontal: 5,
        }}>
        <Left>
          <Text style={styles.textRow2}>
            {dataBarang.Jenis && dataBarang.Jenis !== ''
              ? substringSecondCircum(dataBarang.Jenis)
              : '-'}
          </Text>
        </Left>
        <Body style={{paddingHorizontal: 4}}>
          <Text style={styles.textRow2}>
            {dataBarang.Merk && dataBarang.Merk !== ''
              ? substringSecondCircum(dataBarang.Merk)
              : '-'}
          </Text>
        </Body>
        <Right>
          <Text style={styles.textRow2}>
            {dataBarang.Hargabrg && dataBarang.Hargabrg !== ''
              ? 'Rp.' + formatCurrency(dataBarang.Hargabrg)
              : 'Rp.-'}
          </Text>
        </Right>
      </Row>
    );
  };

  bottomButton = (data, index) => {
    const {goKonfirmasiPassword} = this.props;
    const ORDER_STAT = data.ORDER_STAT;
    const is_approved = data.is_approved;
    const LOB_CODE = data.LOB_CODE;
    if (
      (ORDER_STAT === '3' && is_approved === '1' && LOB_CODE === 'MP') ||
      (ORDER_STAT === '5' && is_approved === '1' && LOB_CODE === 'MP')
    ) {
      return (
        <View style={styles.ViewKonfirmasiPO} key={'item-' + index}>
          <TouchableOpacity
            style={styles.ButtonCancel}
            onPress={goKonfirmasiPassword}>
            <Text style={styles.buttonCancelText}>Pembatalan Order</Text>
          </TouchableOpacity>
        </View>
      );
    }
  };

  listButton = (data, index) => {
    const ORDER_STAT = data.ORDER_STAT;
    const is_approved = data.is_approved;
    const LOB_CODE = data.LOB_CODE;
    // return (
    //   <TouchableOpacity
    //     key={'item-' + index}
    //     style={styles.btnlistOrder}
    //     onPress={() => this.props.goFdeAdd(data.LOB_CODE, data.ORDER_TRX_H_ID)}>
    //     <Text style={styles.textButton}>Lengkapi Data</Text>
    //   </TouchableOpacity>
    // );

    if (ORDER_STAT === '23' && is_approved === '13' && LOB_CODE === 'MP') {
      return (
        <TouchableOpacity
          key={'item-' + index}
          style={styles.btnlistOrder}
          onPress={() => this.props.goPpbkApproval(data)}>
          <Text style={styles.textButton}>Persetujuan</Text>
        </TouchableOpacity>
      );
    }

    // ini di komen untuk ngetest ppbkApprovalPage

    if (ORDER_STAT === '3' && is_approved === '1' && LOB_CODE === 'MP') {
      return (
        <TouchableOpacity
          key={'item-' + index}
          style={styles.btnlistOrder}
          onPress={() =>
            this.props.goFdeAdd(data.LOB_CODE, data.ORDER_TRX_H_ID)
          }>
          <Text style={styles.textButton}>Lengkapi Data</Text>
        </TouchableOpacity>
      );
    } else if (ORDER_STAT === '6' && is_approved === '5' && LOB_CODE === 'MP') {
      return (
        <TouchableOpacity
          key={'item-' + index}
          style={styles.btnlistOrder}
          onPress={() =>
            this.props.goFdeRevise(data.LOB_CODE, data.ORDER_TRX_H_ID)
          }>
          <Text style={styles.textButton}>Butuh Revisi</Text>
        </TouchableOpacity>
      );
    } else {
      return <View style={{height: 30, width: 10, marginTop: -10}}></View>;
    }
  };

  render() {
    const {index, data, goDetailData} = this.props;
    const APP_SOURCE = data.APP_SOURCE;
    const APP_SOURCE_SPLIT = APP_SOURCE.split('^');
    return (
      <TouchableOpacity
        key={'item-' + data.ORDER_TRX_H_ID}
        style={styles.Content}
        onPress={goDetailData}>
        <View style={styles.CardHeader}>
          <View style={styles.HeaderOrderNo}>
            <Text style={styles.TextOrderNo} numberOfLines={1}>
              {data.ORDER_NO}
            </Text>
          </View>
          <View style={styles.DateHeader}>
            <Text style={styles.TextDate}>
              {moment(data.DTM_CRT)
                .utc()
                .locale('en')
                .format('YYYY-MM-DD, h:mm:ss a')}
            </Text>
          </View>
        </View>
        <View style={styles.CardBody}>
          <View style={styles.TopBodyContent}>
            <Text
              style={styles.TextFullname}
              numberOfLines={1}
              ellipsizeMode="tail">
              {data.FULLNAME}
            </Text>
          </View>

          <View style={styles.MiddleBodyContent}>
            <Text
              style={styles.TextIdNo}
              numberOfLines={1}
              ellipsizeMode="tail">
              {data.IDNO}
            </Text>
            {this.listButton(data)}
          </View>

          <View style={styles.MiddleBodyContent}>
            <Text style={styles.textRow3}>Status</Text>
            <Text
              style={styles.textRow3}
              numberOfLines={1}
              ellipsizeMode="tail">
              {data.isapproveddescr}
            </Text>
          </View>
          {data.orderstatdescr !== '' ? (
            <View style={styles.MiddleBodyContent}>
              <Text
                style={styles.textRow3}
                numberOfLines={1}
                ellipsizeMode="tail">
                Note
              </Text>
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={{
                  color: 'black',
                  fontSize: 12,
                  maxWidth: '49%',
                  fontWeight: 'bold',
                }}>
                {data.orderstatdescr}
              </Text>
            </View>
          ) : (
            <View></View>
          )}

          <View style={styles.categoryContainer}>
            <Row style={styles.categoryRow}>
              <Left>
                <Text style={styles.textRow1}>Tipe Barang</Text>
              </Left>
              <Body style={{paddingHorizontal: 3}}>
                <Text style={styles.textRow1}>Merk Barang</Text>
              </Body>
              <Right>
                <Text style={styles.textRow1}>Harga Barang</Text>
              </Right>
            </Row>
            {this.dataBarang(data, index)}
          </View>

          {this.bottomButton(data)}
        </View>
      </TouchableOpacity>
    );
  }
}
