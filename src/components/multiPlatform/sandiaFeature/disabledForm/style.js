

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    Container : {
        marginTop:10
    },
    Title : {
        fontSize:14, 
        color:'#888', 
        marginBottom:-10
    },
    Content : {
        backgroundColor:'white',
        borderWidth:1,
        borderColor:'#bbb',
        width:'100%',
        height:42,
        paddingLeft:10,
        justifyContent:'center',
        borderRadius:5
    },
    ContentTitle : {
        color:'#ccc',
        fontSize:16
    },
    Title : {
        fontSize:15,
        color:'grey'
    }
})