import React, { Component } from 'react';
import { Text, View} from 'react-native';
import { Label } from 'native-base';
import styles from './style';

export default class DisabledForm extends Component {
  render() {
    return (
        <View style={styles.Container}>
          <Label style={styles.Title}>{this.props.title}</Label>
          <View style={styles.Content} disabled = {true} >
            <Text style={styles.ContentTitle}>Silahkan Pilih</Text>
          </View>
        </View>
    );
  }
}