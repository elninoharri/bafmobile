import React, {Component} from 'react';
import {TouchableOpacity, Text, View, Image, ScrollView} from 'react-native';
import {Left, CardItem, Right, Body, Row, Icon} from 'native-base';
import {romanize} from '../../../utils/utilization';

import {styles} from './style';
import Modal from 'react-native-modal';
import ILrectangle from '../../../../assets/img/home/product/icon-rectangle.png';
import ICdeskripsi from '../../../../assets/img/home/product/icon-deskripsi.png';
import ICpersyaratan from '../../../../assets/img/home/product/icon-persyaratan.png';
import ICjangkawaktu from '../../../../assets/img/home/product/icon-jangkawaktu.png';
import ICnontitle from '../../../../assets/img/home/product/icon-nontitle.png';
import ILpengajuan from '../../../../assets/img/home/product/example-alurpengajuan.png';
import {fonts} from '../../../utils/fonts';
import style from '../../android/subHeaderBlue/style';

class ListCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: '',
      showModal: false,
      scrollOffset: null,
    };

    this.scrollViewRef = React.createRef();
  }

  handleOnScroll(event) {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    });
  }

  handleScrollTo = (p) => {
    if (this.scrollViewRef.current) {
      this.scrollViewRef.current.scrollTo(p);
    }
  };

  closeModal = () => {
    this.setState({showModal: false});
  };

  pressHandle = () => {
    this.setState({showModal: true});
  };

  renderMapping = () => {
    let number = 0;
    return this.props.text.map((result, key) => {
      number++;
      if (result.length !== 0) {
        return (
          <View style={styles.row}>
            <View style={{width: 20}}>
              <Text style={styles.textDefault}>{number + '. '}</Text>
            </View>
            <View style={{flex: 1}}>
              <Text style={styles.textDefault}>{result}</Text>
            </View>
          </View>
        );
      } else {
        return <View></View>;
      }
    });
  };

  renderModalMapping = (result, bullet) => {
    let number = 0;
    return result.map((result, key) => {
      number++;
      if (result.length !== 0) {
        return (
          <View style={bullet ? styles.left : styles.leftOff}>
            <View style={styles.row}>
              <View style={{width: 20}}>
                <Text style={bullet ? styles.bulletSize : styles.textDefault}>
                  {(bullet ? '\u2022' : number) + (bullet ? '' : '. ')}
                </Text>
              </View>
              <View>
                <Text style={bullet ? styles.pointText2 : styles.pointText}>
                  {result}
                </Text>
              </View>
            </View>
          </View>
        );
      }
    });
  };

  renderModalSub = (result, bullet) => {
    let number = 0;
    return result.map((result, key) => {
      number++;
      if (result.length !== 0) {
        return (
          <View style={bullet ? styles.left : styles.leftOff}>
            <View style={styles.row}>
              <View style={{width: 20}}>
                <Text style={bullet ? styles.bulletSize : styles.textDefault}>
                  {(bullet ? '\u2022' : number) + (bullet ? '' : '. ')}
                </Text>
              </View>
              <View>
                <Text style={bullet ? styles.pointText2 : styles.pointText}>
                  {result}
                </Text>
              </View>
            </View>
          </View>
        );
      }
    });
  };

  renderModalBullet = (result, bullets) => {
    let number = 0;
    return result.map((result, key) => {
      number++;
      if (result.length !== 0) {
        return (
          <View>
            <View style={styles.row}>
              <View style={{width: 20}}>
                <Text style={styles.bulletSize2}>
                  {(bullets ? '\u2022' : number) + (bullets ? '' : '. ')}
                </Text>
              </View>
              <View>
                <Text style={styles.pointText2}>{result}</Text>
              </View>
            </View>
          </View>
        );
      }
    });
  };

  renderPointBullet = (subcontent) => {
    let number = 0;
    return subcontent.map((result, key) => {
      number++;
      if (result.length !== 0) {
        return (
          <View style={{marginBottom: 10}}>
            <View style={styles.row}>
              <View style={{width: 20}}>
                <Text style={styles.roman}>{romanize(number) + '. '}</Text>
              </View>
              <View>
                <Text style={styles.subContent}>{result.subtitle}</Text>
              </View>
            </View>
            <View style={styles.bulletWrapper}>
              {this.renderModalBullet(result.bullet, true)}
            </View>
          </View>
        );
      }
    });
  };

  renderTextModal = (result) => {
    let number = 0;
    if (result.type === 'Paragraph') {
      return (
        <View style={{marginBottom: 15}}>
          <Text style={styles.titleModal}>{result.title}</Text>
          {result.subtitle ? (
            <Text style={styles.subtitleModal}>{result.subtitle}</Text>
          ) : (
            <View />
          )}
          <Text style={styles.textParagraph}>{result.subcontent}</Text>
          {result.bullet ? (
            this.renderModalMapping(result.bullet, true)
          ) : (
            <View></View>
          )}
        </View>
      );
    }
    if (result.type === 'Point') {
      return (
        <View style={{marginBottom: 15}}>
          <Text style={styles.titleModal}>{result.title}</Text>
          <View style={{marginBottom: 10}}>
            {this.renderModalMapping(result.subcontent)}
          </View>

          {result.bullet ? (
            this.renderModalMapping(result.bullet, true)
          ) : (
            <View></View>
          )}
        </View>
      );
    }

    if (result.type === 'Bullet') {
      return (
        <View style={{marginBottom: 15}}>
          <Text style={styles.titleModal}>{result.title}</Text>
          <View style={{marginBottom: 10}}>
            {this.renderModalMapping(result.subcontent)}
          </View>
          {result.bullets ? (
            <View style={{marginTop: -10}}>
              {this.renderModalBullet(result.bullets, true)}
            </View>
          ) : (
            <View></View>
          )}
        </View>
      );
    }

    if (result.type === 'PointBullet') {
      return (
        <View style={{marginBottom: 15}}>
          <Text style={styles.titleModal}>{result.title}</Text>
          <View style={{marginBottom: 10}}>
            {this.renderPointBullet(result.subcontent)}
          </View>
        </View>
      );
    }

    if (result.type === 'Image') {
      return (
        <View style={styles.imageWrapper}>
          <Image style={styles.image} source={ILpengajuan} />
        </View>
      );
    }
    if (result.type === 'Nested') {
      return (
        <View>
          <Text style={styles.titleModal}>{result.title}</Text>
          <View style>
            {result.subcontent.map((result, key) => {
              return (
                <View>
                  <Text style={styles.subtitleModal}>{result.subtitle}</Text>
                  {result.type === 'Point' ? (
                    this.renderModalMapping(result.data)
                  ) : (
                    <Text style={styles.textDefault}>{result.data}</Text>
                  )}
                  <View style={{marginBottom: 15}} />
                </View>
              );
            })}
          </View>
        </View>
      );
    }
    return <Text style={styles.textDefault}>{result.subcontent}</Text>;
  };

  renderModal = () => {
    return (
      <Modal
        testID={'modal'}
        isVisible={this.state.showModal}
        onSwipeComplete={() => this.closeModal()}
        onBackdropPress={() => this.closeModal()}
        swipeDirection={['down']}
        scrollTo={this.handleScrollTo}
        scrollOffset={this.state.scrollOffset}
        scrollOffsetMax={400 - 300}
        propagateSwipe={true}
        style={styles.modal}>
        <View style={{height: '60%'}}>
          <ScrollView
            ref={this.scrollViewRef}
            onScroll={this.handleOnScroll.bind(this)}
            scrollEventThrottle={16}>
            <View style={styles.modalHeight}>
              {/* icon modal */}
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                }}>
                <Image
                  style={{marginTop: 20, marginBottom: 17}}
                  source={ILrectangle}
                />

                {/* isi content */}
                <View>
                  {this.props.content.map((result, key) => {
                    return (
                      <View
                        style={{
                          alignItems: 'flex-start',
                          marginLeft: 36,
                          marginRight: 36,
                        }}>
                        {this.renderTextModal(result)}
                      </View>
                    );
                  })}
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </Modal>
    );
  };

  renderParagraph = () => {
    return (
      <View style={{borderBottomWidth: 0.5}}>
        <TouchableOpacity onPress={() => this.pressHandle()}>
          <CardItem>
            <Left>
              <Row>
                <Image
                  source={
                    this.props.title == 'Deskripsi Produk'
                      ? ICdeskripsi
                      : this.props.title == 'Persyaratan Umum'
                      ? ICpersyaratan
                      : ICjangkawaktu
                  }
                  style={styles.iconLeft}
                />
                <Body style={{marginRight: '-60%'}}>
                  <Text style={styles.textUp}>{this.props.title}</Text>
                  <Text style={styles.textDefault}>{this.props.text}</Text>
                </Body>
              </Row>
            </Left>
            <Right>
              {this.props.title === 'Jangka Waktu Angsuran' ? (
                <View />
              ) : (
                <Icon
                  size={25}
                  name={this.props.iconRight}
                  style={{marginRight: '5%'}}
                />
              )}
            </Right>
          </CardItem>
        </TouchableOpacity>
        {this.props.title === 'Jangka Waktu Angsuran' ? (
          <View style={{height: 5}} />
        ) : (
          this.renderModal()
        )}
      </View>
    );
  };

  renderPoint = () => {
    return (
      <View style={{borderBottomWidth: 0.5}}>
        <TouchableOpacity onPress={() => this.pressHandle()}>
          <CardItem>
            <Left>
              <Row>
                <Image
                  source={
                    this.props.title == 'Deskripsi Produk'
                      ? ICdeskripsi
                      : this.props.title == 'Persyaratan Umum'
                      ? ICpersyaratan
                      : ICjangkawaktu
                  }
                  style={styles.iconLeft}
                />
                <Body style={{marginRight: '-60%'}}>
                  <Text style={styles.textUp}>{this.props.title}</Text>
                  {this.renderMapping()}
                </Body>
              </Row>
            </Left>
            <Right>
              <Icon
                size={25}
                name={this.props.iconRight}
                style={{marginRight: '5%'}}
              />
            </Right>
          </CardItem>
        </TouchableOpacity>
        {this.renderModal()}
      </View>
    );
  };

  renderSubpage = () => {
    return (
      <View style={{borderBottomWidth: 0.5}}>
        <TouchableOpacity onPress={() => this.pressHandle()}>
          <CardItem>
            <Left>
              <Row>
                <Image
                  source={
                    this.props.title == 'Deskripsi Produk'
                      ? ICdeskripsi
                      : this.props.title == 'Persyaratan Umum'
                      ? ICpersyaratan
                      : ICpersyaratan
                  }
                  style={styles.iconLeft}
                />
                <Body style={{marginRight: '-60%'}}>
                  <Text style={styles.textUp}>{this.props.title}</Text>
                  {this.renderMapping()}
                </Body>
              </Row>
            </Left>
            <Right>
              <Icon
                size={25}
                name={this.props.iconRight}
                style={{marginRight: '5%'}}
              />
            </Right>
          </CardItem>
        </TouchableOpacity>
        {/* //here where modal rendered */}
        {this.renderModal()}
      </View>
    );
  };

  renderBullet = () => {
    return (
      <View style={{borderBottomWidth: 0.5}}>
        <TouchableOpacity onPress={() => this.pressHandle()}>
          <CardItem>
            <Left>
              <Row>
                <Image
                  source={
                    this.props.title == 'Penambahan Informasi'
                      ? ICdeskripsi
                      : ICjangkawaktu
                  }
                  style={styles.iconLeft}
                />
                <Body style={{marginRight: '-60%'}}>
                  <Text style={styles.textUp}>{this.props.title}</Text>
                  {this.renderMapping()}
                </Body>
              </Row>
            </Left>
            <Right>
              <Icon
                size={25}
                name={this.props.iconRight}
                style={{marginRight: '5%'}}
              />
            </Right>
          </CardItem>
        </TouchableOpacity>
        {this.renderModal()}
      </View>
    );
  };

  renderNonTitle = () => {
    return (
      <View>
        <TouchableOpacity onPress={() => this.pressHandle()}>
          <CardItem>
            <Left>
              <Row>
                <Image source={ICnontitle} style={styles.iconLeft} />
                <Body style={{marginRight: '-60%'}}>
                  <Text style={styles.textDefault}>{this.props.text}</Text>
                </Body>
              </Row>
            </Left>
            <Right>
              <View></View>
            </Right>
          </CardItem>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const {type} = this.props;
    return type === 'Paragraph'
      ? this.renderParagraph()
      : type === 'Point'
      ? this.renderPoint()
      : type === 'Bullet'
      ? this.renderBullet()
      : type === 'Subpage'
      ? this.renderSubpage()
      : this.renderNonTitle();
  }
}

export default ListCard;
