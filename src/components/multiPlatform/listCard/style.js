import {StyleSheet, Dimensions} from 'react-native';
import {fonts} from '../../../utils/fonts';
const {width: WIDTH} = Dimensions.get('window');

export const styles = StyleSheet.create({
  left: {
    marginLeft: 30,
  },
  bulletWrapper: {
    marginLeft: 30,
  },
  leftOff: {
    marginLeft: 0,
  },
  roman: {
    fontFamily: fonts.primary.normal,
    color: 'rgba(0, 0, 0, 0.8)',
    fontSize: 14,
  },

  imagePromoDetail: {
    flex: 1,
    alignSelf: 'center',
  },
  textDefault: {
    color: 'rgba(0, 0, 0, 0.8)',
    textAlign: 'justify',
    fontFamily: fonts.primary.normal,
  },
  imageWrapper: {
    width: WIDTH * 0.81,
    height: 190,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  modalHeight: {
    height: 760,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  bulletSize: {
    fontFamily: fonts.primary.bold,
    fontSize: 17,
  },
  bulletSize2: {
    fontFamily: fonts.primary.bold,
    fontSize: 18,
  },

  textParagraph: {
    fontSize: 14,
    lineHeight: 18,
    marginBottom: 10,
    textAlign: 'justify',
    color: 'rgba(0, 0, 0, 0.8)',
    width: WIDTH * 0.8,
    fontFamily: fonts.primary.normal,
  },
  mainOuterComponent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000088',
  },

  row: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    // marginVertical: 2,
  },

  pointText: {
    width: WIDTH * 0.71,
    color: 'rgba(0, 0, 0, 0.8)',
    fontFamily: fonts.primary.normal,
  },

  pointText2: {
    width: WIDTH * 0.61,
    color: 'rgba(0, 0, 0, 0.8)',
    fontFamily: fonts.primary.normal,
    marginTop: 5,
    lineHeight: 15,
  },

  subContent: {
    width: WIDTH * 0.61,
    color: 'rgba(0, 0, 0, 0.8)',
    fontFamily: fonts.primary.normal,
    lineHeight: 18,
    fontSize: 14,
  },

  subtitleModal: {
    fontSize: 14,
    fontFamily: fonts.primary.bold,
    marginBottom: 10,
    color: 'black',
  },

  titleModal: {
    color: 'black',
    fontSize: 18,
    fontFamily: fonts.primary.bold,
    marginTop: 0,
    marginBottom: 15,
  },

  textHead: {
    fontSize: 20,
    fontFamily: fonts.primary.bold,
    color: '#002f5f',
    alignSelf: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  textRow: {
    fontSize: 13,
    color: 'black',
    fontFamily: fonts.primary.bold,
  },
  textUp: {
    color: 'black',
    fontFamily: fonts.primary.bold,
    fontSize: 15,
    paddingRight: 20,
  },
  bodyCard: {
    marginRight: '-60%',
  },
  iconStyle: {
    fontSize: 20,
    paddingRight: 10,
    paddingLeft: 5,
    fontFamily: fonts.primary.normal,
  },
  iconLeft: {
    marginRight: '5%',
  },
  cardStyle: {
    marginTop: -20,
  },
  btnVoucher: {
    backgroundColor: '#002F5F',
    width: '60%',
    height: 40,
    marginTop: 10,
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },

  // scrolable modal
  view: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  scrollableModal: {
    height: 400,
  },

  scrollableModalContent1: {
    // marginTop: 345,
    height: 400,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },

  title: {
    fontSize: 14,
    color: 'black',
    fontFamily: fonts.primary.normal,
  },
});
