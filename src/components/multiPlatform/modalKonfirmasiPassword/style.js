import {StyleSheet, Dimensions} from 'react-native';
import {fonts} from '../../../utils/fonts';

export default StyleSheet.create({
  Modal: {
    backgroundColor: '#F8F8F8',

    borderRadius: 10,
    marginTop: Dimensions.get('window').height * 0.25,
    minHeight: 220,
    maxHeight: 220,
  },
  renderFieldContainer: {
    marginHorizontal: 22,
    marginRight: 30,
    marginTop: 10,
    marginBottom: 20,
  },
  renderFieldLabel: (hasError) => ({
    fontSize: 15,
    fontFamily: fonts.primary.bold,
    color: hasError ? 'red' : 'black',
    marginBottom: 7,
  }),
  renderFieldItem: (hasError) => ({
    borderRadius: 5,
    height: 40,
    borderColor: hasError ? 'red' : '#ccc',
  }),
  renderFieldInput: {fontFamily: fonts.primary.normal, fontSize: 14},
  renderFieldIcon: {
    position: 'absolute',
    alignSelf: 'flex-end',
    right: 10,
    bottom: 8,
    fontSize: 22,
  },
  textConfirm: {
    color: 'white',
    fontFamily: fonts.primary.bold,
    fontSize: 16,
  },
  iconWrapper: {
    marginTop: 10,
    marginRight: 10,
    alignSelf: 'flex-end',
  },
  inputStyle: {
    marginHorizontal: 22,
    marginRight: 30,
    marginTop: 10,
  },
  ErrorDesc: {
    color: 'red',
    alignSelf: 'stretch',
    textAlign: 'right',
    fontSize: 10,
  },
  Icon: {
    color: '#002f5f',
    fontSize: 16,
  },
  title: {
    fontSize: 18,
    fontFamily: fonts.primary.normal,
    color: '#002f5f',
    textAlign: 'center',
  },
  titleWrapper: {justifyContent: 'center'},
  container: {flex: 1, marginTop: 10},
  btnBlue: {
    backgroundColor: '#002f5f',
    marginHorizontal: 22,

    marginBottom: 10,
    height: 40,
    width: 90,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
});
