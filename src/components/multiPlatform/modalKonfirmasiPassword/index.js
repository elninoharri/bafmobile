import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  KeyboardAvoidingView,
  Alert,
} from 'react-native';
import ModalKonfirmasi from 'react-native-modal';
import styles from './style';
import {Field} from 'redux-form';
import {
  Icon,
  Label,
  Input,
  Item,
  Picker,
  Button,
  Header,
  Left,
  Body,
  Title,
  Right,
} from 'native-base';
import {fonts} from '../../../utils/fonts';

export const renderFieldPassword = ({
  input,
  type,
  placeholderInput,
  label,
  secureTextEntry,
  nameIcon,
  onPressIcon,
  meta: {touched, error, warning},
}) => {
  var hasError = false;
  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={styles.renderFieldContainer}>
      <Label style={styles.renderFieldLabel(hasError)}>{label}</Label>
      <View>
        <Item regular style={styles.renderFieldItem(hasError)}>
          <Input
            {...input}
            placeholder={placeholderInput}
            secureTextEntry={secureTextEntry}
            type={type}
            style={styles.renderFieldInput}
          />
        </Item>
        <Icon
          active
          name={nameIcon}
          onPress={onPressIcon}
          style={styles.renderFieldIcon}
        />
      </View>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

export default class ModalKonfirmasiPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      icon: 'eye-off',
      show: true,
    };
  }

  _changeIcon = () => {
    this.setState((prevState) => ({
      icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
      show: !prevState.show,
    }));
  };

  onSubmit(data) {
    console.log('Data : ', data);
    if (!data.password) {
      Alert.alert('Informasi', 'Password Harus diisi');
    }
  }
  render() {
    return (
      <ModalKonfirmasi
        animationIn="slideInUp"
        animationOut="slideOutDown"
        onBackdropPress={this.props.setModalKonfirmasi}
        onSwipeComplete={this.props.setModalKonfirmasi}
        swipeDirection="right"
        isVisible={this.props.modalKonfirmasiPassword}
        style={styles.Modal}>
        <View style={styles.iconWrapper}>
          <Icon
            type="FontAwesome"
            name="times"
            style={styles.Icon}
            onPress={this.props.setModalKonfirmasi}
          />
        </View>
        <View style={styles.titleWrapper}>
          <Text style={styles.title}>Konfirmasi Pembatalan</Text>
        </View>
        <View style={styles.container}>
          <Field
            name="password"
            label="Password"
            component={renderFieldPassword}
            secureTextEntry={this.state.show}
            nameIcon={this.state.icon}
            placeholderInput="Masukkan Password Kamu"
            onPressIcon={() => this._changeIcon()}
          />

          <TouchableOpacity
            style={styles.btnBlue}
            onPress={this.props.handleSubmit(
              this.props.onSubmitKonfirmasiPassword,
            )}
            disabled={this.props.submitting}>
            <Text style={styles.textConfirm}>OK</Text>
          </TouchableOpacity>
        </View>
      </ModalKonfirmasi>
    );
  }
}
