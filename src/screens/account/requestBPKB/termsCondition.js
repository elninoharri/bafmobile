import React, {Component} from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  BackHandler,
  Platform,
  SafeAreaView,
} from 'react-native';

import {
  substringCircum,
  substringSecondCircum,
  substringThirdCircum,
  substringFourthCircum,
  substringFifthCircum,
} from '../../../utils/utilization';

import {
  Container,
  Picker,
  Label,
  Input,
  Item,
  Icon,
  Content,
  Right,
  Header,
  Left,
  Button,
  Body,
  Title,
} from 'native-base';
import {bindActionCreators} from 'redux';
import Toast from 'react-native-easy-toast';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import NetInfo from '@react-native-community/netinfo';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import {connect} from 'react-redux';
import {reduxForm, Field, change, reset} from 'redux-form';
import {styles} from './style';
import {fonts} from '../../../utils/fonts';
import {BAF_COLOR_BLUE} from '../../../utils/constant';
import ListCard from '../../../components/multiPlatform/listCard';
import requestData from './request.json';
import mockData from './mock.json';
import Modal from 'react-native-modal';
import moment from 'moment';
import DatePickerIos from 'react-native-date-picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import analytics from '@react-native-firebase/analytics';
import HTML from 'react-native-render-html';

class RequestBPKBTC extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Syarat & Ketentuan Pengambilan BPKB',
      data: [{Code: '', Descr: 'Pilih'}],
      pickerKontrakSelected: '',
      isConnected: true,
      visible: false,
      show: false,
      BirthDate: new Date(),
      alertShowed: false,
      userToken: false,
    };
  }

  componentDidMount = async () => {
    // const {getBPKB} = this.props;
    var array = [{Code: '', Descr: 'Pilih'}];

    analytics().setCurrentScreen(
      'screenPengambilanBPKBTC',
      'screenPengambilanBPKBTC',
    );
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

    const detailUserNonParse = await AsyncStorage.getItem('detailUser');
    const userToken = await AsyncStorage.getItem('userToken');

    if (userToken) {
      this.setState({userToken: userToken});
    }
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = async () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
        onPressNegativeButton={this.handleNegativeButtonAlert}
      />
    );
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false, alertFor: false});
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false, alertFor: false});
  };

  render() {
    const {handleSubmit, submitting} = this.props;

    const content = `
      <p><strong> Syarat dan Ketentuan Pengambilan BPKB </strong></p>
      <p>&nbsp; <strong> A.&nbsp;Informasi Umum Pengambilan BPKB: </strong></p>
      <ol>
      <li>Pengambilan BPKB hanya dapat dilakukan di Kantor BAF</li>
      <li>Pengambilan BPKB hanya dapat dilakukan sesuai jam operasional Kantor BAF:</li>
      </ol>
      <ul>
      <li>Senin &ndash; Jumat : 08.30 &ndash; 15.00</li>
      <li>Sabtu : 08.30 &ndash; 12.00</li>
      </ul>
      <ol start="3">
      <li>Tanggal pengambilan BPKB yang dapat dipilih konsumen adalah minimal H+1 dari tanggal pada saat melakukan reservasi.</li>
      <li>Pengambilan dengan surat kuasa (oleh pihak ketiga) paling cepat H+3 dari tanggal saat melakukan reservasi.</li>
      <li>Harap datang 15 menit sebelum jadwal pengambilan BPKB apabila pembayaran angsuran terakhir dilakukan di kasir cabang.</li>
      <li>Apablia pembayaran angsuran terakhir dilakukan via Online Payment, pengambilan BPKB dapat dilakukan setelah H+3 setelah melakukan pelunasan.</li>
      </ol>
      <p><strong> B. Syarat Pengambilan BPKB: </strong></p>
      <ol>
      <li>Pengambilan BPKB oleh konsumen langsung:</li>
      </ol>
      <ul>
      <li>Asli identitas diri konsumen : KTP/SIM/Surat Keterangan Domisili/Resi KTP yang masih berlaku,</li>
      <li>Apabila ada perbedaan atau perubahan antara data KTP/SIM konsumen dapat melampirkan surat keterangan dari kelurahan,</li>
      <li>Kuitansi asli / bukti pembayaran angsuran terakhir,</li>
      <li>Menunjukan STNK Asli.</li>
      </ul>
      <ol start="2">
      <li>Pengambilan BPKB dengan Kuasa yang diberikan konsumen kepada pihak keluarga / pihak lain:</li>
      </ol>
      <ul>
      <li>Asli identitas diri konsumen : KTP/SIM/Surat Keterangan Domisili/Resi KTP yang masih berlaku,</li>
      <li>Apabila ada perbedaan atau perubahan antara data KTP/SIM konsumen dapat melampirkan surat keterangan dari kelurahan,</li>
      <li>Kuitansi asli / bukti pembayaran angsuran terakhir,</li>
      <li>Asli Surat kuasa bermaterai dari konsumen kepada penerima kuasa,</li>
      <li>Surat pernyataan bermaterai dari penerima kuasa (Di isi pada saat proses pengambilan BPKB di Kantor BAF),</li>
      <li>Asli identitas KTP/SIM penerima kuasa,</li>
      <li>Kartu Keluarga (Apabila diambil oleh anggota keluarga)</li>
      </ul>
      <p><strong> C. Biaya Pengambilan BPKB </strong></p>
      <ol>
      <li>Tidak dikenakan biaya apabila lokasi pengambilan BPKB sama dengan Kantor BAF tempat pengajuan aplikasi,</li>
      <li>Biaya penitipan BPKB Rp 2.500,- / hari apabila pengambilan BPKB dilakukan setelah 30 hari dari tanggal pelunasan,</li>
      <li>Biaya Pengambilan BPKB dengan kuasa:</li>
      </ol>
      <ul>
      <li>Tidak dikenakan biaya apabila dikuasakan kepada anggota keluarga sesuai dengan kartu keluarga konsumen,</li>
      <li>Biaya pengambilan BPKB yang dikuasakan kepada pihak ketiga / pihak lain:</li>
      <li>Pembiayaan Motor Baru, Motor Bekas &amp; Dana Syariah : Rp. 150.000,</li>
      <li>Pembiayaan Mobil Baru &amp; SGU CAR: Rp. 300.000,-</li>
      </ul>
      <ol start="4">
      <li>Biaya pengambilan BPKB Interbranch (Beda Kantor BAF):</li>
      </ol>
      <ul>
      <li>Pembiayaan Motor Baru, Motor Bekas &amp; Dana Syariah : Rp. 150.000,-</li>
      <li>Pembiayaan Mobil Baru &amp; SGU CAR : Rp. 300.000,-</li>
      </ul>
    `;
    return (
      <Container style={styles.container}>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}

        <Toast
          ref="toast"
          style={styles.toast}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />
        
        <Content disableKBDismissScroll={false} style={styles.content}>
          <View
            style={{
              // backgroundColor: 'red',
              paddingRight: 30,
              paddingLeft: 30,
            }}>
            <HTML
              tagsStyles={{
                ul: {
                  marginTop: 10,
                  marginBottom: -10,
                },
                h1: {
                  fontSize: 18,
                  marginTop: 10,
                  color: 'black',
                  fontFamily: fonts.primary.normal,
                },

                li: {
                  fontFamily: fonts.primary.normal,
                  fontSize: 14,
                  lineHeight: 20,

                  textAlign: 'justify',
                  color: 'rgba(0, 0, 0, 0.8)',
                },
              }}
              listsPrefixesRenderers={{
                ol: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                  const {allowFontScaling, baseFontStyle, index} = passProps;
                  const baseFontSize = baseFontStyle.fontSize || 14;

                  const bulletNumber = htmlAttribs.start
                    ? htmlAttribs.start
                    : index + 1;
                  return (
                    <View style={styles.subContent}>
                      <Text style={[styles.textDefault]}>
                        {bulletNumber + '. '}
                      </Text>
                    </View>
                  );
                },
              }}
              html={content}
            />
          </View>
          <View style={{backgroundColor: 'white'}}>
            <TouchableOpacity
              style={styles.btnKirim}
              onPress={() =>
                this.props.navigation.navigate('RequestBPKP', {params: false})
              }
              disabled={submitting}>
              <Text style={styles.text}>Saya Setuju</Text>
            </TouchableOpacity>
          </View>
        </Content>
        {this.showAlert()}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    formRequestBPKBTC: state.form.formRequestBPKBTC,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

RequestBPKBTC = reduxForm({
  form: 'formRequestBPKBTC',
  enableReinitialize: true,
})(RequestBPKBTC);

export default connect(mapStateToProps, matchDispatchToProps)(RequestBPKBTC);
