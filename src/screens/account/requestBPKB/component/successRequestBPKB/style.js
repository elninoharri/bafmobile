import {StyleSheet} from 'react-native';
import {BAF_COLOR_BLUE} from '../../../../../utils/constant';
import {fonts} from '../../../../../utils/fonts';

export default StyleSheet.create({
  cardFirstFloorSubSeparatorIosSmall: {
    width: 1,
    height: '70%',
    borderRightWidth: 1,
    position: 'absolute',
    right: 8,
    bottom: 10,
    borderRightColor: 'rgba(0,0,0,0.3)',
  },
  cardFirstFloorSubSeparatorIosLarge: {
    width: 1,
    height: '70%',
    borderRightWidth: 1,
    position: 'absolute',
    right: 1,
    top: '100%',
    borderRightColor: 'rgba(0,0,0,0.3)',
  },
  wrapperImageText: {
    alignSelf: 'center',
    marginTop: 60,
    alignItems: 'center',
  },
  textBerhasil: {
    color: BAF_COLOR_BLUE,
    fontWeight: 'bold',
    marginTop: 15,
    fontSize: 22
  },
  textInfo: {
    textAlign: 'justify',
    color: 'rgba(0,0,0,0.6)',
    fontSize: 15,
  },
  wrapperTextInfo: {
    flexDirection: 'column',
    paddingLeft: 34,
    paddingRight: 34,
    marginTop: 35,
  },
  wrapperDetailRequestSuccess: {
    flexDirection: 'row',
    paddingHorizontal: 8,
    marginTop: 10,
    top: 0,
  },
  divide: {
    textAlignVertical: 'top',
    paddingRight: 2,
  },
  wrapperTextDetail: {
    flex: 2,
    paddingTop: 2,
  },
  wrapperTextTitle: {
    flex: 1,
    paddingTop: 1,
  },
  textTitle: {
    color: 'rgba(0,0,0,0.6)',
    fontSize: 15,
  },
  textDetail: {
    color: 'rgba(0,0,0,0.6)',
    fontSize: 15,
  },
  btnKirim: {
    alignSelf: 'center',
    width: '70%',
    height: 40,
    borderRadius: 4,
    backgroundColor: '#002f5f',
    justifyContent: 'center',
    marginTop: 35,
    marginBottom: 29,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  textBack: {
    textAlign: 'center',
    fontSize: 15,
    color: 'white',
    fontFamily: fonts.primary.bold,
  },
  imageBerhasil: {
    height: 100,
    width: 100,
    resizeMode: 'contain',
  },
});
