import React, {Component} from 'react';
import {Container, Content, Text} from 'native-base';
import {
  View,
  Image,
  TouchableOpacity,
  Platform,
} from 'react-native';
import Style from './style';
import SubHeaderAndroid from '../../../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../../../components/ios/subHeaderBlue';

class SuccessRequestBPKB extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSuccessRequestBPKB: null,
    };
  }

  goBack = async () => {
    this.props.navigation.goBack();
  };

  handleInvalid = async () => {
    await AsyncStorage.removeItem('userToken');
    await AsyncStorage.removeItem('userData');
    await AsyncStorage.removeItem('detailUser');
    await AsyncStorage.removeItem('activeAgreement');
    this.setState({
      alertFor: 'invalidToken',
      alertShowed: true,
      alertMessage: 'Sesi login Anda sudah habis',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  render() {
    const {successRequestData} = this.props.route.params;
    return (
      <Container>
        {Platform.OS === 'android' ? (
          <SubHeaderAndroid
            title="Pengambilan BPKB"
          />
        ) : (
          <SubHeaderIos
            title="Pengambilan BPKB"
          />
        )}
        <Content>
          <View style={{flexDirection: 'column'}}>
            <View style={Style.wrapperImageText}>
              <Image
                style={Style.imageBerhasil}
                source={require('../../../../../../assets/img/Icon/icon_success_bolder.png')}
              />
              <Text style={Style.textBerhasil}>Berhasil</Text>
            </View>
            <View style={Style.wrapperTextInfo}>
              <Text
                style={
                  Style.textInfo
                }>{`Anda sudah terdaftar untuk jadwal pengambilan BPKB dan pesan ini telah dikirim ke email Anda.\nBerikut jadwal pengambilan BPKB pada :`}</Text>

              {/* Nama Kantor */}
              <View style={Style.wrapperDetailRequestSuccess}>
                <View style={Style.wrapperTextTitle}>
                  <Text style={Style.textTitle}>Nama Kantor BAF</Text>
                </View>
                <View style={Style.divide}>
                  <Text> : </Text>
                </View>
                <View style={Style.wrapperTextDetail}>
                  <Text style={[Style.textDetail, {fontWeight: 'bold'}]}>
                    {successRequestData.NamaCabang ? successRequestData.NamaCabang : '-'}
                  </Text>
                </View>
              </View>

              {/* Alamat */}
              <View style={Style.wrapperDetailRequestSuccess}>
                <View style={Style.wrapperTextTitle}>
                  <Text style={Style.textTitle}>Alamat Kantor BAF</Text>
                </View>
                <View style={Style.divide}>
                  <Text> : </Text>
                </View>
                <View style={Style.wrapperTextDetail}>
                  <Text style={Style.textDetail}>
                    {successRequestData.AlamatCabang ? successRequestData.AlamatCabang : '-'}
                  </Text>
                </View>
              </View>

              {/* Tanggal */}
              <View style={Style.wrapperDetailRequestSuccess}>
                <View style={Style.wrapperTextTitle}>
                  <Text style={Style.textTitle}>Tanggal</Text>
                </View>
                <View style={Style.divide}>
                  <Text> : </Text>
                </View>
                <View style={Style.wrapperTextDetail}>
                  <Text style={Style.textDetail}>
                    {successRequestData.Tanggal ? successRequestData.Tanggal : '-'}
                  </Text>
                </View>
              </View>

              {/* Jam */}
              <View style={Style.wrapperDetailRequestSuccess}>
                <View style={Style.wrapperTextTitle}>
                  <Text style={Style.textTitle}>Jam</Text>
                </View>
                <View style={Style.divide}>
                  <Text> : </Text>
                </View>
                <View style={Style.wrapperTextDetail}>
                  <Text style={Style.textDetail}>{successRequestData.Jam ? successRequestData.Jam : '-'}</Text>
                </View>
              </View>

              <Text
                style={[
                  Style.textInfo,
                  {marginTop: 10},
                ]}>{`Harap dicatat dan Mohon datang tepat waktu ya !`}</Text>
            </View>
          </View>

          <View style={{backgroundColor: 'white'}}>
            <TouchableOpacity
              style={Style.btnKirim}
              onPress={() => this.props.navigation.navigate('Home')}>
              <Text style={Style.textBack}>Kembali Ke Beranda</Text>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}

export default SuccessRequestBPKB;
