import React, {Component} from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  BackHandler,
  Platform,
  SafeAreaView,
  Alert,
} from 'react-native';

import {
  substringCircum,
  substringSecondCircum,
  substringThirdCircum,
  substringFourthCircum,
  substringFifthCircum,
  substringSixCircum,
  substringSevenCircum,
  jsonParse,
  handleRefreshToken,
  substringStrip,
  substringColone,
} from '../../../utils/utilization';

import {
  Container,
  Label,
  Input,
  Item,
  Icon,
  Content,
  Right,
  Header,
  Picker,
  Left,
  Button,
  Body,
  Title,
} from 'native-base';

import {Picker as PickerAndroid} from '@react-native-picker/picker';
import {bindActionCreators} from 'redux';
import Toast from 'react-native-easy-toast';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import NetInfo from '@react-native-community/netinfo';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import {connect} from 'react-redux';
import {reduxForm, Field, change, reset} from 'redux-form';
import {styles} from './style';
import {fonts} from '../../../utils/fonts';
import {
  BAF_COLOR_BLUE,
  TOKEN_EXP,
  MESSAGE_TOKEN_EXP,
  MESSAGE_TOKEN_INVALID,
} from '../../../utils/constant';
import ListCard from '../../../components/multiPlatform/listCard';
import requestData from './request.json';
import Modal from 'react-native-modal';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import {
  getBPKB,
  submitBPKB,
  timeSlotBPKB,
  checkAvailBook,
  checkHoliday,
  getdetailBpkb,
  rescheduleBpkb,
} from '../../../actions/account';
import {refreshToken} from '../../../actions';
import AsyncStorage from '@react-native-community/async-storage';
import {FormRequestBPKBValidate} from '../../../validates/FormRequestBPKBValidate';
import Spinner from 'react-native-loading-spinner-overlay';
import analytics from '@react-native-firebase/analytics';

export const renderField = ({
  input,
  type,
  label,
  editable,
  keyboardType,
  maxLength,
  multiline,
  placeholder,
  numberOfLines,
  format,
  normalize,
  customError,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '80%', marginTop: 15, alignSelf: 'center'}}>
      <Label
        style={{
          fontSize: 16,
          color: hasError ? 'red' : 'black',
          alignSelf: 'flex-start',
          marginBottom: 5,
          fontFamily: fonts.primary.bold,
        }}>
        {label}
      </Label>
      <Item
        regular
        style={{
          backgroundColor: 'white',
          height: 40,
          borderColor:
            customError || hasError
              ? 'red'
              : !editable
              ? '#DADADA'
              : BAF_COLOR_BLUE,
          borderWidth: 1,
          borderRadius: 5,
          backgroundColor: !editable ? '#F4F4F4' : null,
          paddingRight: 10,
        }}>
        <Input
          style={{
            fontSize: 14,
            paddingLeft: '4%',
            color: editable ? 'black' : '#aaa',
          }}
          {...input}
          type={type}
          placeholder={placeholder}
          editable={editable}
          keyboardType={keyboardType}
          maxLength={maxLength}
          multiline={multiline}
          numberOfLines={numberOfLines}
          format={format}
          normalize={normalize}
        />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderFieldDatePicker = ({
  input,
  type,
  iconright,
  placeholder,
  keyboardType,
  maxLength,
  label,
  secureTextEntry,
  customError,
  editable,
  onPressIcon,
  onChangeDate,
  openModal,
  closeModal,
  visible,

  formatDate,
  modeDate,
  meta: {touched, error},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '80%', marginTop: 15, alignSelf: 'center'}}>
      <Label
        style={{
          fontSize: 16,
          color: hasError ? 'red' : 'black',
          alignSelf: 'flex-start',
          marginBottom: 5, //masukin style.js
          fontFamily: fonts.primary.bold,
        }}>
        {label}
      </Label>
      {Platform.OS == 'android' ? (
        <Item
          regular
          style={{
            backgroundColor: 'white',
            height: 40,
            borderColor:
              customError || hasError
                ? 'red'
                : !editable
                ? BAF_COLOR_BLUE
                : 'rgba(0, 0, 0, 0.5)',
            borderWidth: 1,
            borderRadius: 5,
            // backgroundColor: !editable ? '#F4F4F4' : null,
            paddingRight: 10, //masukin style.js
          }}>
          {visible && (
            <DateTimePicker
              {...input}
              style={{marginVertical: 20}}
              testID="dateTimePicker"
              value={new Date()}
              mode="date"
              // minimumDate={new Date(2021, 4, 28)} 
              // minimumDate={new moment().add(1, 'days').format("YYYY,MM,DD")} 
              minimumDate={ new Date()} 
              display="calendar"
              onChange={(newDate) => onChangeDate(newDate)}
            />
          )}
          <TouchableOpacity
            onPress={() => openModal()}
            disabled={editable}
            style={{flexDirection: 'row'}}>
            <Input
              {...input}
              type={type}
              disabled={true}
              placeholder={placeholder}
              keyboardType={keyboardType}
              maxLength={maxLength}
              secureTextEntry={secureTextEntry}
              style={{fontSize: 14, fontFamily: fonts.primary.normal}}
            />
          </TouchableOpacity>
        </Item>
      ) : (
        <Item
          regular
          style={{
            backgroundColor: 'white',
            height: 40,
            borderColor:
              customError || hasError
                ? 'red'
                : !editable
                ? BAF_COLOR_BLUE
                : '#DADADA',
            borderWidth: 1,
            borderRadius: 5,

            // backgroundColor: !editable ? '#F4F4F4' : null,
            paddingRight: 10,
          }}>
          <Modal isVisible={visible}>
            <View
              style={{
                width: '95%',
                height: 290,
                backgroundColor: 'white',
                borderRadius: 5,
                alignItems: 'center',
                alignSelf: 'center',
              }}>
              <TouchableOpacity
                onPress={closeModal}
                style={{
                  width: 50,
                  height: 30,
                  backgroundColor: BAF_COLOR_BLUE,
                  position: 'absolute',
                  right: 5,
                  top: 3,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 5,
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 14,
                    fontWeight: 'bold',
                  }}>
                  Tutup
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => onChangeDate('today')}
                style={{
                  width: 55,
                  height: 30,
                  backgroundColor: BAF_COLOR_BLUE,
                  position: 'absolute',
                  left: 5,
                  top: 3,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 5,
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 14,
                    fontWeight: 'bold',
                  }}>
                  Hari ini
                </Text>
              </TouchableOpacity>
              <DateTimePicker
                {...input}
                textColor="black"
                testID="dateTimePicker"
                value={new Date()}
                mode="date"
                display="inline"
                minimumDate={new Date()}
                onChange={(newDate) => onChangeDate(newDate)}
                style={{width: '70%', marginTop: 30}}
              />
            </View>
          </Modal>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              onPress={() => openModal()}
              disabled={editable}
              style={{
                backgroundColor: 'transparent',
                position: 'absolute',
                width: '100%',
                height: '100%',
                marginTop: 6,
                alignItems: 'flex-end',
              }}>
              {iconright ? (
                <Icon
                  active
                  name={iconright}
                  style={{color: '#666', top: 5}}
                  onPress={onPressIcon}
                />
              ) : null}
            </TouchableOpacity>

            <Input
              {...input}
              type={type}
              disabled={true}
              placeholder={placeholder}
              keyboardType={keyboardType}
              maxLength={maxLength}
              secureTextEntry={secureTextEntry}
              style={{
                fontSize: 14,
                fontFamily: fonts.primary.normal,

                zIndex: -100,
              }}
            />
          </View>
        </Item>
      )}

      {hasError || customError ? (
        <Text style={styles.errorDesc}>{error || customError}</Text>
      ) : null}
    </View>
  );
};

export const renderFieldPicker = ({
  input,
  label,
  pickerSelected,
  onValueChange,
  customError,
  enabled,
  data,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <SafeAreaView style={{width: '80%', marginTop: 15, alignSelf: 'center'}}>
      <Label
        style={{
          fontSize: 16,
          color: customError || hasError ? 'red' : 'black',
          alignSelf: 'flex-start',
          marginBottom: 5,
          fontFamily: fonts.primary.bold,
        }}>
        {label}
      </Label>
      <View
        style={{
          backgroundColor: 'white',
          height: 40,
          borderColor: !enabled
            ? 'rgba(0, 0, 0, 0.5)'
            : customError || hasError
            ? 'red'
            : BAF_COLOR_BLUE,
          borderWidth: 1,
          borderRadius: 5,
          paddingRight: 10,
        }}>
        {Platform.OS == 'android' ? (
          <PickerAndroid
            {...input}
            renderHeader={(backAction) => (
              <Header>
                <Left>
                  <Button transparent onPress={backAction}>
                    <Icon name="arrow-back" style={{color: 'black'}} />
                  </Button>
                </Left>
                <Body style={{flex: 3}}>
                  <Title>{label}</Title>
                </Body>
                <Right />
              </Header>
            )}
            note={enabled ? false : true}
            mode="dropdown"
            style={{
              marginTop: Platform.OS == 'android' ? -6 : -4,

              fontFamily: fonts.primary.normal,
              color: enabled ? 'rgba(0, 0, 0, 0.8)' : 'rgba(0, 0, 0, 0.5)',
            }}
            enabled={enabled}
            selectedValue={pickerSelected}
            onValueChange={onValueChange}>
            {data}
          </PickerAndroid>
        ) : (
          <Picker
            {...input}
            renderHeader={(backAction) => (
              <Header>
                <Left>
                  <Button transparent onPress={backAction}>
                    <Icon name="arrow-back" style={{color: 'black'}} />
                  </Button>
                </Left>
                <Body style={{flex: 3}}>
                  <Title>{label}</Title>
                </Body>
                <Right />
              </Header>
            )}
            note={enabled ? false : true}
            mode="dropdown"
            style={{
              marginTop: Platform.OS == 'android' ? -6 : -4,
              fontSize: 14,
              fontFamily: fonts.primary.normal,
              color: enabled ? 'rgba(0, 0, 0, 0.8)' : 'rgba(0, 0, 0, 0.5)',
            }}
            enabled={enabled}
            selectedValue={pickerSelected}
            onValueChange={onValueChange}>
            {data}
          </Picker>
        )}
      </View>
      {hasError || customError ? (
        <Text style={styles.errorDesc}>{error || customError}</Text>
      ) : null}
    </SafeAreaView>
  );
};

export const renderFieldHidden = ({input, type, meta: {touched, error}}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '100%'}}>
      <Item style={{display: 'none'}}>
        <Input {...input} ype={type} />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

class RequestBKPB extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Pengambilan BPKB',
      data: [{Code: '', Descr: 'Pilih'}],
      dateTime: [{TimeInterval: '', Descr: 'Pilih Jam'}],
      isAvailable: '',
      kodeCabang: '',
      namaCabang: '',
      pickerKontrakSelected: 'asd',
      pickerTimeSelected: '',
      pickerDateSelected: '',
      isConnected: true,
      visible: false,
      show: false,
      BirthDate: new Date(),
      alertShowed: false,
      userToken: false,
      customErrorTanggal: false,
      customErrorJam: false,
      pickerTimeEnabled: false,
      tanggalDisabled: true,
      dataMergeBPKB: '',
      pickerNokontrak : true,
      ContractStat: '',
      bpkblogID : '',
      dataSuccessRequestBPKB : '',
      errorTanggal: false,
    };
  }

  componentDidMount = async () => {
    const {getBPKB, getdetailBpkb} = this.props;
    
    var array = [{Code: '', Descr: 'Pilih'}];

    analytics().logScreenView({
      screen_name: 'screenPengambilanBPKB',
      screen_class: 'screenPengambilanBPKB',
    });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

 

    const detailUserNonParse = await AsyncStorage.getItem('detailUser');

    const userToken = await AsyncStorage.getItem('userToken');

    if (userToken) {
      this.setState({userToken: userToken  });
    }
    
    var data = this.props.route.params.params;
    if (detailUserNonParse) {
      const detailUserParse = JSON.parse(detailUserNonParse);
      if (detailUserParse.CustID && data > 0 ) {
        getdetailBpkb({BPKBLogID : data}, userToken)
      } else if(detailUserParse.CustID && data == false){
        getBPKB({Custid: detailUserParse.CustID}, userToken);
      }

      this.setState({detailUser: detailUserParse, bpkblogID : data });
    }

    this.checkInternet();

    this.setState({
      data: array,
    });
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      getBPKB,
      getdetailBpkb,
      rescheduleBpkb,
      getBPKBResult,
      submitBPKBResult,
      getBPKBError,
      timeSlotBPKBResult,
      timeSlotBPKBError,
      timeSlotBPKBLoading,
      checkAvailBookResult,
      checkAvailBookError,
      checkAvailBookLoading,
      submitBPKBError,
      checkHoliday,
      checkHolidayLoading,
      checkHolidayError,
      checkHolidayResult,
      timeSlotBPKB,
      formRequestBKPB,
      refreshTokenResult,
      refreshTokenError,
      getdetailBpkbResult,
      getdetailBpkbError,
      rescheduleBpkbError,
      rescheduleBpkbResult,
      rescheduleBpkbLoading,
      detailUserResult,

    } = this.props;

    const {detailUser, dataMergeBPKB} = this.state;

    if (
      detailUserResult &&
      prevProps.detailUserResult !== detailUserResult
    ) {
      const userToken = await AsyncStorage.getItem('userToken');
      var data = this.props.route.params.params;
      getdetailBpkb({BPKBLogID : data}, userToken)
      
    }

    
    if (
      timeSlotBPKBLoading &&
      prevProps.timeSlotBPKBLoading !== timeSlotBPKBLoading
    ) {
      var dateTimeUpdate = [{TimeInterval: '', Descr: 'Loading...'}];
      this.setState({dateTime: dateTimeUpdate});
    }

    // if (
    //   checkHolidayLoading &&
    //   prevProps.checkHolidayLoading !== checkHolidayLoading
    // ) {
    //   console.log('Check holiday on the way...');
    // }

    if (
      checkHolidayError &&
      prevProps.checkHolidayError !== checkHolidayError
    ) {
      //Ketika hit error, akan hit api check holiday lagi
      // checkHoliday({ Date: this.state.pickerDateSelected });
      console.log(
        'checkHolidayError Error Message ' + checkHolidayError.message,
      );
      // code ini di comment karena ketika hit check holiday tidak memakai token lagi

      // const data = await jsonParse(checkHolidayError.message);
      // if (data) {
      //   const refreshToken = data.result.Refreshtoken;
      //   if (data.message == 'A new token is created') {
      //     await handleRefreshToken(refreshToken);
      //     this.setState({ userToken: refreshToken });
      //     checkHoliday({ Date: this.state.pickerDateSelected }, refreshToken);
      //   }
      // } else {
      //   this.setState({
      //     alertShowed: true,
      //     alertMessage: detailUserError.message,
      //     alertTitle: 'Gagal',
      //     alertType: 'error',
      //     alertDoneText: 'OK',
      //   });
      // }
    }

    if (
      checkHolidayResult &&
      prevProps.checkHolidayResult !== checkHolidayResult
    ) {
      console.log("checkHolidayResult " , checkHolidayResult);
      if (checkHolidayResult.HolidaySchmDID == '0') {
        
        timeSlotBPKB({
          DateReq: this.state.pickerDateSelected,
          OfficeCode: formRequestBKPB.values.kodeCabang,
        });

        this.setState({
          customErrorTanggal: false,
          kodeCabang: formRequestBKPB.values.kodeCabang,
          namaCabang: formRequestBKPB.values.cabang,
          errorTanggal: false,
        });
      } else {
        this.setState({
          customErrorTanggal:
            'Tanggal yang kamu pilih adalah hari libur, silahkan pilih tanggal lain',
            errorTanggal: true,
        });
        // this.props.updateField('formRequestBKPB', 'tanggal', '');
        // this.props.updateField('formRequestBKPB', 'jam', '');
        
      }
    }

    if (
      timeSlotBPKBResult &&
      prevProps.timeSlotBPKBResult !== timeSlotBPKBResult
    ) {
      var dateTimeUpdate = [{TimeInterval: '', Descr: 'Pilih Jam'}];
   

      for (var i = 0; i < timeSlotBPKBResult.AllData.length; i++) {
        dateTimeUpdate.push({
          TimeInterval:
            timeSlotBPKBResult.AllData[i].IntervalTimeID +
            '^' +
            timeSlotBPKBResult.AllData[i].IntervalTime,
          Descr: timeSlotBPKBResult.AllData[i].IntervalTime,
        });
      }

      this.setState({dateTime: dateTimeUpdate, pickerTimeEnabled: true});
    }

    if (
      checkAvailBookError &&
      prevProps.checkAvailBookError !== checkAvailBookError
    ) {
      const data = await jsonParse(checkAvailBookError.message);
      if (data) {
        const refreshToken = data.result.Refreshtoken;
        if (data.message == 'A new token is created') {
          await handleRefreshToken(refreshToken);
          this.setState({userToken: refreshToken});
          // detailUser({Userid: this.state.userData.Userid}, refreshToken);
        }
      } else {
        this.setState({
          alertShowed: true,
          alertMessage: checkAvailBookError.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
          pickerTimeSelected: '',
        });
        // console.log(
        //   'checkAvailBook Error Message ' + checkAvailBookError.message,
        // );
      }
    }

    if (
      checkAvailBookResult &&
      prevProps.checkAvailBookResult !== checkAvailBookResult
    ) {
      console.log(checkAvailBookResult);
    }

    if (getBPKBError && prevProps.getBPKBError !== getBPKBError) {
      const data = await jsonParse(getBPKBError.message);
      if (data) {
        this.validateRefreshToken(data);
      } else {
        this.setState({
          alertFor: 'getBPKBError',
          alertShowed: true,
          alertMessage:
            getBPKBError.message == 'Network request failed'
              ? 'Gagal terhubung ke server'
              : getBPKBError.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
    }

    if (getBPKBResult && prevProps.getBPKBResult !== getBPKBResult) {
      var array = [{Code: '', Descr: 'Pilih'}];
      for (var i = 0; i < getBPKBResult.AllData.length; i++) {
        // Terjadi perubahan response untuk sprint17, sedangkan proses masih di sprint16.
        // Sehingga saya buat seperti ini in case ini up dan backend belum update
        const noKontrak = getBPKBResult.AllData[i].NoKontrak
          ? getBPKBResult.AllData[i].NoKontrak
          : getBPKBResult.AllData[i].ContractNo;

        const namaKonsumen = getBPKBResult.AllData[i].NamaKonsumen
          ? getBPKBResult.AllData[i].NamaKonsumen
          : getBPKBResult.AllData[i].CustName;

        array.push({
          Code:
            noKontrak +
            '^' +
            namaKonsumen +
            '^' +
            'Lunas' +
            '^' +
            getBPKBResult.AllData[i].NamaCabang +
            '^' +
            getBPKBResult.AllData[i].KodeCabang +
            '^' +
            getBPKBResult.AllData[i].ContractStat + 
            '^' +
            getBPKBResult.AllData[i].BPKBNo,
          Descr: noKontrak,
        });
      }
      this.setState({
        data: array,
      });
    }
    if (submitBPKBResult && prevProps.submitBPKBResult !== submitBPKBResult) {
      this.props.navigation.navigate('successRequestBPKB',{ successRequestData: submitBPKBResult})
    }

    if (submitBPKBError && prevProps.submitBPKBError !== submitBPKBError) {
  
      const data = await jsonParse(submitBPKBError.message);
      if (data) {
        this.validateRefreshToken(data);
      } else {
        this.setState({
          alertShowed: true,
          alertMessage: submitBPKBError.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
    }

    if (
      timeSlotBPKBError &&
      prevProps.timeSlotBPKBError !== timeSlotBPKBError
    ) {
      var dateTimeSlotError = [{TimeInterval: '', Descr: 'Pilih Jam'}];
      //code handleRefresh token di bawah di hapus karena api timeslot tidak memakai token
      setTimeout(() => {
        this.setState({
          alertFor: 'checkTime',
          alertShowed: true,
          alertMessage:
            timeSlotBPKBError.message == 'Network request failed'
              ? 'Gagal terhubung ke server'
              : 'Pilihan jam tidak tersedia',
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
          dateTime: dateTimeSlotError,
        });
      }, 1000);

      this.props.updateField('formRequestBKPB', 'tanggal', '');
    }

    if (
      getdetailBpkbError !== null &&
      prevProps.getdetailBpkbError !== getdetailBpkbError
    ) {
      const data = await jsonParse(getdetailBpkbError.message);
      if (data) {
        this.validateRefreshToken(data);
      }
    }

    if (
      getdetailBpkbResult !== null &&
      prevProps.getdetailBpkbResult !== getdetailBpkbResult
    ) {
      var array = [];
      var arrayTime = [];
      array.push({
        Code: getdetailBpkbResult.ContractNo,
        Descr: getdetailBpkbResult.ContractNo,
      });
      arrayTime.push({
        TimeInterval: '',
        Descr: getdetailBpkbResult.IntervalTime,
      });
      this.setState({pickerNokontrak : false, data: array, tanggalDisabled: false, dateTime: arrayTime})
      this.props.updateField( 'formRequestBKPB','nomorKontrak', getdetailBpkbResult.ContractNo);
      this.props.updateField( 'formRequestBKPB','nama', getdetailBpkbResult.CustName);
      this.props.updateField( 'formRequestBKPB','statusKontrak', getdetailBpkbResult.ContractStat);
      this.props.updateField( 'formRequestBKPB','cabang', getdetailBpkbResult.NamaCabangPengambilan);
      this.props.updateField( 'formRequestBKPB','kodeCabang', getdetailBpkbResult.KodeCabangPengambilan);
      this.props.updateField( 'formRequestBKPB','tanggal', getdetailBpkbResult.TglPengambilan);
      this.props.updateField( 'formRequestBKPB','getTanggal', getdetailBpkbResult.TglPengambilan);
    }

    if (
      rescheduleBpkbError !== null &&
      prevProps.rescheduleBpkbError !== rescheduleBpkbError
    ) {
      const data = await jsonParse(rescheduleBpkbError.message);
      if (data) {
        this.validateRefreshToken(data);
      }
    }

    if (
      rescheduleBpkbResult !== null &&
      prevProps.rescheduleBpkbResult !== rescheduleBpkbResult
    ) {
      this.props.navigation.navigate('successRequestBPKB',{ successRequestData: rescheduleBpkbResult})
    }


    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      console.log('Error Refresh Token ', refreshTokenError.message);
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      const newToken = refreshTokenResult.Refreshtoken;
      await handleRefreshToken(newToken);
      this.setState({userToken: newToken});
      if (getBPKBError) {
        getBPKB(
          {Custid: this.state.detailUser.CustID},
          newToken,
        );
      } else if (getdetailBpkbError) {
        getdetailBpkb({BPKBLogID : this.state.bpkblogID}, newToken)
      } else if (submitBPKBError) {
        submitBPKB(
          {
            IntervalTimeID: dataMerge.IntervalTimeID,
            TanggalAmbil: dataMerge.TglPermohonan,
            BPKBNo: dataMerge.BPKBNo,
            ContractNo: dataMerge.ContractNo,
            ContractStat: dataMerge.ContractStat,
            Cabang: dataMerge.Cabang,
          },
          newToken,
        );
      } else if(rescheduleBpkbError){
        rescheduleBpkb(
          {
            BPKBLogID: dataMergeBPKB.BPKBLogID,
            IntervalTimeID: dataMergeBPKB.IntervalTimeID,
            TglPermohonan: dataMergeBPKB.TglPermohonan,
          },
          newToken,
        )

      }
    }
  };

  // this function is validate token, if response api expired token this function trigered
  validateRefreshToken = (data) => {
    if (data) {
      if (data.message == MESSAGE_TOKEN_EXP) {
        this.props.refreshToken(this.state.userToken);
      } else if (data.message == MESSAGE_TOKEN_INVALID) {
          this.handleInvalid();
      }
    }
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
        onPressNegativeButton={this.handleNegativeButtonAlert}
      />
    );
  };

  handleBackButton = async () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  goBack = async () => {
    this.props.resetForm('formRequestBKPB');
    this.props.navigation.goBack();
  };

  handleInvalid = async () => {
    await AsyncStorage.removeItem('userToken');
    await AsyncStorage.removeItem('userData');
    await AsyncStorage.removeItem('detailUser');
    await AsyncStorage.removeItem('activeAgreement');
    this.setState({
      alertFor: 'invalidToken',
      alertShowed: true,
      alertMessage: 'Sesi login Anda sudah habis',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  openDatePicker = () => {
    this.setState({visible: true});
  };
  toggleModal = () => {
    this.setState({visible: false});
  };

  openTimePicker = () => {
    this.setState({show: true});
  };

  closeTimePicker = () => {
    this.setState({show: false});
  };

  onChangeDate = (newDate) => {
    const {formRequestBKPB, checkHoliday} = this.props;
    if (newDate === 'today') {
      var date = moment(new Date()).format('YYYY-MM-DD');

      this.setState({
        visible: Platform.OS === 'ios' ? true : false,
        pickerDateSelected: date,
        pickerTimeEnabled: false,
      });

      checkHoliday({Date: date});
      this.props.updateField('formRequestBKPB', 'tanggal', date);
    } else {
      var date = moment(newDate.nativeEvent.timestamp).format('YYYY-MM-DD');

      this.setState({
        visible: Platform.OS === 'ios' ? true : false,
        pickerDateSelected: date,
        pickerTimeEnabled: false,
      });

      checkHoliday({Date: date});
      this.props.updateField('formRequestBKPB', 'tanggal', date);
    }
  };

  onChangeTime = (newTime) => {
    const {formRequestBKPB, checkAvailBook} = this.props;
    this.setState({
      visible: Platform.OS === 'ios' ? true : false,
    });

    if (
      moment().format('YYYY-MM-DD').toString() === this.state.pickerDateSelected
    ) {
      if (
        parseInt(moment().format('HHmm')) >=
        parseInt(
          substringColone(substringStrip(substringSecondCircum(newTime))),
        )
      ) {
        this.setState({
          alertShowed: true,
          alertMessage: 'Waktu sudah tidak tersedia',
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      } else {
        checkAvailBook(
          {
            Date: this.state.pickerDateSelected,
            TimeInterval: substringCircum(newTime),
            OfficeCode: this.state.namaCabang + ' ' + this.state.kodeCabang,
          },
          this.state.userToken,
        );

        this.setState({pickerTimeSelected: newTime});
        this.props.updateField('formRequestBKPB', 'jam', newTime);
      }
    } else {
      checkAvailBook(
        {
          Date: this.state.pickerDateSelected,
          TimeInterval: substringCircum(newTime),
          OfficeCode: this.state.namaCabang + ' ' + this.state.kodeCabang,
        },
        this.state.userToken,
      );

      this.setState({pickerTimeSelected: newTime});
      this.props.updateField('formRequestBKPB', 'jam', newTime);
    }
  };

  kirimHandle = async (value) => {
    const {submitBPKB, rescheduleBpkb} = this.props;
    const userToken = await AsyncStorage.getItem('userToken');
    var data = this.props.route.params.params;

    NetInfo.fetch().then(async (state) => {
      if (state.isConnected) {
        
        const dataMerge = {
          IntervalTimeID: substringCircum(value.jam),
          TglPermohonan: value.tanggal,
          ContractNo: value.nomorKontrak,
          ContractStat: value.contractStat,
          Cabang: value.cabang + ' ' + value.kodeCabang,
          BPKBLogID: data,
          TglPermohonan : value.tanggal,
          BPKBNo: value.BPKBNo,

        };
        if (data > 0) {
          this.setState({ dataMergeBPKB: dataMerge });
          rescheduleBpkb(
            {
              BPKBLogID: dataMerge.BPKBLogID,
              IntervalTimeID: dataMerge.IntervalTimeID,
              TglPermohonan: dataMerge.TglPermohonan,
            },
            userToken,
          )
        } else {
          submitBPKB(
            {
              IntervalTimeID: dataMerge.IntervalTimeID,
              TanggalAmbil: dataMerge.TglPermohonan,
              BPKBNo: dataMerge.BPKBNo,
              ContractNo: dataMerge.ContractNo,
              ContractStat: dataMerge.ContractStat,
              Cabang: dataMerge.Cabang,
            },
            userToken,
          );
          this.setState({ dataMergeBPKB: dataMerge });
        }

      } else {
        this.showToastNoInternet();
      }
    });
  };

  resetField = () => {
    this.setState({
      pickerTimeSelected: '',
    });
  };

  onKontrakChange = async (value) => {
    this.setState({
      pickerKontrakSelected: value,
      pickerTimeEnabled: false,
      customErrorTanggal: false,
      tanggalDisabled: value == '' ? true : false, //this ternary will disabled tanggal when kontrak changed
    });

    this.props.resetForm('formRequestBKPB'); //change kontrak will reset all form in screen

    this.props.updateField(
      'formRequestBKPB',
      'nomorKontrak',
      substringCircum(value),
    );
    this.props.updateField(
      'formRequestBKPB',
      'nama',
      substringSecondCircum(value),
    );
    this.props.updateField(
      'formRequestBKPB',
      'statusKontrak',
      substringThirdCircum(value),
    );
    this.props.updateField(
      'formRequestBKPB',
      'cabang',
      substringFourthCircum(value),
    );
    this.props.updateField(
      'formRequestBKPB',
      'kodeCabang',
      substringFifthCircum(value),
    );

    this.props.updateField(
      'formRequestBKPB',
      'contractStat',
      substringSixCircum(value),
    );

    this.props.updateField(
      'formRequestBKPB',
      'BPKBNo',
      substringSevenCircum(value),
    );
    this.resetField();
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  handlePositiveButtonAlert = () => {
    if (this.state.alertFor === 'submitBPKBResult') {
      this.props.navigation.navigate('Home');
    } else if (this.state.alertFor === 'rescheduleBpkbResult') {
      this.props.navigation.navigate('Home');
    } else if (this.state.alertFor === 'invalidToken') {
      this.props.navigation.navigate('Login');
      this.setState({alertFor: null});
    }
    this.setState({alertShowed: false, alertFor: false});
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false, alertFor: false});
  };

  render() {
    const {
      handleSubmit,
      submitting,
      submitBPKBLoading,
      checkAvailBookLoading,
      timeSlotBPKBLoading,
      rescheduleBpkbLoading,
    } = this.props;
    const list = requestData.requestBPKP.map((result, key) => {
      if (result.lenght !== 0) {
        return (
          <ListCard
            title={result.title}
            text={result.subtitle}
            type={result.type}
            content={result.content}
            iconRight="arrow-forward"
          />
        );
      } else {
        <View>
          <Text>data not found</Text>
        </View>;
      }
    });
    return (
      <Container style={styles.container}>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}

        <Toast
          ref="toast"
          style={styles.toast}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />

        <Spinner
          visible={submitBPKBLoading || rescheduleBpkbLoading ? true : false} // add this ternary to loading check avail booking
          textContent={'Sedang memproses...'}
          overlayColor="rgba(0, 0, 0, 0.80)"
          textStyle={{color: '#FFF'}}
        />

        <Content disableKBDismissScroll={false} style={styles.content}>
          <View style={styles.formWrapper}>
            <Field
              name="nomorKontrak"
              type="text"
              component={renderFieldPicker}
              pickerSelected={this.state.pickerKontrakSelected}
              maxLength={16}
              editable={true}
              enabled={this.state.pickerNokontrak}
              data={this.state.data.map((pickerMerkItem, index) => (
                <Picker.Item
                  label={pickerMerkItem.Descr}
                  value={pickerMerkItem.Code}
                />
              ))}
              label="Pilih Nomor Kontrak"
              keyboardType="number-pad"
              onValueChange={(newValue) => this.onKontrakChange(newValue)}
            />

            <Field
              name="statusKontrak"
              type="text"
              component={renderField}
              maxLength={16}
              editable={false}
              label="Status Kontrak"
              keyboardType="number-pad"
            />
            <Field
              name="nama"
              type="text"
              component={renderField}
              maxLength={16}
              editable={false}
              label="Nama"
              keyboardType="number-pad"
            />
            <Field
              name="cabang"
              type="text"
              component={renderField}
              maxLength={16}
              editable={false}
              label="Kantor BAF"
              keyboardType="number-pad"
            />
            <Field
              name="tanggal"
              type="text"
              date={this.state.BirthDate}
              component={renderFieldDatePicker} //render ganti ke input tanggal cek register screen
              placeholder="Pilih Tanggal"
              onChangeDate={this.onChangeDate}
              customError={this.state.customErrorTanggal}
              openModal={this.openDatePicker}
              closeModal={this.toggleModal}
              maxLength={16}
              iconright="calendar"
              visible={this.state.visible}
              editable={this.state.tanggalDisabled}
              label="Tanggal Pengambilan Dokumen"
              keyboardType="number-pad"
              formatDate="YYYY-MM-DD"
              modeDate="date"
            />

            <Field
              name="jam"
              type="text"
              component={renderFieldPicker}
              pickerSelected={this.state.pickerTimeSelected}
              placeholder="Pilih Jam"
              data={this.state.dateTime.map((pickerMerkItem, index) => (
                <Picker.Item
                  label={pickerMerkItem.Descr}
                  value={pickerMerkItem.TimeInterval}
                />
              ))}
              onValueChange={(newValue) => this.onChangeTime(newValue)}
              customError={this.state.customErrorTanggal}
              label="Jam Pengambilan"
              editable={true}
              enabled={this.state.pickerTimeEnabled}
            />

            <Field
              name="getTanggal"
              type="hidden"
              component={renderFieldHidden}
            />
            <Field
              name="contractStat"
              type="hidden"
              component={renderFieldHidden}
            />

            <Field
              name="BPKBNo"
              type="hidden"
              component={renderFieldHidden}
            />

          </View>
          {/* <View style={styles.line} />

          <View style={{marginTop: 10}}>{list}</View> */}

          <View style={{marginBottom: 30}} />

          <View style={{backgroundColor: 'white'}}>
            <TouchableOpacity
              style={styles.btnKirim}
              onPress={handleSubmit(this.kirimHandle)}
              // disabled={submitting}
              disabled={this.state.errorTanggal  ? true : false && submitting ? true : false}>
              <Text style={styles.text}>Kirim</Text>
            </TouchableOpacity>
          </View>
        </Content>
        {this.showAlert()}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    formRequestBKPB: state.form.formRequestBKPB,
    getBPKBResult: state.getBPKB.result,
    getBPKBLoading: state.getBPKB.loading,
    getBPKBError: state.getBPKB.error,
    submitBPKBResult: state.submitBPKB.result,
    submitBPKBLoading: state.submitBPKB.loading,
    submitBPKBError: state.submitBPKB.error,
    timeSlotBPKBLoading: state.timeSlotBPKB.loading,
    timeSlotBPKBResult: state.timeSlotBPKB.result,
    timeSlotBPKBError: state.timeSlotBPKB.error,
    checkAvailBookLoading: state.checkAvailBook.loading,
    checkAvailBookResult: state.checkAvailBook.result,
    checkAvailBookError: state.checkAvailBook.error,
    checkHolidayLoading: state.checkHoliday.loading,
    checkHolidayResult: state.checkHoliday.result,
    checkHolidayError: state.checkHoliday.error,
    refreshTokenResult: state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
    getdetailBpkbResult: state.getdetailBpkb.result,
    getdetailBpkbError: state.getdetailBpkb.error,
    rescheduleBpkbLoading: state.rescheduleBpkb.loading,
    rescheduleBpkbResult: state.rescheduleBpkb.result,
    rescheduleBpkbError: state.rescheduleBpkb.error,
    detailUserResult: state.detailUser.result,
    detailUserError: state.detailUser.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getBPKB,
      timeSlotBPKB,
      checkAvailBook,
      checkHoliday,
      submitBPKB,
      refreshToken,
      getdetailBpkb,
      rescheduleBpkb,

      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

RequestBKPB = reduxForm({
  form: 'formRequestBKPB',
  enableReinitialize: true,
  validate: FormRequestBPKBValidate,
})(RequestBKPB);

export default connect(mapStateToProps, matchDispatchToProps)(RequestBKPB);
