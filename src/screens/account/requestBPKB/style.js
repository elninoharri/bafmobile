import {StyleSheet, Dimensions, PixelRatio, Platform} from 'react-native';
import {fonts} from '../../../utils/fonts';
const {width} = Dimensions.get('window');
const style =
  Platform.OS === 'android'
    ? {
        marginVertical: 10,
        left: -15,
        width: '105%',
      }
    : {
        zIndex: 1,
        marginVertical: 10,
        left: -15,
        width: '105%',
      };
//zIndex di android membuat autocomplete suggestion tidak bisa di-klik.
//Namun karena sepertinya kasus ini unik di android, saya coba bikin seperti ini.

export const styles = StyleSheet.create({
  input: {
    marginVertical: 10,
    left: -15,
    width: '105%',
  },
  inputAutocomplete: style,
  row: {alignSelf: 'center'},

  dropDownStyle: {
    width: width * 0.85,
    fontSize: 10,
    marginTop: 10,
    marginLeft: Platform.OS == 'android' ? -6 : -12,
    color: 'black',
  },
  subContent: {
    width: 20,

    alignItems: 'center',
    borderRadius: 10,
    height: 20,
    marginRight: 5,
    marginLeft: -20,
  },
  textDefault: {
    fontSize: 14,
    lineHeight: 20,
    fontFamily: fonts.primary.normal,
    color: 'rgba(0, 0, 0, 0.8)',
    textAlign: 'justify',
  },

  dropDownStyleNonEdit: {
    width: width * 0.85,
    fontSize: 10,
    marginTop: 10,
    marginLeft: Platform.OS == 'android' ? -6 : -12,
    color: '#A6AAB4',
  },

  containerAuto: {
    backgroundColor: '#fcfdff',
    flex: 1,
    paddingTop: 25,
  },
  line: {
    backgroundColor: '#F4F4F4',
    width: '100%',
    height: 7,
    marginTop: 40,
  },
  container: {
    flex: 1,
  },
  content: {},
  formWrapper: {
    backgroundColor: 'white',
    paddingBottom: 40,
  },

  formWrapper: {},
  placeholderDate: {
    fontFamily: fonts.primary.normal,
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.5)',
  },
  label: {
    left: 15,
    zIndex: 0,
    marginBottom: -8,
    color: '#002f5f',
    fontFamily: fonts.primary.bold,
    fontSize:
      Platform.OS == 'android'
        ? Dimensions.get('window').width > 360
          ? 18
          : 16
        : Dimensions.get('window').width * PixelRatio.get() > 750
        ? 18
        : 16,
  },

  btnKirim: {
    alignSelf: 'center',
    width: '70%',
    height: 40,
    borderRadius: 4,
    backgroundColor: '#002f5f',
    justifyContent: 'center',
    marginTop: 35,
    marginBottom: 29,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  text: {
    textAlign: 'center',
    fontSize: 14,
    color: 'white',
    fontFamily: fonts.primary.bold,
  },

  errorDesc: {
    color: 'red',
    alignSelf: 'flex-start',
    textAlign: 'left',
    fontSize: 10,
    marginBottom: -12,
    fontFamily: fonts.primary.normal,
    // marginLeft: 15,
  },
  toast: {
    marginTop:
      Platform.OS != 'android'
        ? Dimensions.get('window').width * PixelRatio.get() > 750
          ? 44
          : 20
        : 0,
    backgroundColor: '#FFE6E9',
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'red',
    justifyContent: 'center',
    paddingLeft: 50,
    height: 50,
    borderRadius: 0,
  },

  validDesc: {
    color: 'green',
    textAlign: 'left',
    fontSize: 10,
    fontFamily: fonts.primary.normal,
  },
  // icon: {
  //   fontSize: 20,
  //   color: 'red',
  //   marginRight: 5,
  //   marginTop: -5,
  //   marginLeft: -10,
  // },
});
