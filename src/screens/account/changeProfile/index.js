import React, {Component} from 'react';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import {reduxForm, Field, change, reset} from 'redux-form';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import Autocomplete from 'native-base-autocomplete';
import moment from 'moment';
import 'moment/locale/id';
import ImageResizer from 'react-native-image-resizer';
import ImagePicker from 'react-native-image-picker';
import {uploadOssFile} from '../../../utils/uploadImage';
import {getCity, editUser, refreshToken} from '../../../actions';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Spinner from 'react-native-loading-spinner-overlay';
import {FormChangeProfileValidate} from '../../../validates/FormChangeProfileValidate';
import Toast, {DURATION} from 'react-native-easy-toast';
import {
  ERROR_AUTH,
  MESSAGE_TOKEN_EXP,
  TOKEN_EXP,
  MESSAGE_TOKEN_INVALID,
} from '../../../utils/constant';
import {jsonParse, handleRefreshToken} from '../../../utils/utilization';

import {
  TouchableOpacity,
  Text,
  Alert,
  View,
  Image,
  BackHandler,
  Platform,
  Keyboard,
  Dimensions,
  PixelRatio,
} from 'react-native';
import {
  Container,
  Picker,
  Label,
  Input,
  Item,
  Row,
  Form,
  Icon,
  Content,
  Right,
  ListItem,
  Header,
  Left,
  Button,
  Body,
  Title,
} from 'native-base';

import {styles} from './style';
import {fonts} from '../../../utils/fonts';
import analytics from '@react-native-firebase/analytics';

export const renderField = ({
  input,
  type,
  label,
  icon,
  valueInput,
  iconright,
  placeholder,
  editable,
  keyboardType,
  onChangeValue,
  maxLength,
  placeholderTextColor,
  secureTextEntry,
  onPressIcon,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }

  return (
    <View style={styles.input}>
      <Label style={styles.label}>{label}</Label>
      <Item
        fixedLabel
        style={{
          borderColor: hasError ? 'red' : '#666',
          height: 40,
        }}>
        <Input
          {...input}
          type={type}
          editable={editable}
          placeholder={placeholder}
          keyboardType={keyboardType}
          // onChange={onChangeValue}
          maxLength={maxLength}
          placeholderTextColor={placeholderTextColor ? 'black' : '#A6AAB4'}
          secureTextEntry={secureTextEntry}
          style={{
            fontSize: 14,
            marginLeft: -5,
            color: editable ? 'black' : '#A6AAB4',
            fontFamily: fonts.primary.normal,
          }}
        />
      </Item>
      {hasError ? <Text style={styles.errorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderField2 = ({
  input,
  type,
  label,
  icon,
  valueInput,
  optionList,
  iconright,
  placeholder,
  editable,
  keyboardType,
  onChangeValue,
  maxLength,
  placeholderTextColor,
  secureTextEntry,
  onPressIcon,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }

  return (
    <View style={styles.input}>
      <Label style={styles.label}>{label}</Label>
      <Item
        fixedLabel
        style={{
          borderColor: hasError ? 'red' : '#666',
          height: 40,
        }}>
        <Form>
          <Picker
            {...input}
            renderHeader={(backAction) => (
              <Header>
                <Left>
                  <Button transparent onPress={backAction}>
                    <Icon
                      name="arrow-back"
                      style={{color: 'black', fontSize: 14, marginLeft: 12}}
                    />
                  </Button>
                </Left>
                <Body style={{flex: 3}}>
                  <Title>{label}</Title>
                </Body>
                <Right />
              </Header>
            )}
            note={editable ? false : true}
            enabled={editable}
            mode="dropdown"
            style={
              editable ? styles.dropDownStyle : styles.dropDownStyleNonEdit
            }
            selectedValue={valueInput}
            onValueChange={onChangeValue}>
            {optionList}
          </Picker>
        </Form>
      </Item>
      {hasError ? <Text style={styles.errorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderField3 = ({
  input,
  dataQuery,
  dataCity,
  onPresscity,
  label,
  selectedCity,
  hideResults,
  renderItem,
  icon,
  valueInput,
  iconright,
  placeholder,
  editable,
  keyboardType,
  onChangeValue,
  maxLength,
  placeholderTextColor,
  secureTextEntry,
  onPressIcon,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }

  return (
    <View style={styles.inputAutocomplete}>
      <Label style={styles.label}>{label}</Label>

      <Form style={{marginLeft: 10}}>
        <Autocomplete
          style={{
            fontSize: 14,
            borderBottomWidth: 1,
            borderColor: '#666',
            height: 40,
          }}
          autoCapitalize="none"
          autoCorrect={false}
          data={dataCity}
          defaultValue={dataQuery}
          hideResults={hideResults}
          onChangeText={onChangeValue}
          placeholder={'Masukkan Nama Kota'}
          renderItem={renderItem}
        />
      </Form>
    </View>
  );
};

class ChangeProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Ubah Profil',
      disableButton: false,
      showList: false,
      userToken: false,
      alertFor: false,
      alertShowed: false,
      isConnected: false,
      query: '',
      prevUri: '',
      IdentityUser: '',
      dataMerge: false,
      uri: '',
      cities: [
        {
          City: '',
        },
      ],
      selectedCities: null,
      detailUserParam: false,

      pickerGenderItem: [
        {Code: '', Descr: 'Silahkan Pilih'},
        {Code: 'PRIA', Descr: 'PRIA'},
        {Code: 'WANITA', Descr: 'WANITA'},
      ],
      pickerGenderSelected: '',
      showKeyboard: false,
      spinner: false,
    };
  }

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
    if (this.state.alertFor === 'editUserResult') {
      this.props.navigation.goBack();
      this.setState({alertFor: false});
    } else if (this.state.alertFor === 'invalidToken') {
      this.props.navigation.navigate('Login');
      this.setState({alertFor: null});
    }
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  findCity(query) {
    if (query === '') {
      return [];
    }
    // I tried to modify this function because filter will loop entire array when I just need two result for autofill
    const {cities} = this.state;
    const regex = new RegExp(`${query.trim()}`, 'i');
    const item = [];
    for (const city of cities) {
      if (city.City.search(regex) >= 0) {
        item.push(city);
      }
      if (item.length === 2) {
        return item;
      }
    }
    return item;
    // return cities.filter((city) => city.city_name.search(regex) >= 0);
  }

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.refs.toast.show(
          <View style={{flexDirection: 'row'}}>
            <Icon
              type="FontAwesome"
              name="exclamation-circle"
              style={{color: 'red', marginLeft: '-10%'}}
            />
            <Text
              style={{
                marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
                marginLeft: '3%',
                fontSize: 16,
                color: 'red',
              }}>
              Tidak Ada Koneksi Internet
            </Text>
          </View>,
          DURATION.LENGTH_LONG,
        );
      }
    });
  };

  componentDidMount = async () => {
    const {detailUserParam} = this.props.route.params;
    const {getCity, getCityResult} = this.props;

    analytics().logScreenView({
      screen_name: 'screenChangeProfile',
      screen_class: 'screenChangeProfile',
    });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this.checkInternet();
    const userData = await AsyncStorage.getItem('userData');
    const userToken = await AsyncStorage.getItem('userToken');
    if (userData) {
      const data = JSON.parse(userData);
      this.setState({IdentityUser: data.IdentityUser});
    }
    if (getCityResult) {
      this.setState({cities: getCityResult.AllData});
    } else {
      if (this.state.isConnected) {
        getCity();
      }
    }

    if (userToken) {
      this.setState({userToken: userToken});
    }

    if (detailUserParam) {
      this.props.updateField(
        'formChangeProfile',
        'fullName',
        detailUserParam.Username,
      );
      this.props.updateField('formChangeProfile', 'NIK', detailUserParam.NIK);
      this.props.updateField(
        'formChangeProfile',
        'custID',
        detailUserParam.CustID,
      );
      this.props.updateField(
        'formChangeProfile',
        'noHp',
        detailUserParam.Phoneno,
      );
      this.props.updateField(
        'formChangeProfile',
        'gender',
        detailUserParam.Gender,
      );
      this.props.updateField(
        'formChangeProfile',
        'alamat',
        detailUserParam.UsrAdr,
      );
      this.props.updateField(
        'formChangeProfile',
        'email',
        detailUserParam.Email,
      );
      this.props.updateField(
        'formChangeProfile',
        'birthDate',
        moment(detailUserParam.BirthDate).format('LL'),
      );
      this.props.updateField(
        'formChangeProfile',
        'city',
        detailUserParam.AreaUsr,
      );

      this.setState({
        detailUserParam: detailUserParam,
        pickerGenderSelected: detailUserParam.Gender,
        query: detailUserParam.AreaUsr ? detailUserParam.AreaUsr : '',
        prevUri:
          detailUserParam.UsrImg && detailUserParam.usrImg !== ''
            ? {uri: detailUserParam.UsrImg}
            : require('../../../../assets/img/thumb_profile.png'),
        uri:
          detailUserParam.UsrImg && detailUserParam.usrImg !== ''
            ? {uri: detailUserParam.UsrImg}
            : require('../../../../assets/img/thumb_profile.png'),
      });
    }
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  _keyboardDidShow = () => {
    this.setState({showKeyboard: true});
  };

  _keyboardDidHide = () => {
    this.setState({showKeyboard: false});
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      getCityResult,
      editUserResult,
      editUserError,
      refreshTokenResult,
      refreshTokenError,
    } = this.props;

    if (getCityResult && prevProps.getCityResult !== getCityResult) {
      this.setState({cities: getCityResult.AllData});
    }

    if (editUserResult && prevProps.editUserResult !== editUserResult) {
      this.setState({
        alertFor: 'editUserResult',
        alertShowed: true,
        alertMessage: 'Anda berhasil mengubah profil anda.',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
        spinner: false,
      });
    }

    if (editUserError && prevProps.editUserError !== editUserError) {
      const data = await jsonParse(editUserError.message); //di sini ngeparse sekaligus ngecek apakah data kiriman error berupa json

      if (data) {
        if (data.message == MESSAGE_TOKEN_EXP) {
          this.props.refreshToken(this.state.userToken);
        } else if (data.message == MESSAGE_TOKEN_INVALID) {
          this.handleInvalid();
        }
      } else {
        this.setState({
          alertShowed: true,
          alertMessage: editUserError.message.includes('Internal Server')
            ? 'Maaf, terdapat masalah pada jaringan Anda. \nSilahkan dicoba kembali.'
            : editUserError.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
          spinner: false,
        });
      }
    }

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      console.log(
        'Error Refresh Token Change Profile ',
        refreshTokenError.message,
      );
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      await handleRefreshToken(refreshTokenResult.Refreshtoken);
      this.setState({userToken: refreshTokenResult.Refreshtoken});
      this.props.editUser(
        this.state.dataMerge,
        refreshTokenResult.Refreshtoken,
      );
    }
  };

  onChangeText = async (newValue) => {
    this.setState({query: newValue});
  };

  onGenderChange = async (newValue) => {
    this.setState({pickerGenderSelected: newValue});
    this.props.updateField('formChangeProfile', 'gender', newValue);
  };

  goBack = async () => {
    this.props.navigation.goBack();
    this.props.resetForm('formChangeProfile');
  };

  handleInvalid = async () => {
    await AsyncStorage.removeItem('userToken');
    await AsyncStorage.removeItem('userData');
    await AsyncStorage.removeItem('detailUser');
    await AsyncStorage.removeItem('activeAgreement');
    this.setState({
      alertFor: 'invalidToken',
      alertShowed: true,
      alertMessage: 'Sesi login Anda sudah habis',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
      spinner: false,
    });
  };

  onChangeKota = async (data) => {
    this.setState({query: data, showList: true});
  };

  onClikKota = async (data) => {
    this.setState({
      query: data.City,
      selectedCities: data,
      showList: false,
    });
    this.props.updateField('formChangeProfile', 'city', data.City);
  };

  ChangeProfileSubmit = async (data) => {
    const {uri, prevUri, detailUserParam, IdentityUser} = this.state;
    const {editUser} = this.props;
    const userToken = await AsyncStorage.getItem('userToken');

    this.checkInternet();

    if (!uri.uri || uri.uri === prevUri.uri) {
      if (this.state.isConnected) {
        this.setState({spinner: true});

        const dataMerge = {
          Userid: IdentityUser,
          NIK: data.NIK ? data.NIK : '',
          AreaUsr: data.city ? data.city : '',
          UsrImg: detailUserParam.UsrImg,
          UsrAdr: data.alamat ? data.alamat : '',
          Gender: data.gender ? data.gender : '',
        };
        this.setState({dataMerge: dataMerge});
        editUser(dataMerge, userToken);
      }
    } else {
      try {
        if (this.state.isConnected) {
          this.setState({spinner: true});
          const usrImg = await uploadOssFile(uri.uri, IdentityUser);
          this.setState({prevUri: usrImg, uri: usrImg});
          const dataMerge = {
            Userid: IdentityUser,
            NIK: data.NIK ? data.NIK : '',
            AreaUsr: data.city ? data.city : '',
            UsrImg: usrImg ? usrImg : '',
            UsrAdr: data.alamat ? data.alamat : '',
            Gender: data.gender ? data.gender : '',
          };
          this.setState({dataMerge: dataMerge});
          editUser(dataMerge, userToken);
        }
      } catch (error) {
        this.setState({
          alertShowed: true,
          alertMessage: 'Gagal upload image : ' + error.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
          spinner: false,
        });
      }
    }
  };

  changeImageHandle = async () => {
    const options = {
      title: 'Select Profile Picture',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
        console.log(response.error);
      } else {
        ImageResizer.createResizedImage(response.uri, 2000, 2000, 'JPEG', 50)
          .then(async (response) => {
            var source =
              Platform.OS == 'android' ? response.uri : response.path;
            this.setState({uri: {uri: source}});
          })
          .catch((err) => {
            console.log(err.message);
          });
      }
    });
  };

  render() {
    const {handleSubmit, submitting} = this.props;
    const {
      query,
      selectedCities,
      showList,
      detailUserParam,
      pickerGenderItem,
      pickerGenderSelected,
      editUserLoading,
    } = this.state;
    const cities = this.findCity(query);
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();

    return (
      <Container>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}

        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />

        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />

        <Content
          disableKBDismissScroll={false}
          style={
            Platform.OS !== 'android' && this.state.showKeyboard == true
              ? {marginBottom: 250}
              : null
          }>
          <Row style={styles.row}>
            <Image style={styles.image} source={this.state.uri} />
            <TouchableOpacity
              onPress={this.changeImageHandle}
              style={styles.editImgTouchable}>
              <Image
                style={styles.editImg}
                source={require('../../../../assets/img/thumb_profile.png')}
              />
            </TouchableOpacity>
          </Row>

          <Form style={{paddingHorizontal: '10%', paddingBottom: '10%'}}>
            <Field
              name="custID"
              type="text"
              component={renderField}
              editable={false}
              label="Customer ID"
            />

            <Field
              name="fullName"
              type="text"
              component={renderField}
              editable={false}
              label="Nama Lengkap"
            />

            <Field
              name="NIK"
              type="text"
              component={renderField}
              maxLength={16}
              editable={
                detailUserParam.CustID === '-' || detailUserParam.CustID === ''
                  ? true
                  : false
              }
              label="NIK"
              keyboardType="number-pad"
            />

            <Field
              name="noHp"
              type="text"
              component={renderField}
              maxLength={16}
              editable={false}
              label="Nomor Handphone"
            />

            <Field
              name="gender"
              component={renderField2}
              editable={
                detailUserParam.Gender && detailUserParam.Gender !== ''
                  ? false
                  : true
              }
              label="Jenis Kelamin"
              valueInput={pickerGenderSelected}
              optionList={pickerGenderItem.map((pickerGenderItem, index) => (
                <Picker.Item
                  label={pickerGenderItem.Descr}
                  value={pickerGenderItem.Code}
                />
              ))}
              onChangeValue={(newValue) => this.onGenderChange(newValue)}
            />

            <Field
              name="birthDate"
              type="text"
              component={renderField}
              editable={false}
              label="Tanggal Lahir"
            />

            <Field
              name="email"
              type="text"
              component={renderField}
              editable={false}
              label="Email"
            />

            <Field
              name="city"
              type="text"
              dataQuery={query}
              value={this.state.query}
              dataCity={
                cities.length === 1 && comp(query, cities[0].City) ? [] : cities
              }
              hideResults={selectedCities && selectedCities.City === query}
              onChangeValue={(text) => this.onChangeKota(text)}
              component={renderField3}
              renderItem={(cty) =>
                showList ? (
                  <ListItem
                    style={{
                      backgroundColor: 'white',
                      marginLeft: 5,
                      paddingLeft: 5,
                      borderWidth: 1,
                      zIndex: 1,
                    }}
                    onPress={() => this.onClikKota(cty)}>
                    <Text
                      style={{
                        fontSize: 12,
                        color: 'black',
                        fontFamily: fonts.primary.normal,
                      }}>
                      {cty.City}
                    </Text>
                  </ListItem>
                ) : null
              }
              label="Kota"
            />

            <Field
              name="alamat"
              type="text"
              component={renderField}
              editable={true}
              label="Alamat"
            />

            <TouchableOpacity
              style={styles.btnKirim}
              onPress={handleSubmit(this.ChangeProfileSubmit)}
              disabled={submitting}>
              <Text style={styles.text}>Kirim</Text>
            </TouchableOpacity>
          </Form>
        </Content>
        {this.showAlert()}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    formChangeProfile: state.form.formChangeProfile,
    getCityResult: state.getCity.result,
    editUserResult: state.editUser.result,
    editUserLoading: state.editUser.loading,
    editUserError: state.editUser.error,
    refreshTokenResult: state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      editUser,
      getCity,
      refreshToken,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

ChangeProfile = reduxForm({
  form: 'formChangeProfile',
  enableReinitialize: true,
  validate: FormChangeProfileValidate,
})(ChangeProfile);

export default connect(mapStateToProps, matchDispatchToProps)(ChangeProfile);
