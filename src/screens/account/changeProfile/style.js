import {StyleSheet, Dimensions, PixelRatio, Platform} from 'react-native';
import {fonts} from '../../../utils/fonts';
const {width} = Dimensions.get('window');
const style =
  Platform.OS === 'android'
    ? {
        marginVertical: 10,
        left: -15,
        width: '105%',
      }
    : {
        zIndex: 1,
        marginVertical: 10,
        left: -15,
        width: '105%',
      };
//zIndex di android membuat autocomplete suggestion tidak bisa di-klik.
//Namun karena sepertinya kasus ini unik di android, saya coba bikin seperti ini.

export const styles = StyleSheet.create({
  input: {
    marginVertical: 10,
    left: -15,
    width: '105%',
  },
  inputAutocomplete: style,
  row: {alignSelf: 'center'},

  image: {
    borderRadius: 50,
    marginTop: 15,
    width: 80,
    height: 80
  },
  dropDownStyle: {
    width: width * 0.85,
    fontSize: 10,
    marginTop: 10,
    marginLeft: Platform.OS == 'android' ? -6 : -12,
    color: 'black',
  },
  dropDownStyleNonEdit: {
    width: width * 0.85,
    fontSize: 10,
    marginTop: 10,
    marginLeft: Platform.OS == 'android' ? -6 : -12,
    color: '#A6AAB4',
  },
  editImg: {
    borderRadius: 50,
    width: 28,
    height: 28,
    alignSelf: 'center',
    borderColor: '#FFFFFF',
    borderWidth: 2,
    marginBottom: 40,
    // marginLeft: -25,
    marginTop: 10,
  },
  editImgTouchable: {
    marginLeft: -35,
    height: 50,
    width: 55,
  },
  containerAuto: {
    backgroundColor: '#fcfdff',
    flex: 1,
    paddingTop: 25,
  },

  label: {
    left: 15,
    zIndex: 0,
    marginBottom: -8,
    color: '#002f5f',
    fontFamily: fonts.primary.bold,
    fontSize:
      Platform.OS == 'android'
        ? Dimensions.get('window').width > 360
          ? 18
          : 16
        : Dimensions.get('window').width * PixelRatio.get() > 750
        ? 18
        : 16,
  },

  btnKirim: {
    alignSelf: 'center',
    width: '85%',
    height: 40,
    borderRadius: 4,
    backgroundColor: '#002f5f',
    justifyContent: 'center',
    marginTop: '10%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },

  text: {
    textAlign: 'center',
    fontSize: 14,
    color: 'white',

    fontFamily: fonts.primary.bold,
  },

  errorDesc: {
    color: 'red',
    alignSelf: 'flex-end',
    textAlign: 'left',
    fontSize: 10,
    marginBottom: -12,
    fontFamily: fonts.primary.normal,
    // marginLeft: 15,
  },

  validDesc: {
    color: 'green',
    textAlign: 'left',
    fontSize: 10,
    fontFamily: fonts.primary.normal,
  },
  // icon: {
  //   fontSize: 20,
  //   color: 'red',
  //   marginRight: 5,
  //   marginTop: -5,
  //   marginLeft: -10,
  // },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  
});
