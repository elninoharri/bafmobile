import {StyleSheet, Dimensions} from 'react-native';
import {fonts} from '../../../utils/fonts';

const {width: WIDTH} = Dimensions.get('window');

export const styles = StyleSheet.create({
  image: {
    width: '55%',
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  input: {
    marginVertical: 10,
    left: -15,
    width: '105%',
  },
  inputText: {
    fontSize: 14,
    fontFamily: fonts.primary.normal,
  },
  btnUbahPassword: {
    alignSelf: 'center',
    width: '100%',
    height: 40,
    borderRadius: 4,
    backgroundColor: '#002f5f',
    justifyContent: 'center',
    marginTop: '15%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },

  btnDelay: {
    alignSelf: 'center',
    width: '100%',
    height: 40,
    borderRadius: 4,
    backgroundColor: '#b2bec3',
    justifyContent: 'center',
    marginTop: 30,
  },
  text: {
    textAlign: 'center',
    fontSize: 14,
    color: 'white',
    fontFamily: fonts.primary.bold,
  },

  errorDesc: {
    color: 'red',
    alignSelf: 'flex-start',
    textAlign: 'left',
    fontSize: 10,
    marginBottom: -12,
    fontFamily: fonts.primary.normal,
  },

  validDesc: {
    color: 'green',
    textAlign: 'left',
    fontSize: 10,
    fontFamily: fonts.primary.normal,
  },
  icon: {
    fontSize: 20,
    color: 'red',
    marginRight: 5,
    marginTop: -5,
    marginLeft: -10,
  },
});
