import React, { Component } from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  Image,
  BackHandler,
  Platform,
  Keyboard,
  Dimensions,
  PixelRatio,
} from 'react-native';
import { Container, Form, Input, Item, Row, Icon, Content } from 'native-base';
import Toast from 'react-native-easy-toast';
import { styles } from './style';
import { reduxForm, Field, change, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import NetInfo from '@react-native-community/netinfo';
import { changePassword, refreshToken } from '../../../actions';
import { FormChangePassValidate } from '../../../validates/FormChangePassValidate';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import AsyncStorage from '@react-native-community/async-storage';
import { ERROR_AUTH, UsrCrtValue, MESSAGE_TOKEN_EXP, TOKEN_EXP , MESSAGE_TOKEN_INVALID } from '../../../utils/constant';
import Spinner from 'react-native-loading-spinner-overlay';
import analytics from '@react-native-firebase/analytics';
import { jsonParse, handleRefreshToken } from '../../../utils/utilization';

export const renderField = ({
  input,
  type,
  iconright,
  placeholder,
  editable,
  keyboardType,
  maxLength,
  secureTextEntry,
  onPressIcon,
  meta: { touched, error },
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }

  return (
    <View style={styles.input}>
      <Item
        fixedLabel
        style={{
          // borderColor: !touched ? '#666' : hasError ? 'red' : 'green',
          borderColor: hasError ? 'red' : '#666',
          height: 40,
        }}>
        <Input
          {...input}
          type={type}
          editable={editable}
          placeholder={placeholder}
          keyboardType={keyboardType}
          maxLength={maxLength}
          secureTextEntry={secureTextEntry}
          style={styles.inputText}
        />
        {iconright ? (
          <Icon
            active
            name={iconright}
            style={{ color: '#666' }}
            onPress={onPressIcon}
          />
        ) : null}
      </Item>
      {hasError ? (
        <Row style={{ marginLeft: 25, marginBottom: -16 }}>
          <Icon style={styles.icon} name="ios-checkmark" />
          <Text style={styles.errorDesc}>{error}</Text>
        </Row>
      ) : null}
    </View>
  );
};

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Ubah Password',
      hide: true,
      secondHide: true,
      thirdHide: true,
      isConnected: false,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      userData: '',
      userToken: '',
      oldPassword: '',
      password: '',
      detailUser: false,
    };
  }

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      if (!state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{ flexDirection: 'row' }}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{ color: 'red', marginLeft: '-10%' }}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  componentDidMount = async () => {
    analytics().logScreenView({
      screen_name: 'screenChangePassword',
      screen_class: 'screenChangePassword',
    });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    const userData = await AsyncStorage.getItem('userData');
    const userToken = await AsyncStorage.getItem('userToken');
    const detailUserNonParse = await AsyncStorage.getItem('detailUser');

    if (detailUserNonParse) {
      const detailUserParse = JSON.parse(detailUserNonParse);
      this.setState({detailUser: detailUserParse});
    }
    if (userData) {
      const data = JSON.parse(userData);
      this.setState({ userData: data });
    }
    if (userToken) {
      this.setState({ userToken: userToken });
    }
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentDidUpdate = async (prevProps) => {
    const { changePasswordResult, changePasswordError, refreshTokenResult, refreshTokenError, } = this.props;

    if (
      changePasswordResult !== null &&
      prevProps.changePasswordResult !== changePasswordResult
    ) {
      this.setState({
        alertFor: 'changePasswordResult',
        alertShowed: true,
        alertMessage: 'Berhasil ganti password.',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
      });
    }

    if (
      changePasswordError &&
      prevProps.changePasswordError !== changePasswordError
    ) {
      const data = await jsonParse(changePasswordError.message);
      if (data) {
        if (data.message == MESSAGE_TOKEN_EXP) {
          this.props.refreshToken(this.state.userToken);
        } else if(data.message == MESSAGE_TOKEN_INVALID) {
          this.handleInvalid();
        }
        
        // else if(data.message === 'Password salah'){
        //   console.log("enter validate password wrong")
        //   this.setState({alertFor: 'PasswordSalah',})
        // }
      } else {
        this.setState({
          alertShowed: true,
          alertMessage: changePasswordError.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
      // dulu di sini digunakan untuk pengecekan apakah token expired atau bukan, jika sudah maka user seharusnya dibawa ke login screen kembali,
      // akan tetapi karena token expired hanya dalam 5 menit dan terdapat refresh token, block code ini kita comment terlebih dahulu.

      // if (changePasswordError.message === ERROR_AUTH) {
      //   this.setState({
      //     alertFor: 'changePasswordError',
      //     alertShowed: true,
      //     alertMessage: 'Sesi login Anda sudah habis',
      //     alertTitle: 'Informasi',
      //     alertType: 'info',
      //     alertDoneText: 'OK',
      //   });
      //   await AsyncStorage.removeItem('userToken');
      //   await AsyncStorage.removeItem('userData');
      //   await AsyncStorage.removeItem('detailUser');
      //   await AsyncStorage.removeItem('activeAgreement');
      // } else {
      //   this.setState({
      //     alertShowed: true,
      //     alertMessage: changePasswordError.message,
      //     alertTitle: 'Gagal',
      //     alertType: 'error',
      //     alertDoneText: 'OK',
      //   });
      // }
    }

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      // console.log('Error Refresh Token  ', refreshTokenError.message);
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      await handleRefreshToken(refreshTokenResult.Refreshtoken);
      this.setState({ userToken: refreshTokenResult.Refreshtoken });
      this.props.changePassword(
        {
          OldPassword: this.state.oldPassword,
          Password: this.state.password,
          UsrCrt: UsrCrtValue,
        },
        refreshTokenResult.Refreshtoken,
      );
    }
  };

  showHidePassword() {
    this.setState((prevState) => ({
      icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
      hide: !prevState.hide,
    }));
  }

  showHidePassword2() {
    this.setState((prevState) => ({
      secondIcon: prevState.secondIcon === 'eye' ? 'eye-off' : 'eye',
      secondHide: !prevState.secondHide,
    }));
  }

  showIcon = () => {
    this.setState(() => ({
      icon: 'eye-off',
    }));
  };

  showSecondIcon = () => {
    this.setState(() => ({
      secondIcon: 'eye-off',
    }));
  };

  goBack = async () => {
    this.props.navigation.goBack();
    this.props.resetForm('formChangePassword');
  };

  handleInvalid = async () => {
    await AsyncStorage.removeItem('userToken');
    await AsyncStorage.removeItem('userData');
    await AsyncStorage.removeItem('detailUser');
    await AsyncStorage.removeItem('activeAgreement');
    this.setState({
      alertFor: 'invalidToken',
      alertShowed: true,
      alertMessage: 'Sesi login Anda sudah habis',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  sendPassword = async (data) => {
    const { changePassword, navigation} = this.props;
    const {detailUser} = this.state;
    const userToken = await AsyncStorage.getItem('userToken');
    Keyboard.dismiss();
    this.setState({ oldPassword: data.passNow, password: data.passNew });
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        if(!detailUser){
          navigation.navigate('Login');
        }else{
          changePassword(
            {
              OldPassword: data.passNow,
              Password: data.passNew,
              UsrCr: UsrCrtValue
            },
            userToken,
          );
        }
      } else {
        this.showToastNoInternet();
      }
    });
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handlePositiveButtonAlert = () => {
    this.setState({ alertShowed: false });
    if (this.state.alertFor === 'changePasswordResult' ) {
      this.goBack();
    } else if (this.state.alertFor === 'invalidToken'){
      this.props.navigation.navigate('Login');
      this.setState({  alertFor: null });
    }
  };

  handleNegativeButtonAlert = () => {
    this.setState({ alertShowed: false });
  };

  render() {
    const { handleSubmit, submitting, changePasswordLoading } = this.props;
    return (
      <Container>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
            <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
          )}

        <Spinner
          visible={changePasswordLoading ? true : false}
          textContent={'Loading...'}
          textStyle={{ color: '#FFF' }}
        />

        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />
        <Content disableKBDismissScroll={false}>
          <Image
            style={styles.image}
            source={require('../../../../assets/img/changePassword.png')}
          />

          <Form style={{ paddingHorizontal: '10%', paddingBottom: '10%' }}>
            <Field
              name="passNow"
              type="text"
              component={renderField}
              placeholder="Password saat ini"
              maxLength={15}
              secureTextEntry={this.state.hide}
              onPressIcon={() => this.showHidePassword()}
              onChange={() => this.showIcon()}
              iconright={this.state.icon}
              editable={true}
            />

            <Field
              name="passNew"
              type="text"
              component={renderField}
              placeholder="Password Baru"
              maxLength={15}
              secureTextEntry={this.state.secondHide}
              onPressIcon={() => this.showHidePassword2()}
              onChange={() => this.showSecondIcon()}
              iconright={this.state.secondIcon}
              editable={true}
            />

            <Field
              name="passConfrim"
              type="text"
              component={renderField}
              placeholder="Verifikasi Password"
              maxLength={15}
              secureTextEntry={this.state.thirdHide}
              editable={true}
            />

            <TouchableOpacity
              style={styles.btnUbahPassword}
              onPress={handleSubmit(this.sendPassword)}
              disabled={submitting}>
              <Text style={styles.text}>Ubah Password</Text>
            </TouchableOpacity>
          </Form>

          {this.showAlert()}
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    changePasswordLoading: state.changePassword.loading,
    changePasswordResult: state.changePassword.result,
    changePasswordError: state.changePassword.error,
    formChangePassword: state.form.formChangePassword,
    refreshTokenResult: state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      changePassword,
      refreshToken,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

ChangePassword = reduxForm({
  form: 'formChangePassword',
  enableReinitialize: true,
  validate: FormChangePassValidate,
})(ChangePassword);

export default connect(mapStateToProps, matchDispatchToProps)(ChangePassword);
