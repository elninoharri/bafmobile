import React, {Component} from 'react';
import {Card, Icon, Text} from 'native-base';
import {
  View,
  Image,
  TouchableOpacity,
  Platform,
  Dimensions,
  PixelRatio,
} from 'react-native';
import Style from './style';
import CustomAlertComponent from '../../../../components/multiPlatform/customAlert/CustomAlertComponent';

export default class CardBpkb extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataBpkb: null,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      alertNegativeButtonBackgroundColor: '',
      alertMessageButtonTextStyle: '',
    };
  }

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm'
            ? true
            : false || this.state.alertType === 'warning'
            ? true
            : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
        alertNegativeButtonBackgroundColor={
          this.state.alertNegativeButtonBackgroundColor
        }
        alertMessageButtonTextStyle={this.state.alertMessageButtonTextStyle}
      />
    );
  };

  componentDidMount = () => {};

  componentDidUpdate = () => {
    
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false, alertFor: false});
  };

  render() {
    const {data, cancel, reschedule} = this.props;
    {
      return (
        <View
          style={
            data.Status == '0'
              ? Style.cardContainerProcess
              : Style.cardContainerDone
          }>
          <View style={Style.cardFirstFloor}>
            <View style={Style.cardSubFirstFloorTop}>
              <Text style={Style.cardFirstFloorTitle}>Nomor Referensi</Text>
              <Text style={Style.cardFirstFloorDesc}>
                {!data.ReserveNo ? '-' : data.ReserveNo}
              </Text>
              <View
                style={
                  Platform.OS === 'android'
                    ? Style.cardFirstFloorSubSeparatorIosSmall
                    : Dimensions.get('window').width * PixelRatio.get() < 750
                    ? Style.cardFirstFloorSubSeparatorIosLarge
                    : Style.cardFirstFloorSubSeparatorIosSmall
                }
              />
            </View>
            <View style={Style.cardSubFirstFloor}>
              <Text style={Style.cardFirstFloorTitle}>Nomor Agreement</Text>
              <Text style={Style.cardFirstFloorDesc}>
                {!data.ContractNo ? '-' : data.ContractNo}
              </Text>
            </View>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View
              style={{flex: 1, justifyContent: 'center', paddingLeft: '4%'}}>
              <Text style={Style.cardSecondFloorTitle}>
                Tanggal Pengambilan
              </Text>
              <Text style={Style.cardSecondFloorDesc}>
                {!data.TglPengambilan ? '-' : data.TglPengambilan}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-end',
                paddingRight: '8%',
              }}>
              <Text style={Style.cardSecondFloorTitle}>Jam Pengambilan</Text>
              <Text style={Style.cardSecondFloorDesc}>
                {!data.IntervalTime ? '-' : data.IntervalTime}
              </Text>
            </View>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View
              style={{flex: 1, justifyContent: 'center', paddingLeft: '4%'}}>
              <Text style={Style.cardThirdFloorTitle}>Lokasi Pengambilan</Text>
              <Text style={Style.cardSecondFloorDesc}>
                {!data.NamaCabangPengambilan ? '-' : data.NamaCabangPengambilan}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-end',
                paddingRight: '8%',
              }}>
              <Text style={Style.cardThirdFloorTitle}>Status</Text>
              <Text
                style={
                 data.Status === '0' ? Style.cardThirdFloorDesc :
                  data.Status === '1' ?  Style.cardThirdFloorDescSuccess : Style.cardThirdFloorDescCancel
                }>
                {!data.Status ? '-' : 
                  data.Status === '0' ? 'Sedang Proses' : 
                    data.Status === '1' ? 'Diterima' :
                      data.Status === '2' ? 'Dibatalkan' : '-'
                  }
              </Text>
            </View>
          </View>
          {data.Status == '0' ? (
            <View
              style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
              <View
                style={{flex: 1, justifyContent: 'center', paddingLeft: '4%'}}>
                <TouchableOpacity
                  onPress={cancel}
                  style={Style.btnCancel}>
                  <Text style={Style.txtCancel}>Batalkan</Text>
                </TouchableOpacity>
              </View>
              <View
                style={{flex: 1, justifyContent: 'center', paddingRight: '8%'}}>
                <TouchableOpacity
                  onPress={reschedule}
                  style={Style.btnChangeSchedule}>
                  <Text style={Style.txtChangeSchedule}>Ubah Jadwal</Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}
          <View style={Style.bpkbIconContainer}>
            <Image
              source={require('../../../../../assets/img/bpkbicon.png')}
              style={{width: 40, height: 40}}
            />
          </View>
          {this.showAlert()}
        </View>
      );
    }
  }
}
