import { StyleSheet} from 'react-native';
import { BAF_COLOR_BLUE } from '../../utils/constant';

export default StyleSheet.create({
    containerTabHeaderButton : {
        flex:1,
        flexDirection:'row',
        borderBottomWidth:5,
        borderBottomColor:'rgba(0, 0, 0, 0.15)'
    },
    tabHeaderButtonDisable : {
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        borderBottomWidth:4,
        borderBottomColor:'white'
    },
    tabHeaderButtonEnable : {
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        borderBottomWidth:4,
        borderBottomColor:BAF_COLOR_BLUE
    },
    tabHeaderTitleDisable : {
        fontSize:16,
        color:'black',
        opacity:0.6,
        fontWeight:'bold'
    },
    tabHeaderTitleEnable : {
        fontSize:16,
        color:BAF_COLOR_BLUE,
        opacity:0.9,
        fontWeight:'bold'
    }
})