import { Container, Content } from 'native-base';
import React, {Component, PureComponent} from 'react';
import { Platform,TouchableOpacity,View,Text, BackHandler, } from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import SubHeaderAndroid from '../../components/android/subHeaderBlue';
import SubHeaderIos from '../../components/ios/subHeaderBlue';
import CardBpkb from './component/cardBpkb';
import Style from "./style";
import AsyncStorage from '@react-native-community/async-storage';
import {getListBPKBInProgress, getListBPKBDone, cancelBpkb, rescheduleBpkb} from '../../actions/account/bpkbRequest';
import {refreshToken} from '../../actions';
import CustomAlertComponent from '../../components/multiPlatform/customAlert/CustomAlertComponent'
import {  MESSAGE_TOKEN_EXP, TOKEN_EXP , MESSAGE_TOKEN_INVALID } from '../../utils/constant';
import { jsonParse, handleRefreshToken } from '../../utils/utilization';
import Spinner from 'react-native-loading-spinner-overlay';

class Bpkb extends Component{
    
    constructor(props) {
        super(props);
        this.state = {
            screenTitle: 'Bpkb',
            tabScreen: 'proses',
            dataBpkb: null,
            dataBpkbDone: null,
            dataBpkbInprogress: null,
            userToken: false,
            bpkblogID: '',
            alertFor: null,
            alertShowed: false,
            alertMessage: '',
            alertTitle: '',
            alertType: 'success',
            alertDoneText: '',
            alertCancelText: '',
            alertNegativeButtonBackgroundColor: '',
            alertMessageButtonTextStyle: '',
        };
      }

    goBack = async () => {
        this.props.navigation.goBack();
    };

    getListBPKB = async () => {
        const { getListBPKBDone, getListBPKBInProgress } = this.props;
        const {userToken} = this.state;
        if (userToken) {
            getListBPKBDone(userToken)
            getListBPKBInProgress(userToken)
        }
    }

    handleBackButton = () => {
        if (this.props.navigation.isFocused()) {
            this.goBack();
            return true;
        }
    };

    componentDidMount = async () => {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        const userToken = await AsyncStorage.getItem('userToken');
        if (userToken) {
         this.setState({ userToken: userToken })
        }
        this.getListBPKB();
    }

    componentDidUpdate = async (prevProps, prevState) => {
        const {
            getListBPKBDoneResult,
            getListBPKBDoneError,
            getListBPKBInProgressResult,
            getListBPKBInProgressError,
            cancelBpkbError,
            cancelBpkbResult,
            refreshTokenError,
            refreshTokenResult,
            
        } = this.props;


        if (getListBPKBDoneResult && prevProps.getListBPKBDoneResult !== getListBPKBDoneResult) {
            this.setState({dataBpkbDone : getListBPKBDoneResult.AllData})
        }

        if (getListBPKBDoneError && prevProps.getListBPKBDoneError !== getListBPKBDoneError) {
            const data = await jsonParse(getListBPKBDoneError.message);
            if(data){
                this.validateRefreshToken(data);
            }else if (getListBPKBDoneError.message.includes("404")) {
                this.setState({dataBpkbDone : null})
            } else {
                this.setState({
                    alertShowed: true,
                    alertMessage: getListBPKBDoneError.message,
                    alertTitle: 'Gagal',
                    alertType: 'error',
                    alertDoneText: 'OK',
                });
            }
        }

        if (getListBPKBInProgressResult && prevProps.getListBPKBInProgressResult !== getListBPKBInProgressResult) {
            if(getListBPKBInProgressResult.AllData){
                this.setState({dataBpkbInprogress : getListBPKBInProgressResult.AllData})
            }
            
        }

        if (getListBPKBInProgressError && prevProps.getListBPKBInProgressError !== getListBPKBInProgressError) {
            const data = await jsonParse(getListBPKBInProgressError.message);
            if (data) {
                this.validateRefreshToken(data);
            } else if (getListBPKBInProgressError.message.includes("404")) {
                this.setState({dataBpkbInprogress : null})
            } else {
                this.setState({
                    alertShowed: true,
                    alertMessage: getListBPKBInProgressError.message,
                    alertTitle: 'Gagal',
                    alertType: 'error',
                    alertDoneText: 'OK',
                });
            }

        }

        if (cancelBpkbError && prevProps.cancelBpkbError !== cancelBpkbError) {
            const data = await jsonParse(cancelBpkbError.message);
            if (data) {
                this.validateRefreshToken(data);
            } else {
                this.setState({
                    alertShowed: true,
                    alertMessage: detailUserError.message,
                    alertTitle: 'Gagal',
                    alertType: 'error',
                    alertDoneText: 'OK',
                });
            }
        }

        if (cancelBpkbResult && prevProps.cancelBpkbResult !== cancelBpkbResult) {
            this.setState({
                alertFor: 'cancelBpkbResult',
                alertShowed: true,
                alertMessage: 'Reservasi BPKB Berhasil Di batalkan.',
                alertTitle: 'Berhasil',
                alertType: 'success',
                alertDoneText: 'OK',
              });
        }

        if (
            refreshTokenError !== null &&
            prevProps.refreshTokenError !== refreshTokenError
        ) {
            console.log('Error Refresh Token  ', refreshTokenError.message);
        }

        if (
            refreshTokenResult !== null &&
            prevProps.refreshTokenResult !== refreshTokenResult
        ) {
            await handleRefreshToken(refreshTokenResult.Refreshtoken);
            const newToken =  refreshTokenResult.Refreshtoken;
            this.setState({ userToken: newToken });
            if(cancelBpkbError){
                this.props.cancelBpkb({BPKBLogID : this.state.bpkblogID},newToken )
            } 
           
        }


    }

    changeTab = (TabName) => {
        this.setState({tabScreen:TabName})
    }

    cancelBpkb = (BPKBLogID) => {
      this.setState({
      alertFor: 'cancelBPKBPickup',
      alertShowed: true,
      alertMessage: 'Apakah Anda yakin akan membatalkan\nreservasi?',
      alertTitle: 'Batal Reservasi',
      alertType: 'warning',
      alertDoneText: 'Ya',
      alertCancelText: 'Tidak',
      alertNegativeButtonBackgroundColor: 'white',
      alertMessageButtonTextStyle: 'gray',
      bpkblogID : BPKBLogID,
    });


    }

    rescheduleBpkb = (BPKBLogID) => {
        this.setState({
            alertFor: 'rescheduleBPKBPickup',
            alertShowed: true,
            alertMessage: 'Apakah Anda yakin akan mengubah\njadwal reservasi?',
            alertTitle: 'Ubah Jadwal',
            alertType: 'warning',
            alertDoneText: 'Ya',
            alertCancelText: 'Tidak',
            alertNegativeButtonBackgroundColor: 'white',
            alertMessageButtonTextStyle: 'gray',
            bpkblogID : BPKBLogID
        });
    }

      // this function is validate token, if response api expired token this function trigered
    validateRefreshToken = async (data) => {
        const { refreshToken } = this.props;
        if (data) {
            if (data.message == MESSAGE_TOKEN_EXP) {
                refreshToken(this.state.userToken);
            } else if (data.message == MESSAGE_TOKEN_INVALID) {
                this.handleInvalid();
            }
        }
    };

    showAlert = () => {
        return (
          <CustomAlertComponent
            displayAlert={this.state.alertShowed}
            displayAlertIcon={true}
            alertType={this.state.alertType}
            alertTitleText={this.state.alertTitle}
            alertMessageText={this.state.alertMessage}
            displayPositiveButton={true}
            positiveButtonText={this.state.alertDoneText}
            displayNegativeButton={
              this.state.alertType === 'confirm'
                ? true
                : false || this.state.alertType === 'warning'
                ? true
                : false
            }
            onPressNegativeButton={this.handleNegativeButtonAlert}
            negativeButtonText={this.state.alertCancelText}
            onPressPositiveButton={this.handlePositiveButtonAlert}
            alertNegativeButtonBackgroundColor={
              this.state.alertNegativeButtonBackgroundColor
            }
            alertMessageButtonTextStyle={this.state.alertMessageButtonTextStyle}
          />
        );
      };
    
      handlePositiveButtonAlert = async () => {
        this.setState({ alertShowed: false });
        if(this.state.alertFor === 'cancelBpkbResult'){
            this.changeTab("selesai");
            this.getListBPKB();
        } else if (this.state.alertFor === 'invalidToken'){
          this.props.navigation.navigate('Login');
          this.setState({  alertFor: null });
        } else if(this.state.alertFor === 'cancelBPKBPickup'){
            const userToken = await AsyncStorage.getItem('userToken');
            this.props.cancelBpkb({BPKBLogID : this.state.bpkblogID},userToken )
        } else if (this.state.alertFor === 'rescheduleBPKBPickup'){
            this.props.navigation.navigate('RequestBPKP', { params: this.state.bpkblogID });
        } 
      };
    
      handleNegativeButtonAlert = () => {
        this.setState({ alertShowed: false });
      };
  
    render(){
        const {
            dataBpkbDone,
            dataBpkbInprogress,
        } = this.state;
        const {cancelBpkbLoading} = this.props;

       
        return(
            <Container>
                {Platform.OS === 'android' ? 
                    <SubHeaderAndroid
                        title = "Pesanan Saya"
                        goBack = {() => {this.props.navigation.goBack()}}
                    /> : 
                    <SubHeaderIos
                        title = "Pesanan Saya"
                        goBack = {() => {this.props.navigation.goBack()}}
                    />
                }

                <Spinner
                    visible={cancelBpkbLoading ? true : false} // add this ternary to loading check avail booking
                    textContent={'Sedang memproses...'}
                    overlayColor="rgba(0, 0, 0, 0.80)"
                    textStyle={{ color: '#FFF' }}
                />

                <View style={Style.containerTabHeaderButton}>
                    <TouchableOpacity 
                        onPress={() => this.changeTab("proses") }
                        style={this.state.tabScreen === "proses" ? Style.tabHeaderButtonEnable : Style.tabHeaderButtonDisable}>
                        <Text style={this.state.tabScreen === "proses" ? Style.tabHeaderTitleEnable : Style.tabHeaderTitleDisable}>
                            Sedang Proses
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPress={() => this.changeTab("selesai") }
                        style={this.state.tabScreen !== "proses" ? Style.tabHeaderButtonEnable : Style.tabHeaderButtonDisable}>
                        <Text style={this.state.tabScreen !== "proses" ? Style.tabHeaderTitleEnable : Style.tabHeaderTitleDisable}>
                            Selesai
                        </Text>
                    </TouchableOpacity>
                </View>
                {this.state.tabScreen === "proses" ? 
                    <View style={{flex:9}}>
                        { this.props.getListBPKBInProgressLoading ? (
                            <View style={{width:'100%',height:"50%",justifyContent:'center',alignItems:'center'}} >
                                <Text style={{fontSize:20,color:'grey'}}>
                                Sedang Memuat
                                </Text>
                            </View>
                        ) : (
                            dataBpkbInprogress ?  (
                                <Content style={{width:'100%'}}>
                                    {dataBpkbInprogress.map((data, index) => <CardBpkb data={data} cancel= {()=> this.cancelBpkb(data.BPKBLogID)} 
                                     reschedule={() => this.rescheduleBpkb(data.BPKBLogID)} dataId={this.state.tabScreen}/>)}
                                </Content>
                            ): (
                                <View style={{width:'100%',height:"50%",justifyContent:'center',alignItems:'center'}} >
                                    <Text style={{fontSize:20,color:'grey'}}>
                                        Tidak ada pesanan tersedia
                                    </Text>
                                </View>
                            )
                        ) 
                        }
                        
                    </View>:
                    <View style={{flex:9}}>
                        { this.props.getListBPKBDoneLoading ? (
                            <View style={{width:'100%',height:"50%",justifyContent:'center',alignItems:'center'}} >
                                <Text style={{fontSize:20,color:'grey'}}>
                                Sedang Memuat
                                </Text>
                            </View>
                        ) : (
                            dataBpkbDone ?  (
                                <Content style={{width:'100%'}}>
                                    {dataBpkbDone.map((data, index) => <CardBpkb data={data} dataId={this.state.tabScreen}/>)}
                                </Content>
                            ): (
                                <View style={{width:'100%',height:"50%",justifyContent:'center',alignItems:'center'}} >
                                    <Text style={{fontSize:20,color:'grey'}}>
                                        Tidak ada pesanan tersedia
                                    </Text>
                                </View>
                            )
                        ) 
                        }
                    </View>
                }
                {this.showAlert()}
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        getListBPKBInProgressLoading: state.getListBPKBInProgress.loading,
        getListBPKBInProgressResult: state.getListBPKBInProgress.result,
        getListBPKBInProgressError: state.getListBPKBInProgress.error,
        getListBPKBDoneLoading: state.getListBPKBDone.loading,
        getListBPKBDoneResult: state.getListBPKBDone.result,
        getListBPKBDoneError: state.getListBPKBDone.error,
        cancelBpkbLoading: state.cancelBpkb.loading,
        cancelBpkbResult: state.cancelBpkb.result,
        cancelBpkbError: state.cancelBpkb.error,
        refreshTokenResult: state.refreshToken.result,
        refreshTokenError: state.refreshToken.error,
    };
  }
  
  function matchDispatchToProps(dispatch) {
    return bindActionCreators(
      {
        getListBPKBInProgress,
        getListBPKBDone,
        cancelBpkb,
        rescheduleBpkb,
        refreshToken,
      },
      dispatch,
    );
  }
  
  export default connect(mapStateToProps, matchDispatchToProps)(Bpkb);



