import React, {Component} from 'react';
import {styles} from './style';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import {
  TouchableOpacity,
  Text,
  View,
  Image,
  KeyboardAvoidingView,
  Platform,
  Slider,
  SafeAreaView,
  Dimensions,
  PixelRatio,
  Alert,
} from 'react-native';
import {
  Container,
  Input,
  Item,
  Label,
  Header,
  Left,
  Right,
  Button,
  Body,
  Title,
  Form,
  Row,
  Icon,
  Picker,
  Card,
  CardItem,
  Content,
} from 'native-base';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {reduxForm, Field, change, reset, formValueSelector} from 'redux-form';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import {
  ddeJenis,
  ddeMerk,
} from '../../../actions/sandiaFeature/dataLengkap/dataLengkapTransaksi';
import Toast from 'react-native-easy-toast';
import {
  LOB,
  BAF_COLOR_BLUE,
  CODE_JAMINAN,
  BG_COLOR,
  BORDER_COLOR,
  MESSAGE_TOKEN_EXP,
  TOKEN_EXP,
  MESSAGE_TOKEN_INVALID,
} from '../../../utils/constant';
import {
  substringCircum,
  substringThirdCircum,
  formatCurrency,
  fdeFormatCurrency,
  normalizeCurrency,
  substringSecondCircum,
  jsonParse,
  handleRefreshToken,
} from '../../../utils/utilization';
import {FormSimulationMultiproductValidate} from '../../../validates/FormSimulationMultiproductValidate';
import {
  mpMappingRisk,
  mpInstCalculation,
} from '../../../actions/sandiaFeature/dataAwal';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Spinner from 'react-native-loading-spinner-overlay';
import {detailUser} from '../../../actions/account/changeProfile';
import {fonts} from '../../../utils/fonts';
import {getPengajuanMP} from '../../../actions/lob';
import {refreshToken} from '../../../actions';
import moment from 'moment';
import analytics from '@react-native-firebase/analytics';

export const renderField = ({
  input,
  type,
  label,
  editable,
  keyboardType,
  maxLength,
  multiline,
  placeholder,
  numberOfLines,
  format,
  normalize,
  customError,
  meta: {touched, error},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '80%', marginTop: 15, alignSelf: 'center'}}>
      <Label
        style={{
          fontSize: 16,
          color: hasError ? 'red' : 'black',
          alignSelf: 'center',
          marginBottom: 5,
        }}>
        {label}
      </Label>
      <Item
        regular
        style={{
          backgroundColor: 'white',
          height: 40,
          borderColor: customError || hasError ? 'red' : BAF_COLOR_BLUE,
          borderWidth: 1,
          borderRadius: 5,
          paddingRight: 10,
        }}>
        <Input
          style={{
            fontSize: 14,
            paddingLeft: '4%',
            color: editable ? 'black' : '#aaa',
            fontFamily: fonts.primary.normal,
          }}
          {...input}
          type={type}
          placeholder={placeholder}
          editable={editable}
          keyboardType={keyboardType}
          maxLength={maxLength}
          multiline={multiline}
          numberOfLines={numberOfLines}
          format={format}
          normalize={normalize}
        />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderFieldHidden = ({
  input,
  type,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '100%'}}>
      <Item style={{display: 'none'}}>
        <Input {...input} ype={type} />
      </Item>
      {/* {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null} */}
    </View>
  );
};

export const renderFieldPicker = ({
  input,
  label,
  pickerSelected,
  onValueChange,
  customError,
  enabled,
  data,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <SafeAreaView style={{width: '80%', marginTop: 15, alignSelf: 'center'}}>
      <Label
        style={{
          fontSize: 16,
          color: customError || hasError ? 'red' : 'black',
          alignSelf: 'center',
          marginBottom: 5,
          fontFamily: fonts.primary.normal,
        }}>
        {label}
      </Label>
      <View
        style={{
          height: 40,
          borderColor:
            customError || hasError
              ? 'red'
              : pickerSelected
              ? BAF_COLOR_BLUE
              : BORDER_COLOR,
          borderWidth: 1,
          borderRadius: 5,
          paddingRight: 10,
        }}>
        <Picker
          {...input}
          renderHeader={(backAction) => (
            <Header>
              <Left>
                <Button transparent onPress={backAction}>
                  <Icon name="arrow-back" style={{color: 'black'}} />
                </Button>
              </Left>
              <Body style={{flex: 3}}>
                <Title>{label}</Title>
              </Body>
              <Right />
            </Header>
          )}
          note={enabled ? false : true}
          mode="dropdown"
          style={{
            marginTop: Platform.OS == 'android' ? -6 : -4,
            fontSize: 14,
            width: '100%',
          }}
          enabled={enabled}
          selectedValue={pickerSelected}
          placeholder="Silahkan Pilih"
          onValueChange={onValueChange}>
          {data}
        </Picker>
      </View>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </SafeAreaView>
  );
};

class MultiproductSimulation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Kredit Multi Produk',
      value: 1000000,
      showList: false,
      query: '',
      isConnected: false,

      pickerLobItem: [
        {Code: 'Motor Yamaha'},
        {Code: 'Mobil Baru'},
        {Code: 'Multiproduk'},
        {Code: 'Dana Syariah'},
      ],
      pickerLobSelected: 'Multiproduk',

      pickerJenisItem: [{Code: '', Descr: 'Pilih'}],
      pickerJenisSelected: '',

      pickerMerkItem: [{Code: '', Descr: 'Pilih'}],
      pickerMerkSelected: '',

      pickerTenorItem: [{Code: '', Descr: 'Pilih'}],
      pickerTenorSelected: '',

      pickerDpItem: [{Code: '', Descr: 'Pilih'}],
      pickerDpSelected: '',

      initHargaBarang: '1000000',

      enableMerk: true,
      enableDpTerm: true,
      showResultSimulation: false,

      detailUser: false,
      userData: false,

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: '',
      alertDoneText: '',
      alertCancelText: '',

      errorTipeBarang: false,
      errorMerkBarang: false,
      errorHargaBarang: false,

      isModalVisible: false,
      dataNIK: false,
      LOB: 'MP',
      dataMerge: '',
      userToken: '',
    };
  }

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  componentDidMount = async () => {
    const {ddeJenis, qdeDetailData} = this.props;

    analytics().logScreenView({
      screen_class: 'screenMultiproductSimulation',
      screen_name: 'screenMultiproductSimulation',
    });
    this.checkInternet();

    const detailUserNonParse = await AsyncStorage.getItem('detailUser');
    if (detailUserNonParse) {
      const detailUserParse = JSON.parse(detailUserNonParse);
      this.setState({detailUser: detailUserParse});
    }

    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) {
      this.setState({userToken: userToken});
    }
    this.props.updateField(
      'formMultiProduct',
      'typeCust',
      this.state.detailUser.CustType,
    );
    this.props.updateField(
      'formMultiProduct',
      'hargaBarang',
      this.state.initHargaBarang,
    );

    if (this.state.isConnected) {
      ddeJenis({LOB: LOB, Jaminan: substringCircum(CODE_JAMINAN)}, userToken);
    }
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      ddeJenis_Result,
      ddeMerk_Result,
      ddeMerk_Error,
      ddeMerk_Loading,
      mpMappingRiskResult,
      mpMappingRiskError,
      mpInstCalculationError,
      mpInstCalculationResult,
      getPengajuanMPResult,
      getPengajuanMPError,
      formMultiProduct,
      detailUserResult,
      refreshTokenResult,
      refreshTokenError,
    } = this.props;
    const {dataMerge} = this.state;

    if (detailUserResult && prevProps.detailUserResult !== detailUserResult) {
      this.setState({detailUser: detailUserResult});
    }

    if (
      ddeJenis_Result.length &&
      prevProps.ddeJenis_Result !== ddeJenis_Result
    ) {
      var arrayJenis = [{Code: '', Descr: 'Pilih'}];

      for (var i = 0; i < ddeJenis_Result.length; i++) {
        arrayJenis.push({
          Code:
            ddeJenis_Result[i].Code +
            '^' +
            ddeJenis_Result[i].Descr +
            '^' +
            ddeJenis_Result[i].RiskTypeId,
          Descr: ddeJenis_Result[i].Descr,
        });
      }
      this.setState({
        pickerJenisItem: arrayJenis,
      });
    }

    if (ddeMerk_Result.length && prevProps.ddeMerk_Result !== ddeMerk_Result) {
      var arrayMerk = [{Code: '', Descr: 'Pilih'}];

      for (var i = 0; i < ddeMerk_Result.length; i++) {
        arrayMerk.push({
          Code: ddeMerk_Result[i].Code + '^' + ddeMerk_Result[i].Descr,
          Descr: ddeMerk_Result[i].Descr,
        });
      }
      this.setState({
        pickerMerkItem: arrayMerk,
        enableMerk: true,
      });
    }

    if (ddeMerk_Loading && prevProps.ddeMerk_Loading !== ddeMerk_Loading) {
      var arrayMerk = [{Code: '', Descr: 'Loading...'}];
      this.setState({pickerMerkItem: arrayMerk});
    }

    if (ddeMerk_Error && prevProps.ddeMerk_Error !== ddeMerk_Error) {
      var arrayMerk = [{Code: '', Descr: 'Pilih'}];
      this.setState({pickerMerkItem: arrayMerk});
    }

    if (
      getPengajuanMPResult &&
      prevProps.getPengajuanMPResult !== getPengajuanMPResult
    ) {
      this.setState({
        alertFor: 'getPengajuanMPResult',
        alertShowed: true,
        alertMessage: 'Data anda akan kami proses',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
      });
    }

    if (
      getPengajuanMPError &&
      prevProps.getPengajuanMPError !== getPengajuanMPError
    ) {
      const data = await jsonParse(getPengajuanMPError.message);
      if (data) {
        console.log(data);
        if (data.message == MESSAGE_TOKEN_EXP) {
          this.props.refreshToken(this.state.userToken);
          // this.handleInvalid();
        } else if (data.message == MESSAGE_TOKEN_INVALID) {
          this.handleInvalid();
        } else {
          this.setState({
            alertFor: 'getPengajuanMPError',
            alertShowed: true,
            alertMessage:
              'Terjadi kesalahan pada sistem, harap coba beberapa saat lagi.',
            alertTitle: 'Gagal',
            alertType: 'error',
            alertDoneText: 'OK',
          });
        }
      } else {
        this.setState({
          alertFor: 'getPengajuanMPError',
          alertShowed: true,
          alertMessage:
            'Terjadi kesalahan pada sistem, harap coba beberapa saat lagi.',
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
    }

    if (
      mpMappingRiskError &&
      prevProps.mpMappingRiskError !== mpMappingRiskError
    ) {
      this.setState({
        alertFor: 'mpMappingRiskError',
        alertShowed: true,
        alertMessage: mpMappingRiskError.message,
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
      });
    }

    if (
      mpMappingRiskResult &&
      prevProps.mpMappingRiskResult !== mpMappingRiskResult
    ) {
      this.setState({
        alertFor: 'mpMappingRiskResult',
        alertShowed: true,
        alertMessage: 'Berhasil mendapatkan tenor dan DP',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
      });

      var listTenor = mpMappingRiskResult.Tenor;
      var listDp = mpMappingRiskResult.DP;
      var arrayTenor = [{Code: '', Descr: 'Pilih'}];
      var arrayDp = [{Code: '', Descr: 'Pilih'}];
      for (var i = 0; i < listTenor.length; i++) {
        arrayTenor.push({
          Code: listTenor[i],
          Descr: listTenor[i] + ' Bulan',
        });
      }
      for (var i = 0; i < listDp.length; i++) {
        arrayDp.push({
          Code: listDp[i],
          Descr: (parseFloat(listDp[i]) * 100).toString() + ' %',
        });
      }
      this.setState({
        pickerTenorItem: arrayTenor,
        pickerDpItem: arrayDp,
        pickerTenorSelected: '',
        pickerDpSelected: '',
        enableDpTerm: true,
      });
      this.props.updateField(
        'formMultiProduct',
        'adminFee',
        mpMappingRiskResult.Admin,
      );
      this.props.updateField('formMultiProduct', 'adminType', 'Masuk Angsuran');
      this.props.updateField('formMultiProduct', 'installmentType', 'ADDM');
      this.props.updateField(
        'formMultiProduct',
        'interestRate',
        (parseFloat(mpMappingRiskResult.IntRate) * 100).toString(),
      );
      this.props.updateField(
        'formMultiProduct',
        'insurancePercent',
        (parseFloat(mpMappingRiskResult.Asuransi) * 100).toString(),
      );
      this.props.updateField(
        'formMultiProduct',
        'insuranceAmount',
        (
          parseFloat(mpMappingRiskResult.Asuransi) *
          parseFloat(formMultiProduct.values.hargaBarang)
        ).toString(),
      );
    }

    if (
      mpInstCalculationError &&
      prevProps.mpInstCalculationError !== mpInstCalculationError
    ) {
      this.setState({
        alertFor: 'mpInstCalculationError',
        alertShowed: true,
        alertMessage: mpInstCalculationError.message,
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
      });
    }

    if (
      mpInstCalculationResult &&
      prevProps.mpInstCalculationResult !== mpInstCalculationResult
    ) {
      this.setState({
        showResultSimulation: true,
      });
    }

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      console.log('Error Refresh Token ', refreshTokenError.message);
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      if (getPengajuanMPError) {
        console.log('refreshTokenResult', refreshTokenResult);
        await handleRefreshToken(refreshTokenResult.Refreshtoken);
        this.setState({userToken: refreshTokenResult.Refreshtoken});
        this.props.getPengajuanMP(
          {
            DateReq: dataMerge.DateReq,
            DP: dataMerge.DP,
            JenisBrg: dataMerge.JenisBrg,
            Pinjaman: dataMerge.Pinjaman,
            PromoCode: dataMerge.PromoCode,
            Source: dataMerge.Source,
            Tenor: dataMerge.Tenor,
            TipePengajuan: dataMerge.TipePengajuan,
          },
          refreshTokenResult.Refreshtoken,
        );
      }
    }
  };

  handleInvalid = async () => {
    await AsyncStorage.removeItem('userToken');
    await AsyncStorage.removeItem('userData');
    await AsyncStorage.removeItem('detailUser');
    await AsyncStorage.removeItem('activeAgreement');
    this.setState({
      alertFor: 'invalidToken',
      alertShowed: true,
      alertMessage: 'Sesi login Anda sudah habis',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  goBack = async () => {
    this.props.resetForm('formMultiProduct');
    this.props.navigation.navigate('Home');
  };

  onChangeSlider = async (value) => {
    this.props.updateField('formMultiProduct', 'hargaBarang', value);
    this.props.updateField('formMultiProduct', 'uangMuka', '');
    this.props.updateField('formMultiProduct', 'uangMukaAmount', '');
    this.props.updateField('formMultiProduct', 'tenor', '');

    this.setState({
      pickerDpSelected: '',
      pickerTenorSelected: '',
      errorHargaBarang: false,
      // enableDpTerm: true,
      showResultSimulation: false,
    });

    this.setState({value: parseFloat(value), errorHargaBarang: false});
  };

  onHargaChange = (value) => {
    this.setState({
      pickerDpSelected: '',
      pickerTenorSelected: '',
      errorHargaBarang: false,
      // enableDpTerm: false,
      showResultSimulation: false,
    });

    this.props.updateField('formMultiProduct', 'uangMuka', '');
    this.props.updateField('formMultiProduct', 'uangMukaAmount', '');
    this.props.updateField('formMultiProduct', 'tenor', '');
  };

  onJenisChange = (value) => {
    const {ddeMerk} = this.props;

    this.setState({
      pickerJenisSelected: value,
      pickerMerkSelected: '',
      // enableMerk: false,
      // enableDpTerm: false,
      errorTipeBarang: false,
      showResultSimulation: false,
    });

    this.props.updateField('formMultiProduct', 'uangMuka', '');
    this.props.updateField('formMultiProduct', 'uangMukaAmount', '');
    this.props.updateField('formMultiProduct', 'tenor', '');
    this.props.updateField('formMultiProduct', 'tipeBarang', value);
    this.props.updateField(
      'formMultiProduct',
      'typeRisk',
      substringThirdCircum(value),
    );
    this.props.updateField('formMultiProduct', 'merkBarang', '');

    if (value !== '') {
      ddeMerk({JaminanJenis: substringCircum(value)});
    }
  };

  onMerkChange = (value) => {
    this.setState({
      pickerDpSelected: '',
      pickerTenorSelected: '',
      pickerMerkSelected: value,
      errorMerkBarang: false,
      // enableDpTerm: false,
      showResultSimulation: false,
    });

    this.props.updateField('formMultiProduct', 'uangMuka', '');
    this.props.updateField('formMultiProduct', 'uangMukaAmount', '');
    this.props.updateField('formMultiProduct', 'tenor', '');
    this.props.updateField('formMultiProduct', 'merkBarang', value);
  };

  onTenorChange = (value) => {
    this.setState({
      pickerTenorSelected: value,
      showResultSimulation: false,
    });
    this.props.updateField('formMultiProduct', 'tenor', value);
  };

  onDpChange = (value) => {
    const {hargaBarang} = this.props;
    this.setState({
      pickerDpSelected: value,
      showResultSimulation: false,
    });
    var uangMukaAmount = '';
    this.props.updateField('formMultiProduct', 'uangMuka', value);

    if (!value || value == '') {
      this.props.updateField(
        'formMultiProduct',
        'uangMukaAmount',
        uangMukaAmount,
      );
    } else {
      uangMukaAmount = parseFloat(value) * parseFloat(hargaBarang);
      this.props.updateField(
        'formMultiProduct',
        'uangMukaAmount',
        uangMukaAmount.toString(),
      );
    }
  };

  tenorDPCheck = async () => {
    const {
      tipeBarang,
      merkBarang,
      hargaBarang,
      formMultiProduct,
      mpMappingRisk,
    } = this.props;
    if (!tipeBarang || tipeBarang == '') {
      this.setState({errorTipeBarang: true});
    } else if (!merkBarang || merkBarang == '') {
      this.setState({errorTipeBarang: false, errorMerkBarang: true});
    } else if (
      !hargaBarang ||
      hargaBarang == '' ||
      parseFloat(hargaBarang) < 1000000
    ) {
      this.setState({
        errorTipeBarang: false,
        errorMerkBarang: false,
        errorHargaBarang: true,
      });
    } else {
      this.setState({
        errorTipeBarang: false,
        errorMerkBarang: false,
        errorHargaBarang: false,
        showResultSimulation: false,
      });
      mpMappingRisk({
        RiskType: formMultiProduct.values.typeRisk,
        Hargabrg: hargaBarang,
      });
    }
  };

  submitCalculate = async (data) => {
    const {mpInstCalculation} = this.props;

    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        mpInstCalculation({
          Hargabrg: data.hargaBarang,
          DP: data.uangMuka,
          Tenor: data.tenor,
          Insurance: data.insurancePercent,
          Adminfee: data.adminFee,
          Tipebiayaadmin: data.adminType,
          Bunga: data.interestRate,
          Tipeangsuran: data.installmentType,
        });
      } else {
        this.showToastNoInternet();
      }
    });
  };

  submitData = async (data) => {
    const userToken = await AsyncStorage.getItem('userToken');
    const {detailUser, LOB} = this.state;
    const {navigation, getPengajuanMP} = this.props;
    NetInfo.fetch().then(async (state) => {
      if (state.isConnected) {
        if (!detailUser) {
          navigation.navigate('Login');
          // this.handleInvalid()
        } else {
          analytics().logEvent('eventMultiproductSubmit', {
            item: 'user trying to submit Multiproduct simulation',
          });
          var dataMerge = {
            DateReq: moment().format('YYYY-MM-DD hh:mm:ss'),
            DP: data.uangMuka * 100 + '%',
            JenisBrg:
              substringSecondCircum(data.tipeBarang) +
              ' ' +
              substringSecondCircum(data.merkBarang),
            Pinjaman: 'Rp.' + data.hargaBarang,
            PromoCode: '-',
            Source: 'BAF Mobile',
            Tenor: substringCircum(data.tenor),
            TipePengajuan: LOB,
          };

          this.setState({dataMerge: dataMerge});
          getPengajuanMP(
            {
              DateReq: dataMerge.DateReq,
              DP: dataMerge.DP,
              JenisBrg: dataMerge.JenisBrg,
              Pinjaman: dataMerge.Pinjaman,
              PromoCode: dataMerge.PromoCode,
              Source: dataMerge.Source,
              Tenor: dataMerge.Tenor,
              TipePengajuan: dataMerge.TipePengajuan,
            },
            userToken,
          );
        }
      } else {
        this.showToastNoInternet();
      }
    });
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
        onPressNegativeButton={this.handleNegativeButtonAlert}
      />
    );
  };

  handlePositiveButtonAlert = () => {
    if (this.state.alertFor === 'showModal') {
      this.setState({alertShowed: false, alertFor: false});
      this.showModal();
    } else if (this.state.alertFor === 'getPengajuanMPResult') {
      this.setState({alertShowed: false, alertFor: false});
      this.props.navigation.navigate('Home');
    } else if (this.state.alertFor === 'invalidToken') {
      this.setState({alertShowed: false, alertFor: false});
      this.props.navigation.navigate('Login');
    } else {
      this.setState({alertShowed: false, alertFor: false});
    }
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false, alertFor: false});
  };

  pickerLobChange = async (data) => {
    const {resetForm, navigation} = this.props;
    await resetForm('formMultiProduct');
    this.setState({
      pickerMerkSelected: '',
      pickerJenisSelected: '',

      pickerDpSelected: '',
      mpInstCalculationResult: '',
      // enableMerk: false,
      enableDpTerm: false,
      showResultSimulation: false,
    });

    switch (data) {
      case 'Motor Yamaha':
        navigation.replace('NmcSimulation');
        break;
      case 'Multiproduk':
        navigation.replace('MultiproductSimulation');
        break;
      case 'Mobil Baru':
        navigation.replace('CarSimulation');
        break;
      case 'Dana Syariah':
        navigation.replace('SyanaSimulation');
        break;
      default:
        navigation.replace('MultiSimulation');
    }
  };

  render = () => {
    const {
      value,
      detailUser,
      pickerLobItem,
      pickerLobSelected,
      pickerJenisItem,
      pickerJenisSelected,
      pickerMerkItem,
      pickerMerkSelected,
      pickerTenorItem,
      pickerTenorSelected,
      pickerDpItem,
      pickerDpSelected,
      enableMerk,
      enableDpTerm,
      showResultSimulation,
    } = this.state;

    const {
      handleSubmit,
      submitting,
      mpInstCalculationLoading,
      mpInstCalculationResult,
      mpMappingRiskLoading,
      getPengajuanMPLoading,
    } = this.props;

    return (
      <Container>
        {/* {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )} */}

        <Spinner
          visible={
            mpInstCalculationLoading ||
            mpMappingRiskLoading ||
            getPengajuanMPLoading
              ? true
              : false
          }
          textContent={'Sedang memproses...'}
          textStyle={{color: '#FFF'}}
        />

        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />

        <Content disableKBDismissScroll={false}>
          <Field
            name="tipeBarang"
            component={renderFieldPicker}
            enabled={true}
            label="Tipe Barang"
            customError={this.state.errorTipeBarang}
            pickerSelected={pickerJenisSelected}
            data={pickerJenisItem.map((pickerJenisItem, index) => (
              <Picker.Item
                label={pickerJenisItem.Descr}
                value={pickerJenisItem.Code}
              />
            ))}
            onValueChange={(newValue) => this.onJenisChange(newValue)}
          />

          <Field
            name="merkBarang"
            component={renderFieldPicker}
            enabled={enableMerk}
            label="Merk Barang"
            customError={this.state.errorMerkBarang}
            pickerSelected={pickerMerkSelected}
            data={pickerMerkItem.map((pickerMerkItem, index) => (
              <Picker.Item
                label={pickerMerkItem.Descr}
                value={pickerMerkItem.Code}
              />
            ))}
            onValueChange={(newValue) => this.onMerkChange(newValue)}
          />

          <Field
            name="hargaBarang"
            label="Harga Barang"
            type="text"
            editable={true}
            component={renderField}
            format={fdeFormatCurrency}
            normalize={normalizeCurrency}
            keyboardType={'number-pad'}
            customError={this.state.errorHargaBarang}
            onChange={(newValue) => this.onHargaChange(newValue)}
          />

          <Field name="typeRisk" type="text" component={renderFieldHidden} />

          <TouchableOpacity
            onPress={this.tenorDPCheck}
            style={styles.btncekTenor}
            disabled={submitting}>
            <Text style={styles.textButton}>Cek Tenor DP Tersedia</Text>
          </TouchableOpacity>

          <Field name="adminFee" type="text" component={renderFieldHidden} />

          <Field name="adminType" type="text" component={renderFieldHidden} />

          <Field
            name="installmentType"
            type="text"
            component={renderFieldHidden}
          />

          <Field
            name="interestRate"
            type="text"
            component={renderFieldHidden}
          />

          <Field
            name="insurancePercent"
            type="text"
            component={renderFieldHidden}
          />

          <Field
            name="insuranceAmount"
            type="text"
            component={renderFieldHidden}
          />

          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              marginHorizontal: '5%',
              alignSelf: 'center',
              justifyContent: 'space-between',
            }}>
            <View style={{width: '49%'}}>
              <Field
                name="tenor"
                component={renderFieldPicker}
                label="Tenor"
                enabled={enableDpTerm}
                pickerSelected={pickerTenorSelected}
                data={pickerTenorItem.map((pickerTenorItem, index) => (
                  <Picker.Item
                    label={pickerTenorItem.Descr}
                    value={pickerTenorItem.Code}
                  />
                ))}
                onValueChange={(newValue) => this.onTenorChange(newValue)}
              />
            </View>

            <View style={{width: '49%'}}>
              <Field
                name="uangMuka"
                component={renderFieldPicker}
                label="Uang Muka"
                enabled={enableDpTerm}
                pickerSelected={pickerDpSelected}
                data={pickerDpItem.map((pickerDpItem, index) => (
                  <Picker.Item
                    label={pickerDpItem.Descr}
                    value={pickerDpItem.Code}
                  />
                ))}
                onValueChange={(newValue) => this.onDpChange(newValue)}
              />
            </View>
          </View>

          <Field
            name="uangMukaAmount"
            label="Nominal Uang Muka"
            type="text"
            editable={false}
            component={renderFieldHidden}
            format={formatCurrency}
            normalize={normalizeCurrency}
          />

          <TouchableOpacity
            style={styles.btnHitung}
            onPress={handleSubmit(this.submitCalculate)}
            disabled={submitting}>
            <Text style={styles.textButton}>Hitung Angsuran</Text>
          </TouchableOpacity>
          <View style={{height: 10}} />

          {showResultSimulation ? (
            <Card
              style={{
                backgroundColor: '#f5f5f5',
                width: '85%',
                alignItems: 'center',
                alignSelf: 'center',
                borderRadius: 10,
                marginBottom: '5%',
                paddingBottom: 25,
              }}>
              <Text style={styles.textHeader}>
                Estimasi Angsuran Bulanan Anda
              </Text>
              <Text style={styles.textBelowHeader}>
                Rp.{' '}
                {mpInstCalculationResult
                  ? formatCurrency(
                      mpInstCalculationResult.AngsuranRp.toString(),
                    )
                  : '-'}
              </Text>
              <Text style={styles.textDesc}>/bulan</Text>
              <View style={{height: 15}}></View>
              <Text style={styles.textDesc}>
                Perhitungan hanya bersifat simulasi,
              </Text>
              <Text style={styles.textDesc}>
                dapat berubah sesuai regulasi BAF
              </Text>
              <Text style={styles.textDesc}>
                Ajukan Aplikasi pembiayaan dan
              </Text>
              <Text style={styles.textDesc}>kami akan menghubungi Anda</Text>
              <TouchableOpacity
                style={styles.btnAjukan}
                onPress={handleSubmit(this.submitData)}>
                <Text style={styles.textButton}>Ajukan Sekarang</Text>
              </TouchableOpacity>
            </Card>
          ) : null}
        </Content>

        {this.showAlert()}
      </Container>
    );
  };
}

const selector = formValueSelector('formMultiProduct');

function mapStateToProps(state) {
  return {
    tipeBarang: selector(state, 'tipeBarang'),
    merkBarang: selector(state, 'merkBarang'),
    hargaBarang: selector(state, 'hargaBarang'),
    detailUserResult: state.detailUser.result,
    ddeJaminanJenis_Loading: state.ddeJaminanJenis.loading,
    ddeJaminanJenis_Result: state.ddeJaminanJenis.result,
    ddeJaminanJenis_Error: state.ddeJaminanJenis.error,
    ddeJenis_Loading: state.ddeJenis.loading,
    ddeJenis_Result: state.ddeJenis.result,
    ddeJenis_Error: state.ddeJenis.error,
    ddeMerk_Loading: state.ddeMerk.loading,
    ddeMerk_Result: state.ddeMerk.result,
    ddeMerk_Error: state.ddeMerk.error,
    mpMappingRiskResult: state.mpMappingRisk.result,
    mpMappingRiskLoading: state.mpMappingRisk.loading,
    mpMappingRiskError: state.mpMappingRisk.error,
    mpInstCalculationResult: state.mpInstCalculation.result,
    mpInstCalculationLoading: state.mpInstCalculation.loading,
    mpInstCalculationError: state.mpInstCalculation.error,
    formMultiProduct: state.form.formMultiProduct,
    getPengajuanMPResult: state.getPengajuanMP.result,
    getPengajuanMPError: state.getPengajuanMP.error,
    getPengajuanMPLoading: state.getPengajuanMP.loading,
    refreshTokenResult: state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ddeJenis,
      detailUser,
      ddeMerk,
      mpMappingRisk,
      mpInstCalculation,
      getPengajuanMP,
      refreshToken,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

MultiproductSimulation = reduxForm({
  form: 'formMultiProduct',
  enableReinitialize: true,
  validate: FormSimulationMultiproductValidate,
})(MultiproductSimulation);

export default connect(
  mapStateToProps,
  matchDispatchToProps,
)(MultiproductSimulation);
