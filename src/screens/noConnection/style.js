import {StyleSheet, Dimensions,StatusBar} from 'react-native';
import {BAF_COLOR_BLUE} from '../../utils/constant';

export const styles = StyleSheet.create({
  containerContent: {
    paddingHorizontal: '3%'
  },
  textHeader: {
    textAlign: 'left',
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold',
  },
  textUp: {
    fontSize: 14,
    color: '#002f5f',
    fontWeight: 'bold',
    paddingLeft: '3%',
    paddingVertical: '3%'
  },
  textBelow: {
    fontSize: 12,
    color: 'black',
    marginTop:-25
  },
  imagePromo: {
    height: 140,
    width: 0,
    borderRadius: 10,
    flex: 1,
  },
  iconStyle: {
    fontSize: 14,
    paddingRight: 10,
    marginTop:-25
  },
  containerBig: {
    alignItems: 'center',
    height: Platform.OS === 'ios' ? 44 : StatusBar.currentHeight,
    backgroundColor:BAF_COLOR_BLUE,
  },
  containerSmall: {
    alignItems: 'center',
    height: Platform.OS === 'ios' ? "3%" : StatusBar.currentHeight,
    backgroundColor:BAF_COLOR_BLUE,
  },
  
});
