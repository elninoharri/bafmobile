import React, {Component} from 'react';
import {Text,Image,Dimensions,PixelRatio,View,StatusBar,TouchableOpacity,RefreshControl, Platform} from 'react-native';
import {Container, Left, Icon, CardItem, Card, Content} from 'native-base';
import {styles} from './style';
import {connect} from 'react-redux';
import Toast, {DURATION} from 'react-native-easy-toast';
import {BAF_COLOR_BLUE} from '../../utils/constant';
import RNRestart from 'react-native-restart';

class NoConnection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    };
  }

  componentDidMount = async () => {
    this.refs.toast.show(
      <View style={{flexDirection:'row'}}>
        <Icon type="FontAwesome" name="exclamation-circle" style={{color:"red",marginLeft:'-10%'}} />
        <Text style={{
          marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
          marginLeft: '3%',
          fontSize: 16,
          color: 'red'
        }}>Tidak Ada Koneksi Internet</Text>
      </View>
    ,DURATION.LENGTH_LONG);
  };

  
  onRefresh = () => {
    RNRestart.Restart();
  }
 

  render() {
    return (
      <Container style={{flex: 1}}>
        {Platform.OS != 'android' ? (
          Dimensions.get('window').width * PixelRatio.get() > 750 ? (
              <View style={styles.containerBig}>
                <StatusBar
                  barStyle="light-content"
                  hidden={false}
                  translucent={false}
                  networkActivityIndicatorVisible={true}
                  showHideTransition="fade"
                />
              </View>
            ) : (
              <View style={styles.containerSmall}>
                <StatusBar
                  barStyle="light-content"
                  hidden={false}
                  translucent={false}
                  networkActivityIndicatorVisible={true}
                  showHideTransition="fade"
                /> 
              </View> 
            )
          ) : (
            <StatusBar barStyle="light-content" showHideTransition={true} backgroundColor={BAF_COLOR_BLUE}></StatusBar>
          )
        }

        {Platform.OS != 'android' ? (
          Dimensions.get('window').width * PixelRatio.get() > 750 ? ( 
          <Toast
                ref="toast"
                style={{
                  backgroundColor:'#FFE6E9',
                  width:'100%',
                  borderBottomWidth:2,
                  borderBottomColor:'red',
                  justifyContent:'center',
                  paddingLeft:50,
                  height:50,
                  borderRadius:0}}
                position='top'
                positionValue={"5%"}
                fadeInDuration={2000}
                fadeOutDuration={1000}
                opacity={0.9}
              /> 
          ):(
            <Toast
                ref="toast"
                style={{
                  backgroundColor:'#FFE6E9',
                  width:'100%',
                  borderBottomWidth:2,
                  borderBottomColor:'red',
                  justifyContent:'center',
                  paddingLeft:50,
                  height:50,
                  borderRadius:0}}
                position='top'
                positionValue={"3%"}
                fadeInDuration={2000}
                fadeOutDuration={1000}
                opacity={0.9}
              /> 
          ))
        : (
          <Toast
              ref="toast"
              style={{
                backgroundColor:'#FFE6E9',
                width:'100%',
                borderBottomWidth:2,
                borderBottomColor:'red',
                justifyContent:'center',
                paddingLeft:50,
                height:50,
                borderRadius:0}}
              position='top'
              positionValue={"0%"}
              fadeInDuration={2000}
              fadeOutDuration={1000}
              opacity={0.9}
            /> 
        )
    
    }
        
        <View style={{flex:1}}>
        
        {Platform.OS == 'ios' ? (
          Dimensions.get('window').width * PixelRatio.get() > 750 ? (
            <Image source={require('../../../assets/img/disconnect.png')} resizeMode="contain" style={{width:'90%',alignSelf:'center',marginTop:'5%'}} />
          ) : (
            <Image source={require('../../../assets/img/disconnect.png')} resizeMode="contain" style={{width:'90%',alignSelf:'center',marginTop:'5%'}} />
          )
        ) : (
          Dimensions.get('window').width  > 360 ? (
            <Image source={require('../../../assets/img/disconnect.png')} resizeMode="contain" style={{width:'80%',alignSelf:'center',marginTop:'5%'}} />
          ) : (
            <Image source={require('../../../assets/img/disconnect.png')} resizeMode="contain" style={{width:'70%',alignSelf:'center'}} />
          )
        )}
        
          
          {Platform.OS == 'ios' ? (
            Dimensions.get('window').width * PixelRatio.get() > 750 ? (
              <View style={{alignItems:'center',marginHorizontal:'10%'}}>
                <Text style={{fontSize:25,fontWeight:'bold'}}>Upss.. Koneksi Terputus !</Text>
                <Text style={{marginTop:'5%',textAlign:'center',fontSize:16}}>Sepertinya koneksimu terputus dengan Wifi atau koneksi lainnya</Text>
                <TouchableOpacity onPress={() => {this.onRefresh()}}  style={{width:'100%',height:"18%",backgroundColor:BAF_COLOR_BLUE,marginTop:'10%',alignItems:'center',justifyContent:'center',borderRadius:8,alignSelf:'center'}}>
                  <Text style={{color:'white',fontWeight:'bold'}}>Muat Ulang</Text>
                </TouchableOpacity>
              </View>
            ):(
              <View style={{alignItems:'center',marginHorizontal:'10%'}}>
                <Text style={{fontSize:22,fontWeight:'bold'}}>Upss.. Koneksi Terputus !</Text>
                <Text style={{marginTop:'5%',textAlign:'center',fontSize:14}}>Sepertinya koneksimu terputus dengan Wifi atau koneksi lainnya</Text>
                <TouchableOpacity onPress={() => {this.onRefresh()}} style={{width:'100%',height:"18%",backgroundColor:BAF_COLOR_BLUE,marginTop:'10%',alignItems:'center',justifyContent:'center',borderRadius:8,alignSelf:'center'}}>
                  <Text style={{color:'white',fontWeight:'bold'}}>Muat Ulang</Text>
                </TouchableOpacity>
              </View>
            )
          ) : (
            Dimensions.get('window').width > 360 ?(
            <View style={{alignItems:'center',marginHorizontal:'10%'}}>
              <Text style={{fontSize:25,fontWeight:'bold'}}>Upss.. Koneksi Terputus !</Text>
              <Text style={{marginTop:'5%',textAlign:'center',fontSize:16}}>Sepertinya koneksimu terputus dengan Wifi atau koneksi lainnya</Text>
              <TouchableOpacity onPress={() => {this.onRefresh()}} style={{width:'100%',height:"18%",backgroundColor:BAF_COLOR_BLUE,marginTop:'10%',alignItems:'center',justifyContent:'center',borderRadius:8,alignSelf:'center'}}>
                  <Text style={{color:'white',fontWeight:'bold'}}>Muat Ulang</Text>
                </TouchableOpacity>
            </View>
            ):(
              <View style={{alignItems:'center',marginHorizontal:'14%',marginTop:"-15%"}}>
              <Text style={{fontSize:20,fontWeight:'bold'}}>Upss.. Koneksi Terputus !</Text>
              <Text style={{marginTop:'5%',textAlign:'center',fontSize:12}}>Sepertinya koneksimu terputus dengan Wifi atau koneksi lainnya</Text>
              <TouchableOpacity onPress={() => {this.onRefresh()}} style={{width:'100%',height:"18%",backgroundColor:BAF_COLOR_BLUE,marginTop:'10%',alignItems:'center',justifyContent:'center',borderRadius:8,alignSelf:'center'}}>
                  <Text style={{color:'white',fontWeight:'bold'}}>Muat Ulang</Text>
                </TouchableOpacity>
            </View>
            )
          )
          }
          
          
        </View>

      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
   
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NoConnection);
