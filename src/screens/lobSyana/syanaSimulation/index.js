import React, {Component} from 'react';
import {styles} from './style';
import {
  TouchableOpacity,
  Text,
  View,
  Image,
  SafeAreaView,
  Platform,
  Dimensions,
  PixelRatio,
} from 'react-native';
import {
  Container,
  Input,
  Item,
  Label,
  Header,
  Left,
  Right,
  Button,
  Body,
  Title,
  Icon,
  Picker,
  Card,
  Content,
} from 'native-base';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {reduxForm, Field, change, reset, formValueSelector} from 'redux-form';
import {FormSimulationSyanaValidate} from '../../../validates/FormSimulationSyanaValidate';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-easy-toast';
import {
  BAF_COLOR_BLUE,
  BG_COLOR,
  BORDER_COLOR,
  MESSAGE_TOKEN_EXP,
  TOKEN_EXP,
  MESSAGE_TOKEN_INVALID,
} from '../../../utils/constant';
import {
  formatCurrency,
  normalizeCurrency,
  fdeFormatCurrency,
  substringCircum,
  substringSecondCircum,
  jsonParse,
  handleRefreshToken,
} from '../../../utils/utilization';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Spinner from 'react-native-loading-spinner-overlay';
import {detailUser} from '../../../actions/account/changeProfile';
import syana from './syana.json';
import moment from 'moment';
import {fonts} from '../../../utils/fonts';
import {getPengajuanSyana} from '../../../actions/lob';
import {refreshToken} from '../../../actions';
import analytics from '@react-native-firebase/analytics';

export const renderField = ({
  input,
  type,
  label,
  editable,
  keyboardType,
  maxLength,
  multiline,
  placeholder,
  numberOfLines,
  format,
  normalize,
  customError,
  meta: {touched, error},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '80%', marginTop: 15, alignSelf: 'center'}}>
      <Label
        style={{
          fontSize: 16,
          color: hasError ? 'red' : 'black',
          alignSelf: 'center',
          marginBottom: 5,
          fontFamily: fonts.primary.normal,
        }}>
        {label}
      </Label>
      <Item
        regular
        style={{
          backgroundColor: 'white',
          height: 40,
          borderColor: customError || hasError ? 'red' : BAF_COLOR_BLUE,
          borderWidth: 1,
          borderRadius: 5,
          paddingRight: 10,
        }}>
        <Input
          style={{
            fontSize: 14,
            paddingLeft: '4%',
            color: editable ? 'black' : '#aaa',
            fontFamily: fonts.primary.normal,
          }}
          {...input}
          type={type}
          placeholder={placeholder}
          editable={editable}
          keyboardType={keyboardType}
          maxLength={maxLength}
          multiline={multiline}
          numberOfLines={numberOfLines}
          format={format}
          normalize={normalize}
        />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderFieldHidden = ({input, type, meta: {touched, error}}) => {
  if (touched && error !== undefined) {
  }
  return (
    <View style={{width: '100%'}}>
      <Item style={{display: 'none'}}>
        <Input {...input} ype={type} />
      </Item>
    </View>
  );
};

export const renderFieldPicker = ({
  input,
  label,
  pickerSelected,
  onValueChange,
  customError,
  enabled,
  data,
  meta: {touched, error},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <SafeAreaView style={{width: '80%', marginTop: 15, alignSelf: 'center'}}>
      <Label
        style={{
          fontSize: 16,
          color: customError || hasError ? 'red' : 'black',
          alignSelf: 'center',
          marginBottom: 5,
          fontFamily: fonts.primary.normal,
        }}>
        {label}
      </Label>
      <View
        style={{
          backgroundColor: 'white',
          height: 40,
          borderColor:
            customError || hasError
              ? 'red'
              : pickerSelected
              ? BAF_COLOR_BLUE
              : BORDER_COLOR,
          borderWidth: 1,
          borderRadius: 5,
          paddingRight: 10,
          flexDirection: 'row',
        }}>
        <Picker
          {...input}
          renderHeader={(backAction) => (
            <Header>
              <Left>
                <Button transparent onPress={backAction}>
                  <Icon name="arrow-back" style={{color: 'black'}} />
                </Button>
              </Left>
              <Body style={{flex: 3}}>
                <Title>{label}</Title>
              </Body>
              <Right />
            </Header>
          )}
          note={enabled ? false : true}
          mode="dropdown"
          style={{
            marginTop: Platform.OS == 'android' ? -6 : -4,
            fontSize: 14,
            fontFamily: fonts.primary.normal,
            width: '100%',
          }}
          enabled={enabled}
          selectedValue={pickerSelected}
          placeholder="Silahkan Pilih"
          onValueChange={onValueChange}>
          {data}
        </Picker>
      </View>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </SafeAreaView>
  );
};

class SyanaSimulation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Kredit Dana Syariah',
      value: 1000000,
      initHargaBarang: '1000000',

      showList: false,
      query: '',
      isConnected: false,

      pickerLobItem: [
        {Code: 'Motor Yamaha'},
        {Code: 'Mobil Baru'},
        {Code: 'Multiproduk'},
        {Code: 'Dana Syariah'},
      ],
      pickerLobSelected: 'Dana Syariah',

      pickerMerkSelected: '',

      pickerYearSelected: '',

      pickerTenorSelected: '',

      enableMerk: true,
      showResultSimulation: false,
      calculateResult: false,
      detailUser: false,
      userData: false,

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: '',
      alertDoneText: '',
      alertCancelText: '',

      LOB: 'SYANA',
      userToken: '',
      dataMerge: '',
    };
  }

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected});
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
            fontFamily: fonts.primary.normal,
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  componentDidMount = async () => {
    analytics().logScreenView({
      screen_class: 'screenSyanaSimulation',
      screen_name: 'screenSyanaSimulation',
    });

    this.checkInternet();
    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) {
      this.setState({userToken: userToken});
    }

    const detailUserNonParse = await AsyncStorage.getItem('detailUser');
    if (detailUserNonParse) {
      const detailUserParse = JSON.parse(detailUserNonParse);
      this.setState({detailUser: detailUserParse});
    }
    this.props.updateField(
      'formSyanaProduct',
      'plafonPinjaman',
      this.state.initHargaBarang,
    );
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      detailUserResult,
      getPengajuanSyanaResult,
      getPengajuanSyanaError,
      refreshTokenResult,
      refreshTokenError,
    } = this.props;
    const {dataMerge} = this.state;
    if (detailUserResult && prevProps.detailUserResult !== detailUserResult) {
      this.setState({detailUser: detailUserResult});
    }

    if (
      getPengajuanSyanaError &&
      prevProps.getPengajuanSyanaError !== getPengajuanSyanaError
    ) {
      const data = await jsonParse(getPengajuanSyanaError.message);
      if (data) {
        console.log(data);
        if (data.message == MESSAGE_TOKEN_EXP) {
          this.props.refreshToken(this.state.userToken);
          // this.handleInvalid();
        } else if (data.message == MESSAGE_TOKEN_INVALID) {
          this.handleInvalid();
        } else {
          this.setState({
            alertFor: 'getPengajuanSyanaError',
            alertShowed: true,
            alertMessage:
              'Terjadi kesalahan pada sistem, harap coba beberapa saat lagi.',
            alertTitle: 'Gagal',
            alertType: 'error',
            alertDoneText: 'OK',
          });
        }
      } else {
        this.setState({
          alertFor: 'getPengajuanSyanaError',
          alertShowed: true,
          alertMessage:
            'Terjadi kesalahan pada sistem, harap coba beberapa saat lagi.',
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
    }

    if (
      getPengajuanSyanaResult &&
      prevProps.getPengajuanSyanaResult !== getPengajuanSyanaResult
    ) {
      this.setState({
        alertFor: 'getPengajuanSyanaResult',
        alertShowed: true,
        alertMessage: 'Data anda akan kami proses',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
      });
    }

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      console.log('Error Refresh Token ', refreshTokenError.message);
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      if (getPengajuanSyanaError) {
        await handleRefreshToken(refreshTokenResult.Refreshtoken);
        this.setState({userToken: refreshTokenResult.Refreshtoken});
        this.props.getPengajuanSyana(
          {
            DateReq: dataMerge.DateReq,
            DP: dataMerge.DP,
            JenisBrg: dataMerge.JenisBrg,
            Pinjaman: dataMerge.Pinjaman,
            PromoCode: dataMerge.PromoCode,
            Source: dataMerge.Source,
            Tenor: dataMerge.Tenor,
            TipePengajuan: dataMerge.TipePengajuan,
          },
          refreshTokenResult.Refreshtoken,
        );
      }
    }
  };

  handleInvalid = async () => {
    await AsyncStorage.removeItem('userToken');
    await AsyncStorage.removeItem('userData');
    await AsyncStorage.removeItem('detailUser');
    await AsyncStorage.removeItem('activeAgreement');
    this.setState({
      alertFor: 'invalidToken',
      alertShowed: true,
      alertMessage: 'Sesi login Anda sudah habis',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  goBack = async () => {
    this.props.navigation.goBack();
    this.props.resetForm('formSyanaProduct');
  };

  pickerLobChange = async (data) => {
    const {resetForm, navigation} = this.props;
    this.setState({
      pickerMerkSelected: '',
      pickerYearSelected: '',
      pickerTenorSelected: '',
      calculateResult: false,
      showResultSimulation: false,
    });
    await resetForm('formSyanaProduct');
    switch (data) {
      case 'Motor Yamaha':
        navigation.replace('NmcSimulation');
        break;
      case 'Multiproduk':
        navigation.replace('MultiproductSimulation');
        break;
      case 'Mobil Baru':
        navigation.replace('CarSimulation');
        break;
      case 'Dana Syariah':
        navigation.replace('SyanaSimulation');
        break;
      default:
        navigation.replace('SyanaSimulation');
    }
  };

  onMerkChange = (value) => {
    this.setState({
      pickerMerkSelected: value,
      showResultSimulation: false,
    });
    this.props.updateField('formSyanaProduct', 'merkMotor', value);
  };

  onYearChange = (value) => {
    this.setState({
      pickerYearSelected: value,
      showResultSimulation: false,
    });
    this.props.updateField('formSyanaProduct', 'tahunMotor', value);
  };

  onHargaChange = (value) => {
    this.setState({showResultSimulation: false});
  };

  onTenorChange = (value) => {
    this.setState({
      pickerTenorSelected: value,
      showResultSimulation: false,
    });
    this.props.updateField('formSyanaProduct', 'tenor', value);
  };

  submitCalculate = async (data) => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        var plafonPinjaman = parseFloat(data.plafonPinjaman);
        var Tenor = substringCircum(data.tenor);
        var Rate = substringSecondCircum(data.tenor);
        var calculateResult =
          (plafonPinjaman * (1 + parseFloat(Rate))) / parseFloat(Tenor);
        this.setState({
          showResultSimulation: true,
          calculateResult: Math.round(calculateResult),
        });
      } else {
        this.showToastNoInternet();
      }
    });
  };

  submitData = async (data) => {
    const userToken = await AsyncStorage.getItem('userToken');
    const {detailUser, LOB} = this.state;
    const {navigation, getPengajuanSyana} = this.props;
    NetInfo.fetch().then(async (state) => {
      if (state.isConnected) {
        if (!detailUser) {
          navigation.navigate('Login');
          // this.handleInvalid()
        } else {
          analytics().logEvent('eventSyanaSubmit', {
            item: 'user trying to submit Dana Syariah simulation',
          });
          var dataMerge = {
            Source: 'BAF Mobile',
            JenisBrg: substringCircum(data.merkMotor) + ' ' + data.tahunMotor,
            TipePengajuan: LOB,
            Pinjaman: 'Rp. ' + data.plafonPinjaman,
            Tenor: substringCircum(data.tenor),
            DateReq: moment().format('YYYY-MM-DD hh:mm:ss'),
            PromoCode: '-',
            DP: '-',
          };

          getPengajuanSyana(
            {
              DateReq: dataMerge.DateReq,
              DP: dataMerge.DP,
              JenisBrg: dataMerge.JenisBrg,
              Pinjaman: dataMerge.Pinjaman,
              PromoCode: dataMerge.PromoCode,
              Source: dataMerge.Source,
              Tenor: dataMerge.Tenor,
              TipePengajuan: dataMerge.TipePengajuan,
            },
            userToken,
          );
          this.setState({dataMerge: dataMerge});
        }
      } else {
        this.showToastNoInternet();
      }
    });
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
        onPressNegativeButton={this.handleNegativeButtonAlert}
      />
    );
  };

  handlePositiveButtonAlert = () => {
    if (this.state.alertFor === 'showModal') {
      this.setState({alertShowed: false, alertFor: false});
      this.showModal();
    } else if (
      this.state.alertFor === 'getPengajuanSyanaResult' ||
      this.state.alertFor === 'getPengajuanSyanaError'
    ) {
      this.setState({alertShowed: false, alertFor: false});
      this.props.navigation.navigate('Home');
    } else if (this.state.alertFor === 'invalidToken') {
      this.setState({alertShowed: false, alertFor: false});
      this.props.navigation.navigate('Login');
    } else {
      this.setState({alertShowed: false, alertFor: false});
    }
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false, alertFor: false});
  };

  render = () => {
    const {
      pickerLobItem,
      pickerLobSelected,
      pickerMerkSelected,
      pickerYearSelected,
      pickerTenorSelected,
      enableMerk,
      showResultSimulation,
      calculateResult,
    } = this.state;

    const {handleSubmit, submitting, getPengajuanSyanaLoading} = this.props;

    const SyanaMerk = syana.Merk;
    const SyanaYear = syana.Year;
    const SyanaTenor = syana.Tenor;

    return (
      <Container>
        <Spinner
          visible={getPengajuanSyanaLoading ? true : false}
          textContent={'Sedang memproses...'}
          textStyle={{color: '#FFF'}}
        />

        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />

        <Content disableKBDismissScroll={false}>
          <Field
            name="merkMotor"
            component={renderFieldPicker}
            enabled={enableMerk}
            label="Merk Motor"
            customError={this.state.errormerkMotor}
            pickerSelected={pickerMerkSelected}
            data={SyanaMerk.map((SyanaMerk) => (
              <Picker.Item label={SyanaMerk.Descr} value={SyanaMerk.Code} />
            ))}
            onValueChange={(newValue) => this.onMerkChange(newValue)}
          />

          <Field
            name="tahunMotor"
            component={renderFieldPicker}
            enabled={enableMerk}
            label="Tahun Motor"
            pickerSelected={pickerYearSelected}
            data={SyanaYear.map((SyanaYear) => (
              <Picker.Item label={SyanaYear.Descr} value={SyanaYear.Code} />
            ))}
            onValueChange={(newValue) => this.onYearChange(newValue)}
          />

          <Field
            name="plafonPinjaman"
            label="Jumlah Pinjaman"
            type="text"
            editable={true}
            component={renderField}
            format={fdeFormatCurrency}
            normalize={normalizeCurrency}
            keyboardType={'number-pad'}
            customError={this.state.errorplafonPinjaman}
            onChange={(newValue) => this.onHargaChange(newValue)}
          />

          <Field
            name="tenor"
            component={renderFieldPicker}
            label="Tenor"
            enabled={true}
            pickerSelected={pickerTenorSelected}
            data={SyanaTenor.map((SyanaTenor) => (
              <Picker.Item label={SyanaTenor.Descr} value={SyanaTenor.Code} />
            ))}
            onValueChange={(newValue) => this.onTenorChange(newValue)}
          />

          <TouchableOpacity
            style={styles.btnHitung}
            onPress={handleSubmit(this.submitCalculate)}
            disabled={submitting}>
            <Text style={styles.textButton}>Hitung Angsuran</Text>
          </TouchableOpacity>
          <View style={{height: 10}} />
          {showResultSimulation ? (
            <Card style={styles.simulation}>
              <Text style={styles.textHeader}>
                Estimasi Angsuran Bulanan Anda
              </Text>
              <Text style={styles.textBelowHeader}>
                Rp.{' '}
                {calculateResult
                  ? formatCurrency(calculateResult.toString())
                  : '-'}
              </Text>
              <Text style={styles.textDesc}>/bulan</Text>
              <View style={{height: 15}}></View>
              <Text style={styles.textDesc}>
                Perhitungan hanya bersifat simulasi,
              </Text>
              <Text style={styles.textDesc}>
                dapat berubah sesuai regulasi BAF
              </Text>
              <Text style={styles.textDesc}>
                Ajukan Aplikasi pembiayaan dan
              </Text>
              <Text style={styles.textDesc}>kami akan menghubungi Anda</Text>
              <TouchableOpacity
                style={styles.btnAjukan}
                onPress={handleSubmit(this.submitData)}>
                <Text style={styles.textButton}>Ajukan Sekarang</Text>
              </TouchableOpacity>
            </Card>
          ) : null}
        </Content>

        {this.showAlert()}
      </Container>
    );
  };
}

const selector = formValueSelector('formSyanaProduct');

function mapStateToProps(state) {
  return {
    merkMotor: selector(state, 'merkMotor'),
    tahunMotor: selector(state, 'tahunMotor'),
    plafonPinjaman: selector(state, 'plafonPinjaman'),
    tenor: selector(state, 'tenor'),
    detailUserResult: state.detailUser.result,
    formSyanaProduct: state.form.formSyanaProduct,
    getPengajuanSyanaResult: state.getPengajuanSyana.result,
    getPengajuanSyanaError: state.getPengajuanSyana.error,
    getPengajuanSyanaLoading: state.getPengajuanSyana.loading,
    refreshTokenResult: state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      detailUser,
      getPengajuanSyana,
      refreshToken,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

SyanaSimulation = reduxForm({
  form: 'formSyanaProduct',
  enableReinitialize: true,
  validate: FormSimulationSyanaValidate,
})(SyanaSimulation);

export default connect(mapStateToProps, matchDispatchToProps)(SyanaSimulation);
