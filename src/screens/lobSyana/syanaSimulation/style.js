import {StyleSheet} from 'react-native';
import {fonts} from '../../../utils/fonts';

export const styles = StyleSheet.create({
  card: {
    width: '100%',
    height: 53,
    marginTop: 0,
    marginLeft: 0,
  },
  simulation: {
    backgroundColor: '#f5f5f5',
    width: '85%',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 10,
    marginBottom: '5%',
    paddingBottom: 25,
  },
  headComponent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#f5f5f5',
    paddingHorizontal: '10%',
    paddingVertical: 5,
  },

  textButton: {
    fontFamily: fonts.primary.bold,
    textAlign: 'center',
    fontSize: 14,
    color: 'white',
  },
  textHeader: {
    lineHeight: 15,
    color: 'black',
    fontSize: 14,
    fontFamily: fonts.primary.bold,
    alignSelf: 'center',
    marginTop: 25,
  },
  textDesc: {
    fontSize: 12,
    fontFamily: fonts.primary.normal,
    color: 'black',
  },
  textBelowHeader: {
    color: '#1c1e61',
    fontSize: 20,
    fontFamily: fonts.primary.bold,
    alignSelf: 'center',
    marginVertical: 2,
  },

  btnAjukan: {
    alignSelf: 'center',
    width: '60%',
    height: 40,
    borderRadius: 10,
    fontFamily: fonts.primary.bold,
    backgroundColor: '#002f5f',
    justifyContent: 'center',
    marginTop: '20%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },

  btnHitung: {
    alignSelf: 'center',
    width: '80%',
    height: 40,
    fontFamily: fonts.primary.bold,
    borderRadius: 10,
    backgroundColor: '#002f5f',
    justifyContent: 'center',
    marginTop: '5%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },

  imageTop: {
    height: 45,
    width: 45,
    resizeMode: 'contain',
  },
  dropDownUp: {
    fontFamily: fonts.primary.normal,
    width: '70%',
    borderRadius: 5,
    borderWidth: 0.5,
    backgroundColor: 'white',
    height: 40,
    paddingRight: 5,
  },
  pickerUp: {
    width: '100%',
    height: 40,
  },

  ErrorDesc: {
    color: 'red',
    alignSelf: 'stretch',
    textAlign: 'right',
    fontSize: 10,
    fontFamily: fonts.primary.normal,
  },
});
