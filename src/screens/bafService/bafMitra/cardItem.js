import React from 'react';
import {TouchableOpacity, Text, View, Image} from 'react-native';

import {BAF_COLOR_BLUE} from '../../../utils/constant';
import {fonts} from '../../../utils/fonts';
import {Icon} from 'native-base';
import styles from './style';

export const cardItem = (data, navigation) => {
  console.log('test: ' + data.DealerImg);
  const imageError =
    'https://baf-mobile-dev.oss-ap-souheast-5.aliyuncs.com/Error';
  //url ini memicu defaultSouce keluar dari android device
  //image di android kalo uri-nya gak sesuai format (url, bukan empty string), dia cuman munculin kosong.

  return (
    <>
      <TouchableOpacity style={styles.card} onPress={() => navigation(data)}>
        <View style={{flexDirection: 'row', flex: 1}}>
          <View style={{marginLeft: 18}}>
            <Image
              resizeMode={'stretch'}
              style={styles.imageDealer}
              // source={{uri: data.DealerImg}}
              source={{
                uri:
                  data.DealerImg && data.DealerImg !== ''
                    ? data.DealerImg
                    : imageError,
              }}
              defaultSource={require('../../../../assets/img/home/defaultImageSmall.png')}
            />
          </View>
          <View style={{flexDirection: 'column', flex: 1, marginLeft: 10}}>
            <View>
              <Text style={styles.dealerTitle}>{data.DealerName}</Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 10,
                }}>
                <Icon type="Ionicons" name="call" style={styles.icon} />

                <Text
                  style={{
                    fontSize: 12,
                    color: BAF_COLOR_BLUE,
                    fontFamily: fonts.primary.normal,
                  }}>
                  {data.DealerPhoneno}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                <Icon type="MaterialIcons" name="map" style={styles.icon} />

                <Text numberOfLines={2} style={styles.textAddr}>
                  {data.DealerAddr}
                </Text>
              </View>
            </View>
            <View style={styles.distanceWrapper}>
              {/* <Icon type="MaterialIcons" name="place" style={styles.icon} />
              <Text style={styles.textDistance}>11,21 KM</Text> */}
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
};
