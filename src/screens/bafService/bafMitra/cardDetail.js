import {Container, Icon} from 'native-base';
import React, {Component} from 'react';
import {
  SafeAreaView,
  Text,
  Platform,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Linking,
} from 'react-native';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import {BAF_COLOR_BLUE} from '../../../utils/constant';
import {fonts} from '../../../utils/fonts';
import styles from './style';
import SendSMS from 'react-native-sms';
import openMap from 'react-native-open-maps';
import iconCall from '../../../../assets/img/home/services/icon-call.png';
import iconSms from '../../../../assets/img/home/services/icon-sms.png';
import iconMap from '../../../../assets/img/home/services/icon-maps.png';

import _ from 'lodash';
import HTML from 'react-native-render-html';
import {IGNORED_TAGS} from 'react-native-render-html/src/HTMLUtils';

const tags = _.without(
  IGNORED_TAGS,
  'table',
  'caption',
  'col',
  'colgroup',
  'tbody',
  'td',
  'tfoot',
  'th',
  'thead',
  'tr',
);

const tableDefaultStyle = {
  height: 20,
  // marginRight: 4,
};

const tableColumnStyle = {
  ...tableDefaultStyle,
  flexDirection: 'column',
  alignItems: 'stretch',
};

const tableRowStyle = {
  ...tableDefaultStyle,
  flexDirection: 'row',
  justifyContent: 'space-between',
};

const tdStyle = {
  ...tableDefaultStyle,
  padding: 2,
};

const thStyle = {
  ...tdStyle,
  backgroundColor: '#CCCCCC',
  alignItems: 'center',
};

const renderers = {
  table: (x, c) => <View style={tableColumnStyle}>{c}</View>,
  col: (x, c) => <View style={tableColumnStyle}>{c}</View>,
  colgroup: (x, c) => <View style={tableRowStyle}>{c}</View>,
  tbody: (x, c) => <View style={tableColumnStyle}>{c}</View>,
  tfoot: (x, c) => <View style={tableRowStyle}>{c}</View>,
  th: (x, c) => <View style={thStyle}>{c}</View>,
  thead: (x, c) => <View style={tableRowStyle}>{c}</View>,
  caption: (x, c) => <View style={tableColumnStyle}>{c}</View>,
  tr: (x, c) => <View style={tableRowStyle}>{c}</View>,
  td: (x, c) => <View style={tdStyle}>{c}</View>,
};

class cardDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Dealer/Mitra',
      data: '',
      coordinate: false,
    };
  }

  componentDidMount = async () => {
    const {data, coordinate} = this.props.route.params;
    console.log(data);
    console.log(coordinate);
    this.setState({data: data, coordinate: coordinate});
    this.setState({screenTitle: data.DealerName});
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  header = () => {
    return (
      <>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}
      </>
    );
  };

  imageHeader = () => {
    const {data} = this.state;

    const imageError =
      'https://baf-mobile-dev.oss-ap-souheast-5.aliyuncs.com/Error';

    const test = '';

    return (
      <>
        <View>
          <Image
            source={{
              uri:
                data.DealerImg && data.DealerImg !== ''
                  ? data.DealerImg
                  : imageError,
            }}
            defaultSource={require('../../../../assets/img/home/defaultImage.png')}
            style={{width: Dimensions.get('window').width, height: 300}}
          />
        </View>
      </>
    );
  };

  smsIOS = () => {
    //tested on real device bisa, emulator bakal munculin no name
    Linking.openURL(`sms:&addresses=${this.state.data.DealerPhoneno}&body=`);
  };

  smsAndroid = () => {
    SendSMS.send({
      body: '',
      recipients: [this.state.data.DealerPhoneno],
      successTypes: ['sent', 'queued'],
      allowAndroidSendWithoutReadPermission: true,
    }),
      (completed, cancelled, error) => {
        console.log(
          'SMS Callback: completed: ' +
            completed +
            ' cancelled: ' +
            cancelled +
            'error: ' +
            error,
        );
      };
  };

  topButton = () => {
    return (
      <>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`tel:${this.state.data.DealerPhoneno}`);
            }}
            style={styles.buttonTopDetail('#0EBD88')}>
            <Image
              source={iconCall}
              resizeMode={'contain'}
              style={{width: 35, height: 35}}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              Platform.OS === 'ios' ? this.smsIOS() : this.smsAndroid();
            }}
            style={styles.buttonTopDetail('#0971BD')}>
            <Image
              source={iconSms}
              resizeMode={'contain'}
              style={{width: 35, height: 35}}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              console.log(
                parseInt(this.state.data.DealerLatitude) +
                  ' ' +
                  parseInt(this.state.data.DealerLongitude),
              );
              openMap({
                latitude: parseFloat(this.state.data.DealerLatitude),
                longitude: parseFloat(this.state.data.DealerLongitude),
                zoom: 30,
              });
            }}
            style={styles.buttonTopDetail('#7CB342')}>
            <Image
              source={iconMap}
              resizeMode={'contain'}
              style={{width: 35, height: 35}}
            />
          </TouchableOpacity>
        </View>
      </>
    );
  };

  detailInformation = (data, iconType, iconName, type) => {
    return (
      <>
        <View
          style={{
            flex: 1,
            paddingHorizontal: Dimensions.get('screen').width * 0.1,
          }}>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 20,
            }}>
            <View style={{width: 20, marginRight: 20}}>
              <Icon
                type={iconType}
                name={iconName}
                style={{
                  fontSize: 18,
                  color: BAF_COLOR_BLUE,
                  marginTop: 3,
                }}
              />
            </View>

            {this.typetext(type, data)}
          </View>
        </View>
      </>
    );
  };

  typetext = (type, data) => {
    switch (type) {
      case 'alamat':
        return (
          <>
            <Text style={styles.textAlamatWrapper}>{data}</Text>
          </>
        );
      case 'jam':
        return typeof data === 'string' && data.includes('<') ? (
          <>
            <View
              style={{
                flex: 1,
                height: 60,
              }}>
              <HTML
                ignoredTags={tags}
                renderers={renderers}
                html={data}
                tagsStyles={{
                  td: {
                    fontFamily: fonts.primary.normal,
                    color: 'black',
                    fontSize: 16,
                  },
                }}
              />
            </View>
          </>
        ) : (
          <>
            <View style={{flex: 1}}>
              <View style={styles.textWrapper}>
                <Text style={styles.textDetail}>Hari Kerja</Text>
                <Text style={styles.textDetail}>-</Text>
              </View>
              <View style={styles.textWrapper}>
                <Text style={styles.textDetail}>Sabtu</Text>
                <Text style={styles.textDetail}>-</Text>
              </View>
              <View style={styles.textWrapper}>
                <Text style={styles.textDetail}>Hari Libur</Text>
                <Text style={styles.textDetail}>-</Text>
              </View>
              <Text style={styles.textDetail}>
                Jam operasional dealer dapat berubah tanpa konfirmasi
              </Text>
            </View>
          </>
        );
    }
  };

  mockingDesign = () => {};

  render() {
    return (
      <Container style={{flex: 1}}>
        {this.header()}
        <ScrollView style={{backgroundColor: 'white'}}>
          {this.imageHeader()}
          {this.topButton()}

          <View style={styles.whiteCover}>
            <View style={{marginBottom: 50}} />
            <View>
              {this.detailInformation(
                this.state.data.DealerAddr,
                'MaterialIcons',
                'map',
                'alamat',
              )}
              {this.detailInformation(
                this.state.data.DealerDesc,
                'MaterialIcons',
                'schedule',
                'jam',
              )}
              <View style={{height: 50}} />
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

export default cardDetail;
