import { StyleSheet, Dimensions } from 'react-native';
import {BAF_COLOR_BLUE} from '../../../../utils/constant';
const {width: WIDTH} = Dimensions.get('window');
import {fonts} from '../../../../utils/fonts';
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  searchBarWrapper: {
    // flex: 2,
    alignItems: 'center',
    backgroundColor: '#F6F6F6',
    marginBottom: 20,
    borderRadius: 6,
    marginHorizontal: 20,
    flexDirection: 'row'
  },

  wrapLoc: {
    // flex: 1, 
    justifyContent: 'center',
    alignItems: 'flex-start', 
    paddingLeft: '5%', 
    marginBottom: 10
  },

  loc : {
    fontSize: Dimensions.get('window').width > 360 ? 18 : 14,
    fontFamily: fonts.primary.normal,
    color:"black"
  },
  searchIcon :{
    fontSize: Dimensions.get('window').width > 360 ? 18 : 14,
    marginLeft: 20,
    color: '#B0B0B0',
  },
  searchInput :{
    height: 40,
    width: '100%',
    fontSize: Dimensions.get('window').width > 360 ? 18 : 14,
    fontFamily: fonts.primary.normal,
  },

  wrapRenderForm: {
    // flex: 2,
    marginHorizontal: '1%',
    marginTop: 10
  },

  wrapflatList :{
    flex:1,
    marginHorizontal:'5%',
    // backgroundColor: "grey",
  },

  wrapBtnCity :{
    flex:1,
    height:50,
    justifyContent:'center',
    alignItems:'center',
    // backgroundColor: "pink",
  },

  btnCity: (cityCode, data) =>({
    width: Dimensions.get('window').width > 360 ? 170 : 130, 
    height:40 ,
    backgroundColor:cityCode === data.code ? 'white' : '#F6F6F6',
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center',
    // shadowColor: "#000",
    shadowOffset: {width: 0,height: 1,},
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    // elevation: 3,
    borderColor: 'rgba(0, 0, 0, 0.8)',
    borderColor: cityCode === data.code ? BAF_COLOR_BLUE : 'white',
    borderWidth: 1,
  }),

  textbtnCity:{
    fontSize:12,
    // color:'rgba(0,0,0,0.7)',
    color:'black',
    fontFamily: fonts.primary.normal,

  },

  wrapIconCheck :{
    width:15,
    height:15,
    backgroundColor:BAF_COLOR_BLUE,
    borderRadius:100,
    position:'absolute',
    top:5,
    right:5,
    justifyContent:'center',
    alignItems:'center',
  },

  wrapbtnBoth:{
    // flex:1.5,
    height:50,
    flexDirection:'row',
    marginHorizontal:'5%',
    marginTop: 15,
    marginBottom:15,
    // backgroundColor:"yellow",
  },

  wrapbtnAturUlang:{
    flex:1.5,
    justifyContent:'center',
    alignItems:'center',
  },

  btnAturUlang:{
    width:'75%',
    height:40,
    backgroundColor:'#F6F6F6',
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center',
    shadowColor: "#000",
    shadowOffset: {width: 0,height: 1,},
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3
  },
  
  wrapbtnPakai:{
    flex:1.5,
    justifyContent:'center',
    alignItems:'center'
  },

  btnPakai:{
    width:'75%',
    height:40,
    backgroundColor:BAF_COLOR_BLUE,
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center'
  },

})