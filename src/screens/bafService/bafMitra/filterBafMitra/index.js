import React, {Component} from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  BackHandler,
  Platform,
  FlatList,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import {Container, Icon, Spinner as SpinnerNative} from 'native-base';
import styles from './style';
import SubHeaderAndroid from '../../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../../components/ios/subHeaderBlue'
import { BAF_COLOR_BLUE } from '../../../../utils/constant';
import DATA from '../city.json';
import SearchInput, { createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['code', 'name'];
const emptyData = [];

class FilterBafMitra extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Filter',
      cityCode: null,
      searchTerm: ''
    };
  }

  componentDidMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.goBack);
  };

  componentDidUpdate = async (prevProps, prevState) => {
    
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  renderHeader = () => {
    return (
      <>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}
      </>
    );
  };

  renderForm = () => {
    return (
      <>
        <View style={styles.wrapRenderForm}>
          <View style={styles.wrapLoc}>
            <Text style={styles.loc}>Lokasi</Text>
          </View>
          <View style={styles.searchBarWrapper}>
            <Icon
              name="search-outline"
              style={styles.searchIcon}
            />
            <SearchInput 
              onChangeText={(term) => { this.searchUpdated(term) }} 
              style={styles.searchInput}
              placeholder="Cari Kota"
            />
          </View>
         
        </View>
        
      </>
    );
  };

  handleChooseCity = (id) => {
    this.setState({
      cityCode : id
    })
  }

  renderItem = (item) => {
    const data = item.item;
    const {cityCode} = this.state; 
    return(
      <View style={styles.wrapBtnCity}>
        <TouchableOpacity 
          onPress = {() => {this.handleChooseCity(data.code)}}
          style={styles.btnCity(cityCode, data )}>
          <Text style={styles.textbtnCity}>{data.name}</Text>
          {cityCode !== null ? cityCode === data.code ? (
            <View style={styles.wrapIconCheck}>
              <Icon type="Ionicons" name="checkmark-outline" style={{fontSize:12,color:'white'}} />
            </View>
          ):null : null}
          
        </TouchableOpacity>
      </View>
    )
    
  }

  renderList = () => {
    const {searchTerm} = this.state;
    const filteredData = DATA.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
    return(
      <View  style={styles.wrapflatList}>
         <FlatList
          data={searchTerm !== "" ? filteredData : emptyData}
          numColumns={2}
          renderItem={this.renderItem}
          keyExtractor={item => item.code}
          scrollEnabled={true}
        />
      </View>
    )
  }

  handleAturUlang = () => {
    this.props.navigation.navigate('FilterBafMitra')
  }

  renderFooter = () => {
    return(
      <View style={styles.wrapbtnBoth}>
        <View style={styles.wrapbtnAturUlang}>
          <TouchableOpacity 
            onPress = {() => {this.handleAturUlang()}}
            style={styles.btnAturUlang}>
            <Text style={{fontWeight:'600'}}>Atur Ulang</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.wrapbtnPakai}>
          <TouchableOpacity 
            onPress={() => {this.props.navigation.navigate("BafMitra")}}
            style={styles.btnPakai}>
            <Text style={{color:'white',fontWeight:'600'}}>Pakai</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  searchUpdated(term) {
    this.setState({ 
      searchTerm: 
      term 
    })
  }

  render = () => {
    
    return (
      <Container style={styles.container}>
        {this.renderHeader()}
        {this.renderForm()}
        {this.renderList()}
        {this.renderFooter()}
      </Container>
    );
  };
}

function mapStateToProps(state) {
  return {
    
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {},
    dispatch,
  );
}

export default connect(mapStateToProps, matchDispatchToProps)(FilterBafMitra);
