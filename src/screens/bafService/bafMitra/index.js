import React, {Component} from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  Linking,
  BackHandler,
  StyleSheet,
  Platform,
  PermissionsAndroid,
  SafeAreaView,
  Dimensions,
  Image,
  ToastAndroid,
  PixelRatio,
  TextInput,
  ScrollView,
  Alert,
} from 'react-native';

import Toast from 'react-native-easy-toast';
import Geolocation from 'react-native-geolocation-service';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import {cardItem} from './cardItem';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import ILmotor from '../../../../assets/img/home/example-motoryamaha.png';
import ILmitra from '../../../../assets/img/home/dealer.png';
import {Container, Icon, Spinner as SpinnerNative} from 'native-base';
import {getListDealer} from '../../../actions/home';
import styles from './style';
import {hasLocationPermission} from '../../../utils/utilization';
import {BAF_COLOR_BLUE} from '../../../utils/constant';

class BafMitra extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Dealer/Mitra',
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      detailUser: false,
      isConnected: false,
      active: 'Dealer Motor',
      listDealer: false,
      filteredData: false,
      input: '',
      coordinate: false,
    };
  }

  componentDidMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.goBack);
    const hasLocationPermissionResult = await hasLocationPermission();
    // const hasLocationPermissionResult = true;
    const detailUserNonParse = await AsyncStorage.getItem('detailUser');

    //UNTUK IOS DI EMULATOR, KE FEATURES, LOCATION, CUSTOM LOCATION. MASUKIN LATITUDE SAMA LONGITUDENYA
    //JANGAN DIHAPUS FUNCTION INI

    if (hasLocationPermissionResult) {
      Geolocation.getCurrentPosition(
        (position) => {
          const dataMerge = {
            Latitude: position.coords.latitude,
            Longitute: position.coords.longitude,
          };
          console.log(dataMerge);

          this.props.getListDealer(dataMerge);
        },
        (error) => {
          // See error code charts below.
          this.setState({
            alertFor: 'getListDealer_error',
            alertShowed: true,
            alertMessage: 'Gagal mengakses fitur lokasi perangkat',
            alertTitle: 'Gagal',
            alertType: 'error',
            alertDoneText: 'OK',
            isModalVisible: false,
          });
          console.log(error.code, error.message);
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } else {
      this.setState({
        alertFor: 'getListDealer_error',
        alertShowed: true,
        alertMessage:
          'Gagal mengakses fitur lokasi perangkat, \nmohon aktifkan fitur lokasi Anda',
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
        isModalVisible: false,
      });
    }

    if (detailUserNonParse) {
      const detailUserParse = JSON.parse(detailUserNonParse);
      this.setState({detailUser: detailUserParse});
      console.log(this.state.detailUser);
    }

    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) {
      this.setState({userToken: userToken});
    }
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      getListDealer_result,
      getListDealer_error,
      getListDealer_loading,
    } = this.props;

    if (
      getListDealer_error !== null &&
      prevProps.getListDealer_error !== getListDealer_error
    ) {
      this.setState({
        alertFor: 'getListDealer_error',
        alertShowed: true,
        alertMessage: getListDealer_error.message,
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
        isModalVisible: false,
      });
    }

    if (
      getListDealer_loading !== null &&
      prevProps.getListDealer_loading !== getListDealer_loading
    ) {
      console.log('test');
    }

    if (
      getListDealer_result !== null &&
      prevProps.getListDealer_result !== getListDealer_result
    ) {
      var array = getListDealer_result.AllData;

      this.setState({listDealer: array, filteredData: array});
    }
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
    if (this.state.alertFor === 'getListDealer_error') {
      this.props.navigation.navigate('Home');
    }
    // if (this.state.alertFor === 'updateStatusOrderPrivy_result') {
    //   this.props.navigation.navigate('Home');
    // } else {
    //   this.setState({screenTitle: 'Persetujuan'});
    // }
    this.setState({alertFor: null});
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  goHome = async () => {
    this.props.navigation.navigate('Home');
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  changeTab = async (res) => {
    this.setState({active: res});
  };

  listData = (data) => {
    return (
      <>
        {data.map((element) => {
          return cardItem(element, (data) =>
            this.props.navigation.navigate('MitraDetail', {
              data: data,
              coordinate: this.state.coordinate,
            }),
          );
        })}
      </>
    );
  };

  dataFilter = async (input) => {
    var result = this.state.listDealer.filter((item) =>
      item.DealerName.toLowerCase().includes(input.toLowerCase()),
    );
    await this.setState({filteredData: result});
  };

  //***********COMPONENT */

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  header = () => {
    return (
      <>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}
      </>
    );
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        testIdPositiveButton="OK"
        testIdNegativeButton="Cancel"
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  toast = () => {
    return (
      <>
        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />
      </>
    );
  };

  searchBar = () => {
    return (
      <>
        <View style={{flex: 1, flexDirection: 'row', marginHorizontal: '1%'}}>
          <View style={styles.searchBarWrapper}>
            <Icon
              name="search-outline"
              style={{
                fontSize: 18,
                marginLeft: 20,
                color: '#B0B0B0',
              }}
            />
            <TextInput
              style={{
                height: 40,
                width: '90%',
              }}
              onChangeText={this.dataFilter}
              placeholder="Cari Dealer/Mitra"
            />
          </View>
          <View style={{flex: 1}}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('FilterBafMitra');
              }}
              style={{width: '80%', height: '65%', flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  type="Ionicons"
                  name="filter-outline"
                  style={{fontSize: 22}}
                />
              </View>
              <View
                style={{
                  flex: 2,
                  justifyContent: 'center',
                  alignItems: 'flex-start',
                }}>
                <Text style={{fontSize: 14, color: BAF_COLOR_BLUE}}>
                  Filter
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </>
    );
  };

  buttonTabComponent = (image, active, label, onPress) => {
    return (
      <>
        <TouchableOpacity
          activeOpacity={0}
          style={styles.buttonTopBar(active === label ? true : false)}
          onPress={() => onPress(label)}>
          <Image
            resizeMode="contain"
            style={{width: '50%', height: '50%'}}
            source={image}
          />

          <View style={{justifyContent: 'center', height: 40}}>
            <Text style={styles.labelButtonTop}>{label}</Text>
          </View>
        </TouchableOpacity>
      </>
    );
  };

  dealerMitraRender = () => {
    return (
      <>
        <ScrollView>
          <View style={{paddingTop: 20}}>
            {this.searchBar()}
            <View style={styles.buttonTopWrapper}>
              {this.buttonTabComponent(
                ILmotor,
                this.state.active,
                'Dealer Motor',
                this.changeTab,
              )}
              {this.buttonTabComponent(
                ILmitra,
                this.state.active,
                'Mitra BAF',
                this.changeTab,
              )}
            </View>
            <View style={{paddingHorizontal: 20}}>
              {this.state.listDealer ? (
                <>{this.listData(this.state.filteredData)}</>
              ) : (
                <>
                  <SpinnerNative color="blue" />
                </>
              )}
            </View>
          </View>
        </ScrollView>
      </>
    );
  };

  render = () => {
    return (
      <Container style={styles.container}>
        {this.header()}
        {this.toast()}
        {this.showAlert()}
        {this.dealerMitraRender()}
      </Container>
    );
  };
}

function mapStateToProps(state) {
  return {
    getListDealer_result: state.getListDealer.result,
    getListDealer_error: state.getListDealer.error,
    getListDealer_loading: state.getListDealer.loading,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getListDealer,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, matchDispatchToProps)(BafMitra);
