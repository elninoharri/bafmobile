import React from "react";
import {Container, Content, Accordion, Icon} from "native-base";
import {ImageBackground, Platform, TouchableOpacity} from "react-native";
import SubHeaderAndroid from "../../../components/android/subHeaderBlue";
import SubHeaderIos from "../../../components/ios/subHeaderBlue";
import {Text, View, Image} from "react-native";
import data from './termBafInfo.json'
import styles from './styles';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getBafPointValueAction} from "../../../actions/bafPoint";
import HTML from "react-native-render-html";

class BafPointsInfo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            screenTitle: 'BAFPOINTS',
            buttonReferral: 'Referal Saya',
            category: 'KATEGORI'
        }
    }



    componentDidMount() {

    }

    goBack = async () => {
        this.props.navigation.goBack();
    }

    _renderHeader(item, expanded) {
        return (
            <View style={{paddingBottom: 10}}>
                <View style={styles.viewRenderHeader}>
                    <View style={styles.viewTitleRenderHeader}>
                        <View style={styles.viewIconRenderHeader}>
                            <Image style={{
                                width: 28,
                                height: 28,
                            }} source={{uri: `data:image/png;base64,${item.icon}`}}
                            />
                        </View>
                        <Text style={styles.textRenderHeader}>{item.title}</Text>
                    </View>
                        {expanded ? (
                            <Icon style={{fontSize: 18}} name="chevron-up" />
                        ) : (
                            <Icon style={{fontSize: 18}} name="chevron-down" />
                        )}
                </View>
            </View>
        );
    }

    _renderContent(item) {
        if (item.id === 1) {
            return (
                <View style={{paddingBottom: 10}}>
                    <View style={styles.viewRenderContent}>
                        <View style={styles.viewInnerRenderContent}>
                        <Text style={styles.textRenderContent}>{item.desc}</Text>
                        {item.points.map((point) => {
                            return (
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={styles.textRenderContentBullet}>{'\u2B24\t'}</Text>
                                        <HTML html={point} baseFontStyle={styles.textRenderContentBulletItem} />
                                    </View>
                            );
                        })}
                        </View>
                    </View>
                </View>
            )
        } else if (item.id === 2) {
            return (
                <View style={{paddingBottom: 10}}>
                    <View style={styles.viewRenderContent}>
                        {item.points.map((point, index) => {
                            return (
                                <View style={styles.viewInnerRenderContent}>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={styles.textRenderContentBullet}>{index + 1 + '. '}</Text>
                                        <HTML html={point} baseFontStyle={styles.textRenderContentBulletItem} />
                                    </View>
                                </View>
                            );
                        })}
                    </View>
                </View>
            )
        } else if (item.id === 3) {
            return (
                <View style={{paddingBottom: 10}}>
                    <View style={styles.viewRenderContent}>
                        <View style={styles.viewInnerRenderContent}>
                            <View style={{alignItems: 'center'}}>
                                <Text style={styles.textRenderContent2}>{item.desc + '\n'}</Text>
                                <Image style={{
                                    width: '100%',
                                    height: 52
                                    }} source={require('../../../../assets/img/bafPoint/perhitunganbafpoint.png')}
                                />
                            </View>
                        </View>
                    </View>
                </View>
            )
        } else if (item.id === 4) {
            return (
                <View style={{paddingBottom: 10}}>
                    <View style={styles.viewRenderContent}>
                        {item.points.map((point, index) => {
                            return (
                                <View style={styles.viewInnerRenderContent}>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={styles.textRenderContentBullet}>{index + 1 + '. '}</Text>
                                        <Text style={styles.textRenderContentBulletItem}>{point}</Text>
                                    </View>
                                </View>
                            );
                        })}
                    </View>
                </View>
            )
        } else if (item.id === 5) {
            return (
                <View style={{paddingBottom: 10}}>
                    <View style={styles.viewRenderContent5}>
                        {item.points.map((question) => {
                            return (
                                <View style={styles.viewInnerRenderContent5}>
                                    <Text style={styles.textRenderContent5}>{question.question}</Text>
                                    {question.answer.map((answer, index) => (
                                        <View style={{flexDirection: 'row'}}>
                                            <Text style={styles.textRenderContentBullet}>{index + 1 + '. '}</Text>
                                            <Text style={styles.textRenderContentBulletItem}>{answer}</Text>
                                        </View>
                                    )
                                    )}
                                </View>
                            );
                        })}
                    </View>
                </View>
            )
        } else if (item.id === 6) {
            return (
                <View style={{paddingBottom: 10}}>
                    <View style={styles.viewRenderContent}>
                        <View style={styles.viewInnerRenderContent}>
                            <View style={{alignItems: 'center'}}>
                                    <Image style={{
                                        width: 200,
                                        height: 65
                                    }} source={require('../../../../assets/img/bafPoint/infokontakbafpoint.png')} />
                            </View>
                        </View>
                    </View>
                </View>
            )
        }
    }

    render() {
        return (
            <Container>
                {Platform.OS === 'android' ? (
                    <SubHeaderAndroid
                        title={this.state.screenTitle}
                        goBack={this.goBack}
                    />
                ) : (
                    <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
                )}
                    
                <ImageBackground style={styles.backgroundImage} source={require('../../../../assets/img/bafPoint/BafPointBG.png')}>
                    <View style={styles.viewBafPoint}>
                        <View>
                            <View style={styles.viewBafCoin}>
                                <Image style={styles.imageBafCoin} source={require('../../../../assets/img/home/bafCoin.png')} />
                                <Text style={styles.textBafPoints}>{this.state.screenTitle}</Text>
                            </View>
                            <Text style={styles.textValuePoint}>1.012.000</Text>
                            <TouchableOpacity
                                onPress={() => {console.log('REFERRAL')}}
                                style={styles.btnReferral}
                            >
                                <Text style={styles.textReferral}>
                                    {this.state.buttonReferral}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.viewRedeemPoint}>
                            <TouchableOpacity style={styles.btnRefresh} >
                                <Image style={styles.imageRefresh} source={require('../../../../assets/img/bafPoint/refresh.png')} />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.btnRedeem} >
                                <Text style={styles.textRedeem}>Redeem Poin</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.viewBodyContent}>
                        <Text style={styles.textCategory}>{this.state.category}</Text>
                    </View>
                    <View style={styles.background2}>
                        <Content>
                            <View style={styles.viewAccordion}>
                                    <Accordion
                                        dataArray={data.paragraph}
                                        renderHeader={this._renderHeader}
                                        renderContent={this._renderContent}
                                        expanded={true}
                                        style={{borderWidth: 0}}
                                    />
                            </View>
                        </Content>
                    </View>



                {/* <View style={styles.containerCategories}>
                    <Text style={styles.fontCategory}>{this.state.category}</Text>
                    <Content>
                    <Accordion
                    dataArray={data.paragraph}
                    renderHeader={this._renderHeader}
                    renderContent={this._renderContent}
                    expanded={true}
                    />
                    </Content>
                </View> */}
                </ImageBackground>

            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        bafPointValue: state.getBafPointValueReducer.bafPointValue
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getBafPointValueAction,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(BafPointsInfo);
