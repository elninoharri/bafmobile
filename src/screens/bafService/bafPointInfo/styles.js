import {StyleSheet} from "react-native";
import {fonts} from "../../../utils/fonts";

export default StyleSheet.create({
    backgroundImage: {
        width: '100%',
        height: '94%'
    },

    background2: {
        width: '100%',
        height: '58%',
        backgroundColor: '#F6F5F5',
    },

    viewBafPoint: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        width: '100%',
        height: '30%'
    },

    viewBafCoin: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    imageBafCoin: {
        width: 29,
        height: 29,
    },

    textBafPoints: {
        fontFamily: fonts.primary.bold,
        fontSize: 17,
        color: '#FFFFFF'
    },

    textValuePoint: {
        fontFamily: fonts.primary.bold,
        fontSize: 28,
        color: '#FFFFFF',
        paddingBottom: 10
    },

    viewRedeemPoint: {
        flexDirection: 'row',
        alignItems: 'center'
    },

    btnReferral: {
        backgroundColor: '#05478D',
        borderRadius: 4,
        shadowOpacity: 0.3,
        shadowRadius: 10,
        elevation: 10,
        padding: 10,
        alignItems: 'center'
    },

    textReferral: {
        color: 'white',
        fontSize: 16,
        fontFamily: fonts.primary.normal,
    },

    btnRefresh: {
        backgroundColor: '#076FD9',
        borderRadius: 20,
        padding: 5,
        shadowOpacity: 0.3,
        shadowRadius: 10,
        elevation: 10,
        width: 28,
        height: 28
    },

    imageRefresh: {
        width: 18,
        height: 18,
    },

    btnRedeem: {
        paddingLeft: 10,
    },

    textRedeem: {
        color: 'white',
        borderRadius: 5,
        padding: 10,
        shadowOpacity: 0.3,
        shadowRadius: 10,
        elevation: 10,
        alignItems: 'center',
        backgroundColor: '#076FD9',
        fontSize: 16,
        fontFamily: fonts.primary.normal,
    },
    
    viewBodyContent: {
        backgroundColor: '#F6F5F5',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        padding: 30
    },

    textCategory: {
        paddingBottom: 15,
        fontFamily: fonts.primary.bold,
        fontSize: 17,
        color: '#9B9B9B'
    },

    viewBodyContent: {
        backgroundColor: '#F6F5F5',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        padding: 30,
        paddingBottom: 10
    },

    viewAccordion: {
        backgroundColor: '#F6F5F5',
        paddingTop: 0,
        padding: 30,
    },

    viewRenderHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 16,
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        alignItems: 'center',
    },

    viewTitleRenderHeader: {
        alignItems: 'center',
        flexDirection: 'row',
    },

    viewIconRenderHeader: {
        paddingRight: 12
    },

    textRenderHeader: {
        fontFamily: fonts.primary.bold,
        fontStyle: 'normal',
        fontSize: 16,
        width: '80%',
        color: 'rgba(0, 0, 0, 0.6)'
    },

    viewRenderContent: {
        paddingVertical: 12,
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
    },

    viewRenderContent5: {
        paddingTop: 12,
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
    },

    viewInnerRenderContent: {
        paddingHorizontal: 12
    },

    viewInnerRenderContent5: {
        paddingHorizontal: 12,
        paddingBottom: 12
    },
    
    textRenderContent: {
        fontFamily: fonts.primary.normal,
        textAlign: 'justify',
        fontSize: 11.5,
        width: '100%',
        color: 'rgba(0, 0, 0, 0.7)',
    },

    textRenderContent2: {
        fontFamily: fonts.primary.normal,
        textAlign: 'justify',
        fontSize: 11.5,
        color: 'rgba(0, 0, 0, 0.7)',
    },

    textRenderContent5: {
        fontFamily: fonts.primary.bold,
        textAlign: 'auto',
        fontSize: 11.5,
        color: 'rgba(0, 0, 0, 0.7)'
    },

    textRenderContentBullet: {
        fontFamily: fonts.primary.normal,
        textAlign: 'justify',
        fontSize: 11.5,
        color: 'rgba(0, 0, 0, 0.7)',
    },

    textRenderContentBulletItem: {
        fontFamily: fonts.primary.normal,
        textAlign: 'justify',
        fontSize: 11.5,
        width: '92%',
        color: 'rgba(0, 0, 0, 0.7)',
    },

})
