import React, {Component} from 'react';
import {
    Text,
    BackHandler, Platform, Dimensions, PixelRatio,
} from 'react-native';
import {View} from "react-native-animatable";
import {styles} from "./styles";
import {Accordion, Container, Content, Icon} from "native-base";
import analytics from "@react-native-firebase/analytics";
import NetInfo from "@react-native-community/netinfo";
import Toast from "react-native-easy-toast";
import data from "./term.json";
import SubHeaderAndroid from "../../../components/android/subHeaderBlue";
import SubHeaderIos from "../../../components/ios/subHeaderBlue";

class BafPoin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            screenTitle: 'Syarat dan Ketentuan BAFPOINTS',
            isConnected: false,
        };
    }

    componentDidMount = async () => {
        await analytics().logScreenView({
            screen_class: 'screenTermCondition',
            screen_name: 'screenTermCondition',
        });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.checkInternet();
    };

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        if (this.props.navigation.isFocused()) {
            this.goBack();
            return true;
        }
    };

    checkInternet = () => {
        NetInfo.fetch().then((state) => {
            this.setState({isConnected: state.isConnected});
            if (!this.state.isConnected) {
                this.showToastNoInternet();
            }
        });
    };

    showToastNoInternet = () => {
        this.refs.toast.show(
            <View style={{flexDirection: 'row'}}>
                <Icon
                    type="FontAwesome"
                    name="exclamation-circle"
                    style={{color: 'red', marginLeft: '-10%'}}
                />
                <Text
                    style={{
                        marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
                        marginLeft: '3%',
                        fontSize: 16,
                        color: 'red',
                    }}>
                    Tidak Ada Koneksi Internet
                </Text>
            </View>,
            DURATION.LENGTH_LONG,
        );
    };

    goBack = async () => {
        this.props.navigation.goBack();
    };

    renderHeader(item, expanded) {
        return (
            <View>
                {item.id % 2 ? (
                    <View style={styles.renderHeader}>
                        <Text style={styles.title}>{item.title}</Text>
                        {expanded ? (
                            <Icon style={{fontSize: 18}} name="remove" />
                        ) : (
                            <Icon style={{fontSize: 18}} name="add" />
                        )}
                    </View>
                ) : (
                    <View style={styles.renderHeader2}>
                        <Text style={styles.title}>{item.title}</Text>
                        {expanded ? (
                            <Icon style={{fontSize: 18}} name="remove" />
                        ) : (
                            <Icon style={{fontSize: 18}} name="add" />
                        )}
                    </View>
                )}
            </View>
        );
    }

    renderContent = (content) => {
        let number = 0;
        return content.content.map((item) => {
            number++;
            if (content.content.length !== 0) {
                return (
                    <View style={{backgroundColor: '#F4F4F4'}}>
                        <View style={{marginTop: 4, marginBottom: 4}}>
                            <View style={styles.row}>
                                <View style={{width: 20}}>
                                    <Text style={styles.textDefault}>{number + '. '}</Text>
                                </View>
                                <View style={{flex: 1}}>
                                    <Text style={styles.textDefault}>{item}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                );
            } else {
                return <View></View>;
            }
        });
    };

    toastComponent = () => {
        return (
            <Toast
                ref="toast"
                style={{
                    marginTop:
                        Platform.OS !== 'android'
                            ? Dimensions.get('window').width * PixelRatio.get() > 750
                            ? 44
                            : 20
                            : 0,
                    backgroundColor: '#FFE6E9',
                    width: '100%',
                    borderBottomWidth: 2,
                    borderBottomColor: 'red',
                    justifyContent: 'center',
                    paddingLeft: 50,
                    height: 50,
                    borderRadius: 0,
                }}
                position="top"
                positionValue={0}
                fadeInDuration={2000}
                fadeOutDuration={1000}
                opacity={0.9}
            />
        );
    };

    render() {
        const head = data.head.map((result) => {
            if (result.desc.length !== 0) {
                return <Text style={styles.textHead}> {result.desc}</Text>;
            }
        });

        return (
            <Container>
                {Platform.OS === 'android' ? (
                    <SubHeaderAndroid
                        title={this.state.screenTitle}
                        goBack={this.goBack}
                    />
                ) : (
                    <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
                )}

                <Content>
                    {this.toastComponent()}
                    <View style={styles.head}>{head}</View>

                    <Accordion
                        dataArray={data.paragraph}
                        renderHeader={this.renderHeader}
                        renderContent={this.renderContent}
                        expanded={0}
                        icon={'add'}
                        expandedIcon={'remove'}
                    />
                </Content>
            </Container>
        );
    }
}

export default BafPoin;
