import { StyleSheet, Dimensions } from 'react-native';
import { fonts } from '../../../utils/fonts';

export const styles = StyleSheet.create({
  content: {
    fontFamily: fonts.primary.normal,
    fontSize: 14,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    marginLeft: 18,
    marginRight: 29,
  },
  head: {
    marginLeft: 32,
    marginRight: 31,
    marginTop: 31,
    marginBottom: 10,
  },
  textHead: {
    textAlign: 'justify',
    lineHeight: 25,
    fontFamily: fonts.primary.normal,
  },

  title: {
    fontFamily: fonts.primary.bold,
    fontSize: 16,
    marginLeft: 16,
  },

  subtitle: {
    fontFamily: fonts.primary.bold,
    fontSize: 14,
    marginLeft: 17,
    marginBottom: 5,
  },

  subContent: {
    width: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#C4C4C4',
    borderRadius: 10,
    height: 20,
    marginRight: 15,
  },

  titleheadsub1: {
    fontFamily: fonts.primary.normal,
    fontSize: 14,
  },

  renderHeader: {
    flexDirection: 'row',
    paddingVertical: 11,
    paddingHorizontal: '14%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  renderHeader2: {
    flexDirection: 'row',
    padding: 18,
    borderBottomColor: 'rgba(0, 0, 0, 0.05)',
    borderBottomWidth: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
  },

  renderHeadsub2: {
    flexDirection: 'row',
    padding: 18,
    borderBottomColor: 'rgba(0, 0, 0, 0.05)',
    borderBottomWidth: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  textDefault: {
    fontFamily: fonts.primary.normal,
    // color: 'rgba(0, 0, 0, 0.8)',
    textAlign: 'justify',
  },

  image: {
    width: 33,
    height: 20,
    // flex: 1,
    resizeMode: 'contain',
  },
  imageHeadSub1: {
    marginRight: 20,
    width: 33,
    height: 20,
    resizeMode: 'contain',
  },


});
