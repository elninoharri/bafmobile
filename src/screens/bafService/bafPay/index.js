import React, {Component} from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  SafeAreaView,
  Platform,
  ScrollView,
  Dimensions,
  NativeModules,
  PixelRatio,
  Image,
  useWindowDimensions,
  Linking,
} from 'react-native';
import {
  Container,
  Row,
  Accordion,
  Input,
  Item,
  Label,
  Header,
  Left,
  Right,
  Button,
  Body,
  Title,
  Icon,
  Picker,
  Card,
  Content,
} from 'native-base';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import Toast from 'react-native-easy-toast';
import NetInfo from '@react-native-community/netinfo';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import ILtransfer from '../../../../assets/img/bafpay/Transfer_Bank.png';
import ILgerai from '../../../../assets/img/bafpay/Gerai_Retail.png';
import ILpgm from '../../../../assets/img/bafpay/pgm.png';
import ILshopee from '../../../../assets/img/bafpay/Shopee.png';
import ILtokopedia from '../../../../assets/img/bafpay/Tokopedia.png';
import ILbukalapak from '../../../../assets/img/bafpay/Bukalapak.png';
import ILkantorbank from '../../../../assets/img/bafpay/kantorbank.png';
import ILkantorcab from '../../../../assets/img/bafpay/BAF.png';
import ILbca from '../../../../assets/img/bafpay/BCA.png';
import ILbri from '../../../../assets/img/bafpay/BRI.png';
import ILbni from '../../../../assets/img/bafpay/BNI.png';
import ILmandiri from '../../../../assets/img/bafpay/MANDIRI.png';
import {BAF_COLOR_BLUE} from '../../../utils/constant';
import HTML from 'react-native-render-html';
import data from './bafPay.json';
import {styles} from './style';
import {color, log} from 'react-native-reanimated';
import {regexBackslash} from '../../../utils/utilization';
import {fonts} from '../../../utils/fonts';

class BafPay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Cara Pembayaran',
      isConnected: false,

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: '',
      alertDoneText: '',
      alertCancelText: '',
    };
  }
  componentDidMount = async () => {};

  componentDidUpdate = async () => {};

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected});
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  _renderHeader(item, expanded) {
    return (
      <View style={styles.renderHeader2}>
        <View>
          <Row>
            {item.id == 1 ? (
              <Image style={styles.image} source={ILtransfer} />
            ) : item.id == 2 ? (
              <Image style={styles.image} source={ILgerai} />
            ) : item.id == 3 ? (
              <Image style={styles.image} source={ILpgm} />
            ) : item.id == 4 ? (
              <Image style={styles.image} source={ILshopee} />
            ) : item.id == 5 ? (
              <Image style={styles.image} source={ILtokopedia} />
            ) : item.id == 6 ? (
              <Image style={styles.image} source={ILbukalapak} />
            ) : item.id == 7 ? (
              <Image style={styles.image} source={ILkantorcab} />
            ) : (
              <Image style={styles.image} source={ILkantorbank} />
            )}
            <View>
              <Row>
                <Text style={styles.title}>{item.title}</Text>
                {expanded ? (
                  <View
                    style={{
                      marginLeft: Dimensions.get('window').width * 0.65,
                      position: 'absolute',
                    }}>
                    <Icon
                      style={{fontSize: 20, color: BAF_COLOR_BLUE}}
                      name="ios-checkmark-circle-outline"
                    />
                  </View>
                ) : (
                  <View></View>
                )}
              </Row>
            </View>
          </Row>
        </View>

        {expanded ? (
          <Icon style={{fontSize: 18}} name="chevron-up" />
        ) : (
          <Icon style={{fontSize: 18}} name="chevron-down" />
        )}
      </View>
    );
  }

  htmlRender = (htmlContent) => {
    return (
      <HTML
        html={htmlContent}
        containerStyle={{paddingHorizontal: 40, marginBottom: 20}}
        tagsStyles={{p: {marginTop: 10}}}
        imagesMaxWidth={Dimensions.get('window').width * 0.8}
        baseFontStyle={{fontSize: 16, fontFamily: fonts.primary.normal}}
        onLinkPress={(event, href) => {
          Linking.openURL(href);
        }}
      />
    );
  };

  htmlRenderEcommerce = (htmlContent) => {
    return (
      <HTML
        html={htmlContent}
        containerStyle={{
          paddingHorizontal: 0,

          marginBottom: 0,
          flex: 1,
        }}
        listsPrefixesRenderers={{
          ol: (htmlAttribs, children, convertedCSSStyles, passProps) => {
            const {allowFontScaling, baseFontStyle, index} = passProps;
            const baseFontSize = baseFontStyle.fontSize || 14;

            const bulletNumber = htmlAttribs.start
              ? htmlAttribs.start
              : index + 1;
            return (
              <View style={styles.subContent}>
                <Text style={[styles.textDefault, {color: 'white'}]}>
                  {bulletNumber}
                </Text>
              </View>
            );
          },
        }}
        tagsStyles={{
          p: {marginBottom: 20},
        }}
        imagesMaxWidth={Dimensions.get('window').width * 0.8}
        baseFontStyle={{fontFamily: fonts.primary.normal, textAlign: 'justify'}}
        onLinkPress={(event, href) => {
          Linking.openURL(href);
        }}
      />
    );
  };

  _renderContent = (content) => {
    if (content.lenght !== 0) {
      if (content.content[0].type == 'HTML') {
        const htmlContent = content.content[0].sub1[0].text;
        return this.htmlRender(htmlContent);
      } else {
        return (
          <View>
            <Accordion
              dataArray={content.content}
              renderHeader={this.renderHeadSub1}
              renderContent={this.renderContentSub1}
              // expanded={0}
              icon={'add'}
              expandedIcon={'remove'}
            />
          </View>
        );
      }
    } else {
      return <View></View>;
    }
  };

  renderContentSub1 = (content) => {
    if (content.id == 1) {
      return (
        <View>
          <Accordion
            dataArray={content.sub1}
            renderHeader={this.renderHeadSub2}
            renderContent={this.renderContentSub2}
            expanded={0}
            style={{paddingBottom: 10}}
            icon={'add'}
            expandedIcon={'remove'}
          />
        </View>
      );
    } else if (content.id == 3) {
      return content.sub1.map((result) => {
        return (
          <View style={{paddingBottom: 10}}>
            {this.renderContentSub2(result)}
          </View>
        );
      });
    } else if (content.id == 4) {
      return content.sub1.map((result) => {
        return (
          <View>
            <Text style={styles.subtitle}>{result.text}</Text>
            {this.renderContentSub3(result)}
          </View>
        );
      });
    } else {
      return <View></View>;
    }
  };

  // render view detail content text numbering bank and ecommerce
  renderContentSub2 = (content) => {
    let number = 0;
    return content.sub2.map((item) => {
      number++;
      if (content.lenght !== 0) {
        return (
          <View style={{marginTop: 10}}>
            <View style={{marginTop: 4, marginBottom: 4}}>
              <View style={styles.row}>
                {item.text ? (
                  this.htmlRenderEcommerce(content.sub2[0].text)
                ) : (
                  <>
                    <View style={styles.subContent}>
                      <Text style={[styles.textDefault, {color: 'white'}]}>
                        {number}
                      </Text>
                    </View>
                    <View style={{flex: 1}}>
                      <Text style={styles.textDefault}>{item}</Text>
                    </View>
                  </>
                )}
              </View>
            </View>
          </View>
        );
      }
    });
  };

  renderContentSub3 = (content) => {
    let number = 0;
    return content.sub2.map((item) => {
      number++;
      if (content.lenght !== 0) {
        return (
          <View>
            <View style={{marginTop: 4, marginBottom: 4}}>
              <View style={styles.row}>
                {item.text ? (
                  <>{this.htmlRenderEcommerce(content.sub2[0].text)}</>
                ) : (
                  <>
                    <View style={styles.subContent}>
                      <Text style={[styles.textDefault, {color: 'white'}]}>
                        {number}
                      </Text>
                    </View>
                    <View style={{flex: 1}}>
                      <Text style={styles.textDefault}>{item}</Text>
                    </View>
                  </>
                )}
              </View>
            </View>
          </View>
        );
      }
    });
  };

  //this funtion separate render header view transfer bank and ecommerce
  renderHeadSub1 = (item, expanded) => {
    return (
      <View>
        {item.id == 1 ? (
          this.renderHeadBank(item, expanded)
        ) : item.id == 2 ? (
          <View></View>
        ) : item.id == 3 ? (
          this.renderHeadEcommerce(item, expanded)
        ) : item.id == 4 ? (
          this.renderHeadBl(item, expanded)
        ) : (
          <View></View>
        )}
      </View>
    );
  };

  //render view header for name bank
  renderHeadBank = (item, expanded) => {
    return (
      <View>
        <View style={styles.renderHeader}>
          <Row>
            {item.type == 'Bank BCA' ? (
              <Image style={styles.imageHeadSub1} source={ILbca} />
            ) : item.type == 'Bank BRI' ? (
              <Image style={styles.imageHeadSub1} source={ILbri} />
            ) : item.type == 'Bank BNI' ? (
              <Image style={styles.imageHeadSub1} source={ILbni} />
            ) : item.type == 'Bank MANDIRI' ? (
              <Image style={styles.imageHeadSub1} source={ILmandiri} />
            ) : (
              <View></View>
            )}

            <Text style={styles.titleHeadSub1}>{item.type}</Text>
            {expanded ? (
              <View
                style={{
                  marginLeft: Dimensions.get('window').width * 0.7,
                  position: 'absolute',
                }}>
                <Icon
                  style={{fontSize: 20, color: BAF_COLOR_BLUE}}
                  name="ios-checkmark"
                />
              </View>
            ) : (
              <View></View>
            )}
          </Row>
        </View>
        <View
          style={{
            borderBottomColor: 'rgba(0, 0, 0, 0.05)',
            borderBottomWidth: 1,

            width: Dimensions.get('window').width * 0.7,
            marginLeft: Dimensions.get('window').width * 0.1,
          }}
        />
      </View>
    );
  };

  //render view header PGM, shopee, tokped,
  renderHeadEcommerce = (item, expanded) => {
    return item.sub1.map((result) => {
      if (result.lenght !== 0) {
        return (
          <View style={styles.renderHeadsub2}>
            <Text style={styles.titleheadsub1}>{result.text}</Text>
            {expanded ? (
              <View>
                <Icon
                  style={{fontSize: 20}}
                  type="FontAwesome"
                  name="angle-right"
                />
              </View>
            ) : (
              <View>
                <Icon
                  style={{fontSize: 20}}
                  type="FontAwesome"
                  name="angle-right"
                />
              </View>
            )}
          </View>
        );
      }
    });
  };

  //render view header for bukalapak
  renderHeadBl = (item, expanded) => {
    return (
      <View style={styles.renderHeadsub2}>
        <Text style={styles.titleheadsub1}>{item.type}</Text>
        {expanded ? (
          <View>
            <Icon
              style={{fontSize: 20}}
              type="FontAwesome"
              name="angle-right"
            />
          </View>
        ) : (
          <View>
            <Icon
              style={{fontSize: 20}}
              type="FontAwesome"
              name="angle-right"
            />
          </View>
        )}
      </View>
    );
  };

  renderHeadSub2 = (item, expanded) => {
    return (
      <View style={styles.renderHeadsub2}>
        <Text style={styles.titleheadsub1}>{item.text}</Text>
        {expanded ? (
          <Icon style={{fontSize: 20}} type="FontAwesome" name="angle-right" />
        ) : (
          <Icon style={{fontSize: 20}} type="FontAwesome" name="angle-right" />
        )}
      </View>
    );
  };

  render() {
    return (
      <Container>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}

        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />

        <Accordion
          dataArray={data.ListPay}
          renderHeader={this._renderHeader}
          renderContent={this._renderContent}
          // expanded={0}
          icon={'add'}
          expandedIcon={'remove'}
        />
      </Container>
    );
  }
}

export default BafPay;
