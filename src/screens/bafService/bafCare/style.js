import {StyleSheet, Dimensions} from 'react-native';
import {fonts} from '../../../utils/fonts';

const {Windowheight, width: WIDTH} = Dimensions.get('window');

export const styles = StyleSheet.create({
  callCenterWrapper: {
    marginBottom: 40,
    width: WIDTH * 0.85,
    height: 85,
    elevation: 3,
  },
  title: {
    marginBottom: 10,
    fontSize: 20,
    color: 'rgba(0, 0, 0, 0.8)',

    fontFamily: fonts.primary.bold,
  },
  gap: {
    marginTop: 20,
  },
  page: {
    flex: 1,
    flexGrow: 1,
  },
  placeholder: {
    resizeMode: 'contain',
    width: null,
    height: null,
    flex: 1,
  },
  wrapper: {
    paddingHorizontal: 30,
    color: '#FFFFFF',
  },
  text: {
    marginBottom: 10,
    textAlign: 'justify',
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.8)',

    fontFamily: fonts.primary.normal,
  },
  btnKirim: {
    marginTop: 30,
    backgroundColor: '#002F5F',
    width: '100%',
    height: 40,
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
    marginBottom: 40,
  },
  btnText: {
    color: 'white',
    fontSize: 14,

    fontFamily: fonts.primary.bold,
  },
  textBox: (hasError) => ({
    backgroundColor: 'rgba(196, 196, 196, 0.1)',
    borderWidth: 1,
    borderColor: hasError ? 'red' : 'rgba(0, 0, 0, 0.2)',
    width: '100%',
    height: 120,
  }),
  textCount: (hasError) => ({
    marginTop: 0,
    marginBottom: 30,
    width: '100%',
    textAlign: 'right',
    color: hasError ? 'red' : 'black',

    fontFamily: fonts.primary.normal,
  }),
});
