import React, {Component} from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  Image,
  Platform,
  Keyboard,
  Dimensions,
  PixelRatio,
  Linking,
  BackHandler,
} from 'react-native';
import {Container, Form, Textarea, Icon, Content} from 'native-base';
import Toast, {DURATION} from 'react-native-easy-toast';
import {reduxForm, Field, change, reset} from 'redux-form';
import {bindActionCreators} from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import {emailBafCare} from '../../../actions/home';
import {refreshToken} from '../../../actions';
import {connect} from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import {styles} from './style';
import ILPlaceHolder from '../../../../assets/img/home/services/service-baf-care-call.png';
import {FormKeluhanValidate} from '../../../validates/FormKeluhanValidate';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import analytics from '@react-native-firebase/analytics';
import Spinner from 'react-native-loading-spinner-overlay';
import {jsonParse, handleRefreshToken} from '../../../utils/utilization';
import {
  MESSAGE_TOKEN_EXP,
  TOKEN_EXP,
  MESSAGE_TOKEN_INVALID,
} from '../../../utils/constant';

export const renderField = ({
  input,
  placeholder,
  editable,
  onChangeText,
  maxLength,
  textLength,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }

  return (
    <View>
      <Textarea
        {...input}
        style={styles.textBox(hasError)}
        placeholder={placeholder}
        editable={editable}
        placeholderTextColor={'rgba(0, 0, 0, 0.2)'}
        maxLength={maxLength}
        multiline={true}
        onChangeText={onChangeText}
      />
      <Text style={styles.textCount(hasError)}>{textLength + '/1500'}</Text>
    </View>
  );
};

class BafCare extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Hubungi Kami',
      textLength: 0,
      isConnected: false,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      userToken: false,
      Pesan: '',
      detailUser: false,
    };
  }

  componentDidMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    analytics().logScreenView({
      screen_class: 'screenBafCare',
      screen_name: 'screenBafCare',
    });

    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) {
      this.setState({userToken: userToken});
    }

    const detailUserNonParse = await AsyncStorage.getItem('detailUser');

    if (detailUserNonParse) {
      const detailUserParse = JSON.parse(detailUserNonParse);
      this.setState({detailUser: detailUserParse});
    }
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      emailbafcareError,
      emailbafcareResult,
      refreshTokenResult,
      refreshTokenError,
    } = this.props;

    if (
      emailbafcareError &&
      prevProps.emailbafcareError !== emailbafcareError
    ) {
      const data = await jsonParse(emailbafcareError.message);
      if (data) {
        if (data.message == MESSAGE_TOKEN_EXP) {
          this.props.refreshToken(this.state.userToken);
        } else if (data.message == MESSAGE_TOKEN_INVALID) {
          this.handleInvalid();
        }
      } else {
        this.setState({
          alertFor: 'emailBafCareError',
          alertShowed: true,
          alertMessage:
            'Terjadi kesalahan pada sistem, harap coba beberapa saat lagi.',
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
    }

    if (
      emailbafcareResult &&
      prevProps.emailbafcareResult !== emailbafcareResult
    ) {
      this.setState({
        alertFor: 'emailBafCareResult',
        alertShowed: true,
        alertMessage: 'Keluhan Anda akan segera kami proses.',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
      });
    }

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      await handleRefreshToken(refreshTokenResult.Refreshtoken);
      this.setState({userToken: refreshTokenResult.Refreshtoken});
      this.props.emailBafCare(
        {Message: this.state.Pesan},
        refreshTokenResult.Refreshtoken,
      );
    }
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = async () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  handleInvalid = async () => {
    await AsyncStorage.removeItem('userToken');
    await AsyncStorage.removeItem('userData');
    await AsyncStorage.removeItem('detailUser');
    await AsyncStorage.removeItem('activeAgreement');
    this.setState({
      alertFor: 'invalidToken',
      alertShowed: true,
      alertMessage: 'Sesi login Anda sudah habis',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  sendKeluhan = async (data) => {
    const userToken = await AsyncStorage.getItem('userToken');
    const {detailUser} = this.state;
    const {emailBafCare, navigation} = this.props;
    Keyboard.dismiss();
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        analytics().logEvent('eventKirimkeluhan', {
          item: 'user trying to send complaint',
        });
        if (!detailUser) {
          navigation.navigate('Login');
        } else {
          this.setState({Pesan: data.keluhan});
          emailBafCare({Message: data.keluhan}, userToken);
        }
      } else {
        this.showToastNoInternet();
      }
    });
  };

  changeText = (text) => {
    this.setState({
      textLength: text.length,
    });
  };

  toastComponent = () => {
    return (
      <Toast
        ref="toast"
        style={{
          marginTop:
            Platform.OS != 'android'
              ? Dimensions.get('window').width * PixelRatio.get() > 750
                ? 44
                : 20
              : 0,
          backgroundColor: '#FFE6E9',
          width: '100%',
          borderBottomWidth: 2,
          borderBottomColor: 'red',
          justifyContent: 'center',
          paddingLeft: 50,
          height: 50,
          borderRadius: 0,
        }}
        position="top"
        positionValue={0}
        fadeInDuration={2000}
        fadeOutDuration={1000}
        opacity={0.9}
      />
    );
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false, alertFor: false});
    if (this.state.alertFor === 'emailBafCareResult' || this.state.alertFor === 'emailBafCareError') {
      this.props.resetForm('formBafCare');
      this.props.navigation.navigate('Home');
    }
    if (this.state.alertFor === 'invalidToken') {
      this.props.navigation.navigate('Login');
      this.setState({alertFor: null});
    }
  };

  callNumber = () => {
    analytics().logEvent('eventCallBafCare', {
      item: 'user trying to call BAF Care',
    });
    let phone = '1500750';
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phone}`;
    } else {
      phoneNumber = `tel:${phone}`;
    }

    Linking.canOpenURL(phoneNumber)
      .then((supported) => {
        if (!supported) {
          this.setState({
            alertFor: 'callNumber',
            // alertShowed: true,
            alertMessage: 'Phone number is not available',
            alertTitle: 'Gagal',
            alertType: 'error',
            alertDoneText: 'OK',
          });
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch((err) => console.log(err));
  };

  render() {
    const {handleSubmit, submitting, emailbafcareLoading} = this.props;
    return (
      <Container style={styles.page}>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}

        <Spinner
          visible={emailbafcareLoading ? true : false}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        {this.toastComponent()}

        <Content style={styles.wrapper}>
          <View style={styles.gap} />
          <Text style={styles.title}>Call Center</Text>
          <Text style={styles.text}>Kamu dapat langsung menghubungi kami</Text>
          <TouchableOpacity
            style={styles.callCenterWrapper}
            onPress={() => this.callNumber()}>
            <Image style={styles.placeholder} source={ILPlaceHolder} />
          </TouchableOpacity>
          <Text style={styles.title}>Laporan</Text>
          <Text style={styles.text}>
            Ajukan laporan Kamu, dengan menuliskan pesan Kamu dibawah ini. Kami
            akan segera merespon laporan Kamu melalui email kurang dari 24 jam.
          </Text>
          <Form>
            <Field
              name="keluhan"
              type="text"
              component={renderField}
              placeholder="Deskripsi..."
              maxLength={1500}
              onChangeText={this.changeText}
              editable={true}
              textLength={this.state.textLength}
            />
          </Form>

          <TouchableOpacity
            disabled={submitting}
            onPress={handleSubmit(this.sendKeluhan)}
            style={styles.btnKirim}>
            <Text style={styles.btnText}>Kirim Pesan</Text>
          </TouchableOpacity>
          {this.showAlert()}
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    emailbafcareLoading: state.emailBafCare.loading,
    emailbafcareResult: state.emailBafCare.result,
    emailbafcareError: state.emailBafCare.error,
    refreshTokenResult: state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      emailBafCare,
      refreshToken,
      // detailUser,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

BafCare = reduxForm({
  form: 'formBafCare',
  enableReinitialize: true,
  validate: FormKeluhanValidate,
})(BafCare);

export default connect(mapStateToProps, matchDispatchToProps)(BafCare);
