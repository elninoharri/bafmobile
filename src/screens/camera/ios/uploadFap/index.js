
import React, { PureComponent } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image,Dimensions,PixelRatio} from 'react-native';
import {Icon} from 'native-base';   
import { RNCamera } from 'react-native-camera';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';
import styles from './style';

export const Constants = {
  ...RNCamera.Constants
}

export default class UploadFap extends PureComponent {
    camera=null;
    constructor(props) {
        super(props);
    
        this.state = {
          visible: '',
          showModal: false,
          scrollOffset: null,

          cameraType: Constants.Type.back,
          flashMode: Constants.FlashMode.off,
          recognizedText: null,
          camera : null

        };
      }

    closeModal = () => {
        this.setState({showModal: false});
    };
      
    renderModal = () => {
        return (
          <Modal
            isVisible={this.state.showModal}
            onSwipeComplete={() => this.closeModal()}
            onBackdropPress={() => this.closeModal()}
            swipeDirection={['down']}
            propagateSwipe={true}
            style={{justifyContent:'flex-end',margin:0}}>
            
            <View style={Dimensions.get('window').width * PixelRatio.get() > 750 ? (styles.largeModalContainer) : (styles.smallModalContainer)}>
                <View style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeModalTopLine : styles.smallModalTopLine}/>
                <View style={styles.largeModalTitleContainer}> 
                    <Text style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeTitleText : styles.smallTitleText}>Yuk simak dulu cara foto FAP{"\n"}yang baik dan benar</Text>
                </View>
                <View style={styles.largeModalBodyContainer}>
                    <View style={styles.largeDescriptionImageContainer}>
                      <Image source={require('../../../../../assets/img/uploadImg/fap1.png')} style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeDescriptionImage1 : styles.smallDescriptionImage1} />
                    </View>
                    <View style={styles.largeDescriptionTextContainer}>
                      <Text style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeDescriptionText : styles.smallDescriptionText}>Pastilah formulir FAP diisi dengan huruf{"\n"}cetak dan pastikan semua informasi yang{"\n"}diperlukan telah diisi dengan benar</Text>
                    </View>
                    <View style={styles.largeDescriptionImageContainer}>
                      <Image source={require('../../../../../assets/img/uploadImg/fap2.png')} style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeDescriptionImage2 : styles.smallDescriptionImage2} />
                    </View>
                    <View style={styles.largeDescriptionTextContainer}>
                      <Text style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeDescriptionText : styles.smallDescriptionText}>Pastikan tanda tangan Anda sesuai dengan{"\n"}tanda tangan pada kartu identitas Anda</Text>
                    </View>
                    <View style={styles.largeDescriptionImageContainer}>
                      <Image source={require('../../../../../assets/img/uploadImg/fap3.png')} style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeDescriptionImage3 : styles.smallDescriptionImage3} />
                    </View>
                    <View style={styles.largeDescriptionTextContainer}>
                      <Text style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeDescriptionText : styles.smallDescriptionText}>Formulir yang telah diisi dan ditanda tangani{"\n"}kemudian foto dan pastikan formulir didalam{"\n"}garis panduan</Text>
                    </View>
                </View>
            </View>
            
          </Modal>
        );
      };

  render() {
    const {cancelUpload} = this.props;
    return (
      <View style={styles.container}>
       
        {/* <RNCamera
          ref={ref =>  {
            this.camera = ref;
          }}
          key="camera"
          style={styles.preview}
          type={this.state.cameraType}
          flashMode={this.state.flashMode}
          ratio={this.props.ratio}
          captureAudio={false}
          autoFocus={this.props.autoFocus}
          whiteBalance={this.props.whiteBalance}
          playSoundOnCapture
          width={this.props.width}
          height={this.props.height}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          
        /> */}
        <RNCamera
          ref={ref =>  {
            this.camera = ref;
          }}
          key="camera"
          style={styles.preview}
          type={this.state.cameraType}
          flashMode={this.state.flashMode}
          ratio={this.props.ratio}
          captureAudio={false}
          autoFocus={this.props.autoFocus}
          whiteBalance={this.props.whiteBalance}
          width={this.props.width}
          height={this.props.height}
        >
         {({camera, status, recordAudioPermissionStatus}) => {
           this.setState({camera:camera});
         }}
        </RNCamera>
        <View style={styles.cameraBackground}/>
          {/* <Image source={require('../../../../assets/img/ktp.png')} style={{width:'80%',height:'30%',position:'absolute',top:'30%',alignSelf:'center',opacity:0.6}} /> */}
          <Text style={styles.largeCameraScreenTitle} >Ambil Foto FAP</Text>
          <TouchableOpacity onPress={() => {cancelUpload()}} style={styles.largeCameraBackButton} ><Icon name="arrow-back" style={{color:'white',fontSize:24}}/></TouchableOpacity>
          <TouchableOpacity onPress={() => this.setState({showModal: true})} style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeCameraHintButton : styles.smallCameraHintButton}>
              <View style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeModalTopLine : styles.smallModalTopLine }/>
              <View style={styles.largeCameraHintTextContainer}> 
                  <Text style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeCameraHintText : styles.smallCameraHintText}>Yuk simak dulu cara foto FAP{"\n"}yang baik dan benar</Text>
              </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.takePicture()} style={styles.capture}>
            <Text style={{textAlign:'center'}}> <Icon name="camera-outline" style={ styles.largeCameraCaptureButton}/> </Text>
          </TouchableOpacity>
          {this.renderModal()}
          
      </View>
    );
  }

  // takePicture = async () => {
  //   if (this.camera) {
  //     const options = { quality: 0.5, base64: true, height:360};
  //     const data = await this.camera.takePictureAsync(options);

     
  //     console.log(data.uri);
  //     this.props.goBack(data.uri)
  //   }
  // };
  takePicture = async () => {
    if (this.state.camera !== null) {
      const {camera} = this.state;
      const options = { quality: 0.5, base64: true, height:360};
      const data = await camera.takePictureAsync(options);
      console.log(data.uri);
      this.props.goBack(data.uri)
    }
  };
}

UploadFap.PropTypes = {
  cameraType : PropTypes.any,
  flashMode: PropTypes.any,
  autoFocus: PropTypes.any,
  whiteBalance: PropTypes.any,
  ratio: PropTypes.string,
  quality: PropTypes.number,
  imageWidth: PropTypes.number,
  style: PropTypes.object,
  onCapture: PropTypes.func,
  enabledOCR: PropTypes.bool,
  onClose: PropTypes.func
}

UploadFap.defaultProps = {
  cameraType : Constants.Type.back,
  flashMode: Constants.FlashMode.off,
  autoFocus: Constants.AutoFocus.on,
  whiteBalance: Constants.WhiteBalance.auto,
  quality: 0.5,
  style: null,
  onCapture: null,
  enabledOCR: false,
  onClose: null
}