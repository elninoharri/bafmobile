import { StyleSheet } from 'react-native';


export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'grey',
      },
      preview: {
        top:0,
        flex: 1,
        alignItems: 'center',
      },
      capture: {
        position:'absolute',
        bottom:100,
        backgroundColor: '#002f5f',
        borderRadius: 100,
        alignSelf: 'center',
        width:70,
        height:70,
        justifyContent:'center',
        marginBottom:15
    
      },
      cameraBackground: {
        backgroundColor:'grey',
        width:'100%',
        height:'100%',
        position:'absolute',
        opacity:0.01
      },
      largeModalTopLine : {
        width:'20%',
        height:8,
        backgroundColor:'black',
        alignSelf:'center',
        marginTop:'4%',
        borderRadius:5,
        opacity:0.1
      },
      smallModalTopLine : {
        width:"20%",
        height:5,
        backgroundColor:'black',
        alignSelf:'center',
        marginTop:'4%',
        borderRadius:5,
        opacity:0.1
      },
      largeModalTitleContainer : {
        backgroundColor:'white',
        alignSelf:'center',
        marginTop:'3%',
        marginBottom:'5%'
      },
      largeTitleText: {
        textAlign:'center',
        fontSize:14,
        color:'black'
      },
      smallTitleText: {
        textAlign:'center',
        fontSize:12,
        color:'black'
      },
      largeModalBodyContainer: {
        flex:1,
        flexDirection:'column',
        marginTop:'2%'
      },
      largeDescriptionImageContainer: {
        flex:2,
        alignContent:'center',
        justifyContent:'flex-end',
        
      },
      largeDescriptionImage1: {
        width:185,
        height:113,
        alignSelf:'center'
      },
      largeDescriptionImage2: {
        width:173,
        height:128,
        alignSelf:'center'
      },
      largeDescriptionImage3: {
        width:178,
        height:115,
        alignSelf:'center'
      },
      smallDescriptionImage1: {
        width:138,
        height:85,
        alignSelf:'center'
      },
      smallDescriptionImage2: {
        width:123,
        height:92,
        alignSelf:'center'
      },
      smallDescriptionImage3: {
        width:133,
        height:87,
        alignSelf:'center'
      },
      largeDescriptionTextContainer: {
        marginTop:'1%',
        flex:1, 
        alignSelf:'center'
      },
      largeDescriptionText: {
        textAlign:'center',
        fontSize:12,
        color:'black'
      },
      smallDescriptionText: {
        textAlign:'center',
        fontSize:10,
        color:'black'
      },
      largeCameraScreenTitle: {
        fontSize:14,
        color:'white',
        alignSelf:'center',
        justifyContent:'center',
        position:'absolute',
        top:5
      },
      largeCameraBackButton: {
        alignSelf:'center',
        justifyContent:'center',
        position:'absolute',
        top:5,
        left:'2%'
      },
      largeCameraHintButton: {
        width:'100%',
        height:'12%',
        backgroundColor:'white',
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        position:'absolute',
        bottom:0
      },
      smallCameraHintButton: {
        width:'100%',
        height:'14%',
        backgroundColor:'white',
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        position:'absolute',
        bottom:0
      },
      largeCameraHintStrip: {
        width:'20%',
        height:'10%',
        backgroundColor:'black',
        alignSelf:'center',
        marginTop:'2%',
        borderRadius:5,
        opacity:0.1
      },
      largeCameraHintTextContainer: {
        backgroundColor:'white',
        alignSelf:'center',
        marginTop:'3%'
      },
      largeCameraHintText: {
        textAlign:'center',
        fontSize:14,
        color:'black',
        backgroundColor:'transparent'
      },
      smallCameraHintText: {
        textAlign:'center',
        fontSize:12,
        color:'black',
        backgroundColor:'transparent'
      },
      largeCameraCaptureButton: {
        color:'white',
        fontSize:34
      },
      largeModalContainer: {
        width:'100%',
        height:'92%',
        backgroundColor:'white',
        borderTopRightRadius:10,
        borderTopLeftRadius:10
      },
      smallModalContainer: {
        width:'100%',
        height:'90%',
        backgroundColor:'white',
        borderTopRightRadius:10,
        borderTopLeftRadius:10
      },
      iosLargeModalContainer: {
        width:'100%',
        height:'85%',
        backgroundColor:'white',
        borderTopRightRadius:10,
        borderTopLeftRadius:10
      },
      iosSmallModalContainer: {
        width:'100%',
        height:'85%',
        backgroundColor:'white',
        borderTopRightRadius:10,
        borderTopLeftRadius:10
      }
})