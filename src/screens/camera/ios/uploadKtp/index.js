
import React, { PureComponent } from 'react';
import {Text, TouchableOpacity, View, Image,Dimensions,PixelRatio} from 'react-native';
import {Icon} from 'native-base';   
import { RNCamera } from 'react-native-camera';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types'; 
import styles from './style';
import {check, PERMISSIONS, RESULTS, request} from 'react-native-permissions';

export const Constants = {
  ...RNCamera.Constants
}

export default class UploadKtp extends PureComponent {
    camera=null;
    constructor(props) {
        super(props);
    
        this.state = {
          visible: '',
          showModal: false,
          scrollOffset: null,
          camera:null,
          cameraType: Constants.Type.back,
          flashMode: Constants.FlashMode.off,
          recognizedText: null,

        };
      }
      

      componentDidMount = async() => {
        check(PERMISSIONS.IOS.LOCATION_ALWAYS)
          .then((result) => {
            switch (result) {
              case RESULTS.UNAVAILABLE:
                request(PERMISSIONS.IOS.CAMERA).then((result) => {
                  console.log("request : ",result)
                });
                console.log(
                  'This feature is not available (on this device / in this context)',
                );
                break;
              case RESULTS.DENIED:
                request(PERMISSIONS.IOS.LOCATION_ALWAYS).then((result) => {
                  console.log("request : ",result)
                });
                console.log(
                  'The permission has not been requested / is denied but requestable',
                );
                break;
              case RESULTS.GRANTED:
                console.log('The permission is granted');
                break;
              case RESULTS.BLOCKED:
                request(PERMISSIONS.IOS.CAMERA).then((result) => {
                  console.log("request : ",result)
                });
                console.log('The permission is denied and not requestable anymore');
                break;
            }
          })
          .catch((error) => {
            console.log('Error Permission')
          });
      }

    closeModal = () => {
        this.setState({showModal: false});
    };
      
    renderModal = () => {
        return (
          <Modal
            isVisible={this.state.showModal}
            onSwipeComplete={() => this.closeModal()}
            onBackdropPress={() => this.closeModal()}
            swipeDirection={['down']}
            propagateSwipe={true}
            style={{justifyContent:'flex-end',margin:0}}>
            
            <View style={ Dimensions.get('window').width * PixelRatio.get() > 750 ? (styles.largeModalContainer) : (styles.smallModalContainer)}>
                <View style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeModalTopLine : styles.smallModalTopLine}/>
                <View style={styles.largeModalTitleContainer}> 
                    <Text style={ Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeTitleText : styles.smallTitleText}>Yuk simak dulu cara foto KTP {"\n"}yang baik dan benar</Text>
                </View>
                <View style={styles.largeModalBodyContainer}>
                    <View style={styles.largeDescriptionImageContainer}>
                        <Image source={require('../../../../../assets/img/uploadImg/ktp1.png')} style={ Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeDescriptionImage : styles.smallDescriptionImage} />
                    </View>
                    <View style={styles.largeDescriptionTextContainer}>
                        <Text style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeDescriptionText : styles.smallDescriptionText}>Posisikan KTP di dalam garis panduan</Text>
                    </View>
                    <View style={styles.largeDescriptionImageContainer}>
                        <Image source={require('../../../../../assets/img/uploadImg/ktp2.png')} style={ Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeDescriptionImage : styles.smallDescriptionImage} />
                    </View>
                    <View style={styles.largeDescriptionTextContainer}>
                        <Text style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeDescriptionText : styles.smallDescriptionText}>Foto tidak boleh buram</Text>
                    </View>
                    <View style={styles.largeDescriptionImageContainer}>
                        <Image source={require('../../../../../assets/img/uploadImg/ktp3.png')} style={ Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeDescriptionImage : styles.smallDescriptionImage} />
                    </View>
                    <View style={styles.largeDescriptionTextContainer}>
                        <Text style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeDescriptionText : styles.smallDescriptionText}>Foto tidak boleh terlalu terang atau gelap</Text>
                    </View>
                   
                    
                </View>
            </View>
            
          </Modal>
        );
      };

  render() {
    const {cancelUpload} = this.props;
    return (
      <View style={styles.container}>
       
        <RNCamera
          ref={ref =>  {
            this.camera = ref;
          }}
          key="camera"
          style={styles.preview}
          type={this.state.cameraType}
          flashMode={this.state.flashMode}
          ratio={this.props.ratio}
          captureAudio={false}
          autoFocus={this.props.autoFocus}
          whiteBalance={this.props.whiteBalance}
          width={this.props.width}
          height={this.props.height}
        >
         {({camera, status, recordAudioPermissionStatus}) => {
           this.setState({camera:camera});
         }}
        </RNCamera>
        <View style={styles.cameraBackground}/>
          {/* <Image source={require('../../../../assets/img/ktp.png')} style={{width:'80%',height:'30%',position:'absolute',top:'30%',alignSelf:'center',opacity:0.6}} /> */}
          <Text style={styles.largeCameraScreenTitle} >Ambil Foto KTP</Text>
          <TouchableOpacity 
            onPress={() => {cancelUpload()}} 
            style={styles.largeCameraBackButton} 
          >
              <Icon name="arrow-back" style={{color:'white',fontSize:24}}/>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.setState({showModal: true})} style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeCameraHintButton : styles.smallCameraHintButton}>
              <View style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeModalTopLine : styles.smallModalTopLine}/>
              <View style={styles.largeCameraHintTextContainer}> 
                  <Text style={Dimensions.get('window').width * PixelRatio.get() > 750 ? styles.largeCameraHintText : styles.smallCameraHintText}>Yuk simak dulu cara foto KTP {"\n"}yang baik dan benar</Text>
              </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.takePicture()} style={styles.capture}>
            <Text style={{textAlign:'center'}}> <Icon name="camera-outline" style={ styles.largeCameraCaptureButton}/> </Text>
          </TouchableOpacity>
          {this.renderModal()}
          
      </View>
    );
  }

  takePicture = async () => {
    if (this.state.camera !== null) {
      const {camera} = this.state;
      const options = { quality: 0.5, base64: true, height:360};
      const data = await camera.takePictureAsync(options);
      console.log(data.uri);
      this.props.goBack(data.uri)
    }
  };
}

UploadKtp.PropTypes = {
  cameraType : PropTypes.any,
  flashMode: PropTypes.any,
  autoFocus: PropTypes.any,
  whiteBalance: PropTypes.any,
  ratio: PropTypes.string,
  quality: PropTypes.number,
  imageWidth: PropTypes.number,
  style: PropTypes.object,
  onCapture: PropTypes.func,
  enabledOCR: PropTypes.bool,
  onClose: PropTypes.func
}

UploadKtp.defaultProps = {
  cameraType : Constants.Type.back,
  flashMode: Constants.FlashMode.off,
  autoFocus: Constants.AutoFocus.on,
  whiteBalance: Constants.WhiteBalance.auto,
  quality: 0.5,
  style: null,
  onCapture: null,
  enabledOCR: false,
  onClose: null
}
