import React, {PureComponent} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  CameraRoll,
  Image,
  ImageEditor,
} from 'react-native';
import {Icon} from 'native-base';
import {RNCamera} from 'react-native-camera';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';
import {relativeTimeThreshold} from 'moment';

export const Constants = {
  ...RNCamera.Constants,
};
export default class uploadPpbk extends PureComponent {
  camera = null;
  constructor(props) {
    super(props);

    this.state = {
      visible: '',
      showModal: false,
      scrollOffset: null,

      cameraType: Constants.Type.back,
      flashMode: Constants.FlashMode.off,
      recognizedText: null,
    };
  }

  closeModal = () => {
    this.setState({showModal: false});
  };

  renderModal = () => {
    return (
      <Modal
        isVisible={this.state.showModal}
        onSwipeComplete={() => this.closeModal()}
        onBackdropPress={() => this.closeModal()}
        swipeDirection={['down']}
        propagateSwipe={true}
        style={{justifyContent: 'flex-end', margin: 0}}>
        <View
          style={{
            width: '100%',
            height: '85%',
            backgroundColor: 'white',
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
          }}>
          <View
            style={{
              width: '20%',
              height: 8,
              backgroundColor: 'black',
              alignSelf: 'center',
              marginTop: '4%',
              borderRadius: 5,
              opacity: 0.1,
            }}
          />
          <View
            style={{
              backgroundColor: 'white',
              width: '60%',
              alignSelf: 'center',
              marginTop: '3%',
            }}>
            <Text style={{textAlign: 'center', fontSize: 14, color: 'black'}}>
              Yuk simak dulu cara mengambil foto PPBK yang baik dan benar
            </Text>
          </View>
          <View style={{flex: 1, flexDirection: 'column', marginTop: '2%'}}>
            <View
              style={{
                flex: 2,
                alignContent: 'center',
                justifyContent: 'flex-end',
                marginBottom: '2%',
              }}>
              <Image
                source={require('../../../../assets/img/uploadImg/ppbk1.png')}
                style={{width: 172, height: 105, alignSelf: 'center'}}
              />
            </View>
            <View style={{flex: 1, width: '70%', alignSelf: 'center'}}>
              <Text style={{textAlign: 'center', fontSize: 12, color: 'black'}}>
                Pastikan formulir PPBK diisi denga huruf cetak dan semua
                informasi yang diperlukan telah diisi dengan benar
              </Text>
            </View>
            <View
              style={{
                flex: 2,
                alignContent: 'center',
                justifyContent: 'flex-end',
                marginBottom: '2%',
              }}>
              <Image
                source={require('../../../../assets/img/uploadImg/ppbk2.png')}
                style={{width: 160, height: 125, alignSelf: 'center'}}
              />
            </View>
            <View style={{flex: 1, width: '70%', alignSelf: 'center'}}>
              <Text style={{textAlign: 'center', fontSize: 12, color: 'black'}}>
                Sebelum menandatangani, pahami dulu beberapa ketentuan yang
                berlaku dan pastikan tanda tangan Anda sesuai dengan tanda
                tangan pada KTP Anda
              </Text>
            </View>
            <View
              style={{
                flex: 2,
                alignContent: 'center',
                justifyContent: 'flex-end',
                marginBottom: '2%',
              }}>
              <Image
                source={require('../../../../assets/img/uploadImg/ppbk3.png')}
                style={{width: 170, height: 110, alignSelf: 'center'}}
              />
            </View>
            <View style={{flex: 1, width: '70%', alignSelf: 'center'}}>
              <Text style={{textAlign: 'center', fontSize: 12, color: 'black'}}>
                Formulir yang telah diisi dan ditanda tangani kemudian foto dan
                pastikan formulir didalam garis panduan
              </Text>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    const {cancelUpload} = this.props;
    return (
      <View style={styles.container}>
        <RNCamera
          ref={(ref) => {
            this.camera = ref;
          }}
          key="camera"
          style={styles.preview}
          type={this.state.cameraType}
          flashMode={this.state.flashMode}
          ratio={this.props.ratio}
          captureAudio={false}
          autoFocus={this.props.autoFocus}
          whiteBalance={this.props.whiteBalance}
          playSoundOnCapture
          width={this.props.width}
          height={this.props.height}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />
        <View
          style={{
            backgroundColor: 'grey',
            width: '100%',
            height: '100%',
            position: 'absolute',
            opacity: 0.01,
          }}
        />
        <Text
          style={{
            fontSize: 14,
            color: 'white',
            alignSelf: 'center',
            justifyContent: 'center',
            position: 'absolute',
            top: 5,
          }}>
          Ambil Foto PPBK
        </Text>
        <TouchableOpacity
          onPress={() => {
            cancelUpload();
          }}
          style={{
            alignSelf: 'center',
            justifyContent: 'center',
            position: 'absolute',
            top: 5,
            left: '2%',
          }}>
          <Icon name="arrow-back" style={{color: 'white', fontSize: 24}} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.setState({showModal: true})}
          style={{
            width: '100%',
            height: '12%',
            backgroundColor: 'white',
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            position: 'absolute',
            bottom: 0,
          }}>
          <View
            style={{
              width: '20%',
              height: '10%',
              backgroundColor: 'black',
              alignSelf: 'center',
              marginTop: '2%',
              borderRadius: 5,
              opacity: 0.1,
            }}
          />
          <View
            style={{
              backgroundColor: 'white',
              width: '55%',
              alignSelf: 'center',
              marginTop: '3%',
            }}>
            <Text style={{textAlign: 'center', fontSize: 14, color: 'black'}}>
              Yuk simak dulu cara mengambil foto PPBK yang baik dan benar
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.takePicture()}
          style={styles.capture}>
          <Text style={{textAlign: 'center'}}>
            {' '}
            <Icon
              name="camera-outline"
              style={{color: 'white', fontSize: 34}}
            />{' '}
          </Text>
        </TouchableOpacity>
        {this.renderModal()}
      </View>
    );
  }

  takePicture = async () => {
    if (this.camera) {
      const options = {quality: 0.5, base64: true, height: 360};
      const data = await this.camera.takePictureAsync(options);

      console.log(data.uri);
      this.props.goBack(data.uri);
    }
  };
}

uploadPpbk.PropTypes = {
  cameraType: PropTypes.any,
  flashMode: PropTypes.any,
  autoFocus: PropTypes.any,
  whiteBalance: PropTypes.any,
  ratio: PropTypes.string,
  quality: PropTypes.number,
  imageWidth: PropTypes.number,
  style: PropTypes.object,
  onCapture: PropTypes.func,
  enabledOCR: PropTypes.bool,
  onClose: PropTypes.func,
};

uploadPpbk.defaultProps = {
  cameraType: Constants.Type.back,
  flashMode: Constants.FlashMode.off,
  autoFocus: Constants.AutoFocus.on,
  whiteBalance: Constants.WhiteBalance.auto,
  quality: 0.5,
  style: null,
  onCapture: null,
  enabledOCR: false,
  onClose: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  preview: {
    top: 0,
    flex: 1,
    alignItems: 'center',
  },
  capture: {
    position: 'absolute',
    bottom: 100,
    backgroundColor: '#002f5f',
    borderRadius: 100,
    alignSelf: 'center',
    width: '19%',
    height: '11%',
    justifyContent: 'center',
  },
});
