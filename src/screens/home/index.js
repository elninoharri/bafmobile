import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
  BackHandler,
  StatusBar,
  Platform,
  Dimensions,
  PixelRatio,
  FlatList,
  RefreshControl,
  Alert,
  Linking,
  AppState,
  NativeModules,
  Button,
} from 'react-native';
import RNRestart from 'react-native-restart';
import {Spinner, Container, View, Icon, Content} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import {bindActionCreators} from 'redux';
import {connect, connectAdvanced} from 'react-redux';
import styles from './style';
import FooterAndroid from '../../components/android/footer';
import FooterIos from '../../components/ios/footer';
import Banner from '../../components/multiPlatform/tabHome/banner';
import UserActivationCard from '../../components/multiPlatform/tabHome/activationCard/user';
import GuestActivationCard from '../../components/multiPlatform/tabHome/activationCard/guest';
import InstallmentsCardAndroid from '../../components/android/tabHome/installments/installmentsCard';
import ListOrderCard from '../../components/multiPlatform/tabHome/listOrderCard';
import InstallmentsButton from '../../components/multiPlatform/tabHome/installments/installmentsButton';
import InstallmentsCardIos from '../../components/ios/tabHome/installments/installmentsCard';
import Separator from '../../components/multiPlatform/tabHome/separator';
import Promo from '../../components/multiPlatform/tabHome/promo';
import BafService from '../../components/multiPlatform/tabHome/bafService';
import TabPromo from '../../components/multiPlatform/tabPromo';
import TabAkun from '../../components/multiPlatform/tabAkun';
import TabProduk from '../../components/multiPlatform/tabProduk';
import TabPesan from '../../components/multiPlatform/tabPesan';
import CustomAlertComponent from '../../components/multiPlatform/customAlert/CustomAlertComponent';
import Toast, {DURATION} from 'react-native-easy-toast';
import {
  dataBanner,
  getActiveAgreement,
  submitNik,
  detailBanner,
  resetFilterBanner,
  filterBanner,
} from '../../actions/home';
import {detailUser, refreshToken, logout} from '../../actions';
import {orderCount} from '../../actions/sandiaFeature/dataAwal/';
import {
  TYPE_BANNER_ANNOUNCEMENT,
  TYPE_BANNER_PROMOTION,
  ERROR_AUTH,
  BAF_COLOR_BLUE,
  TOKEN_EXP,
  MESSAGE_TOKEN_EXP,
  MESSAGE_TOKEN_INVALID,
} from '../../utils/constant';
import {
  substringUrl,
  jsonParse,
  handleRefreshToken,
  sliceFirstEightLastSix,
  JailMonkeyUtils,
} from '../../utils/utilization';
import analytics from '@react-native-firebase/analytics';
import FingerprintScanner from 'react-native-fingerprint-scanner';
import banner from '../../components/multiPlatform/tabHome/banner';
import JailMonkey from 'jail-monkey';
import thirdWizard from '../sandiaFeature/fdeAddMp/form/thirdWizard';
import BafPoints from '../../components/multiPlatform/tabHome/bafpoints';

class Home extends Component {
  constructor(props) {
    super(props);
    this.goLogin = this.goLogin.bind(this);
    this.goRegister = this.goRegister.bind(this);
    this.goLogout = this.goLogout.bind(this);
    this.goPaymentHistory = this.goPaymentHistory.bind(this);
    this.submitNikHandle = this.submitNikHandle.bind(this);
    this.tabHome = this.tabHome.bind(this);
    this.tabPromo = this.tabPromo.bind(this);
    this.tabPesan = this.tabPesan.bind(this);
    this.tabProduk = this.tabProduk.bind(this);
    this.tabAkun = this.tabAkun.bind(this);
    // this.tabPesan = this.tabPesan.bind(this);
    this.state = {
      screenTitle: 'Home',
      images: [
        require('../../../assets/img/home/banner/default-banner1.png'),
        require('../../../assets/img/home/banner/default-banner2.png'),
        require('../../../assets/img/home/banner/default-banner3.png'),
      ],
      imagesApi: [
        require('../../../assets/img/home/banner/default-banner1.png'),
        require('../../../assets/img/home/banner/default-banner2.png'),
        require('../../../assets/img/home/banner/default-banner3.png'),
      ],

      detailPromo: [],
      listNotif: false,

      tabState: 'home',
      NIK: false,

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',

      isConnected: false,
      chooseValue: false,

      userData: false,
      userToken: false,
      detailUser: false,
      activeAgreement: false,

      fingerPrint: false,
      popupShowed: false,
      fingerDetected: true,

      countOrder: false,
      appState: false,
      promoHome: false,
    };
  }

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      if (this.state.tabState === 'home') {
        BackHandler.exitApp();
      } else {
        this.tabHome();
      }
      return true;
    }
  };

  apiBanner = async () => {
    this.props.detailBanner({
      BannerID: '',
      Type: TYPE_BANNER_PROMOTION,
      Usergroupid: this.state.detailUser
        ? this.state.detailUser.UserGroupID
        : '',
      Category: '',
    });
  };

  initApi = async () => {
    const {
      dataBanner,
      detailBanner,
      detailUser,
      getActiveAgreement,
      orderCount,
    } = this.props;

    const {userData, userToken} = this.state;
    const compareEmail = await AsyncStorage.getItem('userFinger');

    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        dataBanner({Code: '', Type: TYPE_BANNER_ANNOUNCEMENT});
        this.apiBanner();
        if (userData && userToken) {
          detailUser({}, userToken);
          getActiveAgreement(userToken);
        }
      } else {
        if (compareEmail && compareEmail === this.state.detailUser.Email) {
          this.setState({fingerPrint: true});
        }
        this.showToastNoInternet();
      }
    });
  };

  //this function refresh token when token expired or time out

  componentDidMount = async () => {
    analytics().logScreenView({
      screen_name: 'screenHome',
      screen_class: 'screenHome',
    });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    //jailmonkey ini di bypass ketika development untuk mencegah false alarm
    //gampangnya, pasang ! dibelakang method jailmonkey

    const JailMonkeyVerification = await JailMonkeyUtils();

    if (JailMonkeyVerification) {
      // is this device JailBroken on iOS/Android?
      this.handleJail();
    } else {
      this.detectFingerprintAvailable();

      const jsonUserData = await AsyncStorage.getItem('userData');
      if (jsonUserData) {
        var userData = JSON.parse(jsonUserData);
        this.setState({userData: userData});
      }

      const userToken = await AsyncStorage.getItem('userToken');
      if (userToken) {
        console.log(userToken);
        this.setState({userToken: userToken});
      }

      const jsonDetailUser = await AsyncStorage.getItem('detailUser');
      if (jsonDetailUser) {
        var detailUser = JSON.parse(jsonDetailUser);
        this.setState({detailUser: detailUser});
      }

      const jsonActiveAgreement = await AsyncStorage.getItem('activeAgreement');
      if (jsonActiveAgreement) {
        var activeAgreement = JSON.parse(jsonActiveAgreement);
        this.setState({activeAgreement: activeAgreement});
      }

      this.initApi();

      this._checkInitialUrl();
    }
  };

  handleJail = () => {
    Alert.alert(
      'Informasi',
      'Sepertinya dalam perangkat anda terdeteksi bahaya.',
      [{text: 'OK', onPress: () => BackHandler.exitApp()}],
      {cancelable: false},
    );
    this.setState({
      alertFor: 'jailMonkey',
      alertShowed: true,
      alertMessage: 'Sepertinya dalam perangkat anda terdeteksi bahaya',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'Ok',
    });
  };

  componentWillUnmount = () => {
    Linking.removeEventListener('url', this._handleUrlBackground);
  };

  _checkInitialUrl = async () => {
    Linking.getInitialURL()
      .then((ev) => {
        if (ev) {
          this._handleUrl(ev);
        }
      })
      .catch((err) => {
        // console.log('An error occurred', err);
      });
    Linking.addEventListener('url', this._handleUrlBackground);
  };

  _handleUrl = (url) => {
    if (Platform.OS === 'android') {
      if (url != null) {
        const splitedUrl = substringUrl(url);
        const screenName = splitedUrl[3];
        const orderTrxHid = splitedUrl[4];
        this.props.navigation.navigate(screenName, {params: orderTrxHid});
      }
    } else {
      if (url != null) {
        const splitedUrl = substringUrl(url);
        const screenName = splitedUrl[2];
        const orderTrxHid = splitedUrl[3];
        this.props.navigation.navigate(screenName, {params: orderTrxHid});
      }
    }
  };

  _handleUrlBackground = (url) => {
    if (Platform.OS == 'android') {
      if (url.url != null) {
        const splitedUrl = substringUrl(url.url);
        const screenName = splitedUrl[3];
        const orderTrxHid = splitedUrl[4];
        this.props.navigation.navigate(screenName, {params: orderTrxHid});
      }
    } else {
      if (url.url != null) {
        const splitedUrl = substringUrl(url.url);
        const screenName = splitedUrl[2];
        const orderTrxHid = splitedUrl[3];
        this.props.navigation.navigate(screenName, {params: orderTrxHid});
      }
    }
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      loginResult,
      loginOpenidResult,
      logoutResult,
      logoutError,
      registerResult,
      detailBannerResult,
      detailBannerError,
      filterBannerResult,
      dataBannerResult,
      dataBannerError,
      detailUserResult,
      detailUserError,
      detailUser,
      getActiveAgreement,
      changePasswordError,
      getActiveAgreementResult,
      getActiveAgreementError,
      submitNikResult,
      submitNikError,
      editUserResult,
      orderCountError,
      orderCountResult,
      orderCount,
      editUserError,
      ddeInsert_Result,
      refreshTokenResult,
      refreshTokenError,
    } = this.props;

    if (loginResult && prevProps.loginResult !== loginResult) {
      detailUser({}, loginResult.Usertoken);
      getActiveAgreement(loginResult.Usertoken);
      this.setState({
        userData: {
          Usergroupid: loginResult.UserGroupID,
        },
        userToken: loginResult.Usertoken,
      });
    }

    if (
      getActiveAgreementError &&
      prevProps.getActiveAgreementError !== getActiveAgreementError
    ) {
      const data = await jsonParse(getActiveAgreementError.message);
      if (data) {
        this.validateRefreshToken(data);
      }
    }

    if (
      loginOpenidResult &&
      prevProps.loginOpenidResult !== loginOpenidResult
    ) {
      detailUser({}, loginOpenidResult.Usertoken);
      getActiveAgreement(loginOpenidResult.Usertoken);
      this.setState({
        userData: {
          Usergroupid: loginOpenidResult.Usergroupid,
        },
        userToken: loginOpenidResult.Usertoken,
      });
    }

    if (logoutResult && prevProps.logoutResult !== logoutResult) {
      await AsyncStorage.removeItem('userToken');
      await AsyncStorage.removeItem('userData');
      await AsyncStorage.removeItem('detailUser');
      await AsyncStorage.removeItem('activeAgreement');
      this.resetPromoBanner();
      this.setState({
        userData: false,
        userToken: false,
        detailUser: false,
        activeAgreement: false,
        fingerPrint: false,
        alertFor: 'logout',
        alertShowed: true,
        alertMessage: 'Anda berhasil keluar dari akun Anda.',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
      });
    }

    if (logoutError && prevProps.logoutError !== logoutError) {
      const data = await jsonParse(logoutError.message);
      if (data) {
        if (data.message == MESSAGE_TOKEN_EXP) {
          this.props.refreshToken(this.state.userToken);
        } else {
          await AsyncStorage.removeItem('userToken');
          await AsyncStorage.removeItem('userData');
          await AsyncStorage.removeItem('detailUser');
          await AsyncStorage.removeItem('activeAgreement');
          this.resetPromoBanner();
          this.setState({
            userData: false,
            userToken: false,
            detailUser: false,
            activeAgreement: false,
            fingerPrint: false,
            alertFor: 'logout',
            alertShowed: true,
            alertMessage: 'Anda berhasil keluar dari akun Anda.',
            alertTitle: 'Berhasil',
            alertType: 'success',
            alertDoneText: 'OK',
          });
        }
      }
    }

    if (registerResult && prevProps.registerResult !== registerResult) {
      detailUser({}, registerResult.Usertoken);
      getActiveAgreement(registerResult.Usertoken);
      this.setState({
        userData: {
          Usergroupid: registerResult.UserGroupID,
        },
        userToken: registerResult.Usertoken,
      });
    }

    if (dataBannerResult && prevProps.dataBannerResult !== dataBannerResult) {
      if (dataBannerResult.AllData && dataBannerResult.AllData.length > 0) {
        var bannerData = [];
        dataBannerResult.AllData.forEach(function (banner) {
          bannerData.push(banner.RefBannerImg);
        });
        this.setState({imagesApi: bannerData});
      } else {
        this.setState({imagesApi: this.state.images});
      }
    }

    if (dataBannerError && prevProps.dataBannerError !== dataBannerError) {
      this.setState({imagesApi: this.state.images});
    }

    if (
      detailBannerResult &&
      prevProps.detailBannerResult !== detailBannerResult
    ) {
      if (
        detailBannerResult.ContentName &&
        detailBannerResult.ContentName.length > 0
      ) {
        this.setState({
          detailPromo: detailBannerResult.ContentName,
          promoHome: detailBannerResult.ContentName.slice(0, 3),
        });
      } else {
        // this.setState({imagesApi: this.state.images});
      }
    }

    if (
      detailBannerError &&
      prevProps.detailBannerError !== detailBannerError
    ) {
      this.setState({detailPromo: []});
    }

    if (detailUserResult && prevProps.detailUserResult !== detailUserResult) {
      if (detailUserResult.UserGroupID == '2') {
        orderCount({
          Idno: detailUserResult.NIK,
          Lobcode: '',
          Isapproved: '1',
          Orderstat: '3',
        });
      }

      if (Platform.OS === 'android') {
        NativeModules.MarketingCloudManager.salesforceSetContactKey(
          '200',
          detailUserResult.ContactKey,
        );
        console.log('ContactKeyAndroid=' + detailUserResult.ContactKey);
      } else {
        NativeModules.MarketingCloudModul.setContactKey(
          detailUserResult.ContactKey,
        );
        console.log('ContactKeyIos=' + detailUserResult.ContactKey);
      }

      //karena response param tidak ada userid makan function ini di comment terlebih dahulu
      // let detailUserModified = detailUserResult;
      // detailUserModified.Userid = sliceFirstEightLastSix(
      //   detailUserResult.Userid,
      // );
      // cek listOrder

      // await AsyncStorage.removeItem('detailUser');
      await AsyncStorage.setItem(
        'detailUser',
        JSON.stringify(detailUserResult),
      );

      await this.setState({detailUser: detailUserResult});

      const compareEmail = await AsyncStorage.getItem('userFinger');
      if (compareEmail) {
        if (compareEmail === detailUserResult.Email) {
          this.setState({fingerPrint: true});
        }
      }

      if (this.state.userData.Usergroupid !== detailUserResult.UserGroupID) {
        await AsyncStorage.removeItem('userData');
        await AsyncStorage.setItem(
          'userData',
          JSON.stringify({
            Usergroupid: detailUserResult.UserGroupID,
          }),
        );
        this.setState({
          userData: {
            Usergroupid: detailUserResult.UserGroupID,
          },
        });
      }

      if (detailUserResult.UserGroupID !== null) {
        this.apiBanner();
      }
    }

    if (orderCountResult && prevProps.orderCountResult !== orderCountResult) {
      if (orderCountResult !== 'Data Not Found') {
        this.setState({countOrder: orderCountResult.length});
      } else {
        this.setState({countOrder: false});
      }
    }

    if (orderCountError && prevProps.orderCountError !== orderCountError) {
      this.setState({countOrder: false});
    }

    if (detailUserError && prevProps.detailUserError !== detailUserError) {
      const data = await jsonParse(detailUserError.message);
      if (data) {
        this.validateRefreshToken(data);
      } else {
        //jika selain 401, di mana datanya bukan sebagai JSON yang dikirim di error handle
        // this.setState({
        //   alertShowed: true,
        //   alertMessage: detailUserError.message,
        //   alertTitle: 'Gagal',
        //   alertType: 'error',
        //   alertDoneText: 'OK',
        // });
        console.log('detailUSer Error Message ' + detailUserError.message);
      }

      // dulu di sini digunakan untuk pengecekan apakah token expired atau bukan, jika sudah maka user seharusnya dibawa ke login screen kembali,
      // akan tetapi karena token expired hanya dalam 5 menit dan terdapat refresh token, block code ini kita comment terlebih dahulu.

      // if (detailUserError.message === ERROR_AUTH) {
      //   this.setState({
      //     userData: false,
      //     userToken: false,
      //     detailUser: false,
      //     activeAgreement: false,
      //   });

      //   await AsyncStorage.removeItem('userToken');
      //   await AsyncStorage.removeItem('userData');
      //   await AsyncStorage.removeItem('detailUser');
      //   await AsyncStorage.removeItem('activeAgreement');
      // }
      // else {
      //   if (this.state.userData && this.state.userToken) {
      //     // console.log('detailUser Retry!');
      //     detailUser(
      //       {Userid: this.state.userData.Userid},
      //       this.state.userToken,
      //     );
      //   }
      // }
    }

    if (
      getActiveAgreementResult &&
      prevProps.getActiveAgreementResult !== getActiveAgreementResult
    ) {
      if (
        getActiveAgreementResult.AllData &&
        getActiveAgreementResult.AllData.length > 0
      ) {
        await AsyncStorage.removeItem('activeAgreement');
        await AsyncStorage.setItem(
          'activeAgreement',
          JSON.stringify(getActiveAgreementResult.AllData),
        );
        this.setState({activeAgreement: getActiveAgreementResult.AllData});
      } else {
        this.setState({activeAgreement: false});
      }
    }

    if (
      changePasswordError &&
      prevProps.changePasswordError !== changePasswordError
    ) {
      if (changePasswordError.message === ERROR_AUTH) {
        this.setState({
          userData: false,
          userToken: false,
          detailUser: false,
          activeAgreement: false,
          tabState: 'Akun',
        });
      }
    }

    if (submitNikResult && prevProps.submitNikResult !== submitNikResult) {
      await AsyncStorage.removeItem('userData');
      await AsyncStorage.setItem(
        'userData',
        JSON.stringify({
          Usergroupid: submitNikResult.UserGroupID,
        }),
      );

      this.setState({
        alertShowed: true,
        alertMessage: 'Anda berhasil submit NIK.',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
        userData: {
          Usergroupid: submitNikResult.UserGroupID,
        },
      });
      detailUser({}, this.state.userToken);
      getActiveAgreement(this.state.userToken);
    }

    if (submitNikError && prevProps.submitNikError !== submitNikError) {
      const data = await jsonParse(submitNikError.message); //di sini ngeparse sekaligus ngecek apakah data kiriman error berupa json
      if (data) {
        this.validateRefreshToken(data);
      } else {
        this.setState({
          alertShowed: true,
          alertMessage: submitNikError.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
          isModalVisible: false,
        });
      }
    }

    if (editUserResult && prevProps.editUserResult !== editUserResult) {
      detailUser({}, this.state.userToken);
      getActiveAgreement(this.state.userToken);
    }

    if (editUserError && prevProps.editUserError !== editUserError) {
      if (editUserError.message === ERROR_AUTH) {
        this.setState({
          userData: false,
          userToken: false,
          detailUser: false,
          activeAgreement: false,
        });
      }
    }

    if (
      ddeInsert_Result !== null &&
      prevProps.ddeInsert_Result !== ddeInsert_Result
    ) {
      orderCount({
        Idno: detailUserResult.NIK,
        Lobcode: '',
        Isapproved: '1',
        Orderstat: '3',
      });
    }

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      console.log('Error Refresh Token home ', refreshTokenError.message);
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      await handleRefreshToken(refreshTokenResult.Refreshtoken);
      this.setState({userToken: refreshTokenResult.Refreshtoken});

      if (detailUserError) {
        detailUser({}, refreshTokenResult.Refreshtoken);
      } else if (submitNikError) {
        this.props.submitNik(
          {NIK: this.state.NIK},
          refreshTokenResult.Refreshtoken,
        );
      } else if (logoutError) {
        this.props.logout(refreshTokenResult.Refreshtoken);
      } else if (getActiveAgreementError) {
        getActiveAgreement(refreshTokenResult.Refreshtoken);
      }
    }
  };

  // this function is validate token, if response api expired token this function trigered
  validateRefreshToken = async (data) => {
    const {refreshToken} = this.props;
    if (data) {
      if (data.message == MESSAGE_TOKEN_EXP) {
        refreshToken(this.state.userToken);
      } else if (data.message == MESSAGE_TOKEN_INVALID) {
        this.handleInvalid();
      }
    }
  };

  handleInvalid = async () => {
    await AsyncStorage.removeItem('userToken');
    await AsyncStorage.removeItem('userData');
    await AsyncStorage.removeItem('detailUser');
    await AsyncStorage.removeItem('activeAgreement');
    this.setState({
      alertFor: 'invalidToken',
      alertShowed: true,
      alertMessage: 'Sesi login Anda sudah habis',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  goNoConnection = () => {
    this.props.navigation.navigate('NoConnection');
  };

  goChangeProfile = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        const detailUserParam = this.state.detailUser;
        this.props.navigation.navigate('changeProfile', {detailUserParam});
      } else {
        this.props.navigation.navigate('NoConnection');
      }
    });
  };

  goSubmitOrder = (data) => {
    this.setState({
      alertShowed: true,
      alertMessage: 'Fitur ini sedang dalam tahap pengembangan.',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  goStatusOrder = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        this.props.navigation.navigate('OrderList');
      } else {
        this.props.navigation.navigate('NoConnection');
      }
    });
  };

  goPromoDetail = async (image, title, dateEnd, contenthid, promoCode) => {
    if (contenthid !== '') {
      this.props.navigation.navigate('PromoDetail', {
        image: image,
        title: title,
        dateEnd: dateEnd,
        contenthid: contenthid,
        promoCode: promoCode,
      });
    }
  };

  goLogin = async () => {
    this.props.navigation.navigate('Login');
  };

  goDetailPesan = async (data) => {
    this.props.navigation.navigate('DetailPesan', data);
  };

  goPaymentHistory = async (params) => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        this.props.navigation.navigate('PaymentHistory', params);
      } else {
        this.props.navigation.navigate('NoConnection');
      }
    });
  };

  goBafPay = () => {
    this.props.navigation.navigate('BafPay');
  };

  goFilter = () => {
    this.props.navigation.navigate('FilterPromo');
  };

  onReload = () => {
    this.initApi();
    // RNRestart.Restart();
  };

  reloadBanner = () => {
    this.apiBanner();
  };

  goRegister = async () => {
    this.props.navigation.navigate('Register');
  };

  goPesananSaya = async () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        this.props.navigation.navigate('bpkb');
      } else {
        this.props.navigation.navigate('NoConnection');
      }
    });
  };

  goChangePassword = async () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        this.props.navigation.navigate('changePassword');
      } else {
        this.props.navigation.navigate('NoConnection');
      }
    });
  };

  goRequestBPKB = async () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        // this.props.navigation.navigate('RequestBPKP');
        this.props.navigation.navigate('RequestBPKPTC');
      } else {
        this.props.navigation.navigate('NoConnection');
      }
    });
  };

  goLogout = async () => {
    const {logout} = this.props;
    logout(this.state.userToken);
  };

  goProduct = async (Lob) => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        if (Lob == 'MP') {
          this.props.navigation.navigate('lob', {tabSelected: 'Multiproduk'});
        } else if (Lob == 'NMC') {
          this.props.navigation.navigate('lob', {tabSelected: 'Motor'});
        } else if (Lob == 'CAR') {
          this.props.navigation.navigate('lob', {tabSelected: 'Mobil'});
        } else if (Lob == 'SYANA') {
          this.props.navigation.navigate('lob', {tabSelected: 'Syariah'});
        } else {
          this.setState({
            alertShowed: true,
            alertMessage: 'Fitur ini sedang dalam tahap pengembangan.',
            alertTitle: 'Informasi',
            alertType: 'info',
            alertDoneText: 'OK',
          });
        }
      } else {
        this.props.navigation.navigate('NoConnection');
      }
    });
  };

  resetPromoBanner = () => {
    if (this.props.option) {
      this.props.resetFilterBanner();
      this.apiBanner();
    } else {
      this.apiBanner();
    }
  };

  tabHome = async () => {
    await analytics().logScreenView({
      screen_class: 'tabHome',
      screen_name: 'tabHome',
    });
    this.resetPromoBanner();
    this.setState({tabState: 'home'});
  };

  tabPromo = async () => {
    await analytics().logScreenView({
      screen_class: 'tabPromo',
      screen_name: 'tabPromo',
    });
    this.setState({
      tabState: 'promo',
    });
  };

  tabPesan = async () => {
    await analytics().logScreenView({
      screen_class: 'tabPesan',
      screen_name: 'tabPesan',
    });
    this.resetPromoBanner();
    this.setState({
      tabState: 'pesan',
    });
  };

  submitNikHandle = async (data) => {
    const userToken = await AsyncStorage.getItem('userToken');
    this.setState({NIK: data});
    this.props.submitNik({NIK: data}, userToken);
  };

  tabProduk = async () => {
    await analytics().logScreenView({
      screen_class: 'tabProduk',
      screen_name: 'tabProduk',
    });
    this.resetPromoBanner();
    this.setState({
      tabState: 'produk',
    });
  };

  tabAkun = async () => {
    await analytics().logScreenView({
      screen_class: 'tabAkun',
      screen_name: 'tabAkun',
    });
    this.resetPromoBanner();
    this.setState({tabState: 'akun'});
    this.detectFingerprintAvailable();
  };

  detectFingerprintAvailable = () => {
    FingerprintScanner.isSensorAvailable().catch((error) =>
      this.setState({
        errorMessage: error.message,
        biometric: error.biometric,
        fingerDetected: false,
      }),
    );
  };

  handleFingerprintShowed = async () => {
    if (!this.state.fingerPrint) {
      if (!this.state.fingerDetected && this.state.errorMessage) {
        this.setState({
          alertShowed: true,
          alertMessage: 'Device Anda tidak support fitur ini.',
          alertTitle: 'Informasi',
          alertType: 'info',
          alertDoneText: 'OK',
        });
      } else {
        this.setState({popupShowed: true});
      }
    } else {
      Alert.alert(
        'Autentikasi berhasil',
        'Log in dengan sidik jari telah di-nonaktifkan',
      );
      this.setState({fingerPrint: false, popupShowed: false});
      await AsyncStorage.removeItem('userFinger');
    }
  };

  onAuthenticate = async () => {
    const {detailUser} = this.state;
    this.setState({
      alertShowed: true,
      alertMessage: 'Log in dengan sidik jari telah berhasil diaktifkan.',
      alertTitle: 'Berhasil',
      alertType: 'success',
      alertDoneText: 'OK',
    });
    this.setState({fingerPrint: true, popupShowed: false});
    await AsyncStorage.setItem('userFinger', detailUser.Email);
  };

  handleFingerprintDismissed = () => {
    this.setState({popupShowed: false});
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        testIdPositiveButton="OK"
        testIdNegativeButton="Cancel"
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
    if (this.state.alertFor === 'logout') {
      this.setState({alertFor: null});
    } else if (this.state.alertFor === 'jailMonkey') {
      BackHandler.exitApp();
    } else if (this.state.alertFor === 'invalidToken') {
      this.props.navigation.navigate('Login');
      this.setState({alertFor: null});
    }
  };

  render() {
    //set false to show warning box
    return (
      <Container style={{flex: 1}}>
        {
          Platform.OS == 'android' ? (
            <StatusBar
              barStyle="light-content"
              showHideTransition="fade"
              backgroundColor={BAF_COLOR_BLUE}></StatusBar>
          ) : (
            <View>
              <StatusBar
                barStyle="dark-content"
                hidden={false}
                translucent={false}
                networkActivityIndicatorVisible={true}
                showHideTransition="fade"
              />
            </View>
          )
          // Dimensions.get('window').width * PixelRatio.get() > 750 ? (
          //   <View style={styles.containerBig}>
          //     <StatusBar
          //       barStyle="light-content"
          //       hidden={false}
          //       translucent={false}
          //       networkActivityIndicatorVisible={true}
          //       showHideTransition="fade"
          //     />
          //   </View>
          // ) : (
          //   <View style={styles.containerSmall}>
          //     <StatusBar
          //       barStyle="light-content"
          //       hidden={false}
          //       translucent={false}
          //       networkActivityIndicatorVisible={true}
          //       showHideTransition="fade"
          //     />
          //   </View>
          // )
        }
        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />

        {this.state.tabState == 'home' ? (
          <Content
            refreshControl={
              <RefreshControl
                refreshing={false}
                onRefresh={() => this.onReload()}
              />
            }
            style={{height: '90%'}}>
            <Banner images={this.state.imagesApi} />
            {/* {this.state.userData == false ? (
              <GuestActivationCard userData={this.state.userData} />
            ) : (
              <UserActivationCard userData={this.state.userData} />
            )} */}
            {/* <UserActivationCard userData={this.state.userData} /> */}
            <View style={{height: 15}}></View>

            <BafPoints/>
            {Platform.OS == 'android' ? (
              <InstallmentsCardAndroid
                goLogin={() => this.goLogin()}
                goPaymentHistory={(params) => this.goPaymentHistory(params)}
                goBafPay={() => this.goBafPay()}
                userData={this.state.userData}
                submitNIK={(NIK) => this.submitNikHandle(NIK)}
                userToken={this.state.userToken}
                activeAgreement={this.state.activeAgreement}
                goSubmitOrder={(data) => this.goSubmitOrder(data)}
              />
            ) : (
              <InstallmentsCardIos
                goLogin={() => this.goLogin()}
                goPaymentHistory={(params) => this.goPaymentHistory(params)}
                goBafPay={() => this.goBafPay()}
                userData={this.state.userData}
                userToken={this.state.userToken}
                submitNIK={(NIK) => this.submitNikHandle(NIK)}
                userData={this.state.userData}
                userToken={this.state.userToken}
                activeAgreement={this.state.activeAgreement}
                goSubmitOrder={(data) => this.goSubmitOrder(data)}
              />
            )}

            <ListOrderCard
              userDetail={this.state.detailUser}
              countOrder={this.state.countOrder}
              goLogin={() => this.goLogin()}
              goStatusOrder={() => this.goStatusOrder()}
              userData={this.state.userData}
            />

            <InstallmentsButton
              goNoConnection={this.goNoConnection}
              goProduct={(Lob) => this.goProduct(Lob)}
            />
            <Separator />
            <Promo
              data={this.state.promoHome}
              promoPress={this.goPromoDetail}
              tabPromo={this.tabPromo}
              goNoConnection={this.goNoConnection}  
            />
            <Separator />
            <BafService
              goNoConnection={this.goNoConnection}
              detailUser={this.state.userData}
              nav={(location) => this.props.navigation.navigate(location)}
            />
          </Content>
        ) : this.state.tabState == 'promo' ? (
          <TabPromo
            detailPromo={this.state.detailPromo}
            goPromoDetail={this.goPromoDetail}
            goNavScreen={() => this.goFilter()}
            reloadScreen={() => this.reloadBanner()}
            detailUser={this.state.detailUser}
          />
        ) : this.state.tabState == 'akun' ? (
          <TabAkun
            userData={this.state.userData}
            goLogin={() => this.goLogin()}
            goLogout={() => this.goLogout()}
            goRegister={() => this.goRegister()}
            goPesananSaya={() => this.goPesananSaya()}
            goChangePassword={() => this.goChangePassword()}
            goRequestBPKB={() => this.goRequestBPKB()}
            goChangeProfile={() => this.goChangeProfile()}
            detailUser={this.state.detailUser}
            popupShowed={this.state.popupShowed}
            handleFingerprintDismissed={this.handleFingerprintDismissed}
            handleFingerprintShowed={this.handleFingerprintShowed}
            onAuthenticate={this.onAuthenticate}
            fingerPrint={this.state.fingerPrint}
          />
        ) : this.state.tabState == 'produk' ? (
          <TabProduk />
        ) : this.state.tabState == 'pesan' ? (
          <TabPesan
            goDetailPesan={(data) => this.goDetailPesan(data)}
            goLogin={() => this.goLogin()}
          />
        ) : (
          <> </>
        )}
        {this.showAlert()}
        {Platform.OS == 'android' ? (
          <FooterAndroid
            tabPromo={this.tabPromo}
            tabHome={this.tabHome}
            tabPesan={this.tabPesan}
            tabProduk={this.tabProduk}
            tabAkun={this.tabAkun}
            tabState={this.state.tabState}
          />
        ) : (
          <FooterIos
            tabPromo={this.tabPromo}
            tabHome={this.tabHome}
            tabPesan={this.tabPesan}
            tabProduk={this.tabProduk}
            tabAkun={this.tabAkun}
            tabState={this.state.tabState}
          />
        )}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    loginResult: state.login.result,
    loginOpenidResult: state.loginOpenid.result,
    logoutResult: state.logout.result,
    logoutError: state.logout.error,
    registerResult: state.register.result,
    detailBannerResult: state.detailBanner.result,
    detailBannerLoading: state.detailBanner.loading,
    detailBannerError: state.detailBanner.error,
    dataBannerResult: state.dataBanner.result,
    dataBannerLoading: state.dataBanner.loading,
    dataBannerError: state.dataBanner.error,
    detailUserResult: state.detailUser.result,
    detailUserLoading: state.detailUser.loading,
    detailUserError: state.detailUser.error,
    filterBannerResult: state.filterBanner.result,
    changePasswordError: state.changePassword.error,
    getActiveAgreementResult: state.getActiveAgreement.result,
    getActiveAgreementLoading: state.getActiveAgreement.loading,
    getActiveAgreementError: state.getActiveAgreement.error,
    submitNikResult: state.submitNik.result,
    submitNikLoading: state.submitNik.loading,
    submitNikError: state.submitNik.error,
    editUserResult: state.editUser.result,
    editUserError: state.editUser.error,
    orderCountResult: state.orderCount.result,
    orderCountError: state.orderCount.error,
    ddeInsert_Result: state.ddeInsert.result,
    option: state.filterBanner.result,
    refreshTokenResult: state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      orderCount,
      dataBanner,
      detailBanner,
      getActiveAgreement,
      detailUser,
      submitNik,
      resetFilterBanner,
      filterBanner,
      refreshToken,
      logout,
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

export default connect(mapStateToProps, matchDispatchToProps)(Home);
