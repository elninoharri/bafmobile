import { StyleSheet,Platform,StatusBar } from 'react-native';
import {BAF_COLOR_BLUE} from '../../utils/constant';

export default StyleSheet.create({
    containerBig: {
        alignItems: 'center',
        height: 44,
        backgroundColor:BAF_COLOR_BLUE,
      },
      containerSmall: {
        alignItems: 'center',
        height: "3%",
        backgroundColor:BAF_COLOR_BLUE,
      },
})