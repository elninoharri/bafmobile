import React, {Component} from 'react';
import {
  Text,
  View,
  Platform,
  BackHandler,
  Dimensions,
  PixelRatio,
  ScrollView,
} from 'react-native';
import {Container, Content} from 'native-base';
import {fonts} from '../../../utils/fonts';
import {information, tableConfigProps} from '../../../utils/html/information';
import styles from './style';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import HTML from 'react-native-render-html';

class Information extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Informasi Penting',

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      number: '',
    };
  }

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
    // if (this.state.alertFor === 'ddeInsert_Result') {
    //   this.props.goOrderList();
    // } else if (this.state.alertFor === 'ddeInsert_Error') {
    //   this.props.previousPage();
    // }
    this.setState({alertFor: null});
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  numbering = (data) => {
    switch (data) {
      case 1:
        return 'a';
      case 2:
        return 'b';
      case 3:
        return 'c';
      case 4:
        return 'd';
      case 5:
        return 'e';
      case 6:
        return 'f';
      default:
        break;
    }
  };

  render() {
    const html = information;
    return (
      <Container>
        {this.showAlert()}

        <Content>
          <View
            style={
              Platform.OS == 'android'
                ? Dimensions.get('window').width > 360
                  ? styles.headTextAndroidLarge
                  : styles.headTextAndroidSmall
                : Dimensions.get('window').width * PixelRatio.get() > 750
                ? styles.headTextIosLarge
                : styles.headTextIosSmall
            }>
            <Text
              style={
                Platform.OS == 'android'
                  ? Dimensions.get('window').width > 360
                    ? styles.titleHeadAndroidLarge
                    : styles.titleHeadAndroidSmall
                  : Dimensions.get('window').width * PixelRatio.get() > 750
                  ? styles.titleHeadIosLarge
                  : styles.titleHeadIosSmall
              }>
              Informasi Penting untuk Konsumen Multiproduk
            </Text>
          </View>
          <ScrollView style={{marginHorizontal: '5%'}}>
            <HTML
              source={{html}}
              {...tableConfigProps}
              baseFontStyle={{
                fontSize: 14,
                fontFamily: fonts.primary.normal,
                lineHeight: 20,
                color: 'black',
              }}
              listsPrefixesRenderers={{
                ol: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                  const num = passProps.index + 1;
                  return (
                    <Text
                      style={{fontSize: 14, fontFamily: fonts.primary.normal}}>
                      {' '}
                      {this.numbering(num)}.{' '}
                    </Text>
                  );
                },
              }}
            />
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

export default Information;
