import { StyleSheet, Dimensions, PixelRatio } from 'react-native';
import { fonts } from '../../../utils/fonts';

//Notes
// Untuk ios blm di styling

export default StyleSheet.create({
  headTextAndroidLarge: {
    marginTop: 25,
    width: 357,
    marginBottom: "2%",
    alignSelf: 'center',
    // backgroundColor: "gray"
  },
  headTextAndroidSmall: {
    marginTop: 25,
    width: 280,
    marginBottom: "2%",
    alignSelf: 'center',
    // backgroundColor: "gray"
  },


  headTextIosLarge: {
    width: '90%',
    marginTop: "7%",
    marginBottom: "5%",
    alignSelf: 'center',
  },
  headTextIosSmall: {
    width: '90%',
    marginTop: 30,
    alignSelf: 'center',
  },

  titleHeadAndroidLarge: {
    fontSize: 22,
    fontFamily: fonts.primary.normal,
    color: 'black',
    lineHeight: 28,

  },

  titleHeadAndroidSmall: {
    fontSize: 18,
    fontFamily: fonts.primary.normal,
    color: 'black',
    lineHeight: 28,
  },


  titleHeadIosLarge: {
    fontSize: 24,
    fontFamily: fonts.primary.normal,
    color: 'black'
  },
  titleHeadIosSmall: {
    fontSize: 20,
    fontFamily: fonts.primary.normal,
    color: 'black'
  },

})