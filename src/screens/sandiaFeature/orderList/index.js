import React, { Component } from 'react';
import {
  Text,
  View,
  RefreshControl,
  Platform,
  ScrollView,
  BackHandler,
} from 'react-native';
import { Container, Icon, Spinner, Content } from 'native-base';
import SubHeaderAndroidBlue from '../../../components/android/subHeaderBlue';
import SubHeaderIosBlue from '../../../components/ios/subHeaderBlue';
import styles from './style';
import ListOrder from '../../../components/multiPlatform/sandiaFeature/listOrder';
import SearchInput, { createFilter } from 'react-native-search-filter';
import {
  orderFilter,
  cancelPO,
  confirmPass,
} from '../../../actions/sandiaFeature/dataAwal';
import { detailUser, refreshToken } from '../../../actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, Field, change } from 'redux-form';
import { FormOrderFilterValidate } from '../../../validates/FormOrderFilterValidate';
import AsyncStorage from '@react-native-community/async-storage';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import { fonts } from '../../../utils/fonts';
import analytics from '@react-native-firebase/analytics';
import ModalKonfirmasiPassword from '../../../components/multiPlatform/modalKonfirmasiPassword';
import { default as Spinners } from 'react-native-loading-spinner-overlay';
import { jsonParse, handleRefreshToken } from '../../../utils/utilization';
import {
  TOKEN_EXP,
  MESSAGE_TOKEN_EXP
} from '../../../utils/constant';

class OrderList extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      screenTitle: 'Status Order',
      searchTerm: '',
      modalVisible: false,
      userToken: false,
      detailUser: false,
      spinners: false,
      userData: false,

      pickerLobItem: [
        { Code: 'NMC', Descr: 'Motor Yamaha' },
        { Code: 'MP', Descr: 'Multiproduk' },
        { Code: 'SYANA', Descr: 'Dana Syariah' },
        { Code: 'CAR', Descr: 'Mobil Baru' },
      ],
      pickerLobSelected: 'MP',

      active: false,
      modalKonfirmasiPassword: false,
      orderSelected: '',
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',

      dataBarang: false,
    };
  }

  componentDidMount = async () => {
    const { orderFilter } = this.props;

    analytics().logScreenView({
      screen_class: 'screenListOrder',
      screen_name: 'screenListOrder',
    });
    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) {
      this.setState({ userToken: userToken });
    }

    const jsonDetailUser = await AsyncStorage.getItem('detailUser');
    if (jsonDetailUser) {
      //check jika order datang dari push notif, apakah user dalam keadaan logout atau login
      var detailUser = JSON.parse(jsonDetailUser);
      this.setState({ detailUser: detailUser });
    } else {
      this.props.navigation.navigate('Login');
    }

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

    orderFilter({
      Idno: this.state.detailUser.NIK ? this.state.detailUser.NIK : '',
      Lobcode: '',
      Isapproved: '',
      Orderstat: '',
    });

  };

  componentWillUnmount = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  };

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      orderFilter,
      cancelPO,
      ddeInsert_Result,
      orderFilterResult,
      confirmPassResult,
      confirmPassLoading,
      confirmPassError,
      cancelPOResult,
      cancelPOLoading,
      loginResult,
      loginOpenidResult,
      cancelPOError,
      detailUser,
      detailUserResult,
      detailUserError,
      detailUserLoading,
      refreshTokenResult,
      refreshTokenError,
    } = this.props;
    //Hasil dari login digunakan untuk ngehit API detail user untuk keperluan orderlist - rz
    if (
      (loginResult !== null && prevProps.loginResult !== loginResult) ||
      (loginOpenidResult !== null &&
        prevProps.loginOpenidResult !== loginOpenidResult)
    ) {
      if (loginResult) {
        //dicek apakah masuknya lewat loginresult atau loginOpenIdResult
        console.log("Login success");
        this.setState({
          userData: {
            Usergroupid: loginResult.UserGroupID,
          },
        });
        detailUser({}, loginResult.Usertoken);
      } else if (loginOpenidResult) {
        detailUser(
          {},
          loginOpenidResult.Usertoken,
        );
      }
    }

    if (detailUserError && prevProps.detailUserError !== detailUserError) {
      const data = await jsonParse(detailUserError.message); //di sini ngeparse sekaligus ngecek apakah data kiriman error berupa json
      if (data) {
        this.validateRefreshToken(data);
      } else {
        console.log(detailUserError.message);
      }
    }

    //Hasil dari orderlist menyerupai fungsi di home dengan menambahkan detailuser ke async storage sekaligus menghit API orderfilter  -rz
    if (
      detailUserResult !== null &&
      prevProps.detailUserResult !== detailUserResult
    ) {
      await AsyncStorage.removeItem('detailUser');
      await AsyncStorage.setItem(
        'detailUser',
        JSON.stringify(detailUserResult),
      );
      await this.setState({ detailUser: detailUserResult });
      orderFilter({
        Idno: this.state.detailUser.NIK ? this.state.detailUser.NIK : '',
        Lobcode: '',
        Isapproved: '',
        Orderstat: '',
      });
    }

    if (
      confirmPassResult !== null &&
      prevProps.confirmPassResult !== confirmPassResult
    ) {
      const dataMerge = {
        AppNo: this.state.orderSelected.DEST_ID,
        Orderno: this.state.orderSelected.ORDER_NO,
        Usrcrt: '1',
      };
      console.log(dataMerge);
      cancelPO(dataMerge);
    }

    if (
      confirmPassError !== null &&
      prevProps.confirmPassError !== confirmPassError
    ) {
      const data = await jsonParse(confirmPassError.message); //di sini ngeparse sekaligus ngecek apakah data kiriman error berupa json
      if (data) {
        this.validateRefreshToken(data);
      } else {
        this.setState({
          alertShowed: true,
          alertMessage: confirmPassError.message
            ? confirmPassError.message
            : 'Password yang anda masukan salah',
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
    }

    if (
      cancelPOResult !== null &&
      prevProps.cancelPOResult !== cancelPOResult
    ) {
      this.setState({
        alertShowed: true,
        alertMessage: 'Order anda sudah dibatalkan',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
        spinners: false,
      });

      orderFilter({
        Idno: this.state.detailUser.NIK ? this.state.detailUser.NIK : '',
        Lobcode: '',
        Isapproved: '',
        Orderstat: '',
      });
    }

    if (cancelPOError !== null && prevProps.cancelPOError !== cancelPOError) {
      this.setState({
        alertShowed: true,
        alertMessage: cancelPOError.message,
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
        spinners: false,
      });
    }

    if (
      ddeInsert_Result !== null &&
      prevProps.ddeInsert_Result !== ddeInsert_Result
    ) {
      orderFilter({
        Idno: this.state.detailUser.NIK ? this.state.detailUser.NIK : '',
        Lobcode: '',
        Isapproved: '',
        Orderstat: '',
      });
    }

  

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      console.log('Error Refresh Token ', refreshTokenError.message);
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      await handleRefreshToken(refreshTokenResult.Refreshtoken);
      this.setState({ userToken: refreshTokenResult.Refreshtoken });
      if (detailUserError) {
        detailUser({}, refreshTokenResult.Refreshtoken);
      } else if (confirmPassError) {
        this.props.confirmPass(this.state.dataMerge, refreshTokenResult.Refreshtoken);
      }
    }
  };

  // di sini ngecek apakah result nya berupa message error expired atau tidak
  validateRefreshToken = (data) => {
    if (data) {
      if (data.message ==  MESSAGE_TOKEN_EXP) {
        this.props.refreshToken(this.state.userToken);
      }
    }
  }

  changeLob(value) {
    this.setState({ pickerLobSelected: value });
    this.props.updateField('formOrderFilter', 'Lob', value);
  }

  handlePositiveButtonAlert = () => {
    this.setState({ alertShowed: false });
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  defaultTemplate = () => {
    const {
      orderFilterLoading,
      orderFilterError,
      detailUserLoading,
    } = this.props;
    return orderFilterLoading || detailUserLoading ? ( //Untuk menambahkan komponen loading ketika detailuser atau orderfilter diambil -rz
      <Spinner color="#002f5f" />
    ) : orderFilterError && orderFilterError !== null ? (
      <View style={{ margin: 100, alignItems: 'center' }}>
        <Icon
          type="FontAwesome"
          name="search"
          style={{ color: 'grey', marginBottom: 10, fontSize: 40 }}
        />
        <Text
          style={{
            fontSize: 12,
            color: 'grey',
            fontFamily: fonts.primary.normal,
          }}>
          Data tidak ditemukan
        </Text>
      </View>
    ) : null;
  };

  getListOrder = (data, index) => {
    return (
      <ListOrder
        data={data}
        index={index}
        key={data.ORDER_TRX_H_ID}
        goFdeAdd={this.goFdeAdd}
        goPpbkApproval={this.goPpbkApproval}
        goFdeRevise={this.goFdeRevise}
        goKonfirmasiPassword={() => this.goKonfirmasiPassword(data)}
      />
    );
  };

  goPpbkApproval = (data) => {
    this.props.navigation.navigate('ppbkApproval', { data: data });
  };

  goFdeAdd = (lob, orderID) => {
    if (lob == 'MP') {
      this.props.navigation.navigate('FdeAddMp', { params: orderID });
    } else {
      this.setState({
        alertShowed: true,
        alertMessage: 'Fitur ini sedang dalam tahap pengembangan.',
        alertTitle: 'Informasi',
        alertType: 'info',
        alertDoneText: 'OK',
      });
    }
  };

  goFdeRevise = (lob, orderID) => {
    const Revise = 'Revise';
    if (lob == 'MP') {
      this.props.navigation.navigate('FdeDetailMp', {
        params: orderID,
        Type: Revise,
      });
    } else {
      this.setState({
        alertShowed: true,
        alertMessage: 'Fitur ini sedang dalam tahap pengembangan.',
        alertTitle: 'Informasi',
        alertType: 'info',
        alertDoneText: 'OK',
      });
    }
  };

  goBack = () => {
    this.props.navigation.navigate('Home');
  };

  onChangeText = (term) => {
    this.searchUpdated(term);
  };

  onReload = () => {
    const { orderFilter } = this.props;

    orderFilter({
      Idno: this.state.detailUser.NIK ? this.state.detailUser.NIK : '',
      Lobcode: '',
      Isapproved: '',
      Orderstat: '',
    });
  };

  onSubmit(values) {
    const { orderFilter } = this.props;

    this.setState({ modalVisible: !this.state.modalVisible });

    orderFilter({
      Idno: this.state.detailUser.NIK ? this.state.detailUser.NIK : '',
      Lobcode: '',
      Isapproved: '',
      Orderstat: '',
    });
  }

  onSubmitKonfirmasiPassword = async (data) => {
    const { detailUser, userToken } = this.state;

    if (userToken && detailUser) {
      const dataMerge = {
        Phoneno: detailUser.Phoneno,
        Password: data.password,
      };
      this.setState({ modalKonfirmasiPassword: false, dataMerge: dataMerge });
      this.props.updateField('formQdeFilter', 'password', '');
      this.props.confirmPass(dataMerge, userToken);
    }
  };

  searchUpdated = (term) => {
    this.setState({ searchTerm: term });
  };

  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible });
  };

  goKonfirmasiPassword = (data) => {
    this.setState({
      modalKonfirmasiPassword: true,
      orderSelected: data,
    });
    // this.props.navigation.navigate("KonfirmasiPassword", {Ordertrxhid: Ordertrxhid});
  };

  setModalKonfirmasi = () => {
    this.setState({
      modalKonfirmasiPassword: false,
    });
  };

  render() {
    const KEYS_TO_FILTERS = ['FULLNAME', 'ORDER_NO', 'IDNO'];
    const {
      orderFilterResult,
      handleSubmit,
      submitting,
      cancelPOLoading,
      detailUserLoading,
      confirmPassLoading,
    } = this.props;
    const filteredData = orderFilterResult.filter(
      createFilter(this.state.searchTerm, KEYS_TO_FILTERS),
    );
    return (
      <Container>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroidBlue
            title={this.state.screenTitle}
            goBack={this.goBack}
            goDdeAdd={this.goDdeAdd}
          />
        ) : (
            <SubHeaderIosBlue
              title={this.state.screenTitle}
              goBack={this.goBack}
              goDdeAdd={this.goDdeAdd}
            />
          )}

        <Spinners
          visible={cancelPOLoading || confirmPassLoading ? true : false}
          textContent={'Sedang memproses...'}
          overlayColor="rgba(0, 0, 0, 0.80)"
          textStyle={{ color: '#FFF' }}
        />

        <View style={styles.viewHeader}>
          <SearchInput
            style={styles.SearchInput}
            onChangeText={this.onChangeText}
            placeholder="Cari..."
          />
        </View>
        <ScrollView style={styles.ScrollView}>
          <Content
            refreshControl={
              <RefreshControl
                refreshing={false}
                onRefresh={() => this.onReload()}
              />
            }>
            {filteredData.length > 0
              ? filteredData
                .sort()
                .reverse()
                .map((data, index) => this.getListOrder(data, index))
              : this.defaultTemplate()}
          </Content>
          <View style={styles.ScrollViewBottom} />
        </ScrollView>

        {this.showAlert()}

        <ModalKonfirmasiPassword
          setModalKonfirmasi={() => this.setModalKonfirmasi()}
          modalKonfirmasiPassword={this.state.modalKonfirmasiPassword}
          onSubmitKonfirmasiPassword={this.onSubmitKonfirmasiPassword}
          handleSubmit={handleSubmit}
          submitting={submitting}
        />
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    orderFilterLoading: state.orderFilter.loading,
    orderFilterResult: state.orderFilter.result,
    orderFilterError: state.orderFilter.error,
    formOrderFilter: state.form.formOrderFilter,
    ddeInsert_Result: state.ddeInsert.result,
    cancelPOLoading: state.cancelPO.loading,
    cancelPOResult: state.cancelPO.result,
    cancelPOError: state.cancelPO.error,
    confirmPassLoading: state.confirmPass.loading,
    confirmPassResult: state.confirmPass.result,
    confirmPassError: state.confirmPass.error,
    loginLoading: state.login.loading,
    loginResult: state.login.result,
    loginOpenidLoading: state.loginOpenid.loading,
    loginOpenidResult: state.loginOpenid.result,
    detailUserResult: state.detailUser.result,
    detailUserLoading: state.detailUser.loading,
    detailUserError: state.detailUser.error,
    refreshTokenResult : state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      orderFilter,
      detailUser,
      confirmPass,
      cancelPO,
      refreshToken,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
    },
    dispatch,
  );
}

OrderList = reduxForm({
  form: 'formQdeFilter',
  enableReinitialize: true,
  validate: FormOrderFilterValidate,
})(OrderList);

export default connect(mapStateToProps, matchDispatchToProps)(OrderList);
