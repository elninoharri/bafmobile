import React, {Component} from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  BackHandler,
  StyleSheet,
  Platform,
  Dimensions,
  PixelRatio,
} from 'react-native';

import Toast from 'react-native-easy-toast';

import {
  otpRequest,
  otpRegenerate,
  otpValidatePrivy,
  updateStatusOrderPrivy,
  uploadDoc,
} from '../../../actions/privy';

import PDFView from 'react-native-view-pdf';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';

import {BAF_COLOR_BLUE, MESSAGE_TOKEN_EXP} from '../../../utils/constant';
import {fonts} from '../../../utils/fonts';
import OtpWizard from '../fdeAddMp/form/otpWizard';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import {qdeDetailData} from '../../../actions/sandiaFeature/dataAwal';
import Spinner from 'react-native-loading-spinner-overlay';
import {refreshToken} from '../../../actions';
import {handleRefreshToken, jsonParse} from '../../../utils/utilization';

//di sini kita coba untuk dapetin base 64 dari pdf untuk di render menggunakan library baru di halaman ppbkApprovalPage
//Kita coba gunakan existing otpWizard dengan mengubah komponen otpWizard menjadi dinamis

class PpbkApprovalPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Persetujuan',
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      base64: 'empty',
      dataOrder: false,
      detailUser: false,
      isConnected: false,
      registerID: false,
      transactionID: false,
      chance: 0,
      noHp: false,
      userToken: false,
      dataMergeRequest: false,
      dataMergeRegenerate: false,
      dataMergeValidate: false,
      dataMergeUploadDoc: false,
    };
  }

  componentDidMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.goBack);

    // console.log('data param');
    // console.log(data);

    const detailUserNonParse = await AsyncStorage.getItem('detailUser');
    const {data, TokenSandia} = this.props.route.params;

    if (detailUserNonParse) {
      const detailUserParse = JSON.parse(detailUserNonParse);
      this.setState({detailUser: detailUserParse});
      console.log(this.state.detailUser);
    }

    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) {
      this.setState({userToken: userToken});
    }

    console.log(data);

    this.setState({dataOrder: data});

    //Fix 20-01-2021: Kudu dipasang qdeDetailData_Result untuk dapat no handphone hasil input FDE? ORDER_TRX_H_ID

    this.props.qdeDetailData({Ordertrxhid: data.ORDER_TRX_H_ID}, TokenSandia);

    if (data.PPBKDOC && data.PPBKDOC !== '') {
      //librarynya maunya murni encodeBase64 karena di dalemnya udah punya blob tersendiri
      //, gak bisa data:file/pdf;base64,encodeBase64 jadi harus di trim/replace
      this.setState({
        base64: data.PPBKDOC.includes('data:file/pdf;base64,')
          ? data.PPBKDOC.replace('data:file/pdf;base64,', '')
          : data.PPBKDOC,
      });
    }
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      otpRequestPrivy_result,
      otpRequestPrivy_error,
      otpValidatePrivy_result,
      otpValidatePrivy_error,
      updateStatusOrderPrivy_target,
      updateStatusOrderPrivy_result,
      updateStatusOrderPrivy_error,
      uploadDoc_error,
      uploadDoc_result,
      otpRegeneratePrivy_result,
      otpRegeneratePrivy_error,
      qdeDetailData_result,
      qdeDetailData_error,
      refreshToken_error,
      refreshToken_result,
    } = this.props;

    const {dataOrder, userToken} = this.state;

    if (
      refreshToken_result !== null &&
      prevProps.refreshToken_result !== refreshToken_result
    ) {
      await handleRefreshToken(refreshToken_result.Refreshtoken);
      this.setState({userToken: refreshToken_result.Refreshtoken});

      //if ini akan nembak api ulang ketika error diakibatkan oleh token expired, masing-masing data sudah disimpan di state

      if (otpRequestPrivy_error) {
        this.props.otpRequest(
          this.state.dataMergeRequest,
          refreshToken_result.Refreshtoken,
        );
      }

      if (otpRegeneratePrivy_error) {
        this.props.otpRegenerate(
          this.state.dataMergeRegenerate,
          refreshToken_result.Refreshtoken,
        );
      }

      if (otpValidatePrivy_error) {
        this.props.otpValidatePrivy(
          this.state.dataMergeValidate,
          refreshToken_result.Refreshtoken,
        );
      }

      if (uploadDoc_error) {
        this.props.uploadDoc(
          this.state.dataMergeUploadDoc,
          refreshToken_result.Refreshtoken,
        );
      }
    }

    if (
      otpRequestPrivy_error !== null &&
      prevProps.otpRequestPrivy_error !== otpRequestPrivy_error
    ) {
      const data = await jsonParse(otpRequestPrivy_error.message);
      if (data) {
        this.validateRefreshToken(data);
      } else {
        console.log(otpRequestPrivy_error.message);
      }
    }

    if (
      otpRegeneratePrivy_error !== null &&
      prevProps.otpRegeneratePrivy_error !== otpRegeneratePrivy_error
    ) {
      const data = await jsonParse(otpRegeneratePrivy_error.message);
      if (data) {
        this.validateRefreshToken(data);
      } else {
        console.log(otpRegeneratePrivy_error.message);
      }
    }

    //Fix 20-01-2020: QDEDetailUser buat dapetin data dari isi FDE piloting untuk keperluan data di PPBK Approval
    if (
      qdeDetailData_result !== null &&
      prevProps.qdeDetailData_result !== qdeDetailData_result
    ) {
      this.setState({noHp: qdeDetailData_result.Handphone1});
    }

    //1. HIT API OTP REQUEST --> daftarin register id & transaction id.
    //Kalo user gagal masukin, regenerate, daftarin register id & transaction id baru.
    // error handle: not yet, should be nembak lagi otpRequestnya atau kasih alert dan dibawa lagi ke ppbk page?

    if (
      otpValidatePrivy_error !== null &&
      prevProps.otpValidatePrivy_error !== otpValidatePrivy_error
    ) {
      const data = await jsonParse(otpValidatePrivy_error.message);
      if (data) {
        this.validateRefreshToken(data);
      } else {
        console.log(otpValidatePrivy_error);
      }
    }

    if (
      otpRequestPrivy_result !== null &&
      prevProps.otpRequestPrivy_result !== otpRequestPrivy_result
    ) {
      this.setState({
        registerID: otpRequestPrivy_result.data.register_id,
        transactionID: otpRequestPrivy_result.data.transaction_id,
      });
      console.log(otpRequestPrivy_result);
    }

    if (
      otpRegeneratePrivy_result !== null &&
      prevProps.otpRegeneratePrivy_result !== otpRegeneratePrivy_result
    ) {
      this.setState({
        registerID: otpRegeneratePrivy_result.data.register_id,
        transactionID: otpRegeneratePrivy_result.data.transaction_id,
      });
    }

    //2. Setelah validation, hit uploadDoc API
    // error handle: di dalam otpWizard, 5x gagal dibuang ke home

    if (
      otpValidatePrivy_result !== null &&
      prevProps.otpValidatePrivy_result !== otpValidatePrivy_result
    ) {
      this.handleUploadDoc();
    }

    //3. Setelah berhasil uploadDoc, maka tembak checkStatus untuk BE simpen privyID
    //Error handle: di bawa lagi ke screen persetujuan, melakukan OTP lagi?

    if (
      uploadDoc_result !== null &&
      prevProps.uploadDoc_result !== uploadDoc_result
    ) {
      console.log('Upload doc result kelar');
      this.updateStatusHandle('approve');
    }

    if (
      uploadDoc_error !== null &&
      prevProps.uploadDoc_error !== uploadDoc_error
    ) {
      console.log(uploadDoc_error);
      const data = await jsonParse(uploadDoc_error.message);
      if (data) {
        this.validateRefreshToken(data);
      } else {
        this.setState({
          alertFor: 'uploadDoc_error',
          alertShowed: true,
          alertMessage:
            uploadDoc_error.message +
            ': Koneksi anda bermasalah untuk memproses lebih lanjut dokumen PPBK anda',
          alertTitle: 'Network Error',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
    }

    //5. Untuk hand updateStatusOrderPrivy_result dibedakan antara API untuk reject (23 15) dan approve (23 14)

    if (
      updateStatusOrderPrivy_result !== null &&
      prevProps.updateStatusOrderPrivy_result !== updateStatusOrderPrivy_result
    ) {
      console.log('updateStatusOrderPrivy success');
      console.log(updateStatusOrderPrivy_result);
      //bikin props tambahan di redux untuk param apakah update status untuk batalkan atau untuk berhasil
      if (updateStatusOrderPrivy_target == 'reject') {
        this.setState({
          alertFor: 'updateStatusOrderPrivy_result',
          alertShowed: true,
          alertMessage: 'Order anda berhasil dibatalkan',
          alertTitle: 'Berhasil',
          alertType: 'success',
          alertDoneText: 'OK',
        });
      } else if (updateStatusOrderPrivy_target == 'approve') {
        //Proses otp validation --> upload doc --> updateStatusOrderPrivy
        //Mengikuti figma, maka seakan-akan respon masih berada di validation otp

        this.setState({
          alertFor: 'updateStatusOrderPrivy_result',
          alertShowed: true,
          alertMessage: 'Kode OTP anda valid',
          alertTitle: 'Berhasil',
          alertType: 'success',
          alertDoneText: 'OK',
        });
      }

      //dia akan nembak lagi kalo gagal sesuai dengan target-nya (approve or reject), if 3 kali gagal kasih alert.

      if (
        updateStatusOrderPrivy_error !== null &&
        prevProps.updateStatusOrderPrivy_error !== updateStatusOrderPrivy_error
      ) {
        if (this.state.chance < 3) {
          this.updateStatusHandle(updateStatusOrderPrivy_error.target);
          this.setState((prevState) => ({chance: prevState.chance + 1}));
        } else {
          this.setState({
            chance: 0,
            alertFor: 'updateStatusOrderPrivy_error',
            alertShowed: true,
            alertMessage:
              updateStatusOrderPrivy_error.message +
              ': Koneksi anda bermasalah untuk memproses lebih lanjut pesanan anda',
            alertTitle: 'Network Error',
            alertType: 'error',
            alertDoneText: 'OK',
          });
        }
      }
    }
  };

  validateRefreshToken = async (data) => {
    console.log('enter validate refresh token');

    if (data) {
      if (data.message == MESSAGE_TOKEN_EXP) {
        console.log('hit refresh token');
        this.props.refreshToken(this.state.userToken);
      }
    }
  };

  handleUploadDoc = () => {
    const {dataOrder, userToken} = this.state;

    const dataMerge = {
      reference_number: dataOrder.DEST_ID,
      Docfile: dataOrder.PPBKDOC ? dataOrder.PPBKDOC : '', //nanti dapet dari dataOrder
    };

    //Fix 20-01-2021: Di sagas uploadDoc dipasang replace newLine akibat json.stringfy pada data base64
    this.setState({dataMergeUploadDoc: dataMerge});

    this.props.uploadDoc(dataMerge, userToken);
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
    if (this.state.alertFor === 'updateStatusOrderPrivy_result') {
      this.props.navigation.navigate('Home');
    } else {
      this.setState({screenTitle: 'Persetujuan'});
    }
    this.setState({alertFor: null});
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  otpPrivyHandle = async (command, value) => {
    const {dataOrder, detailUser, noHp, userToken} = this.state;
    //Recieve data in props nav (if possible)
    //Still cannot be tested whether API is working or not if submit API is not done,
    //but we can console log the data merge and see the error as a prove that the whole setup is correct
    switch (command) {
      case 'request':
        //the error in request should hit another request or give prompt to user to reload the page, we need register id for whole process
        const dataMerge = {
          AppNo: dataOrder.DEST_ID, //Appno, checked bahwa DEST_ID memiliki nomor yang sama dengan APPNO di QDEDETAIL
          phone: noHp ? noHp : detailUser.Phoneno, //if no hp not exist, we shoot it from detailUser
          customer_type: 'debitur', //hardcode
          custom_otp: 'false',
          reference_number: dataOrder.DEST_ID, //Appno
        };
        console.log(dataMerge);
        this.setState({dataMergeRequest: dataMerge});
        this.props.otpRequest(dataMerge, userToken); //should be uncomment after test
        break;
      case 'regenerate':
        const dataMergeRegenerate = {
          AppNo: dataOrder.DEST_ID,
          register_id: this.state.registerID, //from request OTP, should be saved in state when componentDidUpdate happen
        };
        console.log(dataMergeRegenerate);
        this.setState({dataMergeRegenerate: dataMergeRegenerate});
        this.props.otpRegenerate(dataMergeRegenerate, userToken);
        break;
      case 'validate':
        const dataMergeValidate = {
          AppNo: dataOrder.DEST_ID,
          register_id: this.state.registerID,
          otp_code: value,
          transaction_id: this.state.transactionID, //from request/regenerate
        };
        console.log(dataMergeValidate);
        this.setState({dataMergeValidate: dataMergeValidate});
        this.props.otpValidatePrivy(dataMergeValidate, userToken);
        break;
      default:
        console.log('Wrong input');
    }
  };

  goHome = async () => {
    this.props.navigation.navigate('Home');
  };

  goBack = async () => {
    {
      if (this.props.navigation.isFocused()) {
        this.state.screenTitle === 'Verifikasi Kode OTP'
          ? this.setState({screenTitle: 'Persetujuan'})
          : this.props.navigation.goBack();
      }
    }
  };

  submitHandle = () => {
    this.setState({screenTitle: 'Verifikasi Kode OTP'});
  };

  updateStatusHandle = async (target) => {
    const {dataOrder} = this.state;

    // 23 13	Sedang Proses E-Sign
    // 23 14	E-Sign Diterima --> after berhasil
    // 23 15	E-Sign Ditolak --> kalo gagal
    // 23 16  Golive E Assign

    switch (target) {
      case 'reject':
        const dataMergeReject = {
          Orderno: dataOrder.ORDER_NO,
          Destid: dataOrder.DEST_ID,
          Podata: '',
          Orderstat: '23',
          Isapproved: '15',
          StatusPrivy: '0',
          Notes: 'Reject', //Diisi apa?
          Usrcrt: '1', //Pake detailUser?
        };

        // terdapat param baru, di kedua sebagai pembeda target update status, untuk confirm atau cancel order
        // sebagai pembeda di componentDidUpdate apakah tujuannya reject atau confirm di alert
        this.props.updateStatusOrderPrivy(dataMergeReject, 'reject');

        break;
      case 'approve':
        const dataMergeApprove = {
          Orderno: dataOrder.ORDER_NO,
          Destid: dataOrder.DEST_ID,
          Podata: '',
          Orderstat: '23',
          Isapproved: '14',
          StatusPrivy: '1',
          Notes: 'Approve', //Diisi apa?
          Usrcrt: '1', //Pake detailUser?
        };

        // terdapat param baru, di kedua sebagai pembeda target update status, untuk confirm atau cancel order
        // sebagai pembeda di componentDidUpdate apakah tujuannya cancel atau confirm di alert
        this.props.updateStatusOrderPrivy(dataMergeApprove, 'approve');
        break;
      default:
        return true;
    }
  };

  //***********COMPONENT */

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  header = () => {
    return (
      <>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}
      </>
    );
  };

  ppbkApprovalPageRender = () => {
    //there should be loading screen when PDFView not rendering, onLoad will setState the animaction of loading (not yet implemented)
    return (
      <>
        <PDFView
          fadeInDuration={250.0}
          style={{flex: 1}}
          resource={this.state.base64} //resource cannot be empty string
          resourceType="base64"
          onLoad={() => console.log(`PDF rendered from pdf`)}
          onError={(error) => console.log('Cannot render PDF', error)}
        />
        <View style={styles.buttonWrapper}>
          {this.button(
            'Batalkan',
            () => this.updateStatusHandle('reject'),
            BAF_COLOR_BLUE,
            BAF_COLOR_BLUE,
            'white',
          )}
          <View style={styles.seperator} />

          {this.button(
            'Setujui',
            this.submitHandle,
            BAF_COLOR_BLUE,
            'white',
            BAF_COLOR_BLUE,
          )}
        </View>
      </>
    );
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        testIdPositiveButton="OK"
        testIdNegativeButton="Cancel"
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  toast = () => {
    return (
      <>
        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />
      </>
    );
  };

  button = (title, handleButton, borderColor, textColor, backgroundColor) => {
    return (
      <>
        <TouchableOpacity
          onPress={handleButton}
          style={styles.button(backgroundColor, borderColor)}>
          <Text style={styles.textButton(textColor)}>{title}</Text>
        </TouchableOpacity>
      </>
    );
  };

  spinner = () => {
    const {
      updateStatusOrderPrivy_loading,

      uploadDoc_loading,

      qdeDetailData_loading,
      otpValidatePrivy_loading,
    } = this.props;
    return (
      <Spinner
        //updateStatus, uploadDoc, untuk OTP udah ada di component-nya
        visible={
          updateStatusOrderPrivy_loading ||
          uploadDoc_loading ||
          qdeDetailData_loading ||
          otpValidatePrivy_loading
            ? true
            : false
        }
        textContent={'Loading...'}
        textStyle={{color: '#FFF'}}
      />
    );
  };

  render = () => {
    return (
      <View style={styles.container}>
        {this.header()}
        {this.spinner()}
        {this.toast()}
        {this.showAlert()}
        {this.state.screenTitle === 'Persetujuan' ? (
          this.ppbkApprovalPageRender()
        ) : (
          //tambah componentLoading di sini
          <OtpWizard
            otpPrivyHandle={this.otpPrivyHandle}
            goHome={() => this.goHome()}
            noHp={this.state.noHp}
          />
        )}
      </View>
    );
  };
}

function mapStateToProps(state) {
  return {
    otpRequestPrivy_loading: state.otpRequestPrivy.loading,
    otpRequestPrivy_result: state.otpRequestPrivy.result,
    otpRequestPrivy_error: state.otpRequestPrivy.error,
    otpRegeneratePrivy_loading: state.otpRegeneratePrivy.loading,
    otpRegeneratePrivy_result: state.otpRegeneratePrivy.result,
    otpRegeneratePrivy_error: state.otpRegeneratePrivy.error,
    otpValidatePrivy_loading: state.otpValidatePrivy.loading,
    otpValidatePrivy_result: state.otpValidatePrivy.result,
    otpValidatePrivy_error: state.otpValidatePrivy.error,
    qdeDetailData_loading: state.qdeDetailData.loading,
    qdeDetailData_result: state.qdeDetailData.result,
    qdeDetailData_error: state.qdeDetailData.error,
    updateStatusOrderPrivy_target: state.updateStatusOrderPrivy.target,
    updateStatusOrderPrivy_result: state.updateStatusOrderPrivy.result,
    updateStatusOrderPrivy_loading: state.updateStatusOrderPrivy.loading,
    updateStatusOrderPrivy_error: state.updateStatusOrderPrivy.error,
    uploadDoc_loading: state.uploadDoc.loading,
    uploadDoc_result: state.uploadDoc.result,
    uploadDoc_error: state.uploadDoc.error,
    refreshToken_result: state.refreshToken.result,
    refreshToken_error: state.refreshToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateStatusOrderPrivy,
      otpRequest,
      otpRegenerate,
      otpValidatePrivy,
      qdeDetailData,
      uploadDoc,
      refreshToken,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, matchDispatchToProps)(PpbkApprovalPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonWrapper: {
    backgroundColor: 'white',
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'space-between',
    flexDirection: 'row',
  },

  seperator: {width: 20},
  button: (backgroundColor, borderColor) => ({
    backgroundColor: backgroundColor,
    borderWidth: 1,
    borderColor: borderColor,
    width: '40%', //should change into percent when checking width for another screen size
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50, //should change into percent when checking width for another screen size
  }),
  textButton: (textColor) => ({
    color: textColor,
    fontSize: 18,
    fontFamily: fonts.primary.normal,
  }),
});
