export const dataArray = [
  {
    title: 'Data Pemohon',
    content: [
      {title: 'Nomor Aplikasi', value: '130MPP11012101230'},
      {title: 'Sumber Aplikasi', value: 'Pusat'},
      {title: 'Nomor Order', value: '12109281029829121'},
      {title: 'Nama Konsumen', value: 'Reza Pratama Nugraha'},
      {title: 'Nama Panggilan', value: 'Reza'},
      {title: 'NIK KTP', value: '827505091150002'},
      {title: 'Masa berlaku KTP', value: 'Selamanya'},
      {title: 'Tempat Lahir', value: 'London'},
      {title: 'Tanggal Lahir', value: '09-11-1995'},
      {title: 'Jenis Kelamin', value: 'Helikopter'},
      {title: 'Pendidikan Terakhir', value: 'S3'},
      {title: 'Kode Area No Telp  Rumah', value: '021'},
      {title: 'No Telp Rumah', value: '8217824'},
      {title: 'No HP 1', value: '085747829578'},
      {title: 'No HP 2', value: ''},
      {title: 'Status Pernikahan', value: 'Single'},
      {title: 'Nama Pasangan', value: ''},
      {title: 'No Telp Pasangan', value: ''},
      {title: 'Agama', value: 'Islam'},
      {title: 'Nama Gadis Ibu Kandung', value: ''},
      {title: 'Email', value: 'itdao13@baf.id'},
    ],
  },
  {
    title: 'Informasi Alamat',
    content: [
      {title: 'Alamat Sesuai KTP', value: 'Jln. Pelipus 187'},
      {title: 'RT', value: '002'},
      {title: 'RW', value: '003'},
      {title: 'Provinsi', value: 'Jawa Barat'},
      {title: 'Kota Kabupaten', value: 'Kota Bekasi'},
      {title: 'Kecamatan', value: 'Rawalumbu'},
      {title: 'Kelurahan/Desa', value: 'Sepanjang Jaya'},
      {title: 'Kode Pos', value: '17114'},
      {title: 'Status Rumah', value: 'Permanent'},
      {title: 'Masa Berlaku Sewa/Kontrak/Kost', value: ''},
      {title: 'Lama Tinggal (Tahun)', value: '5'},
      {title: 'Lama Tinggal (Bulan)', value: '5'},
    ],
  },
  {
    title: 'Informasi Domisili',
    content: [
      {title: 'Alamat Sesuai KTP', value: 'Jln. Pelipus 187'},
      {title: 'RT Domisili', value: '002'},
      {title: 'RW Domisili', value: '003'},
      {title: 'Provinsi Domisili', value: 'Jawa Barat'},
      {title: 'Kota/Kabupaten Domisili', value: 'Kota Bekasi'},
      {title: 'Kecamatan Domisili', value: 'Rawalumbu'},
      {title: 'Kelurahan/Desa Domisili', value: 'Sepanjang Jaya'},
      {title: 'Kode Pos Domisili', value: '17114'},
      {title: 'Status Rumah Domisili', value: 'Permanent'},
      {title: 'Masa Berlaku Sewa/Kontrak/Kost Domisili', value: ''},
      {title: 'Lama Tinggal Domisili (Tahun)', value: '5'},
      {title: 'Lama Tinggal Domisili (Bulan)', value: '5'},
    ],
  },
  {
    title: 'Informasi Pekerjaan',
    content: [
      {title: 'Kategori Pekerjaan', value: 'Swasta'},
      {title: 'Status Pekerjaan', value: 'Kontrak'},
      {title: 'Tipe Pekerjaan', value: 'Outsourcing'},
      {title: 'Jabatan', value: 'Staff'},
      {title: 'Bidang Usaha', value: ''},
      {title: 'Nama Perusahaan', value: 'BAF'},
      {title: 'Bekerja Sejak (Bulan)', value: '11'},
      {title: 'Bekerja Sejak (Tahun)', value: '2020'},
      {title: 'Periode Masa Kontrak (Bulan)', value: '11'},
      {title: 'Periode Masa Kontrak (Tahun)', value: '1'},
      {title: 'Alamat Perusahaan', value: 'Jln. Swadaya 1'},
      {title: 'RT Perusahaan', value: '02'},
      {title: 'RW Perusahaan', value: '08'},
      {title: 'Provinsi Perusahaan', value: 'DKI Jakarta'},
      {title: 'Kota/Kabupaten Perusahaan', value: 'Kota Jakarta Selatan'},
      {title: 'Kecamatan Perusahaan', value: 'Jagakarsa'},
      {title: 'Kelurahan/Desa Perusahaan', value: 'Tanjung Barat'},
      {title: 'Kode Pos Perusahaan', value: '12530'},
      {title: 'Kode Area No Telp Perusahaan', value: '021'},
      {title: 'No Telp Perusahaan', value: '29396000'},
      {title: 'Ekstensi No Telp Perusahaan', value: ''},
      {title: 'Status Perusahaan', value: 'Swasta'},
      {title: 'Penghasilan per Bulan', value: 'Rp. 8.000.000'},
    ],
  },
  {
    title: 'Emergency Kontak',
    content: [
      {title: 'Nama Emergency Kontak 1', value: 'Zahrayinah Muldalifah'},
      {title: 'No HP Emergency Kontak 1', value: '08568295897'},
      {title: 'Hubungan Emergency Kontak 1', value: 'Tetangga'},
      {title: 'Alamat Emergency Kontak 1', value: 'Jln. Kreta 187'},
      {title: 'Nama Emergency Kontak 2', value: ''},
      {title: 'No HP Emergency Kontak 2', value: ''},
      {title: 'Hubungan Emergency Kontak 2', value: ''},
      {title: 'Alamat Emergency Kontak 2', value: ''},
    ],
  },
  {
    title: 'Data Barang',
    content: [
      {title: 'Kode Toko', value: '023'},
      {title: 'Nama Toko', value: 'Toko Bersinar Cahaya'},
      {title: 'Kode Cabang', value: '011'},
      {title: 'Nama Cabang', value: 'Jakarta Pusat'},
      {title: 'Jenis Barang', value: 'Barang Eletronic'},
      {title: 'Merk Barang', value: 'Sony'},
      {title: 'Ukuran', value: 'Kotak'},
      {title: 'Warna', value: 'Putih'},
      {title: 'Tipe', value: 'Console'},
      {title: 'Peruntukan Unit', value: 'Enterteinment'},
      {title: 'Harga Barang', value: 'Rp. 8.200.000'},
      {title: 'Tenor', value: '10'},
      {title: 'Uang Muka', value: '50%'},
      {title: 'Nominal Uang Muka', value: 'Rp. 4.100.000'},
      {title: 'Tipe Biaya Admin', value: 'Cicil'},
      {title: 'Tipe Pembayaran', value: 'Kredit'},
      {title: 'Nama Program', value: 'MSCC C Coba'},
      {title: 'Biaya Admin', value: '200.000'},
      {title: 'Jumlah Pembiayaan', value: 'Rp. 9.000.000'},
      {title: 'Angsuran', value: 'Rp. 400.000'},
      {title: 'Biaya Asuransi', value: ''},
      {title: 'Pembayaran Pertama', value: 'Rp, 500.000'},
      {title: 'Sumber Dana', value: 'Gaji'},
    ],
  },
];
