import {StyleSheet} from 'react-native';
import {FSAlbert} from '../../../utils/fonts';
import {fonts} from '../../../utils/fonts';
import {Platform, Dimensions, PixelRatio} from 'react-native';
import {BAF_COLOR_BLUE} from '../../../utils/constant';

export default StyleSheet.create({
  ButtonDisabled: {
    backgroundColor: 'rgba(52, 52, 52, 0.3)',
    width: '100%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
    borderRadius: 5,
  },
  borderLine: {
    borderBottomWidth: 2,
    borderBottomColor: 'grey',
    paddingBottom: 10,
    marginTop: 20,
  },
  CLContainer: {
    width: 155,
    height: 80,
    marginBottom: 10,
  },
  CLImage: {
    width: 150,
    height: 80,
    marginTop: 10,
    borderRadius: 5,
    backgroundColor: 'white',
  },
  CLDeleteImage: {
    backgroundColor: 'red',
    width: 20,
    height: 20,
    position: 'absolute',
    right: 0,
    top: 5,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  CLDeleteImageIcon: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  CLSubmitButtonTitle: {
    color: 'white',
    fontSize: 16,
  },
  ScrollViewBottom: {
    width: '100%',
    height: 150,
  },
  SDContainerStyle: {
    width: '100%',
  },
  SDInputTextStyle: {
    height: 42,
    padding: 12,
    borderWidth: 1,
    borderColor: '#aaa',
    borderRadius: 5,
  },
  SDItemStyle: {
    padding: 12,
    height: 42,
    marginTop: 2,
    borderColor: '#bbb',
    borderWidth: 1,
    borderRadius: 5,
  },
  SDItemTextStyle: {},
  SDItemsContainerStyle: {
    maxHeight: 135,
  },
  ErrorDesc: {
    color: 'red',
    fontFamily: FSAlbert,
    alignSelf: 'stretch',
    textAlign: 'right',
    fontSize: 10,
  },
  ErrorDescDropdown: {
    color: 'red',
    alignSelf: 'stretch',
    textAlign: 'right',
    right: 13,
    fontSize: 10,
  },
  formItem: {
    left: -5,
    width: '100%',
  },
  formInputDisabled: {
    color: '#bbb',
  },
  phoneValidationButton: {
    backgroundColor: '#347AB7',
    width: '100%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  verificationValidationButton: {
    backgroundColor: '#5CB95D',
    width: '100%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  submitButton: {
    backgroundColor: '#347AB7',
    width: '90%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  nextButton: {
    backgroundColor: '#347AB7',
    width: '47%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  prevButton: {
    backgroundColor: '#EEAF30',
    width: '47%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    marginRight: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  stepIndicatorContainer: {
    height: 92,
    backgroundColor: 'white',
    borderBottomWidth: 2,
    borderBottomColor: '#ddd',
  },
  buttonBottomContainer: {
    position: 'absolute',
    width: '100%',
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    backgroundColor: '#F2F2F2',
    borderTopWidth: 2,
    borderColor: '#ddd',
    height: 80,
  },
  buttonSideBottomContainer: {
    position: 'absolute',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    bottom: 0,
    backgroundColor: '#F2F2F2',
    borderTopWidth: 2,
    borderColor: '#ddd',
    height: 80,
    paddingHorizontal: '5%',
  },
  PickerContainer: {
    marginTop: 2,
    backgroundColor: 'white',
    borderColor: '#aaa',
    borderWidth: 1,
    borderRadius: 5,
  },
  DropdownDisabled: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#bbb',
    width: '100%',
    height: 42,
    paddingLeft: 15,
    justifyContent: 'center',
    marginTop: 15,
    borderRadius: 5,
  },
  borderStyleBase: {
    width: 30,
    height: 45,
  },
  borderStyleHighLighted: {
    borderColor: '#03DAC6',
  },
  underlineStyleBase: {
    width: 50,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
    color: 'black',
    backgroundColor: '#f9f9f9',
    fontSize: 20,
  },
  underlineStyleHighLighted: {
    borderColor: 'black',
  },
  image: {
    width: '100%',
    height: 240,
    alignSelf: 'center',
    resizeMode: 'contain',
  },

  centerWrapper: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  headerTextVerification: {
    color: 'grey',
    fontSize: 22,

    fontFamily: fonts.primary.normal,
  },
  textHeader: {
    color: '#1c1e61',
    fontSize: 20,
    alignSelf: 'center',
    paddingBottom: 10,
  },
  textDesc: {
    color: '#1c1e61',
  },
  rowText: {
    alignSelf: 'center',
    alignContent: 'center',
    textAlign: 'center',
  },
  rowText2: {
    alignSelf: 'center',
  },
  containerOptionalLogin: {
    backgroundColor: '#fff',
    paddingVertical: '3%',
    paddingHorizontal: '25%',
  },
  authLogo: {
    width: 20,
    height: 20,
    marginRight: '10%',
  },
  containerTextBtn: {
    alignItems: 'center',
    left: -10,
    width: '80%',
  },
  btnOtp: {
    backgroundColor: '#FFF',
    flexDirection: 'row',
    width: '100%',
    height: 40,
    marginVertical: 10,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  firstWizardContent: {
    height: '75%',
    width: '100%',
    alignSelf: 'center',
    paddingHorizontal: '5%',
    flex: 1,
  },
  firstWizardToast: {
    marginTop:
      Platform.OS != 'android'
        ? Dimensions.get('window').width * PixelRatio.get() > 750
          ? 44
          : 20
        : 0,
    backgroundColor: '#FFE6E9',
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'red',
    justifyContent: 'center',
    paddingLeft: 50,
    height: 50,
    borderRadius: 0,
  },
  FWLastDataContainer: {
    flexDirection: 'row',
    borderBottomWidth: 0,
    left: -15,
    marginTop: 10,
    marginBottom: -10,
  },
  FWLastDataConfirmButton: {
    fontFamily: FSAlbert,
    fontSize: 16,
    color: '#555',
    marginLeft: '2%',
  },
  FWLastDataCheckBox: {
    borderColor: 'grey',
    borderRadius: 2,
  },
  FWNextButton: {
    fontFamily: FSAlbert,
    color: 'white',
    fontSize: 16,
  },
  SWContainer: {
    height: '75%',
    backgroundColor: 'white',
    width: '100%',
    alignSelf: 'center',
    paddingHorizontal: '5%',
  },
  TWContainer: {
    height: '75%',
    backgroundColor: 'white',
    width: '100%',
    alignSelf: 'center',
    paddingHorizontal: '4%',
  },
  TWListItem: {
    flexDirection: 'row',
    borderBottomWidth: 0,
    left: -15,
    marginTop: 10,
    marginBottom: -15,
  },
  TWCheckBox: {
    borderColor: 'grey',
    borderRadius: 5,
  },
  TWDomisiliText: {
    fontSize: 16,
    color: '#555',
    marginLeft: '2%',
  },
  fourthWizardContainer: {
    height: '75%',
    backgroundColor: 'white',
    width: '100%',
    alignSelf: 'center',
    paddingHorizontal: '4%',
  },
  fifthWizardContainer: {
    height: '75%',
    backgroundColor: 'white',
    width: '100%',
    alignSelf: 'center',
    paddingHorizontal: '4%',
  },

  ///Summary Fde
  title: {
    fontFamily: fonts.primary.bold,
    fontSize: 14,
  },

  row: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    marginLeft: 38,
    marginRight: 29,
  },

  renderHeader: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#F4F4F4',
  },
  renderHeader2: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
  },

  textDefault: {
    fontFamily: fonts.primary.normal,
    color: 'rgba(0, 0, 0, 0.8)',
    textAlign: 'justify',
  },

  btnSubmitAndroidLarge: {
    alignSelf: 'center',
    width: '75%',
    height: 35,
    borderRadius: 5,
    backgroundColor: '#347AB6',
    justifyContent: 'center',
    // marginBottom: '5%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
    marginBottom: '5%',
  },
  btnSubmitAndroidSmall: {
    alignSelf: 'center',
    width: '75%',
    height: 35,
    borderRadius: 5,
    backgroundColor: '#347AB6',
    justifyContent: 'center',
    // marginBottom: '5%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
    marginBottom: '5%',
  },
  btnSubmitIosLarge: {
    alignSelf: 'center',
    width: '75%',
    height: 35,
    borderRadius: 5,
    backgroundColor: '#347AB6',
    justifyContent: 'center',
    // marginBottom: '5%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
    marginBottom: '10%',
  },
  btnSubmitIosSmall: {
    alignSelf: 'center',
    width: '75%',
    height: 35,
    borderRadius: 5,
    backgroundColor: '#347AB6',
    justifyContent: 'center',
    // marginBottom: '5%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
    marginBottom: '5%',
  },
  textButton: {
    textAlign: 'center',
    fontSize: 14,
    color: 'white',
    fontFamily: fonts.primary.bold,
  },

  renderHeaderLarge: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#FFFF',
  },
  renderHeaderSmall: {
    flex: 1,
    flexDirection: 'row',
    padding: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#FFFF',
  },

  renderContentLarge: {
    flex: 1,
    height: 200,
  },
  renderContentSmall: {
    flex: 1,
    height: 100,
  },
  headTextAndroidLarge: {
    width: 300,
    marginTop: '7%',
    marginBottom: '2%',
    alignSelf: 'center',
    fontSize: 14,
  },
  headTextAndroidSmall: {
    marginTop: 30,
    marginBottom: 20,
    alignSelf: 'center',
  },
  headTextIosLarge: {
    width: 300,
    marginTop: '7%',
    marginBottom: '5%',
    alignSelf: 'center',
    fontSize: 14,
  },
  headTextIosSmall: {
    marginTop: 30,
    marginBottom: 20,
    alignSelf: 'center',
  },

  accordionAndroidLarge: {
    borderTopWidth: 0,
    borderBottomWidth: 0,
    marginLeft: '12%',
    marginRight: '12%',
  },

  accordionAndroidSmall: {
    borderTopWidth: 0,
    borderBottomWidth: 0,
    marginLeft: '12%',
  },

  accordionIosLarge: {
    borderTopWidth: 0,
    borderBottomWidth: 0,
    marginLeft: 50,
    marginRight: 50,
  },

  accordionIosSmall: {
    borderTopWidth: 0,
    borderBottomWidth: 0,
    marginLeft: '12%',
  },

  titleNumber: {
    color: 'black',
    fontSize: 16,
    fontFamily: fonts.primary.bold,
  },

  numberingAndroid: {
    width: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: BAF_COLOR_BLUE,
    borderRadius: 10,
    marginRight: '5%',
  },

  numberingIos: {
    width: 16,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: BAF_COLOR_BLUE,
    borderRadius: 10,
    marginRight: '5%',
  },

  imageIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  iconExpandLarge: {
    fontSize: 22,
    color: '#6FD454',
  },
  iconExpandSmall: {
    marginRight: '12%',
    fontSize: 18,
    color: '#6FD454',
  },
  titleHeadAndroidLarge: {
    fontSize: 16,
    fontFamily: fonts.primary.normal,
    color: 'grey',
  },
  titleHeadIosLarge: {
    fontSize: 16,
    fontFamily: fonts.primary.normal,
    color: 'grey',
  },
  titleHeadIosSmall: {
    fontSize: 15,
    fontFamily: fonts.primary.normal,
    color: 'grey',
  },
  titleHead2AndroidLarge: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: 'grey',
  },
  titleHead2AndroidSmall: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: 'grey',
  },
  titleHead2IosLarge: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: 'grey',
  },
  titleHead2IosSmall: {
    fontSize: 15,
    fontFamily: fonts.primary.bold,
    color: 'grey',
  },
  numberingHeaderLarge: {
    fontSize: 12,
    color: 'white',
  },
  numberingHeaderSmall: {
    fontSize: 12,
    color: 'white',
  },

  listItem: {
    flexDirection: 'column',
    alignSelf: 'center',
    alignItems: 'stretch',
    borderBottomWidth: 0,
    marginLeft: '5%',
    marginRight: 5,
    // backgroundColor: "gray",
  },

  listItemSubLarge: {
    borderBottomWidth: 0,
    marginTop: '-7%',
    lineHeight: 20,
  },

  listItemSubSmall: {
    borderBottomWidth: 0,
    marginTop: '-1%',
    paddingTop: -10,
  },

  listItemSubLarge2: {
    borderBottomWidth: 0,
    marginTop: '-5%',
    marginBottom: '-2%',
    lineHeight: 15,
  },

  listItemSubSmall2: {
    borderBottomWidth: 0,
    marginTop: '-7%',
    marginBottom: '-2%',
    paddingBottom: -10,
  },

  checkBoxBottom: {
    borderColor: '#666',
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
   
  },

  titleCheckBoxAndroidLarge: {
    fontFamily: fonts.primary.normal,
    fontSize: 12,
    marginLeft: 20,
    marginRight: 8,
  },
  titleCheckBoxAndroidSmall: {
    fontFamily: fonts.primary.normal,
    textAlign: 'justify',
    fontSize: 10,
    marginLeft: 20,
    marginRight: 8,
  },
  titleCheckBoxIosLarge: {
    fontFamily: fonts.primary.normal,
    textAlign: 'justify',
    fontSize: 12,
    marginLeft: 7,
    marginRight: 8,
  },
  titleCheckBoxIosSmall: {
    fontFamily: fonts.primary.normal,
    textAlign: 'justify',
    fontSize: 10,
    marginLeft: 7,
    marginRight: 8,
  },

  insideContent: {
    flexDirection: 'row',
    height: 25,
    marginLeft: '5%',
  },

  titleContent: {
    fontSize: 12,
    fontFamily: fonts.primary.normal,
    color: '#A6AAB4',
  },

  titleValue: {
    fontSize: 12,
    fontFamily: fonts.primary.normal,
    color: BAF_COLOR_BLUE,
  },
});
