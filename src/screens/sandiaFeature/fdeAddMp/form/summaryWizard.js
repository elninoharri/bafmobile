import React, {Component} from 'react';
import SubHeaderAndroid from '../../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../../components/ios/subHeaderBlue';
import {Container, Icon, Content, Accordion, ListItem} from 'native-base';
import Information from '../../information';
import {
  TouchableOpacity,
  Text,
  View,
  Platform,
  Dimensions,
  PixelRatio,
  ScrollView,
  Image,
  Linking,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import Toast from 'react-native-easy-toast';
import styles from '../style';
import {
  BAF_COLOR_BLUE,
  Urlandroid_Info,
  Urlios_Info,
} from '../../../../utils/constant';
import {reduxForm} from 'redux-form';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import ILedit from '../../../../../assets/img/Icon/edit.png';
import Spinner from 'react-native-loading-spinner-overlay';
import {
  substringCircum,
  substringSecondCircum,
  substringThirdCircum,
} from '../../../../utils/utilization';
import CustomAlertComponent from '../../../../components/multiPlatform/customAlert/CustomAlertComponent';
//fix: ini untuk mocking, kodingan Fadhil jangan dihapus, dicomment aja.
// import {dataArray} from '../mocksummary';

class SummaryWizard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataArray: [
        {
          title: '',
          content: [{title: '', value: ''}],
        },
      ],
      checked: false,
      secondCheck: false,

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
    };
  }

  componentDidMount = () => {
    const {formFdeMpAdd} = this.props;

    const dataArray = [
      {
        title: 'Data Pemohon',
        content: [
          {title: 'Nomor Aplikasi', value: formFdeMpAdd.values.nomorAplikasi},
          {
            title: 'Sumber Aplikasi',
            value: formFdeMpAdd.values.dataSumberAplikasi,
          },
          {title: 'Nomor Order', value: formFdeMpAdd.values.nomorOrder},
          {title: 'Nama Konsumen', value: formFdeMpAdd.values.namaKonsumen},
          {title: 'Nama Panggilan', value: formFdeMpAdd.values.namaPanggilan},
          {title: 'NIK KTP', value: formFdeMpAdd.values.NIK},
          {
            title: 'Masa berlaku KTP',
            value: formFdeMpAdd.values.masaBerlakuKtp,
          },
          {title: 'Tempat Lahir', value: formFdeMpAdd.values.tempatLahir},
          {title: 'Tanggal Lahir', value: formFdeMpAdd.values.tanggalLahir},
          {
            title: 'Jenis Kelamin',
            value: substringSecondCircum(formFdeMpAdd.values.jenisKelamin),
          },
          {
            title: 'Pendidikan Terakhir',
            value: substringSecondCircum(
              formFdeMpAdd.values.pendidikanTerakhir,
            ),
          },
          {title: 'No HP 1', value: formFdeMpAdd.values.noHandphone1},
          {title: 'No HP 2', value: formFdeMpAdd.values.noHandphone2},
          {
            title: 'Status Pernikahan',
            value: substringSecondCircum(formFdeMpAdd.values.statusPernikahan),
          },
          {title: 'Nama Pasangan', value: formFdeMpAdd.values.namaPasangan},
          {
            title: 'No Telp Pasangan',
            value: formFdeMpAdd.values.noHandphonePasangan,
          },
          {
            title: 'Agama',
            value: substringSecondCircum(formFdeMpAdd.values.agama),
          },
          {
            title: 'Nama Gadis Ibu Kandung',
            value: formFdeMpAdd.values.namaIbuKandung,
          },
          {title: 'Email', value: formFdeMpAdd.values.email},
        ],
      },
      {
        title: 'Informasi Alamat',
        content: [
          {title: 'Alamat Sesuai KTP', value: formFdeMpAdd.values.ktpAlamat},
          {title: 'RT', value: formFdeMpAdd.values.ktpRt},
          {title: 'RW', value: formFdeMpAdd.values.ktpRw},
          {
            title: 'Provinsi',
            value: substringSecondCircum(formFdeMpAdd.values.ktpProvinsi),
          },
          {
            title: 'Kota Kabupaten',
            value: substringSecondCircum(formFdeMpAdd.values.ktpKotaKabupaten),
          },
          {
            title: 'Kecamatan',
            value: substringSecondCircum(formFdeMpAdd.values.ktpKecamatan),
          },
          {
            title: 'Kelurahan/Desa',
            value: substringSecondCircum(formFdeMpAdd.values.ktpKelurahan),
          },
          {title: 'Kode Pos', value: formFdeMpAdd.values.ktpKodePos},
          {
            title: 'Status Rumah',
            value: substringSecondCircum(formFdeMpAdd.values.ktpStatusRumah),
          },
          {
            title: 'Masa Berlaku Sewa/Kontrak/Kost',
            value: formFdeMpAdd.values.ktpMasaBerlakuSewa,
          },
          {
            title: 'Lama Tinggal (Tahun)',
            value: formFdeMpAdd.values.ktpLamaTinggalTahun,
          },
          {
            title: 'Lama Tinggal (Bulan)',
            value: formFdeMpAdd.values.ktpLamaTinggalBulan,
          },
        ],
      },
      {
        title: 'Informasi Domisili',
        content: [
          {title: 'Alamat Sesuai KTP', value: formFdeMpAdd.values.checkAddress},
          {title: 'RT Domisili', value: formFdeMpAdd.values.residenceRt},
          {title: 'RW Domisili', value: formFdeMpAdd.values.residenceRw},
          {
            title: 'Provinsi Domisili',
            value: substringSecondCircum(formFdeMpAdd.values.residenceProvinsi),
          },
          {
            title: 'Kota/Kabupaten Domisili',
            value: substringSecondCircum(
              formFdeMpAdd.values.residenceKotaKabupaten,
            ),
          },
          {
            title: 'Kecamatan Domisili',
            value: substringSecondCircum(
              formFdeMpAdd.values.residenceKecamatan,
            ),
          },
          {
            title: 'Kelurahan/Desa Domisili',
            value: substringSecondCircum(
              formFdeMpAdd.values.residenceKelurahan,
            ),
          },
          {
            title: 'Kode Pos Domisili',
            value: formFdeMpAdd.values.residenceKodePos,
          },
          {
            title: 'Status Rumah Domisili',
            value: substringSecondCircum(
              formFdeMpAdd.values.residenceStatusRumah,
            ),
          },
          {
            title: 'Masa Berlaku Sewa/Kontrak/Kost Domisili',
            value: formFdeMpAdd.values.residenceMasaBerlakuSewa,
          },
          {
            title: 'Lama Tinggal Domisili (Tahun)',
            value: formFdeMpAdd.values.residenceLamaTinggalTahun,
          },
          {
            title: 'Lama Tinggal Domisili (Bulan)',
            value: formFdeMpAdd.values.residenceLamaTinggalBulan,
          },
        ],
      },
      {
        title: 'Informasi Pekerjaan',
        content: [
          {
            title: 'Kategori Pekerjaan',
            value:
              formFdeMpAdd.values.jobCategory == null ||
              formFdeMpAdd.values.jobCategory === ''
                ? ' - '
                : substringSecondCircum(formFdeMpAdd.values.jobCategory),
          },
          {
            title: 'Status Pekerjaan',
            value:
              formFdeMpAdd.values.jobStatus == null ||
              formFdeMpAdd.values.jobStatus === ''
                ? ' - '
                : substringSecondCircum(formFdeMpAdd.values.jobStatus),
          },
          {
            title: 'Tipe Pekerjaan',
            value:
              formFdeMpAdd.values.jobType == null ||
              formFdeMpAdd.values.jobType === ''
                ? ' - '
                : substringSecondCircum(formFdeMpAdd.values.jobType),
          },

          {
            title: 'Jabatan',
            value:
              formFdeMpAdd.values.jobPosition == null ||
              formFdeMpAdd.values.jobPosition === ''
                ? ' - '
                : substringSecondCircum(formFdeMpAdd.values.jobPosition),
          },
          {},
          {},
          {
            title: 'Bidang Usaha',
            value:
              formFdeMpAdd.values.industryType == null ||
              formFdeMpAdd.values.industryType === ''
                ? ' - '
                : substringSecondCircum(formFdeMpAdd.values.industryType),
          },
          {},
          {title: 'Nama Perusahaan', value: formFdeMpAdd.values.namaPerusahaan},
          {
            title: 'Bekerja Sejak (Bulan)',
            value:
              formFdeMpAdd.values.lamaUsahaBulan == null ||
              formFdeMpAdd.values.lamaUsahaBulan === ''
                ? ' - '
                : substringSecondCircum(formFdeMpAdd.values.lamaUsahaBulan),
          },
          {
            title: 'Bekerja Sejak (Tahun)',
            value:
              formFdeMpAdd.values.lamaUsahaTahun == null ||
              formFdeMpAdd.values.lamaUsahaTahun === ''
                ? ' - '
                : substringSecondCircum(formFdeMpAdd.values.lamaUsahaTahun),
          },
          {
            title: 'Periode Masa Kontrak (Bulan)',
            value:
              formFdeMpAdd.values.kontrakBulan == null
                ? ''
                : substringSecondCircum(formFdeMpAdd.values.kontrakBulan),
          },
          {
            title: 'Periode Masa Kontrak (Tahun)',
            value:
              formFdeMpAdd.values.kontrakTahun == null
                ? ''
                : substringSecondCircum(formFdeMpAdd.values.kontrakTahun),
          },
          {title: 'Alamat Perusahaan', value: formFdeMpAdd.values.jobAlamat},
          {title: 'RT Perusahaan', value: formFdeMpAdd.values.jobRt},
          {title: 'RW Perusahaan', value: formFdeMpAdd.values.jobRw},
          {
            title: 'Provinsi Perusahaan',
            value: substringSecondCircum(formFdeMpAdd.values.jobProvinsi),
          },
          {
            title: 'Kota/Kabupaten Perusahaan',
            value: substringSecondCircum(formFdeMpAdd.values.jobKotaKabupaten),
          },
          {
            title: 'Kecamatan Perusahaan',
            value: substringSecondCircum(formFdeMpAdd.values.jobKecamatan),
          },
          {
            title: 'Kelurahan/Desa Perusahaan',
            value: substringSecondCircum(formFdeMpAdd.values.jobKelurahan),
          },
          {
            title: 'Kode Pos Perusahaan',
            value: substringThirdCircum(formFdeMpAdd.values.jobKelurahan),
          },
          {
            title: 'Kode Area No Telp Perusahaan',
            value: formFdeMpAdd.values.jobKodeArea,
          },
          {title: 'No Telp Perusahaan', value: formFdeMpAdd.values.jobTelp},
          {
            title: 'Ekstensi No Telp Perusahaan',
            value: formFdeMpAdd.values.jobTelpEkstensi,
          },
          {
            title: 'Status Perusahaan',
            value:
              formFdeMpAdd.values.statusUsaha == null ||
              formFdeMpAdd.values.statusUsaha === ''
                ? ' - '
                : substringSecondCircum(formFdeMpAdd.values.statusUsaha),
          },
          {
            title: 'Penghasilan per Bulan',
            value: formFdeMpAdd.values.penghasilanPerbulan,
          },
        ],
      },
      {
        title: 'Emergency Kontak',
        content: [
          {
            title: 'Nama Emergency Kontak 1',
            value: formFdeMpAdd.values.namaEmergencyKontak1,
          },
          {
            title: 'No HP Emergency Kontak 1',
            value: formFdeMpAdd.values.noHpEmergencyKontak1,
          },
          {
            title: 'Hubungan Emergency Kontak 1',
            value: substringSecondCircum(
              formFdeMpAdd.values.hubunganEmergencyKontak1,
            ),
          },
          {
            title: 'Alamat Emergency Kontak 1',
            value: formFdeMpAdd.values.alamatEmergencyKontak1,
          },
          {
            title: 'Nama Emergency Kontak 2',
            value: formFdeMpAdd.values.namaEmergencyKontak2,
          },
          {
            title: 'No HP Emergency Kontak 2',
            value: formFdeMpAdd.values.noHpEmergencyKontak2,
          },
          {
            title: 'Hubungan Emergency Kontak 2',
            value:
              formFdeMpAdd.values.hubunganEmergencyKontak2 == null
                ? ''
                : substringSecondCircum(
                    formFdeMpAdd.values.hubunganEmergencyKontak2,
                  ),
          },
          {
            title: 'Alamat Emergency Kontak 2',
            value: formFdeMpAdd.values.alamatEmergencyKontak2,
          },
        ],
      },
      {
        title: 'Data Barang',
        content: [
          {title: 'Kode Toko', value: formFdeMpAdd.values.kodeToko},
          {
            title: 'Nama Toko',
            value: formFdeMpAdd.values.namaToko,
          },
          {title: 'Kode Cabang', value: formFdeMpAdd.values.kodeCabang},
          {
            title: 'Nama Cabang',
            value: formFdeMpAdd.values.namaCabang,
          },
          {
            title: 'Jenis Barang',
            value: formFdeMpAdd.values.jenisDisabled,
          },
          {
            title: 'Merk Barang',
            value: formFdeMpAdd.values.merkDisabled,
          },
          {title: 'Ukuran', value: formFdeMpAdd.values.ukuran},
          {title: 'Warna', value: formFdeMpAdd.values.warna},
          {title: 'Tipe', value: formFdeMpAdd.values.tipe},
          {
            title: 'Peruntukan Unit',
            value: formFdeMpAdd.values.financingPurposeView,
          },
          {title: 'Harga Barang', value: formFdeMpAdd.values.hargaBarang},
          {title: 'Tenor', value: formFdeMpAdd.values.jangkaWaktu},
          {title: 'Uang Muka', value: formFdeMpAdd.values.uangMuka},
          {
            title: 'Nominal Uang Muka',
            value: formFdeMpAdd.values.uangMukaAmount,
          },
          {
            title: 'Tipe Biaya Admin',
            value: formFdeMpAdd.values.tipeBiayaAdmin,
          },
          {title: 'Tipe Pembayaran', value: formFdeMpAdd.values.tipePembayaran},
          {
            title: 'Nama Program',
            value: substringSecondCircum(formFdeMpAdd.values.schemeId),
          },
          {title: 'Biaya Admin', value: formFdeMpAdd.values.biayaAdmin},
          {
            title: 'Jumlah Pembiayaan',
            value: formFdeMpAdd.values.jumlahPembiayaan,
          },
          {title: 'Angsuran', value: formFdeMpAdd.values.angsuran},
          {title: 'Biaya Asuransi', value: formFdeMpAdd.values.biayaAsuransi},
          {
            title: 'Pembayaran Pertama',
            value: formFdeMpAdd.values.pembayaranPertama,
          },
          {
            title: 'Sumber Dana',
            value: substringSecondCircum(formFdeMpAdd.values.sourceFunds),
          },
        ],
      },
    ];

    this.setState({dataArray: dataArray});
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {ddeInsert_Error, ddeInsert_Result} = this.props;
    if (
      ddeInsert_Error !== null &&
      prevProps.ddeInsert_Error !== ddeInsert_Error
    ) {
      this.setState({
        alertFor: 'ddeInsert_Error',
        alertShowed: true,
        alertMessage: ddeInsert_Error.message,
        alertTitle: 'Informasi',
        alertType: 'info',
        alertDoneText: 'OK',
      });
    }

    if (
      ddeInsert_Result !== null &&
      prevProps.ddeInsert_Result !== ddeInsert_Result
    ) {
      this.setState({
        alertFor: 'ddeInsert_Result',
        alertShowed: true,
        alertMessage: 'Data Lengkap Kamu sedang diproses',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
      });
    }
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
    if (this.state.alertFor === 'ddeInsert_Result') {
      this.props.goOrderList();
    } else if (this.state.alertFor === 'ddeInsert_Error') {
      this.props.previousPage();
    }
    this.setState({alertFor: null});
  };

  goBack = async () => {
    this.props.previousPage();
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  toastComponent = () => {
    return (
      <Toast
        ref="toast"
        style={{
          marginTop:
            Platform.OS != 'android'
              ? Dimensions.get('window').width * PixelRatio.get() > 750
                ? 44
                : 20
              : 0,
          backgroundColor: '#FFE6E9',
          width: '100%',
          borderBottomWidth: 2,
          borderBottomColor: 'red',
          justifyContent: 'center',
          paddingLeft: 50,
          height: 50,
          borderRadius: 0,
        }}
        position="top"
        positionValue={0}
        fadeInDuration={2000}
        fadeOutDuration={1000}
        opacity={0.9}
      />
    );
  };

  actionCheck = () => {
    if (this.state.checked === false) {
      this.setState({checked: true});
    } else {
      this.setState({checked: false});
    }
  };

  actionCheckSecond = () => {
    if (this.state.secondCheck === false) {
      this.setState({secondCheck: true});
    } else {
      this.setState({secondCheck: false});
    }
  };

  handleClick = (data) => {
    this.props.navigatePage(data);
  };

  alertChecked = () => {
    this.setState({
      alertFor: 'AlertChecked',
      alertShowed: true,
      alertMessage: 'Mohon untuk menyetujui syarat dan ketentuan yang berlaku',
      alertTitle: 'Information',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  _renderHeader = (item, expanded) => {
    return (
      <View
        style={
          Platform.OS == 'android'
            ? Dimensions.get('window').width > 360
              ? styles.renderHeaderLarge
              : styles.renderHeaderSmall
            : Dimensions.get('window').width * PixelRatio.get() > 750
            ? styles.renderHeaderLarge
            : styles.renderHeaderSmall
        }>
        {/* Title and numbering  */}
        <View style={{flexDirection: 'row'}}>
          <View
            style={
              Platform.OS == 'android'
                ? styles.numberingAndroid
                : styles.numberingIos
            }>
            <Text
              style={
                Platform.OS == 'android'
                  ? Dimensions.get('window').width > 360
                    ? styles.numberingHeaderLarge
                    : styles.numberingHeaderSmall
                  : Dimensions.get('window').width * PixelRatio.get() > 750
                  ? styles.numberingHeaderLarge
                  : styles.numberingHeaderSmall
              }>
              {this.state.dataArray.findIndex((e) => e.title === item.title) +
                1}
            </Text>
          </View>
          <Text style={styles.titleNumber}>{item.title}</Text>
          {/* Add icon tambahan */}
          {item.title === 'Data Pemohon' ? (
            <View></View>
          ) : (
            <>
              {expanded ? (
                <View style={{marginLeft: 10}}>
                  <TouchableOpacity
                    onPress={() => this.handleClick(item.title)}>
                    <Image style={styles.imageIcon} source={ILedit} />
                  </TouchableOpacity>
                </View>
              ) : (
                <View></View>
              )}
            </>
          )}
        </View>

        {expanded ? (
          <Icon
            style={
              Platform.OS == 'android'
                ? Dimensions.get('window').width > 360
                  ? styles.iconExpandLarge
                  : styles.iconExpandSmall
                : Dimensions.get('window').width * PixelRatio.get() > 750
                ? styles.iconExpandLarge
                : styles.iconExpandSmall
            }
            name="ios-checkmark-circle"
          />
        ) : (
          <Icon
            style={
              Platform.OS == 'android'
                ? Dimensions.get('window').width > 360
                  ? styles.iconExpandLarge
                  : styles.iconExpandSmall
                : Dimensions.get('window').width * PixelRatio.get() > 750
                ? styles.iconExpandLarge
                : styles.iconExpandSmall
            }
            name="ios-checkmark-circle"
          />
        )}
      </View>
    );
  };

  _renderContent = (dataArray) => {
    return (
      <View
        style={
          Platform.OS == 'android'
            ? Dimensions.get('window').width > 360
              ? styles.renderContentLarge
              : styles.renderContentSmall
            : Dimensions.get('window').width * PixelRatio.get() > 750
            ? styles.renderContentLarge
            : styles.renderContentSmall
        }>
        <ScrollView>
          <View
            style={{
              flexDirection: 'column',
              height: 650,
              marginVertical: '2%',
            }}>
            {dataArray.content.map((e) => {
              return (
                <View style={styles.insideContent}>
                  <View style={{width: '40%'}}>
                    <Text style={styles.titleContent}>{e.title}</Text>
                  </View>
                  <View style={{width: '50%'}}>
                    <Text style={styles.titleValue}>{e.value}</Text>
                  </View>
                </View>
              );
            })}
          </View>
        </ScrollView>
      </View>
    );
  };

  _summaryRender = (
    ddeInsert_Loading,
    handleSubmit,
    submitting,
    dataArray,
    checked,
    secondCheck,
  ) => {
    return (
      <>
        <Content scrollEnabled={false}>
          <View>
            <Spinner
              visible={ddeInsert_Loading ? true : false}
              textContent={'Loading...'}
              textStyle={{color: '#FFF'}}
            />
            <View
              style={
                Platform.OS == 'android'
                  ? Dimensions.get('window').width > 360
                    ? styles.headTextAndroidLarge
                    : styles.headTextAndroidSmall
                  : Dimensions.get('window').width * PixelRatio.get() > 750
                  ? styles.headTextIosLarge
                  : styles.headTextIosSmall
              }>
              <Text
                style={
                  Platform.OS == 'android'
                    ? Dimensions.get('window').width > 360
                      ? styles.titleHeadAndroidLarge
                      : styles.titleHeadAndroidLarge
                    : Dimensions.get('window').width * PixelRatio.get() > 750
                    ? styles.titleHeadIosLarge
                    : styles.titleHeadIosSmall
                }>
                Anda telah melengkapi semua data,
              </Text>
              <Text
                style={
                  Platform.OS == 'android'
                    ? Dimensions.get('window').width > 360
                      ? styles.titleHead2AndroidLarge
                      : styles.titleHead2AndroidSmall
                    : Dimensions.get('window').width * PixelRatio.get() > 750
                    ? styles.titleHead2IosLarge
                    : styles.titleHead2IosSmall
                }>
                Silahkan melakukan pengecekan kembali
              </Text>
            </View>

            <Accordion
              renderContent={this._renderContent}
              renderHeader={this._renderHeader}
              dataArray={dataArray}
              style={
                Platform.OS == 'android'
                  ? Dimensions.get('window').width > 360
                    ? styles.accordionAndroidLarge
                    : styles.accordionAndroidSmall
                  : Dimensions.get('window').width * PixelRatio.get() > 750
                  ? styles.accordionIosLarge
                  : styles.accordionIosSmall
              }
              expanded={0}
            />
          </View>
        </Content>
        <ListItem style={styles.listItem}>
          <ListItem
            //untuk ios nya belum styling list item nya
            style={
              Platform.OS == 'android'
                ? Dimensions.get('window').width > 360
                  ? styles.listItemSubLarge
                  : styles.listItemSubSmall
                : Dimensions.get('window').width * PixelRatio.get() > 750
                ? styles.listItemSubLarge
                : styles.listItemSubLarge
            }>
            <CheckBox
              value={this.state.checked}
              style={styles.checkBoxBottom}
              onValueChange={() => this.actionCheck()}
            />
            <Text
              style={
                Platform.OS == 'android'
                  ? Dimensions.get('window').width > 360
                    ? styles.titleCheckBoxAndroidLarge
                    : styles.titleCheckBoxAndroidSmall
                  : Dimensions.get('window').width * PixelRatio.get() > 750
                  ? styles.titleCheckBoxIosLarge
                  : styles.titleCheckBoxIosSmall
              }>
              Dengan ini saya menyatakan bahwa data yang saya{'\n'}input adalah
              data yang sebenarnya.
            </Text>
          </ListItem>

          <ListItem
            style={
              Platform.OS == 'android'
                ? Dimensions.get('window').width > 360
                  ? styles.listItemSubLarge2
                  : styles.listItemSubSmall2
                : Dimensions.get('window').width * PixelRatio.get() > 750
                ? styles.listItemSubLarge2
                : styles.listItemSubLarge2
            }>
            <CheckBox
              value={this.state.secondCheck}
              style={styles.checkBoxBottom}
              onValueChange={() => this.actionCheckSecond()}
            />

            <Text
              style={
                Platform.OS == 'android'
                  ? Dimensions.get('window').width > 360
                    ? styles.titleCheckBoxAndroidLarge
                    : styles.titleCheckBoxAndroidSmall
                  : Dimensions.get('window').width * PixelRatio.get() > 750
                  ? styles.titleCheckBoxIosLarge
                  : styles.titleCheckBoxIosSmall
              }>
              Saya telah membaca dan menyetujui{' '}
              {
                <Text
                  style={{
                    color: 'blue',
                    fontSize: Dimensions.get('window').width > 360 ? 12 : 10,
                  }}
                  onPress={() => this.props.screenTitleSwitch()}>
                  Informasi{'\n'}Penting untuk Konsumen Multiproduk
                </Text>
              }
            </Text>
          </ListItem>
        </ListItem>
        <TouchableOpacity
          style={
            Platform.OS == 'android'
              ? Dimensions.get('window').width > 360
                ? styles.btnSubmitAndroidLarge
                : styles.btnSubmitAndroidSmall
              : Dimensions.get('window').width * PixelRatio.get() > 750
              ? styles.btnSubmitIosLarge
              : styles.btnSubmitIosSmall
          }
          onPress={
            checked && secondCheck == true ? handleSubmit : this.alertChecked
          }
          disabled={submitting}>
          <Text style={styles.textButton}>Submit</Text>
        </TouchableOpacity>
      </>
    );
  };

  render() {
    const {submitting, handleSubmit, ddeInsert_Loading} = this.props;
    const {dataArray, checked, secondCheck} = this.state;
    return (
      <Container>
        {this.showAlert()}
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.props.screenTitle}
            goBack={
              this.props.screenTitle == 'Informasi Penting'
                ? this.props.screenTitleSwitch
                : this.goBack
            }
          />
        ) : (
          <SubHeaderIos title={this.props.screenTitle} goBack={this.goBack} />
        )}
        {this.props.screenTitle === 'Summary' ? (
          this._summaryRender(
            ddeInsert_Loading,
            handleSubmit,
            submitting,
            dataArray,
            checked,
            secondCheck,
          )
        ) : (
          <Information />
        )}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    formFdeMpAdd: state.form.formFdeMpAdd,
    ddeInsert_Loading: state.ddeInsert.loading,
    ddeInsert_Result: state.ddeInsert.result,
    ddeInsert_Error: state.ddeInsert.error,
  };
}

function matchDispatchToProps() {
  return bindActionCreators({});
}

SummaryWizard = reduxForm({
  form: 'formFdeMpAdd',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true,
  enableReinitialize: true,
})(SummaryWizard);

export default connect(mapStateToProps, matchDispatchToProps)(SummaryWizard);
