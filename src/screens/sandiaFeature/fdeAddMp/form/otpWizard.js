/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-did-update-set-state */
/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  View,
  Text,
  Platform,
  Image,
  TouchableOpacity,
  Alert,
  Keyboard,
  StyleSheet,
  Button,
  BackHandler,
  NativeModules,
  Dimensions,
  PixelRatio,
} from 'react-native';
import {
  Container,
  Icon,
  Form,
  Label,
  Input,
  Item,
  ListItem,
  CheckBox,
  Right,
  Left,
  Body,
  Row,
  DatePicker,
  Content,
} from 'native-base';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Field, reduxForm, change} from 'redux-form';
import styles from '../style';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import SubHeaderAndroid from '../../../../components/android/subHeader';
import SubHeaderIos from '../../../../components/ios/subHeader';
import Toast, {DURATION} from 'react-native-easy-toast';
import {otpSms, otpValidate} from '../../../../actions';
import CustomAlertComponent from '../../../../components/multiPlatform/customAlert/CustomAlertComponent';
import {FormFdeAddMpValidate} from '../../../../validates/FormFdeAddMpValidate';
import Spinner from 'react-native-loading-spinner-overlay';
import {jsonParse} from '../../../../utils/utilization';

var CredolabModul = NativeModules.CredolabModul;

class OtpWizard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      timer: 60,
      message: '',
      code: '',
      detailUser: false,
      phoneMask: '',
      isConnected: false,

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',

      otpChance: 5,
    };
  }

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  componentDidMount = async () => {
    const {otpSms, handleAlert} = this.props;
    this.tick();

    const jsonDetailUser = await AsyncStorage.getItem('detailUser');
    if (jsonDetailUser) {
      var detailUser = JSON.parse(jsonDetailUser);
      var phoneMask = this.mask(detailUser.Phoneno);

      this.setState({
        detailUser: detailUser,
        phoneMask: phoneMask,
      });
    }

    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        if (this.props.otpPrivyHandle) {
          this.props.otpPrivyHandle('request');
        } else {
          otpSms({
            // Phoneno: '08984093007',
            Phoneno: this.state.detailUser.Phoneno,
          });
        }
      } else {
        this.showToastNoInternet();
      }
    });
  };

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      otpValidateResult,
      otpValidateError,
      otpValidatePrivy_error,

      handleSubmit,

      authKeyCredolab,
      ddeInsert_Error,
      ddeInsert_Result,
      qdeDetailData_Result,
    } = this.props;

    if (this.state.timer === 0) {
      clearInterval(this.interval);
      this.setState({
        timer: null,
        show: true,
        message: 'Waktu anda sudah habis',
        error: false,
      });
    }

    if (
      otpValidateResult !== null &&
      prevProps.otpValidateResult !== otpValidateResult
    ) {
      handleSubmit();
      console.log('ini result otp');
    }

    if (
      (otpValidateError !== null &&
        prevProps.otpValidateError !== otpValidateError) ||
      (otpValidatePrivy_error !== null &&
        prevProps.otpValidatePrivy_error !== otpValidatePrivy_error)
    ) {
      //this should not be executed when error is token expired
      const data = await jsonParse(otpValidatePrivy_error.message);
      if (!data) {
        clearInterval(this.interval);
        await this.setState((prevState) => ({
          otpChance: prevState.otpChance - 1,
        }));
        this.setState({
          show: true,
          message:
            'Kode OTP yang anda masukkan salah ! ' +
            this.state.otpChance +
            'x kesempatan',
          error: true,
        });
        if (this.state.otpChance < 1) {
          this.setState({
            alertFor: 'OtpExpired',
            alertShowed: true,
            alertMessage: 'Telah gagal 5x anda akan dialihkan ke halaman utama',
            alertTitle: 'Gagal !',
            alertType: 'error',
            alertDoneText: 'Close',
          });
        }
      }
    }

    if (
      ddeInsert_Error !== null &&
      prevProps.ddeInsert_Error !== ddeInsert_Error
    ) {
      this.setState({
        alertFor: 'ddeInsert_Error',
        alertShowed: true,
        alertMessage: ddeInsert_Error.message,
        alertTitle: 'Informasi',
        alertType: 'info',
        alertDoneText: 'OK',
      });
    }

    if (
      ddeInsert_Result !== null &&
      prevProps.ddeInsert_Result !== ddeInsert_Result
    ) {
      if (authKeyCredolab && authKeyCredolab !== null) {
        try {
          var test = await CredolabModul.setCollectCredolab(
            // `Testing dari eka`,
            qdeDetailData_Result.IdNo + '' + qdeDetailData_Result.APP_NO,
            authKeyCredolab.Auth,
            authKeyCredolab.Url,
          );
          console.log(test); //for testing if credolab success or not
        } catch (e) {
          console.log(e);
        }
      }

      if (otpValidateResult !== null && ddeInsert_Result.status === '5') {
        console.log('validate berhasil');
        this.setState({
          alertFor: 'ddeInsert_Result',
          alertShowed: true,
          alertMessage:
            ddeInsert_Result.message +
            '. No Aplikasi : ' +
            ddeInsert_Result.appno_los,
          alertTitle: 'Berhasil',
          alertType: 'success',
          alertDoneText: 'OK',
        });
      } else if (
        otpValidateResult !== null &&
        ddeInsert_Result.status === '4'
      ) {
        this.setState({
          alertFor: 'ddeInsert_Result',
          alertShowed: true,
          alertMessage: 'Data berhasil dikirim.',
          alertTitle: 'Informasi',
          alertType: 'success',
          alertDoneText: 'OK',
        });
      }
    }
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
    if (this.state.alertFor === 'OtpExpired') {
      this.props.goHome();
    } else if (this.state.alertFor === 'ddeInsert_Result') {
      this.props.goOrderList();
    } else if (this.state.alertFor === 'ddeInsert_Error') {
      this.props.prevScreen();
    }
    this.setState({alertFor: null});
  };

  tick = () => {
    this.interval = setInterval(
      () => this.setState((prevState) => ({timer: prevState.timer - 1})),
      1000,
    );
    this;
  };

  otpSms = () => {
    const {otpSms} = this.props;
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        this.setState({
          show: false,
          timer: 60,
          error: false,
          code: '',
          otpType: 'sms',
        });
        this.tick(); //this gonna trigger time interval
        if (this.props.otpPrivyHandle) {
          if (this.props.otpRequestPrivy_result) {
            this.props.otpPrivyHandle('regenerate');
          } else {
            this.props.otpPrivyHandle('request');
          }
        } else {
          otpSms({
            // Phoneno: '08984093007',
            Phoneno: this.state.detailUser.Phoneno,
          });
        }
      } else {
        this.showToastNoInternet();
      }
    });
  };

  handlerCodeFill = (data) => {
    const {otpValidate} = this.props;
    //fix: handle submit gak boleh ditaruh di handlerCodeFill, kayaknya bekas test lupa dilepas

    if (this.props.otpPrivyHandle) {
      this.props.otpPrivyHandle('validate', data);
    } else {
      otpValidate({
        // Phoneno: '08984093007',
        Phoneno: this.state.detailUser.Phoneno,
        OTP: data,
      });
    }
  };

  mask = (val) => {
    const result =
      val.substr(0, 3) + '*******' + val.substr(val.length - 2, val.length - 1);
    return result;
  };

  render() {
    const {submitting, ddeInsert_Loading, otpPrivyHandle, noHp} = this.props;
    const {show, phoneMask} = this.state;

    return (
      <Container style={{flex: 1}}>
        {this.showAlert()}
        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />
        <Spinner
          visible={ddeInsert_Loading ? true : false}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />

        <Content disableKBDismissScroll={false}>
          <Image
            source={require('../../../../../assets/img/otp.png')}
            style={styles.image}
          />

          <Text style={styles.textHeader}>Masukkan Kode OTP</Text>

          <Row style={styles.rowText}>
            <Text style={styles.textDesc}>
              {otpPrivyHandle
                ? 'Masukkan 5 digit kode yang di kirimkan ke'
                : 'Masukkan 4 digit kode yang di kirimkan ke'}
            </Text>
          </Row>

          <Row style={styles.rowText}>
            <Text style={styles.textDesc}>
              Nomor Anda {noHp ? this.mask(noHp) : phoneMask}
            </Text>
          </Row>

          <Form>
            <OTPInputView
              style={{
                width: '70%',
                height: 100,
                alignSelf: 'center',
              }}
              pinCount={this.props.otpPrivyHandle ? 5 : 4}
              code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
              onCodeChanged={(code) => {
                this.setState({code});
              }}
              autoFocusOnLoad
              codeInputFieldStyle={{
                width: 50,
                height: 45,
                borderWidth: 0,
                borderBottomWidth: 1,
                color: this.state.error ? 'red' : 'black',
                backgroundColor: '#f9f9f9',
                fontSize: 20,
              }}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              onCodeFilled={this.handlerCodeFill}
            />
          </Form>

          {show ? (
            <View>
              <Row style={styles.rowText}>
                <Text style={styles.textDesc}>{this.state.message}</Text>
              </Row>

              <Row style={styles.rowText}>
                <Text style={styles.textDesc}>
                  Silahkan kirim OTP melalui :
                </Text>
              </Row>

              <View style={styles.containerOptionalLogin}>
                <TouchableOpacity
                  style={styles.btnOtp}
                  onPress={() => this.otpSms()}>
                  <Image
                    source={require('../../../../../assets/img/otp-sms.jpg')}
                    style={styles.authLogo}
                  />
                  <View style={styles.containerTextBtn}>
                    <Text
                      style={{
                        color: '#0047CC',
                        fontSize: 16,
                        fontWeight: 'bold',
                      }}>
                      SMS
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            <View>
              <Row style={styles.rowText}>
                <Text style={styles.textDesc}>
                  Silahkan tunggu selama{' '}
                  {<Text style={styles.textDesc}>{this.state.timer}</Text>}
                </Text>
              </Row>

              <Row style={styles.rowText}>
                <Text style={styles.textDesc}>Untuk mengirim ulang kode</Text>
              </Row>
              <View style={{height: 50}} />
            </View>
          )}
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    otpSmsLoading: state.otpSms.loading,
    otpSmsResult: state.otpSms.result,
    otpSmsError: state.otpSms.error,
    otpValidateLoading: state.otpValidate.loading,
    otpValidateResult: state.otpValidate.result,
    otpValidateError: state.otpValidate.error,
    ddeInsert_Result: state.ddeInsert.result,
    ddeInsert_Error: state.ddeInsert.error,
    ddeInsert_Loading: state.ddeInsert.loading,
    qdeDetailData_Result: state.qdeDetailData.result,
    otpValidatePrivy_loading: state.otpValidatePrivy.loading,
    otpValidatePrivy_result: state.otpValidatePrivy.result,
    otpValidatePrivy_error: state.otpValidatePrivy.error,
    otpRequestPrivy_result: state.otpRequestPrivy.result,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      otpSms,
      otpValidate,
    },
    dispatch,
  );
}

OtpWizard = reduxForm({
  form: 'formFdeMpAdd',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true,
  enableReinitialize: true,
  validate: FormFdeAddMpValidate,
})(OtpWizard);

export default connect(mapStateToProps, matchDispatchToProps)(OtpWizard);
