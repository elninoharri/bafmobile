import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { Field, reduxForm, change } from 'redux-form'
import { renderField, renderFieldPicker, renderFieldTextArea } from '../index'
import { Text, View, TouchableOpacity } from 'react-native';
import { Container, Content, Form, Picker } from 'native-base';
import styles from '../style';
import { HideWithKeyboard } from 'react-native-hide-with-keyboard';
import { FormFdeAddMpValidate } from "../../../../validates/FormFdeAddMpValidate"
import { FSAlbert } from '../../../../utils/fonts';

class FifthWizard extends Component {
  constructor(props) {
    super(props)
    this.changeRelationship1 = this.changeRelationship1.bind(this)
    this.changeRelationship2 = this.changeRelationship2.bind(this)
    this.state = {
      pickerRelationshipItem1: [{ Code: '', Descr: 'Silahkan Pilih' }],
      pickerRelationshipSelected1: '',

      pickerRelationshipItem2: [{ Code: '', Descr: 'Silahkan Pilih' }],
      pickerRelationshipSelected2: '',
    }
  }

  componentDidMount = async () => {
    const { formFdeMpAdd, ddeRelationship_Result } = this.props

    if (ddeRelationship_Result.length > 0) {
      var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];

      for (var i=0; i<ddeRelationship_Result.length; i++) {
        arrayData.push({
          Code: ddeRelationship_Result[i].Code + '^' + ddeRelationship_Result[i].Descr,
          Descr: ddeRelationship_Result[i].Descr
        })
      }
      this.setState({ 
        pickerRelationshipItem1: arrayData,
        pickerRelationshipItem2: arrayData
      });
    }

    if (formFdeMpAdd.values) {
      this.setState({ pickerRelationshipSelected1: formFdeMpAdd.values.hubunganEmergencyKontak1 ? formFdeMpAdd.values.hubunganEmergencyKontak1 : this.state.pickerRelationshipSelected1 })
      this.setState({ pickerRelationshipSelected2: formFdeMpAdd.values.hubunganEmergencyKontak2 ? formFdeMpAdd.values.hubunganEmergencyKontak2 : this.state.pickerRelationshipSelected2 })
    }
  }

  changeRelationship1(value){
    this.setState({ pickerRelationshipSelected1: value })
    this.props.updateField('formFdeMpAdd', 'hubunganEmergencyKontak1', value)
  }

  changeRelationship2(value){
    this.setState({ pickerRelationshipSelected2: value })
    this.props.updateField('formFdeMpAdd', 'hubunganEmergencyKontak2', value)
  }

  render(){
    const { handleSubmit, previousPage } = this.props
    const { 
      pickerRelationshipItem1, 
      pickerRelationshipSelected1, 
      pickerRelationshipItem2, 
      pickerRelationshipSelected2 
    } = this.state;
    return (
      <Container>
        <Content disableKBDismissScroll={true} style={styles.fifthWizardContainer}>
          <Form>

          <View style={{marginTop:20,borderBottomWidth:2,borderBottomColor:'grey',paddingBottom:10}}>
            <Text style={{color:'grey',fontSize:18,fontWeight:'bold',fontFamily:FSAlbert}}>Emergency Kontak 1</Text>
          </View>
            <Field
              name="namaEmergencyKontak1"
              type="text"
              component={renderField}
              label="Nama Emergency Kontak 1"
            />
            <Field
              name="noHpEmergencyKontak1"
              type="text"
              component={renderField}
              label="No HP Emergency Kontak 1"
              keyboardType="number-pad"
              maxLength={15}
            />
            <Field
              name="hubunganEmergencyKontak1"
              component={renderFieldPicker}
              label="Hubungan Emergency Kontak 1"
              pickerSelected = {pickerRelationshipSelected1}
              onValueChange = {(itemValue, itemIndex) => this.changeRelationship1(itemValue)}
              data = {pickerRelationshipItem1.map((pickerRelationshipItem1, index) => <Picker.Item label={pickerRelationshipItem1.Descr} value={pickerRelationshipItem1.Code} />)}
            /> 
            <Field
              name="alamatEmergencyKontak1"
              type="text"
              component={renderFieldTextArea}
              label="Alamat Emergency Kontak 1"
              rowSpan={4}
            />  

            <View style={{marginTop:40,borderBottomWidth:2,borderBottomColor:'grey',paddingBottom:10}}>
              <Text style={{color:'grey',fontSize:18,fontWeight:'bold',fontFamily:FSAlbert}}>Emergency Kontak 2</Text>
            </View>
          
            <Field
              name="namaEmergencyKontak2"
              type="text"
              component={renderField}
              label="Nama Emergency Kontak 2"
            />
            <Field
              name="noHpEmergencyKontak2"
              type="text"
              component={renderField}
              label="No HP Emergency Kontak 2"
              keyboardType="number-pad"
              maxLength={15}
            />
            <Field
              name="hubunganEmergencyKontak2"
              component={renderFieldPicker}
              label="Hubungan Emergency Kontak 2"
              pickerSelected = {pickerRelationshipSelected2}
              onValueChange = {(itemValue, itemIndex) => this.changeRelationship2(itemValue)}
              data = {pickerRelationshipItem2.map((pickerRelationshipItem2, index) => <Picker.Item label={pickerRelationshipItem2.Descr} value={pickerRelationshipItem2.Code} />)}
            />
            <Field
              name="alamatEmergencyKontak2"
              type="text"
              component={renderFieldTextArea}
              label="Alamat Emergency Kontak 2"
              rowSpan={4}
            />     
            <View style={styles.ScrollViewBottom}/>
          </Form>
        </Content>
            <HideWithKeyboard style={styles.buttonSideBottomContainer}>
              <TouchableOpacity style={styles.prevButton} onPress={previousPage}>
                <Text style={{color:'white',fontSize:16,fontFamily:FSAlbert}}>Kembali</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.nextButton} onPress={handleSubmit}>
                <Text style={{color:'white',fontSize:16,fontFamily:FSAlbert}}>Lanjut</Text>
              </TouchableOpacity>
            </HideWithKeyboard>
      </Container>
    )
  }
}

function mapStateToProps(state) {
  return {
    ddeRelationship_Loading: state.ddeRelationship.loading,
    ddeRelationship_Result: state.ddeRelationship.result,
    ddeRelationship_Error: state.ddeRelationship.error,
    formFdeMpAdd: state.form.formFdeMpAdd
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ 
    updateField: (form, field, newValue) => dispatch(change(form, field, newValue))
  }, dispatch);
}

FifthWizard =  reduxForm({
  form: 'formFdeMpAdd',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true,
  enableReinitialize: true,
  validate: FormFdeAddMpValidate
})(FifthWizard)

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(FifthWizard);