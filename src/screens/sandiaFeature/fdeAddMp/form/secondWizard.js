import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { Field, reduxForm, change } from 'redux-form'
import { renderField,renderFieldDatepickerIos, renderFieldPicker, renderFieldDatepicker, renderFieldDisabled, renderFieldTextArea } from '../index'
import { Text, View, TouchableOpacity } from 'react-native';
import { Container,Content,Form,Picker } from 'native-base';
import styles from '../style';
import { HideWithKeyboard } from 'react-native-hide-with-keyboard';
import { FormFdeAddMpValidate} from "../../../../validates/FormFdeAddMpValidate"
import { ddeLegalCity, ddeLegalKecamatan, ddeLegalKelurahan } from "../../../../actions/sandiaFeature/dataLengkap/dataLengkapWilayah";
import { substringCircum,substringThirdCircum } from "../../../../utils/utilization";
import AsyncStorage from '@react-native-community/async-storage';
import DisabledForm from "../../../../components/multiPlatform/sandiaFeature/disabledForm";
import { FSAlbert } from '../../../../utils/fonts';

class SecondWizard extends Component {

  constructor(props) {
    super(props)
    this.changeStatusRumah = this.changeStatusRumah.bind(this)
    this.changeProvinsi = this.changeProvinsi.bind(this)
    this.changeKotaKabupaten = this.changeKotaKabupaten.bind(this)
    this.changeKecamatan = this.changeKecamatan.bind(this)
    this.changeKelurahan = this.changeKelurahan.bind(this)
    this.changeMasaBerlakuSewa = this.changeMasaBerlakuSewa.bind(this)
    this.state = {
      userAccess: '',
      userToken: '',
      pickerKtpProvinsiItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
      pickerKtpProvinsiSelected: '',
      pickerKtpKotaKabupatenItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
      pickerKtpKotaKabupatenSelected: '',
      pickerKtpKecamatanItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
      pickerKtpKecamatanSelected: '',
      pickerKtpKelurahanItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
      pickerKtpKelurahanSelected: '',
      pickerKtpStatusRumahItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
      pickerKtpStatusRumahSelected: '',   
      masaBerlakuSewa: '',
      disableKotaKabupaten: true,
      disableKecamatan: true,
      disableKelurahan: true
    }
  }

  componentDidMount = async () => {
    const { 
      formFdeMpAdd, 
      ddeLegalCity, 
      ddeLegalKecamatan, 
      ddeLegalKelurahan, 
      ddeLegalProvince_Result, 
      ddeHouseOwnership_Result 
    } = this.props;

    const jsonUserAccess = await AsyncStorage.getItem('userAccess');
    var userAccess = JSON.parse(jsonUserAccess)
    this.setState({ userAccess: userAccess })

    const userToken = await AsyncStorage.getItem('userToken');
    this.setState({ userToken: userToken })

    if (ddeLegalProvince_Result.length > 0) {
      var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
      for (var i=0; i<ddeLegalProvince_Result.length; i++) {
        arrayData.push({
          Code: ddeLegalProvince_Result[i].Code + '^' + ddeLegalProvince_Result[i].Descr,
          Descr: ddeLegalProvince_Result[i].Descr
        })
      }
      this.setState({ pickerKtpProvinsiItem: arrayData });
    }
    if (ddeHouseOwnership_Result.length > 0) {
      var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
      for (var i=0; i<ddeHouseOwnership_Result.length; i++) {
        arrayData.push({
          Code: ddeHouseOwnership_Result[i].Code + '^' + ddeHouseOwnership_Result[i].Descr,
          Descr: ddeHouseOwnership_Result[i].Descr
        })
      }
      this.setState({ pickerKtpStatusRumahItem: arrayData });
    }
    if (formFdeMpAdd.values) {
      this.setState({ 
        pickerKtpStatusRumahSelected: formFdeMpAdd.values.ktpStatusRumah ? formFdeMpAdd.values.ktpStatusRumah : this.state.pickerKtpStatusRumahSelected,
        masaBerlakuSewa: formFdeMpAdd.values.ktpMasaBerlakuSewa ? formFdeMpAdd.values.ktpMasaBerlakuSewa : this.state.masaBerlakuSewa,
        pickerKtpKelurahanSelected: formFdeMpAdd.values.ktpKelurahan ? formFdeMpAdd.values.ktpKelurahan : this.state.pickerKtpKelurahanSelected
      })  
    }
    if (formFdeMpAdd.values && formFdeMpAdd.values.ktpProvinsi) {
      this.setState({ pickerKtpProvinsiSelected: formFdeMpAdd.values.ktpProvinsi })
      console.log(this.state.pickerKtpProvinsiSelected);
      if (formFdeMpAdd.values.ktpProvinsi !== '') {
        ddeLegalCity({ProvinceId: substringCircum(formFdeMpAdd.values.ktpProvinsi)}, userToken)
      }
    }
    if (formFdeMpAdd.values && formFdeMpAdd.values.ktpKotaKabupaten) {
      this.setState({ pickerKtpKotaKabupatenSelected: formFdeMpAdd.values.ktpKotaKabupaten })
      if (formFdeMpAdd.values.ktpKotaKabupaten !== '') {
        ddeLegalKecamatan({CityId: substringCircum(formFdeMpAdd.values.ktpKotaKabupaten)}, userToken)
      }
    }
    if (formFdeMpAdd.values && formFdeMpAdd.values.ktpKecamatan) {
      this.setState({ pickerKtpKecamatanSelected: formFdeMpAdd.values.ktpKecamatan })
      if (formFdeMpAdd.values.ktpKecamatan !== '') {
        ddeLegalKelurahan({KecamatanId: substringCircum(formFdeMpAdd.values.ktpKecamatan)}, userToken)
      }
    }
  }

  componentDidUpdate = async (prevProps) => {
    const { 
      ddeLegalCity_Result, 
      ddeLegalKecamatan_Result, 
      ddeLegalKelurahan_Result 
    } = this.props;

    if (ddeLegalCity_Result.length && prevProps.ddeLegalCity_Result !== ddeLegalCity_Result) {
      var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
      for (var i=0; i<ddeLegalCity_Result.length; i++) {
        arrayData.push({
          Code: ddeLegalCity_Result[i].Code + '^' + ddeLegalCity_Result[i].Descr,
          Descr: ddeLegalCity_Result[i].Descr
        })
      }
      this.setState({ 
        pickerKtpKotaKabupatenItem: arrayData,
        disableKotaKabupaten: false
      });
    }
    if (ddeLegalKecamatan_Result.length && prevProps.ddeLegalKecamatan_Result !== ddeLegalKecamatan_Result) {
      var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
      for (var i=0; i<ddeLegalKecamatan_Result.length; i++) {
        arrayData.push({
          Code: ddeLegalKecamatan_Result[i].Code + '^' + ddeLegalKecamatan_Result[i].Descr,
          Descr: ddeLegalKecamatan_Result[i].Descr
        })
      }
      this.setState({ 
        pickerKtpKecamatanItem: arrayData,
        disableKecamatan: false
      });
    }
    if (ddeLegalKelurahan_Result.length && prevProps.ddeLegalKelurahan_Result !== ddeLegalKelurahan_Result) {
      var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
      for (var i=0; i<ddeLegalKelurahan_Result.length; i++) {
        arrayData.push({
          Code: ddeLegalKelurahan_Result[i].Code + '^' + ddeLegalKelurahan_Result[i].Descr + '^' + ddeLegalKelurahan_Result[i].KodePos,
          Descr: ddeLegalKelurahan_Result[i].Descr
        })
      }
      this.setState({ 
        pickerKtpKelurahanItem: arrayData,
        disableKelurahan: false
      });
    }
  }

  changeProvinsi(value){
    const { ddeLegalCity } = this.props
    this.setState({
      pickerKtpProvinsiSelected: value,
      pickerKtpKotaKabupatenSelected: '',
      pickerKtpKecamatanSelected: '',
      pickerKtpKelurahanSelected: '',
      disableKotaKabupaten: true,
      disableKecamatan: true,
      disableKelurahan: true
    })
    this.props.updateField('formFdeMpAdd', 'ktpKotaKabupaten', '')
    this.props.updateField('formFdeMpAdd', 'ktpKecamatan', '')
    this.props.updateField('formFdeMpAdd', 'ktpKelurahan', '')
    this.props.updateField('formFdeMpAdd', 'ktpKodePos', '')
    if (value == '') {
      this.props.updateField('formFdeMpAdd', 'ktpProvinsi', '')
    } else {
      this.props.updateField('formFdeMpAdd', 'ktpProvinsi', value)
      ddeLegalCity({ProvinceId: substringCircum(value)})
    }
  }

  changeKotaKabupaten(value) {
    const { ddeLegalKecamatan } = this.props
    this.setState({
      pickerKtpKotaKabupatenSelected: value,
      pickerKtpKecamatanSelected: '',
      pickerKtpKelurahanSelected: '',
      disableKecamatan: true,
      disableKelurahan: true,
    })
    this.props.updateField('formFdeMpAdd', 'ktpKecamatan', '')
    this.props.updateField('formFdeMpAdd', 'ktpKelurahan', '')
    this.props.updateField('formFdeMpAdd', 'ktpKodePos', '')
    
    if(value == ''){
      this.props.updateField('formFdeMpAdd', 'ktpKotaKabupaten', '')
      this.setState({
        disableKecamatan: true,
        disableKelurahan: true
      })
    } else {
      this.props.updateField('formFdeMpAdd', 'ktpKotaKabupaten', value)
      ddeLegalKecamatan({CityId: substringCircum(value)})
    }
  }

  changeKecamatan(value) {
    const { ddeLegalKelurahan } = this.props
    this.setState({
      pickerKtpKecamatanSelected: value,
      pickerKtpKelurahanSelected: '',
      disableKelurahan: true,
    })
    this.props.updateField('formFdeMpAdd', 'ktpKelurahan', '')
    this.props.updateField('formFdeMpAdd', 'ktpKodePos', '')
    
    if(value == ''){
      this.props.updateField('formFdeMpAdd', 'ktpKecamatan', '')
      this.setState({
        disableKelurahan: true
      })
    } else {
      this.props.updateField('formFdeMpAdd', 'ktpKecamatan', value)
      ddeLegalKelurahan({KecamatanId: substringCircum(value)})
    }
  }

  changeKelurahan(value) {
    this.setState({ pickerKtpKelurahanSelected: value })

    if(value == ''){
      this.props.updateField('formFdeMpAdd', 'ktpKelurahan', '')
      this.props.updateField('formFdeMpAdd', 'ktpKodePos', '')
    } else {
      this.props.updateField('formFdeMpAdd', 'ktpKelurahan', value)
      this.props.updateField('formFdeMpAdd', 'ktpKodePos', substringThirdCircum(value))
    }
  }

  changeMasaBerlakuSewa(value){
    this.setState({ masaBerlakuSewa: value })
    this.props.updateField('formFdeMpAdd', 'ktpMasaBerlakuSewa', value)
  }

  changeStatusRumah(value){
    this.setState({ pickerKtpStatusRumahSelected: value })
    if (value == '') {
      this.props.updateField('formFdeMpAdd', 'ktpStatusRumah', '')
    } else {
      this.props.updateField('formFdeMpAdd', 'ktpStatusRumah', value)
    }
  }

  render(){
    const { 
      handleSubmit, 
      previousPage, 
      openDatePicker,
      toggleModal 
    } = this.props;
    const { 
      pickerKtpStatusRumahItem, 
      pickerKtpStatusRumahSelected, 
      pickerKtpProvinsiItem, 
      pickerKtpProvinsiSelected, 
      pickerKtpKotaKabupatenItem, 
      pickerKtpKotaKabupatenSelected, 
      pickerKtpKecamatanItem, 
      pickerKtpKecamatanSelected, 
      pickerKtpKelurahanItem, 
      pickerKtpKelurahanSelected, 
      disableKotaKabupaten, 
      disableKecamatan, 
      disableKelurahan } = this.state;

    return (
      <Container>
        <Content disableKBDismissScroll={true} style={styles.SWContainer}>
          <Form>
            <Field
              name="ktpAlamat"
              type="text"
              component={renderFieldTextArea}
              label="Alamat Sesuai KTP"
              rowSpan={4}
            />
            <Field
              name="ktpRt"
              type="text"
              component={renderField}
              label="RT"
              keyboardType="number-pad"
              maxLength={3}
            />
            <Field
              name="ktpRw"
              type="text"
              component={renderField}
              label="RW"
              keyboardType="number-pad"
              maxLength={3}
            />
            <View style={{marginTop:40}}/>
            <Field
              name="ktpProvinsi"
              component={renderFieldPicker}
              label="Provinsi"
              pickerSelected = {pickerKtpProvinsiSelected}
              onValueChange = {(itemValue) => this.changeProvinsi(itemValue)}
              data = {pickerKtpProvinsiItem.map((pickerKtpProvinsiItem) => <Picker.Item label={pickerKtpProvinsiItem.Descr} value={pickerKtpProvinsiItem.Code} />)}
            />
            { disableKotaKabupaten ? 
              <DisabledForm title = "Kota Kabupaten"/> :
              <Field
                name="ktpKotaKabupaten"
                component={renderFieldPicker}
                label="KotaKabupaten"
                pickerSelected = {pickerKtpKotaKabupatenSelected}
                onValueChange = {(itemValue) => this.changeKotaKabupaten(itemValue)}
                data = {pickerKtpKotaKabupatenItem.map((pickerKtpKotaKabupatenItem) => <Picker.Item label={pickerKtpKotaKabupatenItem.Descr} value={pickerKtpKotaKabupatenItem.Code} />)}
              />
            }
            { disableKecamatan ? 
              <DisabledForm title = "Kecamatan"/> :
              <Field
                name="ktpKecamatan"
                component={renderFieldPicker}
                label="Kecamatan"
                pickerSelected = {pickerKtpKecamatanSelected}
                onValueChange = {(itemValue) => this.changeKecamatan(itemValue)}
                data = {pickerKtpKecamatanItem.map((pickerKtpKecamatanItem) => <Picker.Item label={pickerKtpKecamatanItem.Descr} value={pickerKtpKecamatanItem.Code} />)}
              />
            }
            { disableKelurahan ? 
              <DisabledForm title = "Kelurahan"/> :
              <Field
                name="ktpKelurahan"
                component={renderFieldPicker}
                label="Kelurahan"
                pickerSelected = {pickerKtpKelurahanSelected}
                onValueChange = {(itemValue) => this.changeKelurahan(itemValue)}
                data = {pickerKtpKelurahanItem.map((pickerKtpKelurahanItem) => <Picker.Item label={pickerKtpKelurahanItem.Descr} value={pickerKtpKelurahanItem.Code} />)}
              />
            }
            <Field
              name="ktpKodePos"
              type="text"
              component={renderFieldDisabled}
              label="Kode Pos"
            />
            <View style={{marginTop:40}}/>
            <Field
              name="ktpStatusRumah"
              component={renderFieldPicker}
              label="Status Rumah"
              pickerSelected = {pickerKtpStatusRumahSelected}
              onValueChange = {(itemValue) => this.changeStatusRumah(itemValue)}
              data = {pickerKtpStatusRumahItem.map((pickerKtpStatusRumahItem) => <Picker.Item label={pickerKtpStatusRumahItem.Descr} value={pickerKtpStatusRumahItem.Code} />)}
            />
            <Field
              name="ktpMasaBerlakuSewa"
              component={Platform.OS == 'android' ? renderFieldDatepicker : renderFieldDatepickerIos} 
              placeholder="Masa Berlaku Sewa/Kontrak (bila ada)"
              formatdate="YYYY-MM-DD"
              date={this.state.masaBerlakuSewa}
              openModal={() => openDatePicker()}
              closeModal={() => toggleModal()}
              visible = {this.props.visible}
              onDateChange={(date) => {this.changeMasaBerlakuSewa(date)}}
            />
            <Field
              name="ktpLamaTinggalTahun"
              type="text"
              component={renderField}
              label="Lama Tinggal (Tahun)"
              keyboardType="number-pad"
              maxLength={3}
            />
            <Field
              name="ktpLamaTinggalBulan"
              type="text"
              component={renderField}
              label="Lama Tinggal (Bulan)"
              keyboardType="number-pad"
              maxLength={2}
            />
            <View style={styles.ScrollViewBottom} />
          </Form>
        </Content>
        <HideWithKeyboard style={styles.buttonSideBottomContainer}>
          <TouchableOpacity style={styles.prevButton} onPress={previousPage}>
            <Text style={{color:'white',fontSize:16,fontFamily:FSAlbert}}>Kembali</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.nextButton} onPress={handleSubmit}>
            <Text style={{color:'white',fontSize:16,fontFamily:FSAlbert}}>Lanjut</Text>
          </TouchableOpacity>
        </HideWithKeyboard>
      </Container>
    )
  }
} 

function mapStateToProps(state) {
  return {
    ddeHouseOwnership_Loading: state.ddeHouseOwnership.loading,
    ddeHouseOwnership_Result: state.ddeHouseOwnership.result,
    ddeHouseOwnership_Error: state.ddeHouseOwnership.error,
    ddeLegalProvince_Loading: state.ddeLegalProvince.loading,
    ddeLegalProvince_Result: state.ddeLegalProvince.result,
    ddeLegalProvince_Error: state.ddeLegalProvince.error,
    ddeLegalCity_Loading: state.ddeLegalCity.loading,
    ddeLegalCity_Result: state.ddeLegalCity.result,
    ddeLegalCity_Error: state.ddeLegalCity.error,
    ddeLegalKecamatan_Loading: state.ddeLegalKecamatan.loading,
    ddeLegalKecamatan_Result: state.ddeLegalKecamatan.result,
    ddeLegalKecamatan_Error: state.ddeLegalKecamatan.error,
    ddeLegalKelurahan_Loading: state.ddeLegalKelurahan.loading,
    ddeLegalKelurahan_Result: state.ddeLegalKelurahan.result,
    ddeLegalKelurahan_Error: state.ddeLegalKelurahan.error,
    formFdeMpAdd: state.form.formFdeMpAdd
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ 
    ddeLegalCity,
    ddeLegalKecamatan,
    ddeLegalKelurahan,
    updateField: (form, field, newValue) => dispatch(change(form, field, newValue))
  }, dispatch);
}

SecondWizard =  reduxForm({
  form: 'formFdeMpAdd',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true,
  enableReinitialize: true,
  validate: FormFdeAddMpValidate
})(SecondWizard)

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(SecondWizard);