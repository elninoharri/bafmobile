import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Field, reduxForm, change} from 'redux-form';
import ImgToBase64 from 'react-native-image-base64';
import ImageResizer from 'react-native-image-resizer';
import ImageView from 'react-native-image-view';
import {
  renderField,
  renderFieldHidden,
  renderFieldDisabled,
  renderFieldDatepicker,
  renderFieldPicker,
  renderFieldDatepickerIos,
} from '../index';
import {Text, View, TouchableOpacity, Image, Platform} from 'react-native';
import {
  Container,
  Content,
  Form,
  Picker,
  ListItem,
  CheckBox,
  Button,
} from 'native-base';
import styles from '../style';
import {HideWithKeyboard} from 'react-native-hide-with-keyboard';
import {FormFdeAddMpValidate} from '../../../../validates/FormFdeAddMpValidate';
import {otpCall, otpSms, otpValidate} from '../../../../actions';
import AsyncStorage from '@react-native-community/async-storage';
import Toast, {DURATION} from 'react-native-easy-toast';
import CustomAlertComponent from '../../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Spinner from 'react-native-loading-spinner-overlay';
import UploadKtpAndroid from '../../../camera/android/uploadKtp';
import UploadFotoKonsumenAndroid from '../../../camera/android/uploadFotoKonsumen';
import UploadKtpIos from '../../../camera/ios/uploadKtp';
import UploadFotoKonsumenIos from '../../../camera/ios/uploadFotoKonsumen';
import {fonts} from '../../../../utils/fonts';
import {trimLineBreak} from '../../../../utils/utilization';
import {BAF_COLOR_BLUE} from '../../../../utils/constant';

class FirstWizard extends Component {
  constructor(props) {
    super(props);
    this.changeGender = this.changeGender.bind(this);
    this.changeEducation = this.changeEducation.bind(this);
    this.changeMaritalStatus = this.changeMaritalStatus.bind(this);
    this.changeReligion = this.changeReligion.bind(this);
    this.changeMasaBerlakuKtp = this.changeMasaBerlakuKtp.bind(this);
    this.state = {
      pickerEducationItem: [{Code: '', Descr: 'Silahkan Pilih'}],
      pickerEducationSelected: '',
      pickerGenderItem: [{Code: '', Descr: 'Silahkan Pilih'}],
      pickerGenderSelected: '',
      pickerMaritalStatusItem: [{Code: '', Descr: 'Silahkan Pilih'}],
      pickerMaritalStatusSelected: '',
      pickerReligionItem: [{Code: '', Descr: 'Silahkan Pilih'}],
      pickerReligionSelected: '',
      masaBerlakuKtp: '',
      showDetailPasangan: false,
      detailUser: false,
      userToken: false,
      show: false,
      timer: 60,
      message: '',
      code: '',
      otpType: 'call',
      isConnected: false,
      disabledOtpButton: false,
      validate: false,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      isSelected: false,
      lastDataRequest: false,

      disableButtonUpload: false,
      uploadKtp: false,
      uploadDiri: false,
      uploadKtpView: false,
      uploadDiriView: false,
      isImageViewVisibleKtp: false,
      isImageViewVisibleDiri: false,

    };
  }

  componentDidMount = async () => {
    const {
      ddeEducation_Result,
      ddeReligion_Result,
      ddeGender_Result,
      ddeMaritalStatus_Result,
      formFdeMpAdd,
    } = this.props;

    const jsonDetailUser = await AsyncStorage.getItem('detailUser');
    if (jsonDetailUser) {
      var detailUser = JSON.parse(jsonDetailUser);
      this.setState({detailUser: detailUser});
    }

    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) {
      this.setState({userToken: userToken});
    }

    if (ddeGender_Result.length > 0) {
      let arrGender = [{Code: '', Descr: 'Silahkan Pilih'}];
      for (var i = 0; i < ddeGender_Result.length; i++) {
        arrGender.push({
          Code: ddeGender_Result[i].Code + '^' + ddeGender_Result[i].Descr,
          Descr: ddeGender_Result[i].Descr,
        });
      }
      this.setState({pickerGenderItem: arrGender});
    }

    if (ddeEducation_Result.length > 0) {
      var arrayData = [{Code: '', Descr: 'Silahkan Pilih'}];
      for (var i = 0; i < ddeEducation_Result.length; i++) {
        arrayData.push({
          Code:
            ddeEducation_Result[i].Code + '^' + ddeEducation_Result[i].Descr,
          Descr: ddeEducation_Result[i].Descr,
        });
      }
      this.setState({pickerEducationItem: arrayData});
    }

    if (ddeMaritalStatus_Result.length > 0) {
      var arrayData = [{Code: '', Descr: 'Silahkan Pilih'}];
      for (var i = 0; i < ddeMaritalStatus_Result.length; i++) {
        arrayData.push({
          Code:
            ddeMaritalStatus_Result[i].Code +
            '^' +
            ddeMaritalStatus_Result[i].Descr,
          Descr: ddeMaritalStatus_Result[i].Descr,
        });
      }
      this.setState({pickerMaritalStatusItem: arrayData});
    }

    if (ddeReligion_Result.length > 0) {
      var arrayData = [{Code: '', Descr: 'Silahkan Pilih'}];
      for (var i = 0; i < ddeReligion_Result.length; i++) {
        arrayData.push({
          Code: ddeReligion_Result[i].Code + '^' + ddeReligion_Result[i].Descr,
          Descr: ddeReligion_Result[i].Descr,
        });
      }
      this.setState({pickerReligionItem: arrayData});
    }

    if (formFdeMpAdd.values) {
      // console.log(formFdeMpAdd)
      this.setState({
        showDetailPasangan:
          formFdeMpAdd.values.statusPernikahan &&
          formFdeMpAdd.values.statusPernikahan.indexOf('MAR') !== -1
            ? true
            : false,
      });
      this.setState({
        masaBerlakuKtp: formFdeMpAdd.values.masaBerlakuKtp
          ? formFdeMpAdd.values.masaBerlakuKtp
          : this.state.masaBerlakuKtp,
      });
      this.setState({
        pickerGenderSelected: formFdeMpAdd.values.jenisKelamin
          ? formFdeMpAdd.values.jenisKelamin
          : this.state.pickerGenderSelected,
      });
      this.setState({
        pickerEducationSelected: formFdeMpAdd.values.pendidikanTerakhir
          ? formFdeMpAdd.values.pendidikanTerakhir
          : this.state.pickerEducationSelected,
      });
      this.setState({
        pickerMaritalStatusSelected: formFdeMpAdd.values.statusPernikahan
          ? formFdeMpAdd.values.statusPernikahan
          : this.state.pickerMaritalStatusSelected,
      });
      this.setState({
        pickerReligionSelected: formFdeMpAdd.values.agama
          ? formFdeMpAdd.values.agama
          : this.state.pickerReligionSelected,
      });
    }
  };

  goBackKtp = (url) => {
    ImageResizer.createResizedImage(url, 2000, 2000, 'JPEG', 50)
      .then((response) => {
        var source = Platform.OS === 'android' ? response.uri : response.path;
        ImgToBase64.getBase64String(source)
          .then((base64String) => {
            this.setState({
              uploadKtp: `data:image/jpeg;base64,${base64String}`,
            });
            this.props.sendUploadKtp(this.state.uploadKtp);

            this.props.updateField('formFdeMpAdd', 'uploadKtp', 'done');
          })
          .catch((err) =>
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            }),
          );
      })
      .catch((err) => {
        handleAlert({
          alertShowed: true,
          alertMessage: err.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      });
    this.props.disableCameraUploadKtp();
  };

  goBackFotoKonsumen = async (url) => {
    ImageResizer.createResizedImage(url, 2000, 2000, 'JPEG', 50)
      .then((response) => {
        var source = Platform.OS === 'android' ? response.uri : response.path;
        ImgToBase64.getBase64String(source)
          .then((base64String) => {
            this.setState({
              uploadDiri: `data:image/jpeg;base64,${base64String}`,
            });
            this.props.sendUploadDiri(this.state.uploadDiri);

            this.props.updateField('formFdeMpAdd', 'uploadDiri', 'done');
          })
          .catch((err) =>
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            }),
          );
      })
      .catch((err) => {
        handleAlert({
          alertShowed: true,
          alertMessage: err.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      });
    this.props.disableCameraUploadFotoKonsumen();
  };

  ViewKtp = (image) => {
    return (
      <ImageView
        currentImage={image}
        images={this.imageView(image)}
        imageIndex={0}
        isVisible={this.state.isImageViewVisibleKtp}
        animationType="fade"
        onClose={() => this.setState({isImageViewVisibleKtp: false})}
        isSwipeCloseEnabled={true}
        isTapZoomEnabled={true}
        isPinchZoomEnabled={true}
      />
    );
  };

  ViewDiri = (image) => {
    return (
      <ImageView
        currentImage={image}
        images={this.imageView(image)}
        imageIndex={0}
        isVisible={this.state.isImageViewVisibleDiri}
        animationType="fade"
        onClose={() => this.setState({isImageViewVisibleDiri: false})}
        isSwipeCloseEnabled={true}
        isTapZoomEnabled={true}
        isPinchZoomEnabled={true}
      />
    );
  };

  imageView = (uri) => [
    {
      source: {
        uri: uri,
      },
    },
  ];

  cancelUploadKtp = async () => {
    this.props.disableCameraUploadKtp();
  };

  cancelUploadFotoKonsumen = async () => {
    this.props.disableCameraUploadFotoKonsumen();
  };

  clearKtp() {
    this.setState({
      uploadKtp: false,
    });
    this.props.updateField('formFdeMpAdd', 'uploadKtp', '');
  }

  ViewDiri = (image) => {
    return (
      <ImageView
        currentImage={image}
        images={this.imageView(image)}
        imageIndex={0}
        isVisible={this.state.isImageViewVisibleDiri}
        animationType="fade"
        onClose={() => this.setState({isImageViewVisibleDiri: false})}
        isSwipeCloseEnabled={true}
        isTapZoomEnabled={true}
        isPinchZoomEnabled={true}
      />
    );
  };

  componentDidUpdate = async (prevProps) => {
    const {
      ddeEducation_Result,
      ddeReligion_Result,
      ddeGender_Result,
      ddeMaritalStatus_Result,
      otpValidateResult,
      otpValidateError,
      qdePersonalDetailData_Result,
      requestLastData,
      verificationScreenEnable,
      cameraUploadKtpEnable,
      cameraUploadFotoKonsumenEnable,
    } = this.props;

    if (
      otpValidateResult !== null &&
      prevProps.otpValidateResult !== otpValidateResult
    ) {
      this.setState({
        validate: otpValidateResult.Validate,
      });
    }

    if (
      verificationScreenEnable !== prevProps.verificationScreenEnable &&
      verificationScreenEnable == false
    ) {
      this.handleScrollToEnd();
    }

    if (
      cameraUploadKtpEnable !== prevProps.cameraUploadKtpEnable &&
      cameraUploadKtpEnable == false
    ) {
      this.handleScrollToEnd();
    }

    if (
      cameraUploadFotoKonsumenEnable !==
        prevProps.cameraUploadFotoKonsumenEnable &&
      cameraUploadFotoKonsumenEnable == false
    ) {
      this.handleScrollToEnd();
    }

    if (
      otpValidateError !== null &&
      prevProps.otpValidateError !== otpValidateError
    ) {
      // console.log('gagal');
      this.setState({
        alertShowed: true,
        alertMessage: 'Kode OTP yang anda masukkan salah',
        alertTitle: 'Gagal !',
        alertType: 'error',
        alertDoneText: 'Back',
      });
    }

    if (this.state.timer === 0) {
      clearInterval(this.interval);
      this.setState({
        disabledOtpButton: false,
        timer: null,
        error: false,
      });
    }

    if (
      ddeGender_Result.length &&
      prevProps.ddeGender_Result !== ddeGender_Result
    ) {
      var arrGender = [{Code: '', Descr: 'Silahkan Pilih'}];

      for (var i = 0; i < ddeGender_Result.length; i++) {
        arrGender.push({
          Code: ddeGender_Result[i].Code + '^' + ddeGender_Result[i].Descr,
          Descr: ddeGender_Result[i].Descr,
        });
      }
      // console.log(arrGender)
      this.setState({pickerGenderItem: arrGender});
    }

    if (
      ddeEducation_Result.length &&
      prevProps.ddeEducation_Result !== ddeEducation_Result
    ) {
      var arrayData = [{Code: '', Descr: 'Silahkan Pilih'}];

      for (var i = 0; i < ddeEducation_Result.length; i++) {
        arrayData.push({
          Code:
            ddeEducation_Result[i].Code + '^' + ddeEducation_Result[i].Descr,
          Descr: ddeEducation_Result[i].Descr,
        });
      }
      this.setState({pickerEducationItem: arrayData});
    }

    if (
      ddeMaritalStatus_Result.length &&
      prevProps.ddeMaritalStatus_Result !== ddeMaritalStatus_Result
    ) {
      var arrayData = [{Code: '', Descr: 'Silahkan Pilih'}];

      for (var i = 0; i < ddeMaritalStatus_Result.length; i++) {
        arrayData.push({
          Code:
            ddeMaritalStatus_Result[i].Code +
            '^' +
            ddeMaritalStatus_Result[i].Descr,
          Descr: ddeMaritalStatus_Result[i].Descr,
        });
      }
      this.setState({pickerMaritalStatusItem: arrayData});
    }

    if (
      ddeReligion_Result.length &&
      prevProps.ddeReligion_Result !== ddeReligion_Result
    ) {
      var arrayData = [{Code: '', Descr: 'Silahkan Pilih'}];

      for (var i = 0; i < ddeReligion_Result.length; i++) {
        arrayData.push({
          Code: ddeReligion_Result[i].Code + '^' + ddeReligion_Result[i].Descr,
          Descr: ddeReligion_Result[i].Descr,
        });
      }
      this.setState({pickerReligionItem: arrayData});
    }

    if (
      qdePersonalDetailData_Result !== null &&
      prevProps.qdePersonalDetailData_Result !== qdePersonalDetailData_Result
    ) {
      this.setState({
        pickerGenderSelected: qdePersonalDetailData_Result.JenisKelamin
          ? qdePersonalDetailData_Result.JenisKelamin
          : '',
        pickerEducationSelected: qdePersonalDetailData_Result.PendidikanTerakhir
          ? qdePersonalDetailData_Result.PendidikanTerakhir
          : '',
        pickerReligionSelected: qdePersonalDetailData_Result.Agama
          ? qdePersonalDetailData_Result.Agama
          : '',
        pickerMaritalStatusSelected: qdePersonalDetailData_Result.StatusPernikahan
          ? qdePersonalDetailData_Result.StatusPernikahan
          : '',
      });
    }
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});

    if (this.state.alertFor === 'logout') {
      this.setState({alertFor: null});
    }
  };

  imagePickerDiri() {
    this.props.enableCameraUploadFotoKonsumen();
  }

  imagePickerKtp() {
    this.props.enableCameraUploadKtp();
  }

  clearDiri() {
    this.setState({
      uploadDiri: false,
    });
    this.props.updateField('formFdeMpAdd', 'uploadDiri', '');
  }

  changeMasaBerlakuKtp(value) {
    this.setState({
      masaBerlakuKtp: value,
    });
    this.props.updateField('formFdeMpAdd', 'masaBerlakuKtp', value);
  }

  changeGender(value) {
    this.setState({
      pickerGenderSelected: value,
    });
    if (value == '') {
      this.props.updateField(
        'formFdeMpAdd',
        'jenisKelamin',
        this.state.pickerGenderItem.Code,
      );
    } else {
      this.props.updateField('formFdeMpAdd', 'jenisKelamin', value);
    }
  }

  changeEducation(value) {
    this.setState({
      pickerEducationSelected: value,
    });
    if (value == '') {
      this.props.updateField(
        'formFdeMpAdd',
        'pendidikanTerakhir',
        this.state.pickerEducationItem.Code,
      );
    } else {
      this.props.updateField('formFdeMpAdd', 'pendidikanTerakhir', value);
    }
  }

  changeMaritalStatus(value) {
    this.setState({
      pickerMaritalStatusSelected: value,
    });
    if (value == '') {
      this.props.updateField(
        'formFdeMpAdd',
        'statusPernikahan',
        this.state.pickerMaritalStatusItem.Code,
      );
      this.setState({showDetailPasangan: false});
    } else {
      this.props.updateField('formFdeMpAdd', 'statusPernikahan', value);
      if (value.indexOf('MAR') !== -1) {
        this.setState({
          showDetailPasangan: true,
        });
      } else {
        this.setState({showDetailPasangan: false});
      }
    }
  }

  changeReligion(value) {
    this.setState({
      pickerReligionSelected: value,
    });
    if (value == '') {
      this.props.updateField(
        'formFdeMpAdd',
        'agama',
        this.state.pickerReligionItem.Code,
      );
    } else {
      this.props.updateField('formFdeMpAdd', 'agama', value);
    }
  }

  setSelection = () => {
    this.setState({
      isSelected: !isSelected,
    });
  };

  handleScrollToEnd = async () => {
    console.log('test');
    setTimeout(() => {
      this.content._root.scrollToEnd();
      console.log('done');
    }, 1000);
  };

  faceVerificationComponent = () => {
    
      return (
        <>
          <TouchableOpacity
            onPress={() => this.props.verificationFaceHandle()}
            disabled={this.props.verifikasiWajah}
            style={
              this.props.verifikasiWajah
                ? styles.ButtonDisabled
                : styles.verificationValidationButton
            }>
            <Text
              style={{
                color: 'white',
                fontSize: 16,
                fontFamily: fonts.primary.normal,
              }}>
              Verifikasi Wajah
            </Text>
          </TouchableOpacity>

          <Field
            name="verifikasiWajah"
            type="hidden"
            component={renderFieldHidden}
          />
        </>
      );
  };

  faceVerification = () => {
    return (
      <>
        <Container style={{flex: 1}}>
          <Content
            disableKBDismissScroll={true}
            style={styles.firstWizardContent}>
            <View style={styles.borderLine}>
              <Text style={styles.headerTextVerification}>
                Verifikasi Wajah
              </Text>
            </View>
            <View style={styles.centerWrapper}>
              <View style={{height: 40}} />
              <Text
                style={{
                  fontFamily: fonts.primary.bold,
                  fontSize: 18,
                  width: '100%',
                  color: BAF_COLOR_BLUE,
                  textAlign: 'center',
                }}>
                {'Pastikan Tidak Ada Orang Lain\n Selain Kamu Saat Merekam.'}
              </Text>
              <View style={{height: 17}} />
              <Text
                style={{
                  fontFamily: fonts.primary.normal,
                  fontSize: 14,
                  width: '100%',
                  color: BAF_COLOR_BLUE,
                  textAlign: 'center',
                }}>
                {
                  'Kamu perlu merekam diri sendiri saat melakukan \n 3 gerakan sederhana'
                }
              </Text>
              <View style={{height: 80}} />
              <TouchableOpacity
                onPress={this.props.callLivenessCheck}
                style={{
                  height: 60,
                  width: 60,
                  backgroundColor: BAF_COLOR_BLUE,
                  borderRadius: 100,
                }}
              />
              <View style={{height: 20}} />
              <Text
                style={{
                  color: BAF_COLOR_BLUE,
                  fontFamily: fonts.primary.normal,
                  fontSize: 16,
                }}>
                Tekan mulai rekaman
              </Text>
              <View style={{height: 100}} />
            </View>
          </Content>
        </Container>
      </>
    );
  };

  render() {
    const {
      handleSubmit,
      submitting,
      openDatePicker,
      toggleModal,
      lastDataRequest,
      lastDataStatus,
      loading,
      lastOrderStatus,
      jenisKelamin,
      cameraUploadKtpEnable,
      cameraUploadFotoKonsumenEnable,
      previousPage,
      submitPrivy_loading,
      verificationScreenEnable,
      verificationFaceHandle,
    } = this.props;
    const {
      pickerGenderItem,
      pickerGenderSelected,
      pickerEducationItem,
      pickerEducationSelected,
      pickerMaritalStatusItem,
      pickerMaritalStatusSelected,
      pickerReligionItem,
      pickerReligionSelected,
    } = this.state;
    if (cameraUploadKtpEnable && Platform.OS === 'android') {
      return (
        <UploadKtpAndroid
          goBack={(url) => this.goBackKtp(url)}
          cancelUpload={() => this.cancelUploadKtp()}
        />
      );
    }
    if (cameraUploadFotoKonsumenEnable && Platform.OS === 'android') {
      return (
        <UploadFotoKonsumenAndroid
          goBack={(url) => this.goBackFotoKonsumen(url)}
          cancelUpload={() => this.cancelUploadFotoKonsumen()}
        />
      );
    }

    if (cameraUploadKtpEnable && Platform.OS !== 'android') {
      return (
        <UploadKtpIos
          goBack={(url) => this.goBackKtp(url)}
          cancelUpload={() => this.cancelUploadKtp()}
        />
      );
    }
    if (cameraUploadFotoKonsumenEnable && Platform.OS !== 'android') {
      return (
        <UploadFotoKonsumenIos
          goBack={(url) => this.goBackFotoKonsumen(url)}
          cancelUpload={() => this.cancelUploadFotoKonsumen()}
        />
      );
    }
    if (verificationScreenEnable) {
      return this.faceVerification();
    } else {
      return (
        <Container style={{flex: 1}}>
          <Content
            ref={(ref) => (this.content = ref)}
            disableKBDismissScroll={true}
            style={styles.firstWizardContent}>
            <Toast
              ref="toast"
              style={styles.firstWizardToast}
              position="top"
              positionValue={0}
              fadeInDuration={2000}
              fadeOutDuration={1000}
              opacity={0.9}
            />
            <Form>
              <Spinner
                visible={loading || submitPrivy_loading ? true : false}
                textContent={'Loading...'}
                textStyle={{color: '#FFF'}}
              />
              <ListItem style={styles.FWLastDataContainer}>
                <CheckBox
                  checked={lastDataStatus}
                  style={styles.FWLastdataCheckBox}
                  onPress={lastDataRequest}
                />
                <Text style={styles.FWLastDataConfirmButton}>
                  Gunakan data sebelumnya ?{' '}
                </Text>
              </ListItem>

              <Field
                name="orderTrxhId"
                type="text"
                component={renderFieldHidden}
                label="Order Id"
              />
              <Field
                name="nomorAplikasi"
                type="text"
                component={renderFieldDisabled}
                label="Nomor Aplikasi"
              />
              <Field
                name="dataSumberAplikasi"
                type="text"
                component={renderFieldDisabled}
                label="Sumber Aplikasi"
              />
              <Field
                name="nomorOrder"
                type="text"
                component={renderFieldDisabled}
                label="Nomor Order"
              />

              <View style={{marginTop: 40}} />

              <Field
                name="namaKonsumen"
                type="text"
                component={renderFieldDisabled}
                label="Nama Konsumen"
              />
              <Field
                name="namaPanggilan"
                type="text"
                component={renderField}
                label="Nama Panggilan (bila ada)"
              />
              <Field
                name="NIK"
                type="text"
                component={renderFieldDisabled}
                label="NIK KTP"
              />
              <Field
                name="masaBerlakuKtp"
                component={
                  Platform.OS == 'android'
                    ? renderFieldDatepicker
                    : renderFieldDatepickerIos
                }
                placeholder="Masa berlaku Ktp (bila ada)"
                formatdate="YYYY-MM-DD"
                date={this.state.masaBerlakuKtp}
                onDateChange={(date) => {
                  this.changeMasaBerlakuKtp(date);
                }}
                openModal={() => openDatePicker()}
                closeModal={() => toggleModal()}
                visible={this.props.visible}
              />
              <Field
                name="tempatLahir"
                type="text"
                component={renderFieldDisabled}
                label="Tempat Lahir"
              />
              <Field
                name="tanggalLahir"
                type="text"
                component={renderFieldDisabled}
                label="Tanggal Lahir"
              />
              <Field
                name="jenisKelamin"
                component={renderFieldPicker}
                label="Jenis kelamin"
                pickerSelected={pickerGenderSelected}
                onValueChange={(itemValue, itemIndex) =>
                  this.changeGender(itemValue)
                }
                data={pickerGenderItem.map((pickerGenderItem, index) => (
                  <Picker.Item
                    label={pickerGenderItem.Descr}
                    value={pickerGenderItem.Code}
                  />
                ))}
              />
              <Field
                name="pendidikanTerakhir"
                component={renderFieldPicker}
                label="Pendidikan Terakhir"
                pickerSelected={pickerEducationSelected}
                onValueChange={(itemValue, itemIndex) =>
                  this.changeEducation(itemValue)
                }
                data={pickerEducationItem.map((pickerEducationItem, index) => (
                  <Picker.Item
                    label={pickerEducationItem.Descr}
                    value={pickerEducationItem.Code}
                  />
                ))}
              />

              <Field
                name="pendidikanTerakhirLainya"
                type="text"
                component={renderFieldHidden}
                label="Pendidikan terakhir Lainya"
              />
              <View style={{marginTop: 40}} />
              <Field
                name="kodeArea"
                type="text"
                component={renderFieldHidden}
                label="Kode Area"
                keyboardType="number-pad"
                maxLength={4}
              />
              <Field
                name="telepon"
                type="text"
                component={renderFieldHidden}
                label="Telepon"
                keyboardType="number-pad"
                maxLength={10}
              />
              <Field
                name="noHandphone1"
                type="text"
                component={renderFieldDisabled}
                label="No Handphone 1"
              />
              <Field
                name="noHandphone2"
                type="text"
                component={renderField}
                label="No Handphone 2 (bila ada)"
                keyboardType="number-pad"
                maxLength={15}
              />
              <Field
                name="noHandphone3"
                type="text"
                component={renderField}
                label="No Handphone 3 (bila ada)"
                keyboardType="number-pad"
                maxLength={15}
              />
              <View style={{marginTop: 40}} />
              <Field
                name="statusPernikahan"
                component={renderFieldPicker}
                label="Status Pernikahan"
                pickerSelected={pickerMaritalStatusSelected}
                onValueChange={(itemValue, itemIndex) =>
                  this.changeMaritalStatus(itemValue)
                }
                data={pickerMaritalStatusItem.map(
                  (pickerMaritalStatusItem, index) => (
                    <Picker.Item
                      label={pickerMaritalStatusItem.Descr}
                      value={pickerMaritalStatusItem.Code}
                    />
                  ),
                )}
              />
              <Field
                name="namaPasangan"
                type="text"
                component={
                  this.state.showDetailPasangan
                    ? renderField
                    : renderFieldHidden
                }
                label="Nama Pasangan"
              />
              <Field
                name="noHandphonePasangan"
                type="text"
                component={
                  this.state.showDetailPasangan
                    ? renderField
                    : renderFieldHidden
                }
                label="No Handphone Pasangan"
                keyboardType="number-pad"
                maxLength={15}
              />
              <Field
                name="agama"
                component={renderFieldPicker}
                label="Agama"
                pickerSelected={pickerReligionSelected}
                onValueChange={(itemValue, itemIndex) =>
                  this.changeReligion(itemValue)
                }
                data={pickerReligionItem.map((pickerReligionItem, index) => (
                  <Picker.Item
                    label={pickerReligionItem.Descr}
                    value={pickerReligionItem.Code}
                  />
                ))}
              />
              <Field
                name="namaIbuKandung"
                type="text"
                component={renderFieldDisabled}
                label="Nama Ibu Kandung"
              />
              <Field
                name="email"
                type="text"
                component={renderFieldDisabled}
                label="Email"
              />
              {this.props.fdePrivy == 1 && (
                <>
                  <View
                    style={{
                      borderBottomWidth: 2,
                      borderBottomColor: 'grey',
                      paddingBottom: 10,
                      marginTop: 30,
                    }}>
                    <Text
                      style={{
                        color: 'grey',
                        fontSize: 18,
                        fontWeight: 'bold',
                        fontFamily: fonts.primary.normal,
                      }}>
                      Data Upload
                    </Text>
                  </View>
                  {this.state.uploadKtp ? (
                    <TouchableOpacity
                      style={styles.CLContainer}
                      onPress={() => {
                        this.setState({isImageViewVisibleKtp: true});
                      }}>
                      <Image
                        source={{uri: this.state.uploadKtp}}
                        style={styles.CLImage}
                      />
                      <TouchableOpacity
                        style={styles.CLDeleteImage}
                        onPress={() => this.clearKtp()}>
                        <Text style={styles.CLDeleteImageIcon}>X</Text>
                      </TouchableOpacity>
                    </TouchableOpacity>
                  ) : null}

                  <TouchableOpacity
                    style={
                      this.state.disableButtonUpload || this.state.uploadKtp
                        ? styles.ButtonDisabled
                        : styles.verificationValidationButton
                    }
                    disabled={
                      this.state.disableButtonUpload || this.state.uploadKtp
                        ? true
                        : false
                    }
                    onPress={() => this.imagePickerKtp()}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 16,
                        fontFamily: fonts.primary.normal,
                      }}>
                      Upload Foto KTP
                    </Text>
                  </TouchableOpacity>
                  <Field
                    name="uploadKtp"
                    type="hidden"
                    component={renderFieldHidden}
                  />

                  {this.state.uploadDiri ? (
                    <TouchableOpacity
                      style={styles.CLContainer}
                      onPress={() => {
                        this.setState({isImageViewVisibleDiri: true});
                      }}>
                      <Image
                        source={{uri: this.state.uploadDiri}}
                        style={styles.CLImage}
                      />
                      <TouchableOpacity
                        style={styles.CLDeleteImage}
                        onPress={() => this.clearDiri()}>
                        <Text style={styles.CLDeleteImageIcon}>X</Text>
                      </TouchableOpacity>
                    </TouchableOpacity>
                  ) : null}

                  <TouchableOpacity
                    style={
                      this.state.disableButtonUpload || this.state.uploadDiri
                        ? styles.ButtonDisabled
                        : styles.verificationValidationButton
                    }
                    disabled={
                      this.state.disableButtonUpload || this.state.uploadDiri
                        ? true
                        : false
                    }
                    onPress={() => this.imagePickerDiri()}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 16,
                        fontFamily: fonts.primary.normal,
                      }}>
                      Upload Foto Konsumen
                    </Text>
                  </TouchableOpacity>

                  <Field
                    name="uploadDiri"
                    type="hidden"
                    component={renderFieldHidden}
                  />

                  {this.faceVerificationComponent()}
                  {this.ViewKtp(this.state.uploadKtp)}
                  {this.ViewDiri(this.state.uploadDiri)}
                </>
              )}

              <View style={styles.ScrollViewBottom} />
            </Form>
            {this.showAlert()}
          </Content>

          <HideWithKeyboard style={styles.buttonBottomContainer}>
            <TouchableOpacity
              style={styles.submitButton}
              onPress={handleSubmit}
              disabled={submitting}>
              <Text style={styles.FWNextButton}>Lanjut</Text>
            </TouchableOpacity>
          </HideWithKeyboard>
        </Container>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    ddeEducation_Loading: state.ddeEducation.loading,
    ddeEducation_Result: state.ddeEducation.result,
    ddeEducation_Error: state.ddeEducation.error,
    ddeGender_Loading: state.ddeGender.loading,
    ddeGender_Result: state.ddeGender.result,
    ddeGender_Error: state.ddeGender.error,
    ddeMaritalStatus_Loading: state.ddeMaritalStatus.loading,
    ddeMaritalStatus_Result: state.ddeMaritalStatus.result,
    ddeMaritalStatus_Error: state.ddeMaritalStatus.error,
    ddeReligion_Loading: state.ddeReligion.loading,
    ddeReligion_Result: state.ddeReligion.result,
    ddeReligion_Error: state.ddeReligion.error,
    formFdeMpAdd: state.form.formFdeMpAdd,
    otpSmsLoading: state.otpSms.loading,
    otpSmsResult: state.otpSms.result,
    otpSmsError: state.otpSms.error,
    otpValidateLoading: state.otpValidate.loading,
    otpValidateResult: state.otpValidate.result,
    otpValidateError: state.otpValidate.error,
    qdePersonalDetailData_Result: state.qdePersonalDetailData.result,
    submitPrivy_loading: state.submitPrivy.loading,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      otpCall,
      otpSms,
      otpValidate,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
    },
    dispatch,
  );
}

FirstWizard = reduxForm({
  form: 'formFdeMpAdd',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true,
  enableReinitialize: true,
  validate: FormFdeAddMpValidate,
})(FirstWizard);

export default connect(mapStateToProps, matchDispatchToProps)(FirstWizard);
