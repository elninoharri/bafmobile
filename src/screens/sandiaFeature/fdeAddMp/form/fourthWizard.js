import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { Field, reduxForm, change } from 'redux-form'
import { renderField, renderFieldPicker, renderFieldDisabled, renderFieldTextArea } from '../index'
import { Text, View, TouchableOpacity } from 'react-native';
import { Container, Content, Form, Picker } from 'native-base';
import styles from '../style';
import { HideWithKeyboard } from 'react-native-hide-with-keyboard';
import { FormFdeAddMpValidate } from "../../../../validates/FormFdeAddMpValidate"
import { ddeJobCity, ddeJobKecamatan, ddeJobKelurahan } from "../../../../actions/sandiaFeature/dataLengkap/dataLengkapWilayah";
import { ddeJobType, ddeJobPosition, ddeIndustryType, ddeLamaUsahaTahun, ddeLamaUsahaBulan,
    ddeKontrakTahun, ddeKontrakBulan } from "../../../../actions/sandiaFeature/dataLengkap/dataLengkapPekerjaan";
import { substringCircum, substringThirdCircum, normalizeCurrency,fdeFormatCurrency } from "../../../../utils/utilization";
import AsyncStorage from '@react-native-community/async-storage';
import DisabledForm from "../../../../components/multiPlatform/sandiaFeature/disabledForm";
import { CODE_FIELD_JOB_CATEGORY, CODE_FIELD_JOB_STATUS_2 } from '../../../../utils/constant'
import { FSAlbert } from '../../../../utils/fonts';

const items = [
  {
    id: 1,
    name: 'Pria',
  },
  {
    id: 2,
    name: 'Wanita',
  }
];

class FourthWizard extends Component {
    constructor(props) {
        super(props)
        this.changeJobCategory = this.changeJobCategory.bind(this)
        this.changeJobStatus = this.changeJobStatus.bind(this)
        this.changeJobType = this.changeJobType.bind(this)
        this.changeJobPosition = this.changeJobPosition.bind(this)
        this.changeIndustryType = this.changeIndustryType.bind(this)
        this.changeProvinsi = this.changeProvinsi.bind(this)
        this.changeKotaKabupaten = this.changeKotaKabupaten.bind(this)
        this.changeKecamatan = this.changeKecamatan.bind(this)
        this.changeKelurahan = this.changeKelurahan.bind(this)
        this.changeLamaUsahaBulan = this.changeLamaUsahaBulan.bind(this)
        this.changeLamaUsahaTahun = this.changeLamaUsahaTahun.bind(this)
        this.changeKontrakBulan = this.changeKontrakBulan.bind(this)
        this.changeKontrakTahun = this.changeKontrakTahun.bind(this)
        this.changeOfficeOwnership = this.changeOfficeOwnership.bind(this)
        this.state = {
          userAccess: '',
          userToken: '',

          pickerJobCategoryItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
          pickerJobCategorySelected: '',

          pickerJobStatusItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
          pickerJobStatusSelected: '',

          pickerJobTypeItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
          pickerJobTypeSelected: '',

          pickerJobPositionItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
          pickerJobPositionSelected: '',

          pickerJobProvinsiItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
          pickerJobProvinsiSelected: '',

          pickerJobKotaKabupatenItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
          pickerJobKotaKabupatenSelected: '',

          pickerJobKecamatanItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
          pickerJobKecamatanSelected: '',

          pickerJobKelurahanItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
          pickerJobKelurahanSelected: '',

          pickerLamaUsahaTahunItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
          pickerLamaUsahaTahunSelected: '',

          pickerLamaUsahaBulanItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
          pickerLamaUsahaBulanSelected: '',

          pickerKontrakTahunItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
          pickerKontrakTahunSelected: '',

          pickerKontrakBulanItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
          pickerKontrakBulanSelected: '',
          
          pickerOfficeOwnershipItem: [{ Code: '', Descr: 'Silahkan Pilih' }],
          pickerOfficeOwnershipSelected: '',

          disableJobType: true,
          disableJobPosition: true,
          disableIndustryType: true,
          disableKotaKabupaten: true,
          disableKecamatan: true,
          disableKelurahan: true,
          disableJobStatus: true,
          disabledPeriodeKontrak: true,
        }
    }

    componentDidMount = async () => {
        const { 
          formFdeMpAdd, 
          ddeJobCity, 
          ddeJobKecamatan, 
          ddeJobKelurahan, 
          ddeJobType, 
          ddeJobPosition, 
          ddeIndustryType, 
          ddeJobCategory_Result, 
          ddeJobStatus_Result, 
          ddeJobProvince_Result, 
          ddeOfficeOwnership_Result, 
          ddeLamaUsahaBulan_Result,
          ddeLamaUsahaTahun_Result, 
          ddeKontrakBulan_Result, 
          ddeKontrakTahun_Result 
        } = this.props;
    
        const jsonUserAccess = await AsyncStorage.getItem('userAccess');
        var userAccess = JSON.parse(jsonUserAccess)
        this.setState({ userAccess: userAccess })
    
        const userToken = await AsyncStorage.getItem('userToken');
        this.setState({ userToken: userToken })

        if (ddeJobCategory_Result.length > 0) {
          var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
          for (var i=0; i<ddeJobCategory_Result.length; i++) {
            arrayData.push({
              Code: ddeJobCategory_Result[i].Code + '^' + ddeJobCategory_Result[i].Descr,
              Descr: ddeJobCategory_Result[i].Descr
            })
          }
          this.setState({ pickerJobCategoryItem: arrayData });
        }

        if (ddeJobStatus_Result.length > 0) {
          var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
          for (var i=0; i<ddeJobStatus_Result.length; i++) {
            arrayData.push({
              Code: ddeJobStatus_Result[i].Code + '^' + ddeJobStatus_Result[i].Descr,
              Descr: ddeJobStatus_Result[i].Descr
            })
          }
          this.setState({ pickerJobStatusItem: arrayData });
        }

        if (ddeJobProvince_Result.length > 0) {
          var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
          for (var i=0; i<ddeJobProvince_Result.length; i++) {
            arrayData.push({
              Code: ddeJobProvince_Result[i].Code + '^' + ddeJobProvince_Result[i].Descr,
              Descr: ddeJobProvince_Result[i].Descr
            })
          }
          this.setState({ pickerJobProvinsiItem: arrayData });
        }
    
        if (ddeOfficeOwnership_Result.length > 0) {
          var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
          for (var i=0; i<ddeOfficeOwnership_Result.length; i++) {
            arrayData.push({
              Code: ddeOfficeOwnership_Result[i].Code + '^' + ddeOfficeOwnership_Result[i].Descr,
              Descr: ddeOfficeOwnership_Result[i].Descr
            })
          }
          this.setState({ pickerOfficeOwnershipItem: arrayData });
        }

        if (ddeLamaUsahaBulan_Result.length > 0) {
          var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
          for (var i=0; i<ddeLamaUsahaBulan_Result.length; i++) {
            arrayData.push({
              Code: ddeLamaUsahaBulan_Result[i].Code + '^' + ddeLamaUsahaBulan_Result[i].Descr,
              Descr: ddeLamaUsahaBulan_Result[i].Descr
            })
          }
          this.setState({ pickerLamaUsahaBulanItem: arrayData });
        }

        if (ddeLamaUsahaTahun_Result.length > 0) {
          var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
          for (var i=0; i<ddeLamaUsahaTahun_Result.length; i++) {
            arrayData.push({
              Code: ddeLamaUsahaTahun_Result[i].Code + '^' + ddeLamaUsahaTahun_Result[i].Descr,
              Descr: ddeLamaUsahaTahun_Result[i].Descr
            })
          }
          this.setState({ pickerLamaUsahaTahunItem: arrayData });
        }

        if (ddeKontrakBulan_Result.length > 0) {
          var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
          for (var i=0; i<ddeKontrakBulan_Result.length; i++) {
            arrayData.push({
              Code: ddeKontrakBulan_Result[i].Code + '^' + ddeKontrakBulan_Result[i].Descr,
              Descr: ddeKontrakBulan_Result[i].Descr
            })
          }
          this.setState({ pickerKontrakBulanItem: arrayData });
        }

        if (ddeKontrakTahun_Result.length > 0) {
          var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
          for (var i=0; i<ddeKontrakTahun_Result.length; i++) {
            arrayData.push({
              Code: ddeKontrakTahun_Result[i].Code + '^' + ddeKontrakTahun_Result[i].Descr,
              Descr: ddeKontrakTahun_Result[i].Descr
            })
          }
          this.setState({ pickerKontrakTahunItem: arrayData });
        }
        
        if (formFdeMpAdd.values) {
          this.setState({ 
            pickerJobStatusSelected: formFdeMpAdd.values.jobStatus ? formFdeMpAdd.values.jobStatus : this.state.pickerJobStatusSelected,
            pickerLamaUsahaTahunSelected: formFdeMpAdd.values.lamaUsahaTahun ? formFdeMpAdd.values.lamaUsahaTahun : this.state.pickerLamaUsahaTahunSelected,
            pickerLamaUsahaBulanSelected: formFdeMpAdd.values.lamaUsahaBulan ? formFdeMpAdd.values.lamaUsahaBulan : this.state.pickerLamaUsahaBulanSelected,
            pickerKontrakTahunSelected: formFdeMpAdd.values.kontrakTahun ? formFdeMpAdd.values.kontrakTahun : this.state.pickerKontrakTahunSelected,
            pickerKontrakBulanSelected: formFdeMpAdd.values.kontrakBulan ? formFdeMpAdd.values.kontrakBulan : this.state.pickerKontrakBulanSelected,
            pickerOfficeOwnershipSelected: formFdeMpAdd.values.statusUsaha ? formFdeMpAdd.values.statusUsaha : this.state.pickerOfficeOwnershipSelected,
            pickerJobKelurahanSelected: formFdeMpAdd.values.jobKelurahan ? formFdeMpAdd.values.jobKelurahan : this.state.pickerJobKelurahanSelected,
            pickerIndustryTypeSelected: formFdeMpAdd.values.industryType ? formFdeMpAdd.values.industryType : this.state.pickerindustryTypeSelected
          })
        }

        if (formFdeMpAdd.values && formFdeMpAdd.values.jobCategory) {
          this.setState({ pickerJobCategorySelected: formFdeMpAdd.values.jobCategory })
          if (formFdeMpAdd.values.jobCategory !== '') {
            ddeJobType({JobCategory: substringCircum(formFdeMpAdd.values.jobCategory)}, userToken)
            if (substringCircum(formFdeMpAdd.values.jobCategory).indexOf(CODE_FIELD_JOB_CATEGORY) !== -1 ) {
              this.setState({ disableJobStatus: false })
            }
          }
        }

        if (formFdeMpAdd.values && formFdeMpAdd.values.jobStatus) {
          if (formFdeMpAdd.values.jobStatus !== '') {
            substringCircum(formFdeMpAdd.values.jobStatus).indexOf(CODE_FIELD_JOB_STATUS_2) !== -1 ?
              this.setState({ disabledPeriodeKontrak: true }) : this.setState({ disabledPeriodeKontrak: false })
          }
        }

        if (formFdeMpAdd.values && formFdeMpAdd.values.jobType) {
          this.setState({ pickerJobTypeSelected: formFdeMpAdd.values.jobType })
          if (formFdeMpAdd.values.jobType !== '') {
            ddeJobPosition({JobType: substringCircum(formFdeMpAdd.values.jobType)}, userToken)
          }
        }

        if (formFdeMpAdd.values && formFdeMpAdd.values.jobPosition) {
          this.setState({ pickerJobPositionSelected: formFdeMpAdd.values.jobPosition })
          if (formFdeMpAdd.values.jobPosition !== '') {
            ddeIndustryType({JobType: substringCircum(formFdeMpAdd.values.jobType), JobPosition: substringCircum(formFdeMpAdd.values.jobPosition)})
          }
        }

        if (formFdeMpAdd.values && formFdeMpAdd.values.jobProvinsi) {
          this.setState({ pickerJobProvinsiSelected: formFdeMpAdd.values.jobProvinsi })
          if (formFdeMpAdd.values.jobProvinsi !== '') {
            ddeJobCity({ProvinceId: substringCircum(formFdeMpAdd.values.jobProvinsi)}, userToken)
          }
        }
    
        if (formFdeMpAdd.values && formFdeMpAdd.values.jobKotaKabupaten) {
          this.setState({ pickerJobKotaKabupatenSelected: formFdeMpAdd.values.jobKotaKabupaten })
          if (formFdeMpAdd.values.jobKotaKabupaten !== '') {
            ddeJobKecamatan({CityId: substringCircum(formFdeMpAdd.values.jobKotaKabupaten)}, userToken)
          }
        }
    
        if (formFdeMpAdd.values && formFdeMpAdd.values.jobKecamatan) {
          this.setState({ pickerJobKecamatanSelected: formFdeMpAdd.values.jobKecamatan })
          if (formFdeMpAdd.values.jobKecamatan !== '') {
            ddeJobKelurahan({KecamatanId: substringCircum(formFdeMpAdd.values.jobKecamatan)}, userToken)
          }
        }
    }

    componentDidUpdate = async (prevProps, prevState) => {
      const { 
        ddeJobCity_Result, 
        ddeJobKecamatan_Result, 
        ddeJobKelurahan_Result, 
        ddeJobType_Result, 
        ddeJobPosition_Result, 
        ddeIndustryType_Result,  
      } = this.props;
      
      if (ddeJobCity_Result.length && prevProps.ddeJobCity_Result !== ddeJobCity_Result) {
        var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
        for (var i=0; i<ddeJobCity_Result.length; i++) {
          arrayData.push({
            Code: ddeJobCity_Result[i].Code + '^' + ddeJobCity_Result[i].Descr,
            Descr: ddeJobCity_Result[i].Descr
          })
        }
        this.setState({ 
          pickerJobKotaKabupatenItem: arrayData,
          disableKotaKabupaten: false
        });
      }
  
      if (ddeJobKecamatan_Result.length && prevProps.ddeJobKecamatan_Result !== ddeJobKecamatan_Result) {
        var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
        for (var i=0; i<ddeJobKecamatan_Result.length; i++) {
          arrayData.push({
            Code: ddeJobKecamatan_Result[i].Code + '^' + ddeJobKecamatan_Result[i].Descr,
            Descr: ddeJobKecamatan_Result[i].Descr
          })
        }
        this.setState({ 
          pickerJobKecamatanItem: arrayData,
          disableKecamatan: false
        });
      }
  
      if (ddeJobKelurahan_Result.length && prevProps.ddeJobKelurahan_Result !== ddeJobKelurahan_Result) {
        var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
        for (var i=0; i<ddeJobKelurahan_Result.length; i++) {
          arrayData.push({
            Code: ddeJobKelurahan_Result[i].Code + '^' + ddeJobKelurahan_Result[i].Descr + '^' + ddeJobKelurahan_Result[i].KodePos,
            Descr: ddeJobKelurahan_Result[i].Descr
          })
        }
        this.setState({ 
          pickerJobKelurahanItem: arrayData,
          disableKelurahan: false
        });
      }

      if (ddeJobType_Result.length && prevProps.ddeJobType_Result !== ddeJobType_Result) {
        var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
  
        for (var i=0; i<ddeJobType_Result.length; i++) {
          arrayData.push({
            Code: ddeJobType_Result[i].Code + '^' + ddeJobType_Result[i].Descr,
            Descr: ddeJobType_Result[i].Descr
          })
        }
        this.setState({ 
          pickerJobTypeItem: arrayData,
          disableJobType: false
        });
      }

      if (ddeJobPosition_Result.length && prevProps.ddeJobPosition_Result !== ddeJobPosition_Result) {
        var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
        for (var i=0; i<ddeJobPosition_Result.length; i++) {
          arrayData.push({
            Code: ddeJobPosition_Result[i].Code + '^' + ddeJobPosition_Result[i].Descr,
            Descr: ddeJobPosition_Result[i].Descr
          })
        }
        this.setState({ 
          pickerJobPositionItem: arrayData,
          disableJobPosition: false
        });
      }

      if (ddeIndustryType_Result.length && prevProps.ddeIndustryType_Result !== ddeIndustryType_Result) {
        var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
        for (var i=0; i<ddeIndustryType_Result.length; i++) {
          arrayData.push({
            Code: ddeIndustryType_Result[i].Code + '^' + ddeIndustryType_Result[i].Descr,
            Descr: ddeIndustryType_Result[i].Descr
          })
        }
        this.setState({ 
          pickerIndustryTypeItem: arrayData,
          disableIndustryType: false
        });
      }
    }

    changeJobCategory(value){
      const { ddeJobType } = this.props
      this.setState({
        pickerJobCategorySelected: value,
        pickerJobStatusSelected: '',
        pickerJobTypeSelected: '',
        pickerJobPositionSelected: '',
        pickerIndustryTypeSelected: '',
        disableJobType: true,
        disableJobPosition: true,
        disableIndustryType: true,
        disableJobStatus: true
      })
      this.props.updateField('formFdeMpAdd', 'jobType', '')
      this.props.updateField('formFdeMpAdd', 'jobPosition', '')
      this.props.updateField('formFdeMpAdd', 'industryType', '')
      this.props.updateField('formFdeMpAdd', 'jobStatus', '')
      if (value == '') {
        this.props.updateField('formFdeMpAdd', 'jobCategory', '')
      } else {
        this.props.updateField('formFdeMpAdd', 'jobCategory', value)
        ddeJobType({JobCategory: substringCircum(value)}, this.state.userToken)
        if (substringCircum(value).indexOf(CODE_FIELD_JOB_CATEGORY) !== -1 ) {
          this.setState({ disableJobStatus: false })
        }
      }
    }

    changeJobStatus(value){
      this.setState({ pickerJobStatusSelected: value })
      if (value == '') {
        this.props.updateField('formFdeMpAdd', 'jobStatus', '')
      } else {
        this.props.updateField('formFdeMpAdd', 'jobStatus', value)
        substringCircum(value).indexOf(CODE_FIELD_JOB_STATUS_2) !== -1 ?
          this.setState({ disabledPeriodeKontrak: true }) : this.setState({ disabledPeriodeKontrak: false })
      }
    }

    changeJobType(value) {
      const { ddeJobPosition } = this.props
      this.setState({
        pickerJobTypeSelected: value,
        pickerJobPositionSelected: '',
        pickerIndustryTypeSelected: '',
        disableJobPosition: true,
        disableIndustryType: true,
      })
      this.props.updateField('formFdeMpAdd', 'jobPosition', '')
      this.props.updateField('formFdeMpAdd', 'industryType', '')
      if(value == ''){
        this.props.updateField('formFdeMpAdd', 'jobType', '')
        this.setState({
          disableJobPosition: true,
          disableIndustryType: true
        })
      } else {
        this.props.updateField('formFdeMpAdd', 'jobType', value)
        ddeJobPosition({JobType: substringCircum(value)}, this.state.userToken)
      }
    }

    changeJobPosition(value){
        const { ddeIndustryType, formFdeMpAdd } = this.props
        this.setState({
          pickerJobPositionSelected: value,
          pickerIndustryTypeSelected: '',
          disableIndustryType: true,
        })
        if(value == ''){
          this.props.updateField('formFdeMpAdd', 'jobPosition', '')
          this.setState({ disableIndustryType: true })
        } else {
          this.props.updateField('formFdeMpAdd', 'jobPosition', value)
          ddeIndustryType({JobType: substringCircum(formFdeMpAdd.values.jobType), JobPosition: substringCircum(value)}, this.state.userToken)
          
        }
    }
    changeIndustryType(value) {
        this.setState({ pickerIndustryTypeSelected: value })

        if(value == ''){
          this.props.updateField('formFdeMpAdd', 'industryType', "")
        } else {
          this.props.updateField('formFdeMpAdd', 'industryType', value)
        }
    }

    changeProvinsi(value){
        const { ddeJobCity } = this.props
        this.setState({
          pickerJobProvinsiSelected: value,
          pickerKotaKabupatenSelected: '',
          pickerKecamatanSelected: '',
          pickerKelurahanSelected: '',
          disableKotaKabupaten: true,
          disableKecamatan: true,
          disableKelurahan: true
        })
        this.props.updateField('formFdeMpAdd', 'jobKecamatan', '')
        this.props.updateField('formFdeMpAdd', 'jobKelurahan', '')
        this.props.updateField('formFdeMpAdd', 'jobKodePos', '')
        if (value == '') {
          this.props.updateField('formFdeMpAdd', 'jobProvinsi', this.state.pickerJobProvinsiItem.Code)
        } else {
          this.props.updateField('formFdeMpAdd', 'jobProvinsi', value)
          ddeJobCity({ProvinceId: substringCircum(value)}, this.state.userToken)
        }
    }
    
    changeKotaKabupaten(value) {
      const { ddeJobKecamatan } = this.props
      this.setState({
        pickerJobKotaKabupatenSelected: value,
        pickerJobKecamatanSelected: '',
        pickerJobKelurahanSelected: '',
        disableKecamatan: true,
        disableKelurahan: true,
      })
      this.props.updateField('formFdeMpAdd', 'jobKecamatan', '')
      this.props.updateField('formFdeMpAdd', 'jobKelurahan', '')
      this.props.updateField('formFdeMpAdd', 'jobKodePos', '')
      
      if(value == ''){
        this.props.updateField('formFdeMpAdd', 'jobKotaKabupaten', '')
        this.setState({
          disableKecamatan: true,
          disableKelurahan: true
        })
      } else {
        this.props.updateField('formFdeMpAdd', 'jobKotaKabupaten', value)
        ddeJobKecamatan({CityId: substringCircum(value)})
      }
    }
  
    changeKecamatan(value) {
      const { ddeJobKelurahan } = this.props
      this.setState({
        pickerJobKecamatanSelected: value,
        pickerJobKelurahanSelected: '',
        disableKelurahan: true,
      })
      this.props.updateField('formFdeMpAdd', 'jobKelurahan', '')
      this.props.updateField('formFdeMpAdd', 'jobKodePos', '')
      
      if(value == ''){
        this.props.updateField('formFdeMpAdd', 'jobKecamatan', '')
        this.setState({
          disableKelurahan: true
        })
      } else {
        this.props.updateField('formFdeMpAdd', 'jobKecamatan', value)
        ddeJobKelurahan({KecamatanId: substringCircum(value)})
      }
    }
  
    changeKelurahan(value) {
      this.setState({ pickerJobKelurahanSelected: value })
  
      if(value == ''){
        this.props.updateField('formFdeMpAdd', 'jobKelurahan', '')
        this.props.updateField('formFdeMpAdd', 'jobKodePos', '')
      } else {
        this.props.updateField('formFdeMpAdd', 'jobKelurahan', value)
        this.props.updateField('formFdeMpAdd', 'jobKodePos', substringThirdCircum(value))
      }
    }

    changeLamaUsahaBulan(value){
      this.setState({ pickerLamaUsahaBulanSelected: value })

      if (value == '') {
        this.props.updateField('formFdeMpAdd', 'lamaUsahaBulan', this.state.pickerLamaUsahaBulanItem.Code)
      } else {
        this.props.updateField('formFdeMpAdd', 'lamaUsahaBulan', value)
      }
    }

    changeLamaUsahaTahun(value){
      this.setState({ pickerLamaUsahaTahunSelected: value })

      if (value == '') {
        this.props.updateField('formFdeMpAdd', 'lamaUsahaTahun', this.state.pickerLamaUsahaTahunItem.Code)
      } else {
        this.props.updateField('formFdeMpAdd', 'lamaUsahaTahun', value)
      }
    }

    changeKontrakBulan(value){
      this.setState({ pickerKontrakBulanSelected: value })

      if (value == '') {
        this.props.updateField('formFdeMpAdd', 'kontrakBulan', this.state.pickerKontrakBulanItem.Code)
      } else {
        this.props.updateField('formFdeMpAdd', 'kontrakBulan', value)
      }
    }

    changeKontrakTahun(value){
      this.setState({ pickerKontrakTahunSelected: value })

      if (value == '') {
        this.props.updateField('formFdeMpAdd', 'kontrakTahun', this.state.pickerKontrakTahunItem.Code)
      } else {
        this.props.updateField('formFdeMpAdd', 'kontrakTahun', value)
      }
    }

    changeOfficeOwnership(value){
      this.setState({ pickerOfficeOwnershipSelected: value })

      if (value == '') {
        this.props.updateField('formFdeMpAdd', 'statusUsaha', this.state.pickerOfficeOwnershipItem.Code)
      } else {
        this.props.updateField('formFdeMpAdd', 'statusUsaha', value)
      }
    }

    render(){
        const { handleSubmit, previousPage } = this.props
        const { 
          pickerJobStatusItem, 
          pickerJobStatusSelected, 
          pickerJobCategoryItem, 
          pickerJobCategorySelected, 
          pickerJobTypeItem, 
          pickerJobTypeSelected,
          pickerJobPositionItem, 
          pickerJobPositionSelected, 
          pickerIndustryTypeItem, 
          pickerIndustryTypeSelected, 
          pickerJobProvinsiItem, 
          pickerJobProvinsiSelected, 
          pickerJobKotaKabupatenItem, 
          pickerJobKotaKabupatenSelected, 
          pickerJobKecamatanItem, 
          pickerJobKecamatanSelected, 
          pickerJobKelurahanItem, 
          pickerJobKelurahanSelected, 
          pickerLamaUsahaTahunItem, 
          pickerLamaUsahaTahunSelected, 
          pickerLamaUsahaBulanItem, 
          pickerLamaUsahaBulanSelected,
          pickerKontrakTahunItem, 
          pickerKontrakTahunSelected, 
          pickerKontrakBulanItem, 
          pickerKontrakBulanSelected,
          pickerOfficeOwnershipItem, 
          pickerOfficeOwnershipSelected,
          disableKotaKabupaten, 
          disableKecamatan, 
          disableKelurahan, 
          disableJobType, 
          disableJobPosition, 
          disableIndustryType 
        } = this.state;
        
        return (
        <Container>
          <Content disableKBDismissScroll={true} style={styles.fourthWizardContainer}>
            <Form>

              <Field
                name="jobCategory"
                component={renderFieldPicker}
                label="Kategori Pekerjaan"
                pickerSelected = {pickerJobCategorySelected}
                onValueChange = {(itemValue) => this.changeJobCategory(itemValue)}
                data = {pickerJobCategoryItem.map((pickerJobCategoryItem) => <Picker.Item label={pickerJobCategoryItem.Descr} value={pickerJobCategoryItem.Code} />)}
              />
              
              <Field
                name="jobStatus"
                component={renderFieldPicker}
                label="Status Pekerjaan"
                pickerSelected = {pickerJobStatusSelected}
                onValueChange = {(itemValue) => this.changeJobStatus(itemValue)}
                data = {pickerJobStatusItem.map((pickerJobStatusItem) => <Picker.Item label={pickerJobStatusItem.Descr} value={pickerJobStatusItem.Code} />)}
                enabled= {this.state.disableJobStatus ? false : true}
              />
              
              { disableJobType ? <DisabledForm title = "Tipe Pekerjaan"/> :
                <Field
                  name="jobType"
                  component={renderFieldPicker}
                  label="Tipe Pekerjaan"
                  pickerSelected = {pickerJobTypeSelected}
                  onValueChange = {(itemValue) => this.changeJobType(itemValue)}
                  data = {pickerJobTypeItem.map((pickerJobTypeItem) => <Picker.Item label={pickerJobTypeItem.Descr} value={pickerJobTypeItem.Code} />)}
                />
              }

              { disableJobPosition ? <DisabledForm title = "Jabatan"/> :
                <Field
                  name="jobPosition"
                  component={renderFieldPicker}
                  label="Jabatan"
                  pickerSelected = {pickerJobPositionSelected}
                  onValueChange = {(itemValue) => this.changeJobPosition(itemValue)}
                  data = {pickerJobPositionItem.map((pickerJobPositionItem) => <Picker.Item label={pickerJobPositionItem.Descr} value={pickerJobPositionItem.Code} />)}
                />
              }

              { disableIndustryType ? <DisabledForm title = "Bidang Usaha"/> :
                <Field
                  name="industryType"
                  component={renderFieldPicker}
                  label="Bidang Usaha"
                  pickerSelected = {pickerIndustryTypeSelected}
                  onValueChange = {(itemValue) => this.changeIndustryType(itemValue)}
                  data = {pickerIndustryTypeItem.map((pickerIndustryTypeItem) => <Picker.Item label={pickerIndustryTypeItem.Descr} value={pickerIndustryTypeItem.Code} />)}
                />
              }
                   
              <View style={{marginTop:40}}/>
              
              <Field
                name="namaPerusahaan"
                type="text"
                component={renderField}
                label="Nama Perusahaan"
              />

              <Field
                name="lamaUsahaBulan"
                component={renderFieldPicker}
                label="Bekerja Sejak (Bulan)"
                pickerSelected = {pickerLamaUsahaBulanSelected}
                onValueChange = {(itemValue, itemIndex) => this.changeLamaUsahaBulan(itemValue)}
                data = {pickerLamaUsahaBulanItem.map((pickerLamaUsahaBulanItem, index) => <Picker.Item label={pickerLamaUsahaBulanItem.Descr} value={pickerLamaUsahaBulanItem.Code} />)}
              />

              <Field
                name="lamaUsahaTahun"
                component={renderFieldPicker}
                label="Bekerja Sejak (Tahun)"
                pickerSelected = {pickerLamaUsahaTahunSelected}
                onValueChange = {(itemValue, itemIndex) => this.changeLamaUsahaTahun(itemValue)}
                data = {pickerLamaUsahaTahunItem.map((pickerLamaUsahaTahunItem, index) => <Picker.Item label={pickerLamaUsahaTahunItem.Descr} value={pickerLamaUsahaTahunItem.Code} />)}
              />

              <Field
                name="kontrakBulan"
                component={renderFieldPicker}
                label="Periode Masa Kontrak (Bulan)"
                pickerSelected = {pickerKontrakBulanSelected}
                onValueChange = {(itemValue, itemIndex) => this.changeKontrakBulan(itemValue)}
                data = {pickerKontrakBulanItem.map((pickerKontrakBulanItem, index) => <Picker.Item label={pickerKontrakBulanItem.Descr} value={pickerKontrakBulanItem.Code} />)}
                enabled={this.state.disabledPeriodeKontrak ? false : true}
              />

              <Field
                name="kontrakTahun"
                component={renderFieldPicker}
                label="Periode Masa Kontrak (Tahun)"
                pickerSelected = {pickerKontrakTahunSelected}
                onValueChange = {(itemValue, itemIndex) => this.changeKontrakTahun(itemValue)}
                data = {pickerKontrakTahunItem.map((pickerKontrakTahunItem, index) => <Picker.Item label={pickerKontrakTahunItem.Descr} value={pickerKontrakTahunItem.Code} />)}
                enabled={this.state.disabledPeriodeKontrak ? false : true}
              />
              
              <View style={{marginTop:40}}/>

              <Field
                  name="jobAlamat"
                  type="text"
                  component={renderFieldTextArea}
                  label="Alamat Perusahaan"
                  rowSpan={4}
              />
              
              <Field
                  name="jobRt"
                  type="text"
                  component={renderField}
                  label="RT Perusahaan"
                  keyboardType="number-pad"
                  maxLength={3}
              />
              
              <Field
                  name="jobRw"
                  type="text"
                  component={renderField}
                  label="RW Perusahaan"
                  keyboardType="number-pad"
                  maxLength={3}
              />
              
              <View style={{marginTop:40}}/>
              
              <Field
                name="jobProvinsi"
                component={renderFieldPicker}
                label="Provinsi"
                pickerSelected = {pickerJobProvinsiSelected}
                onValueChange = {(itemValue, itemIndex) => this.changeProvinsi(itemValue)}
                data = {pickerJobProvinsiItem.map((pickerJobProvinsiItem, index) => <Picker.Item label={pickerJobProvinsiItem.Descr} value={pickerJobProvinsiItem.Code} />)}
              />

              { disableKotaKabupaten ? <DisabledForm title = "Kota Kabupaten"/> :
                <Field
                  name="jobKotaKabupaten"
                  component={renderFieldPicker}
                  label="Kota Kabupaten"
                  pickerSelected = {pickerJobKotaKabupatenSelected}
                  onValueChange = {(itemValue, itemIndex) => this.changeKotaKabupaten(itemValue)}
                  data = {pickerJobKotaKabupatenItem.map((pickerJobKotaKabupatenItem, index) => <Picker.Item label={pickerJobKotaKabupatenItem.Descr} value={pickerJobKotaKabupatenItem.Code} />)}
                />
              }

              { disableKecamatan ? <DisabledForm title = "Kecamatan"/> :
                <Field
                  name="jobKecamatan"
                  component={renderFieldPicker}
                  label="Kecamatan"
                  pickerSelected = {pickerJobKecamatanSelected}
                  onValueChange = {(itemValue, itemIndex) => this.changeKecamatan(itemValue)}
                  data = {pickerJobKecamatanItem.map((pickerJobKecamatanItem, index) => <Picker.Item label={pickerJobKecamatanItem.Descr} value={pickerJobKecamatanItem.Code} />)}
                />
              }

              { disableKelurahan ? <DisabledForm title = "Kelurahan"/>:
                <Field
                  name="jobKelurahan"
                  component={renderFieldPicker}
                  label="Kelurahan"
                  pickerSelected = {pickerJobKelurahanSelected}
                  onValueChange = {(itemValue, itemIndex) => this.changeKelurahan(itemValue)}
                  data = {pickerJobKelurahanItem.map((pickerJobKelurahanItem, index) => <Picker.Item label={pickerJobKelurahanItem.Descr} value={pickerJobKelurahanItem.Code} />)}
                />
              }
              
              <Field
                  name="jobKodePos"
                  type="text"
                  component={renderFieldDisabled}
                  label="Kode Pos"
              />
              
              <View style={{marginTop:40}}/>
              
              <Field
                  name="jobKodeArea"
                  type="text"
                  component={renderField}
                  label="Kode Area Telp Perusahaan"
                  keyboardType="number-pad"
                  maxLength={4}
              />
              
              <Field
                  name="jobTelp"
                  type="text"
                  component={renderField}
                  label="No Telp Perusahaan"
                  keyboardType="number-pad"
                  maxLength={10}
              />
              
              <Field
                  name="jobTelpEkstensi"
                  type="text"
                  component={renderField}
                  label="Ekstensi Telp Perusahaan (bila ada)"
                  keyboardType="number-pad"
                  maxLength={10}
              />
              
              <View style={{marginTop:40}}/>

              <Field
                name="statusUsaha"
                component={renderFieldPicker}
                label="Status Usaha"
                pickerSelected = {pickerOfficeOwnershipSelected}
                onValueChange = {(itemValue) => this.changeOfficeOwnership(itemValue)}
                data = {pickerOfficeOwnershipItem.map((pickerOfficeOwnershipItem) => <Picker.Item label={pickerOfficeOwnershipItem.Descr} value={pickerOfficeOwnershipItem.Code} />)}
              />

              <Field
                name="penghasilanPerbulan"
                type="text"
                component={renderField}
                label="Penghasilan Perbulan"
                keyboardType="number-pad"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />

            <View style={styles.ScrollViewBottom}/>  
          </Form>
        </Content>

        <HideWithKeyboard style={styles.buttonSideBottomContainer}>
          <TouchableOpacity style={styles.prevButton} onPress={previousPage}>
            <Text style={{color:'white',fontSize:16,fontFamily:FSAlbert}}>Kembali</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.nextButton} onPress={handleSubmit}>
            <Text style={{color:'white',fontSize:16,fontFamily:FSAlbert}}>Lanjut</Text>
          </TouchableOpacity>
        </HideWithKeyboard>
      </Container>
    )}
}

function mapStateToProps(state) {
    return {
      ddeJobCategory_Loading: state.ddeJobCategory.loading,
      ddeJobCategory_Result: state.ddeJobCategory.result,
      ddeJobCategory_Error: state.ddeJobCategory.error,
      ddeJobStatus_Loading: state.ddeJobStatus.loading,
      ddeJobStatus_Result: state.ddeJobStatus.result,
      ddeJobStatus_Error: state.ddeJobStatus.error,
      ddeJobType_Loading: state.ddeJobType.loading,
      ddeJobType_Result: state.ddeJobType.result,
      ddeJobType_Error: state.ddeJobType.error,
      ddeJobPosition_Loading: state.ddeJobPosition.loading,
      ddeJobPosition_Result: state.ddeJobPosition.result,
      ddeJobPosition_Error: state.ddeJobPosition.error,
      ddeIndustryType_Loading: state.ddeIndustryType.loading,
      ddeIndustryType_Result: state.ddeIndustryType.result,
      ddeIndustryType_Error: state.ddeIndustryType.error,
      ddeJobProvince_Loading: state.ddeJobProvince.loading,
      ddeJobProvince_Result: state.ddeJobProvince.result,
      ddeJobProvince_Error: state.ddeJobProvince.error,
      ddeJobCity_Loading: state.ddeJobCity.loading,
      ddeJobCity_Result: state.ddeJobCity.result,
      ddeJobCity_Error: state.ddeJobCity.error,
      ddeJobKecamatan_Loading: state.ddeJobKecamatan.loading,
      ddeJobKecamatan_Result: state.ddeJobKecamatan.result,
      ddeJobKecamatan_Error: state.ddeJobKecamatan.error,
      ddeJobKelurahan_Loading: state.ddeJobKelurahan.loading,
      ddeJobKelurahan_Result: state.ddeJobKelurahan.result,
      ddeJobKelurahan_Error: state.ddeJobKelurahan.error,
      ddeLamaUsahaTahun_Loading: state.ddeLamaUsahaTahun.loading,
      ddeLamaUsahaTahun_Result: state.ddeLamaUsahaTahun.result,
      ddeLamaUsahaTahun_Error: state.ddeLamaUsahaTahun.error,
      ddeLamaUsahaBulan_Loading: state.ddeLamaUsahaBulan.loading,
      ddeLamaUsahaBulan_Result: state.ddeLamaUsahaBulan.result,
      ddeLamaUsahaBulan_Error: state.ddeLamaUsahaBulan.error,
      ddeKontrakTahun_Loading: state.ddeKontrakTahun.loading,
      ddeKontrakTahun_Result: state.ddeKontrakTahun.result,
      ddeKontrakTahun_Error: state.ddeKontrakTahun.error,
      ddeKontrakBulan_Loading: state.ddeKontrakBulan.loading,
      ddeKontrakBulan_Result: state.ddeKontrakBulan.result,
      ddeKontrakBulan_Error: state.ddeKontrakBulan.error,
      ddeOfficeOwnership_Loading: state.ddeOfficeOwnership.loading,
      ddeOfficeOwnership_Result: state.ddeOfficeOwnership.result,
      ddeOfficeOwnership_Error: state.ddeOfficeOwnership.error,
      formFdeMpAdd: state.form.formFdeMpAdd
    };
  } 

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ 
        ddeJobCity,
        ddeJobKecamatan,
        ddeJobKelurahan,
        ddeJobType, 
        ddeJobPosition, 
        ddeIndustryType, 
        ddeLamaUsahaTahun, 
        ddeLamaUsahaBulan,
        ddeKontrakTahun, 
        ddeKontrakBulan,
        updateField: (form, field, newValue) => dispatch(change(form, field, newValue))
    }, dispatch);
}

FourthWizard =  reduxForm({
    form: 'formFdeMpAdd',
    destroyOnUnmount: false, // <------ preserve form data
    forceUnregisterOnUnmount: true,
    enableReinitialize: true,
    validate: FormFdeAddMpValidate
})(FourthWizard)

export default connect(
    mapStateToProps,
    matchDispatchToProps
)(FourthWizard);