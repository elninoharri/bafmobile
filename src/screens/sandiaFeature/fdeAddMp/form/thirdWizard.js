import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { Field, reduxForm, change } from 'redux-form'
import { renderField, renderFieldHidden, renderFieldPicker,renderFieldDatepickerIos, renderFieldDatepicker, renderFieldDisabled, renderFieldTextArea } from '../index'
import { Text, View, TouchableOpacity } from 'react-native';
import { Container, Spinner, Content, Form, Picker, ListItem, CheckBox } from 'native-base';
import styles from '../style';
import { HideWithKeyboard } from 'react-native-hide-with-keyboard';
import { FormFdeAddMpValidate } from "../../../../validates/FormFdeAddMpValidate"
import { ddeResidenceCity, ddeResidenceKecamatan, ddeResidenceKelurahan } from "../../../../actions/sandiaFeature/dataLengkap/dataLengkapWilayah";
import { substringCircum, substringSecondCircum, substringThirdCircum } from "../../../../utils/utilization";
import AsyncStorage from '@react-native-community/async-storage';
import DisabledForm from "../../../../components/multiPlatform/sandiaFeature/disabledForm";
import { FSAlbert } from '../../../../utils/fonts';

class ThirdWizard extends Component {
  constructor(props) {
    super(props)
    this.changeStatusRumah = this.changeStatusRumah.bind(this)
    this.changeProvinsi = this.changeProvinsi.bind(this)
    this.changeKotaKabupaten = this.changeKotaKabupaten.bind(this)
    this.changeKecamatan = this.changeKecamatan.bind(this)
    this.changeKelurahan = this.changeKelurahan.bind(this)
    this.changeMasaBerlakuSewa = this.changeMasaBerlakuSewa.bind(this)
    this.checkAddress = this.checkAddress.bind(this)
    this.state = {
      userAccess: '',
      userToken: '',
      pickerResidenceProvinsiItem: [{ Code: '', Descr: 'Silahkan Pilih'}],
      pickerResidenceProvinsiSelected: '',
      pickerResidenceKotaKabupatenItem: [{ Code: '', Descr: 'Silahkan Pilih'}],
      pickerResidenceKotaKabupatenSelected: '',
      pickerResidenceKecamatanItem: [{ Code: '', Descr: 'Silahkan Pilih'}],
      pickerResidenceKecamatanSelected: '',
      pickerResidenceKelurahanItem: [{ Code: '', Descr: 'Silahkan Pilih'}],
      pickerResidenceKelurahanSelected: '',
      pickerResidenceStatusRumahItem: [{ Code: '', Descr: 'Silahkan Pilih'}],
      pickerResidenceStatusRumahSelected: '',
      masaBerlakuSewa: '',
      disableKotaKabupaten: true,
      disableKecamatan: true,
      disableKelurahan: true,
      disableForm: false,
      checkedAddress: false
    }
  }

  componentDidMount = async () => {
    const { 
      formFdeMpAdd, 
      ddeResidenceCity, 
      ddeResidenceKecamatan, 
      ddeResidenceKelurahan, 
      ddeResidenceProvince_Result, 
      ddeHouseOwnership_Result 
    } = this.props;

    const jsonUserAccess = await AsyncStorage.getItem('userAccess');
    var userAccess = JSON.parse(jsonUserAccess)
    this.setState({ userAccess: userAccess })

    const userToken = await AsyncStorage.getItem('userToken');
    this.setState({ userToken: userToken })

    if (ddeResidenceProvince_Result.length > 0) {
      var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
      for (var i=0; i<ddeResidenceProvince_Result.length; i++) {
        arrayData.push({
          Code: ddeResidenceProvince_Result[i].Code + '^' + ddeResidenceProvince_Result[i].Descr,
          Descr: ddeResidenceProvince_Result[i].Descr
        })
      }
      this.setState({ pickerResidenceProvinsiItem: arrayData });
    }

    if (ddeHouseOwnership_Result.length > 0) {
      var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];
      for (var i=0; i<ddeHouseOwnership_Result.length; i++) {
        arrayData.push({
          Code: ddeHouseOwnership_Result[i].Code + '^' + ddeHouseOwnership_Result[i].Descr,
          Descr: ddeHouseOwnership_Result[i].Descr
        })
      }
      this.setState({ pickerResidenceStatusRumahItem: arrayData });
    }

    if (formFdeMpAdd.values) {
      if (formFdeMpAdd.values.checkAddress && formFdeMpAdd.values.checkAddress === '1') {
        this.setState({ 
          checkAddress: true,
          disableForm: true,
          pickerResidenceStatusRumahSelected: formFdeMpAdd.values.ktpStatusRumah ? formFdeMpAdd.values.ktpStatusRumah : this.state.pickerResidenceStatusRumahSelected,
          masaBerlakuSewa: formFdeMpAdd.values.ktpMasaBerlakuSewa ? formFdeMpAdd.values.ktpMasaBerlakuSewa : this.state.masaBerlakuSewa,
          pickerResidenceKelurahanSelected: formFdeMpAdd.values.ktpKelurahan ? formFdeMpAdd.values.ktpKelurahan : this.state.pickerResidenceKelurahanSelected
        })
        this.props.updateField('formFdeMpAdd', 'residenceAlamat', formFdeMpAdd.values.ktpAlamat)
        this.props.updateField('formFdeMpAdd', 'residenceRt', formFdeMpAdd.values.ktpRt)
        this.props.updateField('formFdeMpAdd', 'residenceRw', formFdeMpAdd.values.ktpRw)
        this.props.updateField('formFdeMpAdd', 'residenceProvinsi', formFdeMpAdd.values.ktpProvinsi)
        this.props.updateField('formFdeMpAdd', 'residenceKotaKabupaten', formFdeMpAdd.values.ktpKotaKabupaten)
        this.props.updateField('formFdeMpAdd', 'residenceKecamatan', formFdeMpAdd.values.ktpKecamatan)
        this.props.updateField('formFdeMpAdd', 'residenceKelurahan', formFdeMpAdd.values.ktpKelurahan)
        this.props.updateField('formFdeMpAdd', 'residenceKodePos', formFdeMpAdd.values.ktpKodePos)
        this.props.updateField('formFdeMpAdd', 'residenceStatusRumah', formFdeMpAdd.values.ktpStatusRumah)
        this.props.updateField('formFdeMpAdd', 'residenceMasaBerlakuSewa', formFdeMpAdd.values.ktpMasaBerlakuSewa)
        this.props.updateField('formFdeMpAdd', 'residenceLamaTinggalTahun', formFdeMpAdd.values.ktpLamaTinggalTahun)
        this.props.updateField('formFdeMpAdd', 'residenceLamaTinggalBulan', formFdeMpAdd.values.ktpLamaTinggalBulan)
      } else {
        this.setState({ 
          checkAddress: false,
          disableForm: false,
          pickerResidenceStatusRumahSelected: formFdeMpAdd.values.residenceStatusRumah ? formFdeMpAdd.values.residenceStatusRumah : this.state.pickerResidenceStatusRumahSelected,
          masaBerlakuSewa: formFdeMpAdd.values.residenceMasaBerlakuSewa ? formFdeMpAdd.values.residenceMasaBerlakuSewa : this.state.masaBerlakuSewa,
          pickerResidenceKelurahanSelected: formFdeMpAdd.values.residenceKelurahan ? formFdeMpAdd.values.residenceKelurahan : this.state.pickerResidenceKelurahanSelected
        })
      }
    }
    
    if (formFdeMpAdd.values && formFdeMpAdd.values.residenceProvinsi) {
      this.setState({ pickerResidenceProvinsiSelected: formFdeMpAdd.values.checkAddress && formFdeMpAdd.values.checkAddress === '1' ? 
        formFdeMpAdd.values.ktpProvinsi : formFdeMpAdd.values.residenceProvinsi })

      if (formFdeMpAdd.values.residenceProvinsi !== '') {
        ddeResidenceCity({ProvinceId: substringCircum(formFdeMpAdd.values.residenceProvinsi)}, userToken)
      }
    }

    if (formFdeMpAdd.values && formFdeMpAdd.values.residenceKotaKabupaten) {
      this.setState({ pickerResidenceKotaKabupatenSelected: formFdeMpAdd.values.checkAddress && formFdeMpAdd.values.checkAddress === '1' ? 
        formFdeMpAdd.values.ktpKotaKabupaten : formFdeMpAdd.values.residenceKotaKabupaten })

      if (formFdeMpAdd.values.residenceKotaKabupaten !== '') {
        ddeResidenceKecamatan({CityId: substringCircum(formFdeMpAdd.values.residenceKotaKabupaten)}, userToken)
      }
    }

    if (formFdeMpAdd.values && formFdeMpAdd.values.residenceKecamatan) {
      this.setState({ pickerResidenceKecamatanSelected: formFdeMpAdd.values.checkAddress && formFdeMpAdd.values.checkAddress === '1' ? 
        formFdeMpAdd.values.ktpKecamatan : formFdeMpAdd.values.residenceKecamatan })

      if (formFdeMpAdd.values.residenceKecamatan !== '') {
        ddeResidenceKelurahan({KecamatanId: substringCircum(formFdeMpAdd.values.residenceKecamatan)}, userToken)
      }
    }
  }

  componentDidUpdate = async (prevProps, prevState) => {
    const { ddeResidenceCity_Result, ddeResidenceKecamatan_Result, ddeResidenceKelurahan_Result, formFdeMpAdd } = this.props

    if (ddeResidenceCity_Result.length && prevProps.ddeResidenceCity_Result !== ddeResidenceCity_Result) {
      var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];

      for (var i=0; i<ddeResidenceCity_Result.length; i++) {
        arrayData.push({
          Code: ddeResidenceCity_Result[i].Code + '^' + ddeResidenceCity_Result[i].Descr,
          Descr: ddeResidenceCity_Result[i].Descr
        })
      }
      this.setState({ 
        pickerResidenceKotaKabupatenItem: arrayData,
        disableKotaKabupaten: false
      });
    }

    if (ddeResidenceKecamatan_Result.length && prevProps.ddeResidenceKecamatan_Result !== ddeResidenceKecamatan_Result) {
      var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];

      for (var i=0; i<ddeResidenceKecamatan_Result.length; i++) {
        arrayData.push({
          Code: ddeResidenceKecamatan_Result[i].Code + '^' + ddeResidenceKecamatan_Result[i].Descr,
          Descr: ddeResidenceKecamatan_Result[i].Descr
        })
      }
      this.setState({ 
        pickerResidenceKecamatanItem: arrayData,
        disableKecamatan: false
      });
    }

    if (ddeResidenceKelurahan_Result.length && prevProps.ddeResidenceKelurahan_Result !== ddeResidenceKelurahan_Result) {
      var arrayData = [{ Code: '', Descr: 'Silahkan Pilih' }];

      for (var i=0; i<ddeResidenceKelurahan_Result.length; i++) {
        arrayData.push({
          Code: ddeResidenceKelurahan_Result[i].Code + '^' + ddeResidenceKelurahan_Result[i].Descr + '^' + ddeResidenceKelurahan_Result[i].KodePos,
          Descr: ddeResidenceKelurahan_Result[i].Descr
        })
      }
      this.setState({ 
        pickerResidenceKelurahanItem: arrayData,
        disableKelurahan: false
      });
    }
  }

  checkAddress(){
    const { 
      formFdeMpAdd, 
      ddeResidenceCity, 
      ddeResidenceKecamatan, 
      ddeResidenceKelurahan 
    } = this.props;

    if (formFdeMpAdd.values.checkAddress && formFdeMpAdd.values.checkAddress === '1') {
      this.props.updateField('formFdeMpAdd', 'checkAddress', '0')
      this.props.updateField('formFdeMpAdd', 'residenceAlamat', '')
      this.props.updateField('formFdeMpAdd', 'residenceRt', '')
      this.props.updateField('formFdeMpAdd', 'residenceRw', '')
      this.props.updateField('formFdeMpAdd', 'residenceProvinsi', this.state.pickerResidenceProvinsiDefault)
      this.props.updateField('formFdeMpAdd', 'residenceKotaKabupaten', '')
      this.props.updateField('formFdeMpAdd', 'residenceKecamatan', '')
      this.props.updateField('formFdeMpAdd', 'residenceKelurahan', '')
      this.props.updateField('formFdeMpAdd', 'residenceKodePos', '')
      this.props.updateField('formFdeMpAdd', 'residenceStatusRumah', this.state.pickerResidenceStatusRumahDefault)
      this.props.updateField('formFdeMpAdd', 'residenceMasaBerlakuSewa', '')
      this.props.updateField('formFdeMpAdd', 'residenceLamaTinggalTahun', '')
      this.props.updateField('formFdeMpAdd', 'residenceLamaTinggalBulan', '')
      
      this.setState({
        checkAddress: false,
        disableKotaKabupaten: true,
        disableKecamatan: true,
        disableKelurahan: true,
        pickerResidenceProvinsiSelected: '',
        pickerResidenceKotaKabupatenSelected: '',
        pickerResidenceKecamatanSelected: '',
        pickerResidenceKelurahanSelected: '',
        pickerResidenceStatusRumahSelected: '',
        masaBerlakuSewa: '',
        disableForm: false
      })
    } else {
      this.props.updateField('formFdeMpAdd', 'checkAddress', '1')
      this.props.updateField('formFdeMpAdd', 'residenceAlamat', formFdeMpAdd.values.ktpAlamat)
      this.props.updateField('formFdeMpAdd', 'residenceRt', formFdeMpAdd.values.ktpRt)
      this.props.updateField('formFdeMpAdd', 'residenceRw', formFdeMpAdd.values.ktpRw)
      this.props.updateField('formFdeMpAdd', 'residenceProvinsi', formFdeMpAdd.values.ktpProvinsi)
      this.props.updateField('formFdeMpAdd', 'residenceKotaKabupaten', formFdeMpAdd.values.ktpKotaKabupaten)
      this.props.updateField('formFdeMpAdd', 'residenceKecamatan', formFdeMpAdd.values.ktpKecamatan)
      this.props.updateField('formFdeMpAdd', 'residenceKelurahan', formFdeMpAdd.values.ktpKelurahan)
      this.props.updateField('formFdeMpAdd', 'residenceKodePos', formFdeMpAdd.values.ktpKodePos)
      this.props.updateField('formFdeMpAdd', 'residenceStatusRumah', formFdeMpAdd.values.ktpStatusRumah)
      this.props.updateField('formFdeMpAdd', 'residenceMasaBerlakuSewa', formFdeMpAdd.values.ktpMasaBerlakuSewa)
      this.props.updateField('formFdeMpAdd', 'residenceLamaTinggalTahun', formFdeMpAdd.values.ktpLamaTinggalTahun)
      this.props.updateField('formFdeMpAdd', 'residenceLamaTinggalBulan', formFdeMpAdd.values.ktpLamaTinggalBulan)
      this.setState({
        checkAddress: true,
        pickerResidenceProvinsiSelected: formFdeMpAdd.values.ktpProvinsi,
        pickerResidenceKotaKabupatenSelected: formFdeMpAdd.values.ktpKotaKabupaten,
        pickerResidenceKecamatanSelected: formFdeMpAdd.values.ktpKecamatan,
        pickerResidenceKelurahanSelected: formFdeMpAdd.values.ktpKelurahan,
        pickerResidenceStatusRumahSelected: formFdeMpAdd.values.ktpStatusRumah,
        masaBerlakuSewa: formFdeMpAdd.values.ktpMasaBerlakuSewa,
        disableForm: true
      })
      ddeResidenceCity({ProvinceId: substringCircum(formFdeMpAdd.values.ktpProvinsi)})
      ddeResidenceKecamatan({CityId: substringCircum(formFdeMpAdd.values.ktpKotaKabupaten)})
      ddeResidenceKelurahan({KecamatanId: substringCircum(formFdeMpAdd.values.ktpKecamatan)})
    }
  }

  changeProvinsi(value){
    const { ddeResidenceCity } = this.props
    this.setState({
      pickerResidenceProvinsiSelected: value,
      pickerResidenceKotaKabupatenSelected: '',
      pickerResidenceKecamatanSelected: '',
      pickerResidenceKelurahanSelected: '',
      disableKotaKabupaten: true,
      disableKecamatan: true,
      disableKelurahan: true
    })
    this.props.updateField('formFdeMpAdd', 'residenceKotaKabupaten', '')
    this.props.updateField('formFdeMpAdd', 'residenceKecamatan', '')
    this.props.updateField('formFdeMpAdd', 'residenceKelurahan', '')
    this.props.updateField('formFdeMpAdd', 'residenceKodePos', '')
    if (value == '') {
      this.props.updateField('formFdeMpAdd', 'residenceProvinsi', '')
    } else {
      this.props.updateField('formFdeMpAdd', 'residenceProvinsi', value)
      ddeResidenceCity({ProvinceId: substringCircum(value)})
    }
  }

  changeKotaKabupaten(value) {
    const { ddeResidenceKecamatan } = this.props
    this.setState({
      pickerResidenceKotaKabupatenSelected: value,
      pickerResidenceKecamatanSelected: '',
      pickerResidenceKelurahanSelected: '',
      disableKecamatan: true,
      disableKelurahan: true,
    })
    this.props.updateField('formFdeMpAdd', 'residenceKecamatan', '')
    this.props.updateField('formFdeMpAdd', 'residenceKelurahan', '')
    this.props.updateField('formFdeMpAdd', 'residenceKodePos', '')
    
    if(value == ''){
      this.props.updateField('formFdeMpAdd', 'residenceKotaKabupaten', '')
      this.setState({
        disableKecamatan: true,
        disableKelurahan: true
      })
    } else {
      this.props.updateField('formFdeMpAdd', 'residenceKotaKabupaten', value)
      ddeResidenceKecamatan({CityId: substringCircum(value)})
    }
  }

  changeKecamatan(value) {
    const { ddeResidenceKelurahan } = this.props
    this.setState({
      pickerResidenceKecamatanSelected: value,
      pickerResidenceKelurahanSelected: '',
      disableKelurahan: true,
    })
    this.props.updateField('formFdeMpAdd', 'residenceKelurahan', '')
    this.props.updateField('formFdeMpAdd', 'residenceKodePos', '')
    
    if(value == ''){
      this.props.updateField('formFdeMpAdd', 'residenceKecamatan', '')
      this.setState({
        disableKelurahan: true
      })
    } else {
      this.props.updateField('formFdeMpAdd', 'residenceKecamatan', value)
      ddeResidenceKelurahan({KecamatanId: substringCircum(value)})
    }
  }

  changeKelurahan(value) {
    this.setState({ pickerResidenceKelurahanSelected: value })

    if(value == ''){
      this.props.updateField('formFdeMpAdd', 'residenceKelurahan', '')
      this.props.updateField('formFdeMpAdd', 'residenceKodePos', '')
    } else {
      this.props.updateField('formFdeMpAdd', 'residenceKelurahan', value)
      this.props.updateField('formFdeMpAdd', 'residenceKodePos', substringThirdCircum(value))
    }
  }

  changeMasaBerlakuSewa(value){
    this.setState({ masaBerlakuSewa: value })
    this.props.updateField('formFdeMpAdd', 'residenceMasaBerlakuSewa', value)
  }

  changeStatusRumah(value){
    this.setState({ pickerResidenceStatusRumahSelected: value })
    if (value == '') {
      this.props.updateField('formFdeMpAdd', 'residenceStatusRumah', this.state.pickerResidenceStatusRumahItem.Code)
    } else {
      this.props.updateField('formFdeMpAdd', 'residenceStatusRumah', value)
    }
  }

  render(){
    const { handleSubmit, previousPage,openDatePicker,toggleModal } = this.props
    const { pickerResidenceStatusRumahItem, pickerResidenceStatusRumahSelected, pickerResidenceProvinsiItem, pickerResidenceProvinsiSelected, 
      pickerResidenceKotaKabupatenItem, pickerResidenceKotaKabupatenSelected, pickerResidenceKecamatanItem, pickerResidenceKecamatanSelected,
      pickerResidenceKelurahanItem, pickerResidenceKelurahanSelected, disableKotaKabupaten, disableKecamatan, disableKelurahan } = this.state

    return (
      <Container>
        <Content disableKBDismissScroll={true} style={styles.TWContainer}>
          <Form>
            
            <ListItem style={styles.TWListItem}>
              <CheckBox checked={this.state.checkAddress} style={styles.TWCheckBox} onPress = {() => this.checkAddress()} />  
              <Text style={styles.TWDomisiliText}>Domisili sama dengan KTP ? </Text>
            </ListItem>
            
            <Field
              name="checkAddress"
              type="text"
              component={renderFieldHidden}
              label="Alamat Sesuai KTP"
            />
            
            <View style={{marginTop:40}}/>

            <Field
              name="residenceAlamat"
              type="text"
              component={renderFieldTextArea}
              label="Alamat Domisili"
              rowSpan={4}
              disabled={this.state.disableForm ? true : false}
            />

            <Field
              name="residenceRt"
              type="text"
              component={renderField}
              label="RT"
              keyboardType="number-pad"
              maxLength={3}
              editable={this.state.disableForm ? false : true}
            />

            <Field
              name="residenceRw"
              type="text"
              component={renderField}
              label="RW"
              keyboardType="number-pad"
              maxLength={3}
              editable={this.state.disableForm ? false : true}
            />

            <View style={{marginTop:40}}/>

            <Field
              name="residenceProvinsi"
              component={renderFieldPicker}
              label="Provinsi"
              pickerSelected = {pickerResidenceProvinsiSelected}
              onValueChange = {(itemValue, itemIndex) => this.changeProvinsi(itemValue)}
              data = {pickerResidenceProvinsiItem.map((pickerResidenceProvinsiItem, index) => <Picker.Item label={pickerResidenceProvinsiItem.Descr} value={pickerResidenceProvinsiItem.Code} />)}
              enabled={this.state.disableForm ? false : true}
            />

            { disableKotaKabupaten ? 
              <DisabledForm title = "Kota Kabupaten"/> :
              <Field
                name="residenceKotaKabupaten"
                component={renderFieldPicker}
                label="Kota Kabupaten"
                pickerSelected = {pickerResidenceKotaKabupatenSelected}
                onValueChange = {(itemValue, itemIndex) => this.changeKotaKabupaten(itemValue)}
                data = {pickerResidenceKotaKabupatenItem.map((pickerResidenceKotaKabupatenItem, index) => <Picker.Item label={pickerResidenceKotaKabupatenItem.Descr} value={pickerResidenceKotaKabupatenItem.Code} />)}
                enabled={this.state.disableForm ? false : true}
              />
            }

            { disableKecamatan ? 
              <DisabledForm title = "Kecamatan"/> :
              <Field
                name="residenceKecamatan"
                component={renderFieldPicker}
                label="Kecamatan"
                pickerSelected = {pickerResidenceKecamatanSelected}
                onValueChange = {(itemValue, itemIndex) => this.changeKecamatan(itemValue)}
                data = {pickerResidenceKecamatanItem.map((pickerResidenceKecamatanItem, index) => <Picker.Item label={pickerResidenceKecamatanItem.Descr} value={pickerResidenceKecamatanItem.Code} />)}
                enabled={this.state.disableForm ? false : true}
              />
            }

            { disableKelurahan ? 
              <DisabledForm title = "Kelurahan"/>:
              <Field
                name="residenceKelurahan"
                component={renderFieldPicker}
                label="Kelurahan"
                pickerSelected = {pickerResidenceKelurahanSelected}
                onValueChange = {(itemValue, itemIndex) => this.changeKelurahan(itemValue)}
                data = {pickerResidenceKelurahanItem.map((pickerResidenceKelurahanItem, index) => <Picker.Item label={pickerResidenceKelurahanItem.Descr} value={pickerResidenceKelurahanItem.Code} />)}
                enabled={this.state.disableForm ? false : true}
              />
            }

            <Field
              name="residenceKodePos"
              type="text"
              component={renderFieldDisabled}
              label="Kode Pos"
            />

            <Field
              name="residenceStatusRumah"
              component={renderFieldPicker}
              label="Status Rumah"
              pickerSelected = {pickerResidenceStatusRumahSelected}
              onValueChange = {(itemValue, itemIndex) => this.changeStatusRumah(itemValue)}
              data = {pickerResidenceStatusRumahItem.map((pickerResidenceStatusRumahItem, index) => <Picker.Item label={pickerResidenceStatusRumahItem.Descr} value={pickerResidenceStatusRumahItem.Code} />)}
              enabled={this.state.disableForm ? false : true}
            />

            <View style={{marginTop:40}}/>
           
            {this.state.disableForm ? 
              <Field
                name="residenceMasaBerlakuSewa"
                type="text"
                component={renderFieldDisabled}
                label="Masa Berlaku Sewa/Kontrak (bila ada)"
              /> :
              <Field
                name="residenceMasaBerlakuSewa"
                component={Platform.OS == 'android' ? renderFieldDatepicker : renderFieldDatepickerIos} 
                openModal={() => openDatePicker()}
                closeModal={() => toggleModal()}
                visible = {this.props.visible}
                placeholder="Masa Berlaku Sewa/Kontrak (bila ada)"
                formatdate="YYYY-MM-DD"
                date={this.state.masaBerlakuSewa}
                onDateChange={(date) => {this.changeMasaBerlakuSewa(date)}}
              />
            }

            <Field
              name="residenceLamaTinggalTahun"
              type="text"
              component={renderField}
              label="Lama Tinggal (Tahun)"
              keyboardType="number-pad"
              maxLength={3}
              editable={this.state.disableForm ? false : true}
            />

            <Field
              name="residenceLamaTinggalBulan"
              type="text"
              component={renderField}
              label="Lama Tinggal (Bulan)"
              keyboardType="number-pad"
              maxLength={2}
              editable={this.state.disableForm ? false : true}
            />

            <View style={styles.ScrollViewBottom} />
          </Form>
        </Content>
        <HideWithKeyboard style={styles.buttonSideBottomContainer}>
          <TouchableOpacity style={styles.prevButton} onPress={previousPage}>
            <Text style={{color:'white',fontSize:16,fontFamily:FSAlbert}}>Kembali</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.nextButton} onPress={handleSubmit}>
            <Text style={{color:'white',fontSize:16,fontFamily:FSAlbert}}>Lanjut</Text>
          </TouchableOpacity>
        </HideWithKeyboard>
      </Container>
    )
  }
}

function mapStateToProps(state) {
  return {
    ddeHouseOwnership_Loading: state.ddeHouseOwnership.loading,
    ddeHouseOwnership_Result: state.ddeHouseOwnership.result,
    ddeHouseOwnership_Error: state.ddeHouseOwnership.error,
    ddeResidenceProvince_Loading: state.ddeResidenceProvince.loading,
    ddeResidenceProvince_Result: state.ddeResidenceProvince.result,
    ddeResidenceProvince_Error: state.ddeResidenceProvince.error,
    ddeResidenceCity_Loading: state.ddeResidenceCity.loading,
    ddeResidenceCity_Result: state.ddeResidenceCity.result,
    ddeResidenceCity_Error: state.ddeResidenceCity.error,
    ddeResidenceKecamatan_Loading: state.ddeResidenceKecamatan.loading,
    ddeResidenceKecamatan_Result: state.ddeResidenceKecamatan.result,
    ddeResidenceKecamatan_Error: state.ddeResidenceKecamatan.error,
    ddeResidenceKelurahan_Loading: state.ddeResidenceKelurahan.loading,
    ddeResidenceKelurahan_Result: state.ddeResidenceKelurahan.result,
    ddeResidenceKelurahan_Error: state.ddeResidenceKelurahan.error,
    formFdeMpAdd: state.form.formFdeMpAdd
  };
} 

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ 
    ddeResidenceCity,
    ddeResidenceKecamatan,
    ddeResidenceKelurahan,
    updateField: (form, field, newValue) => dispatch(change(form, field, newValue))
  }, dispatch);
}

ThirdWizard =  reduxForm({
  form: 'formFdeMpAdd',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true,
  enableReinitialize: true,
  validate: FormFdeAddMpValidate
})(ThirdWizard)

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(ThirdWizard);