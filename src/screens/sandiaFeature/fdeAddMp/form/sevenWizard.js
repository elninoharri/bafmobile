import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Field, reduxForm, change} from 'redux-form';
import {renderFieldHidden} from '../index';
import {Text, View, TouchableOpacity, Image, Platform} from 'react-native';
import {Container, Content, Form} from 'native-base';
import styles from '../style';
import {HideWithKeyboard} from 'react-native-hide-with-keyboard';
import {FormFdeAddMpValidate} from '../../../../validates/FormFdeAddMpValidate';
import ImgToBase64 from 'react-native-image-base64';
import ImageResizer from 'react-native-image-resizer';
import ImageView from 'react-native-image-view';
import CustomAlertComponent from '../../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Spinner from 'react-native-loading-spinner-overlay';
import {FSAlbert} from '../../../../utils/fonts';
import UploadKtpAndroid from '../../../camera/android/uploadKtp';
import UploadFotoKonsumenAndroid from '../../../camera/android/uploadFotoKonsumen';
import UploadKtpIos from '../../../camera/ios/uploadKtp';
import UploadFotoKonsumenIos from '../../../camera/ios/uploadFotoKonsumen';

class SevenWizard extends Component {
  constructor(props) {
    super(props);
    this.imagePickerKtp = this.imagePickerKtp.bind(this);
    this.imagePickerDiri = this.imagePickerDiri.bind(this);

    this.state = {
      disableButtonUpload: false,
      uploadKtp: false,
      uploadDiri: false,
      uploadKtpView: false,
      uploadDiriView: false,
      isImageViewVisibleKtp: false,
      isImageViewVisibleDiri: false,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      disabledOtpButton: false,
      validate: false,
      detailUser: false,
    };
  }

  componentDidMount = async () => {
    const {formFdeMpAdd} = this.props;
    if (formFdeMpAdd.values) {
      console.log(formFdeMpAdd.values);
      this.setState({
        uploadKtp: formFdeMpAdd.values.uploadKtp
          ? formFdeMpAdd.values.uploadKtp
          : false,
        uploadDiri: formFdeMpAdd.values.uploadDiri
          ? formFdeMpAdd.values.uploadDiri
          : false,
      });
    }
  };

  componentDidUpdate = async () => {
    const {} = this.props;
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  imagePickerKtp() {
    this.props.enableCameraUploadKtp();
  }

  clearKtp() {
    this.setState({
      uploadKtp: false,
    });
    this.props.updateField('formFdeMpAdd', 'uploadKtp', '');
  }

  imagePickerDiri() {
    this.props.enableCameraUploadFotoKonsumen();
  }

  clearDiri() {
    this.setState({
      uploadDiri: false,
    });
    this.props.updateField('formFdeMpAdd', 'uploadDiri', '');
  }

  imageView = (uri) => [
    {
      source: {
        uri: uri,
      },
    },
  ];

  ViewKtp = (image) => {
    return (
      <ImageView
        currentImage={image}
        images={this.imageView(image)}
        imageIndex={0}
        isVisible={this.state.isImageViewVisibleKtp}
        animationType="fade"
        onClose={() => this.setState({isImageViewVisibleKtp: false})}
        isSwipeCloseEnabled={true}
        isTapZoomEnabled={true}
        isPinchZoomEnabled={true}
      />
    );
  };

  ViewDiri = (image) => {
    return (
      <ImageView
        currentImage={image}
        images={this.imageView(image)}
        imageIndex={0}
        isVisible={this.state.isImageViewVisibleDiri}
        animationType="fade"
        onClose={() => this.setState({isImageViewVisibleDiri: false})}
        isSwipeCloseEnabled={true}
        isTapZoomEnabled={true}
        isPinchZoomEnabled={true}
      />
    );
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  goBackKtp = (url) => {
    ImageResizer.createResizedImage(url, 2000, 2000, 'JPEG', 50)
      .then((response) => {
        var source = Platform.OS === 'android' ? response.uri : response.path;
        ImgToBase64.getBase64String(source)
          .then((base64String) => {
            this.setState({
              uploadKtp: 'data:image/jpeg;base64,' + base64String,
            });
            this.props.sendUploadKtp(this.state.uploadKtp, response);
            this.props.updateField('formFdeMpAdd', 'uploadKtp', 'done');
          })
          .catch((err) =>
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            }),
          );
      })
      .catch((err) => {
        handleAlert({
          alertShowed: true,
          alertMessage: err.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      });
    this.props.disableCameraUploadKtp();
  };

  goBackFotoKonsumen = (url) => {
    ImageResizer.createResizedImage(url, 2000, 2000, 'JPEG', 50)
      .then((response) => {
        var source = Platform.OS === 'android' ? response.uri : response.path;
        ImgToBase64.getBase64String(source)
          .then((base64String) => {
            this.setState({
              uploadDiri: 'data:image/jpeg;base64,' + base64String,
            });
            this.props.sendUploadDiri(this.state.uploadDiri, response);
            this.props.updateField('formFdeMpAdd', 'uploadDiri', 'done');
          })
          .catch((err) =>
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            }),
          );
      })
      .catch((err) => {
        handleAlert({
          alertShowed: true,
          alertMessage: err.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      });
    this.props.disableCameraUploadFotoKonsumen();
  };

  goBackFap = (url) => {
    ImageResizer.createResizedImage(url, 2000, 2000, 'JPEG', 50)
      .then((response) => {
        var source = Platform.OS === 'android' ? response.uri : response.path;
        ImgToBase64.getBase64String(source)
          .then((base64String) => {
            this.setState({
              uploadFap: 'data:image/jpeg;base64,' + base64String,
            });
            this.props.sendUploadFap(this.state.uploadFap);
            this.props.updateField('formFdeMpAdd', 'uploadFap', 'done');
          })
          .catch((err) =>
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            }),
          );
      })
      .catch((err) => {
        handleAlert({
          alertShowed: true,
          alertMessage: err.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      });
    this.props.disableCameraUploadFap();
  };

  goBackPpbk = (url) => {
    ImageResizer.createResizedImage(url, 2000, 2000, 'JPEG', 50)
      .then((response) => {
        var source = Platform.OS === 'android' ? response.uri : response.path;
        ImgToBase64.getBase64String(source)
          .then((base64String) => {
            this.setState({
              uploadPpbk: 'data:image/jpeg;base64,' + base64String,
            });
            this.props.sendUploadPpbk(this.state.uploadPpbk);
            this.props.updateField('formFdeMpAdd', 'uploadPpbk', 'done');
          })
          .catch((err) =>
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            }),
          );
      })
      .catch((err) => {
        handleAlert({
          alertShowed: true,
          alertMessage: err.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      });
    this.props.disableCameraUploadPpbk();
  };

  cancelUploadKtp = async () => {
    this.props.disableCameraUploadKtp();
  };

  cancelUploadFotoKonsumen = async () => {
    this.props.disableCameraUploadFotoKonsumen();
  };

  cancelUploadFap = async () => {
    this.props.disableCameraUploadFap();
  };

  cancelUploadPpbk = async () => {
    this.props.disableCameraUploadPpbk();
  };

  render() {
    const {
      handleSubmit,
      previousPage,
      cameraUploadKtpEnable,
      cameraUploadFotoKonsumenEnable,
    } = this.props;
    const {} = this.state;

    if (cameraUploadKtpEnable && Platform.OS === 'android') {
      return (
        <UploadKtpAndroid
          goBack={(url) => this.goBackKtp(url)}
          cancelUpload={() => this.cancelUploadKtp()}
        />
      );
    }
    if (cameraUploadFotoKonsumenEnable && Platform.OS === 'android') {
      return (
        <UploadFotoKonsumenAndroid
          goBack={(url) => this.goBackFotoKonsumen(url)}
          cancelUpload={() => this.cancelUploadFotoKonsumen()}
        />
      );
    }

    if (cameraUploadKtpEnable && Platform.OS !== 'android') {
      return (
        <UploadKtpIos
          goBack={(url) => this.goBackKtp(url)}
          cancelUpload={() => this.cancelUploadKtp()}
        />
      );
    }
    if (cameraUploadFotoKonsumenEnable && Platform.OS !== 'android') {
      return (
        <UploadFotoKonsumenIos
          goBack={(url) => this.goBackFotoKonsumen(url)}
          cancelUpload={() => this.cancelUploadFotoKonsumen()}
        />
      );
    } else {
      return (
        <Container>
          {this.showAlert()}
          <Content
            disableKBDismissScroll={true}
            style={{
              height: '75%',
              backgroundColor: 'white',
              width: '100%',
              alignSelf: 'center',
              paddingHorizontal: '4%',
            }}>
            <Form>
              <View
                style={{
                  marginTop: 10,
                  borderBottomWidth: 2,
                  borderBottomColor: 'grey',
                  paddingBottom: 10,
                }}>
                <Text
                  style={{
                    color: 'grey',
                    fontSize: 18,
                    fontWeight: 'bold',
                    fontFamily: FSAlbert,
                  }}>
                  Data Upload
                </Text>
              </View>

              {this.state.uploadKtp ? (
                <TouchableOpacity
                  style={styles.CLContainer}
                  onPress={() => {
                    this.setState({isImageViewVisibleKtp: true});
                  }}>
                  <Image
                    source={{uri: this.state.uploadKtp}}
                    style={styles.CLImage}
                  />
                  <TouchableOpacity
                    style={styles.CLDeleteImage}
                    onPress={() => this.clearKtp()}>
                    <Text style={styles.CLDeleteImageIcon}>X</Text>
                  </TouchableOpacity>
                </TouchableOpacity>
              ) : null}

              <TouchableOpacity
                style={
                  this.state.disableButtonUpload || this.state.uploadKtp
                    ? styles.ButtonDisabled
                    : styles.verificationValidationButton
                }
                disabled={
                  this.state.disableButtonUpload || this.state.uploadKtp
                    ? true
                    : false
                }
                onPress={this.imagePickerKtp}>
                <Text
                  style={{color: 'white', fontSize: 16, fontFamily: FSAlbert}}>
                  Upload Foto KTP
                </Text>
              </TouchableOpacity>

              <Field
                name="uploadKtp"
                type="hidden"
                component={renderFieldHidden}
              />

              {this.state.uploadDiri ? (
                <TouchableOpacity
                  style={styles.CLContainer}
                  onPress={() => {
                    this.setState({isImageViewVisibleDiri: true});
                  }}>
                  <Image
                    source={{uri: this.state.uploadDiri}}
                    style={styles.CLImage}
                  />
                  <TouchableOpacity
                    style={styles.CLDeleteImage}
                    onPress={() => this.clearDiri()}>
                    <Text style={styles.CLDeleteImageIcon}>X</Text>
                  </TouchableOpacity>
                </TouchableOpacity>
              ) : null}

              <TouchableOpacity
                style={
                  this.state.disableButtonUpload || this.state.uploadDiri
                    ? styles.ButtonDisabled
                    : styles.verificationValidationButton
                }
                disabled={
                  this.state.disableButtonUpload || this.state.uploadDiri
                    ? true
                    : false
                }
                onPress={this.imagePickerDiri}>
                <Text
                  style={{color: 'white', fontSize: 16, fontFamily: FSAlbert}}>
                  Upload Foto Konsumen
                </Text>
              </TouchableOpacity>

              <Field
                name="uploadDiri"
                type="hidden"
                component={renderFieldHidden}
              />

              {this.ViewKtp(this.state.uploadKtp)}
              {this.ViewDiri(this.state.uploadDiri)}
            </Form>
          </Content>

          <HideWithKeyboard style={styles.buttonSideBottomContainer}>
            <TouchableOpacity style={styles.prevButton} onPress={previousPage}>
              <Text
                style={{color: 'white', fontSize: 16, fontFamily: FSAlbert}}>
                Kembali
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.nextButton} onPress={handleSubmit}>
              <Text
                style={{color: 'white', fontSize: 16, fontFamily: FSAlbert}}>
                Submit
              </Text>
            </TouchableOpacity>
          </HideWithKeyboard>
        </Container>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    formFdeMpAdd: state.form.formFdeMpAdd,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
    },
    dispatch,
  );
}

SevenWizard = reduxForm({
  form: 'formFdeMpAdd',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true,
  enableReinitialize: true,
  validate: FormFdeAddMpValidate,
})(SevenWizard);

export default connect(mapStateToProps, matchDispatchToProps)(SevenWizard);
