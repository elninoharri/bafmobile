import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Field, reduxForm, change, formValueSelector} from 'redux-form';
import {
  renderField,
  renderFieldHidden,
  renderFieldPicker,
  renderFieldDatepicker,
  renderFieldDisabled,
  renderFieldPlafond,
} from '../index';
import {
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  BackHandler,
  Platform,
} from 'react-native';
import {
  Container,
  Content,
  Form,
  Picker,
  ListItem,
  CheckBox,
} from 'native-base';
import styles from '../style';
import {HideWithKeyboard} from 'react-native-hide-with-keyboard';
import {FormFdeAddMpValidate} from '../../../../validates/FormFdeAddMpValidate';
import {
  ddeMerk,
  ddeSchemeId,
  ddeFinancialData,
} from '../../../../actions/sandiaFeature/dataLengkap/dataLengkapTransaksi';
import {
  substringCircum,
  splitDot,
  substringSecondCircum,
  substringFifthCircum,
  substringThirdCircum,
  substringFourthCircum,
  normalizeCurrency,
  fdeFormatCurrency,
} from '../../../../utils/utilization';
import {LOB} from '../../../../utils/constant';
import AsyncStorage from '@react-native-community/async-storage';
// import ImagePicker from 'react-native-image-picker';
import ImgToBase64 from 'react-native-image-base64';
import ImageResizer from 'react-native-image-resizer';
import ImageView from 'react-native-image-view';
import DisabledForm from '../../../../components/multiPlatform/sandiaFeature/disabledForm';
import CustomAlertComponent from '../../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Spinner from 'react-native-loading-spinner-overlay';
import {otpSms, otpValidate} from '../../../../actions';
import NetInfo from '@react-native-community/netinfo';
import Toast, {DURATION} from 'react-native-easy-toast';
import {
  mpMappingRisk,
  mpInstCalculation,
} from '../../../../actions/sandiaFeature/dataAwal';
import {FSAlbert, FSAlbertBold} from '../../../../utils/fonts';
import UploadKtpAndroid from '../../../camera/android/uploadKtp';
import UploadFotoKonsumenAndroid from '../../../camera/android/uploadFotoKonsumen';
import UploadFapAndroid from '../../../camera/android/uploadFap';
import UploadPpbkAndroid from '../../../camera/android/uploadPpbk';

import UploadKtpIos from '../../../camera/ios/uploadKtp';
import UploadFotoKonsumenIos from '../../../camera/ios/uploadFotoKonsumen';
import UploadFapIos from '../../../camera/ios/uploadFap';
import UploadPpbkIos from '../../../camera/ios/uploadPpbk';

var options = {
  title: 'Select Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class SixthWizard extends Component {
  constructor(props) {
    super(props);
    this.changeSchemeId = this.changeSchemeId.bind(this);
    // this.changeFinancingPurpose = this.changeFinancingPurpose.bind(this)
    this.changeSourceFunds = this.changeSourceFunds.bind(this);
    this.imagePickerKtp = this.imagePickerKtp.bind(this);
    this.imagePickerDiri = this.imagePickerDiri.bind(this);
    this.imagePickerFap = this.imagePickerFap.bind(this);
    this.imagePickerPpbk = this.imagePickerPpbk.bind(this);
    this.hitCalculate = this.hitCalculate.bind(this);
    this.resetCalculate = this.resetCalculate.bind(this);
    this.state = {
      userAccess: '',
      userToken: '',
      pickerJenisItem: [{Code: '', Descr: 'Silahkan Pilih'}],
      pickerJenisSelected: '',
      pickerMerkItem: [{Code: '', Descr: 'Silahkan Pilih'}],
      pickerMerkSelected: '',
      pickerSchemeIdItem: [{Code: '', Descr: 'Silahkan Pilih'}],
      pickerSchemeIdSelected: '',
      pickerFinancingPurposeItem: [{Code: '', Descr: 'Silahkan Pilih'}],
      pickerFinancingPurposeSelected: '',
      pickerSourceFundsItem: [{Code: '', Descr: 'Silahkan Pilih'}],
      pickerSourceFundsSelected: '',
      pickerTenorItem: [{Code: '', Descr: 'Pilih'}],
      pickerTenorSelected: '',
      pickerDpItem: [{Code: '', Descr: 'Pilih'}],
      pickerDpSelected: '',
      disableJenis: true,
      disableMerk: true,
      disableButtonUpload: false,
      uploadKtp: false,
      uploadDiri: false,
      uploadFap: false,
      uploadPpbk: false,
      uploadKtpView: false,
      uploadDiriView: false,
      isImageViewVisibleKtp: false,
      isImageViewVisibleDiri: false,
      isImageViewVisibleFap: false,
      isImageViewVisiblePpbk: false,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      timer: 60,
      disabledOtpButton: false,
      validate: false,
      detailUser: false,
      otpChance: 3,
      otpSmsLoading: false,
      hargaBarang: null,
    };
  }

  componentDidMount = async () => {
    const {
      formFdeMpAdd,
      ddeMerk,
      ddeSchemeId,
      qdeDetailData_Result,
      ddeFinancingPurpose_Result,
      ddeJenis_Result,
      ddeSourceFunds_Result,
      mpMappingRiskResult,
    } = this.props;

    const jsonUserAccess = await AsyncStorage.getItem('userAccess');
    var userAccess = JSON.parse(jsonUserAccess);
    this.setState({userAccess: userAccess});
    const jsonDetailUser = await AsyncStorage.getItem('detailUser');
    if (jsonDetailUser) {
      var detailUser = JSON.parse(jsonDetailUser);
      this.setState({detailUser: detailUser});
    }
    const userToken = await AsyncStorage.getItem('userToken');
    this.setState({userToken: userToken});

    if (ddeJenis_Result.length > 0) {
      var arrayData = [{Code: '', Descr: 'Silahkan Pilih'}];

      for (var i = 0; i < ddeJenis_Result.length; i++) {
        arrayData.push({
          Code: ddeJenis_Result[i].Code + '^' + ddeJenis_Result[i].Descr,
          Descr: ddeJenis_Result[i].Descr,
        });
      }
      this.setState({
        pickerJenisItem: arrayData,
        disableJenis: false,
      });
    }

    if (ddeFinancingPurpose_Result.length > 0) {
      var arrayData = [{Code: '', Descr: 'Silahkan Pilih'}];

      for (var i = 0; i < ddeFinancingPurpose_Result.length; i++) {
        arrayData.push({
          Code:
            ddeFinancingPurpose_Result[i].Code +
            '^' +
            ddeFinancingPurpose_Result[i].Descr,
          Descr: ddeFinancingPurpose_Result[i].Descr,
        });
      }
      this.setState({
        pickerFinancingPurposeItem: arrayData,
        disableFinancingPurpose: false,
      });
    }

    if (ddeSourceFunds_Result.length > 0) {
      var arrayData = [{Code: '', Descr: 'Silahkan Pilih'}];

      for (var i = 0; i < ddeSourceFunds_Result.length; i++) {
        arrayData.push({
          Code:
            ddeSourceFunds_Result[i].Code +
            '^' +
            ddeSourceFunds_Result[i].Descr,
          Descr: ddeSourceFunds_Result[i].Descr,
        });
      }
      this.setState({
        pickerSourceFundsItem: arrayData,
        disableSourceFunds: false,
      });
    }

    if (mpMappingRiskResult) {
      let listTenor = mpMappingRiskResult.Tenor;
      let listDp = mpMappingRiskResult.DP;

      let arrayTenor = [{Code: '', Descr: 'Silahkan Pilih'}];
      let arrayDp = [{Code: '', Descr: 'Silahkan Pilih'}];

      for (var i = 0; i < listDp.length; i++) {
        arrayDp.push({
          Code: listDp[i],
          Descr: (parseFloat(listDp[i]) * 100).toString() + ' %',
        });
      }

      for (var i = 0; i < listTenor.length; i++) {
        arrayTenor.push({
          Code: listTenor[i],
          Descr: listTenor[i] + ' Bulan',
        });
      }

      this.setState({
        pickerTenorItem: arrayTenor,
        pickerDpItem: arrayDp,
      });
    }

    if (formFdeMpAdd.values) {
      console.log(formFdeMpAdd.values);
      let valDPAmount = formFdeMpAdd.values.uangMukaAmount
        ? formFdeMpAdd.values.uangMukaAmount
        : 0;
      let valUnitPrice = formFdeMpAdd.values.hargaBarang
        ? formFdeMpAdd.values.hargaBarang
        : 0;
      let valDPPercentage = (
        parseFloat(valDPAmount) / parseFloat(valUnitPrice)
      ).toFixed(2);

      this.props.updateField(
        'formFdeMpAdd',
        'jangkaWaktuValue',
        formFdeMpAdd.values.jangkaWaktu,
      );

      this.props.updateField('formFdeMpAdd', 'uangMukaValue', valDPPercentage);
      //Troubleshoot on uang muka field give error
      this.props.updateField('formFdeMpAdd', 'uangMuka', valDPPercentage);

      this.setState({
        pickerFinancingPurposeSelected: formFdeMpAdd.values.financingPurpose
          ? formFdeMpAdd.values.financingPurpose
          : this.state.pickerFinancingPurposeSelected,
        pickerSourceFundsSelected: formFdeMpAdd.values.sourceFunds
          ? formFdeMpAdd.values.sourceFunds
          : this.state.pickerSourceFundsSelected,
        pickerMerkSelected: formFdeMpAdd.values.merk
          ? formFdeMpAdd.values.merk
          : this.state.pickerMerkSelected,
        pickerSchemeIdSelected: formFdeMpAdd.values.schemeId
          ? formFdeMpAdd.values.schemeId
          : this.state.pickerSchemeIdSelected,
        pickerTenorSelected: formFdeMpAdd.values.jangkaWaktu
          ? formFdeMpAdd.values.jangkaWaktu
          : this.state.pickerTenorSelected,
        pickerDpSelected: formFdeMpAdd.values.uangMukaAmount
          ? valDPPercentage
          : this.state.pickerDpSelected,

        uploadKtp: formFdeMpAdd.values.uploadKtp
          ? formFdeMpAdd.values.uploadKtp
          : false,
        uploadDiri: formFdeMpAdd.values.uploadDiri
          ? formFdeMpAdd.values.uploadDiri
          : false,
        uploadFap: formFdeMpAdd.values.uploadFap
          ? formFdeMpAdd.values.uploadFap
          : false,
        uploadPpbk: formFdeMpAdd.values.uploadPpbk
          ? formFdeMpAdd.values.uploadPpbk
          : false,
      });
    }

    if (formFdeMpAdd.values && formFdeMpAdd.values.jenis) {
      this.setState({pickerJenisSelected: formFdeMpAdd.values.jenis});

      if (formFdeMpAdd.values.jenis !== '') {
        ddeMerk(
          {JaminanJenis: substringCircum(formFdeMpAdd.values.jenis)},
          userToken,
        );
        ddeSchemeId({
          AppNo: formFdeMpAdd.values.nomorAplikasi,
          JaminanJenis: substringCircum(formFdeMpAdd.values.jenis),
          userToken,
        });
      }
    }

    if (formFdeMpAdd.values && formFdeMpAdd.values.schemeId) {
      if (formFdeMpAdd.values.schemeId !== '') {
        this.setState({
          disableHargaBarang: false,
          disableHitCalculate: false,
        });
      }
    }

    if (formFdeMpAdd.values && formFdeMpAdd.values.financialSchemeId) {
      if (formFdeMpAdd.values.financialSchemeId !== '') {
        this.setState({disableButtonUpload: false});
      }
      this.setState({
        disableHitCalculate: true,
        disableHargaBarang: true,
        disableUangMuka: true,
        disableJangkaWaktu: true,
        disableResetCalculate: false,
      });
    }
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      ddeMerk_Result,
      ddeSchemeId_Result,
      ddeFinancialData_Result,
    } = this.props;

    if (this.state.timer === 0) {
      clearInterval(this.interval);
      this.setState({
        otpSmsLoading: false,
        timer: null,
        error: false,
      });
    }

    if (ddeMerk_Result.length && prevProps.ddeMerk_Result !== ddeMerk_Result) {
      var arrayData = [{Code: '', Descr: 'Silahkan Pilih'}];

      for (var i = 0; i < ddeMerk_Result.length; i++) {
        arrayData.push({
          Code: ddeMerk_Result[i].Code + '^' + ddeMerk_Result[i].Descr,
          Descr: ddeMerk_Result[i].Descr,
        });
      }
      this.setState({
        pickerMerkItem: arrayData,
        disableMerk: false,
      });
    }

    if (
      ddeSchemeId_Result.length &&
      prevProps.ddeSchemeId_Result !== ddeSchemeId_Result
    ) {
      var arrayData = [{Code: '', Descr: 'Silahkan Pilih'}];

      for (var i = 0; i < ddeSchemeId_Result.length; i++) {
        //  add push value ddeSchemeId_Result[i].TipeBiayaAsuransi
        arrayData.push({
          Code:
            ddeSchemeId_Result[i].Code +
            '^' +
            ddeSchemeId_Result[i].Descr +
            '^' +
            ddeSchemeId_Result[i].TipeBiayaAdmin +
            '^' +
            ddeSchemeId_Result[i].TipePembayaran +
            '^' +
            ddeSchemeId_Result[i].TipeBiayaAsuransi,
          Descr: ddeSchemeId_Result[i].Descr,
        });
      }
      this.setState({
        pickerSchemeIdItem: arrayData,
        disableSchemeId: false,
      });
    }

    if (
      ddeFinancialData_Result &&
      prevProps.ddeFinancialData_Result !== ddeFinancialData_Result &&
      ddeFinancialData_Result !== null
    ) {
      if (
        ddeFinancialData_Result.SchemeId === 'null' ||
        ddeFinancialData_Result.SchemeId === null
      ) {
        this.setState({
          disableButtonUpload: true,
          alertShowed: true,
          alertMessage: 'Hasil kalkulasi masih belum benar.',
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      } else {
        console.log(ddeFinancialData_Result);
        this.setState({
          alertShowed: true,
          alertMessage: 'Berhasil mendapatkan hasil kalkulasi',
          alertTitle: 'Berhasil',
          alertType: 'success',
          alertDoneText: 'OK',
        });
        this.props.updateField(
          'formFdeMpAdd',
          'financialSchemeId',
          ddeFinancialData_Result.SchemeId
            ? ddeFinancialData_Result.SchemeId === 'null'
              ? ''
              : ddeFinancialData_Result.SchemeId
            : '',
        );
        this.props.updateField(
          'formFdeMpAdd',
          'biayaAdmin',
          ddeFinancialData_Result.AdminFeeValue
            ? ddeFinancialData_Result.AdminFeeValue.toString()
            : '0',
        );
        this.props.updateField(
          'formFdeMpAdd',
          'jumlahPembiayaan',
          ddeFinancialData_Result.TotalPlafon
            ? ddeFinancialData_Result.TotalPlafon.toString()
            : '0',
        );
        this.props.updateField(
          'formFdeMpAdd',
          'bungaEfektif',
          ddeFinancialData_Result.InterestSelling
            ? ddeFinancialData_Result.InterestSelling.toString()
            : '0',
        );
        this.props.updateField(
          'formFdeMpAdd',
          'angsuran',
          ddeFinancialData_Result.Angsuran
            ? ddeFinancialData_Result.Angsuran.toString()
            : '0',
        );
        this.props.updateField(
          'formFdeMpAdd',
          'biayaAsuransi',
          ddeFinancialData_Result.InsuranceFeeValue
            ? ddeFinancialData_Result.InsuranceFeeValue.toString()
            : '0',
        );
        this.props.updateField(
          'formFdeMpAdd',
          'pembayaranPertama',
          ddeFinancialData_Result.TotalPembayaranPertama
            ? ddeFinancialData_Result.TotalPembayaranPertama.toString()
            : '0',
        );
        this.setState({
          disableHitCalculate: true,
          disableHargaBarang: true,
          disableUangMuka: true,
          disableJangkaWaktu: true,
          disableButtonUpload: false,
          disableResetCalculate: false,
        });
      }
    }
  };

  changeSchemeId(value) {
    this.setState({pickerSchemeIdSelected: value});

    //add  update field in program id
    this.props.updateField('formFdeMpAdd', 'programID', substringCircum(value));

    this.props.updateField(
      'formFdeMpAdd',
      'tipeBiayaAdmin',
      substringThirdCircum(value),
    );

    this.props.updateField(
      'formFdeMpAdd',
      'tipePembayaran',
      substringFourthCircum(value),
    );

    //add  update field in tipe biaya asuransi
    this.props.updateField(
      'formFdeMpAdd',
      'tipeBiayaAsuransi',
      substringFifthCircum(value),
    );

    this.resetCalculate();

    if (value == '') {
      this.props.updateField('formFdeMpAdd', 'schemeId', '');
    } else {
      this.props.updateField('formFdeMpAdd', 'schemeId', value);
      this.setState({
        disableHargaBarang: false,
        disableHitCalculate: false,
        disableResetCalculate: true,
      });
    }
  }

  changeTenor = (value) => {
    this.resetCalculate();
    this.setState({pickerTenorSelected: value});
    this.props.updateField('formFdeMpAdd', 'jangkaWaktu', value);
    this.props.updateField('formFdeMpAdd', 'jangkaWaktuValue', value);
  };

  changeDp(value) {
    const {formFdeMpAdd} = this.props;
    this.resetCalculate();
    this.setState({pickerDpSelected: value});
    var uangMukaAmount = '';
    this.props.updateField('formFdeMpAdd', 'uangMuka', value);
    this.props.updateField('formFdeMpAdd', 'uangMukaValue', value);

    if (!value || value == '') {
      this.props.updateField('formFdeMpAdd', 'uangMukaAmount', '');
    } else {
      uangMukaAmount =
        parseFloat(value) * parseFloat(formFdeMpAdd.values.hargaBarang);
      this.props.updateField(
        'formFdeMpAdd',
        'uangMukaAmount',
        uangMukaAmount.toString(),
      );
    }
  }

  // changeFinancingPurpose(value){
  //   this.setState({ pickerFinancingPurposeSelected: value })
  //   console.log(value)

  //   if (value == '') {
  //     this.props.updateField('formFdeMpAdd', 'financingPurpose', this.state.pickerFinancingPurposeItem.Code)
  //   } else {
  //     this.props.updateField('formFdeMpAdd', 'financingPurpose', value)
  //   }
  // }

  changeSourceFunds(value) {
    this.setState({pickerSourceFundsSelected: value});

    if (value == '') {
      this.props.updateField(
        'formFdeMpAdd',
        'sourceFunds',
        this.state.pickerSourceFundsItem.Code,
      );
    } else {
      this.props.updateField('formFdeMpAdd', 'sourceFunds', value);
    }
  }

  hitCalculate = () => {
    const {ddeFinancialData, formFdeMpAdd, handleAlert} = this.props;

    if (
      !formFdeMpAdd.values.hargaBarang ||
      formFdeMpAdd.values.hargaBarang == ''
    ) {
      handleAlert({
        alertShowed: true,
        alertMessage: 'Isian harga barang msih belum benar',
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
      });
    } else if (
      !formFdeMpAdd.values.uangMukaValue ||
      formFdeMpAdd.values.uangMukaValue == ''
    ) {
      handleAlert({
        alertShowed: true,
        alertMessage: 'Isian uang muka masih belum benar',
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
      });
    } else if (
      !formFdeMpAdd.values.jangkaWaktuValue ||
      formFdeMpAdd.values.jangkaWaktuValue == ''
    ) {
      handleAlert({
        alertShowed: true,
        alertMessage: 'Isian jangka waktu masih belum benar',
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
      });
    } else if (
      !formFdeMpAdd.values.schemeId ||
      formFdeMpAdd.values.schemeId == ''
    ) {
      handleAlert({
        alertShowed: true,
        alertMessage: 'Isian nama program masih belum benar',
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
      });
    } else {
      ddeFinancialData({
        ProgramId: substringCircum(formFdeMpAdd.values.schemeId),
        HargaBarang: formFdeMpAdd.values.hargaBarang,
        DP: formFdeMpAdd.values.uangMukaAmount,
        Tenor: formFdeMpAdd.values.jangkaWaktuValue,
        Cabang: formFdeMpAdd.values.kodeCabang,
        JaminanJenis: substringCircum(formFdeMpAdd.values.jenis),
        JaminanMerk: substringCircum(formFdeMpAdd.values.merk),
        JaminanModel: '',
        JaminanTipe: '',
      });
    }
  };

  resetCalculate = async () => {
    this.props.updateField('formFdeMpAdd', 'financialSchemeId', '');
    this.props.updateField('formFdeMpAdd', 'biayaAdmin', '');
    this.props.updateField('formFdeMpAdd', 'jumlahPembiayaan', '');
    this.props.updateField('formFdeMpAdd', 'bungaEfektif', '');
    this.props.updateField('formFdeMpAdd', 'angsuran', '');
    this.props.updateField('formFdeMpAdd', 'biayaAsuransi', '');
    this.props.updateField('formFdeMpAdd', 'pembayaranPertama', '');
  };

  imagePickerKtp() {
    this.props.enableCameraUploadKtp();
  }

  clearKtp() {
    this.setState({
      uploadKtp: false,
    });
    this.props.updateField('formFdeMpAdd', 'uploadKtp', '');
  }

  imagePickerDiri() {
    this.props.enableCameraUploadFotoKonsumen();
  }

  clearDiri() {
    this.setState({
      uploadDiri: false,
    });
    this.props.updateField('formFdeMpAdd', 'uploadDiri', '');
  }

  imagePickerFap() {
    this.props.enableCameraUploadFap();
  }

  clearFap() {
    this.setState({
      uploadFap: false,
    });
    this.props.updateField('formFdeMpAdd', 'uploadFap', '');
  }

  imagePickerPpbk() {
    this.props.enableCameraUploadPpbk();
  }

  clearPpbk() {
    this.setState({
      uploadPpbk: false,
    });
    this.props.updateField('formFdeMpAdd', 'uploadPpbk', '');
  }

  imageView = (uri) => [
    {
      source: {
        uri: uri,
      },
    },
  ];

  ViewKtp = (image) => {
    return (
      <ImageView
        currentImage={image}
        images={this.imageView(image)}
        imageIndex={0}
        isVisible={this.state.isImageViewVisibleKtp}
        animationType="fade"
        onClose={() => this.setState({isImageViewVisibleKtp: false})}
        isSwipeCloseEnabled={true}
        isTapZoomEnabled={true}
        isPinchZoomEnabled={true}
      />
    );
  };

  ViewDiri = (image) => {
    return (
      <ImageView
        currentImage={image}
        images={this.imageView(image)}
        imageIndex={0}
        isVisible={this.state.isImageViewVisibleDiri}
        animationType="fade"
        onClose={() => this.setState({isImageViewVisibleDiri: false})}
        isSwipeCloseEnabled={true}
        isTapZoomEnabled={true}
        isPinchZoomEnabled={true}
      />
    );
  };

  ViewFap = (image) => {
    return (
      <ImageView
        currentImage={image}
        images={this.imageView(image)}
        imageIndex={0}
        isVisible={this.state.isImageViewVisibleFap}
        animationType="fade"
        onClose={() => this.setState({isImageViewVisibleFap: false})}
        isSwipeCloseEnabled={true}
        isTapZoomEnabled={true}
        isPinchZoomEnabled={true}
      />
    );
  };

  ViewPpbk = (image) => {
    return (
      <ImageView
        currentImage={image}
        images={this.imageView(image)}
        imageIndex={0}
        isVisible={this.state.isImageViewVisiblePpbk}
        animationType="fade"
        onClose={() => this.setState({isImageViewVisiblePpbk: false})}
        isSwipeCloseEnabled={true}
        isTapZoomEnabled={true}
        isPinchZoomEnabled={true}
      />
    );
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  tick = () => {
    this.setState({otpSmsLoading: true});
    this.interval = setInterval(
      () => this.setState((prevState) => ({timer: prevState.timer - 1})),
      1000,
    );
  };

  goBackKtp = (url) => {
    ImageResizer.createResizedImage(url, 2000, 2000, 'JPEG', 50)
      .then((response) => {
        var source = Platform.OS === 'android' ? response.uri : response.path;
        ImgToBase64.getBase64String(source)
          .then((base64String) => {
            this.setState({
              uploadKtp: 'data:image/jpeg;base64,' + base64String,
            });
            this.props.sendUploadKtp(this.state.uploadKtp);
            this.props.updateField('formFdeMpAdd', 'uploadKtp', 'done');
          })
          .catch((err) =>
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            }),
          );
      })
      .catch((err) => {
        handleAlert({
          alertShowed: true,
          alertMessage: err.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      });
    this.props.disableCameraUploadKtp();
  };

  goBackFotoKonsumen = (url) => {
    ImageResizer.createResizedImage(url, 2000, 2000, 'JPEG', 50)
      .then((response) => {
        var source = Platform.OS === 'android' ? response.uri : response.path;
        ImgToBase64.getBase64String(source)
          .then((base64String) => {
            this.setState({
              uploadDiri: 'data:image/jpeg;base64,' + base64String,
            });
            this.props.sendUploadDiri(this.state.uploadDiri);
            this.props.updateField('formFdeMpAdd', 'uploadDiri', 'done');
          })
          .catch((err) =>
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            }),
          );
      })
      .catch((err) => {
        handleAlert({
          alertShowed: true,
          alertMessage: err.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      });
    this.props.disableCameraUploadFotoKonsumen();
  };

  goBackFap = (url) => {
    ImageResizer.createResizedImage(url, 2000, 2000, 'JPEG', 50)
      .then((response) => {
        var source = Platform.OS === 'android' ? response.uri : response.path;
        ImgToBase64.getBase64String(source)
          .then((base64String) => {
            this.setState({
              uploadFap: 'data:image/jpeg;base64,' + base64String,
            });
            this.props.sendUploadFap(this.state.uploadFap);
            this.props.updateField('formFdeMpAdd', 'uploadFap', 'done');
          })
          .catch((err) =>
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            }),
          );
      })
      .catch((err) => {
        handleAlert({
          alertShowed: true,
          alertMessage: err.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      });
    this.props.disableCameraUploadFap();
  };

  goBackPpbk = (url) => {
    ImageResizer.createResizedImage(url, 2000, 2000, 'JPEG', 50)
      .then((response) => {
        var source = Platform.OS === 'android' ? response.uri : response.path;
        ImgToBase64.getBase64String(source)
          .then((base64String) => {
            this.setState({
              uploadPpbk: 'data:image/jpeg;base64,' + base64String,
            });
            this.props.sendUploadPpbk(this.state.uploadPpbk);
            this.props.updateField('formFdeMpAdd', 'uploadPpbk', 'done');
          })
          .catch((err) =>
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            }),
          );
      })
      .catch((err) => {
        handleAlert({
          alertShowed: true,
          alertMessage: err.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      });
    this.props.disableCameraUploadPpbk();
  };

  cancelUploadKtp = async () => {
    this.props.disableCameraUploadKtp();
  };

  cancelUploadFotoKonsumen = async () => {
    this.props.disableCameraUploadFotoKonsumen();
  };

  cancelUploadFap = async () => {
    this.props.disableCameraUploadFap();
  };

  cancelUploadPpbk = async () => {
    this.props.disableCameraUploadPpbk();
  };

  render() {
    const {
      handleSubmit,
      previousPage,
      mpMappingRiskLoading,
      cameraUploadKtpEnable,
      cameraUploadFapEnable,
      cameraUploadFotoKonsumenEnable,
      cameraUploadPpbkEnable,
    } = this.props;
    const {
      pickerSchemeIdItem,
      pickerSchemeIdSelected,
      pickerFinancingPurposeItem,
      pickerFinancingPurposeSelected,
      pickerSourceFundsItem,
      pickerSourceFundsSelected,
      pickerTenorItem,
      pickerTenorSelected,
      pickerDpSelected,
      pickerDpItem,
      disableSchemeId,
    } = this.state;

    if (cameraUploadKtpEnable && Platform.OS === 'android') {
      return (
        <UploadKtpAndroid
          goBack={(url) => this.goBackKtp(url)}
          cancelUpload={() => this.cancelUploadKtp()}
        />
      );
    }
    if (cameraUploadFotoKonsumenEnable && Platform.OS === 'android') {
      return (
        <UploadFotoKonsumenAndroid
          goBack={(url) => this.goBackFotoKonsumen(url)}
          cancelUpload={() => this.cancelUploadFotoKonsumen()}
        />
      );
    }
    if (cameraUploadFapEnable && Platform.OS === 'android') {
      return (
        <UploadFapAndroid
          goBack={(url) => this.goBackFap(url)}
          cancelUpload={() => this.cancelUploadFap()}
        />
      );
    }
    if (cameraUploadPpbkEnable && Platform.OS === 'android') {
      return (
        <UploadPpbkAndroid
          goBack={(url) => this.goBackPpbk(url)}
          cancelUpload={() => this.cancelUploadPpbk()}
        />
      );
    }
    if (cameraUploadKtpEnable && Platform.OS !== 'android') {
      return (
        <UploadKtpIos
          goBack={(url) => this.goBackKtp(url)}
          cancelUpload={() => this.cancelUploadKtp()}
        />
      );
    }
    if (cameraUploadFotoKonsumenEnable && Platform.OS !== 'android') {
      return (
        <UploadFotoKonsumenIos
          goBack={(url) => this.goBackFotoKonsumen(url)}
          cancelUpload={() => this.cancelUploadFotoKonsumen()}
        />
      );
    }
    if (cameraUploadFapEnable && Platform.OS !== 'android') {
      return (
        <UploadFapIos
          goBack={(url) => this.goBackFap(url)}
          cancelUpload={() => this.cancelUploadFap()}
        />
      );
    }
    if (cameraUploadPpbkEnable && Platform.OS !== 'android') {
      return (
        <UploadPpbkIos
          goBack={(url) => this.goBackPpbk(url)}
          cancelUpload={() => this.cancelUploadPpbk()}
        />
      );
    } else {
      return (
        <Container>
          <Spinner
            visible={mpMappingRiskLoading ? true : false}
            textContent={'Loading...'}
            textStyle={{color: '#FFF'}}
          />

          {this.showAlert()}

          <Content
            disableKBDismissScroll={true}
            style={{
              height: '75%',
              backgroundColor: 'white',
              width: '100%',
              alignSelf: 'center',
              paddingHorizontal: '4%',
            }}>
            <Form>
              <View
                style={{
                  marginTop: 20,
                  borderBottomWidth: 2,
                  borderBottomColor: 'grey',
                  paddingBottom: 10,
                }}>
                <Text
                  style={{
                    color: 'grey',
                    fontSize: 18,
                    fontWeight: 'bold',
                    fontFamily: FSAlbert,
                  }}>
                  Data Toko
                </Text>
              </View>

              <Field
                name="kodeToko"
                type="text"
                component={renderFieldDisabled}
                label="Kode Toko"
              />

              <Field
                name="namaToko"
                type="text"
                component={renderFieldDisabled}
                label="Nama Toko"
              />

              <Field
                name="kodeCabang"
                type="text"
                component={renderFieldDisabled}
                label="Kode Cabang"
              />

              <Field
                name="namaCabang"
                type="text"
                component={renderFieldDisabled}
                label="Nama Cabang"
              />

              <View
                style={{
                  marginTop: 40,
                  borderBottomWidth: 2,
                  borderBottomColor: 'grey',
                  paddingBottom: 10,
                }}>
                <Text
                  style={{
                    color: 'grey',
                    fontSize: 18,
                    fontWeight: 'bold',
                    fontFamily: FSAlbert,
                  }}>
                  Data Barang Pembiayaan
                </Text>
              </View>

              <Field
                name="jaminanJenis"
                type="text"
                component={renderFieldHidden}
                label="Jaminan Jenis"
              />

              <Field
                name="jenisDisabled"
                type="text"
                component={renderFieldDisabled}
                label="Jenis Barang"
              />

              <Field
                name="merkDisabled"
                type="text"
                component={renderFieldDisabled}
                label="Merk Barang"
              />

              <Field
                name="jenis"
                type="text"
                component={renderFieldHidden}
                label="Jenis Barang"
              />

              <Field
                name="merk"
                type="text"
                component={renderFieldHidden}
                label="Merk Barang"
              />

              <Field name="risk" type="text" component={renderFieldHidden} />

              <Field
                name="ukuran"
                type="text"
                component={renderFieldDisabled}
                label="Ukuran"
              />

              <Field
                name="warna"
                type="text"
                component={renderFieldDisabled}
                label="Warna"
              />

              <Field
                name="tipe"
                type="text"
                component={renderFieldDisabled}
                label="Tipe"
              />

              <Field
                name="financingPurpose"
                type="text"
                component={renderFieldHidden}
              />

              <Field
                name="financingPurposeView"
                type="text"
                component={renderFieldDisabled}
                label="Peruntukan Unit"
              />

              {/* <Field
              name="financingPurpose"
              component={renderFieldPicker}
              label="Peruntukan Unit"
              pickerSelected = {pickerFinancingPurposeSelected}
              onValueChange = {(itemValue, itemIndex) => this.changeFinancingPurpose(itemValue)}
              data = {pickerFinancingPurposeItem.map((pickerFinancingPurposeItem, index) => <Picker.Item label={pickerFinancingPurposeItem.Descr} value={pickerFinancingPurposeItem.Code} />)}
            /> */}

              <Field
                name="hargaBarang"
                type="text"
                component={renderFieldDisabled}
                label="Harga Barang"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />

              {/* <TouchableOpacity 
              style={styles.verificationValidationButton} 
              onPress={() => this.cekTenor()}
              disabled={false}>
                <Text style={{color:'white',fontSize:16}}>Cek Tenor Tersedia</Text>
            </TouchableOpacity> */}

              <Field
                name="jangkaWaktu"
                type="text"
                component={renderFieldPicker}
                label="Tenor"
                pickerSelected={pickerTenorSelected}
                data={pickerTenorItem.map((pickerTenorItem, index) => (
                  <Picker.Item
                    label={pickerTenorItem.Descr}
                    value={pickerTenorItem.Code}
                  />
                ))}
                onValueChange={(newValue) => this.changeTenor(newValue)}
              />

              <Field
                name="jangkaWaktuValue"
                type="text"
                component={renderFieldDisabled}
                label="Tenor"
              />

              <Field
                name="uangMuka"
                type="text"
                component={renderFieldPicker}
                label="Uang Muka"
                pickerSelected={pickerDpSelected}
                data={pickerDpItem.map((pickerDpItem, index) => (
                  <Picker.Item
                    label={pickerDpItem.Descr}
                    value={pickerDpItem.Code}
                  />
                ))}
                onValueChange={(newValue) => this.changeDp(newValue)}
              />

              <Field
                name="uangMukaValue"
                type="text"
                component={renderFieldHidden}
                label="Uang Muka"
              />

              <Field
                name="uangMukaAmount"
                label="Nominal Uang Muka"
                type="text"
                component={renderFieldDisabled}
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />

              {disableSchemeId ? (
                <DisabledForm title="Nama Program" />
              ) : (
                <Field
                  name="schemeId"
                  component={renderFieldPicker}
                  label="Nama Program"
                  pickerSelected={pickerSchemeIdSelected}
                  onValueChange={(itemValue, itemIndex) =>
                    this.changeSchemeId(itemValue)
                  }
                  data={pickerSchemeIdItem.map((pickerSchemeIdItem, index) => (
                    <Picker.Item
                      label={pickerSchemeIdItem.Descr}
                      value={pickerSchemeIdItem.Code}
                    />
                  ))}
                />
              )}

              {/* add field hidden Program ID */}
              <Field
                name="programID"
                type="text"
                component={renderFieldHidden}
                label="Program Id"
              />

              {/* add field hidden tipe biaya asuransi */}
              <Field
                name="tipeBiayaAsuransi"
                type="text"
                component={renderFieldHidden}
                label="Tipe Biaya Asuransi"
              />

              <Field
                name="tipeBiayaAdmin"
                type="text"
                component={renderFieldDisabled}
                label="Tipe Biaya Admin"
              />

              <Field
                name="tipePembayaran"
                type="text"
                component={renderFieldDisabled}
                label="Tipe Pembayaran"
              />

              <TouchableOpacity
                style={styles.verificationValidationButton}
                onPress={this.hitCalculate}
                disabled={false}>
                <Text
                  style={{color: 'white', fontSize: 16, fontFamily: FSAlbert}}>
                  Kalkulasi
                </Text>
              </TouchableOpacity>

              <Field
                name="financialSchemeId"
                type="text"
                component={renderFieldDisabled}
                label="Nama Program"
              />

              <Field
                name="biayaAdmin"
                type="text"
                component={renderFieldDisabled}
                label="Biaya Admin"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />

              <Field
                name="jumlahPembiayaan"
                type="text"
                component={renderFieldDisabled}
                label="Jumlah Pembiayaan"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />

              <Field
                name="bungaEfektif"
                type="text"
                component={renderFieldHidden}
                label="Bunga Efektif"
              />

              <Field
                name="angsuran"
                type="text"
                component={renderFieldDisabled}
                label="Angsuran"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />

              <Field
                name="biayaAsuransi"
                type="text"
                component={renderFieldDisabled}
                label="Biaya Asuransi"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />

              <Field
                name="pembayaranPertama"
                type="text"
                component={renderFieldDisabled}
                label="Pembayaran Pertama"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />

              <Field
                name="sourceFunds"
                component={renderFieldPicker}
                label="Sumber Dana"
                pickerSelected={pickerSourceFundsSelected}
                onValueChange={(itemValue, itemIndex) =>
                  this.changeSourceFunds(itemValue)
                }
                data={pickerSourceFundsItem.map(
                  (pickerSourceFundsItem, index) => (
                    <Picker.Item
                      label={pickerSourceFundsItem.Descr}
                      value={pickerSourceFundsItem.Code}
                    />
                  ),
                )}
              />

              {this.props.fdePrivy == 0 && (
                <>
                  <View
                    style={{
                      marginTop: 40,
                      borderBottomWidth: 2,
                      borderBottomColor: 'grey',
                      paddingBottom: 10,
                    }}>
                    <Text
                      style={{
                        color: 'grey',
                        fontSize: 18,
                        fontWeight: 'bold',
                        fontFamily: FSAlbert,
                      }}>
                      Data Upload
                    </Text>
                  </View>

                  <View style={{marginTop: 40}} />

                  {this.state.uploadKtp ? (
                    <TouchableOpacity
                      style={styles.CLContainer}
                      onPress={() => {
                        this.setState({isImageViewVisibleKtp: true});
                      }}>
                      <Image
                        source={{uri: this.state.uploadKtp}}
                        style={styles.CLImage}
                      />
                      <TouchableOpacity
                        style={styles.CLDeleteImage}
                        onPress={() => this.clearKtp()}>
                        <Text style={styles.CLDeleteImageIcon}>X</Text>
                      </TouchableOpacity>
                    </TouchableOpacity>
                  ) : null}

                  <TouchableOpacity
                    style={
                      this.state.disableButtonUpload || this.state.uploadKtp
                        ? styles.ButtonDisabled
                        : styles.verificationValidationButton
                    }
                    disabled={
                      this.state.disableButtonUpload || this.state.uploadKtp
                        ? true
                        : false
                    }
                    onPress={this.imagePickerKtp}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 16,
                        fontFamily: FSAlbert,
                      }}>
                      Upload Foto KTP
                    </Text>
                  </TouchableOpacity>

                  <Field
                    name="uploadKtp"
                    type="hidden"
                    component={renderFieldHidden}
                  />

                  {this.state.uploadDiri ? (
                    <TouchableOpacity
                      style={styles.CLContainer}
                      onPress={() => {
                        this.setState({isImageViewVisibleDiri: true});
                      }}>
                      <Image
                        source={{uri: this.state.uploadDiri}}
                        style={styles.CLImage}
                      />
                      <TouchableOpacity
                        style={styles.CLDeleteImage}
                        onPress={() => this.clearDiri()}>
                        <Text style={styles.CLDeleteImageIcon}>X</Text>
                      </TouchableOpacity>
                    </TouchableOpacity>
                  ) : null}

                  <TouchableOpacity
                    style={
                      this.state.disableButtonUpload || this.state.uploadDiri
                        ? styles.ButtonDisabled
                        : styles.verificationValidationButton
                    }
                    disabled={
                      this.state.disableButtonUpload || this.state.uploadDiri
                        ? true
                        : false
                    }
                    onPress={this.imagePickerDiri}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 16,
                        fontFamily: FSAlbert,
                      }}>
                      Upload Foto Konsumen
                    </Text>
                  </TouchableOpacity>

                  <Field
                    name="uploadDiri"
                    type="hidden"
                    component={renderFieldHidden}
                  />

                  {this.state.uploadFap ? (
                    <TouchableOpacity
                      style={styles.CLContainer}
                      onPress={() => {
                        this.setState({isImageViewVisibleFap: true});
                      }}>
                      <Image
                        source={{uri: this.state.uploadFap}}
                        style={styles.CLImage}
                      />
                      <TouchableOpacity
                        style={styles.CLDeleteImage}
                        onPress={() => this.clearFap()}>
                        <Text style={styles.CLDeleteImageIcon}>X</Text>
                      </TouchableOpacity>
                    </TouchableOpacity>
                  ) : null}

                  <TouchableOpacity
                    style={
                      this.state.disableButtonUpload || this.state.uploadFap
                        ? styles.ButtonDisabled
                        : styles.verificationValidationButton
                    }
                    disabled={
                      this.state.disableButtonUpload || this.state.uploadFap
                        ? true
                        : false
                    }
                    onPress={this.imagePickerFap}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 16,
                        fontFamily: FSAlbert,
                      }}>
                      Upload Foto FAP
                    </Text>
                  </TouchableOpacity>

                  <Field
                    name="uploadFap"
                    type="hidden"
                    component={renderFieldHidden}
                  />

                  {this.state.uploadPpbk ? (
                    <TouchableOpacity
                      style={styles.CLContainer}
                      onPress={() => {
                        this.setState({isImageViewVisiblePpbk: true});
                      }}>
                      <Image
                        source={{uri: this.state.uploadPpbk}}
                        style={styles.CLImage}
                      />
                      <TouchableOpacity
                        style={styles.CLDeleteImage}
                        onPress={() => this.clearPpbk()}>
                        <Text style={styles.CLDeleteImageIcon}>X</Text>
                      </TouchableOpacity>
                    </TouchableOpacity>
                  ) : null}

                  <TouchableOpacity
                    style={
                      this.state.disableButtonUpload || this.state.uploadPpbk
                        ? styles.ButtonDisabled
                        : styles.verificationValidationButton
                    }
                    disabled={
                      this.state.disableButtonUpload || this.state.uploadPpbk
                        ? true
                        : false
                    }
                    onPress={this.imagePickerPpbk}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 16,
                        fontFamily: FSAlbert,
                      }}>
                      Upload Foto PPBK
                    </Text>
                  </TouchableOpacity>

                  <Field
                    name="uploadPpbk"
                    type="hidden"
                    component={renderFieldHidden}
                  />

                  <Field
                    name="channel"
                    type="hidden"
                    component={renderFieldHidden}
                    label="Channel"
                  />

                  {this.ViewKtp(this.state.uploadKtp)}
                  {this.ViewDiri(this.state.uploadDiri)}
                  {this.ViewFap(this.state.uploadFap)}
                  {this.ViewPpbk(this.state.uploadPpbk)}
                </>
              )}

              <View style={{marginTop: 150}} />
            </Form>
          </Content>

          <HideWithKeyboard style={styles.buttonSideBottomContainer}>
            <TouchableOpacity style={styles.prevButton} onPress={previousPage}>
              <Text
                style={{color: 'white', fontSize: 16, fontFamily: FSAlbert}}>
                Kembali
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.nextButton} onPress={handleSubmit}>
              <Text
                style={{color: 'white', fontSize: 16, fontFamily: FSAlbert}}>
                Submit
              </Text>
            </TouchableOpacity>
          </HideWithKeyboard>
        </Container>
      );
    }
  }
}

// const selector = formValueSelector('formFdeMpAdd');

function mapStateToProps(state) {
  return {
    qdeDetailData_Loading: state.qdeDetailData.loading,
    qdeDetailData_Result: state.qdeDetailData.result,
    qdeDetailData_Error: state.qdeDetailData.error,
    ddeJenis_Loading: state.ddeJenis.loading,
    ddeJenis_Result: state.ddeJenis.result,
    ddeJenis_Error: state.ddeJenis.error,
    ddeMerk_Loading: state.ddeMerk.loading,
    ddeMerk_Result: state.ddeMerk.result,
    ddeMerk_Error: state.ddeMerk.error,
    ddeSchemeId_Loading: state.ddeSchemeId.loading,
    ddeSchemeId_Result: state.ddeSchemeId.result,
    ddeSchemeId_Error: state.ddeSchemeId.error,
    ddeFinancialData_Loading: state.ddeFinancialData.loading,
    ddeFinancialData_Result: state.ddeFinancialData.result,
    ddeFinancialData_Error: state.ddeFinancialData.error,
    ddeFinancingPurpose_Loading: state.ddeFinancingPurpose.loading,
    ddeFinancingPurpose_Result: state.ddeFinancingPurpose.result,
    ddeFinancingPurpose_Error: state.ddeFinancingPurpose.error,
    ddeSourceFunds_Loading: state.ddeSourceFunds.loading,
    ddeSourceFunds_Result: state.ddeSourceFunds.result,
    ddeSourceFunds_Error: state.ddeSourceFunds.error,
    formFdeMpAdd: state.form.formFdeMpAdd,
    otpValidateLoading: state.otpValidate.loading,
    otpValidateResult: state.otpValidate.result,
    otpValidateError: state.otpValidate.error,
    mpMappingRiskResult: state.mpMappingRisk.result,
    mpMappingRiskLoading: state.mpMappingRisk.loading,
    mpMappingRiskError: state.mpMappingRisk.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ddeMerk,
      ddeSchemeId,
      ddeFinancialData,
      otpSms,
      otpValidate,
      mpMappingRisk,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
    },
    dispatch,
  );
}

SixthWizard = reduxForm({
  form: 'formFdeMpAdd',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true,
  enableReinitialize: true,
  validate: FormFdeAddMpValidate,
})(SixthWizard);

export default connect(mapStateToProps, matchDispatchToProps)(SixthWizard);
