import React, {Component} from 'react';
import {
  Text,
  View,
  Platform,
  SafeAreaView,
  BackHandler,
  TouchableOpacity,
  NativeModules,
} from 'react-native';
import {
  Container,
  Icon,
  Spinner,
  Label,
  Input,
  Item,
  Picker,
  Button,
  Header,
  Left,
  Body,
  Title,
  Right,
  Textarea,
  Content,
} from 'native-base';
import SubHeaderAndroidBlue from '../../../components/android/subHeaderBlue';
import SubHeaderIosBlue from '../../../components/ios/subHeaderBlue';
import styles from './style';
import DatePicker from 'react-native-datepicker';
import FirstWizard from './form/firstWizard';
import SecondWizard from './form/secondWizard';
import ThirdWizard from './form/thirdWizard';
import FourthWizard from './form/fourthWizard';
import FifthWizard from './form/fifthWizard';
import SixthWizard from './form/sixthWizard';
import SevenWizard from './form/sevenWizard';
import SummaryWizard from './form/summaryWizard';
import OtpWizard from './form/otpWizard';
import PropTypes from 'prop-types';
import StepIndicator from 'react-native-step-indicator';
import {
  substringNoHpFirstCircum,
  substringNoHpSecondCircum,
  substringSecondCircum,
  substringCircum,
  mergeValuesData,
  trimLineBreak,
  handleRefreshToken,
  jsonParse,
} from '../../../utils/utilization';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getCabangStatus, submitPrivy} from '../../../actions/privy';

import {
  qdeDetailData,
  qdePersonalDetailData,
} from '../../../actions/sandiaFeature/dataAwal';
import {ddeInsert} from '../../../actions/sandiaFeature/dataLengkap';
import {
  ddeEducation,
  ddeGender,
  ddeMaritalStatus,
  ddeRelationship,
  ddeReligion,
  ddeHouseOwnership,
  ddeOfficeOwnership,
} from '../../../actions/sandiaFeature/dataLengkap/dataLengkapPersonal';
import {
  ddeLegalProvince,
  ddeResidenceProvince,
  ddeJobProvince,
} from '../../../actions/sandiaFeature/dataLengkap/dataLengkapWilayah';
import {
  ddeJobCategory,
  ddeJobStatus,
  ddeLamaUsahaTahun,
  ddeLamaUsahaBulan,
  ddeKontrakTahun,
  ddeKontrakBulan,
} from '../../../actions/sandiaFeature/dataLengkap/dataLengkapPekerjaan';
import {
  ddeJaminanJenis,
  ddeFinancingPurpose,
  ddeSouceFunds,
  ddeJenis,
} from '../../../actions/sandiaFeature/dataLengkap/dataLengkapTransaksi';
import {getAuthkey} from '../../../actions/credoLabs';
import {generateToken} from '../../../actions/sandiaFeature/generateToken';

import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import {Field, reduxForm, change, reset} from 'redux-form';
import {
  CODE_EDUCATION,
  CODE_GENDER,
  CODE_MARITAL,
  CODE_RELATIONSHIP,
  LOB,
  CODE_RELIGION,
  CODE_HOUSE_OWNERSHIP,
  CODE_OFFICE_OWNERSHIP,
  CODE_JOB_CATEGORY,
  CODE_JOB_STATUS,
  CODE_YEAR,
  CODE_YEAR_PERIOD,
  CODE_MONTH,
  CODE_FINANCING_PURPOSE,
  CODE_SOURCE_FUNDS,
  CODE_JAMINAN,
  APP_NAME,
  CODE_TEST,
  MESSAGE_TOKEN_EXP,
} from '../../../utils/constant';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Modal from 'react-native-modal';
import DatePickerIos from 'react-native-date-picker';
import {Picker as PickerAndroid} from '@react-native-picker/picker';
import {otpSms, otpValidate, refreshToken} from '../../../actions';
import {mpMappingRisk} from '../../../actions/sandiaFeature/dataAwal';
import {fonts, FSAlbert, FSAlbertBold} from '../../../utils/fonts';
import analytics from '@react-native-firebase/analytics';
import {log} from 'react-native-reanimated';	
import {isNonNullChain} from 'typescript';

export const renderField = ({
  input,
  type,
  label,
  editable,
  keyboardType,
  maxLength,
  multiline,
  numberOfLines,
  format,
  normalize,
  placeholder,
  meta: {touched, error},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '100%', marginTop: 10}}>
      <Label
        style={{
          fontFamily: FSAlbert,
          fontSize: 15,
          color: hasError ? 'red' : '#666',
          marginBottom: 0,
        }}>
        {label}
      </Label>
      <Item
        regular
        style={{
          borderRadius: 5,
          height: 40,
          borderColor: hasError ? 'red' : '#ccc',
        }}>
        <Input
          placeholder={placeholder}
          style={styles.formInput}
          {...input}
          type={type}
          editable={editable}
          autoCapitalize="characters"
          keyboardType={keyboardType}
          maxLength={maxLength}
          multiline={multiline}
          numberOfLines={numberOfLines}
          format={format}
          normalize={normalize}
        />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderFieldTextArea = ({
  input,
  label,
  disabled,
  rowSpan,
  meta: {touched, error},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '100%', marginTop: 10}}>
      <Label
        style={{
          fontSize: 15,
          color: hasError ? 'red' : '#666',
          marginBottom: 0,
          fontFamily: FSAlbert,
        }}>
        {label}
      </Label>
      <Textarea
        {...input}
        rowSpan={rowSpan}
        bordered
        style={{borderRadius: 5, borderColor: hasError ? 'red' : '#ccc'}}
        autoCapitalize="characters"
        disabled={disabled}
      />
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderFieldDisabled = ({
  input,
  type,
  label,
  format,
  normalize,
  placeholder,
  meta: {touched, error},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '100%', marginTop: 10}}>
      <Label
        style={{
          fontSize: 15,
          color: '#666',
          marginBottom: 0,
          fontFamily: FSAlbert,
        }}>
        {label}
      </Label>
      <Item
        regular
        style={{
          borderRadius: 5,
          height: 40,
          borderColor: hasError ? 'red' : '#ccc',
        }}>
        <Input
          style={styles.formInputDisabled}
          placeholder={placeholder}
          {...input}
          type={type}
          editable={false}
          format={format}
          normalize={normalize}></Input>
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderFieldHidden = ({input, type, meta: {touched, error}}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '100%'}}>
      <Item style={{display: 'none'}}>
        <Input {...input} ype={type} />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderFieldDatepicker = ({
  input,
  onDateChange,
  date,
  formatdate,
  placeholder,
  meta: {touched, error},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <SafeAreaView style={{width: '100%', marginTop: 10}}>
      <Label
        style={{
          fontFamily: FSAlbert,
          fontSize: 15,
          color: hasError ? 'red' : '#666',
          marginBottom: 0,
        }}>
        {placeholder}
      </Label>
      <DatePicker
        {...input}
        style={{width: '100%', color: 'red'}}
        date={date}
        mode="date"
        placeholder="Pilih Tanggal"
        format={formatdate}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        showIcon={false}
        customStyles={{
          dateIcon: {
            position: 'absolute',
            right: 0,
            top: 4,
            marginLeft: 0,
          },
          dateInput: {
            alignItems: 'flex-start',
            paddingLeft: 10,
            borderWidth: 1,
            borderRadius: 5,
            borderColor: hasError ? 'red' : '#ccc',
            height: 40,
          },
        }}
        onDateChange={onDateChange}
      />
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </SafeAreaView>
  );
};

export const renderFieldDatepickerIos = ({
  input,
  placeholder,
  visible,
  closeModal,
  openModal,
  meta: {touched, error},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <SafeAreaView style={{width: '100%', marginTop: 10}}>
      <Modal isVisible={visible}>
        <View
          style={{
            width: '95%',
            backgroundColor: 'white',
            borderRadius: 5,
            alignItems: 'center',
            alignSelf: 'center',
          }}>
          <TouchableOpacity onPress={closeModal} style={{width: '100%'}}>
            <Icon
              active
              type="FontAwesome"
              name={'times'}
              style={{
                color: '#666',
                fontSize: 24,
                position: 'absolute',
                right: '3%',
                top: 3,
              }}
            />
          </TouchableOpacity>
          <DatePickerIos
            {...input}
            style={{marginVertical: 20}}
            testID="dateTimePicker"
            value={new Date()}
            mode={'date'}
            is24Hour={true}
            display="default"
            onDateChange={(newDate) =>
              input.onChange(moment(newDate).format('YYYY-MM-DD'))
            }
          />
        </View>
      </Modal>
      <TouchableOpacity onPress={() => openModal()}>
        <Label
          style={{
            fontFamily: FSAlbert,
            fontSize: 15,
            color: '#666',
            marginBottom: 0,
          }}>
          {placeholder}
        </Label>

        <Item
          regular
          style={{
            borderRadius: 5,
            height: 40,
            borderColor: hasError ? 'red' : '#ccc',
          }}>
          <Input
            style={styles.formInput}
            {...input}
            type="text"
            editable={false}></Input>
        </Item>
        <View
          style={{
            position: 'absolute',
            width: '100%',
            height: '90%',
            backgroundColor: 'transparent',
          }}
        />
      </TouchableOpacity>

      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </SafeAreaView>
  );
};

export const renderFieldPicker = ({
  input,
  label,
  pickerSelected,
  enabled,
  onValueChange,
  data,
  meta: {touched, error},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <SafeAreaView style={{width: '100%', marginTop: 10}}>
      <Label
        style={{
          fontFamily: FSAlbert,
          fontSize: 15,
          color: hasError ? 'red' : '#666',
        }}>
        {label}
      </Label>
      <View
        style={{
          backgroundColor: 'white',
          height: 40,
          borderColor: hasError ? 'red' : '#ccc',
          borderWidth: 1,
          borderRadius: 5,
        }}>
        {Platform.OS == 'android' ? (
          <PickerAndroid
            {...input}
            renderHeader={(backAction) => (
              <Header>
                <Left>
                  <Button transparent onPress={backAction}>
                    <Icon name="arrow-back" style={{color: 'black'}} />
                  </Button>
                </Left>
                <Body style={{flex: 3}}>
                  <Title>{label}</Title>
                </Body>
                <Right />
              </Header>
            )}
            note={enabled ? false : true}
            mode="dropdown"
            style={{
              marginTop: Platform.OS == 'android' ? -6 : -4,

              fontFamily: fonts.primary.normal,
              color: enabled ? 'rgba(0, 0, 0, 0.8)' : 'rgba(0, 0, 0, 0.5)',
            }}
            enabled={enabled}
            selectedValue={pickerSelected}
            onValueChange={onValueChange}>
            {data}
          </PickerAndroid>
        ) : (
          <Picker
            {...input}
            renderHeader={(backAction) => (
              <Header>
                <Left>
                  <Button transparent onPress={backAction}>
                    <Icon name="arrow-back" style={{color: 'black'}} />
                  </Button>
                </Left>
                <Body style={{flex: 3}}>
                  <Title>{label}</Title>
                </Body>
                <Right />
              </Header>
            )}
            note={enabled == false ? true : false}
            mode="dropdown"
            style={{marginTop: -5, width: '100%'}}
            enabled={enabled}
            selectedValue={pickerSelected}
            placeholder="Silahkan Pilih"
            onValueChange={onValueChange}>
            {data}
          </Picker>
        )}
      </View>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </SafeAreaView>
  );
};

const labels = [
  'Data\nPemohon',
  'Informasi\nAlamat',
  'Informasi\nDomisili',
  'Informasi\nPekerjaan',
  'Emergency\nKontak',
  'Data\nBarang',
];

//add label indicator for summary in fde privy
const label = [
  'Data\nPemohon',
  'Informasi\nAlamat',
  'Informasi\nDomisili',
  'Informasi\nPekerjaan',
  'Emergency\nKontak',
  'Data\nBarang',
];

const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#5CB95D',
  stepStrokeWidth: 2,
  stepStrokeFinishedColor: '#5CB95D',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: '#5CB95D',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#5CB95D',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#5CB95D',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelFontFamily: FSAlbert,
  labelSize: 10,
  currentStepLabelColor: '#5CB95D',
};

class FdeAddMp extends Component {
  constructor(props) {
    super(props);
    this.submitForm = this.submitForm.bind(this);
    this.handleAlert = this.handleAlert.bind(this);
    this.state = {
      screenTitle: 'Isi Data Lengkap',
      page: 0,
      currentPosition: 0,
      userAccess: false,
      Isapproved: '1',
      Lobcode: LOB,
      Dtmcrt: moment().utc().subtract(6, 'days').format('YYYY-MM-DD'),
      Dtmcrtto: moment().utc().format('YYYY-MM-DD'),
      uploadKtp: false,
      uploadDiri: false,
      uploadFap: '',
      uploadPpbk: '',
      detailUser: false,
      userData: false,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      validateStatus: false,
      visible: false,
      lastDataRequest: false,
      lastDataLoading: false,
      lastOrderStatus: false,
      jenisKelamin: null,
      authKeyCredolab: false,
      cameraUploadKtpEnable: false,
      cameraUploadFotoKonsumenEnable: false,
      cameraUploadFapEnable: false,
      cameraUploadPpbkEnable: false,
      codeCabang: '',
      fdePrivy: '',
      ktpUri: false,
      uploadDiriUri: false,
      screenTitleSummary: 'Summary',
      userToken: false,
      dataPrivy: false,
      verificationScreenEnable: false,
      verifikasiWajah: false,
      TokenSandia : '',
      orderID :''
    };
  }

  componentDidMount = async () => {
    const {
      qdeDetailData,
      ddeEducation,
      ddeGender,
      ddeMaritalStatus,
      ddeRelationship,
      ddeReligion,
      ddeHouseOwnership,
      ddeOfficeOwnership,
      ddeLegalProvince,
      ddeResidenceProvince,
      ddeJobProvince,
      ddeJobCategory,
      ddeJobStatus,
      ddeLamaUsahaTahun,
      ddeLamaUsahaBulan,
      ddeKontrakTahun,
      getAuthkey,
      ddeKontrakBulan,
      ddeJaminanJenis,
      ddeJenis,
      ddeFinancingPurpose,
      ddeSouceFunds,
      getCabangStatus,
      generateToken,
    } = this.props;

    //lemparan dari order id gue set state untuk di consume di didUpdate
    var orderTrxHId = this.props.route.params.params;
    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) {
      this.setState({userToken: userToken, orderID : orderTrxHId});
    }

    analytics().logScreenView({
      screen_class: 'screenFdeAddMultiproduct',
      screen_name: 'screenFdeAddMultiproduct',
    });

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

    const jsonUserData = await AsyncStorage.getItem('userData');
    if (jsonUserData) {
      var userData = JSON.parse(jsonUserData);
      this.setState({userData: userData});
    }

    const jsonDetailUser = await AsyncStorage.getItem('detailUser');
    if (jsonDetailUser) {
      var detailUser = JSON.parse(jsonDetailUser);
      this.setState({detailUser: detailUser});
    }

    if (this.state.userData) {
      if (this.state.userData.Usergroupid == '1') {
        this.setState({
          alertFor: 'EmptyNik',
          alertShowed: true,
          alertMessage: 'Anda Belum Mengisi NIK',
          alertTitle: 'Informasi',
          alertType: 'info',
          alertDoneText: 'OK',
        });
      }
    } else {
      this.setState({
        alertFor: 'NoLoginAccess',
        alertShowed: true,
        alertMessage: 'Anda Belum Login',
        alertTitle: 'Informasi',
        alertType: 'info',
        alertDoneText: 'OK',
      });
    }
    // di sini gue tambahin untuk hit generate token sandia
    generateToken(userToken);
    // qdeDetailData({Ordertrxhid: this.props.route.params.params}, this.state.TokenSandia);
    ddeEducation({MasterTypeCode: CODE_EDUCATION});
    ddeGender({MasterTypeCode: CODE_GENDER});
    ddeMaritalStatus({MasterTypeCode: CODE_MARITAL});
    ddeRelationship({MasterTypeCode: CODE_RELATIONSHIP});
    ddeReligion({MasterTypeCode: CODE_RELIGION});

    ddeHouseOwnership({MasterTypeCode: CODE_HOUSE_OWNERSHIP});
    ddeOfficeOwnership({MasterTypeCode: CODE_OFFICE_OWNERSHIP});

    ddeLegalProvince({});
    ddeResidenceProvince({});
    ddeJobProvince({});
    getAuthkey({});

    ddeJobCategory({MasterTypeCode: CODE_JOB_CATEGORY});
    ddeJobStatus({MasterTypeCode: CODE_JOB_STATUS});
    ddeLamaUsahaTahun({MasterTypeCode: CODE_YEAR});
    ddeLamaUsahaBulan({MasterTypeCode: CODE_MONTH});
    ddeKontrakTahun({MasterTypeCode: CODE_YEAR_PERIOD});
    ddeKontrakBulan({MasterTypeCode: CODE_MONTH});
    ddeJenis({LOB: LOB, Jaminan: substringCircum(CODE_JAMINAN)});
    ddeFinancingPurpose({MasterTypeCode: CODE_FINANCING_PURPOSE});
    ddeSouceFunds({MasterTypeCode: CODE_SOURCE_FUNDS});

    //getAuthKey for credolab
    getAuthkey();

    //flag for fde privy nanti berubah response dari BE
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      //Bikin kalo lagi di screen information, dia balik lagi ke summary bukan ke yang lain
      if (this.state.screenTitleSummary === 'Informasi Penting') {
        this.setState({screenTitleSummary: 'Summary'});
        return true;
      }
      //bikin kalo lagi di screen liveness check, dia teken back button maka balik ke form
      if (this.state.verificationScreenEnable) {
        this.verificationFaceHandle();
        return true;
      } else {
        this.props.navigation.goBack();
        this.props.resetForm('formFdeMpAdd');
        return true;
      }
    }
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      qdeDetailData_Error,
      qdeDetailData_Result,
      qdeDetailData,
      ddeInsert_Error,
      ddeInsert_Result,
      qdePersonalDetailData_Result,
      qdePersonalDetailData_Error,
      qdePersonalDetailData_Loading,	
      otpValidateResult,	
      otpValidateError,
      getAuthkey_Result,
      mpMappingRisk,
      getAuthkey_Error,
      getCabangStatus_Result,
      getCabangStatus_Error,
      getCabangStatus_Loading,
      getCabangStatus,
      submitPrivy_loading,
      submitPrivy_result,
      submitPrivy_error,
      refreshTokenResult,
      refreshTokenError,
      generateTokenResult,
      generateTokenError,
    } = this.props;
    const {detailUser, userToken, orderID} = this.state;
    if (
      generateTokenError !== null &&
      prevProps.generateTokenError !== generateTokenError
    ) {
      console.log("generateTokenError ", generateTokenError);
    }

    if (
      generateTokenResult !== null &&
      prevProps.generateTokenResult !== generateTokenResult
    ) {
      //ketika generate token sandia berhasil akan hit qdeDetailData
      console.log("generateTokenResult ", generateTokenResult);
      if(this.state.userData){
        qdeDetailData({Ordertrxhid: orderID}, generateTokenResult.Sandiatoken);
      }
    }
    
    //pemasangan refresh token untuk submitPrivy, error: token expired --> renew token --> check mana yang error --> api tembak lagi
    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      await handleRefreshToken(refreshTokenResult.Refreshtoken);
      this.setState({userToken: refreshTokenResult.Refreshtoken});
      if (submitPrivy_error) {
        this.props.submitPrivy(this.state.dataPrivy, this.state.userToken);
      }
    }

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      console.log('Error Refresh Token ', refreshTokenError.message);
    }

    if (
      submitPrivy_error !== null &&
      prevProps.submitPrivy_error !== submitPrivy_error
    ) {
      console.log(submitPrivy_error);

      const data = await jsonParse(submitPrivy_error.message);

      if (data) {
        this.validateRefreshToken(data);
      } else {
        this.setState({
          alertFor: 'Submit Privy Error',
          alertShowed: true,
          alertMessage:
            submitPrivy_error.message +
            ': Koneksi anda bermasalah untuk melanjutkan ke sesi berikutnya',
          alertTitle: 'Network Error',
          alertType: 'info',
          alertDoneText: 'OK',
        });
      }
    }

    if (
      submitPrivy_result !== null &&
      prevProps.submitPrivy_result !== submitPrivy_result
    ) {
      //saving token for FDE Piloting purpose
      console.log('SUBMIT PRIVY SUCCESS ');

      this.props.updateField(
        'formFdeMpAdd',
        'RegisterTokenPrivy',
        submitPrivy_result.data.register_token,
      );
      this.setState({page: this.state.page + 1});
      this.setState({currentPosition: this.state.currentPosition + 1});
    }

    if (
      submitPrivy_loading !== null &&
      prevProps.submitPrivy_loading !== submitPrivy_loading
    ) {
      console.log('submitPrivy ditembak!');
    }

    if (
      ddeInsert_Result !== null &&
      prevProps.ddeInsert_Result !== ddeInsert_Result
    ) {
      console.log('success ddeInsert ', ddeInsert_Result);
    }

    if (
      getAuthkey_Result !== null &&
      prevProps.getAuthkey_Result !== getAuthkey_Result
    ) {
      this.setState({authKeyCredolab: getAuthkey_Result});
    }

    if (
      qdeDetailData_Result !== null &&
      prevProps.qdeDetailData_Result !== qdeDetailData_Result
    ) {
      console.log("success qdeDetailData_Result ");
      if (detailUser.NIK !== qdeDetailData_Result.IdNo) {
        this.setState({
          alertFor: 'NoAccessToForm',
          alertShowed: true,
          alertMessage: 'Anda Tidak Memiliki Akses Untuk Form Ini',
          alertTitle: 'Informasi',
          alertType: 'info',
          alertDoneText: 'OK',
        });
      } else {
        if (
          qdeDetailData_Result.ORDER_STAT !== '3' &&
          qdeDetailData_Result.is_approved !== '1'
        ) {
          this.setState({
            alertFor: 'NoAccessToForm',
            alertShowed: true,
            alertMessage: 'Form ini bukan dalam tahap Lengkapi Data',
            alertTitle: 'Informasi',
            alertType: 'info',
            alertDoneText: 'OK',
          });
        } else {
          //state code cabang untuk kebutuhan api getCabangStatus
          getCabangStatus({
            LOB: LOB,
            Cabang: substringCircum(qdeDetailData_Result.Cabang),
          });
          console.log('get cabang status ditembak');
          this.props.updateField('formFdeMpAdd', 'channel', 'BAF MOBILE');
          this.props.updateField(
            'formFdeMpAdd',
            'orderTrxhId',
            qdeDetailData_Result.ORDER_TRX_H_ID,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'nomorOrder',
            qdeDetailData_Result.ORDER_NO,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'nomorAplikasi',
            qdeDetailData_Result.APP_NO,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'dataToko',
            substringSecondCircum(qdeDetailData_Result.SupplCode),
          );
          this.props.updateField(
            'formFdeMpAdd',
            'dataCabang',
            substringSecondCircum(qdeDetailData_Result.Cabang),
          );
          this.props.updateField(
            'formFdeMpAdd',
            'namaKonsumen',
            qdeDetailData_Result.Fullname,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'NIK',
            qdeDetailData_Result.IdNo,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'namaIbuKandung',
            qdeDetailData_Result.NamaIbuKandung,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'tempatLahir',
            qdeDetailData_Result.TempatLahir,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'tanggalLahir',
            qdeDetailData_Result.TanggalLahir,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'noHandphone1',
            qdeDetailData_Result.Handphone1,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'noHandphone2',
            qdeDetailData_Result.Handphone2,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'noHandphone3',
            qdeDetailData_Result.Handphone3,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'email',
            qdeDetailData_Result.Email1,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'dataSumberAplikasi',
            substringSecondCircum(qdeDetailData_Result.SourceAplikasi),
          );
          this.props.updateField(
            'formFdeMpAdd',
            'kodeToko',
            substringCircum(qdeDetailData_Result.SupplCode),
          );
          this.props.updateField(
            'formFdeMpAdd',
            'namaToko',
            substringSecondCircum(qdeDetailData_Result.SupplCode),
          );
          this.props.updateField(
            'formFdeMpAdd',
            'kodeCabang',
            substringCircum(qdeDetailData_Result.Cabang),
          );
          this.props.updateField(
            'formFdeMpAdd',
            'namaCabang',
            substringSecondCircum(qdeDetailData_Result.Cabang),
          );
          this.props.updateField('formFdeMpAdd', 'jaminanJenis', CODE_JAMINAN);
          this.props.updateField(
            'formFdeMpAdd',
            'jenisDisabled',
            substringSecondCircum(qdeDetailData_Result.Jenis),
          );
          this.props.updateField(
            'formFdeMpAdd',
            'merkDisabled',
            substringSecondCircum(qdeDetailData_Result.Merk),
          );
          this.props.updateField(
            'formFdeMpAdd',
            'jenis',
            qdeDetailData_Result.Jenis,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'merk',
            qdeDetailData_Result.Merk,
          );

          this.props.updateField(
            'formFdeMpAdd',
            'ukuran',
            qdeDetailData_Result.Ukuran,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'warna',
            qdeDetailData_Result.Warna,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'tipe',
            qdeDetailData_Result.Model,
          );

          this.props.updateField(
            'formFdeMpAdd',
            'hargaBarang',
            qdeDetailData_Result.Hargabrg,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'risk',
            qdeDetailData_Result.Risk,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'uangMukaAmount',
            qdeDetailData_Result.DP,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'jangkaWaktu',
            qdeDetailData_Result.Tenor,
          );
          this.props.updateField(
            'formFdeMpAdd',
            'financingPurpose',
            'PM^Multiguna',
          );
          this.props.updateField(
            'formFdeMpAdd',
            'financingPurposeView',
            'Multiguna',
          );
          this.props.updateField(
            'formFdeMpAdd',
            'kodeArea',
            substringNoHpFirstCircum(qdeDetailData_Result.Handphone1),
          );
          this.props.updateField(
            'formFdeMpAdd',
            'telepon',
            substringNoHpSecondCircum(qdeDetailData_Result.Handphone1),
          );
          mpMappingRisk({
            RiskType: qdeDetailData_Result.Risk,
            Hargabrg: qdeDetailData_Result.Hargabrg,
          });
        }
      }
    }

    if (
      getCabangStatus_Result !== null &&
      prevProps.getCabangStatus_Result !== getCabangStatus_Result
    ) {
      //Dalam proses develop branch piloting sampe kita dapet data yang balikin 1, maka ini kita comment dulu.
      this.setState({fdePrivy: getCabangStatus_Result.UsePrivy});

      if (getCabangStatus_Result.UsePrivy) {
        this.setState({page: 1});
      }
    }

    if (
      getCabangStatus_Error !== null &&
      prevProps.getCabangStatus_Error !== getCabangStatus_Error
    ) {
      console.log('error fetch api getCabangStatus', getCabangStatus_Error);
    }

    if (
      qdePersonalDetailData_Result !== null &&
      prevProps.qdePersonalDetailData_Result !== qdePersonalDetailData_Result
    ) {
      this.setState({lastOrderStatus: true});
      this.setState({
        alertFor: 'qdePersonalDetailData_Result',
        alertShowed: true,
        alertMessage: 'Berhasil load data terakhir Anda.',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
      });
      this.props.updateField(
        'formFdeMpAdd',
        'namaPanggilan',
        qdePersonalDetailData_Result.NamaPanggilan
          ? qdePersonalDetailData_Result.NamaPanggilan
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'masaBerlakuKtp',
        qdePersonalDetailData_Result.JatuhTempoKTP
          ? qdePersonalDetailData_Result.JatuhTempoKTP
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'telepon',
        qdePersonalDetailData_Result.TeleponRumah
          ? qdePersonalDetailData_Result.TeleponRumah
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'kodeArea',
        qdePersonalDetailData_Result.TeleponRumahArea
          ? qdePersonalDetailData_Result.TeleponRumahArea
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'jenisKelamin',
        qdePersonalDetailData_Result.JenisKelamin
          ? qdePersonalDetailData_Result.JenisKelamin
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'pendidikanTerakhir',
        qdePersonalDetailData_Result.PendidikanTerakhir
          ? qdePersonalDetailData_Result.PendidikanTerakhir
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'agama',
        qdePersonalDetailData_Result.Agama
          ? qdePersonalDetailData_Result.Agama
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'statusPernikahan',
        qdePersonalDetailData_Result.StatusPernikahan
          ? qdePersonalDetailData_Result.StatusPernikahan
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'ktpAlamat',
        qdePersonalDetailData_Result.KTPAlamat
          ? qdePersonalDetailData_Result.KTPAlamat
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'ktpRt',
        qdePersonalDetailData_Result.KTPRT
          ? qdePersonalDetailData_Result.KTPRT
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'ktpRw',
        qdePersonalDetailData_Result.KTPRW
          ? qdePersonalDetailData_Result.KTPRW
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'ktpStatusRumah',
        qdePersonalDetailData_Result.KTPKepemilikanRumah
          ? qdePersonalDetailData_Result.KTPKepemilikanRumah
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'ktpMasaBerlakuSewa',
        qdePersonalDetailData_Result.KTPKontrakExpDate
          ? qdePersonalDetailData_Result.KTPKontrakExpDate
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'ktpLamaTinggalTahun',
        qdePersonalDetailData_Result.KTPLamaMenempatiThn
          ? qdePersonalDetailData_Result.KTPLamaMenempatiThn
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'ktpLamaTinggalBulan',
        qdePersonalDetailData_Result.KTPLamaMenempatiBln
          ? qdePersonalDetailData_Result.KTPLamaMenempatiBln
          : '',
      );

      this.props.updateField(
        'formFdeMpAdd',
        'residenceAlamat',
        qdePersonalDetailData_Result.DomisiliAlamat
          ? qdePersonalDetailData_Result.DomisiliAlamat
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'residenceRt',
        qdePersonalDetailData_Result.DomisiliRT
          ? qdePersonalDetailData_Result.DomisiliRT
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'residenceRw',
        qdePersonalDetailData_Result.DomisiliRW
          ? qdePersonalDetailData_Result.DomisiliRW
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'residenceStatusRumah',
        qdePersonalDetailData_Result.DomisiliKepemilikanRumah
          ? qdePersonalDetailData_Result.DomisiliKepemilikanRumah
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'residenceMasaBerlakuSewa',
        qdePersonalDetailData_Result.DomisiliKontrakExpDate
          ? qdePersonalDetailData_Result.DomisiliKontrakExpDate
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'residenceLamaTinggalTahun',
        qdePersonalDetailData_Result.DomisiliLamaMenempatiThn
          ? qdePersonalDetailData_Result.DomisiliLamaMenempatiThn
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'residenceLamaTinggalBulan',
        qdePersonalDetailData_Result.DomisiliLamaMenempatiBln
          ? qdePersonalDetailData_Result.DomisiliLamaMenempatiBln
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'residenceLamaTinggalBulan',
        qdePersonalDetailData_Result.DomisiliLamaMenempatiBln
          ? qdePersonalDetailData_Result.DomisiliLamaMenempatiBln
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'jobCategory',
        qdePersonalDetailData_Result.StatusKepegawaian
          ? qdePersonalDetailData_Result.StatusKepegawaian
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'jobCategory',
        qdePersonalDetailData_Result.StatusKepegawaian
          ? qdePersonalDetailData_Result.StatusKepegawaian
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'jobStatus',
        qdePersonalDetailData_Result.JobStatus
          ? qdePersonalDetailData_Result.JobStatus
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'jobType',
        qdePersonalDetailData_Result.TipePekerjaan
          ? qdePersonalDetailData_Result.TipePekerjaan
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'jobPosition',
        qdePersonalDetailData_Result.Jabatan
          ? qdePersonalDetailData_Result.Jabatan
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'industryType',
        qdePersonalDetailData_Result.BidangUsaha
          ? qdePersonalDetailData_Result.BidangUsaha
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'namaPerusahaan',
        qdePersonalDetailData_Result.NamaPerusahaan
          ? qdePersonalDetailData_Result.NamaPerusahaan
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'lamaUsahaBulan',
        qdePersonalDetailData_Result.LamaUsahaBln
          ? qdePersonalDetailData_Result.LamaUsahaBln
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'lamaUsahaTahun',
        qdePersonalDetailData_Result.LamaUsahaThn
          ? qdePersonalDetailData_Result.LamaUsahaThn
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'kontrakBulan',
        qdePersonalDetailData_Result.PeriodeKontrakBln
          ? qdePersonalDetailData_Result.PeriodeKontrakBln
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'kontrakTahun',
        qdePersonalDetailData_Result.PeriodeKontrakThn
          ? qdePersonalDetailData_Result.PeriodeKontrakThn
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'jobAlamat',
        qdePersonalDetailData_Result.JobAlamat
          ? qdePersonalDetailData_Result.JobAlamat
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'jobRt',
        qdePersonalDetailData_Result.JobRT
          ? qdePersonalDetailData_Result.JobRT
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'jobRw',
        qdePersonalDetailData_Result.JobRW
          ? qdePersonalDetailData_Result.JobRW
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'jobKodeArea',
        qdePersonalDetailData_Result.JobTeleponArea
          ? qdePersonalDetailData_Result.JobTeleponArea
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'jobTelp',
        qdePersonalDetailData_Result.JobTelepon
          ? qdePersonalDetailData_Result.JobTelepon
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'jobTelpEkstensi',
        qdePersonalDetailData_Result.JobTeleponExt
          ? qdePersonalDetailData_Result.JobTeleponExt
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'statusUsaha',
        qdePersonalDetailData_Result.StatusTempatUsaha
          ? qdePersonalDetailData_Result.StatusTempatUsaha
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'penghasilanPerbulan',
        qdePersonalDetailData_Result.Penghasilan
          ? qdePersonalDetailData_Result.Penghasilan
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'namaEmergencyKontak1',
        qdePersonalDetailData_Result.NamaEC1
          ? qdePersonalDetailData_Result.NamaEC1
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'noHpEmergencyKontak1',
        qdePersonalDetailData_Result.HandPhoneEC1
          ? qdePersonalDetailData_Result.HandPhoneEC1
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'hubunganEmergencyKontak1',
        qdePersonalDetailData_Result.HubunganEC1
          ? qdePersonalDetailData_Result.HubunganEC1
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'alamatEmergencyKontak1',
        qdePersonalDetailData_Result.AlamatEC1
          ? qdePersonalDetailData_Result.AlamatEC1
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'namaEmergencyKontak2',
        qdePersonalDetailData_Result.NamaEC2
          ? qdePersonalDetailData_Result.NamaEC2
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'noHpEmergencyKontak2',
        qdePersonalDetailData_Result.HandPhoneEC2
          ? qdePersonalDetailData_Result.HandPhoneEC2
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'hubunganEmergencyKontak2',
        qdePersonalDetailData_Result.HubunganEC2
          ? qdePersonalDetailData_Result.HubunganEC2
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'alamatEmergencyKontak2',
        qdePersonalDetailData_Result.AlamatEC2
          ? qdePersonalDetailData_Result.AlamatEC2
          : '',
      );
      this.props.updateField(
        'formFdeMpAdd',
        'sourceFunds',
        qdePersonalDetailData_Result.SumberDana
          ? qdePersonalDetailData_Result.SumberDana
          : '',
      );
    }

    if (
      qdeDetailData_Error !== null &&
      prevProps.qdeDetailData_Error !== qdeDetailData_Error
    ) {
      this.setState({
        alertFor: 'qdeDetailData_Error',
        alertShowed: true,
        alertMessage: qdeDetailData_Error.message,
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
      });
    }

    if (
      qdePersonalDetailData_Error !== null &&
      prevProps.qdePersonalDetailData_Error !== qdePersonalDetailData_Error
    ) {
      if (qdePersonalDetailData_Error.message == 'Data Not Found') {
        this.setState({
          alertFor: 'qdePersonalDetailData_Error',
          alertShowed: true,
          alertMessage: 'Anda belum memiliki data sebelumnya',
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      } else {
        this.setState({
          alertFor: 'qdePersonalDetailData_Error',
          alertShowed: true,
          alertMessage: qdePersonalDetailData_Error.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
    }

    if (
      getAuthkey_Error !== null &&
      prevProps.getAuthkey_Error !== getAuthkey_Error
    ) {
      console.log('gagal mendapatkan URL Credolabs');
      // this.setState({
      //   alertFor: 'getAuthkey_Error',
      //   alertShowed: true,
      //   alertMessage: 'Gagal Mendapatkan URL Credolabs',
      //   alertTitle: 'Gagal',
      //   alertType: 'error',
      //   alertDoneText: 'OK',
      // });
    }
  };

  defaultTemplate = () => {
    return (
      <Content
        style={{
          flexDirection: 'row',
          alignSelf: 'center',
          alignContent: 'center',
        }}>
        <Spinner color="#002f5f" />
        <Text style={{color: 'grey'}}>Sedang memuat data</Text>
      </Content>
    );
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  catchValidateStatus = async (result) => {
    await this.setState({
      validateStatus: result,
    });
  };

  callLivenessCheck = () => {
    if(Platform.OS === 'android'){
      NativeModules.PrivyLivenessCheckActivityManager.switchToAnotherActivity(
        {strData: 'Data transmitted by RN to Android'},
        (onDone) => {
          this.setState({
            verifikasiWajah: onDone.result ? onDone.result : onDone,
          });
          this.props.updateField(
            'formFdeMpAdd',
            'verifikasiWajah',
            onDone.result ? onDone.result : onDone,
          );
  
          this.verificationFaceHandle();
        },
        (onCancel) => {
          this.setState({
            alertFor: 'callLivenessCheck Fail',
            alertShowed: true,
            alertMessage: `Verifikasi wajah gagal, silahkan melakukan verifikasi sekali lagi.`,
            alertTitle: 'Gagal',
            alertType: 'error',
            alertDoneText: 'OK',
          });
        },
      );
    }else{
      //comment this V  V V to disable privy 
      NativeModules.PrivyModul.doLiveness(data => {
        if(data === '0' || data === '0.0' || data === null || data === 0.0 || data === 0){
          this.setState({
            alertFor: 'callLivenessCheck',
            alertShowed: true,
            alertMessage: `Verifikasi wajah gagal, silahkan melakukan verifikasi sekali lagi.`,
            alertTitle: 'Gagal',
            alertType: 'error',
            alertDoneText: 'OK',
          });
        }
        else{
          console.log("Data = ",data )
          this.setState({
            verifikasiWajah: data,
          });
          this.props.updateField(
            'formFdeMpAdd',
            'verifikasiWajah',
            data,
          );
          this.setState({
            alertFor: 'callLivenessCheck',
            alertShowed: true,
            alertMessage: `Verifikasi wajah berhasil`,
            alertTitle: 'Berhasil',
            alertType: 'success',
            alertDoneText: 'OK',
          });
          
        }
      })

      //uncomment this V V V if you disable privy
      //return null
      
    }
  };

  //switch antara screen liveness check sama bukan
  verificationFaceHandle = () => {
    if (this.state.verificationScreenEnable) {
      this.setState({verificationScreenEnable: false});
    } else {
      this.setState({verificationScreenEnable: true});
    }
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
    if (
      this.state.alertFor === 'NoLoginAccess' ||
      this.state.alertFor === 'NoAccessToForm' ||
      this.state.alertFor == 'qdeDetailData_Error'
    ) {
      this.goBack();
      this.props.resetForm('formFdeMpAdd');
    } else if (
      this.state.alertFor === 'EmptyNik' ||
      this.state.alertFor === 'OtpExpired'
    ) {
      this.props.navigation.navigate('Home');
      this.props.resetForm('formFdeMpAdd');
    } else if (this.state.alertFor === 'ddeInsert_Result') {
      this.props.navigation.navigate('OrderList');
      this.props.resetForm('formFdeMpAdd');
    } else if (this.state.alertFor === 'ddeInsert_Error') {
      this.previousPage();
      this.props.resetForm('formFdeMpAdd');
    } else if (this.state.alertFor === 'callLivenessCheck') {
      this.verificationFaceHandle();
    }
    this.setState({alertFor: null});
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handleAlert(value) {
    this.setState({
      alertFor: value.alertFor,
      alertShowed: value.alertShowed,
      alertMessage: value.alertMessage,
      alertTitle: value.alertTitle,
      alertType: value.alertType,
      alertDoneText: value.alertDoneText,
    });
  }

  validateRefreshToken = async (data) => {
    console.log('enter validate refrsh token');
    if (data) {
      if (data.message == MESSAGE_TOKEN_EXP) {
        console.log('hit refresh token');
        this.props.refreshToken(this.state.userToken);
      }
    }
  };

  submitFDEPrivy = async (data) => {
    // submit privy nya udah gue cek, dan berhasil
    const {submitPrivy} = this.props;
    var dataPrivy = {
      //mainin di sini kalo mau develop pilloting reference numbernya biar gak gagal submit
      // reference_number: '130MPP11012549731',
      reference_number: data.nomorAplikasi,
      //fix: sebelumnya pake detailUser, sepertinya lupa kalo di form FDE ada email
      email: data.email,
      ktpFile: this.state.uploadKtp,
      selfieFile: this.state.uploadDiri,
      nik: data.NIK,
      nama: data.namaKonsumen,
      tanggal_lahir: data.tanggalLahir,
      phone: data.noHandphone1,
    };

    //in case error happen because of token expired
    this.setState({dataPrivy: dataPrivy});

    //buat cek data base64 udah bener atau belum
    console.log(trimLineBreak(JSON.stringify(dataPrivy)));
    console.log(this.state.userToken);
    submitPrivy(dataPrivy, this.state.userToken);
  };

  nextPage = () => {
    this.setState({page: this.state.page + 1});
    this.setState({currentPosition: this.state.currentPosition + 1});
  };

  previousPage = () => {
    this.setState({page: this.state.page - 1});
    this.setState({currentPosition: this.state.currentPosition - 1});
  };

  //ini gue tambahin fucntion untuk navigasi screen jika ada screen yang mau di revisi di summary screen dan udah gue test
  navPage = (data) => {
    switch (data) {
      case 'Data Pemohon':
        this.setState({page: this.state.page - 6});
        this.setState({currentPosition: this.state.currentPosition - 6});
        break;

      case 'Informasi Alamat':
        this.setState({page: this.state.page - 5});
        this.setState({currentPosition: this.state.currentPosition - 5});
        break;

      case 'Informasi Domisili':
        this.setState({page: this.state.page - 4});
        this.setState({currentPosition: this.state.currentPosition - 4});
        break;

      case 'Informasi Pekerjaan':
        this.setState({page: this.state.page - 3});
        this.setState({currentPosition: this.state.currentPosition - 3});
        break;

      case 'Emergency Kontak':
        this.setState({page: this.state.page - 2});
        this.setState({currentPosition: this.state.currentPosition - 2});
        break;

      case 'Data Barang':
        this.setState({page: this.state.page - 1});
        this.setState({currentPosition: this.state.currentPosition - 1});
        break;

      default:
    }
  };

  goBack = () => {
    this.props.navigation.goBack();
    this.props.resetForm('formFdeMpAdd');
  };

  goHome = () => {
    this.props.navigation.navigate('Home');
  };

  goOrderList = () => {
    this.props.navigation.navigate('OrderList');
    this.props.resetForm('formFdeMpAdd');
  };

  goInformation = () => {
    this.props.navigation.navigate('Information');
  };

  getUploadKtp = (data) => {
    this.setState({uploadKtp: data});
  };

  getUploadDiri = (data) => {
    this.setState({uploadDiri: data});
  };

  getUploadFap = (data) => {
    this.setState({uploadFap: data});
  };

  getUploadPpbk = (data) => {
    this.setState({uploadPpbk: data});
  };

  requestLastData = () => {
    this.setState({
      alertShowed: true,
      alertMessage: 'Fitur data terakhir belum tersedia',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'Oke',
    });
  };

  submitForm(data) {
    const {ddeInsert} = this.props;

    var dataMerge = {
      AppNo: data.nomorAplikasi,
      NamaPanggilan: data.namaPanggilan ? data.namaPanggilan.toUpperCase() : '',
      JatuhTempoKTP: data.masaBerlakuKtp ? data.masaBerlakuKtp : '',
      JenisKelamin: data.jenisKelamin,
      PendidikanTerakhir: data.pendidikanTerakhir,
      PendidikanTerakhirLainnya: data.pendidikanTerakhirLainnya
        ? data.pendidikanTerakhirLainnya
        : '',
      TeleponRumah: data.telepon,
      HandPhone2: data.noHandphone2 ? data.noHandphone2 : '',
      HandPhone3: data.noHandphone3 ? data.noHandphone3 : '',
      StatusPernikahan: data.statusPernikahan,
      NamaPasangan: data.namaPasangan ? data.namaPasangan.toUpperCase() : '',
      HandPhonePasangan: data.noHandphonePasangan
        ? data.noHandphonePasangan
        : '',
      Agama: data.agama,
      NamaIbuKandung: data.namaIbuKandung.toUpperCase(),

      KTPAlamat: data.ktpAlamat.toUpperCase(),
      KTPKelurahan: substringSecondCircum(data.ktpKelurahan),
      KTPKecamatan: substringSecondCircum(data.ktpKecamatan),
      KTPKotamadya: substringSecondCircum(data.ktpKotaKabupaten),
      KTPPropinsi: substringSecondCircum(data.ktpProvinsi),
      KTPKodePos: data.ktpKodePos,
      KTPRT: data.ktpRt,
      KTPRW: data.ktpRw,
      KTPKepemilikanRumah: data.ktpStatusRumah,
      KTPKontrakExpDate: data.ktpMasaBerlakuSewa ? data.ktpMasaBerlakuSewa : '',
      KTPLamaMenempatiBln: data.ktpLamaTinggalBulan,

      DomisiliAlamat: data.residenceAlamat.toUpperCase(),
      DomisiliKelurahan: substringSecondCircum(data.residenceKelurahan),
      DomisiliKecamatan: substringSecondCircum(data.residenceKecamatan),
      DomisiliKotamadya: substringSecondCircum(data.residenceKotaKabupaten),
      DomisiliPropinsi: substringSecondCircum(data.residenceProvinsi),
      DomisiliKodePos: data.residenceKodePos,
      DomisiliRT: data.residenceRt,
      DomisiliRW: data.residenceRw,
      DomisiliKepemilikanRumah: data.residenceStatusRumah,
      DomisiliKontrakExpDate: data.residenceMasaBerlakuSewa
        ? data.residenceMasaBerlakuSewa
        : '',
      DomisiliLamaMenempatiBln: data.residenceLamaTinggalBulan,

      StatusKepegawaian: data.jobCategory,
      TipePekerjaan: data.jobType,
      Jabatan: data.jobPosition,
      BidangUsaha: data.industryType,
      NamaPerusahaan: data.namaPerusahaan.toUpperCase(),
      LamaUsahaBln: data.lamaUsahaBulan,
      PeriodeKontrakBln: data.kontrakBulan ? data.kontrakBulan : '',

      JobAlamat: data.jobAlamat.toUpperCase(),
      JobKelurahan: substringSecondCircum(data.jobKelurahan),
      JobKecamatan: substringSecondCircum(data.jobKecamatan),
      JobKotamadya: substringSecondCircum(data.jobKotaKabupaten),
      JobPropinsi: substringSecondCircum(data.jobProvinsi),
      JobKodePos: data.jobKodePos,
      JobRT: data.jobRt,
      JobRW: data.jobRw,
      JobTelepon: data.jobTelp ? data.jobTelp : '',
      StatusTempatUsaha: data.statusUsaha,
      Penghasilan: data.penghasilanPerbulan,

      NamaEC1: data.namaEmergencyKontak1.toUpperCase(),
      HandPhoneEC1: data.noHpEmergencyKontak1,
      HubunganEC1: data.hubunganEmergencyKontak1,
      AlamatEC1: data.alamatEmergencyKontak1.toUpperCase(),
      NamaEC2: data.namaEmergencyKontak2
        ? data.namaEmergencyKontak2.toUpperCase()
        : '',
      HandPhoneEC2: data.noHpEmergencyKontak2 ? data.noHpEmergencyKontak2 : '',
      HubunganEC2: data.hubunganEmergencyKontak2
        ? data.hubunganEmergencyKontak2
        : '',
      AlamatEC2: data.alamatEmergencyKontak2
        ? data.alamatEmergencyKontak2.toUpperCase()
        : '',

      SupplierBranch: data.kodeToko + '^' + data.namaToko,

      JaminanJenis: data.jenis,
      Merk: data.merk,
      Ukuran: data.ukuran.toUpperCase(),
      Warna: data.warna.toUpperCase(),
      TipeBarang: data.tipe.toUpperCase(),
      Peruntukan: data.financingPurpose,
      HargaBarang: data.hargaBarang,
      DP: data.uangMukaAmount,
      SchemeId: data.financialSchemeId,
      TipePembayaran: data.tipePembayaran,
      TipeBiayaAdmin: data.tipeBiayaAdmin,
      Bunga: data.bungaEfektif,
      JangkaWaktu: data.jangkaWaktu,
      BiayaAdmin: data.biayaAdmin,
      JumlahPembiayaan: data.jumlahPembiayaan,
      BiayaAsuransi: data.biayaAsuransi,
      Angsuran: data.angsuran,
      TotalPembayaranPertama: data.pembayaranPertama,
      SumberDana: data.sourceFunds,

      DomisiliLamaMenempatiThn: data.residenceLamaTinggalTahun,
      KTPLamaMenempatiThn: data.ktpLamaTinggalTahun,
      LamaUsahaThn: data.lamaUsahaTahun,
      PeriodeKontrakThn: data.kontrakTahun ? data.kontrakTahun : '',

      JobTeleponArea: data.jobKodeArea ? data.jobKodeArea : '',
      JobTeleponExt: data.jobTelpEkstensi ? data.jobTelpEkstensi : '',
      TeleponRumahArea: data.kodeArea,

      UploadFAP: this.state.uploadFap,
      UploadFotoKonsumen: this.state.uploadDiri,
      UploadKTP: this.state.uploadKtp,
      UploadPPBK: this.state.uploadPpbk,

      Orderno: data.nomorOrder,
      Cabang: data.kodeCabang + '^' + data.namaCabang,
      JaminanCategory: data.jaminanJenis,

      MobileUserEmailFDE: this.state.detailUser.Email,
      MobileUserFullNameFDE: this.state.detailUser.Username,

      JobStatus: data.jobStatus ? data.jobStatus : '',
      Channel: APP_NAME,

      TipeBiayaAsuransi: data.tipeBiayaAsuransi,
      ProgramId: data.programID,
      // di sini gue tambahim field di data merge submit fde
      UsePrivy: this.state.fdePrivy == 1 ? '1' : '0',
      RegisterTokenPrivy:
        this.state.fdePrivy == 1 ? data.RegisterTokenPrivy : '',
    };

    var arrayData = [];

    for (var key in dataMerge) {
      arrayData.push(key + '~' + dataMerge[key]);
    }

    // Json stringfy nya gue tambahin replace
    var dataMergeString = JSON.stringify(arrayData).replace(/\\n/g, '');
    var ValuesData = mergeValuesData(dataMergeString);

    // console.log("Data Values Data");
    // console.log(ValuesData);
    ddeInsert({
      Ordertrxhid: data.orderTrxhId,
      Valuesdata: ValuesData,
      Usrcrt: this.state.userAccess.Userid,
    });
  }

  toggleModal = () => {
    this.setState({visible: false});
  };

  openDatePicker = () => {
    this.setState({visible: true});
  };

  lastDataRequest = async () => {
    const {qdePersonalDetailData, qdeDetailData_Result} = this.props;
    await this.setState({
      lastDataRequest: !this.state.lastDataRequest,
    });

    if (this.state.lastDataRequest) {
      qdePersonalDetailData({
        Idno: this.state.detailUser.NIK,
      });
    } else {
      this.setState({
        lastOrderStatus: false,
      });
    }
  };

  handlerCodeFill = (data) => {
    const {otpValidate} = this.props;
    otpValidate({
      Phoneno: this.state.detailUser.Phoneno,
      OTP: data,
    });
  };

  enableCameraUploadKtp = () => {
    this.setState({
      cameraUploadKtpEnable: true,
    });
  };

  enableCameraUploadFotoKonsumen = () => {
    this.setState({
      cameraUploadFotoKonsumenEnable: true,
    });
  };

  enableCameraUploadFap = () => {
    this.setState({
      cameraUploadFapEnable: true,
    });
  };

  //switch screen title di summary
  screenTitleSummarySwitch = () => {
    if (this.state.screenTitleSummary === 'Summary') {
      this.setState({screenTitleSummary: 'Informasi Penting'});
    } else {
      this.setState({screenTitleSummary: 'Summary'});
    }
  };

  enableCameraUploadPpbk = () => {
    this.setState({
      cameraUploadPpbkEnable: true,
    });
  };

  disableCameraUploadKtp = () => {
    this.setState({
      cameraUploadKtpEnable: false,
    });
  };

  disableCameraUploadFotoKonsumen = () => {
    this.setState({
      cameraUploadFotoKonsumenEnable: false,
    });
  };

  disableCameraUploadFap = () => {
    this.setState({
      cameraUploadFapEnable: false,
    });
  };

  disableCameraUploadPpbk = () => {
    this.setState({
      cameraUploadPpbkEnable: false,
    });
  };

  render() {
    const {
      page,
      ddeInsert_Loading,
      cameraUploadKtpEnable,
      cameraUploadFotoKonsumenEnable,
      cameraUploadFapEnable,
      cameraUploadPpbkEnable,
      fdePrivy,
    } = this.state;
    const {qdeDetailData_Result} = this.props;

    return (
      <Container style={{flex: 1}}>
        {/* di sini gue tambahin ternary, jika fdeprivy == 1 maka header di summry wizard hilang */}
        {(page == 7) & (fdePrivy == 1) ? (
          <View></View>
        ) : (
          <>
            {Platform.OS == 'android' ? (
              <SubHeaderAndroidBlue
                title={this.state.screenTitle}
                goBack={this.goBack}
              />
            ) : (
              <SubHeaderIosBlue
                title={this.state.screenTitle}
                goBack={this.goBack}
              />
            )}
            {!cameraUploadFotoKonsumenEnable &&
            !cameraUploadKtpEnable &&
            !cameraUploadFapEnable &&
            !cameraUploadPpbkEnable ? (
              <View style={styles.stepIndicatorContainer}>
                {this.showAlert()}
                <View
                  style={{
                    width: '100%',
                    backgroundColor: '#F2F2F2',
                    height: 90,
                    paddingTop: 20,
                  }}>
                  {/* add ternary in step indicator to add or min on the top head indicator */}
                  <StepIndicator
                    customStyles={customStyles}
                    currentPosition={this.state.currentPosition}
                    labels={this.state.fdePrivy == 1 ? label : labels}
                    stepCount={6}
                  />
                </View>
              </View>
            ) : null}
          </>
        )}

        {page === 0 && this.defaultTemplate()}
        {/* add ternary in this wizard */}
        {page === 1 && (
          <FirstWizard
            onSubmit={
              this.state.fdePrivy == 1 ? this.submitFDEPrivy : this.nextPage
            }
            verifikasiWajah={this.state.verifikasiWajah}
            callLivenessCheck={this.callLivenessCheck}
            verificationFaceHandle={this.verificationFaceHandle}
            verificationScreenEnable={this.state.verificationScreenEnable}
            loading={this.props.qdePersonalDetailData_Loading}
            requestLastData={this.requestLastData}
            visible={this.state.visible}
            openDatePicker={() => this.openDatePicker()}
            toggleModal={() => this.toggleModal()}
            lastDataRequest={() => this.lastDataRequest()}
            lastDataStatus={this.state.lastDataRequest}
            lastOrderStatus={this.state.lastOrderStatus}
            jenisKelamin={this.state.jenisKelamin}
            fdePrivy={this.state.fdePrivy}
            sendUploadKtp={this.getUploadKtp}
            sendUploadDiri={this.getUploadDiri}
            handleAlert={this.handleAlert}
            catchValidateStatus={this.catchValidateStatus}
            enableCameraUploadKtp={this.enableCameraUploadKtp}
            disableCameraUploadKtp={this.disableCameraUploadKtp}
            enableCameraUploadFotoKonsumen={() =>
              this.enableCameraUploadFotoKonsumen()
            }
            disableCameraUploadFotoKonsumen={
              this.disableCameraUploadFotoKonsumen
            }
            cameraUploadKtpEnable={cameraUploadKtpEnable}
            cameraUploadFotoKonsumenEnable={cameraUploadFotoKonsumenEnable}
          />
        )}

        {page === 2 && (
          <SecondWizard
            previousPage={this.state.fdePrivy == 1 ? null : this.previousPage}
            onSubmit={this.nextPage}
            visible={this.state.visible}
            openDatePicker={() => this.openDatePicker()}
            toggleModal={() => this.toggleModal()}
          />
        )}
        {page === 3 && (
          <ThirdWizard
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
            visible={this.state.visible}
            openDatePicker={() => this.openDatePicker()}
            toggleModal={() => this.toggleModal()}
          />
        )}
        {page === 4 && (
          <FourthWizard
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
          />
        )}
        {page === 5 && (
          <FifthWizard
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
          />
        )}
        {/* add ternary in this wizard */}
        {page === 6 && (
          <SixthWizard
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
            sendUploadKtp={this.getUploadKtp}
            sendUploadDiri={this.getUploadDiri}
            sendUploadFap={this.getUploadFap}
            sendUploadPpbk={this.getUploadPpbk}
            handleAlert={this.handleAlert}
            catchValidateStatus={this.catchValidateStatus}
            otpValidated={this.state.validateStatus}
            riskType={qdeDetailData_Result.Risk}
            enableCameraUploadKtp={this.enableCameraUploadKtp}
            disableCameraUploadKtp={this.disableCameraUploadKtp}
            enableCameraUploadFotoKonsumen={this.enableCameraUploadFotoKonsumen}
            disableCameraUploadFotoKonsumen={
              this.disableCameraUploadFotoKonsumen
            }
            enableCameraUploadFap={this.enableCameraUploadFap}
            disableCameraUploadFap={this.disableCameraUploadFap}
            enableCameraUploadPpbk={this.enableCameraUploadPpbk}
            disableCameraUploadPpbk={this.disableCameraUploadPpbk}
            cameraUploadKtpEnable={cameraUploadKtpEnable}
            cameraUploadFotoKonsumenEnable={cameraUploadFotoKonsumenEnable}
            cameraUploadFapEnable={cameraUploadFapEnable}
            cameraUploadPpbkEnable={cameraUploadPpbkEnable}
            fdePrivy={this.state.fdePrivy}
          />
        )}
        {page === 7 &&
          (this.state.fdePrivy == 1 ? (
            <SummaryWizard
              onSubmit={this.submitForm}
              navigatePage={this.navPage}
              goOrderList={this.goOrderList}
              goInformation={this.goInformation}
              previousPage={this.previousPage}
              //fix problem di information, perubahan screen title untuk ternary perubahan antar komponen
              screenTitle={this.state.screenTitleSummary}
              screenTitleSwitch={this.screenTitleSummarySwitch}
            />
          ) : (
            <OtpWizard
              // previousPage={this.previousPage}
              onSubmit={this.submitForm}
              catchValidateStatus={this.catchValidateStatus}
              handleAlert={this.handleAlert}
              goHome={this.goHome}
              goOrderList={this.goOrderList}
              prevScreen={this.previousPage}
              handlerCodeFill={this.handlerCodeFill}
              authKeyCredolab={this.state.authKeyCredolab}
            />
          ))}
      </Container>
    );
  }
}

FdeAddMp.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    qdeDetailData_Loading: state.qdeDetailData.loading,
    qdeDetailData_Result: state.qdeDetailData.result,
    qdeDetailData_Error: state.qdeDetailData.error,
    ddeInsert_Loading: state.ddeInsert.loading,
    ddeInsert_Result: state.ddeInsert.result,
    ddeInsert_Error: state.ddeInsert.error,
    formFdeMpAdd: state.form.formFdeMpAdd,
    qdePersonalDetailData_Loading: state.qdePersonalDetailData.loading,
    qdePersonalDetailData_Result: state.qdePersonalDetailData.result,
    qdePersonalDetailData_Error: state.qdePersonalDetailData.error,
    getAuthkey_Loading: state.getAuthkey.loading,
    getAuthkey_Result: state.getAuthkey.result,
    getAuthkey_Error: state.getAuthkey.error,

    getCabangStatus_Loading: state.getCabangStatus.loading,
    getCabangStatus_Result: state.getCabangStatus.result,
    getCabangStatus_Error: state.getCabangStatus.error,

    submitPrivy_loading: state.submitPrivy.loading,
    submitPrivy_result: state.submitPrivy.result,
    submitPrivy_error: state.submitPrivy.error,

    otpSmsLoading: state.otpSms.loading,
    otpSmsResult: state.otpSms.result,
    otpSmsError: state.otpSms.error,
    otpValidateLoading: state.otpValidate.loading,
    otpValidateResult: state.otpValidate.result,
    otpValidateError: state.otpValidate.error,
    mpMappingRiskResult: state.mpMappingRisk.result,
    mpMappingRiskLoading: state.mpMappingRisk.loading,
    mpMappingRiskError: state.mpMappingRisk.error,

    refreshTokenResult: state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
    generateTokenResult : state.generateToken.result,
    generateTokenError: state.generateToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      qdePersonalDetailData,
      ddeEducation,
      ddeGender,
      ddeMaritalStatus,
      getAuthkey,
      ddeRelationship,
      ddeReligion,
      ddeHouseOwnership,
      ddeOfficeOwnership,
      ddeLegalProvince,
      ddeResidenceProvince,
      ddeJobProvince,
      ddeJobCategory,
      ddeJobStatus,
      ddeLamaUsahaTahun,
      ddeLamaUsahaBulan,
      ddeKontrakTahun,
      ddeKontrakBulan,
      ddeJaminanJenis,
      ddeJenis,
      ddeFinancingPurpose,
      ddeSouceFunds,
      ddeInsert,
      qdeDetailData,
      getCabangStatus,
      otpSms,
      otpValidate,
      mpMappingRisk,
      submitPrivy,
      refreshToken,
      generateToken,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

export default connect(mapStateToProps, matchDispatchToProps)(FdeAddMp);
