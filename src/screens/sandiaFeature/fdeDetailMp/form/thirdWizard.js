import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { Field, reduxForm, change } from 'redux-form'
import { renderFieldDisabled,renderFieldHidden } from '../index'
import { Text, View, TouchableOpacity } from 'react-native';
import { Container, Spinner, Content, Form, Picker, ListItem, CheckBox } from 'native-base';
import styles from '../style';
import { HideWithKeyboard } from 'react-native-hide-with-keyboard';
import { FormDdeReviseValidate } from "../../../../validates/FormDdeReviseValidate";
import { substringCircum, substringSecondCircum } from "../../../../utils/utilization";



class ThirdWizard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userAccess: '',
      userToken: '',
    }
  }

  componentDidMount = () => {
    const {data} = this.props;
    this.props.updateField('formDdeDetail', 'residenceAlamat', data.DomisiliAlamat);
    this.props.updateField('formDdeDetail', 'residenceRt', data.DomisiliRT);
    this.props.updateField('formDdeDetail', 'residenceRw', data.DomisiliRW);
    this.props.updateField('formDdeDetail', 'residenceProvinsi', data.DomisiliPropinsi);
    this.props.updateField('formDdeDetail', 'residenceKotaKabupaten', data.DomisiliKotamadya);
    this.props.updateField('formDdeDetail', 'residenceKecamatan', data.DomisiliKecamatan);
    this.props.updateField('formDdeDetail', 'residenceKelurahan', data.DomisiliKelurahan);
    this.props.updateField('formDdeDetail', 'residenceKodePos', data.DomisiliKodePos);
    this.props.updateField('formDdeDetail', 'residenceStatusRumah', data.DomisiliKepemilikanRumah ? substringSecondCircum(data.DomisiliKepemilikanRumah) : '');
    this.props.updateField('formDdeDetail', 'residenceMasaBerlakuSewa', data.DomisiliKontrakExpDate);
    this.props.updateField('formDdeDetail', 'residenceLamaTinggalTahun', data.DomisiliLamaMenempatiThn);
    this.props.updateField('formDdeDetail', 'residenceLamaTinggalBulan', data.DomisiliLamaMenempatiBln);
   }

  render(){
    const { handleSubmit, previousPage, ddeHouseOwnership_Result, ddeResidenceProvince_Result } = this.props
    const { pickerResidenceStatusRumahDefault, pickerResidenceStatusRumahItem, pickerResidenceStatusRumahSelected, 
      pickerResidenceProvinsiDefault, pickerResidenceProvinsiItem, pickerResidenceProvinsiSelected, disableKotaKabupaten,
      disableKecamatan, disableKelurahan } = this.state

    return (
      <Container>
        <Content disableKBDismissScroll={true} style={{height:'75%',backgroundColor:'white',width:'100%',alignSelf:'center',paddingHorizontal:'4%'}}>
          <Form>
            <Field
                name="checkAddress"
                type="text"
                component={renderFieldHidden}
                label="Alamat Sesuai KTP"
            />
            
            <Field
              name="residenceAlamat"
              type="text"
              component={renderFieldDisabled}
              label="Alamat Sesuai KTP"
            />
            <Field
              name="residenceRt"
              type="text"
              component={renderFieldDisabled}
              label="RT"
            />
            <Field
              name="residenceRw"
              type="text"
              component={renderFieldDisabled}
              label="RW"
            />

            <View style={{marginTop:40}}/>

            <Field
              name="residenceProvinsi"
              type="text"
              component={renderFieldDisabled}
              label="Provinsi"
            />
            <Field
              name="residenceKotaKabupaten"
              type="text"
              component={renderFieldDisabled}
              label="Kota Kabupaten"
            />
            <Field
              name="residenceKecamatan"
              type="text"
              component={renderFieldDisabled}
              label="Kecamatan"
            />
            <Field
              name="residenceKelurahan"
              type="text"
              component={renderFieldDisabled}
              label="Kelurahan"
            />
            <Field
              name="residenceKodePos"
              type="text"
              component={renderFieldDisabled}
              label="Kode Pos"
            />
            <Field
              name="residenceStatusRumah"
              type="text"
              component={renderFieldDisabled}
              label="Status Rumah"
            />

            <View style={{marginTop:40}}/>

            <Field
              name="residenceMasaBerlakuSewa"
              type="text"
              component={renderFieldDisabled}
              label="Masa Berlaku Sewa/Kontrak"
            />
            <Field
              name="residenceLamaTinggalTahun"
              type="text"
              component={renderFieldDisabled}
              label="Lama Tinggal (Tahun)"
            />
            <Field
              name="residenceLamaTinggalBulan"
              type="text"
              component={renderFieldDisabled}
              label="Lama Tinggal (Bulan)"
            />

            <View style={styles.ScrollViewBottom} />
          </Form>
        </Content>
            <HideWithKeyboard style={styles.buttonSideBottomContainer}>
              <TouchableOpacity style={styles.prevButton} onPress={previousPage}>
                <Text style={{color:'white',fontSize:16}}>Kembali</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.nextButton} onPress={handleSubmit}>
                <Text style={{color:'white',fontSize:16}}>Lanjut</Text>
              </TouchableOpacity>
            </HideWithKeyboard>
      </Container>
    )
  }
}

function mapStateToProps(state) {
  return {
    formDdeDetail: state.form.formDdeDetail
  };
} 

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ 
    updateField: (form, field, newValue) => dispatch(change(form, field, newValue))
  }, dispatch);
}

ThirdWizard =  reduxForm({
  form: 'formDdeDetail',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true,
  enableReinitialize: true,
  validate: FormDdeReviseValidate
})(ThirdWizard)

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(ThirdWizard);