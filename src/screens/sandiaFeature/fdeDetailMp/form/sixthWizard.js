import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { Field, reduxForm, change, formValueSelector } from 'redux-form'
import { renderField, renderFieldDropdown, renderFieldPicker, renderFieldDatepicker, renderFieldDisabled,renderFieldHidden } from '../index'
import { Text, View, TouchableOpacity, Image, PermissionsAndroid, Platform } from 'react-native';
import { Container, Content, Form, Picker, ListItem, CheckBox } from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';
import styles from '../style';
import { HideWithKeyboard } from 'react-native-hide-with-keyboard';
import { FormDdeReviseValidate } from "../../../../validates/FormDdeReviseValidate";
import { substringCircum, substringSecondCircum, fdeFormatCurrency, normalizeCurrency } from "../../../../utils/utilization";
// import ImagePicker from 'react-native-image-picker';
import ImgToBase64 from 'react-native-image-base64';
import ImageResizer from 'react-native-image-resizer';
import ImageView from 'react-native-image-view';
import fetch_blob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';
import moment from 'moment';
import UploadKtpAndroid from '../../../camera/android/uploadKtp';
import UploadFotoKonsumenAndroid from '../../../camera/android/uploadFotoKonsumen';
import UploadFapAndroid from '../../../camera/android/uploadFap';
import UploadPpbkAndroid from '../../../camera/android/uploadPpbk';
import UploadSimAndroid from '../../../camera/android/uploadSim';
import UploadSuratKeteranganAndroid from '../../../camera/android/uploadSuratKeterangan';
import UploadKartuKeluargaAndroid from '../../../camera/android/uploadKartuKeluarga';

import UploadKtpIos from '../../../camera/ios/uploadKtp';
import UploadFotoKonsumenIos from '../../../camera/ios/uploadFotoKonsumen';
import UploadFapIos from '../../../camera/ios/uploadFap';
import UploadPpbkIos from '../../../camera/ios/uploadPpbk';
import UploadSimIos from '../../../camera/ios/uploadSim';
import UploadSuratKeteranganIos from '../../../camera/ios/uploadSuratKeterangan';
import UploadKartuKeluargaIos from '../../../camera/ios/uploadKartuKeluarga';
import CustomAlertComponent from '../../../../components/multiPlatform/customAlert/CustomAlertComponent';

var options = {
  title: 'Select Image',
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

class SixthWizard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      uploadConsentLetter : '',
      uploadKtp : '',
      uploadFotoKonsumen : '',
      uploadFap : '',
      uploadPpbk : '',

      uploadSuratKeterangan : '',
      uploadKartuKeluarga : '',
      uploadSim : '',

      isConsentLetterVisible:false,
      isKtpVisible:false,
      isFotoKonsumenVisible:false,
      isFapVisible:false,
      isPpbkVisible:false,

      isSkVisible:false,
      isKkVisible:false,
      isSimVisible:false,

      consentLetterFilenameIOS:'X_ConsentLetter_' + moment().format('X') + '.jpeg',
      consentLetterFilenameAndroid:'/X_ConsentLetter_' + moment().format('X') + '.jpeg',

      fotoKtpFilenameIOS:'X_FotoKtp_' + moment().format('X') + '.jpeg',
      fotoKtpFilenameAndroid:'/X_FotoKtp_' + moment().format('X') + '.jpeg',

      fotoKonsumenFilenameIOS:'X_FotoKonsumen_' + moment().format('X') + '.jpeg',
      fotoKonsumenFilenameAndroid:'/X_FotoKonsumen_' + moment().format('X') + '.jpeg',

      fotoFapFilenameIOS:'X_FotoFap_' + moment().format('X') + '.jpeg',
      fotoFapFilenameAndroid:'/X_FotoFap_' + moment().format('X') + '.jpeg',

      fotoPpbkFilenameIOS:'X_FotoPpbk_' + moment().format('X') + '.jpeg',
      fotoPpbkFilenameAndroid:'/X_FotoPpbk_' + moment().format('X') + '.jpeg',

      typeScreen : '',

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
    }
  }

  componentDidMount = () => {
    const { data} = this.props;
    console.log(data)
    
    this.props.updateField('formDdeDetail', 'kodeToko', data.SupplCode ? substringCircum(data.SupplCode) : '');
    this.props.updateField('formDdeDetail', 'namaToko', data.SupplierBranch ? substringSecondCircum(data.SupplierBranch) : '');
    this.props.updateField('formDdeDetail', 'kodeCabang', data.Cabang ? substringCircum(data.Cabang) : '');
    this.props.updateField('formDdeDetail', 'namaCabang', data.Cabang ? substringSecondCircum(data.Cabang) : '');
    this.props.updateField('formDdeDetail', 'jaminanJenis', data.JaminanCategory ? substringSecondCircum(data.JaminanCategory) : '');
    this.props.updateField('formDdeDetail', 'jenis', data.JaminanJenis ? substringSecondCircum(data.JaminanJenis) : '');
    this.props.updateField('formDdeDetail', 'merk', data.Merk ? substringSecondCircum(data.Merk) : '');
    this.props.updateField('formDdeDetail', 'ukuran', data.Ukuran);
    this.props.updateField('formDdeDetail', 'warna', data.Warna);
    this.props.updateField('formDdeDetail', 'tipe', data.TipeBarang);
    this.props.updateField('formDdeDetail', 'financingPurpose', data.Peruntukan ? substringSecondCircum(data.Peruntukan) : '');
    this.props.updateField('formDdeDetail', 'schemeId', data.SchemeId);
    this.props.updateField('formDdeDetail', 'tipeBiayaAdmin', data.TipeBiayaAdmin);
    this.props.updateField('formDdeDetail', 'tipePembayaran', data.TipePembayaran);
    this.props.updateField('formDdeDetail', 'hargaBarang', data.HargaBarang);
    this.props.updateField('formDdeDetail', 'uangMuka', data.DP);
    this.props.updateField('formDdeDetail', 'jangkaWaktu', data.JangkaWaktu);
    this.props.updateField('formDdeDetail', 'financialSchemeId', data.SchemeId);
    this.props.updateField('formDdeDetail', 'biayaAdmin', data.BiayaAdmin);
    this.props.updateField('formDdeDetail', 'jumlahPembiayaan', data.JumlahPembiayaan);
    this.props.updateField('formDdeDetail', 'bungaEfektif', data.Bunga);
    this.props.updateField('formDdeDetail', 'angsuran', data.Angsuran);
    this.props.updateField('formDdeDetail', 'biayaAsuransi', data.BiayaAsuransi);
    this.props.updateField('formDdeDetail', 'pembayaranPertama', data.TotalPembayaranPertama);
    this.props.updateField('formDdeDetail', 'sourceFunds', data.SumberDana ? substringSecondCircum(data.SumberDana) : '');
    this.props.updateField('formDdeDetail', 'uploadKtp', data.UploadKTP ? 'done' : '');
    this.props.updateField('formDdeDetail', 'uploadFotoKonsumen', data.UploadFotoKonsumen ? 'done' : '');
    this.props.updateField('formDdeDetail', 'uploadFap', data.UploadFAP ? 'done' : '');
    this.props.updateField('formDdeDetail', 'uploadPpbk', data.UploadPPBK ? 'done' : '');
    this.props.updateField('formDdeDetail', 'uploadSk', data.UploadSuratKeterangan ? 'done' : '');
    this.props.updateField('formDdeDetail', 'uploadKk', data.UploadKK ? 'done' : '');
    this.props.updateField('formDdeDetail', 'uploadSim', data.UploadSIM ? 'done' : '');
    this.setState({
      uploadConsentLetter : data.UploadConsentLetter ? data.UploadConsentLetter : '',
      uploadKtp : data.UploadKTP ? data.UploadKTP : '',
      uploadFap : data.UploadFAP ? data.UploadFAP : '',
      uploadFotoKonsumen : data.UploadFotoKonsumen ? data.UploadFotoKonsumen : '',
      uploadPpbk : data.UploadPPBK ? data.UploadPPBK : '',
      uploadSk : data.UploadSuratKeterangan ? data.UploadSuratKeterangan : '',
      uploadKk : data.UploadKK ? data.UploadKK : '',
      uploadSim : data.UploadSIM ? data.UploadSIM : '',

      consentLetterFilenameIOS: data.ORDER_NO + '_ConsentLetter_' + moment().format('X') + '.jpeg',
      consentLetterFilenameAndroid: '/' + data.ORDER_NO + '_ConsentLetter_' + moment().format('X') + '.jpeg',
      fotoKtpFilenameIOS: data.ORDER_NO + '_FotoKtp_' + moment().format('X') + '.jpeg',
      fotoKtpFilenameAndroid: '/' + data.ORDER_NO + '_FotoKtp_' + moment().format('X') + '.jpeg',
      fotoKonsumenFilenameIOS: data.ORDER_NO + '_FotoKonsumen_' + moment().format('X') + '.jpeg',
      fotoKonsumenFilenameAndroid: '/' + data.ORDER_NO + '_FotoKonsumen_' + moment().format('X') + '.jpeg',
      fotoFapFilenameIOS: data.ORDER_NO + '_FotoFap_' + moment().format('X') + '.jpeg',
      fotoFapFilenameAndroid: '/' + data.ORDER_NO + '_FotoFap_' + moment().format('X') + '.jpeg',
      fotoPpbkFilenameIOS: data.ORDER_NO + '_FotoPpbk_' + moment().format('X') + '.jpeg',
      fotoPpbkFilenameAndroid: '/' + data.ORDER_NO + '_FotoPpbk_' + moment().format('X') + '.jpeg',
      typeScreen: this.props.type
    })
    this.props.sendUploadKtp(data.UploadKTP ? data.UploadKTP : '')
    this.props.sendUploadDiri(data.UploadFotoKonsumen ? data.UploadFotoKonsumen : '')
    this.props.sendUploadFap(data.UploadFAP ? data.UploadFAP : '')
    this.props.sendUploadPpbk(data.UploadPPBK ? data.UploadPPBK : '')
    this.props.sendUploadSuratKeterangan(data.UploadSuratKeterangan ? data.UploadSuratKeterangan : '')
    this.props.sendUploadKartuKeluarga(data.UploadKK ? data.UploadKK : '')
    this.props.sendUploadSim(data.UploadSIM ? data.UploadSIM : '')
  }

  requestExternalStoragePermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'My App Storage Permission',
          message: 'My App needs access to your storage ' +
            'so you can save your photos',
        },
      );
      return granted;
    } catch (err) {
      console.error('Failed to request permission ', err);
      return null;
    }
  };

  downloadConsentLetterAndroid = async (url) => {
    await this.requestExternalStoragePermission();
    const fs = fetch_blob.fs;
    const dirs = fetch_blob.fs.dirs;
    const file_path = dirs.DownloadDir + this.state.consentLetterFilenameAndroid;
    var image_url = ''

    if (url.includes('data:image/jpeg;base64,')) { image_url = url.split('data:image/jpeg;base64,') }
    else if (url.includes('data:image/jpg;base64,')) { image_url = url.split('data:image/jpg;base64,') }
    else { image_url = url.split('data:image/png;base64,') }

    var image_data = image_url;
    var res_image_data = image_data[1];

    RNFS.writeFile(file_path, res_image_data, 'base64')
    .then((success) => {
      alert('Sedang mengunduh gambar, mohon ditunggu')
    })
    .catch((error) => {
      alert("Maaf, Terjadi kesalahan dalam pengunduhan");
    });
  }

  downloadConsentLetterIos = (url) => {
    const fs = fetch_blob.fs;
    const dirs = fetch_blob.fs.dirs;
    const file_path = dirs.DownloadDir + this.state.consentLetterFilenameIOS;
    var image_url = ''
    
    if (url.includes('data:image/jpeg;base64,')) { image_url = url.split('data:image/jpeg;base64,') }
    else if (url.includes('data:image/jpg;base64,')) { image_url = url.split('data:image/jpg;base64,') }
    else { image_url = url.split('data:image/png;base64,') }
    
    var image_data = image_url;
    var res_image_data = image_data[1];

    RNFS.writeFile(file_path, res_image_data, 'base64')
    .then((success) => {
      alert('Sedang mengunduh gambar, mohon ditunggu')
    })
    .catch((error) => {
      alert("Maaf, Terjadi kesalahan dalam pengunduhan");
    });
  }

  downloadFotoKtpAndroid = async (url) => {
    await this.requestExternalStoragePermission();
    const fs = fetch_blob.fs;
    const dirs = fetch_blob.fs.dirs;
    const file_path = dirs.DownloadDir + this.state.fotoKtpFilenameAndroid;
    var image_url = ''

    if (url.includes('data:image/jpeg;base64,')) { image_url = url.split('data:image/jpeg;base64,') }
    else if (url.includes('data:image/jpg;base64,')) { image_url = url.split('data:image/jpg;base64,') }
    else { image_url = url.split('data:image/png;base64,') }

    var image_data = image_url;
    var res_image_data = image_data[1];

    RNFS.writeFile(file_path, res_image_data, 'base64')
    .then((success) => {
      alert('Sedang mengunduh gambar, mohon ditunggu')
    })
    .catch((error) => {
      alert("Maaf, Terjadi kesalahan dalam pengunduhan");
    });
  }

  downloadFotoKtpIos = (url) => {
    const fs = fetch_blob.fs;
    const dirs = fetch_blob.fs.dirs;
    const file_path = dirs.DownloadDir + this.state.fotoKtpFilenameIOS;
    var image_url = ''
    
    if (url.includes('data:image/jpeg;base64,')) { image_url = url.split('data:image/jpeg;base64,') }
    else if (url.includes('data:image/jpg;base64,')) { image_url = url.split('data:image/jpg;base64,') }
    else { image_url = url.split('data:image/png;base64,') }
    
    var image_data = image_url;
    var res_image_data = image_data[1];

    RNFS.writeFile(file_path, res_image_data, 'base64')
    .then((success) => {
      alert('Sedang mengunduh gambar, mohon ditunggu')
    })
    .catch((error) => {
      alert("Maaf, Terjadi kesalahan dalam pengunduhan");
    });
  }

  downloadFotoKonsumenAndroid = async (url) => {
    await this.requestExternalStoragePermission();
    const fs = fetch_blob.fs;
    const dirs = fetch_blob.fs.dirs;
    const file_path = dirs.DownloadDir + this.state.fotoKonsumenFilenameAndroid;
    var image_url = ''

    if (url.includes('data:image/jpeg;base64,')) { image_url = url.split('data:image/jpeg;base64,') }
    else if (url.includes('data:image/jpg;base64,')) { image_url = url.split('data:image/jpg;base64,') }
    else { image_url = url.split('data:image/png;base64,') }

    var image_data = image_url;
    var res_image_data = image_data[1];

    RNFS.writeFile(file_path, res_image_data, 'base64')
    .then((success) => {
      alert('Sedang mengunduh gambar, mohon ditunggu')
    })
    .catch((error) => {
      alert("Maaf, Terjadi kesalahan dalam pengunduhan");
    });
  }

  downloadFotoKonsumenIos = (url) => {
    const fs = fetch_blob.fs;
    const dirs = fetch_blob.fs.dirs;
    const file_path = dirs.DownloadDir + this.state.fotoKonsumenFilenameIOS;
    var image_url = ''
    
    if (url.includes('data:image/jpeg;base64,')) { image_url = url.split('data:image/jpeg;base64,') }
    else if (url.includes('data:image/jpg;base64,')) { image_url = url.split('data:image/jpg;base64,') }
    else { image_url = url.split('data:image/png;base64,') }
    
    var image_data = image_url;
    var res_image_data = image_data[1];

    RNFS.writeFile(file_path, res_image_data, 'base64')
    .then((success) => {
      alert('Sedang mengunduh gambar, mohon ditunggu')
    })
    .catch((error) => {
      alert("Maaf, Terjadi kesalahan dalam pengunduhan");
    });
  }

  downloadFotoFapAndroid = async (url) => {
    await this.requestExternalStoragePermission();
    const fs = fetch_blob.fs;
    const dirs = fetch_blob.fs.dirs;
    const file_path = dirs.DownloadDir + this.state.fotoFapFilenameAndroid;
    var image_url = ''

    if (url.includes('data:image/jpeg;base64,')) { image_url = url.split('data:image/jpeg;base64,') }
    else if (url.includes('data:image/jpg;base64,')) { image_url = url.split('data:image/jpg;base64,') }
    else { image_url = url.split('data:image/png;base64,') }

    var image_data = image_url;
    var res_image_data = image_data[1];

    RNFS.writeFile(file_path, res_image_data, 'base64')
    .then((success) => {
      alert('Sedang mengunduh gambar, mohon ditunggu')
    })
    .catch((error) => {
      alert("Maaf, Terjadi kesalahan dalam pengunduhan");
    });
  }

  downloadFotoFapIos = (url) => {
    const fs = fetch_blob.fs;
    const dirs = fetch_blob.fs.dirs;
    const file_path = dirs.DownloadDir + this.state.fotoFapFilenameIOS;
    var image_url = ''
    
    if (url.includes('data:image/jpeg;base64,')) { image_url = url.split('data:image/jpeg;base64,') }
    else if (url.includes('data:image/jpg;base64,')) { image_url = url.split('data:image/jpg;base64,') }
    else { image_url = url.split('data:image/png;base64,') }
    
    var image_data = image_url;
    var res_image_data = image_data[1];

    RNFS.writeFile(file_path, res_image_data, 'base64')
    .then((success) => {
      alert('Sedang mengunduh gambar, mohon ditunggu')
    })
    .catch((error) => {
      alert("Maaf, Terjadi kesalahan dalam pengunduhan");
    });
  }

  downloadFotoPpbkAndroid = async (url) => {
    await this.requestExternalStoragePermission();
    const fs = fetch_blob.fs;
    const dirs = fetch_blob.fs.dirs;
    const file_path = dirs.DownloadDir + this.state.fotoPpbkFilenameAndroid;
    var image_url = ''

    if (url.includes('data:image/jpeg;base64,')) { image_url = url.split('data:image/jpeg;base64,') }
    else if (url.includes('data:image/jpg;base64,')) { image_url = url.split('data:image/jpg;base64,') }
    else { image_url = url.split('data:image/png;base64,') }

    var image_data = image_url;
    var res_image_data = image_data[1];

    RNFS.writeFile(file_path, res_image_data, 'base64')
    .then((success) => {
      alert('Sedang mengunduh gambar, mohon ditunggu')
    })
    .catch((error) => {
      alert("Maaf, Terjadi kesalahan dalam pengunduhan");
    });
  }

  downloadFotoPpbkIos = (url) => {
    const fs = fetch_blob.fs;
    const dirs = fetch_blob.fs.dirs;
    const file_path = dirs.DownloadDir + this.state.fotoPpbkFilenameIOS;
    var image_url = ''
    
    if (url.includes('data:image/jpeg;base64,')) { image_url = url.split('data:image/jpeg;base64,') }
    else if (url.includes('data:image/jpg;base64,')) { image_url = url.split('data:image/jpg;base64,') }
    else { image_url = url.split('data:image/png;base64,') }
    
    var image_data = image_url;
    var res_image_data = image_data[1];

    RNFS.writeFile(file_path, res_image_data, 'base64')
    .then((success) => {
      alert('Sedang mengunduh gambar, mohon ditunggu')
    })
    .catch((error) => {
      alert("Maaf, Terjadi kesalahan dalam pengunduhan");
    });
  }

  imageView = (uri) => [
      {
        source: {
            uri: uri,
        }
      },
  ];
  
  ViewConsentLetter = (image) => {
    return(
       <ImageView
              currentImage = {image}
              images={this.imageView(image)}
              imageIndex={0}
              isVisible={this.state.isConsentLetterVisible}
              animationType="fade"
              onClose={() => this.setState({isConsentLetterVisible: false})}
              isSwipeCloseEnabled={true}
              isTapZoomEnabled={true}
              isPinchZoomEnabled={true}
      />
    )
  }

  ViewKtp = (image) => {
    return(
       <ImageView
              currentImage = {image}
              images={this.imageView(image)}
              imageIndex={0}
              isVisible={this.state.isKtpVisible}
              animationType="fade"
              onClose={() => this.setState({isKtpVisible: false})}
              isSwipeCloseEnabled={true}
              isTapZoomEnabled={true}
              isPinchZoomEnabled={true}
      />
    )
  }

  ViewDiri = (image) => {
    return(
       <ImageView
              currentImage = {image}
              images={this.imageView(image)}
              imageIndex={0}
              isVisible={this.state.isFotoKonsumenVisible}
              animationType="fade"
              onClose={() => this.setState({isFotoKonsumenVisible: false})}
              isSwipeCloseEnabled={true}
              isTapZoomEnabled={true}
              isPinchZoomEnabled={true}
      />
    )
  }

  ViewFap = (image) => {
    return(
       <ImageView
              currentImage = {image}
              images={this.imageView(image)}
              imageIndex={0}
              isVisible={this.state.isFapVisible}
              animationType="fade"
              onClose={() => this.setState({isFapVisible: false})}
              isSwipeCloseEnabled={true}
              isTapZoomEnabled={true}
              isPinchZoomEnabled={true}
      />
    )
  }

  ViewPpbk = (image) => {
    return(
       <ImageView
              currentImage = {image}
              images={this.imageView(image)}
              imageIndex={0}
              isVisible={this.state.isPpbkVisible}
              animationType="fade"
              onClose={() => this.setState({isPpbkVisible: false})}
              isSwipeCloseEnabled={true}
              isTapZoomEnabled={true}
              isPinchZoomEnabled={true}
      />
    )
  }

  ViewSk = (image) => {
    return(
       <ImageView
              currentImage = {image}
              images={this.imageView(image)}
              imageIndex={0}
              isVisible={this.state.isSkVisible}
              animationType="fade"
              onClose={() => this.setState({isSkVisible: false})}
              isSwipeCloseEnabled={true}
              isTapZoomEnabled={true}
              isPinchZoomEnabled={true}
      />
    )
  }

  ViewKk = (image) => {
    return(
       <ImageView
              currentImage = {image}
              images={this.imageView(image)}
              imageIndex={0}
              isVisible={this.state.isKkVisible}
              animationType="fade"
              onClose={() => this.setState({isKkVisible: false})}
              isSwipeCloseEnabled={true}
              isTapZoomEnabled={true}
              isPinchZoomEnabled={true}
      />
    )
  }

  ViewSim = (image) => {
    return(
       <ImageView
              currentImage = {image}
              images={this.imageView(image)}
              imageIndex={0}
              isVisible={this.state.isSimVisible}
              animationType="fade"
              onClose={() => this.setState({isSimVisible: false})}
              isSwipeCloseEnabled={true}
              isTapZoomEnabled={true}
              isPinchZoomEnabled={true}
      />
    )
  }

  imagePickerKtp = () => {
    this.props.enableCameraUploadKtp()
  }
  imagePickerFap = () => {
    this.props.enableCameraUploadFap()
  }
  imagePickerPpbk = () => {
    this.props.enableCameraUploadPpbk()
  }
  imagePickerDiri = () => {
    this.props.enableCameraUploadFotoKonsumen()
  }
  imagePickerSim = () => {
    this.props.enableCameraUploadSim()
  }
  imagePickerKartuKeluarga = () => {
    this.props.enableCameraUploadKartuKeluarga()
  }
  imagePickerSuratKeterangan = () => {
    this.props.enableCameraUploadSuratKeterangan()
  }

  

  goBackKtp = (url) => {
    console.log('from Six : ', url)
        ImageResizer.createResizedImage(
          url,
          2000,
          2000,
          'JPEG',
          50,
        )
          .then(response => {
            
            var source = Platform.OS === 'android' ? url : response.path
            ImgToBase64.getBase64String(source)
              .then(base64String => {
                this.setState({ uploadKtp: 'data:image/jpeg;base64,' + base64String })
                this.props.sendUploadKtp(this.state.uploadKtp)
                this.props.updateField('formFdeMpAdd', 'uploadKtp', "done")
              })
              .catch(err =>
                handleAlert({
                  alertShowed: true,
                  alertMessage: err.message,
                  alertTitle: 'Gagal',
                  alertType: 'error',
                  alertDoneText: 'OK',
                })
              );
          })
          .catch(err => {
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            })
          });
    this.props.disableCameraUploadKtp()
  }
  goBackFotoKonsumen = (url) => {
    console.log('from Six : ', url)
        ImageResizer.createResizedImage(
          url,
          2000,
          2000,
          'JPEG',
          50,
        )
          .then(response => {
            
            var source = Platform.OS === 'android' ? url : response.path
            ImgToBase64.getBase64String(source)
              .then(base64String => {
                this.setState({ uploadDiri: 'data:image/jpeg;base64,' + base64String })
                this.props.sendUploadDiri(this.state.uploadDiri)
                this.props.updateField('formFdeMpAdd', 'uploadDiri', "done")
              })
              .catch(err =>
                handleAlert({
                  alertShowed: true,
                  alertMessage: err.message,
                  alertTitle: 'Gagal',
                  alertType: 'error',
                  alertDoneText: 'OK',
                })
              );
          })
          .catch(err => {
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            })
          });
    this.props.disableCameraUploadFotoKonsumen()
  }
  goBackFap = (url) => {
    console.log('from Six : ', url)
        ImageResizer.createResizedImage(
          url,
          2000,
          2000,
          'JPEG',
          50,
        )
          .then(response => {
            
            var source = Platform.OS === 'android' ? url : response.path
            ImgToBase64.getBase64String(source)
              .then(base64String => {
                this.setState({ uploadFap: 'data:image/jpeg;base64,' + base64String })
                this.props.sendUploadFap(this.state.uploadFap)
                this.props.updateField('formFdeMpAdd', 'uploadFap', "done")
              })
              .catch(err =>
                handleAlert({
                  alertShowed: true,
                  alertMessage: err.message,
                  alertTitle: 'Gagal',
                  alertType: 'error',
                  alertDoneText: 'OK',
                })
              );
          })
          .catch(err => {
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            })
          });
    this.props.disableCameraUploadFap()
  }
  goBackPpbk = (url) => {
    console.log('from Six PPBK : ', url)
        ImageResizer.createResizedImage(
          url,
          2000,
          2000,
          'JPEG',
          50,
        )
          .then(response => {
            
            var source = Platform.OS === 'android' ? url : response.path
            ImgToBase64.getBase64String(source)
              .then(base64String => {
                this.setState({ uploadPpbk: 'data:image/jpeg;base64,' + base64String })
                this.props.sendUploadPpbk(this.state.uploadPpbk)
                this.props.updateField('formFdeMpAdd', 'uploadPpbk', "done")
              })
              .catch(err =>
                handleAlert({
                  alertShowed: true,
                  alertMessage: err.message,
                  alertTitle: 'Gagal',
                  alertType: 'error',
                  alertDoneText: 'OK',
                })
              );
          })
          .catch(err => {
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            })
          });
    this.props.disableCameraUploadPpbk()
  }
  goBackSim = (url) => {
    console.log('from Six SIM : ', url)
        ImageResizer.createResizedImage(
          url,
          2000,
          2000,
          'JPEG',
          50,
        )
          .then(response => {
            
            var source = Platform.OS === 'android' ? url : response.path
            ImgToBase64.getBase64String(source)
              .then(base64String => {
                this.setState({ uploadSim: 'data:image/jpeg;base64,' + base64String })
                this.props.sendUploadSim(this.state.uploadSim)
                this.props.updateField('formFdeMpAdd', 'uploadSim', "done")
              })
              .catch(err =>
                handleAlert({
                  alertShowed: true,
                  alertMessage: err.message,
                  alertTitle: 'Gagal',
                  alertType: 'error',
                  alertDoneText: 'OK',
                })
              );
          })
          .catch(err => {
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            })
          });
    this.props.disableCameraUploadSim()
  }
  goBackSuratKeterangan = (url) => {
    console.log('from Six Surat Keterangan : ', url)
        ImageResizer.createResizedImage(
          url,
          2000,
          2000,
          'JPEG',
          50,
        )
          .then(response => {
            
            var source = Platform.OS === 'android' ? url : response.path
            ImgToBase64.getBase64String(source)
              .then(base64String => {
                this.setState({ uploadSuratKeterangan: 'data:image/jpeg;base64,' + base64String })
                this.props.sendUploadSuratKeterangan(this.state.uploadSuratKeterangan)
                this.props.updateField('formFdeMpAdd', 'uploadSk', "done")
              })
              .catch(err =>
                handleAlert({
                  alertShowed: true,
                  alertMessage: err.message,
                  alertTitle: 'Gagal',
                  alertType: 'error',
                  alertDoneText: 'OK',
                })
              );
          })
          .catch(err => {
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            })
          });
    this.props.disableCameraUploadSuratKeterangan()
  }
  goBackKartuKeluarga = (url) => {
    console.log('from Six KK : ', url)
        ImageResizer.createResizedImage(
          url,
          2000,
          2000,
          'JPEG',
          50,
        )
          .then(response => {
            
            var source = Platform.OS === 'android' ? url : response.path
            ImgToBase64.getBase64String(source)
              .then(base64String => {
                this.setState({ uploadKartuKeluarga: 'data:image/jpeg;base64,' + base64String })
                this.props.sendUploadKartuKeluarga(this.state.uploadKartuKeluarga)
                this.props.updateField('formFdeMpAdd', 'uploadKk', "done")
              })
              .catch(err =>
                handleAlert({
                  alertShowed: true,
                  alertMessage: err.message,
                  alertTitle: 'Gagal',
                  alertType: 'error',
                  alertDoneText: 'OK',
                })
              );
          })
          .catch(err => {
            handleAlert({
              alertShowed: true,
              alertMessage: err.message,
              alertTitle: 'Gagal',
              alertType: 'error',
              alertDoneText: 'OK',
            })
          });
    this.props.disableCameraUploadKartuKeluarga()
  }

  cancelUploadKtp = async () => {
    this.props.disableCameraUploadKtp()
  }
  cancelUploadFap = async () => {
    this.props.disableCameraUploadFap()
  }
  cancelUploadPpbk = async () => {
    this.props.disableCameraUploadPpbk()
  }
  cancelUploadFotoKonsumen = async () => {
    this.props.disableCameraUploadFotoKonsumen()
  }
  cancelUploadSim = async () => {
    this.props.disableCameraUploadSim()
  }
  cancelUploadKartuKeluarga = async () => {
    this.props.disableCameraUploadKartuKeluarga()
  }
  cancelUploadSuratKeterangan = async () => {
    this.props.disableCameraUploadSuratKeterangan()
  }

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handleNegativeButtonAlert = () => {
    this.setState({ alertShowed: false });
  };

  handlePositiveButtonAlert = () => {
    this.setState({ alertShowed: false });
  };

  render(){
    const { 
      handleSubmit, 
      previousPage, 
      ddeRevise_Loading,
      cameraUploadKtpEnable,
      cameraUploadFapEnable,
      cameraUploadFotoKonsumenEnable,
      cameraUploadPpbkEnable,
      cameraUploadSimEnable,
      cameraUploadKartuKeluargaEnable,
      cameraUploadSuratKeteranganEnable 
    } = this.props;

    if(cameraUploadKtpEnable && Platform.OS === 'android'){
      return(
        <UploadKtpAndroid
          goBack = {(url) => this.goBackKtp(url)}
          cancelUpload = {() => this.cancelUploadKtp()}
        />
      ) 
    }if(cameraUploadFotoKonsumenEnable && Platform.OS === 'android'){
      return(
        <UploadFotoKonsumenAndroid
          goBack = {(url) => this.goBackFotoKonsumen(url)}
          cancelUpload = {() => this.cancelUploadFotoKonsumen()}
        />
      ) 
    }if(cameraUploadFapEnable && Platform.OS === 'android'){
      return(
        <UploadFapAndroid
          goBack = {(url) => this.goBackFap(url)}
          cancelUpload = {() => this.cancelUploadFap()}
        />
      ) 
    }if(cameraUploadPpbkEnable && Platform.OS === 'android'){
      return(
        <UploadPpbkAndroid
          goBack = {(url) => this.goBackPpbk(url)}
          cancelUpload = {() => this.cancelUploadPpbk()}
        />
      ) 
    }if(cameraUploadSimEnable && Platform.OS === 'android'){
      return(
        <UploadSimAndroid
          goBack = {(url) => this.goBackSim(url)}
          cancelUpload = {() => this.cancelUploadSim()}
        />
      ) 
    }if(cameraUploadSuratKeteranganEnable && Platform.OS === 'android'){
      return(
        <UploadSuratKeteranganAndroid
          goBack = {(url) => this.goBackSuratKeterangan(url)}
          cancelUpload = {() => this.cancelUploadSuratKeterangan()}
        />
      ) 
    }if(cameraUploadKartuKeluargaEnable && Platform.OS === 'android'){
      return(
        <UploadKartuKeluargaAndroid
          goBack = {(url) => this.goBackKartuKeluarga(url)}
          cancelUpload = {() => this.cancelUploadKartuKeluarga()}
        />
      ) 
    }if(cameraUploadKtpEnable && Platform.OS !== 'android'){
      return(
        <UploadKtpIos
          goBack = {(url) => this.goBackKtp(url)}
          cancelUpload = {() => this.cancelUploadKtp()}
        />
      ) 
    }if(cameraUploadFotoKonsumenEnable && Platform.OS !== 'android'){
      return(
        <UploadFotoKonsumenIos
          goBack = {(url) => this.goBackFotoKonsumen(url)}
          cancelUpload = {() => this.cancelUploadFotoKonsumen()}
        />
      ) 
    }if(cameraUploadFapEnable && Platform.OS !== 'android'){
      return(
        <UploadFapIos
          goBack = {(url) => this.goBackFap(url)}
          cancelUpload = {() => this.cancelUploadFap()}
        />
      ) 
    }if(cameraUploadPpbkEnable && Platform.OS !== 'android'){
      return(
        <UploadPpbkIos
          goBack = {(url) => this.goBackPpbk(url)}
          cancelUpload = {() => this.cancelUploadPpbk()}
        />
      ) 
    }if(cameraUploadSimEnable && Platform.OS !== 'android'){
      return(
        <UploadSimIos
          goBack = {(url) => this.goBackSim(url)}
          cancelUpload = {() => this.cancelUploadSim()}
        />
      ) 
    }if(cameraUploadSuratKeteranganEnable && Platform.OS !== 'android'){
      return(
        <UploadSuratKeteranganIos
          goBack = {(url) => this.goBackSuratKeterangan(url)}
          cancelUpload = {() => this.cancelUploadSuratKeterangan()}
        />
      ) 
    }if(cameraUploadKartuKeluargaEnable && Platform.OS !== 'android'){
      return(
        <UploadKartuKeluargaIos
          goBack = {(url) => this.goBackKartuKeluarga(url)}
          cancelUpload = {() => this.cancelUploadKartuKeluarga()}
        />
      ) 
    }else{
      return (
        <Container>
          <Content disableKBDismissScroll={true} style={{height:'75%',backgroundColor:'white',width:'100%',paddingHorizontal:'4%'}}>
            <Form>
              <Spinner
                visible={
                  ddeRevise_Loading ? true : false
                }
                textContent={'Sedang memproses...'}
                overlayColor="rgba(0, 0, 0, 0.80)"
                textStyle={{color: '#FFF'}}
              />
               {this.showAlert()}
              <View style={{marginTop:20,borderBottomWidth:2,borderBottomColor:'grey',paddingBottom:10}}>
                <Text style={{color:'grey',fontSize:18,fontWeight:'bold'}}>Data Toko</Text>
              </View>
  
              <Field
                name="kodeToko"
                type="text"
                component={renderFieldDisabled}
                label="Kode Toko"
              />
              <Field
                name="namaToko"
                type="text"
                component={renderFieldDisabled}
                label="Nama Toko"
              />
              <Field
                name="kodeCabang"
                type="text"
                component={renderFieldDisabled}
                label="Kode Cabang"
              />
              <Field
                name="namaCabang"
                type="text"
                component={renderFieldDisabled}
                label="Nama Cabang"
              />
  
              <View style={{marginTop:40,borderBottomWidth:2,borderBottomColor:'grey',paddingBottom:10}}>
                <Text style={{color:'grey',fontSize:18,fontWeight:'bold'}}>Data Barang Pembiayaan</Text>
              </View>
  
              <Field
                name="jaminanJenis"
                type="text"
                component={renderFieldDisabled}
                label="Tujuan Pembiayaan untuk Pembelian"
              />
              <Field
                name="jenis"
                type="text"
                component={renderFieldDisabled}
                label="Jenis Barang"
              />
              <Field
                name="merk"
                type="text"
                component={renderFieldDisabled}
                label="Merek Barang"
              />
              <Field
                name="ukuran"
                type="text"
                component={renderFieldDisabled}
                label="Ukuran"
              />
              <Field
                name="warna"
                type="text"
                component={renderFieldDisabled}
                label="Warna"
              />
              <Field
                name="tipe"
                type="text"
                component={renderFieldDisabled}
                label="Tipe"
              />
              <Field
                name="financingPurpose"
                type="text"
                component={renderFieldDisabled}
                label="Peruntukan Unit"
              />
            
              <View style={{marginTop:40}}/>
  
              <Field
                name="schemeId"
                type="text"
                component={renderFieldDisabled}
                label="Nama Program"
              />
              <Field
                name="tipeBiayaAdmin"
                type="text"
                component={renderFieldDisabled}
                label="Tipe Biaya Admin"
              />
              <Field
                name="tipePembayaran"
                type="text"
                component={renderFieldDisabled}
                label="Tipe Pembayaran"
              />
              <Field
                name="hargaBarang"
                type="text"
                component={renderFieldDisabled}
                label="Harga Barang"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />
              <Field
                name="uangMuka"
                type="text"
                component={renderFieldDisabled}
                label="Uang Muka"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />
              <Field
                name="jangkaWaktu"
                type="text"
                component={renderFieldDisabled}
                label="Tenor"
              />
           
              <View style={{marginTop:40}}/>
  
              <Field
                name="financialSchemeId"
                type="text"
                component={renderFieldHidden}
                label="Biaya Admin"
              />
              <Field
                name="biayaAdmin"
                type="text"
                component={renderFieldDisabled}
                label="Biaya Admin"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />
              <Field
                name="jumlahPembiayaan"
                type="text"
                component={renderFieldDisabled}
                label="Jumlah Pembiayaan"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />
              <Field
                name="bungaEfektif"
                type="text"
                component={renderFieldHidden}
                label="Bunga Efektif"
              />
              <Field
                name="angsuran"
                type="text"
                component={renderFieldDisabled}
                label="Angsuran"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />
              <Field
                name="biayaAsuransi"
                type="text"
                component={renderFieldDisabled}
                label="Biaya Asuransi"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />
              <Field
                name="pembayaranPertama"
                type="text"
                component={renderFieldDisabled}
                label="Pembayaran Pertama"
                format={fdeFormatCurrency}
                normalize={normalizeCurrency}
              />
  
              <View style={{marginTop:40}}/>
  
              <Field
                name="sourceFunds"
                type="text"
                component={renderFieldDisabled}
                label="Sumber Dana"
              />
              <View style={{marginVertical:40,borderBottomWidth:2,borderBottomColor:'grey',paddingBottom:10}}>
                <Text style={{color:'grey',fontSize:18, fontWeight:"bold"}}>Data Upload</Text>
              </View>
  
              <Text style={{ marginTop:10, fontSize:15, color:"#666" }}>Consent Letter</Text>
              {this.state.uploadConsentLetter && this.state.uploadConsentLetter !== '' ?
                <View style={{width: "100%", height:85, paddingLeft:0, borderRadius:5}} >
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <TouchableOpacity style={{width: 150, height:80, borderRadius:5}} onPress={() => {this.setState({isConsentLetterVisible : true})}}>
                      <Image source={{uri:this.state.uploadConsentLetter}} style={{width: 150, height:80, borderRadius:5}} />
                    </TouchableOpacity>
                  </View> 
                </View> : null
              }
  
              <Text style={{ marginTop:10, fontSize:15, color:"#666" }}>Foto KTP</Text>
              {this.state.uploadKtp && this.state.uploadKtp !== '' ?
                <View style={{width: "100%", height:85, paddingLeft:0, borderRadius:5}} >
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <TouchableOpacity style={{width: 150, height:80, borderRadius:5}} onPress={() => {this.setState({isKtpVisible : true})}}>
                      <Image source={{uri:this.state.uploadKtp}} style={{width: 150, height:80, borderRadius:5}} />
                    </TouchableOpacity>
                  </View> 
                </View> : null
              }
  
              {this.state.typeScreen === 'Revise' ? 
                <TouchableOpacity 
                  style={styles.verificationValidationButton} 
                  disabled = {false}
                  onPress = {this.imagePickerKtp}
                >
                  <Text style={{color:'white',fontSize:16}}>Upload Foto KTP</Text>
                </TouchableOpacity> : null
              }
  
              <Field  
                name="uploadKtp"
                type="hidden"
                component={renderFieldHidden}
              />
  
              <Text style={{ marginTop:10, fontSize:15, color:"#666" }}>Foto Konsumen</Text>
              {this.state.uploadFotoKonsumen && this.state.uploadFotoKonsumen !== '' ?
                <View style={{width: "100%", height:85, paddingLeft:0, borderRadius:5}} >
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <TouchableOpacity style={{width: 150, height:80, borderRadius:5}} onPress={() => {this.setState({isFotoKonsumenVisible : true})}}>
                      <Image source={{uri:this.state.uploadFotoKonsumen}} style={{width: 150, height:80, borderRadius:5}} />
                    </TouchableOpacity>
                  </View> 
                </View> : null
              }
  
              {this.state.typeScreen === 'Revise' ? 
                <TouchableOpacity 
                  style={styles.verificationValidationButton} 
                  disabled = {false}
                  onPress = {this.imagePickerFotoKonsumen}
                >
                  <Text style={{color:'white',fontSize:16}}>Upload Foto Konsumen</Text>
                </TouchableOpacity> : null
              }
  
              <Field  
                name="uploadFotoKonsumen"
                type="hidden"
                component={renderFieldHidden}
              />
  
              <Text style={{ marginTop:10, fontSize:15, color:"#666" }}>Foto FAP</Text>
              {this.state.uploadFap && this.state.uploadFap !== '' ?
                <View style={{width: "100%", height:85, paddingLeft:0, borderRadius:5}} >
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <TouchableOpacity style={{width: 150, height:80, borderRadius:5}} onPress={() => {this.setState({isFapVisible : true})}}>
                      <Image source={{uri:this.state.uploadFap}} style={{width: 150, height:80, borderRadius:5}} />
                    </TouchableOpacity>
                  </View> 
                </View> : null
              }
  
              {this.state.typeScreen === 'Revise' ? 
                <TouchableOpacity 
                  style={styles.verificationValidationButton} 
                  disabled = {false}
                  onPress = {this.imagePickerFap}
                >
                  <Text style={{color:'white',fontSize:16}}>Upload Foto FAP</Text>
                </TouchableOpacity> : null
              }
  
              <Field  
                name="uploadFap"
                type="hidden"
                component={renderFieldHidden}
              />
  
              <Text style={{ marginTop:10, fontSize:15, color:"#666" }}>Foto PPBK</Text>
              {this.state.uploadPpbk && this.state.uploadPpbk !== '' ?
                <View style={{width: "100%", height:85, paddingLeft:0, borderRadius:5}} >
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <TouchableOpacity style={{width: 150, height:80, borderRadius:5}} onPress={() => {this.setState({isPpbkVisible : true})}}>
                      <Image source={{uri:this.state.uploadPpbk}} style={{width: 150, height:80, borderRadius:5}} />
                    </TouchableOpacity>
                  </View> 
                </View> : null
              }
  
              {this.state.typeScreen === 'Revise' ? 
                <TouchableOpacity 
                  style={styles.verificationValidationButton} 
                  disabled = {false}
                  onPress = {this.imagePickerPpbk}
                >
                  <Text style={{color:'white',fontSize:16}}>Upload Foto PPBK</Text>
                </TouchableOpacity> : null
              }
  
              <Field  
                name="uploadPpbk"
                type="hidden"
                component={renderFieldHidden}
              />
  
              <Text style={{ marginTop:10, fontSize:15, color:"#666" }}>Foto Surat Keterangan</Text>
              {this.state.uploadSuratKeterangan && this.state.uploadSuratKeterangan !== '' ?
                <View style={{width: "100%", height:85, paddingLeft:0, borderRadius:5}} >
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <TouchableOpacity style={{width: 150, height:80, borderRadius:5}} onPress={() => {this.setState({isSkVisible : true})}}>
                      <Image source={{uri:this.state.uploadSuratKeterangan}} style={{width: 150, height:80, borderRadius:5}} />
                    </TouchableOpacity>
                  </View> 
                </View> : null
              }
  
              {this.state.typeScreen === 'Revise' ? 
                <TouchableOpacity 
                  style={styles.verificationValidationButton} 
                  disabled = {false}
                  onPress = {this.imagePickerSuratKeterangan}
                >
                  <Text style={{color:'white',fontSize:16}}>Upload Foto Surat Keterangan</Text>
                </TouchableOpacity> : null
              }
  
              <Field  
                name="uploadSk"
                type="hidden"
                component={renderFieldHidden}
              />
  
              <Text style={{ marginTop:10, fontSize:15, color:"#666" }}>Foto KK</Text>
              {this.state.uploadKartuKeluarga && this.state.uploadKartuKeluarga !== '' ?
                <View style={{width: "100%", height:85, paddingLeft:0, borderRadius:5}} >
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <TouchableOpacity style={{width: 150, height:80, borderRadius:5}} onPress={() => {this.setState({isKkVisible : true})}}>
                      <Image source={{uri:this.state.uploadKartuKeluarga}} style={{width: 150, height:80, borderRadius:5}} />
                    </TouchableOpacity>
                  </View> 
                </View> : null
              }
  
              {this.state.typeScreen === 'Revise' ? 
                <TouchableOpacity 
                  style={styles.verificationValidationButton} 
                  disabled = {false}
                  onPress = {this.imagePickerKartuKeluarga}
                >
                  <Text style={{color:'white',fontSize:16}}>Upload Foto KK</Text>
                </TouchableOpacity> : null
              }
  
              <Field  
                name="uploadKk"
                type="hidden"
                component={renderFieldHidden}
              />
  
              <Text style={{ marginTop:10, fontSize:15, color:"#666" }}>Foto SIM</Text>
              {this.state.uploadSim && this.state.uploadSim !== '' ?
                <View style={{width: "100%", height:85, paddingLeft:0, borderRadius:5}} >
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <TouchableOpacity style={{width: 150, height:80, borderRadius:5}} onPress={() => {this.setState({isSimVisible : true})}}>
                      <Image source={{uri:this.state.uploadSim}} style={{width: 150, height:80, borderRadius:5}} />
                    </TouchableOpacity>
                  </View> 
                </View> : null
              }
  
              {this.state.typeScreen === 'Revise' ? 
                <TouchableOpacity 
                  style={styles.verificationValidationButton} 
                  disabled = {false}
                  onPress = {this.imagePickerSim}
                >
                  <Text style={{color:'white',fontSize:16}}>Upload Foto SIM</Text>
                </TouchableOpacity> : null
              }
              
              <Field  
                name="uploadSim"
                type="hidden"
                component={renderFieldHidden}
              />
  
              {this.ViewConsentLetter(this.state.uploadConsentLetter)}
              {this.ViewKtp(this.state.uploadKtp)}
              {this.ViewDiri(this.state.uploadDiri)}
              {this.ViewFap(this.state.uploadFap)}
              {this.ViewPpbk(this.state.uploadPpbk)}
  
              {this.ViewSk(this.state.uploadSk)}
              {this.ViewKk(this.state.uploadKk)}
              {this.ViewSim(this.state.uploadSim)}
  
              <View style={{marginTop:150}}/>
            </Form>
          </Content>
          
            <HideWithKeyboard style={styles.buttonSideBottomContainer}>
              <TouchableOpacity style={styles.prevButton} onPress={previousPage}>
                <Text style={{color:'white',fontSize:16}}>Kembali</Text>
              </TouchableOpacity>
              {this.state.typeScreen === 'Revise' ? 
                <TouchableOpacity style={styles.nextButton} onPress={handleSubmit}>
                  <Text style={{color:'white',fontSize:16}}>Revisi</Text>
                </TouchableOpacity> : null
              }
            </HideWithKeyboard>
         
        </Container>
      )
    }
  }
} 

function mapStateToProps(state) {
  return {
    formDdeDetail: state.form.formDdeDetail
  };
} 

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ 
    updateField: (form, field, newValue) => dispatch(change(form, field, newValue))
  }, dispatch);
}

SixthWizard =  reduxForm({
  form: 'formDdeDetail',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true,
  enableReinitialize: true,
  validate: FormDdeReviseValidate
})(SixthWizard)

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(SixthWizard);