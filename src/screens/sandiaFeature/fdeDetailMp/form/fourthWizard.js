import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { Field, reduxForm, change } from 'redux-form'
import { renderField, renderFieldDropdown, renderFieldPicker, renderFieldDatepicker, renderFieldDisabled } from '../index'
import { Text, View, TouchableOpacity } from 'react-native';
import { Container, Spinner, Content, Form, Picker } from 'native-base';
import styles from '../style';
import { HideWithKeyboard } from 'react-native-hide-with-keyboard';
import { FormDdeReviseValidate } from "../../../../validates/FormDdeReviseValidate"
import { substringCircum, substringSecondCircum, deleteThirdCircum, normalizeCurrency,fdeFormatCurrency } from "../../../../utils/utilization";

class FourthWizard extends Component {
    constructor(props) {
      super(props)
      this.state = {
        userAccess: '',
        userToken: '',
      }
    }

   componentDidMount = () => {
    const {data} = this.props;
    this.props.updateField('formDdeDetail', 'jobCategory', data.StatusKepegawaian ? substringSecondCircum(data.StatusKepegawaian) : '');
    this.props.updateField('formDdeDetail', 'jobStatus', data.JobStatus ? substringSecondCircum(data.JobStatus) : '');
    this.props.updateField('formDdeDetail', 'tipePekerjaan', data.TipePekerjaan ? substringSecondCircum(data.TipePekerjaan) : '');
    this.props.updateField('formDdeDetail', 'jobPosition', data.Jabatan ? substringSecondCircum(data.Jabatan) : '');
    this.props.updateField('formDdeDetail', 'industryType', data.BidangUsaha ? substringSecondCircum(data.BidangUsaha) : '');
    this.props.updateField('formDdeDetail', 'namaPerusahaan', data.NamaPerusahaan);
    this.props.updateField('formDdeDetail', 'lamaUsahaBulan', data.LamaUsahaBln ? substringSecondCircum(data.LamaUsahaBln) : '');
    this.props.updateField('formDdeDetail', 'lamaUsahaTahun', data.LamaUsahaThn ? substringSecondCircum(data.LamaUsahaThn) : '');
    this.props.updateField('formDdeDetail', 'kontrakBulan', data.PeriodeKontrakBln ? substringSecondCircum(data.PeriodeKontrakBln) : '');
    this.props.updateField('formDdeDetail', 'kontrakTahun', data.PeriodeKontrakThn ? substringSecondCircum(data.PeriodeKontrakThn) : '');
    this.props.updateField('formDdeDetail', 'jobAlamat', data.JobAlamat);
    this.props.updateField('formDdeDetail', 'jobRt', data.JobRT);
    this.props.updateField('formDdeDetail', 'jobRw', data.JobRW);
    this.props.updateField('formDdeDetail', 'jobProvinsi', data.JobPropinsi);
    this.props.updateField('formDdeDetail', 'jobKotaKabupaten', data.JobKotamadya);
    this.props.updateField('formDdeDetail', 'jobKecamatan', data.JobKecamatan);
    this.props.updateField('formDdeDetail', 'jobKelurahan', data.JobKelurahan);
    this.props.updateField('formDdeDetail', 'jobKodePos', data.JobKodePos);
    this.props.updateField('formDdeDetail', 'jobKodeArea', data.JobTeleponArea);
    this.props.updateField('formDdeDetail', 'jobTelp', data.JobTelepon);
    this.props.updateField('formDdeDetail', 'jobTelpEkstensi', data.JobTelponExt);
    this.props.updateField('formDdeDetail', 'statusUsaha', substringSecondCircum(data.StatusTempatUsaha));     
    this.props.updateField('formDdeDetail', 'penghasilanPerbulan', data.Penghasilan);
   }

    render(){
        const { handleSubmit, previousPage } = this.props
        
        return (
        <Container>
            <Content disableKBDismissScroll={true} style={{height:'75%',backgroundColor:'white',width:'100%',alignSelf:'center',paddingHorizontal:'4%'}}>
                <Form>
                    <Field
                      name="jobCategory"
                      type="text"
                      component={renderFieldDisabled}
                      label="Kategori Pekerjaan"
                    />
                    <Field
                      name="jobStatus"
                      type="text"
                      component={renderFieldDisabled}
                      label="Status Pekerjaan"
                    />
                    <Field
                      name="tipePekerjaan"
                      type="text"
                      component={renderFieldDisabled}
                      label="Tipe Pekerjaan"
                    />
                    <Field
                      name="jobPosition"
                      type="text"
                      component={renderFieldDisabled}
                      label="Jabatan"
                    />
                    <Field
                      name="industryType"
                      type="text"
                      component={renderFieldDisabled}
                      label="Bidang Usaha"
                    />
                  
                    <View style={{marginTop:40}}/>

                    <Field
                        name="namaPerusahaan"
                        type="text"
                        component={renderFieldDisabled}
                        label="Nama Perusahaan"
                    />
                    <Field
                      name="lamaUsahaBulan"
                      type="text"
                      component={renderFieldDisabled}
                      label="Bekerja Sejak (Bulan)"
                    />
                    <Field
                      name="lamaUsahaTahun"
                      type="text"
                      component={renderFieldDisabled}
                      label="Bekerja Sejak (Tahun)"
                    />
                    <Field
                      name="kontrakBulan"
                      type="text"
                      component={renderFieldDisabled}
                      label="Periode Masa Kontrak (Bulan)"
                    />
                    <Field
                      name="kontrakTahun"
                      type="text"
                      component={renderFieldDisabled}
                      label="Periode Masa Kontrak (Tahun)"
                    />
              
                    <View style={{marginTop:40}}/>

                    <Field
                        name="jobAlamat"
                        type="text"
                        component={renderFieldDisabled}
                        label="Alamat Perusahaan"
                    />
                    <Field
                        name="jobRt"
                        type="text"
                        component={renderFieldDisabled}
                        label="RT Perusahaan"
                    />
                    <Field
                        name="jobRw"
                        type="text"
                        component={renderFieldDisabled}
                        label="RW Perusahaan"
                    />

                    <View style={{marginTop:40}}/>

                    <Field
                      name="jobProvinsi"
                      type="text"
                      component={renderFieldDisabled}
                      label="Provinsi"
                    />
                    <Field
                      name="jobKotaKabupaten"
                      type="text"
                      component={renderFieldDisabled}
                      label="Kota Kabupaten"
                    />
                    <Field
                      name="jobKecamatan"
                      type="text"
                      component={renderFieldDisabled}
                      label="Kecamatan"
                    />
                    <Field
                      name="jobKelurahan"
                      type="text"
                      component={renderFieldDisabled}
                      label="Kelurahan"
                    />
                    <Field
                        name="jobKodePos"
                        type="text"
                        component={renderFieldDisabled}
                        label="Kode Pos"
                    />

                    <View style={{marginTop:40}}/>

                    <Field
                        name="jobKodeArea"
                        type="text"
                        component={renderFieldDisabled}
                        label="Kode Area Telp Perusahaan"
                    />
                    <Field
                        name="jobTelp"
                        type="text"
                        component={renderFieldDisabled}
                        label="No Telp Perusahaan"
                    />
                    <Field
                        name="jobTelpEkstensi"
                        type="text"
                        component={renderFieldDisabled}
                        label="Ekstensi Telp Perusahaan (bila ada)"
                    />

                    <View style={{marginTop:40}}/>

                    <Field
                      name="statusUsaha"
                      type="text"
                      component={renderFieldDisabled}
                      label="Status Usaha"
                    />
                    <Field
                        name="penghasilanPerbulan"
                        type="text"
                        component={renderFieldDisabled}
                        label="Penghasilan Perbulan"
                        format={fdeFormatCurrency}
                        normalize={normalizeCurrency}
                    />

                    <View style={styles.ScrollViewBottom}/>  
                </Form>
            </Content>
            <HideWithKeyboard style={styles.buttonSideBottomContainer}>
                <TouchableOpacity style={styles.prevButton} onPress={previousPage}>
                <Text style={{color:'white',fontSize:16}}>Kembali</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.nextButton} onPress={handleSubmit}>
                <Text style={{color:'white',fontSize:16}}>Lanjut</Text>
                </TouchableOpacity>
            </HideWithKeyboard>
        </Container>
    )}
}

function mapStateToProps(state) {
    return {
      formDdeDetail: state.form.formDdeDetail
    };
  } 

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ 
        updateField: (form, field, newValue) => dispatch(change(form, field, newValue))
    }, dispatch);
}

FourthWizard =  reduxForm({
    form: 'formDdeDetail',
    destroyOnUnmount: false, // <------ preserve form data
    forceUnregisterOnUnmount: true,
    enableReinitialize: true,
    validate: FormDdeReviseValidate
})(FourthWizard)

export default connect(
    mapStateToProps,
    matchDispatchToProps
)(FourthWizard);