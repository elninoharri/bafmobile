import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { Field, reduxForm, change } from 'redux-form'
import { renderFieldDisabled } from '../index'
import { Text, View, TouchableOpacity } from 'react-native';
import { Container, Spinner, Content, Form, Picker } from 'native-base';
import styles from '../style';
import { HideWithKeyboard } from 'react-native-hide-with-keyboard';
import { FormDdeReviseValidate } from "../../../../validates/FormDdeReviseValidate";
import { substringCircum, substringSecondCircum } from "../../../../utils/utilization";

class SecondWizard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userAccess: '',
      userToken: '',
    }
  }

   componentDidMount = () => {
    const {data} = this.props;
    this.props.updateField('formDdeDetail', 'ktpAlamat', data.KTPAlamat);
    this.props.updateField('formDdeDetail', 'ktpRt', data.KTPRT);
    this.props.updateField('formDdeDetail', 'ktpRw', data.KTPRW);
    this.props.updateField('formDdeDetail', 'ktpProvinsi', data.KTPPropinsi);
    this.props.updateField('formDdeDetail', 'ktpKotaKabupaten', data.KTPKotamadya);
    this.props.updateField('formDdeDetail', 'ktpKecamatan', data.KTPKecamatan);
    this.props.updateField('formDdeDetail', 'ktpKelurahan', data.KTPKelurahan);
    this.props.updateField('formDdeDetail', 'ktpKodePos', data.KTPKodePos);
    this.props.updateField('formDdeDetail', 'ktpStatusRumah', data.KTPKepemilikanRumah ? substringSecondCircum(data.KTPKepemilikanRumah) : '');
    this.props.updateField('formDdeDetail', 'ktpMasaBerlakuSewa', data.KTPKontrakExpDate);
    this.props.updateField('formDdeDetail', 'ktpLamaTinggalTahun', data.KTPLamaMenempatiThn);
    this.props.updateField('formDdeDetail', 'ktpLamaTinggalBulan', data.KTPLamaMenempatiBln);
   }

  render(){
    const { handleSubmit, previousPage, ddeHouseOwnership_Result, ddeLegalProvince_Result } = this.props
    const { pickerKtpStatusRumahDefault, pickerKtpStatusRumahItem, pickerKtpStatusRumahSelected, 
      pickerKtpProvinsiDefault, pickerKtpProvinsiItem, pickerKtpProvinsiSelected, disableKotaKabupaten,
      disableKecamatan, disableKelurahan } = this.state

    return (
      <Container>
        <Content disableKBDismissScroll={true} style={{height:'75%',backgroundColor:'white',width:'100%',alignSelf:'center',paddingHorizontal:'5%'}}>
          <Form>
            <Field
                name="ktpAlamat"
                type="text"
                component={renderFieldDisabled}
                label="Alamat Sesuai KTP"
              />
              <Field
                name="ktpRt"
                type="text"
                component={renderFieldDisabled}
                label="RT"
              />
              <Field
                name="ktpRw"
                type="text"
                component={renderFieldDisabled}
                label="RW"
              />

              <View style={{marginTop:40}}/>

              <Field
                name="ktpProvinsi"
                type="text"
                component={renderFieldDisabled}
                label="Provinsi"
              />

              <Field
                name="ktpKotaKabupaten"
                type="text"
                component={renderFieldDisabled}
                label="Kota Kabupaten"
              />

              <Field
                name="ktpKecamatan"
                type="text"
                component={renderFieldDisabled}
                label="Kecamatan"
              />

              <Field
                name="ktpKelurahan"
                type="text"
                component={renderFieldDisabled}
                label="Kelurahan"
              />

              <Field
                name="ktpKodePos"
                type="text"
                component={renderFieldDisabled}
                label="Kode Pos"
              />

              <View style={{marginTop:40}}/>

              <Field
                name="ktpStatusRumah"
                type="text"
                component={renderFieldDisabled}
                label="Status Rumah"
              />

              <Field
                name="ktpMasaBerlakuSewa"
                type="text"
                component={renderFieldDisabled}
                label="Masa Berlaku Sewa/Kontrak"
              />
            
              <Field
                name="ktpLamaTinggalTahun"
                type="text"
                component={renderFieldDisabled}
                label="Lama Tinggal (Tahun)"
              />
              <Field
                name="ktpLamaTinggalBulan"
                type="text"
                component={renderFieldDisabled}
                label="Lama Tinggal (Bulan)"
              />
              <View style={styles.ScrollViewBottom} />
          </Form>
        </Content>
            <HideWithKeyboard style={styles.buttonSideBottomContainer}>
              <TouchableOpacity style={styles.prevButton} onPress={previousPage}>
                <Text style={{color:'white',fontSize:16}}>Kembali</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.nextButton} onPress={handleSubmit}>
                <Text style={{color:'white',fontSize:16}}>Lanjut</Text>
              </TouchableOpacity>
            </HideWithKeyboard>
      </Container>
    )
  }
} 

function mapStateToProps(state) {
  return {
    formDdeDetail: state.form.formDdeDetail
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    updateField: (form, field, newValue) => dispatch(change(form, field, newValue))
  }, dispatch);
}

SecondWizard =  reduxForm({
  form: 'formDdeDetail',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true,
  enableReinitialize: true,
  validate: FormDdeReviseValidate
})(SecondWizard)

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(SecondWizard);