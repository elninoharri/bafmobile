import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { Field, reduxForm, change } from 'redux-form'
import {renderField, renderFieldHidden, renderFieldDisabled, renderFieldDatepicker, renderFieldPicker } from '../index'
import { Text, View, TouchableOpacity,Alert, BackHandler } from 'react-native';
import { Container, Spinner, Content, Form, Picker } from 'native-base';
import styles from '../style';
import { ddeDetail } from "../../../../actions/sandiaFeature/dataLengkap";
import { HideWithKeyboard } from 'react-native-hide-with-keyboard';
import AsyncStorage from '@react-native-community/async-storage';
import { FormDdeReviseValidate } from "../../../../validates/FormDdeReviseValidate";
import { substringCircum,substringSecondCircum } from '../../../../utils/utilization';

class FirstWizard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userAccess: '',
      userToken: '',
    }
  }

  componentDidMount = () => {
    const {data} = this.props;
    this.props.updateField('formDdeDetail', 'orderTrxhId', data.ORDER_TRX_H_ID);
    this.props.updateField('formDdeDetail', 'nomorAplikasi', data.APP_NO);
    this.props.updateField('formDdeDetail', 'dataSumberAplikasi', data.SourceAplikasi ? substringCircum(data.SourceAplikasi) : '');
    this.props.updateField('formDdeDetail', 'nomorOrder', data.ORDER_NO);
    this.props.updateField('formDdeDetail', 'namaKonsumen', data.Fullname);
    this.props.updateField('formDdeDetail', 'namaPanggilan', data.NamaPanggilan);
    this.props.updateField('formDdeDetail', 'NIK', data.IdNo);
    this.props.updateField('formDdeDetail', 'masaBerlakuKtp', data.JatuhTempoKTP);
    this.props.updateField('formDdeDetail', 'tempatLahir', data.TempatLahir);
    this.props.updateField('formDdeDetail', 'tanggalLahir', data.TanggalLahir);
    this.props.updateField('formDdeDetail', 'jenisKelamin', data.JenisKelamin ? substringSecondCircum(data.JenisKelamin) : '');
    this.props.updateField('formDdeDetail', 'pendidikanTerakhir', data.PendidikanTerakhir ? substringSecondCircum(data.PendidikanTerakhir) : '');
    this.props.updateField('formDdeDetail', 'pendidikanTerakhirLainnya', data.PendidikanTerakhirLainnya);
    this.props.updateField('formDdeDetail', 'kodeArea', data.KTPKodePos);
    this.props.updateField('formDdeDetail', 'telepon', data.TeleponRumah);
    this.props.updateField('formDdeDetail', 'noHandphone1', data.Handphone1);
    this.props.updateField('formDdeDetail', 'noHandphone2', data.Handphone2);
    this.props.updateField('formDdeDetail', 'noHandphone3', data.Handphone3);
    this.props.updateField('formDdeDetail', 'statusPernikahan', data.StatusPernikahan ? substringSecondCircum(data.StatusPernikahan) : '');
    this.props.updateField('formDdeDetail', 'namaPasangan', data.NamaPasangan);
    this.props.updateField('formDdeDetail', 'noHandphonePasangan', data.HandPhonePasangan);
    this.props.updateField('formDdeDetail', 'agama', data.Agama ? substringSecondCircum(data.Agama) : '');
    this.props.updateField('formDdeDetail', 'namaIbuKandung', data.NamaIbuKandung);
    this.props.updateField('formDdeDetail', 'email', data.Email1);
  }

  render() {
    
    const { handleSubmit, submitting, ddeGender_Result, ddeEducation_Result, ddeMaritalStatus_Result, ddeReligion_Result } = this.props
    const { pickerGenderItem, pickerGenderDefault, pickerGenderSelected, pickerEducationItem, pickerEducationDefault, pickerEducationSelected,
      pickerMaritalStatusItem, pickerMaritalStatusDefault, pickerMaritalStatusSelected, pickerReligionItem, pickerReligionDefault, pickerReligionSelected, } = this.state

    return (
      <Container>
        <Content disableKBDismissScroll={true} style={{height:'75%',width:'100%',alignSelf:'center',paddingHorizontal:'5%'}}>
          <Form>
            
            <Field
              name="orderTrxhId"
              type="text"
              component={renderFieldHidden}
              label="Order Id"
            />
            <Field
              name="nomorAplikasi"
              type="text"
              component={renderFieldDisabled}
              label="Nomor Aplikasi"
            />
            <Field
              name="dataSumberAplikasi"
              type="text"
              component={renderFieldDisabled}
              label="Sumber Aplikasi"
            />
            <Field
              name="nomorOrder"
              type="text"
              component={renderFieldDisabled}
              label="Nomor Order"
            />

            <View style={{marginTop:40}}/>

            <Field
              name="namaKonsumen"
              type="text"
              component={renderFieldDisabled}
              label="Nama Konsumen"
            />
            <Field
              name="namaPanggilan"
              type="text"
              component={renderFieldDisabled}
              label="Nama Panggilan"
            />
            <Field
              name="NIK"
              type="text"
              component={renderFieldDisabled}
              label="NIK KTP"
            />
            <Field
              name="masaBerlakuKtp"
              type="text"
              component={renderFieldDisabled}
              label="Masa berlaku KTP"
            />
            <Field
              name="tempatLahir"
              type="text"
              component={renderFieldDisabled}
              label="Tempat Lahir"
            />
            <Field
              name="tanggalLahir"
              type="text"
              component={renderFieldDisabled}
              label="Tanggal Lahir"
            />
            <Field
              name="jenisKelamin"
              type="text"
              component={renderFieldDisabled}
              label="Jenis Kelamin"
            />
            <Field
              name="pendidikanTerakhir"
              type="text"
              component={renderFieldDisabled}
              label="Pendidikan Terakhir"
            />
            <Field
              name="pendidikanTerakhirLainya"
              type="text"
              component={renderFieldHidden}
              label="Pendidikan terakhir Lainya"
            />

            <View style={{marginTop:40}}/>

            <Field
              name="kodeArea"
              type="text"
              component={renderFieldHidden}
              label="Kode Area"
            />
            <Field
              name="telepon"
              type="text"
              component={renderFieldHidden}
              label="Telepon"
            />
            <Field
              name="noHandphone1"
              type="text"
              component={renderFieldDisabled}
              label="No Handphone 1"
            />
            <Field
              name="noHandphone2"
              type="text"
              component={renderFieldDisabled}
              label="No Handphone 2 (bila ada)"
            />
            <Field
              name="noHandphone3"
              type="text"
              component={renderFieldDisabled}
              label="No Handphone 3 (bila ada)"
            />

            <View style={{marginTop:40}}/>

            <Field
              name="statusPernikahan"
              type="text"
              component={renderFieldDisabled}
              label="Status Pernikahan"
            />
            <Field
              name="namaPasangan"
              type="text"
              component={renderFieldDisabled}
              label="Nama Pasangan"
            />
            <Field
              name="noHandphonePasangan"
              type="text"
              component={renderFieldDisabled}
              label="No Handphone Pasangan"
            />
            <Field
              name="agama"
              type="text"
              component={renderFieldDisabled}
              label="Agama"
            />
            <Field
              name="namaIbuKandung"
              type="text"
              component={renderFieldDisabled}
              label="Nama Ibu Kandung"
            />
            <Field
              name="email"
              type="text"
              component={renderFieldDisabled}
              label="Email"
            />
            <View style={styles.ScrollViewBottom} />
          </Form>
        </Content>

        <HideWithKeyboard style={styles.buttonBottomContainer}>
          <TouchableOpacity style={styles.submitButton} onPress={handleSubmit} disabled={submitting}>
            <Text style={{color:'white',fontSize:16}}>Lanjut</Text>
          </TouchableOpacity>
        </HideWithKeyboard>
      
      </Container>
    )}
}

function mapStateToProps(state) {
  return {
    ddeDetail_Loading: state.ddeDetail.loading,
    ddeDetail_Result: state.ddeDetail.result,
    ddeDetail_Error: state.ddeDetail.error,
    formDdeDetail: state.form.formDdeDetail
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ 
    ddeDetail,
    updateField: (form, field, newValue) => dispatch(change(form, field, newValue))
  }, dispatch);
}

FirstWizard =  reduxForm({
  form: 'formDdeDetail',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true,
  enableReinitialize: true,
  validate: FormDdeReviseValidate
})(FirstWizard)

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(FirstWizard);

