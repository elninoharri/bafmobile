import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { Field, reduxForm, change } from 'redux-form'
import { renderField, renderFieldPicker,renderFieldDisabled } from '../index'
import { Text, View, TouchableOpacity } from 'react-native';
import { Container, Spinner, Content, Form, Picker } from 'native-base';
import styles from '../style';
import { HideWithKeyboard } from 'react-native-hide-with-keyboard';
import { FormDdeReviseValidate } from "../../../../validates/FormDdeReviseValidate"
import {substringSecondCircum} from '../../../../utils/utilization';

class FifthWizard extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

   componentDidMount = () => {
    const {data} = this.props;
       this.props.updateField('formDdeDetail', 'namaEmergencyKontak1', data.NamaEC1);
       this.props.updateField('formDdeDetail', 'noHpEmergencyKontak1', data.HandPhoneEC1);
       this.props.updateField('formDdeDetail', 'hubunganEmergencyKontak1', data.HubunganEC1 ? substringSecondCircum(data.HubunganEC1) : '');
       this.props.updateField('formDdeDetail', 'alamatEmergencyKontak1', data.AlamatEC1);
       this.props.updateField('formDdeDetail', 'namaEmergencyKontak2', data.NamaEC2);
       this.props.updateField('formDdeDetail', 'noHpEmergencyKontak2', data.HandPhoneEC2);
       this.props.updateField('formDdeDetail', 'hubunganEmergencyKontak2', data.HubunganEC2 ? substringSecondCircum(data.HubunganEC2) : '');
       this.props.updateField('formDdeDetail', 'alamatEmergencyKontak2', data.AlamatEC2);
   }

  render(){
    const { handleSubmit, previousPage, ddeRelationship_Result } = this.props
    const { pickerRelationship1Item, pickerRelationshipDefault1, pickerRelationshipSelected1, pickerRelationship2Item, pickerRelationshipDefault2, pickerRelationshipSelected2 } = this.state
    return (
      <Container>
        <Content disableKBDismissScroll={true} style={{height:'75%',backgroundColor:'white',width:'100%',alignSelf:'center',paddingHorizontal:'4%'}}>
          <Form>

          <View style={{marginTop:20,borderBottomWidth:2,borderBottomColor:'grey',paddingBottom:10}}>
            <Text style={{color:'grey',fontSize:18,fontWeight:'bold'}}>Emergency Kontak 1</Text>
          </View>
          
            <Field
              name="namaEmergencyKontak1"
              type="text"
              component={renderFieldDisabled}
              label="Nama Emergency Kontak 1"
            />
            <Field
              name="noHpEmergencyKontak1"
              type="text"
              component={renderFieldDisabled}
              label="No HP Emergency Kontak 1"
            />
            <Field
              name="hubunganEmergencyKontak1"
              type="text"
              component={renderFieldDisabled}
              label="Hubungan Emergency Kontak 1"
            />
            <Field
              name="alamatEmergencyKontak1"
              type="text"
              component={renderFieldDisabled}
              label="Alamat Emergency Kontak 1"
            />  

          <View style={{marginTop:40,borderBottomWidth:2,borderBottomColor:'grey',paddingBottom:10}}>
            <Text style={{color:'grey',fontSize:18,fontWeight:'bold'}}>Emergency Kontak 2</Text>
          </View>
          
           <Field
              name="namaEmergencyKontak2"
              type="text"
              component={renderFieldDisabled}
              label="Nama Emergency Kontak 2"
            />
            <Field
              name="noHpEmergencyKontak2"
              type="text"
              component={renderFieldDisabled}
              label="No HP Emergency Kontak 2"
            />
            <Field
              name="hubunganEmergencyKontak2"
              type="text"
              component={renderFieldDisabled}
              label="Hubungan Emergency Kontak 2"
            />
            <Field
              name="alamatEmergencyKontak2"
              type="text"
              component={renderFieldDisabled}
              label="Alamat Emergency Kontak 2"
            /> 
            
            <View style={styles.ScrollViewBottom}/>
          </Form>
        </Content>
            <HideWithKeyboard style={styles.buttonSideBottomContainer}>
              <TouchableOpacity style={styles.prevButton} onPress={previousPage}>
                <Text style={{color:'white',fontSize:16}}>Kembali</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.nextButton} onPress={handleSubmit}>
                <Text style={{color:'white',fontSize:16}}>Lanjut</Text>
              </TouchableOpacity>
            </HideWithKeyboard>
      </Container>
    )
  }
}

function mapStateToProps(state) {
  return {
    formDdeDetail: state.form.formDdeDetail
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ 
    updateField: (form, field, newValue) => dispatch(change(form, field, newValue))
  }, dispatch);
}

FifthWizard =  reduxForm({
  form: 'formDdeDetail',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true,
  enableReinitialize: true,
  validate: FormDdeReviseValidate
})(FifthWizard)

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(FifthWizard);