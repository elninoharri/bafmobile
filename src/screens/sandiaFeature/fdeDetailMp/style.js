import { StyleSheet } from 'react-native';
import { FSAlbertBold,FSAlbert} from '../../../utils/fonts';

export default StyleSheet.create({
    ScrollViewBottom : {
        width:'100%',
        height:150
    },
    SDContainerStyle : { 
        width:'100%',
    },
    SDInputTextStyle : {
        height: 42,
        padding: 12,
        borderWidth: 1,
        borderColor: '#aaa',
        borderRadius: 5
    },
    SDItemStyle : {
        padding: 12,
        height:42,
        marginTop: 2,
        borderColor: '#bbb',
        borderWidth: 1,
        borderRadius: 5
    },
    SDItemTextStyle : { 
       
    },
    SDItemsContainerStyle : { 
        maxHeight:  135
    },
    ErrorDesc : { 
        color:'red',
        alignSelf:'stretch',
        textAlign:'right',
        fontSize:10,
        fontFamily:FSAlbert
    },
    ErrorDescDropdown : { 
        color:'red',
        alignSelf:'stretch',
        textAlign:'right',
        right:13,
        fontSize:10,
        fontFamily:FSAlbert
    }, 
    formItem : {
        left:-5,
        width:'100%'
    },
    formInputDisabled : {
        color : '#bbb'
    },
    phoneValidationButton : {
        backgroundColor:'#347AB7',
        width:'100%',
        height:40,
        alignItems:'center',
        justifyContent:'center',
        marginTop:15,
        borderRadius:5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 3
    },
    verificationValidationButton : {
        backgroundColor:'#5CB95D',
        width:'100%',
        height:40,
        alignItems:'center',
        justifyContent:'center',
        marginTop:15,
        borderRadius:5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 3
    },
    submitButton : {
        backgroundColor:'#347AB7',
        width:'90%',
        height:40,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 3
    },
    nextButton : {
        backgroundColor:'#347AB7',
        width:'47%',
        height:40,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 3
    },
    prevButton : {
        backgroundColor:'#EEAF30',
        width:'47%',
        height:40,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:5,
        marginRight:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 3
    },
    stepIndicatorContainer: {
        height:110, 
        backgroundColor:'white', 
        borderBottomWidth: 2,
        borderBottomColor: '#ddd'
    },
    buttonBottomContainer : {
        position:"absolute", 
        width:"100%", 
        alignItems:'center', 
        bottom:0, 
        justifyContent:"center", 
        backgroundColor:'#fff',
        borderTopWidth:2, 
        borderColor:'#ddd', 
        height:100
    },
    buttonSideBottomContainer : {
        position:"absolute", 
        flexDirection:'row',
        justifyContent:'space-between',
        width:"100%",
        bottom:0, 
        backgroundColor:'#fff',
        borderTopWidth:2, 
        borderColor:'#ddd', 
        height:100,
        paddingVertical:27,
        paddingHorizontal:"5%"
    },
    PickerContainer : {
        marginTop:2,
        backgroundColor: 'white',
        borderColor: '#aaa',
        borderWidth: 1,
        borderRadius: 5
    },
    DropdownDisabled : {
        backgroundColor:'white',
        borderWidth:1,
        borderColor:'#bbb',
        width:'100%',
        height:42,
        paddingLeft:15,
        justifyContent:'center',
        marginTop:15,
        borderRadius:5
    },
    ButtonDisabled : {
        backgroundColor:'rgba(52, 52, 52, 0.3)',
        width:'100%',
        height:40,
        alignItems:'center',
        justifyContent:'center',
        marginTop:15,
        borderRadius:5,
    },
})