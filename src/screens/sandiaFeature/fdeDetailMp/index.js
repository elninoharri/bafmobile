import React, { Component } from 'react';
import { Text, View, Platform, BackHandler } from 'react-native';
import { Container, Spinner, Label, Input, Item, Content } from 'native-base';
import SubHeaderAndroidBlue from '../../../components/android/subHeaderBlue';
import SubHeaderIosBlue from '../../../components/ios/subHeaderBlue';
import styles from './style';
import FirstWizard from './form/firstWizard';
import SecondWizard from './form/secondWizard';
import ThirdWizard from './form/thirdWizard';
import FourthWizard from './form/fourthWizard';
import FifthWizard from './form/fifthWizard';
import SixthWizard from './form/sixthWizard';
import StepIndicator from 'react-native-step-indicator';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import AsyncStorage from '@react-native-community/async-storage';
import { change, reset } from 'redux-form'
import { ddeRevise, ddeFilter, ddeDetail } from "../../../actions/sandiaFeature/dataLengkap";
import { LOB } from '../../../utils/constant'
import { mergeValuesData } from '../../../utils/utilization'
import moment from 'moment';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';

export const renderFieldDisabled = ({
  input,
  type,
  label,
  format,
  normalize,
  meta: {
    touched,
    error,
  }
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{ width: '100%', marginTop: 10 }}>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
      <Label style={{ fontSize: 15, color: "#666", marginBottom: 0 }}>{label}</Label>
      <Item regular style={{ borderRadius: 5, height: 40, borderColor: "#ccc" }} >
        <Input style={styles.formInputDisabled} {...input} type={type} editable={false} format={format} normalize={normalize}></Input>
      </Item>
    </View>
  )
}

export const renderFieldHidden = ({
  input,
  type,
  meta: {
    touched,
    error,
  }
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{ width: '100%' }}>
      <Item style={{ display: "none" }}>
        <Input {...input} ype={type} />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  )
}

const labels = ["Data\nPemohon", "Informasi\nAlamat", "Informasi\nDomisili", "Informasi\nPekerjaan", "Emergency\nKontak", "Data\nBarang"];

const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#5CB95D',
  stepStrokeWidth: 2,
  stepStrokeFinishedColor: '#5CB95D',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: '#5CB95D',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#5CB95D',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#5CB95D',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 10,
  currentStepLabelColor: '#5CB95D'
}


class FdeDetailMp extends Component {
  constructor(props) {
    super(props)
    this.submitForm = this.submitForm.bind(this);
    this.handleAlert = this.handleAlert.bind(this);
    this.state = {
      screenTitle: this.props.route.params.Type === "Revise" ? "Revisi Data Lengkap" : "Detail Data Lengkap",
      disableAllField: true,
      page: 0,
      currentPosition: 0,
      userAccess: '',
      userToken: '',
      Isapproved: '1',
      Lobcode: LOB,
      Dtmcrt: moment().utc().subtract(6, 'days').format('YYYY-MM-DD'),
      Dtmcrtto: moment().utc().format('YYYY-MM-DD'),
      data: {},
      type: this.props.route.params.Type,
      uploadKtp: false,
      uploadDiri: false,
      uploadFap: false,
      uploadPpbk: false,
      uploadSk: false,
      uploadKk: false,
      uploadSim: false,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      cameraUploadKtpEnable: false,
      cameraUploadFotoKonsumenEnable: false,
      cameraUploadFapEnable: false,
      cameraUploadPpbkEnable: false,
      cameraUploadSimEnable: false,
      cameraUploadKartuKeluargaEnable: false,
      cameraUploadSuratKeteranganEnable: false
    }
  }

  componentDidMount = async () => {
    const { ddeDetail } = this.props;

    const jsonUserAccess = await AsyncStorage.getItem('userAccess');
    var userAccess = JSON.parse(jsonUserAccess)
    this.setState({ userAccess: userAccess })

    const userToken = await AsyncStorage.getItem('userToken');
    this.setState({ userToken: userToken })

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

    // ddeDetail({Ordertrxhid : this.props.route.params.Ordertrxhid},userToken)
    ddeDetail({ Ordertrxhid: this.props.route.params.params }, userToken)
    this.props.route.params.params
  }

  defaultTemplate = () => {
    return (
      <Content style={{ flexDirection: 'row', alignSelf: 'center', alignContent: 'center' }}>
        <Spinner color='#002f5f' />
        <Text style={{ color: 'grey' }}>Sedang memuat data</Text>
      </Content>

    )
  }

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.props.navigation.goBack();
      this.props.resetForm('formDdeDetail')
      return true;
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
    const { ddeDetail_Error, ddeDetail_Result, ddeRevise_Result, ddeRevise_Error, ddeRevise_Loading } = this.props

    if (ddeDetail_Error !== null && prevProps.ddeDetail_Error !== ddeDetail_Error) {
      this.setState({
        alertFor: 'FailedLoadData',
        alertShowed: true,
        alertMessage: "Koneksi Bermasalah ", ddeDetail_Error,
        alertTitle: 'Informasi',
        alertType: 'info',
        alertDoneText: 'OK',
      })
      console.log(ddeDetail_Error)

    }

    if (ddeDetail_Result !== null && prevProps.ddeDetail_Result !== ddeDetail_Result) {
      this.setState({
        data: ddeDetail_Result,
        page: 1
      })
    }



    if (ddeRevise_Result !== null && prevProps.ddeRevise_Result !== ddeRevise_Result) {
      this.props.resetForm('formDdeDetail')

      if (ddeRevise_Result.status === '7') {
        this.setState({
          alertFor: 'ddeRevise_Result',
          alertShowed: true,
          alertMessage: 'Data Berhasil di Revisi',
          alertTitle: 'Informasi',
          alertType: 'success',
          alertDoneText: 'OK',
        })
      } else {
        this.setState({
          alertFor: 'ddeRevise_Error',
          alertShowed: true,
          alertMessage: ddeRevise_Result.message || 'Terjadi kesalahan koneksi silahkan, kirim ulang data anda.',
          alertTitle: 'Informasi',
          alertType: 'info',
          alertDoneText: 'OK',
        })
      }
    }

    if (ddeRevise_Error !== null && prevProps.ddeRevise_Error !== ddeRevise_Error) {
      this.setState({
        alertFor: 'ddeRevise_Error',
        alertShowed: true,
        alertMessage: 'Gagal Revisi data. Error : ' + ddeRevise_Error.message,
        alertTitle: 'Informasi',
        alertType: 'info',
        alertDoneText: 'OK',
      })
    }
  }

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handleAlert(value) {
    this.setState({
      alertShowed: value.alertShowed,
      alertMessage: value.alertMessage,
      alertTitle: value.alertTitle,
      alertType: value.alertType,
      alertDoneText: value.alertDoneText,
    })
  }

  handlePositiveButtonAlert = () => {
    this.setState({ alertShowed: false });
    if (this.state.alertFor === 'NoLoginAccess' || this.state.alertFor === 'NoAccessToForm' || this.state.alertFor === 'qdeDetailData_Error') {
      this.goBack();
      this.props.resetForm('formFdeMpAdd');
    } else if (this.state.alertFor === 'EmptyNik' || this.state.alertFor === 'OtpExpired' || this.state.alertFor === 'FailedLoadData') {
      this.props.navigation.navigate('Home');
      this.props.resetForm('formFdeMpAdd');
    } else if (this.state.alertFor === 'ddeInsert_Result' || this.state.alertFor === 'ddeInsert_Error' || this.state.alertFor === 'ddeRevise_Result' || this.state.alertFor === 'ddeRevise_Error') {
      this.props.navigation.navigate('OrderList');
      this.props.resetForm('formFdeMpAdd');
    }
    this.setState({ alertFor: null });
  };

  handleNegativeButtonAlert = () => {
    this.setState({ alertShowed: false });
  };

  nextPage = () => {
    this.setState({ page: this.state.page + 1 })
    this.setState({ currentPosition: this.state.currentPosition + 1 })
  }

  previousPage = () => {
    this.setState({ page: this.state.page - 1 })
    this.setState({ currentPosition: this.state.currentPosition - 1 })
  }

  goBack = () => {
    this.props.navigation.goBack();
    this.props.resetForm('formDdeDetail')
  }

  getUploadKtp = (data) => {
    this.setState({ uploadKtp: data })
  }

  getUploadDiri = (data) => {
    this.setState({ uploadDiri: data })
  }

  getUploadFap = (data) => {
    this.setState({ uploadFap: data })
  }

  getUploadPpbk = (data) => {
    this.setState({ uploadPpbk: data })
  }

  getUploadSk = (data) => {
    this.setState({ uploadSk: data })
  }

  getUploadKk = (data) => {
    this.setState({ uploadKk: data })
  }

  getUploadSim = (data) => {
    this.setState({ uploadSim: data })
  }

  submitForm(data) {
    const { ddeRevise } = this.props

    var dataMerge = {
      UploadKTP: this.state.uploadKtp,
      UploadFotoKonsumen: this.state.uploadDiri,
      UploadFAP: this.state.uploadFap,
      UploadPPBK: this.state.uploadPpbk,
      UploadSuratKeterangan: this.state.uploadSk,
      UploadKK: this.state.uploadKk,
      UploadSIM: this.state.uploadSim,
      Orderno: data.nomorOrder,
      Appno: data.nomorAplikasi
    }

    var arrayData = []
    for (var key in dataMerge) {
      arrayData.push(key + "~" + dataMerge[key])
    }

    var dataMergeString = JSON.stringify(arrayData)
    var ValuesData = mergeValuesData(dataMergeString)

    ddeRevise({
      Ordertrxhid: data.orderTrxhId,
      Valuesdata: ValuesData,
      Usrcrt: this.state.data.Usrcrt
    });

  }

  enableCameraUploadKtp = () => {
    this.setState({
      cameraUploadKtpEnable: true
    })
  }

  enableCameraUploadFotoKonsumen = () => {
    this.setState({
      cameraUploadFotoKonsumenEnable: true
    })
  }

  enableCameraUploadFap = () => {
    this.setState({
      cameraUploadFapEnable: true
    })
  }

  enableCameraUploadPpbk = () => {
    this.setState({
      cameraUploadPpbkEnable: true
    })
  }

  enableCameraUploadSim = () => {
    this.setState({
      cameraUploadSimEnable: true
    })
  }

  enableCameraUploadKartuKeluarga = () => {
    this.setState({
      cameraUploadKartuKeluargaEnable: true
    })
  }

  enableCameraUploadSuratKeterangan = () => {
    this.setState({
      cameraUploadSuratKeteranganEnable: true
    })
  }

  disableCameraUploadKtp = () => {
    this.setState({
      cameraUploadKtpEnable: false
    })
  }

  disableCameraUploadFotoKonsumen = () => {
    this.setState({
      cameraUploadFotoKonsumenEnable: false
    })
  }

  disableCameraUploadFap = () => {
    this.setState({
      cameraUploadFapEnable: false
    })
  }

  disableCameraUploadPpbk = () => {
    this.setState({
      cameraUploadPpbkEnable: false
    })
  }

  disableCameraUploadSim = () => {
    this.setState({
      cameraUploadSimEnable: false
    })
  }

  disableCameraUploadKartuKeluarga = () => {
    this.setState({
      cameraUploadKartuKeluargaEnable: false
    })
  }

  disableCameraUploadSuratKeterangan = () => {
    this.setState({
      cameraUploadSuratKeteranganEnable: false
    })
  }

  render() {
    const {
      page,
      cameraUploadKtpEnable,
      cameraUploadFotoKonsumenEnable,
      cameraUploadFapEnable,
      cameraUploadPpbkEnable,
      cameraUploadSimEnable,
      cameraUploadSuratKeteranganEnable,
      cameraUploadKartuKeluargaEnable
    } = this.state
    const { ddeRevise_Loading } = this.props;

    return (
      <Container style={{ flex: 1 }}>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroidBlue
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
            <SubHeaderIosBlue
              title={this.state.screenTitle}
              goBack={this.goBack}
            />
          )}


        {page !== 7 && !cameraUploadFotoKonsumenEnable && !cameraUploadKtpEnable && !cameraUploadFapEnable && !cameraUploadPpbkEnable && !cameraUploadSimEnable && !cameraUploadSuratKeteranganEnable && !cameraUploadKartuKeluargaEnable ?
          <View style={styles.stepIndicatorContainer}>
            {this.showAlert()}
            <View
              style={{
                width: '100%',
                backgroundColor: '#F2F2F2',
                height: 110,
                paddingTop: 20,
              }}>
              <StepIndicator
                customStyles={customStyles}
                currentPosition={this.state.currentPosition}
                labels={labels}
                stepCount={6}
              />
            </View>
          </View> : null
        }

        {page === 0 && this.defaultTemplate()}
        {page === 1 &&
          <FirstWizard
            onSubmit={this.nextPage}
            data={this.state.data}
          />}
        {page === 2 && (
          <SecondWizard
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
            data={this.state.data}
          />
        )}
        {page === 3 && (
          <ThirdWizard
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
            data={this.state.data}
          />
        )}
        {page === 4 && (
          <FourthWizard
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
            data={this.state.data}
          />
        )}
        {page === 5 && (
          <FifthWizard
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
            data={this.state.data}
          />
        )}
        {page === 6 && (
          <SixthWizard
            previousPage={this.previousPage}
            onSubmit={this.submitForm}
            type={this.state.type}
            data={this.state.data}
            sendUploadKtp={this.getUploadKtp}
            sendUploadDiri={this.getUploadDiri}
            sendUploadFap={this.getUploadFap}
            sendUploadPpbk={this.getUploadPpbk}
            sendUploadSuratKeterangan={this.getUploadSk}
            sendUploadKartuKeluarga={this.getUploadKk}
            sendUploadSim={this.getUploadSim}
            handleAlert={this.handleAlert}
            ddeRevise_Loading={ddeRevise_Loading}
            enableCameraUploadKtp={this.enableCameraUploadKtp}
            disableCameraUploadKtp={this.disableCameraUploadKtp}
            enableCameraUploadFotoKonsumen={this.enableCameraUploadFotoKonsumen}
            disableCameraUploadFotoKonsumen={this.disableCameraUploadFotoKonsumen}
            enableCameraUploadFap={this.enableCameraUploadFap}
            disableCameraUploadFap={this.disableCameraUploadFap}
            enableCameraUploadPpbk={this.enableCameraUploadPpbk}
            disableCameraUploadPpbk={this.disableCameraUploadPpbk}
            enableCameraUploadSim={this.enableCameraUploadSim}
            disableCameraUploadSim={this.disableCameraUploadSim}
            enableCameraUploadKartuKeluarga={this.enableCameraUploadKartuKeluarga}
            disableCameraUploadKartuKeluarga={this.disableCameraUploadKartuKeluarga}
            enableCameraUploadSuratKeterangan={this.enableCameraUploadSuratKeterangan}
            disableCameraUploadSuratKeterangan={this.disableCameraUploadSuratKeterangan}
            cameraUploadKtpEnable={cameraUploadKtpEnable}
            cameraUploadFotoKonsumenEnable={cameraUploadFotoKonsumenEnable}
            cameraUploadFapEnable={cameraUploadFapEnable}
            cameraUploadPpbkEnable={cameraUploadPpbkEnable}
            cameraUploadSimEnable={cameraUploadSimEnable}
            cameraUploadSuratKeteranganEnable={cameraUploadSuratKeteranganEnable}
            cameraUploadKartuKeluargaEnable={cameraUploadKartuKeluargaEnable}
          />
        )}

      </Container>
    );
  }

}


function mapStateToProps(state) {
  return {
    ddeDetail_Loading: state.ddeDetail.loading,
    ddeDetail_Result: state.ddeDetail.result,
    ddeDetail_Error: state.ddeDetail.error,
    ddeRevise_Loading: state.ddeRevise.loading,
    ddeRevise_Result: state.ddeRevise.result,
    ddeRevise_Error: state.ddeRevise.error,
    formDdeDetail: state.form.formDdeDetail,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    ddeDetail,
    ddeRevise,
    ddeFilter,
    updateField: (form, field, newValue) => dispatch(change(form, field, newValue)),
    resetForm: (form) => dispatch(reset(form))
  }, dispatch);
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(FdeDetailMp);
