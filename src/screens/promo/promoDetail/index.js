import React, {Component} from 'react';
import {
  Text,
  Image,
  View,
  TouchableOpacity,
  ScrollView,
  BackHandler,
  Dimensions,
  PixelRatio,
  Alert,
  ToastAndroid,
  Platform,
} from 'react-native';
import {
  Container,
  Left,
  Icon,
  CardItem,
  Card,
  Content,
  Spinner as SpinnerNative,
  Row,
  Right,
  Body,
} from 'native-base';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import {styles} from './style';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';
import {getPengajuanData} from '../../../actions/lob';
import {refreshToken} from '../../../actions';
import {detailUser} from '../../../actions/account/changeProfile';
import {refContentD, getVoucherCode} from '../../../actions/home';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import Toast, {DURATION} from 'react-native-easy-toast';
import SimpleToast from 'react-native-simple-toast';
import ListPromo from '../../../components/multiPlatform/tabPromo/listPromo';
import analytics from '@react-native-firebase/analytics';
import {
  handleRefreshToken,
  jsonParse,
  regexHTML,
} from '../../../utils/utilization';
import ModalVoucher from 'react-native-modal';
import {
  BAF_COLOR_BLUE,
  MESSAGE_TOKEN_EXP,
  MESSAGE_TOKEN_INVALID,
  TOKEN_EXP,
} from '../../../utils/constant';
import {fonts} from '../../../utils/fonts';
import Clipboard from '@react-native-community/clipboard';

const mockingCodeVoucher =
  'B4F51TRI234HGH3G4Y2G43H2HVB346263432GRH3GTRT6577GF567UG64333T43767GTT7T6566GHFY5467578YFU667689U9UKYIT767UUYYI666UYT6576';

const mockingCodeVoucher2 = 'B4F51TRI';

class PromoDetail extends Component {
  constructor(props) {
    super();
    this.submitData = this.submitData.bind(this);
    this.state = {
      detailPromoParam: false,

      title: false,
      dateEnd: false,
      detailUser: false,
      contenthID: false,
      contentData: [],
      bannerCode: false,
      promoCode: '',
      isConnected: false,

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      arr: [],
      loading: true,
      modalVoucherVisible: false,
      pressed: false,
      spinnerLoading: false,
      containPromo: false,
      userToken: '',
      dataMerge: '',
    };
  }

  componentDidMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this.checkInternet();

    var title = this.props.route.params.title;
    var bannerCode = this.props.route.params.promoCode;

    var dateEnd = moment(this.props.route.params.dateEnd).format('DD MMM YYYY');

    this.setState({
      title: title,
      dateEnd: dateEnd,
      bannerCode: bannerCode,
    });

    const detailUserNonParse = await AsyncStorage.getItem('detailUser');
    if (detailUserNonParse) {
      const detailUserParse = JSON.parse(detailUserNonParse);
      this.setState({detailUser: detailUserParse});
    }

    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) {
      this.setState({userToken: userToken});
    }

    this.initApi();
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      detailUserResult,
      refContentDResult,
      refContentDError,
      getVoucherCodeResult,
      getVoucherCodeLoading,
      getVoucherCodeError,
      refContentDLoading,
      refreshToken_result,
      getPengajuanDataResult,
      getPengajuanDataError,
      refreshTokenResult,
      refreshTokenError,
    } = this.props;
    const {dataMerge} = this.state;

    if (detailUserResult && prevProps.detailUserResult !== detailUserResult) {
      this.setState({detailUser: detailUserResult});
      const userToken = await AsyncStorage.getItem('userToken');
      if (userToken) {
        this.setState({userToken: userToken});
      }
    }

    if (
      refreshToken_result !== null &&
      prevProps.refreshToken_result !== refreshToken_result
    ) {
      await handleRefreshToken(refreshToken_result.Refreshtoken);
      this.setState({userToken: refreshToken_result.Refreshtoken});

      this.props.getVoucherCode(
        this.state.dataMerge,
        refreshToken_result.Refreshtoken,
      );
    }

    if (
      refContentDResult &&
      prevProps.refContentDResult !== refContentDResult
    ) {
      if (refContentDResult.ContainsPromo) {
        //di sini nembak api get voucher
        this.setState({containPromo: true});
      }
      if (refContentDResult.AllData && refContentDResult.AllData.length > 0) {
        this.setState({contentData: refContentDResult.AllData});

        console.log(refContentDResult);
        var arr = await this.insertionSort(refContentDResult.AllData); // di sini di mana data bakal di sorted
        this.setState({
          arr: arr,
          containPromo: refContentDResult.ContainsPromo,
        });
      } else {
        // this.setState({imagesApi: this.state.images});
      }
    }

    if (
      getVoucherCodeLoading &&
      prevProps.getVoucherCodeLoading !== getVoucherCodeLoading
    ) {
      console.log('get voucher code berhasil ditembak');
    }

    if (
      getVoucherCodeResult &&
      prevProps.getVoucherCodeResult !== getVoucherCodeResult
    ) {
      if (getVoucherCodeResult.VoucherCode !== null) {
        //misal voucher sudah habis button tidak dimunculkan
        this.setState({
          spinnerLoading: false,
          promoCode: getVoucherCodeResult.VoucherCode,
          modalVoucherVisible: true,
          pressed: false,
        });
      } else {
        SimpleToast.show('Kuota Voucher Sudah Habis.', Toast.SHORT);
        //Suatu action yang menyatakan bahwa kuota voucher sudah habis
      }
    }

    if (
      getVoucherCodeError &&
      prevProps.getVoucherCodeError !== getVoucherCodeError
    ) {
      const data = await jsonParse(getVoucherCodeError.message);
      if (data) {
        this.validateRefreshToken(data);
      } else {
        this.setState({
          alertShowed: true,
          alertMessage: getVoucherCodeError.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
          spinnerLoading: false,
          pressed: false,
        });
      }
    }

    if (refContentDError && prevProps.refContentDError !== refContentDError) {
      console.log(refContentDError);
      this.setState({loading: false});
    }

    if (
      getPengajuanDataError &&
      prevProps.getPengajuanDataError !== getPengajuanDataError
    ) {
      const data = await jsonParse(getPengajuanDataError.message);
      if (data) {
        if (data.message == MESSAGE_TOKEN_EXP) {
          this.props.refreshToken(this.state.userToken);
        }
      } else {
        this.setState({
          alertShowed: true,
          alertMessage: getPengajuanDataError.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
    }

    if (
      getPengajuanDataResult &&
      prevProps.getPengajuanDataResult !== getPengajuanDataResult
    ) {
      this.setState({
        alertFor: 'getPengajuanDataResult',
        alertShowed: true,
        alertMessage: 'Anda Berhasil mengajukan voucher melalui BAF Mobile',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
        spinnerLoading: false,
        pressed: false,
      });
    }

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      console.log('Error Refresh Token ', refreshTokenError.message);
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      await handleRefreshToken(refreshTokenResult.Refreshtoken);
      this.setState({userToken: refreshTokenResult.Refreshtoken});
      this.props.getPengajuanData(
        {
          DateReq: dataMerge.DateReq,
          DP: dataMerge.DP,
          JenisBrg: dataMerge.JenisBrg,
          Pinjaman: dataMerge.Pinjaman,
          PromoCode: dataMerge.PromoCode,
          Source: dataMerge.Source,
          Tenor: dataMerge.Tenor,
          TipePengajuan: dataMerge.TipePengajuan,
        },
        refreshTokenResult.Refreshtoken,
      );
    }
  };

  //FUNCTION

  initApi = async () => {
    const {refContentD} = this.props;

    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        var contenthid = this.props.route.params.contenthid;

        refContentD({
          Contenthid: contenthid,
          BannerCode: this.state.bannerCode,
        });
      } else {
        this.showToastNoInternet();
      }
    });
  };

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  handleInvalid = async () => {
    await AsyncStorage.removeItem('userToken');
    await AsyncStorage.removeItem('userData');
    await AsyncStorage.removeItem('detailUser');
    await AsyncStorage.removeItem('activeAgreement');
    this.setState({
      alertFor: 'invalidToken',
      alertShowed: true,
      alertMessage: 'Sesi login Anda sudah habis',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  validateRefreshToken = async (data) => {
    console.log('enter validate refresh token');
    console.log(data.message);

    if (data) {
      if (data.message == MESSAGE_TOKEN_EXP) {
        this.props.refreshToken(this.state.userToken);
      } else if (data.message == MESSAGE_TOKEN_INVALID) {
        this.setState({
          pressed: false,
        });
        this.handleInvalid();
      }
    }
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  submitData = async () => {
    const {detailUser, LOB, promoCode} = this.state;
    const {navigation, getPengajuanData} = this.props;

    await this.setState({modalVoucherVisible: false, pressed: true});
    //Proses ini dilakukan untuk menunggu modal dari tunjukan voucher turun karena bentrok sama modal Spinner
    setTimeout(() => {
      NetInfo.fetch().then(async (state) => {
        if (state.isConnected) {
          if (!detailUser) {
            navigation.navigate('Login');
          } else {
            this.setState({spinnerLoading: true});
            analytics().logEvent('eventPromoSubmit', {
              item: 'user trying to submit ajukan promo',
            });

            var dataMerge = {
              Source: 'BAF Mobile',
              DP: '',
              JenisBrg: '',
              Pinjaman: '',
              Tenor: '',
              TipePengajuan: 'LOB',
              DateReq: moment().format('YYYY-MM-DD hh:mm:ss'),
              PromoCode: promoCode,
            };

            console.log(dataMerge);

            getPengajuanData(dataMerge, this.state.userToken);
            this.setState({dataMerge: dataMerge});
          }
        } else {
          this.showToastNoInternet();
        }
      });
    }, 500);
  };

  handlePositiveButtonAlert = () => {
    if (this.state.alertFor === 'showModal') {
      this.setState({alertShowed: false, alertFor: false});
      this.showModal();
    } else if (this.state.alertFor === 'getPengajuanDataResult') {
      this.setState({alertShowed: false, alertFor: false});
      this.props.navigation.navigate('Home');
    } else if (this.state.alertFor === 'invalidToken') {
      this.setState({alertShowed: false, alertFor: false});
      this.props.navigation.navigate('Login');
    } else {
      this.setState({alertShowed: false, alertFor: false});
    }
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false, alertFor: false});
  };

  showModal = () => {
    if (this.state.detailUser) {
      const dataMerge = {BannerCode: this.state.bannerCode};
      this.setState({dataMerge: dataMerge});
      console.log(
        'ini yang ditembak: ' +
          dataMerge.BannerCode +
          ' ' +
          this.state.userToken,
      );
      this.setState({pressed: true});
      this.props.getVoucherCode(dataMerge, this.state.userToken);
    } else {
      //Ini gak kita kasih handle invalid karena user belum login,
      //jadi tulisan prompt sesi login anda habis yang dipake untuk invalid token gak make sense
      this.props.navigation.navigate('Login');
    }
  };

  setModalVoucher = () => {
    this.setState({
      modalVoucherVisible: false,
    });
  };

  insertionSort = (arr) => {
    const len = arr.length;
    for (let i = 0; i < len; i++) {
      let el = arr[i];
      let j;

      for (
        j = i - 1;
        j >= 0 &&
        parseInt(arr[j].RefContentPageDPos) > parseInt(el.RefContentPageDPos);
        j--
      ) {
        arr[j + 1] = arr[j];
      }
      arr[j + 1] = el;
    }
    return arr;
  };

  copyClipboard = () => {
    Clipboard.setString(this.state.promoCode);
    SimpleToast.show('Voucher Berhasil Disalin', Toast.SHORT);
  };

  //Component

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
        onPressNegativeButton={this.handleNegativeButtonAlert}
      />
    );
  };

  button = (input) => {
    if (input == 'Modal') {
      return (
        <>
          {/* <TouchableOpacity
            style={styles.btnAjukan}
            onPress={() => this.submitData()}>
            <Text style={styles.textbutton}>Gunakan Voucher</Text>
          </TouchableOpacity> */}
        </>
      );
    } else {
      return (
        <>
          <View style={{marginTop: 100}}>
            <TouchableOpacity
              disabled={this.state.pressed}
              style={styles.btnVoucher}
              onPress={() => this.showModal()}>
              <Text style={styles.textbutton2}>Dapatkan Voucher</Text>
            </TouchableOpacity>

            {/* <TouchableOpacity
              disabled={this.state.pressed}
              style={styles.btnAjukan}
              onPress={() => this.submitData()}>
              <Text style={styles.textbutton}>Ajukan Sekarang</Text>
            </TouchableOpacity> */}
          </View>
        </>
      );
    }
  };

  modalVoucher = () => {
    return (
      <>
        <ModalVoucher
          animationIn="slideInUp"
          animationOut="slideOutDown"
          onBackdropPress={this.setModalVoucher}
          onSwipeComplete={this.setModalVoucher}
          swipeDirection="right"
          isVisible={this.state.modalVoucherVisible}
          deviceWidth={Dimensions.get('window').width}
          deviceHeight={Dimensions.get('window').height}>
          <View style={styles.modalContainer}>
            <Text style={styles.headerTextModal}>Kode Voucher</Text>
            <View style={styles.promoCodeContainer}>
              <Text style={styles.promoCodeText}>{this.state.promoCode}</Text>
            </View>
            <TouchableOpacity
              style={{marginTop: 16}}
              onPress={this.copyClipboard}>
              <View style={styles.iconContainer}>
                <Icon type="Ionicons" name="copy-outline" style={styles.icon} />
                <Text style={styles.iconText}>Salin</Text>
              </View>
            </TouchableOpacity>
            <View style={styles.descriptionTextCont}>
              <Text style={styles.descriptionText}>
                Kode voucher hanya dapat digunakan 1 kali per user
              </Text>
            </View>
            <View style={{height: 20}} />
            {this.button('Modal')}
          </View>
        </ModalVoucher>
      </>
    );
  };

  render() {
    const {submitting, getPengajuanDataLoading} = this.props;
    const {arr, loading} = this.state;

    // note data nya pake dummy untuk styling
    const listdetailPromo = arr.map((result, key) => {
      if (result.lenght !== 0) {
        return (
          <ListPromo
            title={result.ContentDName}
            // content={result.ContentDDescr.includes('<') ?
            //   regexHTML(result.ContentDDescr) :
            //   result.ContentDDescr}
            content={result.ContentDDescr}
          />
        );
      } else {
        <View>
          <Text>No Data</Text>
        </View>;
      }
    });
    return (
      <Container style={{flex: 1}}>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid title="Promo" goBack={this.goBack} />
        ) : (
          <SubHeaderIos title="Promo" goBack={this.goBack} />
        )}

        <Spinner
          visible={this.state.spinnerLoading ? true : false}
          textContent={'Sedang memproses...'}
          overlayColor="rgba(0, 0, 0, 0.80)"
          textStyle={{color: '#FFF'}}
        />

        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />

        <Content disableKBDismissScroll={false}>
          <Image
            resizeMode="stretch"
            style={styles.imagePromoDetail}
            source={{uri: this.props.route.params.image}}
          />

          <Text style={styles.textHead}>{this.state.title}</Text>

          {/* border bottom dot */}
          <View style={styles.borderDash}>
            <View style={styles.borderSecond} />
          </View>
          <Card>
            <CardItem>
              <Left>
                <Text style={styles.textRow}>Berlaku hingga</Text>
              </Left>
              <Right>
                <Text style={styles.textRow}> {this.state.dateEnd}</Text>
              </Right>
            </CardItem>
          </Card>
          {listdetailPromo}
          {arr.length !== 0 ? (
            this.state.containPromo ? (
              this.button()
            ) : (
              <></>
            )
          ) : loading ? (
            <>
              <SpinnerNative color="blue" />
            </>
          ) : (
            <></>
          )}
        </Content>
        {this.showAlert()}
        {this.modalVoucher()}
      </Container>
    );
  }
}
function mapStateToProps(state) {
  return {
    detailUserResult: state.detailUser.result,
    refContentDResult: state.refContentD.result,
    refContentDError: state.refContentD.error,
    refContentDLoading: state.refContentD.loading,
    getPengajuanDataResult: state.getPengajuanData.result,
    getPengajuanDataError: state.getPengajuanData.error,
    getPengajuanDataLoading: state.getPengajuanData.loading,
    getVoucherCodeResult: state.getVoucherCode.result,
    getVoucherCodeError: state.getVoucherCode.error,
    getVoucherCodeLoading: state.getVoucherCode.loading,
    refreshToken_result: state.refreshToken.result,
    refreshToken_error: state.refreshToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      detailUser,
      getPengajuanData,
      refreshToken,
      refContentD,
      getVoucherCode,
      refreshToken,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, matchDispatchToProps)(PromoDetail);
