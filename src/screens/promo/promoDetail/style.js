import {StyleSheet, Dimensions, PixelRatio} from 'react-native';
import {BAF_COLOR_BLUE} from '../../../utils/constant';
const {width: WIDTH} = Dimensions.get('window');
import {fonts} from '../../../utils/fonts';

export const styles = StyleSheet.create({
  imagePromoDetail: {
    width: WIDTH,
    height: WIDTH * 0.44,
    alignSelf: 'center',
  },

  imageBelow: {
    marginRight: '5%',
  },

  textHead: {
    fontSize: 20,
    fontFamily: fonts.primary.bold,
    color: '#002f5f',
    textAlign: 'center',
    alignSelf: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  textRow: {
    color: 'black',
    fontFamily: fonts.primary.bold,
    fontSize: 12,
  },
  textUp: {
    color: 'black',
    fontFamily: fonts.primary.bold,
    fontSize: 15,
    paddingRight: 20,
  },
  modalContainer: {
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    paddingVertical: 40,
  },
  headerTextModal: {
    color: BAF_COLOR_BLUE,
    fontSize: 22,
    textAlign: 'center',
    fontFamily: fonts.primary.bold,
  },
  iconText: {
    fontFamily: fonts.primary.normal,
    color: 'rgba(0, 0, 0, 0.6)',
    fontSize: 14,
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  descriptionTextCont: {marginTop: 20, maxWidth: 200, alignItems: 'center'},
  descriptionText: {
    textAlign: 'center',
    fontFamily: fonts.primary.normal,
    color: 'rgba(0, 0, 0, 0.6)',
    fontSize: 16,
  },
  icon: {
    fontSize: 18,
    marginRight: 4,
    color: BAF_COLOR_BLUE,
  },
  promoCodeText: {
    fontSize: 18,
    fontFamily: fonts.primary.normal,
    color: BAF_COLOR_BLUE,
    textAlign: 'center',
  },
  promoCodeContainer: {
    marginTop: 30,
    borderRadius: 1,
    borderWidth: 1,
    borderColor: '#002F5F',
    maxHeight: 150,
    minWidth: 226,
    maxWidth: 277,
    borderStyle: 'dashed',
    padding: 14,
  },
  textbutton: {
    color: 'white',
    fontSize: 18,
    fontFamily: fonts.primary.normal,
  },
  textbutton2: {
    color: BAF_COLOR_BLUE,
    fontSize: 18,
    fontFamily: fonts.primary.normal,
  },
  bodyCard: {
    marginRight: '-60%',
  },
  iconStyle: {
    fontSize: 20,
    paddingRight: 10,
    paddingLeft: 5,
  },
  cardStyle: {
    marginTop: -20,
  },
  borderDash: {
    height: 1,
    width: '100%',
    borderRadius: 1,
    borderWidth: 1,
    borderColor: 'gray',
    borderStyle: 'dashed',
    zIndex: 1,
  },
  borderSecond: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    width: '100%',
    height: 1,
    backgroundColor: 'white',
    zIndex: 1,
  },
  btnAjukan: {
    backgroundColor: '#002F5F',
    width: '70%',
    height: 40,
    marginBottom: 20,

    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  btnVoucher: {
    borderColor: '#002F5F',
    width: '70%',
    height: 40,
    marginBottom: 20,
    borderWidth: 1,

    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },

  textParagraph: {
    fontSize: 14,
    lineHeight: 18,
    marginBottom: 10,
    textAlign: 'justify',
    color: 'rgba(0, 0, 0, 0.8)',
    width: WIDTH * 0.8,
    fontFamily: fonts.primary.normal,
  },

  // scrolable modal
  view: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  scrollableModal: {
    height: 400,
  },

  scrollableModalContent1: {
    height: 400,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },

  modalHeight: {
    height: 760,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },

  titleModal: {
    color: 'black',
    fontSize: 18,
    fontFamily: fonts.primary.bold,
    marginTop: 0,
    marginBottom: 15,
  },
  subtitleModal: {
    fontSize: 14,
    fontFamily: fonts.primary.bold,
    marginBottom: 10,
    color: 'black',
  },
});
