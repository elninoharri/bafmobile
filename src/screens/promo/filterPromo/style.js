import {StyleSheet, Dimensions, PixelRatio} from 'react-native';
const {width: WIDTH} = Dimensions.get('window');
import {fonts} from '../../../utils/fonts';

export const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  headView: {
    marginTop: 20,
    marginLeft: 40,
    marginBottom: 37,
  },
  textUp: {
    fontSize: 18,
    fontFamily: fonts.primary.normal,
    color: 'black',
  },
  checkBoxText: {
    marginLeft: 5,
    fontSize: 18,
    color: 'black',
    fontFamily: fonts.primary.normal,
  },
  rowCheckBox: {
    flexDirection: 'row',
    marginLeft: 51,
  },

  line: {
    borderStyle: 'solid',
    borderWidth: 2,
    marginTop: 10,
    marginBottom: 10,
    borderColor: 'rgba(220, 221, 223, 0.5)',
  },
  btnHasil: {
    alignSelf: 'center',
    width: '80%',
    height: 40,
    borderRadius: 10,
    backgroundColor: '#002f5f',
    justifyContent: 'center',
    marginTop: 40,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
    fontFamily: fonts.primary.bold,
  },

  toast: {
    marginTop:
      Platform.OS != 'android'
        ? Dimensions.get('window').width * PixelRatio.get() > 750
          ? 44
          : 20
        : 0,
    backgroundColor: '#FFE6E9',
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'red',
    justifyContent: 'center',
    paddingLeft: 50,
    height: 50,
    borderRadius: 0,
  },
});
