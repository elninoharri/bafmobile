import React, {Component} from 'react';
import {Text, Image, View, TouchableOpacity} from 'react-native';
import {
  Container,
  Left,
  Icon,
  CardItem,
  Card,
  Content,
  Row,
  Right,
  Body,
} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import Toast, {DURATION} from 'react-native-easy-toast';
import {styles} from './style';
import CheckBox from '@react-native-community/checkbox';
import {filterBanner, detailBanner} from '../../../actions/home';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import Spinner from 'react-native-loading-spinner-overlay';

import {TYPE_BANNER_PROMOTION} from '../../../utils/constant';

class FilterPromo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Filter Promo',
      BNMC: false,
      BCAR: false,
      BMP: false,
      BSYANA: false,
      OTHERS: false,
      choice: false,
      detailUser: false,
      isConnected: false,
    };
  }

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  componentDidMount = async () => {
    const {option} = this.props;

    const jsonDetailUser = await AsyncStorage.getItem('detailUser');
    if (jsonDetailUser) {
      var detailUser = JSON.parse(jsonDetailUser);
      this.setState({detailUser: detailUser});
    }

    if (option) {
      this.onValueChange(true, option);
    }
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  sendHasil = async () => {
    const {detailUser} = this.state;
    this.props.filterBanner(this.state.choice);
    this.props.navigation.navigate('Home');

    const {detailBanner} = this.props;
    NetInfo.fetch().then(async (state) => {
      if (state.isConnected) {
        detailBanner({
          BannerID: '',
          Type: TYPE_BANNER_PROMOTION,
          Usergroupid: detailUser ? detailUser.UserGroupID : '',
          Category: this.state.choice,
        });
      }
    });
  };

  onValueChange = (value, valueName) => {
    if (value) {
      this.setState({choice: valueName});
    } else {
      this.setState({choice: ''});
    }

    switch (valueName) {
      case 'BNMC':
        this.setState({
          BNMC: value,
          BCAR: false,
          BMP: false,
          BSYANA: false,
          OTHERS: false,
        });
        break;
      case 'BCAR':
        this.setState({
          BCAR: value,
          BNMC: false,
          BMP: false,
          BSYANA: false,
          OTHERS: false,
        });
        break;
      case 'BMP':
        this.setState({
          BMP: value,
          BNMC: false,
          BCAR: false,
          BSYANA: false,
          OTHERS: false,
        });
        break;
      case 'BSYANA':
        this.setState({
          BSYANA: value,
          BMP: false,
          BNMC: false,
          BCAR: false,
          OTHERS: false,
        });
        break;
      case 'OTHERS':
        this.setState({
          OTHERS: value,
          BMP: false,
          BNMC: false,
          BSYANA: false,
          BCAR: false,
        });
        break;
      default:
        this.setState({
          OTHERS: false,
          BCAR: false,
          BMP: false,
          BSYANA: false,
          BNMC: false,
        });
    }
  };

  handlePositiveButtonAlert = () => {
    this.goBack();
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  render() {
    const {handleSubmit, submitting, detailBannerLoading} = this.props;
    return (
      <Container style={styles.page}>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}

        <Spinner
          visible={detailBannerLoading ? true : false}
          textContent={'Sedang memproses...'}
          overlayColor="rgba(0, 0, 0, 0.80)"
          textStyle={{color: '#FFF'}}
        />

        <View style={styles.headView}>
          <Text style={styles.textUp}>Pilih Kategori Promo</Text>
        </View>

        <Content style={{backgroundColor: 'white'}}>
          <Toast
            ref="toast"
            style={styles.toast}
            position="top"
            positionValue={0}
            fadeInDuration={2000}
            fadeOutDuration={1000}
            opacity={0.9}
          />

          <View style={styles.rowCheckBox}>
            <CheckBox
              disabled={false}
              value={this.state.BNMC}
              tintColors={{true: '#002f5f'}}
              onValueChange={(value) => this.onValueChange(value, 'BNMC')}
            />
            <Text style={styles.checkBoxText}>Motor</Text>
          </View>
          <View style={styles.line}></View>

          <View style={styles.rowCheckBox}>
            <CheckBox
              disabled={false}
              value={this.state.BCAR}
              tintColors={{true: '#002f5f'}}
              onValueChange={(value) => this.onValueChange(value, 'BCAR')}
            />
            <Text style={styles.checkBoxText}>Mobil</Text>
          </View>
          <View style={styles.line}></View>

          <View style={styles.rowCheckBox}>
            <CheckBox
              disabled={false}
              value={this.state.BMP}
              tintColors={{true: '#002f5f'}}
              onValueChange={(value) => this.onValueChange(value, 'BMP')}
            />
            <Text style={styles.checkBoxText}>Multi Produk</Text>
          </View>
          <View style={styles.line}></View>

          <View style={styles.rowCheckBox}>
            <CheckBox
              disabled={false}
              value={this.state.BSYANA}
              tintColors={{true: '#002f5f'}}
              onValueChange={(value) => this.onValueChange(value, 'BSYANA')}
            />
            <Text style={styles.checkBoxText}>Dana Syariah</Text>
          </View>
          <View style={styles.line}></View>

          <View style={styles.rowCheckBox}>
            <CheckBox
              disabled={false}
              value={this.state.OTHERS}
              tintColors={{true: '#002f5f'}}
              onValueChange={(value) => this.onValueChange(value, 'OTHERS')}
            />
            <Text style={styles.checkBoxText}>Lainnya</Text>
          </View>
          <View style={styles.line}></View>

          <TouchableOpacity
            style={styles.btnHasil}
            onPress={() => this.sendHasil()}
            disabled={submitting}>
            <Text style={styles.text}>Tampilkan Hasil</Text>
          </TouchableOpacity>

          <Content disableKBDismissScroll={false}></Content>
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    option: state.filterBanner.result,
    detailBannerResult: state.detailBanner.result,
    detailBannerLoading: state.detailBanner.loading,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      detailBanner,
      filterBanner,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, matchDispatchToProps)(FilterPromo);
