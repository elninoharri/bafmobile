import React, {Component} from 'react';
import {
  Text,
  Alert,
  View,
  BackHandler,
  Platform,
  Dimensions,
  PixelRatio,
  Linking,
  NativeModules,
} from 'react-native';
import {Spinner, Container, Icon} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import {checkVersion, checkToken} from '../../../actions';
import {createTableBanner} from '../../../sqlite/query';
import {openDatabase} from 'react-native-sqlite-storage';
import {APP_VERSION, ERROR_AUTH} from '../../../utils/constant';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Toast, {DURATION} from 'react-native-easy-toast';
import {navigateScreen} from '../../../App';
import JailMonkey from 'jail-monkey';
import {JailMonkeyUtils} from '../../../utils/utilization';

let db = openDatabase({name: 'sqlite_bafmobile.db', createFromLocation: 1});
class AuthLoading extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Auth Loading',
      isConnected: false,
      userData: false,
      userToken: false,
      dataBanner: [],

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
    };
  }

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      BackHandler.exitApp();
      return true;
    }
  };

  checkInternet = async () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.refs.toast.show(
          <View style={{flexDirection: 'row'}}>
            <Icon
              type="FontAwesome"
              name="exclamation-circle"
              style={{color: 'red', marginLeft: '-10%'}}
            />
            <Text
              style={{
                marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
                marginLeft: '3%',
                fontSize: 16,
                color: 'red',
              }}>
              Tidak Ada Koneksi Internet
            </Text>
          </View>,
          DURATION.LENGTH_LONG,
        ); // show toast when no connection
      } else {
        this.getStateAsync(state.isConnected);
      }
    });
  };

  componentDidMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    createTableBanner(); // create table Banner if not exist

    //jailmonkey ini di bypass ketika development untuk mencegah false alarm
    //gampangnya, pasang ! dibelakang method jailmonkey

    const JailMonkeyVerification = await JailMonkeyUtils();

    if (JailMonkeyVerification) {
      // is this device JailBroken on iOS/Android?
      this.handleJail();
    } else {
      this.checkInternet();
    }
  };

  handleJail = () => {
    this.setState({
      alertFor: 'jailMonkey',
      alertShowed: true,
      alertMessage: 'Sepertinya dalam perangkat anda terdeteksi bahaya',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'Ok',
    });
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      checkVersionResult,
      checkVersionError,
      checkTokenResult,
      checkTokenError,
    } = this.props;
    const {isConnected, userData, userToken} = this.state;

    if (isConnected && prevState.isConnected !== isConnected) {
      this.getStateAsync(isConnected);
    }

    if (
      checkVersionResult &&
      prevProps.checkVersionResult !== checkVersionResult
    ) {
      if (checkVersionResult.GSettingDescr !== APP_VERSION) {
        this.setState({
          alertFor: 'updateAppConfirm',
          alertShowed: true,
          alertMessage: 'Versi belum update. Update aplikasi?',
          alertTitle: 'Konfirmasi',
          alertType: 'confirm',
          alertDoneText: 'Ya',
          alertCancelText: 'Tidak',
        });
      } else {
        this.actionCheckToken();
      }
    }

    if (
      checkVersionError &&
      prevProps.checkVersionError !== checkVersionError
    ) {
      this.actionCheckToken();
    }

    if (checkTokenResult && prevProps.checkTokenResult !== checkTokenResult) {
      if (navigateScreen !== '') {
        console.log(navigateScreen);
        this.props.navigation.replace('OrderList');
      } else {
        this.props.navigation.replace('Home');
      }
    }

    if (checkTokenError && prevProps.checkTokenError !== checkTokenError) {
      this.setState({
        alertFor: 'checkTokenError',
        alertShowed: true,
        alertMessage: 'Sesi login Anda sudah habis',
        alertTitle: 'Informasi',
        alertType: 'info',
        alertDoneText: 'OK',
      });
      await AsyncStorage.removeItem('userToken');
      await AsyncStorage.removeItem('userData');
      await AsyncStorage.removeItem('detailUser');
      await AsyncStorage.removeItem('activeAgreement');

      // if (checkTokenError.message === ERROR_AUTH) {
      //   this.setState({
      //     alertFor: 'checkTokenError',
      //     alertShowed: true,
      //     alertMessage: 'Sesi login Anda sudah habis',
      //     alertTitle: 'Informasi',
      //     alertType: 'info',
      //     alertDoneText: 'OK'
      //   })
      //   await AsyncStorage.removeItem('userToken')
      //   await AsyncStorage.removeItem('userData')
      //   await AsyncStorage.removeItem('detailUser')
      //   await AsyncStorage.removeItem('activeAgreement')
      // } else {
      //   this.setState({
      //     alertFor: 'checkTokenError',
      //     alertShowed: true,
      //     alertMessage: checkTokenError.message,
      //     alertTitle: 'Gagal',
      //     alertType: 'error',
      //     alertDoneText: 'OK'
      //   })
      // }
    }
  };

  getStateAsync = async (isConnected) => {
    const {checkVersion} = this.props;

    // get all resource from asyncStorage
    const firstInstall = await AsyncStorage.getItem('firstInstall');
    const userToken = await AsyncStorage.getItem('userToken');
    const userData = await AsyncStorage.getItem('userData');

    if (!firstInstall) {
      this.props.navigation.replace('Welcome'); // go to welcome screen when first time install
    } else {
      if (userToken && userData) {
        this.setState({
          // set state userToken and userData
          userToken: userToken,
          userData: JSON.parse(userData),
        });
      }

      isConnected
        ? checkVersion(userToken)
        : this.props.navigation.replace('Home');
    }
  };

  actionCheckToken = async () => {
    const {userToken} = this.state;
    const {checkToken} = this.props;

    userToken ? checkToken(userToken) : this.props.navigation.replace('Home');
  };

  actionUpdateApp = async () => {
    const URL =
      Platform.OS === 'android'
        ? 'https://play.google.com/store/apps/details?id=com.id.bussanautofinance&hl=en'
        : 'https://apps.apple.com/us/app/baf-mobile/id1499452126';

    Linking.openURL(URL).catch((err) => {
      this.setState({
        alertFor: 'updateAppAction',
        alertShowed: true,
        alertMessage: 'Gagal buka url untuk update aplikasi',
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
      });
    });
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
        onPressNegativeButton={this.handleNegativeButtonAlert}
      />
    );
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
    if (
      this.state.alertFor === 'checkTokenError' ||
      this.state.alertFor === 'updateAppAction'
    ) {
      this.props.navigation.replace('Home');
    } else if (this.state.alertFor === 'updateAppConfirm') {
      this.actionUpdateApp();
    } else if (this.state.alertFor === 'jailMonkey') {
      BackHandler.exitApp();
    }
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
    if (this.state.alertFor === 'updateAppConfirm') {
      this.actionCheckToken();
    }
  };

  // getBanner = () => {
  //   let temp = [];
  //   db.transaction((tx) => {
  //     tx.executeSql('SELECT * FROM BANNER', [], (tx, results) => {
  //       for (let i = 0; i < results.rows.length; ++i) {
  //         temp.push(results.rows.item(i));
  //       }
  //       this.setState({dataBanner: temp});
  //       db.close();
  //     });
  //   });
  // };

  // insertBanner = (data) => {
  //   let arrValues = [];

  //   for (let i = 0; i < data.length; i++) {
  //     data[i].employee_name = `"${data[i].employee_name}"`;
  //     data[i].employee_age = `"${data[i].employee_age}"`;

  //     arrValues.push(
  //       '(' + data[i].employee_name + ',' + data[i].employee_age + ')',
  //     );
  //   }

  //   db.transaction((tx) => {
  //     tx.executeSql('DELETE FROM BANNER', []);
  //     tx.executeSql(
  //       'INSERT INTO BANNER (BANNER_NAME, BANNER_DESC) VALUES ' +
  //         String(arrValues),
  //       [],
  //       (tx, results) => {
  //         db.close();
  //         return results;
  //       },
  //     );
  //   });
  // };

  render() {
    return (
      <Container
        style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        {Platform.OS != 'android' ? (
          Dimensions.get('window').width * PixelRatio.get() > 750 ? (
            <Toast
              ref="toast"
              style={{
                backgroundColor: '#FFE6E9',
                width: '100%',
                borderBottomWidth: 2,
                borderBottomColor: 'red',
                justifyContent: 'center',
                paddingLeft: 50,
                height: 50,
                borderRadius: 0,
              }}
              position="top"
              positionValue={'5%'}
              fadeInDuration={2000}
              fadeOutDuration={1000}
              opacity={0.9}
            />
          ) : (
            <Toast
              ref="toast"
              style={{
                backgroundColor: '#FFE6E9',
                width: '100%',
                borderBottomWidth: 2,
                borderBottomColor: 'red',
                justifyContent: 'center',
                paddingLeft: 50,
                height: 50,
                borderRadius: 0,
              }}
              position="top"
              positionValue={'3%'}
              fadeInDuration={2000}
              fadeOutDuration={1000}
              opacity={0.9}
            />
          )
        ) : (
          <Toast
            ref="toast"
            style={{
              backgroundColor: '#FFE6E9',
              width: '100%',
              borderBottomWidth: 2,
              borderBottomColor: 'red',
              justifyContent: 'center',
              paddingLeft: 50,
              height: 50,
              borderRadius: 0,
            }}
            position="top"
            positionValue={'0%'}
            fadeInDuration={2000}
            fadeOutDuration={1000}
            opacity={0.9}
          />
        )}
        <Spinner color="#002f5f" />
        <Text style={{color: '#002f5f', fontSize: 10}}>
          Proses ini membutuhkan koneksi internet
        </Text>
        {this.showAlert()}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    checkVersionResult: state.checkVersion.result,
    checkVersionLoading: state.checkVersion.loading,
    checkVersionError: state.checkVersion.error,
    checkTokenResult: state.checkToken.result,
    checkTokenLoading: state.checkToken.loading,
    checkTokenError: state.checkToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      checkVersion,
      checkToken,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, matchDispatchToProps)(AuthLoading);
