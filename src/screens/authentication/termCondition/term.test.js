import React from 'react';
import {Provider} from 'react-redux';
import {store} from '../../../store/store';
import {fireEvent, render} from 'react-native-testing-library';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import TermCondition from '.';

const Stack = createStackNavigator();

const component = (
  <Provider store={store}>
    <NavigationContainer>
      <Stack.Navigator headerMode="none" initialRouteName="TermCondition">
        <Stack.Screen name="TermCondition" component={TermCondition} />
      </Stack.Navigator>
    </NavigationContainer>
  </Provider>
);

test('When click Button Saya Setuju show pop up notifikasi Terima Kasih', async () => {
  const {findByText} = render(component);
  const toClick = await findByText('Saya Setuju');

  fireEvent(toClick, 'press');

  const newBody = await findByText('Terima Kasih');
  expect(newBody).toBeTruthy();
});

