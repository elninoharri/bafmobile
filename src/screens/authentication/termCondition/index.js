import React, {Component} from 'react';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import {Container, Row, Icon, Content, Accordion} from 'native-base';
import {
  TouchableOpacity,
  Text,
  View,
  Platform,
  Dimensions,
  PixelRatio,
  BackHandler,
} from 'react-native';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import NetInfo from '@react-native-community/netinfo';
import data from './term.json';
import Toast from 'react-native-easy-toast';
import {styles} from './style';
import AsyncStorage from '@react-native-community/async-storage';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import analytics from '@react-native-firebase/analytics';

class TermCondition extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Kebijakan Privasi',
      detailUser: false,

      isConnected: false,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
    };
  }

  componentDidMount = async () => {
    analytics().logScreenView({
      screen_class: 'screenTermCondition',
      screen_name: 'screenTermCondition',
    });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this.checkInternet();
    const jsonDetailUser = await AsyncStorage.getItem('detailUser');
    if (jsonDetailUser) {
      var detailUser = JSON.parse(jsonDetailUser);
      this.setState({detailUser: detailUser});
    }
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected});
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  _renderHeader(item, expanded) {
    return (
      <View>
        {item.id % 2 ? (
          <View style={styles.renderHeader}>
            <Text style={styles.title}>{item.title}</Text>
            {expanded ? (
              <Icon style={{fontSize: 18}} name="remove" />
            ) : (
              <Icon style={{fontSize: 18}} name="add" />
            )}
          </View>
        ) : (
          <View style={styles.renderHeader2}>
            <Text style={styles.title}>{item.title}</Text>
            {expanded ? (
              <Icon style={{fontSize: 18}} name="remove" />
            ) : (
              <Icon style={{fontSize: 18}} name="add" />
            )}
          </View>
        )}
      </View>
    );
  }

  _renderContent = (content) => {
    let number = 0;
    return content.content.map((item) => {
      number++;
      if (content.lenght !== 0) {
        return (
          <View style={{backgroundColor: '#F4F4F4'}}>
            <View style={{marginTop: 4, marginBottom: 4}}>
              <View style={styles.row}>
                <View style={{width: 20}}>
                  <Text style={styles.textDefault}>{number + '. '}</Text>
                </View>
                <View style={{flex: 1}}>
                  <Text style={styles.textDefault}>{item}</Text>
                </View>
              </View>
            </View>
          </View>
        );
      } else {
        return <View></View>;
      }
    });
  };

  toastComponent = () => {
    return (
      <Toast
        ref="toast"
        style={{
          marginTop:
            Platform.OS != 'android'
              ? Dimensions.get('window').width * PixelRatio.get() > 750
                ? 44
                : 20
              : 0,
          backgroundColor: '#FFE6E9',
          width: '100%',
          borderBottomWidth: 2,
          borderBottomColor: 'red',
          justifyContent: 'center',
          paddingLeft: 50,
          height: 50,
          borderRadius: 0,
        }}
        position="top"
        positionValue={0}
        fadeInDuration={2000}
        fadeOutDuration={1000}
        opacity={0.9}
      />
    );
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  sendSetuju = async () => {
    if (this.state.detailUser) {
      console.log('data user nya sudah ada');
    } else {
      this.setState({
        alertFor: 'approveEula',
        alertShowed: true,
        alertMessage: 'Data anda sudah terverifikasi',
        alertTitle: 'Terima Kasih',
        alertType: 'success',
        alertDoneText: 'OK',
      });
    }
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  handlePositiveButtonAlert = () => {
    if ((this.props.alertFor = 'approveEula')) {
      this.goBack();
    }
    this.setState({
      alertShowed: false,
      alertFor: null,
    });
  };

  render() {
    const head = data.head.map((result) => {
      if (result.lenght !== 0) {
        return <Text style={styles.textHead}> {result.desc}</Text>;
      }
    });

    return (
      <Container>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}

        <Content>
          {this.toastComponent()}
          <View style={styles.head}>{head}</View>

          <Accordion
            dataArray={data.paragraf}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
            expanded={0}
            icon={'add'}
            expandedIcon={'remove'}
          />

          <View style={{paddingVertical: '3%', paddingHorizontal: '27%'}}>
            <View>
              <Row style={{alignSelf: 'center'}}>
                {!this.state.detailUser ? (
                  <>
                    <TouchableOpacity
                      style={styles.btnBatal}
                      onPress={() => this.goBack()}>
                      <Text style={styles.textBtnBatal}>Batal</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.btnSetuju}
                      onPress={() => this.sendSetuju()}>
                      <Text style={styles.textBtnSetuju}>Saya Setuju</Text>
                    </TouchableOpacity>
                  </>
                ) : (
                  <>
                    <TouchableOpacity
                      disabled={true}
                      style={styles.btnBatalDisable}
                      onPress={() => this.goBack()}>
                      <Text style={styles.textDisable}>Batal</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      disabled={true}
                      style={styles.btnSetujuDisable}
                      onPress={() => this.sendSetuju()}>
                      <Text style={styles.textBtnSetuju}>Saya Setuju</Text>
                    </TouchableOpacity>
                  </>
                )}
              </Row>
            </View>
          </View>
          {this.showAlert()}
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(TermCondition);
