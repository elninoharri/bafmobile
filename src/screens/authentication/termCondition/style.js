import {StyleSheet} from 'react-native';
import {fonts} from '../../../utils/fonts';

export const styles = StyleSheet.create({
  content: {
    fontFamily: fonts.primary.normal,
    fontSize: 14,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    marginLeft: 38,
    marginRight: 29,
  },
  head: {
    marginLeft: 32,
    marginRight: 31,
    marginTop: 31,
    marginBottom: 10,
  },
  textHead: {
    textAlign: 'justify',
    lineHeight: 25,
    fontFamily: fonts.primary.normal,
  },

  title: {
    fontFamily: fonts.primary.bold,
    fontSize: 14,
  },

  renderHeader: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#F4F4F4',
  },
  renderHeader2: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
  },

  textDefault: {
    fontFamily: fonts.primary.normal,
    color: 'rgba(0, 0, 0, 0.8)',
    textAlign: 'justify',
  },

  textBtnBatal: {
    color: '#002F5F',
    fontSize: 14,
    fontFamily: fonts.primary.normal,
  },
  textDisable: {
    color: '#C4C4C4',
    fontSize: 14,
    fontFamily: fonts.primary.bold,
  },

  textBtnSetuju: {
    color: 'white',
    fontSize: 14,
    fontFamily: fonts.primary.normal,
  },

  btnBatal: {
    backgroundColor: 'white',
    flexDirection: 'row',
    width: '95%',
    height: 40,
    marginVertical: 10,
    marginRight: 20,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: '#002F5F',
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },

  btnBatalDisable: {
    backgroundColor: 'white',
    flexDirection: 'row',
    width: '95%',
    height: 40,
    marginVertical: 10,
    marginRight: 20,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: '#C4C4C4',
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  btnSetuju: {
    backgroundColor: '#002F5F',
    flexDirection: 'row',
    width: '95%',
    height: 40,
    marginVertical: 10,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },

  btnSetujuDisable: {
    backgroundColor: '#C4C4C4',
    flexDirection: 'row',
    width: '95%',
    height: 40,
    marginVertical: 10,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
});
