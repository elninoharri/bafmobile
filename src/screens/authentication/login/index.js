import React, {Component} from 'react';
import {
  View,
  Text,
  Platform,
  BackHandler,
  Image,
  TouchableOpacity,
  Alert,
  Keyboard,
  Dimensions,
  PixelRatio,
} from 'react-native';
import {
  Container,
  Icon,
  Form,
  Label,
  Input,
  Item,
  ListItem,
  CheckBox,
  Right,
  Left,
  Body,
  Content,
} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import SubHeaderAndroid from '../../../components/android/subHeader';
import SubHeaderIos from '../../../components/ios/subHeader';
import {
  login,
  loginOpenid,
  saveToken,
} from '../../../actions/authentication/login';
import {refreshToken} from '../../../actions';
import Toast, {DURATION} from 'react-native-easy-toast';
import {styles} from './style';
import {Field, reduxForm, change, reset} from 'redux-form';
import {FormLoginValidate} from '../../../validates/FormLoginValidate';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Spinner from 'react-native-loading-spinner-overlay';
import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import FingerprintPopup from '../../../components/multiPlatform/fingerprint/componentPopup';
import {fonts} from '../../../utils/fonts';
import analytics from '@react-native-firebase/analytics';
import {fcmService} from '../../../pushnotif/FCMService';
import {localNotificationService} from '../../../pushnotif/LocalNotificationService';
import FingerprintScanner from 'react-native-fingerprint-scanner';
import JailMonkey from 'jail-monkey';
import {MESSAGE_TOKEN_EXP, TOKEN_EXP} from '../../../utils/constant';
import {
  jsonParse,
  handleRefreshToken,
  JailMonkeyUtils,
} from '../../../utils/utilization';
import Keychain from 'react-native-keychain';

export const renderField = ({
  input,
  type,
  label,
  icon,
  iconright,
  placeholder,
  editable,
  keyboardType,
  maxLength,
  secureTextEntry,
  testId,
  name,
  onPressIcon,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{marginVertical: 10, left: -15, width: '105%'}}>
      <Item
        fixedLabel
        style={{borderColor: hasError ? 'red' : '#666', height: 40}}>
        <Icon
          active
          type="FontAwesome"
          name={icon}
          style={{color: hasError ? 'red' : '#666'}}
        />
        <Input
          {...input}
          type={type}
          testID={testId}
          editable={editable}
          placeholder={placeholder}
          keyboardType={keyboardType}
          maxLength={maxLength}
          secureTextEntry={secureTextEntry}
          style={{fontSize: 14, fontFamily: fonts.primary.normal}}
        />
        {iconright ? (
          <Icon
            active
            name={iconright}
            style={{color: '#666'}}
            onPress={onPressIcon}
          />
        ) : null}
      </Item>
      {hasError ? <Text style={styles.errorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderFieldHidden = ({
  input,
  type,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '100%'}}>
      <Item style={{display: 'none'}}>
        <Input {...input} ype={type} />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

class Login extends Component {
  constructor(props) {
    super(props);
    this.goLogin = this.goLogin.bind(this);
    this.state = {
      screenTitle: 'Login',
      userInfo: {},
      isConnected: false,
      checked: false,
      userLogin: false,
      icon: 'eye',
      hide: true,
      fingerPrint: false,
      userId: false,
      userToken: false,
      firebaseToken: false,
      email: false,
      popupShowed: false,
      fingerDetected: true,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
    };
  }

  componentDidMount = async () => {
    analytics().logScreenView({
      screen_class: 'screenLogin',
      screen_name: 'screenLogin',
    });

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

    //jailmonkey ini di bypass ketika development untuk mencegah false alarm
    //gampangnya, pasang ! dibelakang method jailmonkey

    const JailMonkeyVerification = await JailMonkeyUtils();

    if (JailMonkeyVerification) {
      this.handleJail();
    } else {
      const rememberMe = await AsyncStorage.getItem('rememberMe');
      if (rememberMe) {
        var userLogin = JSON.parse(rememberMe);
        this.setState({userLogin: userLogin, checked: true});
        this.props.updateField('formLogin', 'phoneNo', userLogin.Phoneno);
        this.props.updateField('formLogin', 'password', userLogin.Password);
        this.props.updateField('formLogin', 'rememberMe', '1');
      } else {
        this.setState({checked: false});
        this.props.updateField('formLogin', 'rememberMe', '0');
      }
      this.detectFingerprintAvailable();
      const fingerPrint = await AsyncStorage.getItem('userFinger');

      if (fingerPrint) {
        this.setState({email: fingerPrint, fingerPrint: true});
      }
    }
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      loginResult,
      loginError,
      loginOpenidResult,
      loginOpenidError,
      saveToken,
      saveTokenResult,
      saveTokenError,
      refreshTokenResult,
      refreshTokenError,
    } = this.props;

    if (
      saveTokenResult !== null &&
      prevProps.saveTokenResult !== saveTokenResult
    ) {
      // console.log("saveTokenResult Succes ", saveTokenResult)
    }

    if (
      saveTokenError !== null &&
      prevProps.saveTokenError !== saveTokenError
    ) {
      // console.log("saveTokenError msg " , saveTokenError );
      const data = await jsonParse(saveTokenError.message);
      if (data) {
        if (data.message == MESSAGE_TOKEN_EXP) {
          // console.log("hit refresh token")
          this.props.refreshToken(this.state.userToken);
        }
      }
    }

    if (loginError !== null && prevProps.loginError !== loginError) {
      const data = await jsonParse(loginError.message);
      if (data) {
        this.setState({
          alertShowed: true,
          alertMessage: data.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      } else {
        this.setState({
          alertShowed: true,
          alertMessage: loginError.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
    }

    if (loginResult && prevProps.loginResult !== loginResult) {
      fcmService.registerAppWithFCM();
      fcmService.registerToken(onRegister);

      function onRegister(token) {
        // Save Token Device
        saveToken(
          {
            Token: token,
          },
          loginResult.Usertoken,
        );
      }

      this.setState({
        alertFor: 'loginResult',
        alertShowed: true,
        alertMessage: 'Anda berhasil masuk',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
        userToken: loginResult.Usertoken,
      });

      await AsyncStorage.setItem('userToken', loginResult.Usertoken);
      await AsyncStorage.setItem(
        'userData',
        JSON.stringify({
          Usergroupid: loginResult.UserGroupID,
        }),
      );
    }

    //loginError101
    if (
      loginOpenidError !== null &&
      prevProps.loginOpenidError !== loginOpenidError
    ) {
      this.setState({
        alertShowed: true,
        alertMessage: loginOpenidError.message,
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
      });
    }

    if (
      loginOpenidResult &&
      prevProps.loginOpenidResult !== loginOpenidResult
    ) {
      this.setState({
        alertFor: 'loginOpenidResult',
        alertShowed: true,
        alertMessage: 'Anda berhasil masuk',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
        userToken: loginOpenidResult.Usertoken,
      });

      await AsyncStorage.setItem('userToken', loginOpenidResult.Usertoken);
      await AsyncStorage.setItem(
        'userData',
        JSON.stringify({
          Usergroupid: loginOpenidResult.Usergroupid,
        }),
      );
    }

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      // console.log('Error Refresh Token ', refreshTokenError.message);
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      // console.log('Succes Refresh Token ', refreshTokenResult);
      await handleRefreshToken(refreshTokenResult.Refreshtoken);
      this.setState({userToken: refreshTokenResult.Refreshtoken});
      fcmService.registerToken(onRegister);
      function onRegister(token) {
        saveToken(
          {
            Token: token,
          },
          refreshTokenResult.Refreshtoken,
        );
      }
    }
  };

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  handleJail = () => {
    this.setState({
      alertFor: 'jailMonkey',
      alertShowed: true,
      alertMessage: 'Sepertinya dalam perangkat anda terdeteksi bahaya',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'Ok',
    });
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  goBack = async () => {
    this.props.navigation.goBack();
    this.props.resetForm('formLogin');
  };

  showHidePassword() {
    this.setState((prevState) => ({
      icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
      hide: !prevState.hide,
    }));
  }

  actionCheck = () => {
    this.setState((prevState) => ({
      checked: !prevState.checked,
    }));
    this.state.checked
      ? this.props.updateField('formLogin', 'rememberMe', '0')
      : this.props.updateField('formLogin', 'rememberMe', '1');
  };

  goLogin = async (data) => {
    const {login} = this.props;

    Keyboard.dismiss();

    if (
      data.rememberMe &&
      data.rememberMe == '1' &&
      data.phoneNo &&
      data.phoneNo !== '' &&
      data.password &&
      data.password !== ''
    ) {
      await AsyncStorage.removeItem('rememberMe');
      await AsyncStorage.setItem(
        'rememberMe',
        JSON.stringify({Phoneno: data.phoneNo, Password: data.password}),
      );
    } else {
      await AsyncStorage.removeItem('rememberMe');
    }

    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        analytics().logEvent('eventLogin', {
          item: 'user trying to login',
        });
        login({
          Phoneno: data.phoneNo,
          Password: data.password,
        });
      } else {
        this.showToastNoInternet();
      }
    });
  };

  goRegister = async () => {
    this.props.resetForm('formLogin');
    this.props.navigation.navigate('Register');
  };

  goForgotPassword = async () => {
    this.props.resetForm('formLogin');
    this.props.navigation.navigate('ForgotPassword');
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        testIdPositiveButton="OK"
        testIdNegativeButton="Cancel"
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
    if (
      this.state.alertFor === 'loginResult' ||
      this.state.alertFor === 'loginOpenidResult'
    ) {
      this.props.navigation.goBack();
      // this.props.route.params.returnData(this.props.loginResult);
    } else if (this.state.alertFor === 'jailMonkey') {
      BackHandler.exitApp();
    } else if (this.state.alertFor === 'loginOpenId') {
      const {email} = this.state;
      const {loginOpenid} = this.props;
      loginOpenid({CredCode: 'FINGERPRINT', Email: email});
    }
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  loginFacebook = () => {
    const {loginOpenid} = this.props;

    this.setState({
      alertShowed: true,
      alertMessage: 'Fitur ini sedang dalam tahap pengembangan.',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
    // LoginManager.logInWithPermissions(['public_profile', 'email']).then(
    //   (result) => {
    //     if (result.isCancelled) {
    //       console.log('Login cancelled')
    //     } else {
    //       const PROFILE_REQUEST_PARAMS = {
    //         fields: { string: 'id, name, email'}
    //       };
    //       const profileRequest = new GraphRequest(
    //         '/me',
    //         {parameters: PROFILE_REQUEST_PARAMS},
    //         (err, results) => {
    //           if (err) {
    //             console.log('login info has error: ' + err.message);
    //           } else {
    //             console.log('results:', results);
    //             loginOpenid({CredCode: 'GOOGLE', Email:results.email})
    //           }
    //         },
    //       );
    //       new GraphRequestManager().addRequest(profileRequest).start();
    //     }
    //   },
    //   (error) => {
    //     console.log('Login fail with error: ' + error)
    //   }
    // )
  };

  loginGoogle = () => {
    this.setState({
      alertShowed: true,
      alertMessage: 'Fitur ini sedang dalam tahap pengembangan.',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  detectFingerprintAvailable = () => {
    FingerprintScanner.isSensorAvailable().catch((error) =>
      this.setState({
        errorMessage: error.message,
        biometric: error.biometric,
        fingerDetected: false,
      }),
    );
  };

  handleErrorLoginFinger = (error) => {
    if (
      error ==
      'UserFallback: Authentication was canceled because the user tapped the fallback button (Enter Password).'
    ) {
    } else {
      this.setState({
        alertShowed: true,
        alertMessage:
          'Tunggu fingerPrint 30 detik atau masukkan username dan password',
        alertTitle: 'Informasi',
        alertType: 'info',
        alertDoneText: 'OK',
      });
    }
  };

  onAuthenticate = async () => {
    if (Platform.OS === 'android') {
      const {loginOpenid} = this.props;
      const {email} = this.state;
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
          loginOpenid({CredCode: 'FINGERPRINT', Email: email});
          this.setState({popupShowed: false});
        } else {
          this.showToastNoInternet();
        }
      });
    } else {
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
          Keychain.getGenericPassword({
            service: 'BafTouchIdService',
          })
            .then((result) => {
              if (!result) {
                this.setState({
                  alertShowed: true,
                  alertMessage: 'Tidak ada fingerprint terdaftar',
                  alertTitle: 'Informasi',
                  alertType: 'error',
                  alertDoneText: 'OK',
                });
              } else {
                console.log('Otentikasi Berhasil');
                this.setState({
                  alertFor: 'loginOpenId',
                  alertShowed: true,
                  alertMessage: 'Otentikasi Berhasil',
                  alertTitle: 'Informasi',
                  alertType: 'success',
                  alertDoneText: 'OK',
                  popupShowed: false,
                });
              }
            })
            .catch(async (error) => {
              if ((await Keychain.getSupportedBiometryType()) === null) {
                //after 5 failed attempts
                // biometrics authentication is disabled systemwide
                // Keychain.getSupportBiometryType() will return null
                console.log('Gagal setelah 5 upaya gagal');
                this.setState({
                  alertShowed: true,
                  alertMessage: 'Gagal setelah 5 upaya gagal',
                  alertTitle: 'Informasi',
                  alertType: 'error',
                  alertDoneText: 'OK',
                });
                return;
              }
              if (
                error.message ===
                'The user name or passphrase you entered is not correct.'
              ) {
                //invalid biometrics credentials after 3 attempts
                console.log('Gagal setelah 3 upaya gagal');
                this.setState({
                  alertShowed: true,
                  alertMessage: 'Gagal setelah 3 upaya gagal',
                  alertTitle: 'Informasi',
                  alertType: 'error',
                  alertDoneText: 'OK',
                });
              }
              if (error.message === 'User canceled the operation.') {
                //Authentication was canceled
                console.log('Autentikasi telah dibatalkan');
                this.setState({
                  alertShowed: true,
                  alertMessage: 'Autentikasi telah dibatalkan',
                  alertTitle: 'Informasi',
                  alertType: 'error',
                  alertDoneText: 'OK',
                });
              }
            });
        } else {
          this.showToastNoInternet();
        }
      });
    }
  };

  handleFingerprintDismissed = () => {
    this.setState({popupShowed: false});
  };

  handleFingerprintShowed = () => {
    if (Platform.OS === 'android') {
      if (!this.state.fingerDetected && this.state.errorMessage) {
        this.setState({
          alertShowed: true,
          alertMessage: 'Device Anda tidak support fitur ini.',
          alertTitle: 'Informasi',
          alertType: 'info',
          alertDoneText: 'OK',
        });
      }
      if (!this.state.fingerPrint) {
        this.setState({
          alertShowed: true,
          alertMessage: 'Aktifkan fitur fingerprint pada akun anda.',
          alertTitle: 'Informasi',
          alertType: 'info',
          alertDoneText: 'OK',
        });
      } else {
        this.setState({popupShowed: true});
      }
    } else {
      if (!this.state.fingerPrint) {
        this.setState({
          alertShowed: true,
          alertMessage: 'Aktifkan fitur fingerprint pada akun anda.',
          alertTitle: 'Informasi',
          alertType: 'info',
          alertDoneText: 'OK',
        });
      } else {
        const BiometryTypes = {
          TouchID: 'TouchID',
        };
        Keychain.getSupportedBiometryType().then((biometryType) => {
          switch (biometryType) {
            case BiometryTypes.TouchID:
              this.setState({popupShowed: true});
              break;
            default:
              this.setState({
                alertShowed: true,
                alertMessage: 'Touch ID tidak mendukung diperangkat anda',
                alertTitle: 'Informasi',
                alertType: 'error',
                alertDoneText: 'OK',
                fingerPrint: false,
              });
              break;
          }
        });
      }
    }
  };

  // getInfoFromToken = (token) => {
  //   const PROFILE_REQUEST_PARAMS = {
  //     fields: {
  //       string: 'id, name, email',
  //     },
  //   };
  //   const profileRequest = new GraphRequest(
  //     '/me',
  //     {token, parameters: PROFILE_REQUEST_PARAMS},
  //     (error, result) => {
  //       if (error) {
  //         console.log('login info has error: ' + error);
  //       } else {
  //         console.log('result:', result);
  //       }
  //     },
  //   );
  //   new GraphRequestManager().addRequest(profileRequest).start();
  // };

  render() {
    const {
      handleSubmit,
      submitting,
      loginLoading,
      loginOpenidLoading,
    } = this.props;

    const {popupShowed} = this.state;

    return (
      <Container style={{flex: 1}}>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid title="Masuk BAF Mobile" goBack={this.goBack} />
        ) : (
          <SubHeaderIos title="Masuk BAF Mobile" goBack={this.goBack} />
        )}

        <Spinner
          visible={loginLoading || loginOpenidLoading ? true : false}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />

        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />

        <Content disableKBDismissScroll={false}>
          <Image
            source={require('../../../../assets/img/bafwhiteNew.png')}
            style={styles.bafLogo}
          />
          <Text style={styles.textBelowLogo}>Masuk dengan</Text>

          <View style={styles.containerOptionalLogin}>
            {/* <View>
              <TouchableOpacity
                style={styles.btnFacebook}
                onPress={() => this.loginFacebook()}>
                <Image
                  source={require('../../../assets/img/facebook.png')}
                  style={styles.authLogo}></Image>
                <Text
                  style={{color: 'white', fontSize: 14, fontFamily: fonts.primary.normal}}>
                  Facebook
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.btnGoogle}
                onPress={() => this.loginGoogle()}>
                <Image
                  source={require('../../../assets/img/google.png')}
                  style={styles.authLogo}></Image>
                <Text
                  style={{color: 'black', fontSize: 14, fontFamily: fonts.primary.normal}}>
                  Google
                </Text>
              </TouchableOpacity>
            </View> */}
            <View>
              <TouchableOpacity
                style={styles.fingerprint}
                onPress={this.handleFingerprintShowed}>
                <Image
                  testID="FingerprintImage"
                  source={require('../../../../assets/img/fingerprint.png')}
                  style={styles.fingerLogo}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              {/* <FingerPrint
                onAuthenticate={this.onAuthenticate}
                fingerPrintExist={this.state.fingerPrint}
              /> */}
            </View>
          </View>

          <View style={styles.containerDivider}>
            <Left>
              <View style={styles.listDivider}></View>
            </Left>
            <Body style={{maxWidth: 70}}>
              <Text style={styles.textAboveForm}>Atau</Text>
            </Body>
            <Right>
              <View style={styles.listDivider}></View>
            </Right>
          </View>

          <Form style={styles.containerForm}>
            <Field
              name="phoneNo"
              testId="phoneNo"
              type="text"
              component={renderField}
              placeholder="Nomor Handphone"
              icon="phone"
              iconright={false}
              maxLength={15}
              keyboardType="number-pad"
              editable={true}
            />

            <Field
              name="password"
              testId="password"
              type="text"
              component={renderField}
              placeholder="Password"
              icon="lock"
              iconright={this.state.icon}
              secureTextEntry={this.state.hide}
              onPressIcon={() => this.showHidePassword()}
              editable={true}
            />

            <Field name="rememberMe" component={renderFieldHidden} />

            <View style={styles.containerCheckBox}>
              <ListItem style={styles.listItemCheckBox}>
                <CheckBox
                  checked={this.state.checked}
                  style={{borderColor: '#666'}}
                  onPress={() => this.actionCheck()}
                />
                <Text
                  style={{
                    fontSize: 12,
                    color: '#666',
                    fontFamily: fonts.primary.normal,
                  }}>
                  {'  '}
                  Selalu ingat saya
                </Text>
              </ListItem>
              <TouchableOpacity onPress={() => this.goForgotPassword()}>
                <Text
                  style={{
                    fontSize: 12,
                    color: '#666',
                    fontFamily: fonts.primary.normal,
                    marginTop: 5,
                  }}>
                  Lupa Password?
                </Text>
              </TouchableOpacity>
            </View>

            <TouchableOpacity
              style={styles.btnLogin}
              onPress={handleSubmit(this.goLogin)}
              disabled={submitting}>
              <Text
                style={{
                  color: 'white',
                  fontSize: 14,
                  fontFamily: fonts.primary.normal,
                  fontWeight: 'bold',
                }}>
                Masuk
              </Text>
            </TouchableOpacity>

            <View style={styles.containerTextBottom}>
              <Text
                style={{
                  fontSize: 12,
                  fontFamily: fonts.primary.normal,
                  color: '#002F5F',
                  marginTop: 10,
                }}>
                Belum daftar di BAF Mobile ?{' '}
              </Text>
              <TouchableOpacity onPress={() => this.goRegister()}>
                <Text
                  style={{
                    fontSize: 12,
                    color: '#002F5F',
                    fontFamily: fonts.primary.normal,
                    fontWeight: 'bold',
                    marginTop: 10,
                  }}>
                  Daftar Disini
                </Text>
              </TouchableOpacity>
            </View>
          </Form>
          {this.showAlert()}

          {/* <LoginButton style={styles.btnFacebook}
            onLoginFinished={(error, result) => {
              if (error) {
                console.log('login has error: ' + result.error);
              } else if (result.isCancelled) {
                console.log('login is cancelled.');
              } else {
                AccessToken.getCurrentAccessToken().then((data) => {
                  const accessToken = data.accessToken.toString();
                  this.getInfoFromToken(accessToken);
                });
              }
            }}
            onLogoutFinished={() => this.setState({userInfo: {}})}
          /> */}
        </Content>
        {popupShowed && (
          <FingerprintPopup
            style={styles.popup}
            handlePopupDismissed={this.handleFingerprintDismissed}
            onAuthenticate={this.onAuthenticate}
            handleErrorFinger={this.handleErrorLoginFinger}
          />
        )}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    loginLoading: state.login.loading,
    loginResult: state.login.result,
    loginError: state.login.error,
    loginOpenidLoading: state.loginOpenid.loading,
    loginOpenidResult: state.loginOpenid.result,
    loginOpenidError: state.loginOpenid.error,
    saveTokenLoading: state.saveToken.loading,
    saveTokenResult: state.saveToken.result,
    saveTokenError: state.saveToken.error,
    refreshTokenResult: state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
    formLogin: state.form.formLogin,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      login,
      loginOpenid,
      saveToken,
      refreshToken,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

Login = reduxForm({
  form: 'formLogin',
  enableReinitialize: true,
  validate: FormLoginValidate,
})(Login);

export default connect(mapStateToProps, matchDispatchToProps)(Login);
