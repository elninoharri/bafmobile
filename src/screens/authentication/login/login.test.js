import Login from '../login';
import React from 'react';
import {Provider} from 'react-redux';
import {store} from '../../../store/store';
import {fireEvent, render} from 'react-native-testing-library';
import {
  createStackNavigator,
} from '@react-navigation/stack';
import Register from '../register';
import ForgotPassword from '..//forgotPassword';
import {NavigationContainer} from '@react-navigation/native';


const Stack = createStackNavigator();

const component = (
  <Provider store={store}>
    <NavigationContainer>
      <Stack.Navigator headerMode="none" initialRouteName="Login">
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
      </Stack.Navigator>
    </NavigationContainer>
  </Provider>
);

test('given empty nomor handphone, user see error "Nomor Handphone harus diisi"', () => {
  const {getByPlaceholder, getByText, getAllByText} = render(component);

  const input = getByPlaceholder('Password');

  fireEvent(input, 'onChange', '123456789');
  fireEvent.press(getByText('Masuk'));

  const errorElements = getAllByText('Nomor Handphone harus diisi');
  expect(errorElements).toHaveLength(1);
});

test('given empty nomor password, user see error "Password harus diisi"', () => {
  const {getByPlaceholder, getByText, getAllByText} = render(component);

  const input = getByPlaceholder('Nomor Handphone');

  fireEvent(input, 'onChange', '08128885659');
  fireEvent.press(getByText('Masuk'));

  const errorElements = getAllByText('Password harus diisi');
  expect(errorElements).toHaveLength(1);
});

test('clicking on Lupa Password? takes you to the forgotPassword screen', async () => {
  const {findByText} = render(component);
  const toClick = await findByText('Lupa Password?');

  fireEvent(toClick, 'press');
  const newBody = await findByText('Silahkan masukkan No HP Anda');

  expect(newBody).toBeTruthy();
});

test('clicking on Daftar Disini, takes you to the Register screen', async () => {
  const {findByText} = render(component);
  const toClick = await findByText('Daftar Disini');

  fireEvent(toClick, 'press');
  const newBody = await findByText('Yuk Registrasi BAF Mobile !');

  expect(newBody).toBeTruthy();
});

test('button forgot password check', async () => {

  const Stack = createStackNavigator();

  const component = (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator headerMode="none" initialRouteName="Login">
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
  const {findByText, getByPlaceholder} = render(component);

  const toClick = await findByText('Lupa Password?');

    fireEvent(toClick, 'press');
    const newHeader = await findByText('Lupa Password');


    expect(newHeader).toBeTruthy();
});

test('button registration check', async () => {

  const Stack = createStackNavigator();

  const component = (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator headerMode="none" initialRouteName="Login">
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
  const {findByText, getByPlaceholder} = render(component);

  const toClick = await findByText('Daftar Disini');

    fireEvent(toClick, 'press');
    const newHeader = await findByText('Registrasi');


    expect(newHeader).toBeTruthy();
});

test('button fingerprint check', async () => {

  const Stack = createStackNavigator();

  const component = (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator headerMode="none" initialRouteName="Login">
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
  const {findByText, getByPlaceholder, findByTestId} = render(component);

  const toClick = await findByTestId('FingerprintImage');

    fireEvent(toClick, 'press');
    const newHeader = await findByText('Informasi');


    expect(newHeader).toBeTruthy();
});

test('Success Login check', async() => {

  const Stack = createStackNavigator();

  const component = (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator headerMode="none" initialRouteName="Login">
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
  const {findByText,getByTestId} = render(component);

  const inputNoHp = await getByTestId('phoneNo');
  const inputPassword = await getByTestId('password');
  await fireEvent(inputNoHp, 'onChange', '022202220222');
  await fireEvent(inputPassword, 'onChange', 'Bussan100');

  const toClick = await findByText('Masuk');
    fireEvent(toClick, 'press');

  const errorElements = findByText('Anda berhasil masuk');
  expect(errorElements).toBeTruthy();
});

test('Failed Login check', async() => {
  const Stack = createStackNavigator();
  const component = (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator headerMode="none" initialRouteName="Login">
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
  const {findByText,getByTestId} = render(component);

  const inputNoHp = await getByTestId('phoneNo');
  const inputPassword = await getByTestId('password');
  await fireEvent(inputNoHp, 'onChange', '022202220223');
  await fireEvent(inputPassword, 'onChange', 'Bussan000');

  const toClick = await findByText('Masuk');
    fireEvent(toClick, 'press');

  const errorElements = findByText('Login gagal');
  expect(errorElements).toBeTruthy();
});

test('Success Navigate to Account Screen after login', async() => {
  const Stack = createStackNavigator();
  const component = (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator headerMode="none" initialRouteName="Login">
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
  const {findByText,getByTestId} = render(component);

  const inputNoHp = await getByTestId('phoneNo');
  const inputPassword = await getByTestId('password');
  await fireEvent(inputNoHp, 'onChange', '022202220222');
  await fireEvent(inputPassword, 'onChange', 'Bussan100');

    const pressMasuk = await findByText('Masuk');
    fireEvent(pressMasuk, 'press');

    const pressOK = await getByTestId('OK');
    fireEvent(pressOK, 'press');

  const errorElements = findByText('Riwayat Kontrak');
  expect(errorElements).toBeTruthy();
});