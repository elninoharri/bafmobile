import {StyleSheet, Dimensions, Platform} from 'react-native';
import {fonts} from '../../../utils/fonts';
const {width: WIDTH} = Dimensions.get('window');

export const styles = StyleSheet.create({
  head: {
    marginLeft: 32,
    marginRight: 31,
    marginTop: 31,
    marginBottom: 10,
  },
  desc: {
    marginTop: 15,
    fontSize: 14,
    fontFamily: fonts.primary.normal,
    lineHeight: 15,
    textAlign: 'justify',
  },
  title: {
    fontSize: 24,
    fontFamily: fonts.primary.normal,
    color: 'black',
    marginTop: 10,
  },
  headContent: {
    fontSize: 14,
    lineHeight: 25,
    textAlign: 'justify',
    fontFamily: fonts.primary.normal,
  },
  textContent: {
    width: WIDTH * 0.71,
    fontFamily: fonts.primary.normal,
  },
  rowRender: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    marginVertical: 2,
  },
  bullet: {
    fontSize: 20,
    fontFamily: fonts.primary.normal,
    color: 'rgba(0, 0, 0, 0.8)',
  },
  toastConnection: {
    marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
    marginLeft: '3%',
    fontSize: 16,
    color: 'red',
    fontFamily: fonts.primary.normal,
  },
});
