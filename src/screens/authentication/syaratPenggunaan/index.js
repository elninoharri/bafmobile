import React, {Component} from 'react';
import SubHeaderAndroid from '../../../components/android/subHeaderBlue';
import SubHeaderIos from '../../../components/ios/subHeaderBlue';
import {Container, Icon, Content} from 'native-base';
import {
  Text,
  View,
  Platform,
  Dimensions,
  PixelRatio,
  FlatList,
  BackHandler,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import data from './syarat.json';
import Toast from 'react-native-easy-toast';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {styles} from './style';
import analytics from '@react-native-firebase/analytics';

class SyaratPenggunaan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Persyaratan dan Ketentuan',
      isConnected: false,
    };
  }

  componentDidMount = async () => {
    analytics().logScreenView({
      screen_class: 'screenSyaratPenggunaan',
      screen_name: 'screenSyaratPenggunaan',
    });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this.checkInternet();
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected});
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text style={styles.toastConnection}>Tidak Ada Koneksi Internet</Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  toastComponent = () => {
    return (
      <Toast
        ref="toast"
        style={{
          marginTop:
            Platform.OS != 'android'
              ? Dimensions.get('window').width * PixelRatio.get() > 750
                ? 44
                : 20
              : 0,
          backgroundColor: '#FFE6E9',
          width: '100%',
          borderBottomWidth: 2,
          borderBottomColor: 'red',
          justifyContent: 'center',
          paddingLeft: 50,
          height: 50,
          borderRadius: 0,
        }}
        position="top"
        positionValue={0}
        fadeInDuration={2000}
        fadeOutDuration={1000}
        opacity={0.9}
      />
    );
  };

  _renderItem = ({item}) => {
    return item.content.map((res) => {
      if (res.length !== 0) {
        return (
          <View style={{marginLeft: 60}}>
            <View style={styles.rowRender}>
              <View style={{width: 20}}>
                <Text style={styles.bullet}>{'\u2022' + ''}</Text>
              </View>
              <View>
                <Text style={styles.textContent}>{res}</Text>
              </View>
            </View>
          </View>
        );
      }
    });
  };

  render() {
    const head = data.head.map((result) => {
      if (result.lenght !== 0) {
        return <Text style={styles.headContent}>{result.desc}</Text>;
      }
    });

    return (
      <Container>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}

        <Content>
          {this.toastComponent()}
          <View style={styles.head}>
            {head}
            <Text style={styles.title}>Syarat Pengunaan</Text>
            <Text style={styles.desc}>
              Ketersediaan e-voucher sesuai dengan promosi. Berikut ini adalah
              persyaratan dan ketentuan umum dari e-voucher :
            </Text>
          </View>
          <FlatList data={data.paragraf} renderItem={this._renderItem} />
        </Content>
      </Container>
    );
  }
}

function mapStateToProps() {
  return {};
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(SyaratPenggunaan);
