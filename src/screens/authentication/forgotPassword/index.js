/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  TouchableOpacity,
  Text,
  Alert,
  View,
  Image,
  BackHandler,
  Platform,
  Keyboard,
  Dimensions,
  PixelRatio,
} from 'react-native';
import {
  Container,
  Form,
  Label,
  Input,
  Item,
  Row,
  Icon,
  Content,
} from 'native-base';
import {styles} from './style';
import {reduxForm, Field, change, reset} from 'redux-form';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import SubHeaderAndroid from '../../../components/android/subHeader';
import SubHeaderIos from '../../../components/ios/subHeader';
import NetInfo from '@react-native-community/netinfo';
import {forgotPassword} from '../../../actions';
import {FormForgotValidate} from '../../../validates/FormForgotValidate';
import Toast, {DURATION} from 'react-native-easy-toast';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import analytics from '@react-native-firebase/analytics';
import Spinner from 'react-native-loading-spinner-overlay';

export const renderField = ({
  input,
  type,
  label,
  icon,
  iconright,
  placeholder,
  editable,
  keyboardType,
  maxLength,
  secureTextEntry,
  onPressIcon,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={styles.input}>
      <Item fixedLabel style={{borderColor: hasError ? 'red' : '#666'}}>
        <Icon
          active
          type="FontAwesome"
          name={icon}
          style={{color: hasError ? 'red' : '#666'}}
        />
        <Input
          {...input}
          type={type}
          editable={editable}
          placeholder={placeholder}
          keyboardType={keyboardType}
          maxLength={maxLength}
          secureTextEntry={secureTextEntry}
          style={styles.inputText}
        />
        {iconright ? (
          <Icon
            active
            name={iconright}
            style={{color: '#666'}}
            onPress={onPressIcon}
          />
        ) : null}
      </Item>
      {hasError ? <Text style={styles.errorDesc}>{error}</Text> : null}
    </View>
  );
};

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Lupa Password',
      Email: '',
      disableButton: false,
      isConnected: false,

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
    };
  }

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.props.navigation.navigate('Login');
      return true;
    }
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      if (!state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  componentDidMount = async () => {
    analytics().logScreenView({
      screen_class: 'screenForgotPassword',
      screen_name: 'screenForgotPassword',
    });

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this.checkInternet();
  };

  componentWillUnmount = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  };

  componentDidUpdate(prevProps, prevState) {
    const {forgotPasswordError, forgotPasswordResult} = this.props;
    if (
      forgotPasswordError &&
      prevProps.forgotPasswordError !== forgotPasswordError
    ) {
      this.setState({
        alertShowed: true,
        alertMessage: forgotPasswordError.message,
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
      });
    }

    if (
      forgotPasswordResult &&
      prevProps.forgotPasswordResult !== forgotPasswordResult
    ) {
      this.setState({
        alertFor: 'forgotPasswordResult',
        alertShowed: true,
        alertMessage:
          'Email lupa password akan dikirim ke ' + forgotPasswordResult.Email,
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
      });
    }
  }

  goBack = async () => {
    this.props.navigation.goBack();
    this.props.resetForm('formForgotPassword');
  };

  sendEmail = (data) => {
    const {forgotPassword} = this.props;

    Keyboard.dismiss();

    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        analytics().logEvent('eventForgotPassword', {
          item: 'user trying to forgot password',
        });
        forgotPassword({Phoneno: data.phoneNo});
      } else {
        this.showToastNoInternet();
      }
    });
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  handlePositiveButtonAlert = () => {
    this.setState({alertShowed: false});
    if (this.state.alertFor === 'forgotPasswordResult') {
      this.goBack();
    }
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false});
  };

  render() {
    const {handleSubmit, submitting, forgotPasswordLoading} = this.props;
    return (
      <Container>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}

        <Spinner
          visible={forgotPasswordLoading ? true : false}
          textContent={'Loading...'}
          textStyle={{ color: '#FFF' }}
        />

        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />
        <Content disableKBDismissScroll={false}>
          <Image
            style={styles.image}
            testID="forgotPasswordImage"
            source={require('../../../../assets/img/forgotPassword.png')}
          />
          <Text style={styles.title}>Silahkan masukkan No HP Anda</Text>
          <Text style={styles.text2}>
            Kami akan mengirimkan tautan ke email Anda, silahkan periksa email
            anda untuk memperbaharui password Anda.
          </Text>

          <Form style={{paddingHorizontal: '10%'}}>
            <Field
              name="phoneNo"
              type="text"
              component={renderField}
              placeholder="Nomor Handphone"
              icon="phone"
              iconright={false}
              maxLength={15}
              keyboardType="number-pad"
              editable={true}
            />

            {this.state.disableButton ? (
              <TouchableOpacity
                style={styles.btnDelay}
                disabled={this.state.disableButton}>
                <Text style={styles.text}>Kirim</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={styles.btnKirim}
                onPress={handleSubmit(this.sendEmail)}
                disabled={submitting}>
                <Text style={styles.text}>Kirim</Text>
              </TouchableOpacity>
            )}
          </Form>

          <Row style={{alignSelf: 'center', marginBottom: '10%'}}>
            <Text style={styles.text3}>Belum daftar di BAF Mobile ? </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Register')}>
              <Text style={styles.text4}>Daftar Disini</Text>
            </TouchableOpacity>
          </Row>

          {this.showAlert()}
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    forgotPasswordLoading: state.forgotPassword.loading,
    forgotPasswordResult: state.forgotPassword.result,
    forgotPasswordError: state.forgotPassword.error,
    formForgotPassword: state.form.formForgotPassword,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      forgotPassword,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

ForgotPassword = reduxForm({
  form: 'formForgotPassword',
  enableReinitialize: true,
  validate: FormForgotValidate,
})(ForgotPassword);

export default connect(mapStateToProps, matchDispatchToProps)(ForgotPassword);
