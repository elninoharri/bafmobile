import ForgotPassword from '../forgotPassword';
import Register from '../../register';
import React from 'react';
import Login from '../../login';
import {Provider} from 'react-redux';
import {store} from '../../../store';
import {fireEvent, render} from 'react-native-testing-library';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import renderer from 'react-test-renderer';

const Stack = createStackNavigator();

const component = (
  <Provider store={store}>
    <NavigationContainer>
      <Stack.Navigator headerMode="none" initialRouteName="ForgotPassword">
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        <Stack.Screen name="Login" component={Login} />
      </Stack.Navigator>
    </NavigationContainer>
  </Provider>
);

test('forgot password renders correctly', () => {
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});

test('every component rendered', () => {
  const {getByPlaceholder, getByTestId, getByText} = render(component);
  const Title = getByText('Lupa Password');
  const Body = getByText('Silahkan masukkan No HP Anda');
  const Body2 = getByText(
    'Kami akan mengirimkan tautan ke email Anda, silahkan periksa email anda untuk memperbaharui password Anda.',
  );
  const Body3 = getByText('Daftar Disini');
  const Button = getByText('Kirim');
  const Form = getByPlaceholder('Nomor Handphone');
  const Image = getByTestId('forgotPasswordImage');

  expect(Image).toBeTruthy();
  expect(Body).toBeTruthy();
  expect(Body2).toBeTruthy();
  expect(Body3).toBeTruthy();
  expect(Form).toBeTruthy();
  expect(Title).toBeTruthy();
  expect(Button).toBeTruthy();
});

test('given empty nomor handphone, user see error "Nomor Handphone harus diisi"', () => {
  const {getByText, getAllByText} = render(component);

  fireEvent.press(getByText('Kirim'));

  const errorElements = getAllByText('Nomor Handphone harus diisi');
  expect(errorElements).toHaveLength(1);
});

test('given nomor handphone less than 9, user see error "No Handphone 1 diisi 9 - 15 digit"', () => {
  const {getByPlaceholder, getByText, getAllByText} = render(component);
  const input = getByPlaceholder('Nomor Handphone');
  fireEvent(input, 'onChange', '123');

  fireEvent.press(getByText('Kirim'));

  const errorElements = getAllByText('No Handphone 1 diisi 9 - 15 digit');
  expect(errorElements).toHaveLength(1);
});

test('clicking on "Daftar Disini", takes you to the Register screen', async () => {
  const {findByText} = render(component);
  const toClick = await findByText('Daftar Disini');

  fireEvent(toClick, 'press');
  const newBody = await findByText('Yuk Registrasi BAF Mobile !');

  expect(newBody).toBeTruthy();
});
