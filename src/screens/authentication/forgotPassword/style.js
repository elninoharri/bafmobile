import {StyleSheet, Dimensions} from 'react-native';
import {fonts} from '../../../utils/fonts';

const {width: WIDTH} = Dimensions.get('window');

export const styles = StyleSheet.create({
  image: {
    width:'100%',
    height: 320,
    alignSelf: 'center',
    resizeMode: 'contain'
  },
  input: {
    marginBottom:10, 
    left:-15, 
    width:'105%'
  },
  inputText: {
    fontSize: 14,
    fontFamily: fonts.primary.normal,
  },
  btnKirim: {
    alignSelf: 'center',
    width: '100%',
    height: 40,
    borderRadius: 4,
    backgroundColor: '#002f5f',
    justifyContent: 'center',
    marginTop: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  btnDelay: {
    alignSelf: 'center',
    width: '100%',
    height: 40,
    borderRadius: 4,
    backgroundColor: '#b2bec3',
    justifyContent: 'center',
    marginTop: 30,
  },
  text: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
    fontFamily: fonts.primary.normal,
  },
  text2: {
    fontSize: 14,
    color: '#5B5B5B',
    textAlign: 'center',
    marginVertical: 10,
    marginHorizontal: '10%',
    fontFamily: fonts.primary.normal,
  },
  text3: {
    marginTop: 10,
    fontSize: 12,
    color: '#002F5F',
    fontFamily: fonts.primary.normal,
  },
  text4: {
    marginTop: 10,
    fontSize: 12,
    color: '#002F5F',
    fontWeight: 'bold',
    fontFamily: fonts.primary.normal,
  },
  errorDesc : { 
    color:'red',
    alignSelf:'stretch',
    textAlign:'right',
    fontSize:10,
    marginBottom:-12,
    fontFamily: fonts.primary.normal,
  },
  title: {
    fontSize: 19,
    fontWeight: '700',
    color: '#002F5F',
    textAlign: 'center',
    marginTop: '-10%',
    fontFamily: fonts.primary.normal,
  },
});
