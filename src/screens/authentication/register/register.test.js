import Register from './index';
import React from 'react';
import {Provider} from 'react-redux';
import {store} from '../../../store/store';
import {fireEvent, render} from 'react-native-testing-library';
import {
  createStackNavigator,
} from '@react-navigation/stack';
import Login from '../login';
import ForgotPassword from '../forgotPassword';
import TermCondition from '../termCondition';
import SyaratPenggunaan from '../syaratPenggunaan';
import {NavigationContainer} from '@react-navigation/native';


const Stack = createStackNavigator();

const component = (
  <Provider store={store}>
    <NavigationContainer>
      <Stack.Navigator headerMode="none" initialRouteName="Register">
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        <Stack.Screen name="TermCondition" component={TermCondition} />
        <Stack.Screen name="SyaratPenggunaan" component={SyaratPenggunaan} />
      </Stack.Navigator>
    </NavigationContainer>
  </Provider>
);

test('Check => Field "Nama Lengkap" empty', () => {
    const {getByTestId, getAllByText} = render(component);
    fireEvent.press(getByTestId('Daftar'));
    const errorElements = getAllByText('Nama harus diisi');
    expect(errorElements).toHaveLength(1);
});
test('Check => Field "Nomor Handphone" empty', () => {
    const {getByTestId, getAllByText} = render(component);
    fireEvent.press(getByTestId('Daftar'));
    const errorElements = getAllByText('Nomor Handphone harus diisi');
    expect(errorElements).toHaveLength(1);
});
test('Check => Field "Email" empty', () => {
    const {getByTestId, getAllByText} = render(component);
    fireEvent.press(getByTestId('Daftar'));
    const errorElements = getAllByText('Email minimal 9 digit (ada @ dan .)');
    expect(errorElements).toHaveLength(1);
});
test('Check => Field "Password" empty', () => {
    const {getByTestId, getAllByText} = render(component);
    fireEvent.press(getByTestId('Daftar'));
    const errorElements = getAllByText('Password harus diisi');
    expect(errorElements).toHaveLength(1);
});
test('Check => Field "Tempat Lahir" empty', () => {
    const {getByTestId, getAllByText} = render(component);
    fireEvent.press(getByTestId('Daftar'));
    const errorElements = getAllByText('Tempat harus diisi');
    expect(errorElements).toHaveLength(1);
});
test('Check => Field "Tanggal Lahir" empty', () => {
    const {getByTestId, getAllByText} = render(component);
    fireEvent.press(getByTestId('Daftar'));
    const errorElements = getAllByText('Tanggal Lahir harus diisi');
    expect(errorElements).toHaveLength(1);
});
test('Check => Button "Kebijakan Privasi" navigation', () => {
    const {getByTestId, getAllByText} = render(component);
    fireEvent.press(getByTestId('TermCondition'));
    const headerText = getAllByText('Kebijakan Privasi');
    expect(headerText).toHaveLength(1);
});
test('Check => Button "Syarat Penggunaan" navigation', () => {
    const {getByTestId, getAllByText} = render(component);
    fireEvent.press(getByTestId('SyaratPenggunaan'));
    const headerText = getAllByText('Persyaratan dan Ketentuan');
    expect(headerText).toHaveLength(1);
});
test('Check => Open Modal DatePicker', () => {
  const {getAllByText,getByTestId,getByPlaceholder} = render(component);
  fireEvent.press(getByTestId('OpenDatePicker'));
  fireEvent.press(getByTestId('closeDatePicker'));
  fireEvent(getByPlaceholder('Nama Lengkap Anda'), 'onChange', 'Unit Test 001');
  fireEvent(getByPlaceholder('Nomor Handphone'), 'onChange', '085156510534');
  fireEvent(getByPlaceholder('Email'), 'onChange', 'hasbi22109@gmail.com');
  fireEvent(getByPlaceholder('Password'), 'onChange', 'Bussan100');
  fireEvent(getByPlaceholder('Tempat'), 'onChange', 'Tangerang');
  const headerText = getAllByText('');
  expect(headerText).toHaveLength(1);
});
