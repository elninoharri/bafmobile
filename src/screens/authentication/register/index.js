import React, { Component } from 'react';
import {
  View,
  Text,
  Platform,
  BackHandler,
  Image,
  TouchableOpacity,
  Keyboard,
  Dimensions,
  PixelRatio,
} from 'react-native';
import {
  Container,
  Icon,
  Form,
  Input,
  Item,
  Right,
  Left,
  Body,
  Row,
  DatePicker,
  Content,
} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import SubHeaderAndroid from '../../../components/android/subHeader';
import SubHeaderIos from '../../../components/ios/subHeader';
import { styles } from './style';
import { Field, reduxForm, change, reset } from 'redux-form';
import moment from 'moment';
import Toast from 'react-native-easy-toast';
import { checkPhoneNumber } from '../../../actions/authentication/register';
import { FormRegisterValidate } from '../../../validates/FormRegisterValidate';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Spinner from 'react-native-loading-spinner-overlay';
import Modal from 'react-native-modal';
import DatePickerIos from 'react-native-date-picker';
import { fonts } from '../../../utils/fonts';
import analytics from '@react-native-firebase/analytics';

export const renderField = ({
  input,
  type,
  icon,
  iconright,
  placeholder,
  editable,
  styleType,
  keyboardType,
  maxLength,
  secureTextEntry,
  onPressIcon,
  meta: { touched, error },
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={styleType}>
      <Item
        fixedLabel
        style={{ borderColor: hasError ? 'red' : '#666', height: 40 }}>
        <Icon
          active
          type="FontAwesome"
          name={icon}
          style={{ color: hasError ? 'red' : '#666' }}
        />
        <Input
          {...input}
          type={type}
          editable={editable}
          placeholder={placeholder}
          keyboardType={keyboardType}
          maxLength={maxLength}
          secureTextEntry={secureTextEntry}
          style={styles.input}
        />
        {iconright ? (
          <Icon
            active
            name={iconright}
            style={{ color: '#666' }}
            onPress={onPressIcon}
          />
        ) : null}
      </Item>
      {hasError ? <Text style={styles.errorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderField2 = ({
  input,
  type,
  icon,
  iconright,
  placeholder,
  styleType,
  keyboardType,
  maxLength,
  secureTextEntry,
  onPressIcon,
  openModal,
  closeModal,
  visible,
  meta: { touched, error },
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={styleType}>
      {Platform.OS == 'android' ? (
        <Item
          onPress={() => (show = true)}
          fixedLabel
          style={{ borderColor: hasError ? 'red' : '#666', marginBottom: 0 }}>
          <TouchableOpacity
            onPress={Platform.OS == 'android' ? null : openModal}>
            <Icon
              active
              type="FontAwesome"
              name={icon}
              style={{ color: hasError ? 'red' : '#666' }}
            />
          </TouchableOpacity>

          <DatePicker
            {...input}
            defaultDate={new Date()}
            minimumDate={new Date(1940, 1, 1)}
            maximumDate={new Date()}
            locale={'id'}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={'fade'}
            androidMode={'default'}
            placeHolderText={placeholder}
            textStyle={styles.textDate}
            placeHolderTextStyle={styles.placeholderDate}
            onDateChange={(newDate) =>
              input.onChange(moment(newDate).format('YYYY-MM-DD'))
            }
            disabled={Platform.OS == 'android' ? false : true}
          />

          {iconright ? (
            <Icon
              active
              name={iconright}
              style={{ color: '#666' }}
              onPress={onPressIcon}
            />
          ) : null}
        </Item>
      ) : (
          <Item
            fixedLabel
            style={{ borderColor: hasError ? 'red' : '#666', height: 40 }}>
            <Modal testID="modalDatePicker" isVisible={visible}>
              <View
                style={{
                  width: '95%',
                  backgroundColor: 'white',
                  borderRadius: 5,
                  alignItems: 'center',
                  alignSelf: 'center',
                }}>
                <TouchableOpacity
                  testID="closeDatePicker"
                  onPress={closeModal}
                  style={{ width: '100%' }}>
                  <Icon
                    active
                    type="FontAwesome"
                    name={'times'}
                    style={{
                      color: '#666',
                      fontSize: 24,
                      position: 'absolute',
                      right: 0,
                      top: 3,
                    }}
                  />
                </TouchableOpacity>
                <DatePickerIos
                  {...input}
                  style={{ marginVertical: 20 }}
                  testID="dateTimePicker"
                  value={new Date()}
                  mode={'date'}
                  is24Hour={true}
                  display="default"
                  onDateChange={(newDate) =>
                    input.onChange(moment(newDate).format('YYYY-MM-DD'))
                  }
                />
              </View>
            </Modal>
            <TouchableOpacity
              onPress={() => openModal()}
              testID="OpenDatePicker"
              style={{ flexDirection: 'row' }}>
              <Icon
                active
                type="FontAwesome"
                name={icon}
                style={{
                  color: hasError ? 'red' : '#666',
                  marginTop: '8%',
                  marginLeft: '5%',
                }}
              />

              <Input
                {...input}
                type={type}
                disabled={true}
                placeholder={placeholder}
                keyboardType={keyboardType}
                maxLength={maxLength}
                secureTextEntry={secureTextEntry}
                style={{ fontSize: 14, fontFamily: fonts.primary.normal }}
              />

              {iconright ? (
                <Icon
                  active
                  name={iconright}
                  style={{ color: '#666' }}
                  onPress={onPressIcon}
                />
              ) : null}
            </TouchableOpacity>
          </Item>
        )}

      {hasError ? <Text style={styles.errorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderFieldHidden = ({ input, type, meta: { touched, error } }) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{ width: '100%' }}>
      <Item style={{ display: 'none' }}>
        <Input {...input} ype={type} />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      icon: 'eye',
      hide: true,
      isConnected: false,
      show: false,
      userRegister: false,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      visible: false,
      BirthDate: new Date(),
    };
  }

  componentDidMount = async () => {
    analytics().logScreenView({
      screen_class: 'screenRegister',
      screen_name: 'screenRegister',
    });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentDidUpdate = async (prevProps) => {
    const { checkPhoneNumberResult, checkPhoneNumberError } = this.props;

    if (
      checkPhoneNumberError !== null &&
      prevProps.checkPhoneNumberError !== checkPhoneNumberError
    ) {
      console.log("CheckPhone Error")
      await AsyncStorage.removeItem('userRegister');
      this.setState({
        alertShowed: true,
        alertMessage: checkPhoneNumberError.message,
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
      });
    }

    if (
      checkPhoneNumberResult &&
      prevProps.checkPhoneNumberResult !== checkPhoneNumberResult
    ) {
      if (
        checkPhoneNumberResult.StatusCode === '200' ||
        checkPhoneNumberResult.StatusCode === 200
      ) {
        this.setState({ show: true });
      } else {
        this.setState({
          alertShowed: true,
          alertMessage: 'Nomor Handphone sudah pernah digunakan',
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
    }
  };

  handleBackButton = async () => {
    if (this.props.navigation.isFocused()) {
      if (this.state.show) {
        this.setState({ show: false });
      } else {
        this.props.navigation.goBack();
        this.props.resetForm('formRegister');
        await AsyncStorage.removeItem('userRegister');
        return true;
      }
    }
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{ flexDirection: 'row' }}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{ color: 'red', marginLeft: '-10%' }}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            fontFamily: fonts.primary.normal,
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  handleNegativeButtonAlert = () => {
    this.setState({ alertShowed: false });
  };

  handlePositiveButtonAlert = () => {
    this.setState({ alertShowed: false });
  };

  registerFacebook = () => {
    this.setState({
      alertShowed: true,
      alertMessage: 'Fitur ini sedang dalam tahap pengembangan.',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  registerGoogle = () => {
    this.setState({
      alertShowed: true,
      alertMessage: 'Fitur ini sedang dalam tahap pengembangan.',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  showHidePassword() {
    this.setState((prevState) => ({
      icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
      hide: !prevState.hide,
    }));
  }

  goBack = async () => {
    if (this.state.show) {
      this.setState({ show: false });
    } else {
      this.props.navigation.goBack();
      this.props.resetForm('formRegister');
      await AsyncStorage.removeItem('userRegister');
    }
  };

  goPrivacy = () => {
    this.props.navigation.navigate('TermCondition');
  };

  goSyaratPengunaan = () => {
    this.props.navigation.navigate('SyaratPenggunaan');
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  toggleModal = () => {
    this.setState({ visible: false });
  };

  openDatePicker = () => {
    this.props.updateField('formRegister', 'BirthDate', '2020-08-05');
    this.setState({ visible: true });
  };

  daftarHandler = async (data) => {
    const { checkPhoneNumber } = this.props;
    await AsyncStorage.removeItem('userRegister');
    await AsyncStorage.setItem('userRegister', JSON.stringify(data));
    Keyboard.dismiss();

    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        analytics().logEvent('eventRegisterOtp', {
          item: 'user trying to check phone number with otp',
        });
        //request nya di ganti dengan GET
        checkPhoneNumber({Phoneno: data.phoneNo});
      } else {
        this.showToastNoInternet();
      }
    });
  };

  onChangeDate = (newDate) => {
    this.setState({ BirthDate: newDate });
  };

  render() {
    const { handleSubmit, submitting, checkPhoneNumberLoading } = this.props;
    const { show } = this.state;

    return (
      <Container style={{ flex: 1 }}>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid title="Registrasi" goBack={this.goBack} />
        ) : (
            <SubHeaderIos title="Registrasi" goBack={this.goBack} />
          )}
        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />

        <Spinner
          visible={checkPhoneNumberLoading ? true : false}
          textContent={'Loading...'}
          textStyle={{ color: '#FFF' }}
        />

        {show ? (
          <Content disableKBDismissScroll={false}>
            <Text style={styles.textHeader}>AYO SELANGKAH LAGI !</Text>
            <Text style={styles.textBelowHeader}>JANGAN DIANGKAT !</Text>
            <Image
              source={require('../../../../assets/img/otp-info.png')}
              style={styles.image}></Image>

            <Row style={styles.rowText}>
              <Text style={styles.textBlack}>
                Kami akan melakukan{' '}
                {<Text style={styles.textRed}>MISSCALL</Text>} ke nomor
              </Text>
            </Row>

            <Row style={styles.rowText}>
              <Text style={styles.textBlack}>
                yang Anda daftarkan.{' '}
                {<Text style={styles.textRed}>4 DIGIT </Text>}terakhir nomor
              </Text>
            </Row>

            <Row style={styles.rowText}>
              <Text style={styles.textBlack}>
                tersebut adalah kode {<Text style={styles.textRed}>OTP </Text>}
                anda
              </Text>
            </Row>

            <TouchableOpacity
              style={styles.btnOtp}
              disabled={submitting}
              testID="KirimOtp"
              onPress={() => this.props.navigation.navigate('Verifikasi')}>
              <Text
                style={{
                  color: '#1c1e61',
                  fontSize: 16,
                  fontFamily: fonts.primary.bold,
                }}>
                SAYA MENGERTI, KIRIM OTP
              </Text>
            </TouchableOpacity>
          </Content>
        ) : (
            <Content disableKBDismissScroll={false}>
              <Text style={styles.textHeader}>Yuk Registrasi BAF Mobile !</Text>
              <Text style={styles.textBelowHeader}>
                Semua Impianmu akan Terwujud Disini
            </Text>

              {/* <View style={styles.containerOptionalRegister}>
              <View>
                <Row style={styles.rowButton}>
                  <TouchableOpacity
                    style={styles.btnFacebook}
                    onPress={() => this.registerFacebook()}>
                    <Image
                      source={require('../../../assets/img/facebook.png')}
                      style={styles.authLogo}></Image>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 14,
                        fontFamily: 'arial',
                      }}>
                      Facebook
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.btnGoogle}
                    onPress={() => this.registerGoogle()}>
                    <Image
                      source={require('../../../assets/img/google.png')}
                      style={styles.authLogo}></Image>
                    <Text
                      style={{
                        color: 'black',
                        fontSize: 14,
                        fontFamily: 'arial',
                      }}>
                      Google
                    </Text>
                  </TouchableOpacity>
                </Row>
              </View>
            </View> */}

              <View style={styles.containerDivider}>
                <Left>
                  <View style={styles.listDivider}></View>
                </Left>
                <Body style={{ maxWidth: 70 }}>
                  <Text style={styles.textBelowLogo}>Daftar</Text>
                </Body>
                <Right>
                  <View style={styles.listDivider}></View>
                </Right>
              </View>

              <Form style={styles.containerForm}>
                <Field
                  name="Username"
                  type="text"
                  styleType={styles.inputDefault}
                  component={renderField}
                  placeholder="Nama Lengkap Anda"
                  icon="user"
                  iconright={false}
                  editable={true}
                />

                <Field
                  name="phoneNo"
                  type="text"
                  styleType={styles.inputDefault}
                  component={renderField}
                  placeholder="Nomor Handphone"
                  icon="phone"
                  iconright={false}
                  maxLength={15}
                  keyboardType="number-pad"
                  editable={true}
                />

                <Field
                  name="Email"
                  type="text"
                  styleType={styles.inputDefault}
                  component={renderField}
                  placeholder="Email"
                  icon="envelope"
                  iconright={false}
                  editable={true}
                />

                <Field
                  name="password"
                  type="text"
                  styleType={styles.inputDefault}
                  component={renderField}
                  placeholder="Password"
                  icon="lock"
                  iconright={this.state.icon}
                  secureTextEntry={this.state.hide}
                  onPressIcon={() => this.showHidePassword()}
                  editable={true}
                />

                <Row style={styles.rowButton}>
                  <Field
                    name="BirthPlace"
                    type="text"
                    component={renderField}
                    styleType={styles.inputHalf}
                    placeholder="Tempat"
                    icon="map-marker"
                    iconright={false}
                    editable={true}
                  />

                  <Field
                    name="BirthDate"
                    type="text"
                    date={this.state.BirthDate}
                    openModal={() => {
                      this.openDatePicker();
                    }}
                    closeModal={() => {
                      this.toggleModal();
                    }}
                    visible={this.state.visible}
                    component={renderField2}
                    styleType={styles.inputDate}
                    placeholder="Tanggal Lahir"
                    icon="calendar"
                    iconright={false}
                    onDateChange={(date) => {
                      this.onChangeDate(date);
                    }}
                  />
                </Row>

                <TouchableOpacity
                  testID="Daftar"
                  style={styles.btnRegister}
                  disabled={submitting}
                  onPress={handleSubmit(this.daftarHandler)}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 14,
                      fontFamily: fonts.primary.bold,
                    }}>
                    Daftar
                </Text>
                </TouchableOpacity>

                <View style={styles.containerTextBottom}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      fontSize: 12,
                      fontFamily: fonts.primary.normal,
                      color: '#002F5F',
                      marginTop: 5,
                    }}>
                    Dengan Mendaftar, Saya Setuju dengan{' '}
                  </Text>
                  <Text
                    testID="TermCondition"
                    style={{
                      fontSize: 12,
                      fontFamily: fonts.primary.bold,
                      color: '#002F5F',
                    }}
                    onPress={() => this.goPrivacy()}>
                    Kebijakan Privasi{' '}
                    {
                      <Text
                        style={{
                          color: '#002F5F',
                          fontFamily: fonts.primary.normal,
                        }}>
                        dan{' '}
                      </Text>
                    }
                  </Text>
                  <Text
                    testID="SyaratPenggunaan"
                    style={{
                      fontSize: 12,
                      fontFamily: 'arial',
                      color: '#002F5F',
                      fontFamily: fonts.primary.bold,
                    }}
                    onPress={() => this.goSyaratPengunaan()}>
                    Syarat Penggunaan
                </Text>
                </View>
              </Form>
              {this.showAlert()}
            </Content>
          )}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    checkPhoneNumberLoading: state.checkPhoneNumber.loading,
    checkPhoneNumberResult: state.checkPhoneNumber.result,
    checkPhoneNumberError: state.checkPhoneNumber.error,
    formRegister: state.form.formRegister,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      checkPhoneNumber,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

Register = reduxForm({
  form: 'formRegister',
  enableReinitialize: true,
  validate: FormRegisterValidate,
})(Register);

export default connect(mapStateToProps, matchDispatchToProps)(Register);
