/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-did-update-set-state */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
  View,
  Text,
  Platform,
  Image,
  TouchableOpacity,
  Alert,
  Keyboard,
  StyleSheet,
  Button,
  NativeModules,
  BackHandler,
  Dimensions,
  PixelRatio,
} from 'react-native';
import {
  Spinner,
  Container,
  Icon,
  Form,
  Label,
  Input,
  Item,
  ListItem,
  CheckBox,
  Right,
  Left,
  Body,
  Row,
  DatePicker,
  Content,
} from 'native-base';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { styles } from './style';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import SubHeaderAndroid from '../../../components/android/subHeader';
import SubHeaderIos from '../../../components/ios/subHeader';
import Toast, { DURATION } from 'react-native-easy-toast';
import {
  otpCall,
  otpSms,
  otpValidate,
  register,
} from '../../../actions/authentication/register';
import { getAuthkey } from '../../../actions/credoLabs';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import analytics from '@react-native-firebase/analytics';
import moment from 'moment';

var CredolabModul = NativeModules.CredolabModul;

class Verifikasi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      timer: 60,
      message: '',
      code: '',
      userRegister: { phoneNo: '' },
      otpType: 'call',
      isConnected: false,
      authKeyCredolab: false,
      UsrRef: '',
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
    };
  }

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.props.navigation.navigate('Register');
      return true;
    }
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{ flexDirection: 'row' }}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{ color: 'red', marginLeft: '-10%' }}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  componentDidMount = async () => {
    const { otpCall, getAuthkey } = this.props;
    this.tick();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    const storageUserRegister = await AsyncStorage.getItem('userRegister');
    const userRegister = JSON.parse(storageUserRegister);
    this.setState({ userRegister: userRegister });
    getAuthkey();

    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        otpCall({ msisdn: this.state.userRegister.phoneNo });
      } else {
        this.showToastNoInternet();
      }
    });
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      otpValidateResult,
      otpValidateLoading,
      otpValidateError,
      registerResult,
      registerError,
      getAuthkey_Result,
      getAuthkey_Error,
      register,
    } = this.props;
    const { userRegister } = this.state;

    if (this.state.timer === 0) {
      clearInterval(this.interval);
      this.setState({
        timer: null,
        show: true,
        message: 'Waktu anda sudah habis',
        error: false,
      });
    }

    if (
      getAuthkey_Result !== null &&
      prevProps.getAuthkey_Result !== getAuthkey_Result
    ) {
      this.setState({ authKeyCredolab: getAuthkey_Result });
    }

    if (
      otpValidateResult !== null &&
      prevProps.otpValidateResult !== otpValidateResult
    ) {
      analytics().logEvent('eventRegisterUser', {
        item: 'user trying to register the data',
      });
      register({
        Username: userRegister.Username,
        Password: userRegister.password,
        Email: userRegister.Email,
        Phoneno: userRegister.phoneNo,
        BirthPlace: userRegister.BirthPlace,
        BirthDate: userRegister.BirthDate,
        AreaUsr: userRegister.BirthPlace,
        UsrRef: this.state.UsrRef, //added UsrRef, default: '', with CredolabService function: phoneNo + timestamp
        UsrCrt: userRegister.Username,
        UsrAdr: '',
        UsrImg: '',
        Gender: '',
      });
    }

    if (
      otpValidateError !== null &&
      prevProps.otpValidateError !== otpValidateError
    ) {
      clearInterval(this.interval);
      this.setState({
        show: true,
        message: 'Kode OTP yang anda masukkan salah',
        error: true,
      });
    }

    if (
      registerResult !== null &&
      prevProps.registerResult !== registerResult
    ) {
      await AsyncStorage.removeItem('userRegister');
      await AsyncStorage.setItem('userToken', registerResult.Usertoken);
      await AsyncStorage.setItem(
        'userData',
        JSON.stringify({
          Usergroupid: registerResult.UserGroupID,
        }),
      );
      await this.credolabService();

      this.setState({
        alertShowed: true,
        alertFor: 'registerSuccess',
        alertMessage: 'Anda berhasil daftar di BAF Mobile',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
      });
    }

    if (registerError !== null && prevProps.registerError !== registerError) {
      this.setState({
        alertShowed: true,
        alertMessage: registerError.message,
        alertTitle: 'Gagal',
        alertType: 'error',
        alertDoneText: 'OK',
      });
    }
  };

  componentWillUnmount = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    clearInterval(this.interval);
  };

  goBack = async () => {
    this.props.navigation.navigate('Register');
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        onPressNegativeButton={this.handleNegativeButtonAlert}
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
      />
    );
  };

  //Memanggil credolab service dengan menggunakan signature no handphnoe dan timestamp
  //Dipanggil ketika Register berhasil

  credolabService = async () => {
    const { authKeyCredolab, userRegister } = this.state;
    const timeStamp = moment(new Date());
    const refNumber = userRegister.phoneNo + '' + timeStamp;
    if (authKeyCredolab && authKeyCredolab !== null) {
      try {
        var credolabCallback = await CredolabModul.setCollectCredolab(
          refNumber,
          authKeyCredolab.Auth,
          authKeyCredolab.Url,
        );
        this.setState({ UsrRef: refNumber });
        console.log(credolabCallback); //for testing if credolab success or not
      } catch (e) {
        console.log(e);
      }
    }
  };

  handleNegativeButtonAlert = () => {
    this.setState({ alertShowed: false });
  };

  handlePositiveButtonAlert = () => {
    this.setState({ alertShowed: false });
    if (this.state.alertFor === 'registerSuccess') {
      this.props.navigation.navigate('Home');
    }
  };

  tick = () => {
    this.interval = setInterval(
      () => this.setState((prevState) => ({ timer: prevState.timer - 1 })),
      1000,
    );
    this;
  };

  otpCall = () => {
    const { otpCall } = this.props;

    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        this.setState({
          show: false,
          timer: 60,
          error: false,
          code: '',
          otpType: 'call',
        });
        this.tick(); //this gonna trigger time interval
        analytics().logEvent('eventOtpCall', {
          item: 'user trying otp call',
        });
        otpCall({ msisdn: this.state.userRegister.phoneNo });
      } else {
        this.showToastNoInternet();
      }
    });
  };

  otpSms = () => {
    const { otpSms } = this.props;

    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        this.setState({
          show: false,
          timer: 60,
          error: false,
          code: '',
          otpType: 'sms',
        });
        this.tick(); //this gonna trigger time interval
        analytics().logEvent('eventOtpSms', {
          item: 'user trying otp sms',
        });
        otpSms({ Phoneno: this.state.userRegister.phoneNo });
      } else {
        this.showToastNoInternet();
      }
    });
  };

  otpWhatsapp = () => {
    this.setState({
      alertShowed: true,
      alertMessage: 'Fitur ini sedang dalam tahap pengembangan.',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  handlerCodeFill = (data) => {
    const { otpValidate } = this.props;
    analytics().logEvent('eventOtpValidation', {
      item: 'user trying otp validation',
    });
    otpValidate({
      Phoneno: this.state.userRegister.phoneNo,
      OTP: data,
    });
  };

  mask = (val) => {
    const result =
      val.substr(0, 3) + '*******' + val.substr(val.length - 2, val.length - 1);
    return result;
  };

  render() {
    const { handleSubmit, submitting } = this.props;
    const { show } = this.state;

    var phoneNo = this.mask(this.state.userRegister.phoneNo);

    return (
      <Container style={{ flex: 1 }}>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid title="Verifikasi Akun" goBack={this.goBack} />
        ) : (
            <SubHeaderIos title="Verifikasi Akun" goBack={this.goBack} />
          )}
        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />

        <Content disableKBDismissScroll={false}>
          <Image
            source={require('../../../../assets/img/otp.png')}
            style={styles.image}
          />

          <Text style={styles.textHeader}>Masukkan Kode OTP</Text>

          <Row style={styles.rowText}>
            <Text style={styles.textDesc}>
              Masukkan 4 digit kode yang di kirimkan ke
            </Text>
          </Row>

          <Row style={styles.rowText}>
            <Text style={styles.textDesc}>Nomor Anda {phoneNo}</Text>
          </Row>

          <OTPInputView
            style={{
              width: '70%',
              height: 100,
              alignSelf: 'center',
            }}
            pinCount={4}
            code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
            onCodeChanged={(code) => {
              this.setState({ code });
            }}
            autoFocusOnLoad
            codeInputFieldStyle={{
              width: 50,
              height: 45,
              borderWidth: 0,
              borderBottomWidth: 1,
              color: this.state.error ? 'red' : 'black',
              backgroundColor: '#f9f9f9',
              fontSize: 20,
            }}
            codeInputHighlightStyle={styles.underlineStyleHighLighted}
            onCodeFilled={this.handlerCodeFill}
          />

          {show ? (
            <View>
              <Row style={styles.rowText}>
                <Text style={styles.textDesc}>{this.state.message}</Text>
              </Row>

              <Row style={styles.rowText}>
                <Text style={styles.textDesc}>
                  Silahkan kirim OTP melalui :
                </Text>
              </Row>

              <View style={styles.containerOptionalLogin}>
                <TouchableOpacity
                  style={styles.btnOtp}
                  onPress={() => this.otpCall()}>
                  <Image
                    source={require('../../../../assets/img/otp-call.jpg')}
                    style={styles.authLogo}
                  />
                  <View style={styles.containerTextBtn}>
                    <Text
                      style={{
                        color: '#002f5f',
                        fontSize: 16,
                        fontWeight: 'bold',
                      }}>
                      Panggilan
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.btnOtp}
                  onPress={() => this.otpSms()}>
                  <Image
                    source={require('../../../../assets/img/otp-sms.jpg')}
                    style={styles.authLogo}
                  />
                  <View style={styles.containerTextBtn}>
                    <Text
                      style={{
                        color: '#0047CC',
                        fontSize: 16,
                        fontWeight: 'bold',
                      }}>
                      SMS
                    </Text>
                  </View>
                </TouchableOpacity>
                {/* <TouchableOpacity
                  style={styles.btnOtp}
                  onPress={() => this.otpWhatsapp()}>
                  <Image
                    source={require('../../../assets/img/otp-wa.jpg')}
                    style={styles.authLogo}
                  />
                  <View style={styles.containerTextBtn}>
                    <Text
                      style={{
                        color: '#00B837',
                        fontSize: 16,
                        fontWeight: 'bold',
                      }}>
                      Whatsapp
                    </Text>
                  </View>
                </TouchableOpacity> */}
              </View>
            </View>
          ) : (
              <View>
                <Row style={styles.rowText}>
                  <Text style={styles.textDesc}>
                    Silahkan tunggu selama{' '}
                    {<Text style={styles.textDesc}>{this.state.timer}</Text>}
                  </Text>
                </Row>

                <Row style={styles.rowText}>
                  <Text style={styles.textDesc}>Untuk mengirim ulang kode</Text>
                </Row>
              </View>
            )}
          {this.showAlert()}
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    otpCallLoading: state.otpCall.loading,
    otpCallResult: state.otpCall.result,
    otpCallError: state.otpCall.error,
    otpSmsLoading: state.otpSms.loading,
    otpSmsResult: state.otpSms.result,
    otpSmsError: state.otpSms.error,
    otpValidateLoading: state.otpValidate.loading,
    otpValidateResult: state.otpValidate.result,
    otpValidateError: state.otpValidate.error,
    registerLoading: state.register.loading,
    registerResult: state.register.result,
    registerError: state.register.error,
    getAuthkey_Loading: state.getAuthkey.loading,
    getAuthkey_Result: state.getAuthkey.result,
    getAuthkey_Error: state.getAuthkey.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      otpCall,
      otpSms,
      otpValidate,
      getAuthkey,
      register,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, matchDispatchToProps)(Verifikasi);
