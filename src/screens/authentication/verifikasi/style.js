/* eslint-disable prettier/prettier */
import {StyleSheet, Dimensions} from 'react-native';
import {fonts} from '../../../utils/fonts';

const {width: WIDTH} = Dimensions.get('window');

export const styles = StyleSheet.create({
  borderStyleBase: {
    width: 30,
    height: 45,
  },
  borderStyleHighLighted: {
    borderColor: '#03DAC6',
  },
  underlineStyleBase: {
    width: 50,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
    color: 'black',
    backgroundColor: '#f9f9f9',
    fontSize: 20,
  },
  underlineStyleHighLighted: {
    borderColor: 'black',
  },
  image: {
    width: '100%',
    height: 240,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  textHeader: {
    color: '#1c1e61',
    fontSize: 20,
    alignSelf: 'center',
    paddingBottom: 10,
    fontFamily: fonts.primary.normal,
  },
  textDesc: {
    color: '#1c1e61',
  },

  rowText: {
    alignSelf: 'center',
    alignContent: 'center',
    textAlign: 'center',
  },
  rowText2: {
    alignSelf: 'center',
  },
  containerOptionalLogin: {
    backgroundColor: '#fff',
    paddingVertical: '3%',
    paddingHorizontal: '25%',
  },
  authLogo: {
    width: 20,
    height: 20,
    marginRight: '10%',
  },
  containerTextBtn: {
    alignItems: 'center',
    left: -10,
    width: '80%',
  },
  btnOtp: {
    backgroundColor: '#FFF',
    flexDirection: 'row',
    width: '100%',
    height: 40,
    marginVertical: 10,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
});
