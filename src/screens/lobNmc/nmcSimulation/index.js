import React, {Component} from 'react';
import {styles} from './style';
import {
  TouchableOpacity,
  Text,
  View,
  Platform,
  SafeAreaView,
  Dimensions,
  PixelRatio,
  Image,
} from 'react-native';
import {
  Container,
  Input,
  Item,
  Label,
  Header,
  Left,
  Right,
  Button,
  Body,
  Title,
  Icon,
  Picker,
  Card,
  Content,
} from 'native-base';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {reduxForm, Field, change, reset, formValueSelector} from 'redux-form';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-easy-toast';
import {
  BAF_COLOR_BLUE,
  BORDER_COLOR,
  BG_COLOR,
  MESSAGE_TOKEN_EXP,
  TOKEN_EXP,
  MESSAGE_TOKEN_INVALID,
} from '../../../utils/constant';
import {
  substringCircum,
  substringSecondCircum,
  formatCurrency,
  fdeFormatCurrency,
  normalizeCurrency,
  jsonParse,
  handleRefreshToken,
} from '../../../utils/utilization';
import {FormSimulationNmcValidate} from '../../../validates/FormSimulationNmcValidate';
import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Spinner from 'react-native-loading-spinner-overlay';
import NMC from './nmc.json';
import moment from 'moment';
import {fonts} from '../../../utils/fonts';
import {getPengajuanNMC} from '../../../actions/lob';
import analytics from '@react-native-firebase/analytics';
import {getOTR} from '../../../actions/lob';
import {refreshToken} from '../../../actions';

export const renderField = ({
  input,
  type,
  label,
  editable,
  keyboardType,
  maxLength,
  multiline,
  placeholder,
  numberOfLines,
  format,
  normalize,
  customError,
  onChangeText,
  meta: {touched, error},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '80%', marginTop: 15, alignSelf: 'center'}}>
      <Label
        style={{
          fontSize: 16,
          color: hasError ? 'red' : 'black',
          alignSelf: 'center',
          marginBottom: 5,
          fontFamily: fonts.primary.normal,
        }}>
        {label}
      </Label>
      <Item
        regular
        style={{
          backgroundColor: 'white',
          height: 40,
          borderColor: customError || hasError ? 'red' : BAF_COLOR_BLUE,
          borderWidth: 1,
          borderRadius: 5,
          paddingRight: 10,
        }}>
        <Input
          style={{
            fontSize: 14,
            paddingLeft: '4%',
            color: editable ? 'black' : '#aaa',
            fontFamily: fonts.primary.normal,
          }}
          {...input}
          type={type}
          onChangeText={onChangeText}
          placeholder={placeholder}
          editable={editable}
          keyboardType={keyboardType}
          maxLength={maxLength}
          multiline={multiline}
          numberOfLines={numberOfLines}
          format={format}
          normalize={normalize}
        />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderFieldDisabled = ({
  input,
  type,
  label,
  editable,
  placeholder,
  format,
  normalize,
  customError,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '80%', marginTop: 15, alignSelf: 'center'}}>
      <Label
        style={{
          fontSize: 16,
          color: hasError ? 'red' : 'black',
          alignSelf: 'center',
          marginBottom: 5,
        }}>
        {label}
      </Label>
      <Item
        regular
        style={{
          backgroundColor: BG_COLOR,
          height: 40,
          borderColor: customError || hasError ? 'red' : BG_COLOR,
          borderWidth: 1,
          borderRadius: 5,
          paddingRight: 10,
        }}>
        <Input
          style={{
            fontSize: 14,
            paddingLeft: '4%',
            color: 'black',
          }}
          {...input}
          placeholder={placeholder}
          type={type}
          editable={false}
          format={format}
          normalize={normalize}
        />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderFieldHidden = ({
  input,
  type,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '100%'}}>
      <Item style={{display: 'none'}}>
        <Input {...input} ype={type} />
      </Item>
      {/* {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null} */}
    </View>
  );
};

export const renderFieldPicker = ({
  input,
  label,
  pickerSelected,
  onValueChange,
  customError,
  enabled,
  data,
  testId,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <SafeAreaView
      testID={testId}
      style={{width: '80%', marginTop: 15, alignSelf: 'center'}}>
      <Label
        style={{
          fontSize: 14,
          color: customError || hasError ? 'red' : 'black',
          alignSelf: 'center',
          marginBottom: 5,
          fontFamily: fonts.primary.normal,
        }}>
        {label}
      </Label>
      <View
        style={{
          backgroundColor: 'white',
          height: 40,
          borderColor:
            customError || hasError
              ? 'red'
              : pickerSelected
              ? BAF_COLOR_BLUE
              : BORDER_COLOR,
          borderWidth: 1,
          borderRadius: 5,
          paddingRight: 10,
        }}>
        <Picker
          {...input}
          renderHeader={(backAction) => (
            <Header>
              <Left>
                <Button transparent onPress={backAction}>
                  <Icon name="arrow-back" style={{color: 'black'}} />
                </Button>
              </Left>
              <Body style={{flex: 3}}>
                <Title>{label}</Title>
              </Body>
              <Right />
            </Header>
          )}
          note={enabled ? false : true}
          mode="dropdown"
          style={{
            marginTop: Platform.OS == 'android' ? -6 : -4,
            fontSize: 14,
            width: '100%',
          }}
          enabled={enabled}
          selectedValue={pickerSelected}
          placeholder="Silahkan Pilih"
          onValueChange={onValueChange}>
          {data}
        </Picker>
      </View>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </SafeAreaView>
  );
};

class NmcSimulation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Kredit Motor Yamaha',
      showList: false,
      query: '',
      isConnected: false,

      pickerLobItem: [
        {Code: 'Motor Yamaha'},
        {Code: 'Mobil Baru'},
        {Code: 'Multiproduk'},
        {Code: 'Dana Syariah'},
      ],
      pickerLobSelected: 'Motor Yamaha',

      pickerTipeSelected: '',

      pickerTenorSelected: '',

      pickerDpSelected: '',

      showResultSimulation: false,

      detailUser: false,
      userData: false,

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: '',
      alertDoneText: '',
      alertCancelText: '',

      isModalVisible: false,
      dataNIK: false,
      merk: 'YAMAHA',
      calculateResult: false,
      LOB: 'NMC',
      userToken: '',
      dataMerge: '',
    };
  }

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  componentDidMount = async () => {
    analytics().logScreenView({
      screen_class: 'screenNmcSimulation',
      screen_name: 'screenNmcSimulation',
    });
    this.checkInternet();
    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) {
      this.setState({userToken: userToken});
    }

    const detailUserNonParse = await AsyncStorage.getItem('detailUser');
    if (detailUserNonParse) {
      const detailUserParse = JSON.parse(detailUserNonParse);
      this.setState({detailUser: detailUserParse});
    }
    this.props.updateField('formNmc', 'merkMotor', this.state.merk);
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      detailUserResult,
      getPengajuanNMCResult,
      getPengajuanNMCError,
      getOTR_Result,
      getOTR_Error,
      getOTR_Loading,
      refreshTokenResult,
      refreshTokenError,
    } = this.props;
    const {dataMerge} = this.state;
    if (detailUserResult && prevProps.detailUserResult !== detailUserResult) {
      this.setState({
        detailUser: detailUserResult,
      });
    }

    if (
      getPengajuanNMCError &&
      prevProps.getPengajuanNMCError !== getPengajuanNMCError
    ) {
      const data = await jsonParse(getPengajuanNMCError.message);
      if (data) {
        console.log(data);
        if (data.message == MESSAGE_TOKEN_EXP) {
          this.props.refreshToken(this.state.userToken);
          // this.handleInvalid();
        } else if (data.message == MESSAGE_TOKEN_INVALID) {
          this.handleInvalid();
        } else {
          this.setState({
            alertFor: 'getPengajuanNMCError',
            alertShowed: true,
            alertMessage:
              'Terjadi kesalahan pada sistem, harap coba beberapa saat lagi.',
            alertTitle: 'Gagal',
            alertType: 'error',
            alertDoneText: 'OK',
          });
        }
      } else {
        this.setState({
          alertFor: 'getPengajuanNMCError',
          alertShowed: true,
          alertMessage:
            'Terjadi kesalahan pada sistem, harap coba beberapa saat lagi.',
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
    }

    if (
      getPengajuanNMCResult &&
      prevProps.getPengajuanNMCResult !== getPengajuanNMCResult
    ) {
      this.setState({
        alertFor: 'getPengajuanNMCResult',
        alertShowed: true,
        alertMessage: 'Data anda akan kami proses',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
      });
    }

    if (getOTR_Loading && prevProps.getOTR_Loading !== getOTR_Loading) {
    }

    if (getOTR_Error && prevProps.getOTR_Error !== getOTR_Error) {
      this.props.updateField('formNmc', 'hargaMotor', '');
    }

    if (getOTR_Result && prevProps.getOTR_Result !== getOTR_Result) {
      if (getOTR_Result.LOBCode == this.state.LOB) {
        this.props.updateField('formNmc', 'hargaMotor', getOTR_Result.OTRPrice);
      }
    }

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      console.log('Error Refresh Token ', refreshTokenError.message);
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      if (getPengajuanNMCError) {
        await handleRefreshToken(refreshTokenResult.Refreshtoken);
        this.setState({userToken: refreshTokenResult.Refreshtoken});
        this.props.getPengajuanNMC(
          {
            DateReq: dataMerge.DateReq,
            DP: dataMerge.DP,
            JenisBrg: dataMerge.JenisBrg,
            Pinjaman: dataMerge.Pinjaman,
            PromoCode: dataMerge.PromoCode,
            Source: dataMerge.Source,
            Tenor: dataMerge.Tenor,
            TipePengajuan: dataMerge.TipePengajuan,
          },
          refreshTokenResult.Refreshtoken,
        );
      }
    }
  };

  handleInvalid = async () => {
    await AsyncStorage.removeItem('userToken');
    await AsyncStorage.removeItem('userData');
    await AsyncStorage.removeItem('detailUser');
    await AsyncStorage.removeItem('activeAgreement');
    this.setState({
      alertFor: 'invalidToken',
      alertShowed: true,
      alertMessage: 'Sesi login Anda sudah habis',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  pickerLobChange = async (data) => {
    const {resetForm, navigation} = this.props;
    this.setState({
      pickerMerkSelected: '',
      pickerDpSelected: '',
      showResultSimulation: false,
    });
    await resetForm('formNmc');
    switch (data) {
      case 'Motor Yamaha':
        navigation.replace('NmcSimulation');
        break;
      case 'Multiproduk':
        navigation.replace('MultiproductSimulation');
        break;
      case 'Dana Syariah':
        navigation.replace('SyanaSimulation');
        break;
      default:
        navigation.navigate('CarSimulation');
    }
  };

  onTipeChange = (value) => {
    const {getOTR} = this.props;
    const {LOB} = this.state;
    this.setState({
      pickerTipeSelected: value,
    });
    this.props.updateField('formNmc', 'tipeMotor', value);
    this.resetField();
    if (value) {
      getOTR({
        LobCode: LOB,
        MerkCode: '',
        TipeCode: value,
        ManufactureYear: '',
      });
    }
  };

  onHargaChange = async (value) => {
    this.resetField();
  };

  onTenorChange = (value) => {
    this.setState({
      pickerTenorSelected: value,
      showResultSimulation: false,
    });
    this.props.updateField('formNmc', 'tenor', value);
  };

  onDpChange = (value) => {
    const {formNmc} = this.props;
    this.setState({
      pickerDpSelected: value,
      showResultSimulation: false,
    });
    var uangMukaAmount = '';
    this.props.updateField('formNmc', 'uangMuka', value);

    if (!value || value == '') {
      this.props.updateField('formNmc', 'uangMukaAmount', uangMukaAmount);
    } else {
      if (formNmc.values.hargaMotor > 0) {
        uangMukaAmount =
          parseFloat(value) * parseFloat(formNmc.values.hargaMotor);
        this.props.updateField(
          'formNmc',
          'uangMukaAmount',
          uangMukaAmount.toString(),
        );
      } else {
        this.props.updateField('formNmc', 'uangMukaAmount', '0');
      }
    }
  };

  resetField = () => {
    this.setState({
      pickerTenorSelected: '',
      pickerDpSelected: '',
    });
    this.props.updateField('formNmc', 'tenor', '');
    this.props.updateField('formNmc', 'uangMuka', '');
    this.props.updateField('formNmc', 'uangMukaAmount', '');
    this.setState({showResultSimulation: false});
  };

  submitCalculate = async (data) => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        var HargaMotor = parseFloat(data.hargaMotor);
        var UangMukaAmount = parseFloat(data.uangMukaAmount);
        var Tenor = substringCircum(data.tenor);
        var Rate = substringSecondCircum(data.tenor);

        var calculateResult =
          ((HargaMotor - UangMukaAmount) * (1 + parseFloat(Rate))) /
          parseFloat(Tenor);
        this.setState({
          showResultSimulation: true,
          calculateResult: Math.round(calculateResult),
        });
      } else {
        this.showToastNoInternet();
      }
    });
  };

  submitData = async (data) => {
    const userToken = await AsyncStorage.getItem('userToken');
    const {detailUser, LOB} = this.state;
    const {navigation, getPengajuanNMC} = this.props;
    NetInfo.fetch().then(async (state) => {
      if (state.isConnected) {
        if (!detailUser) {
          navigation.navigate('Login');
          // this.handleInvalid()
        } else {
          analytics().logEvent('eventNmcSubmit', {
            item: 'user trying to submit NMC simulation',
          });

          var dataMerge = {
            DateReq: moment().format('YYYY-MM-DD hh:mm:ss'),
            DP: data.uangMuka * 100 + '%',
            JenisBrg: data.merkMotor + ' ' + data.tipeMotor,
            Pinjaman: 'Rp.' + data.hargaMotor,
            PromoCode: '-',
            Source: 'BAF Mobile',
            Tenor: substringCircum(data.tenor),
            TipePengajuan: LOB,
          };
          getPengajuanNMC(
            {
              DateReq: dataMerge.DateReq,
              DP: dataMerge.DP,
              JenisBrg: dataMerge.JenisBrg,
              Pinjaman: dataMerge.Pinjaman,
              PromoCode: dataMerge.PromoCode,
              Source: dataMerge.Source,
              Tenor: dataMerge.Tenor,
              TipePengajuan: dataMerge.TipePengajuan,
            },
            userToken,
          );
          this.setState({dataMerge: dataMerge});
        }
      } else {
        this.showToastNoInternet();
      }
    });
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
        onPressNegativeButton={this.handleNegativeButtonAlert}
      />
    );
  };

  handlePositiveButtonAlert = () => {
    if (this.state.alertFor === 'showModal') {
      this.setState({alertShowed: false, alertFor: false});
      this.showModal();
    } else if (
      this.state.alertFor === 'getPengajuanNMCResult' ||
      this.state.alertFor === 'getPengajuanNMCError'
    ) {
      this.setState({alertShowed: false, alertFor: false});
      this.props.navigation.navigate('Home');
    } else if (this.state.alertFor === 'invalidToken') {
      this.setState({alertShowed: false, alertFor: false});
      this.props.navigation.navigate('Login');
    } else {
      this.setState({alertShowed: false, alertFor: false});
    }
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false, alertFor: false});
  };

  showIosLoadingSpinnerNmc = () => {
    return (
      <View
        style={{
          backgroundColor: 'grey',
          opacity: 0.5,
          position: 'absolute',
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={require('../../../../assets/img/loading.gif')}
          style={{width: 150, height: 150}}
        />
      </View>
    );
  };

  render = () => {
    const {
      pickerLobItem,
      pickerLobSelected,
      pickerTipeSelected,
      pickerTenorSelected,
      pickerDpSelected,
      showResultSimulation,
      calculateResult,
    } = this.state;

    const {
      handleSubmit,
      submitting,
      mpInstCalculationLoading,
      mpMappingRiskLoading,
      getPengajuanNMCLoading,
      getOTR_Loading,
    } = this.props;

    const NMCTipe = NMC.Tipe;
    const NMCDp = NMC.Dp;
    const NMCTenor = NMC.Tenor;

    return (
      <Container>
        {Platform.OS === 'android' ? (
          <Spinner
            visible={getPengajuanNMCLoading || getOTR_Loading ? true : false}
            textContent={'Sedang memproses...'}
            textStyle={{color: '#FFF'}}
          />
        ) : null}

        <Toast
          ref="toast"
          style={{
            marginTop:
              Platform.OS != 'android'
                ? Dimensions.get('window').width * PixelRatio.get() > 750
                  ? 44
                  : 20
                : 0,
            backgroundColor: '#FFE6E9',
            width: '100%',
            borderBottomWidth: 2,
            borderBottomColor: 'red',
            justifyContent: 'center',
            paddingLeft: 50,
            height: 50,
            borderRadius: 0,
          }}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />

        <Content disableKBDismissScroll={false}>
          <Field
            name="merkMotor"
            component={renderField}
            editable={false}
            label="Merk Motor"
          />

          <Field
            name="tipeMotor"
            testId="tipeMotor"
            component={renderFieldPicker}
            enabled={true}
            label="Tipe Motor"
            customError={this.state.errorTipeMotor}
            pickerSelected={pickerTipeSelected}
            data={NMCTipe.map((NMCTipe, index) => (
              <Picker.Item label={NMCTipe.Descr} value={NMCTipe.Code} />
            ))}
            onValueChange={(newValue) => this.onTipeChange(newValue)}
          />

          <Field
            name="hargaMotor"
            label="Harga Motor"
            type="text"
            editable={true}
            placeholder="Rp. 0"
            component={renderField}
            format={fdeFormatCurrency}
            normalize={normalizeCurrency}
            keyboardType={'number-pad'}
            customError={this.state.errorHargaMotor}
          />

          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              marginHorizontal: '5%',
              alignSelf: 'center',
              justifyContent: 'space-between',
            }}>
            <View style={{width: '49%'}}>
              <Field
                name="tenor"
                component={renderFieldPicker}
                label="Tenor"
                enabled={true}
                pickerSelected={pickerTenorSelected}
                data={NMCTenor.map((NMCTenor, index) => (
                  <Picker.Item label={NMCTenor.Descr} value={NMCTenor.Code} />
                ))}
                onValueChange={(newValue) => this.onTenorChange(newValue)}
              />
            </View>

            <View style={{width: '49%'}}>
              <Field
                name="uangMuka"
                component={renderFieldPicker}
                label="Uang Muka"
                enabled={true}
                pickerSelected={pickerDpSelected}
                data={NMCDp.map((NMCDp, index) => (
                  <Picker.Item label={NMCDp.Descr} value={NMCDp.Code} />
                ))}
                onValueChange={(newValue) => this.onDpChange(newValue)}
              />
            </View>
          </View>

          <Field
            name="uangMukaAmount"
            label="Nominal Uang Muka"
            type="text"
            editable={false}
            component={renderFieldHidden}
            format={formatCurrency}
            normalize={normalizeCurrency}
          />

          <TouchableOpacity
            testID="HitungAngsuran"
            style={styles.btnHitung}
            onPress={handleSubmit(this.submitCalculate)}
            disabled={submitting}>
            <Text style={styles.textButton}>Hitung Angsuran</Text>
          </TouchableOpacity>
          <View style={{height: 10}} />

          {showResultSimulation ? (
            <Card
              style={{
                backgroundColor: '#f5f5f5',
                width: '85%',
                alignItems: 'center',
                alignSelf: 'center',
                borderRadius: 10,
                marginBottom: '5%',
                paddingBottom: 25,
              }}>
              <Text style={styles.textHeader}>
                Estimasi Angsuran Bulanan Anda
              </Text>
              <Text style={styles.textBelowHeader}>
                Rp.{' '}
                {calculateResult
                  ? formatCurrency(calculateResult.toString())
                  : '-'}
              </Text>
              <Text style={styles.textDesc}>/bulan</Text>
              <View style={{height: 15}}></View>
              <Text style={styles.textDesc}>
                Perhitungan hanya bersifat simulasi,
              </Text>
              <Text style={styles.textDesc}>
                dapat berubah sesuai regulasi BAF
              </Text>
              <Text style={styles.textDesc}>
                Ajukan Aplikasi pembiayaan dan
              </Text>
              <Text style={styles.textDesc}>kami akan menghubungi Anda</Text>
              <TouchableOpacity
                style={styles.btnAjukan}
                onPress={handleSubmit(this.submitData)}>
                <Text style={styles.textButton}>Ajukan Sekarang</Text>
              </TouchableOpacity>
            </Card>
          ) : null}
        </Content>
        {Platform.OS !== 'android'
          ? getOTR_Loading || getPengajuanNMCLoading
            ? this.showIosLoadingSpinnerNmc()
            : null
          : null}
        {this.showAlert()}
      </Container>
    );
  };
}

const selector = formValueSelector('formNmc');

function mapStateToProps(state) {
  return {
    tipeMotor: selector(state, 'tipeMotor'),
    merkMotor: selector(state, 'merkMotor'),
    hargaMotor: selector(state, 'hargaMotor'),
    detailUserResult: state.detailUser.result,
    getPengajuanNMCResult: state.getPengajuanNMC.result,
    getPengajuanNMCError: state.getPengajuanNMC.error,
    getPengajuanNMCLoading: state.getPengajuanNMC.loading,
    formNmc: state.form.formNmc,
    getOTR_Result: state.getOTR.result,
    getOTR_Error: state.getOTR.error,
    getOTR_Loading: state.getOTR.loading,
    refreshTokenResult: state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getPengajuanNMC,
      getOTR,
      refreshToken,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

NmcSimulation = reduxForm({
  form: 'formNmc',
  enableReinitialize: true,
  validate: FormSimulationNmcValidate,
})(NmcSimulation);

export default connect(mapStateToProps, matchDispatchToProps)(NmcSimulation);
