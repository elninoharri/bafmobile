import React, {Component} from 'react';

import {Container} from 'native-base';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import SubHeaderAndroid from '../../components/android/subHeaderBlue';
import SubHeaderIos from '../../components/ios/subHeaderBlue';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {change, reset} from 'redux-form';

import NmcSimulation from '../../screens/lobNmc/nmcSimulation';
import CarSimulation from '../../screens/lobCar/carSimulation';
import SyanaSimulation from '../../screens/lobSyana/syanaSimulation';
import MultiproductSimulation from '../../screens/lobMultiproduct/multiproductSimulation';

import MyTabBar from '../../components/multiPlatform/lob';

class lob extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Motor Yamaha',
    };
  }

  //Mengganti subheader title
  changeSubHeaderTitle = (value) => {
    switch (value) {
      case 'NMC':
        this.setState({screenTitle: 'Motor Yamaha'});
        break;
      case 'mobil':
        this.setState({screenTitle: 'Mobil Baru'});
        break;
      case 'syariah':
        this.setState({screenTitle: 'Dana Syariah'});
        break;
      case 'MP':
        this.setState({screenTitle: 'Multi Produk'});
        break;
      default:
        this.setState({screenTitle: 'Motor'});
    }
  };

  goBack = async () => {
    this.props.navigation.navigate('Home');
  };

  render() {
    const Tab = createMaterialTopTabNavigator();
    const {tabSelected} = this.props.route.params;
    return (
      <Container>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid
            title={this.state.screenTitle}
            goBack={this.goBack}
          />
        ) : (
          <SubHeaderIos title={this.state.screenTitle} goBack={this.goBack} />
        )}

        <Tab.Navigator
          initialRouteName={tabSelected} //sebagai acuan di home bakal dibuka di mana tab pertamanya
          tabBar={(props) => <MyTabBar {...props} />} //custom tab bar
          backBehavior="history" //props untuk pengaturan back button ke mana,
          //order akan ke tab yang dibuka sebelumnya dan terakhir ke screen sebelumnya (home)
          tabBarOptions={{
            activeTintColor: '#e91e63',
            labelStyle: {fontSize: 12},
            style: {backgroundColor: 'powderblue'},
            showIcon: true,
          }}>
          <Tab.Screen
            name="Motor"
            children={() => (
              <NmcSimulation
                subHeaderTitle={this.changeSubHeaderTitle}
                navigation={this.props.navigation}
              />
            )}
            options={{tabBarLabel: 'motor'}} //ini label yang diprops ke custom tab bar
            listeners={{
              //dalam topBarNav semua screen di mount sehingga lifecycle mount dan unmount gak berlaku,
              //listener mengganti lifecyce ini dengan nangkep di mana tab yang lagi focus (gak blur) dan eksekusi perintah
              focus: (e) => {
                this.changeSubHeaderTitle('NMC'); //di sini yang dieksekusi pergantian subHeaderTitle
              },
            }}
          />

          <Tab.Screen
            name="Mobil"
            children={() => (
              <CarSimulation
                subHeaderTitle={this.changeSubHeaderTitle}
                navigation={this.props.navigation}
              />
            )}
            options={{tabBarLabel: 'mobil'}}
            listeners={{
              focus: (e) => {
                this.changeSubHeaderTitle('mobil');
              },
            }}
          />

          <Tab.Screen
            name="Multiproduk"
            children={() => (
              <MultiproductSimulation
                subHeaderTitle={this.changeSubHeaderTitle}
                navigation={this.props.navigation}
              />
            )}
            options={{tabBarLabel: 'MP'}}
            listeners={{
              focus: (e) => {
                this.changeSubHeaderTitle('MP');
              },
            }}
          />
          <Tab.Screen
            name="Syariah"
            children={() => (
              <SyanaSimulation
                subHeaderTitle={this.changeSubHeaderTitle}
                navigation={this.props.navigation}
              />
            )}
            options={{tabBarLabel: 'Syariah'}}
            listeners={{
              focus: (e) => {
                this.changeSubHeaderTitle('syariah');
              },
            }}
          />
        </Tab.Navigator>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

export default connect(mapStateToProps, matchDispatchToProps)(lob);
