import React, {Component} from 'react';
import {Text, View, BackHandler, Dimensions, PixelRatio} from 'react-native';
import {
  Container,
  Left,
  Icon,
  CardItem,
  Card,
  Content,
  Row,
  Right,
  Body,
} from 'native-base';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import SubHeaderAndroid from '../../components/android/subHeaderBlue';
import SubHeaderIos from '../../components/ios/subHeaderBlue';
import CustomAlertComponent from '../../components/multiPlatform/customAlert/CustomAlertComponent';
import Spinner from 'react-native-loading-spinner-overlay';
import {detailUser} from '../../actions/account/changeProfile';
import {getPaymentHistory} from '../../actions/home';
import { refreshToken } from '../../actions';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import Toast, {DURATION} from 'react-native-easy-toast';
import analytics from '@react-native-firebase/analytics';
import {styles} from './style';
import {jsonParse, handleRefreshToken} from '../../utils/utilization';
import { MESSAGE_TOKEN_EXP, TOKEN_EXP } from '../../utils/constant';

class PaymentHistory extends Component {
  constructor(props) {
    super();
    this.state = {
      imageBanner: '',
      aggrementNo: false,
      lobCode: false,
      arrParams: [],
      arrPaymentData: [],
      arrData: false,

      detailUser: false,
      isConnected: false,

      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: 'success',
      alertDoneText: '',
      alertCancelText: '',
      userToken: '',
    };
  }

  initApi = async () => {
    const {getPaymentHistory} = this.props;
    const jsonUserData = await AsyncStorage.getItem('userData');
    const userToken = await AsyncStorage.getItem('userToken');
    let arrData = this.props.route.params;
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        this.setState({arrData: arrData});
        this.setState({userToken: userToken});
        getPaymentHistory({AgrNo: arrData.AgrNo}, userToken);
      } else {
        this.showToastNoInternet();
      }
    });
  };

  componentDidMount = async () => {
    const {getPaymentHistoryResult} = this.props;

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this.checkInternet();
    analytics().logEvent('eventPaymentHistory', {
      item: 'user trying to see payment history',
    });
    this.setState({
      arrParams: this.props.route.params,
      lobCode: this.props.route.params.LOBCode,
    });
    const detailUserNonParse = await AsyncStorage.getItem('detailUser');
    const userDataNonParse = await AsyncStorage.getItem('userData');

    if (userDataNonParse) {
      const userDataParse = JSON.parse(userDataNonParse);
      this.setState({userData: userDataParse});
    }
    if (detailUserNonParse) {
      const detailUserParse = JSON.parse(detailUserNonParse);
      this.setState({detailUser: detailUserParse});
    }

    this.initApi();
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      detailUserResult,
      getPaymentHistoryResult,
      getPaymentHistoryError,
      refreshTokenResult, 
      refreshTokenError
    } = this.props;

    if (detailUserResult && prevProps.detailUserResult !== detailUserResult) {
      this.setState({detailUser: detailUserResult});
    }

    if (
      getPaymentHistoryError &&
      prevProps.getPaymentHistoryError !== getPaymentHistoryError
    ) {
      // console.log("getPaymentHistoryError Error ", getPaymentHistoryError)
      const data = await jsonParse(getPaymentHistoryError.message); //di sini ngeparse sekaligus ngecek apakah data kiriman error berupa json
      if (data) {
        if (data.message == MESSAGE_TOKEN_EXP ) {
          this.props.refreshToken(this.state.userToken);
        }
      } else {
        this.setState({
          alertShowed: true,
          alertMessage: getPaymentHistoryError.message,
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
          arrPaymentData: [],
        });
      }
    }

    if (
      getPaymentHistoryResult.AllData &&
      prevProps.getPaymentHistoryResult !== getPaymentHistoryResult
    ) {
      this.setState({arrPaymentData: getPaymentHistoryResult.AllData});
    }

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      console.log('Error Refresh Token ', refreshTokenError.message);
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      await handleRefreshToken(refreshTokenResult.Refreshtoken);
      this.setState({ userToken: refreshTokenResult.Refreshtoken });
      this.props.getPaymentHistory(
        {AgrNo: this.state.arrData.AgrNo},
        refreshTokenResult.Refreshtoken,
      );
    }
  };

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.goBack();
      return true;
    }
  };

  goBack = async () => {
    this.props.navigation.goBack();
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
        onPressNegativeButton={this.handleNegativeButtonAlert}
      />
    );
  };

  handlePositiveButtonAlert = () => {
    if (this.state.alertFor === 'showModal') {
      this.setState({alertShowed: false, alertFor: false});
      this.showModal();
    } else {
      this.setState({alertShowed: false, alertFor: false});
    }
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false, alertFor: false});
  };

  render() {
    const {getPaymentHistoryLoading, getPaymentHistoryError} = this.props;

    return (
      <Container>
        {Platform.OS == 'android' ? (
          <SubHeaderAndroid title="History Pembayaran" goBack={this.goBack} />
        ) : (
          <SubHeaderIos title="History Pembayaran" goBack={this.goBack} />
        )}
        <Content>
          <View>
            <Card>
              <CardItem style={styles.cardItem}>
                <Text style={styles.title}>
                  {this.state.lobCode == 'C01'
                    ? 'Pembiayaan Motor Yamaha'
                    : this.state.lobCode == 'C03'
                    ? 'Flexy'
                    : this.state.lobCode == 'C05'
                    ? 'Pembiayaan Motor Bekas Yamaha'
                    : this.state.lobCode == 'C08'
                    ? 'Pembiayaan Mobil'
                    : this.state.lobCode == 'C09'
                    ? 'Pembiayaan Peralatan Berat'
                    : this.state.lobCode == 'C15'
                    ? 'Pembiayaan Multiproduct'
                    : this.state.lobCode == 'S01'
                    ? 'Pembiayaan Motor Syariah'
                    : this.state.lobCode == 'S05'
                    ? 'Pembiayaan Motor Bekas Syariah'
                    : this.state.lobCode == 'S15'
                    ? 'Dana Syariah'
                    : '-'}
                </Text>
              </CardItem>
            </Card>
          </View>
          <View style={styles.cardWrapper}>
            <Card style={styles.card}>
              <Row style={styles.row}>
                <Left>
                  <Text style={styles.titleAngsuran}>Angsuran Ke</Text>
                </Left>
                <Body>
                  <Text style={styles.titleDefault}>Jatuh Tempo</Text>
                </Body>
                <Right>
                  <Text style={styles.titleDefault}>Tanggal Bayar</Text>
                </Right>
              </Row>
              <View style={styles.borderLine} />
              <View style={styles.contentWrapper}>
                {getPaymentHistoryError ? (
                  <Row>
                    <Text style={styles.textDefault}>Data Not Found</Text>
                  </Row>
                ) : (
                  this.state.arrPaymentData.map((rows) => {
                    return (
                      <Row style={styles.row2}>
                        <Left>
                          <Text style={styles.textAngsuranKe}>
                            {rows.InstSeqNo}
                          </Text>
                        </Left>
                        <Body style={{paddingHorizontal: 4}}>
                          <Text style={styles.textJatuhTempo}>
                            {rows.TglJatuhTempo}
                          </Text>
                        </Body>
                        <Right>
                          <Text style={styles.textBayarAngsuran}>
                            {!rows.TglBayarAngsuran
                              ? '–'
                              : rows.TglBayarAngsuran}
                          </Text>
                        </Right>
                      </Row>
                    );
                  })
                )}
              </View>
            </Card>
          </View>
          <Spinner
            visible={getPaymentHistoryLoading ? true : false}
            textContent={'Sedang memproses...'}
            overlayColor="rgba(0, 0, 0, 0.80)"
            textStyle={{color: '#FFF'}}
          />

          <Toast
            ref="toast"
            style={{
              marginTop:
                Platform.OS != 'android'
                  ? Dimensions.get('window').width * PixelRatio.get() > 750
                    ? 44
                    : 20
                  : 0,
              backgroundColor: '#FFE6E9',
              width: '100%',
              borderBottomWidth: 2,
              borderBottomColor: 'red',
              justifyContent: 'center',
              paddingLeft: 50,
              height: 50,
              borderRadius: 0,
            }}
            position="top"
            positionValue={0}
            fadeInDuration={2000}
            fadeOutDuration={1000}
            opacity={0.9}
          />
          {this.showAlert()}
        </Content>
      </Container>
    );
  }
}
function mapStateToProps(state) {
  return {
    detailUserResult: state.detailUser.result,
    detailUserLoading: state.detailUser.loading,
    detailUserError: state.detailUser.error,
    getPaymentHistoryResult: state.getPaymentHistory.result,
    getPaymentHistoryError: state.getPaymentHistory.error,
    getPaymentHistoryLoading: state.getPaymentHistory.loading,
    refreshTokenResult: state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      detailUser,
      getPaymentHistory,
      refreshToken,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, matchDispatchToProps)(PaymentHistory);
