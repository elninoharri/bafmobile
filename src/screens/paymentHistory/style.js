import {StyleSheet, Dimensions, PixelRatio, Platform} from 'react-native';
import {fonts} from '../../utils/fonts';
const {width} = Dimensions.get('window');

export const styles = StyleSheet.create({
  cardItem: {alignSelf: 'center', textAlign: 'center'},
  title: {
    fontSize: 14,
    color: '#72777B',
    fontFamily: fonts.primary.normal,
    textAlign: 'center',
  },
  cardWrapper: {padding: 10},
  card: {paddingTop: 10},
  row: {
    alignSelf: 'center',
    height: 35,
    paddingHorizontal: 15,
  },
  titleAngsuran: {
    fontFamily: fonts.primary.bold,
    color: 'rgba(0, 0, 0, 0.5)',
    fontSize: 16,
    maxWidth: 71,
    textAlign: 'center',
  },
  titleDefault: {
    fontFamily: fonts.primary.bold,
    fontSize: 16,
    alignSelf: 'center',
    textAlign: 'center',
    color: 'rgba(0, 0, 0, 0.5)',
  },
  titleJatuhTempo: {
    fontFamily: fonts.primary.bold,
    fontSize: 16,
    alignSelf: 'center',
    textAlign: 'center',
    color: 'rgba(0, 0, 0, 0.5)',
    marginRight: 20,
  },
  borderLine: {
    borderBottomColor: 'rgba(0, 0, 0, 0.2)',
    borderBottomWidth: 1,
    marginTop: 10,
  },
  contentWrapper: {
    backgroundColor: '#EFEFEF',
    paddingLeft: 20,
  },
  textDefault: {
    fontFamily: fonts.primary.bold,
    fontSize: 16,
    color: 'rgba(0, 0, 0, 0.5)',
    alignSelf: 'center',
    textAlign: 'center',
  },
  textJatuhTempo: {
    fontSize: width > 320 ? 16 : 14,
    color: '#EEAF30',
    textAlign: 'center',
    alignSelf: 'center',
    marginRight: 20,
    width: 200,
    fontFamily: fonts.primary.normal,
  },
  textBayarAngsuran: {
    fontSize: width > 320 ? 16 : 14,
    color: '#00091a',
    fontFamily: fonts.primary.normal,
    textAlign: 'center',
    alignSelf: 'center',
    width: 200,
    marginRight: 20,
  },
  textAngsuranKe: {
    fontSize: width > 320 ? 16 : 14,
    color: '#00091a',
    fontFamily: fonts.primary.normal,
  },
  row2: {
    alignSelf: 'center',
    height: 35,
    paddingHorizontal: 5,
  },
});
