import {StyleSheet} from 'react-native';
import {fonts} from '../../utils/fonts';

export const styles = StyleSheet.create({
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  image: {
    height: '60%',
    marginTop: '-15%',
    resizeMode: 'contain',
  },
  text: {
    fontSize: 14,
    fontFamily: fonts.primary.normal,
    color: '#5B5B5B',
    textAlign: 'center',
    marginTop: 10,
    marginHorizontal: '10%'
  },
  title: {
    fontSize: 22,
    fontFamily: fonts.primary.normal,
    fontWeight: '700',
    color: '#002F5F',
    textAlign: 'center',
    marginTop: 20,
  },
  buttonCustom: {
    width: '80%',
    height: 40,
    backgroundColor: '#002F5F',
    borderRadius: 4,
    marginTop: 40,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  next: {
    color: 'white',
    fontSize: 14,
    fontFamily: fonts.primary.normal,
    fontWeight: 'bold'
  },
  dot: {
    width: 15,
    height: 7,
    marginBottom: '145%',
    backgroundColor: '#C3C3C3',
  },
  dotActive: {
    marginBottom: '145%',
    width: 25,
    height: 7,
    backgroundColor: '#002F5F',
  },
});
