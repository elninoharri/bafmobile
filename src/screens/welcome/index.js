/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, StatusBar, TouchableOpacity, BackHandler} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import AppIntroSlider from 'react-native-app-intro-slider';
import {styles} from './style';
import NetInfo from "@react-native-community/netinfo";
import JailMonkey from 'jail-monkey'

const slides = [
  {
    key: 0,
    title: 'Praktis',
    text:
      'Kini pengajuan kredit dan bayar angsuran dapat dilakukan kapan saja dan dimana saja melalui BAF Mobile',
    image: require('../../../assets/img/welcome/welcome1.png'),
    backgroundColor: '#ffffff',
  },
  {
    key: 1,
    title: 'Selalu Setia',
    text:
      'Tak perlu khawatir lupa tentang detail kontrak pembiayaan karena kami akan selalu setia mengingatkan Anda',
    image: require('../../../assets/img/welcome/welcome2.png'),
    backgroundColor: '#ffffff',
  },
  {
    key: 2,
    title: 'Promo Menarik',
    text:
      'Dapatkan beragam promo dan rewards menarik khusus untuk pengguna BAF Mobile. Nantikan program setiap bulanya.',
    image: require('../../../assets/img/welcome/welcome3.png'),
    backgroundColor: '#ffffff',
  },
];

class Welcome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Welcome',
    };
  }

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      BackHandler.exitApp()
      return true;
    }
  }

  componentDidMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        if (JailMonkey.isJailBroken()) {
          this.handleJail
        } else if (JailMonkey.canMockLocation()) {
          this.handleJail
        } else if (JailMonkey.trustFall()) {
          this.handleJail
        } else if (JailMonkey.hookDetected()) {
          this.handleJail
        } 
        // else if (JailMonkey.isDebuggedMode()) {
        //  this.handleJail
        // } 
        else {

        }
      } else {
        Alert.alert(
          'Informasi',
          'Koneksi bermasalah',
          [
            {text: 'OK', onPress: () => {
              this.props.navigation.replace('Login')
            }},
          ],
        );
      }
    })
  };

  handleJail = () => {
    Alert.alert(
      'Informasi',
      'Sepertinya dalam perangkat anda terdeteksi bahaya.',
      [
        {text: 'OK', onPress: () => 
          BackHandler.exitApp()
        },
      ],
      {cancelable: false},
    );
  }
  
  componentDidUpdate = async () => {};

  _renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <Image style={styles.image} source={item.image} />
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.text}>{item.text}</Text>
        <TouchableOpacity
          onPress={(e) => this._okPressed(e, item)}
          style={styles.buttonCustom}>
          <Text style={styles.next}>{item.key == 2 ? 'Lanjut' : 'Lewati'}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  _okPressed = (e, item) => {
    if (item.key == 2) {
      this.doneHandler();
    }
    this.AppIntroSlider.goToSlide(item.key + 1);
  };

  doneHandler = () => {
    AsyncStorage.setItem('firstInstall', 'alreadyInstall');
    this.props.navigation.replace('Home');
  };

  _renderNextButton = () => {
    return (
      <View style={{opacity:0}}>
      </View>
    );
  };
  _renderDoneButton = () => {
    return (
      <View style={{opacity:0}}>
      </View>
    );
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="white" barStyle="dark-content"/>
        <AppIntroSlider
          renderItem={this._renderItem}
          ref={(ref) => (this.AppIntroSlider = ref)}
          data={slides}
          dotStyle={styles.dot}
          activeDotStyle={styles.dotActive}
          renderDoneButton={this._renderDoneButton}
          renderNextButton={this._renderNextButton}
        />
      </View>
    );
  }
}

function mapStateToProps() {
  return {};
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Welcome);
