import React, {Component} from 'react';
import {styles} from './style';
import moment from 'moment';
import {
  TouchableOpacity,
  Text,
  View,
  SafeAreaView,
  Platform,
  Image,
} from 'react-native';
import {
  Container,
  Input,
  Item,
  Label,
  Header,
  Left,
  Right,
  Button,
  Body,
  Title,
  Icon,
  Picker,
  Card,
  Content,
} from 'native-base';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {reduxForm, Field, change, reset, formValueSelector} from 'redux-form';

import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';

import Toast from 'react-native-easy-toast';
import {
  BAF_COLOR_BLUE,
  BG_COLOR,
  BORDER_COLOR,
  MESSAGE_TOKEN_EXP,
  TOKEN_EXP,
  MESSAGE_TOKEN_INVALID,
} from '../../../utils/constant';
import {
  substringCircum,
  substringSecondCircum,
  formatCurrency,
  normalizeCurrency,
  fdeFormatCurrency,
  jsonParse,
  handleRefreshToken,
} from '../../../utils/utilization';

import {fonts} from '../../../utils/fonts';
import {FormSimulationCarValidate} from '../../../validates/FormSimulationCarValidate.js';

import CustomAlertComponent from '../../../components/multiPlatform/customAlert/CustomAlertComponent';
import Spinner from 'react-native-loading-spinner-overlay';
import car from './car.json';
import analytics from '@react-native-firebase/analytics';

import {getPengajuanCar, getOTR} from '../../../actions/lob';
import {refreshToken} from '../../../actions';

export const renderField = ({
  input,
  type,
  label,
  editable,
  keyboardType,
  maxLength,
  multiline,
  placeholder,
  numberOfLines,
  format,
  normalize,
  customError,
  meta: {touched, error},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '80%', marginTop: 15, alignSelf: 'center'}}>
      <Label
        style={{
          fontSize: 16,
          color: hasError ? 'red' : 'black',
          alignSelf: 'center',
          marginBottom: 5,
        }}>
        {label}
      </Label>
      <Item
        regular
        style={{
          backgroundColor: 'white',
          height: 40,
          borderColor: customError || hasError ? 'red' : BAF_COLOR_BLUE,
          borderWidth: 1,
          borderRadius: 5,
          paddingRight: 10,
        }}>
        <Input
          style={{
            fontSize: 14,
            paddingLeft: '4%',
            color: editable ? 'black' : '#aaa',
            fontFamily: fonts.primary.normal,
          }}
          {...input}
          type={type}
          placeholder={placeholder}
          editable={editable}
          keyboardType={keyboardType}
          maxLength={maxLength}
          multiline={multiline}
          numberOfLines={numberOfLines}
          format={format}
          normalize={normalize}
        />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderFieldHidden = ({
  input,
  type,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '100%'}}>
      <Item style={{display: 'none'}}>
        <Input {...input} ype={type} />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

export const renderFieldPicker = ({
  input,
  label,
  pickerSelected,
  onValueChange,
  customError,
  enabled,
  data,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <SafeAreaView style={{width: '80%', marginTop: 15, alignSelf: 'center'}}>
      <Label
        style={{
          fontSize: 16,
          color: customError || hasError ? 'red' : 'black',
          alignSelf: 'center',
          marginBottom: 5,
          fontFamily: fonts.primary.normal,
        }}>
        {label}
      </Label>
      <View
        style={{
          backgroundColor: 'white',
          height: 40,
          borderColor:
            customError || hasError
              ? 'red'
              : pickerSelected
              ? BAF_COLOR_BLUE
              : BORDER_COLOR,
          borderWidth: 1,
          borderRadius: 5,
          paddingRight: 10,
        }}>
        <Picker
          {...input}
          renderHeader={(backAction) => (
            <Header>
              <Left>
                <Button transparent onPress={backAction}>
                  <Icon name="arrow-back" style={{color: 'black'}} />
                </Button>
              </Left>
              <Body style={{flex: 3}}>
                <Title>{label}</Title>
              </Body>
              <Right />
            </Header>
          )}
          note={enabled ? false : true}
          mode="dropdown"
          style={{
            marginTop: Platform.OS == 'android' ? -6 : -4,
            fontSize: 14,
            width: '100%',
          }}
          enabled={enabled}
          selectedValue={pickerSelected}
          placeholder="Silahkan Pilih"
          onValueChange={onValueChange}>
          {data}
        </Picker>
      </View>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </SafeAreaView>
  );
};

export const renderFieldDisabled = ({
  input,
  type,
  label,
  editable,
  placeholder,
  format,
  normalize,
  customError,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }
  return (
    <View style={{width: '80%', marginTop: 15, alignSelf: 'center'}}>
      <Label
        style={{
          fontSize: 16,
          color: hasError ? 'red' : 'black',
          alignSelf: 'center',
          marginBottom: 5,
        }}>
        {label}
      </Label>
      <Item
        regular
        style={{
          backgroundColor: BG_COLOR,
          height: 40,
          borderColor: customError || hasError ? 'red' : BG_COLOR,
          borderWidth: 1,
          borderRadius: 5,
          paddingRight: 10,
        }}>
        <Input
          style={{
            fontSize: 14,
            paddingLeft: '4%',
            color: 'black',
          }}
          {...input}
          placeholder={placeholder}
          type={type}
          editable={false}
          format={format}
          normalize={normalize}
        />
      </Item>
      {hasError ? <Text style={styles.ErrorDesc}>{error}</Text> : null}
    </View>
  );
};

class CarSimulation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenTitle: 'Kredit Mobil Baru',
      isConnected: false,
      LOB: 'CAR',
      pickerLobItem: [
        {Code: 'Motor Yamaha'},
        {Code: 'Mobil Baru'},
        {Code: 'Multiproduk'},
        {Code: 'Dana Syariah'},
      ],
      pickerLobSelected: 'Mobil Baru',
      pickerJenisItem: [{Code: '', Descr: 'Pilih'}],
      pickerJenisSelected: '',
      pickerMerkItem: [{Code: '', Descr: 'Pilih'}],
      pickerMerkSelected: '',
      pickerTenorItem: [{Code: '', Descr: 'Pilih'}],
      pickerTenorSelected: '',
      pickerDpItem: [{Code: '', Descr: 'Pilih'}],
      pickerDpSelected: '',
      enableDpTerm: false,
      showResultSimulation: false,
      calculateResult: false,
      detailUser: false,
      userData: false,
      alertFor: null,
      alertShowed: false,
      alertMessage: '',
      alertTitle: '',
      alertType: '',
      alertDoneText: '',
      alertCancelText: '',
      userToken: '',
      dataMerge: '',
    };
  }

  componentDidMount = async () => {
    analytics().logScreenView({
      screen_class: 'screenCarSimulation',
      screen_name: 'screenCarSimulation',
    });
    this.checkInternet();
    const userToken = await AsyncStorage.getItem('userToken');
    if (userToken) {
      this.setState({userToken: userToken});
    }

    const detailUserNonParse = await AsyncStorage.getItem('detailUser');
    if (detailUserNonParse) {
      const detailUserParse = JSON.parse(detailUserNonParse);
      this.setState({detailUser: detailUserParse});
    }
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {
      getPengajuanCarResult,
      detailUserResult,
      getPengajuanCarError,
      getOTR_Result,
      getOTR_Error,
      getOTR_Loading,
      refreshTokenResult,
      refreshTokenError,
    } = this.props;
    const {dataMerge} = this.state;

    if (detailUserResult && prevProps.detailUserResult !== detailUserResult) {
      this.setState({detailUser: detailUserResult});
    }

    if (
      getPengajuanCarError &&
      prevProps.getPengajuanCarError !== getPengajuanCarError
    ) {
      const data = await jsonParse(getPengajuanCarError.message);
      if (data) {
        console.log(data.message);
        if (data.message == MESSAGE_TOKEN_EXP) {
          this.props.refreshToken(this.state.userToken);
          // this.handleInvalid();
        } else if (data.message == MESSAGE_TOKEN_INVALID) {
          this.handleInvalid();
        } else {
          this.setState({
            alertFor: 'getPengajuanCarError',
            alertShowed: true,
            alertMessage:
              'Terjadi kesalahan pada sistem, harap coba beberapa saat lagi.',
            alertTitle: 'Gagal',
            alertType: 'error',
            alertDoneText: 'OK',
          });
        }
      } else {
        this.setState({
          alertFor: 'getPengajuanCarError',
          alertShowed: true,
          alertMessage:
            'Terjadi kesalahan pada sistem, harap coba beberapa saat lagi.',
          alertTitle: 'Gagal',
          alertType: 'error',
          alertDoneText: 'OK',
        });
      }
    }

    if (
      getPengajuanCarResult &&
      prevProps.getPengajuanCarResult !== getPengajuanCarResult
    ) {
      this.setState({
        alertFor: 'getPengajuanCarResult',
        alertShowed: true,
        alertMessage: 'Data anda akan kami proses',
        alertTitle: 'Berhasil',
        alertType: 'success',
        alertDoneText: 'OK',
      });
    }

    if (getOTR_Loading && prevProps.getOTR_Loading !== getOTR_Loading) {
    }

    if (getOTR_Error && prevProps.getOTR_Error !== getOTR_Error) {
      console.log(getOTR_Error);
      this.props.updateField('formCarProduct', 'hargaBarang', '');
    }

    if (getOTR_Result && prevProps.getOTR_Result !== getOTR_Result) {
      console.log(getOTR_Result);
      if (getOTR_Result.LOBCode == this.state.LOB) {
        this.props.updateField(
          'formCarProduct',
          'hargaBarang',
          getOTR_Result.OTRPrice,
        );
      }
      this.setState({enableDpTerm: true});
    }

    if (
      refreshTokenError !== null &&
      prevProps.refreshTokenError !== refreshTokenError
    ) {
      console.log('Error Refresh Token ', refreshTokenError.message);
    }

    if (
      refreshTokenResult !== null &&
      prevProps.refreshTokenResult !== refreshTokenResult
    ) {
      if (getPengajuanCarError) {
        await handleRefreshToken(refreshTokenResult.Refreshtoken);
        this.setState({userToken: refreshTokenResult.Refreshtoken});
        this.props.getPengajuanCar(
          {
            DateReq: dataMerge.DateReq,
            DP: dataMerge.DP,
            JenisBrg: dataMerge.JenisBrg,
            Pinjaman: dataMerge.Pinjaman,
            PromoCode: dataMerge.PromoCode,
            Source: dataMerge.Source,
            Tenor: dataMerge.Tenor,
            TipePengajuan: dataMerge.TipePengajuan,
          },
          refreshTokenResult.Refreshtoken,
        );
      }
    }
  };

  handleInvalid = async () => {
    await AsyncStorage.removeItem('userToken');
    await AsyncStorage.removeItem('userData');
    await AsyncStorage.removeItem('detailUser');
    await AsyncStorage.removeItem('activeAgreement');
    this.setState({
      alertFor: 'invalidToken',
      alertShowed: true,
      alertMessage: 'Sesi login Anda sudah habis',
      alertTitle: 'Informasi',
      alertType: 'info',
      alertDoneText: 'OK',
    });
  };

  checkInternet = () => {
    NetInfo.fetch().then((state) => {
      this.setState({isConnected: state.isConnected}); // set state isConnected based on result
      if (!this.state.isConnected) {
        this.showToastNoInternet();
      }
    });
  };

  showToastNoInternet = () => {
    this.refs.toast.show(
      <View style={{flexDirection: 'row'}}>
        <Icon
          type="FontAwesome"
          name="exclamation-circle"
          style={{color: 'red', marginLeft: '-10%'}}
        />
        <Text
          style={{
            marginTop: Platform.OS == 'android' ? '1%' : '1.5%',
            marginLeft: '3%',
            fontSize: 16,
            color: 'red',
          }}>
          Tidak Ada Koneksi Internet
        </Text>
      </View>,
      DURATION.LENGTH_LONG,
    );
  };

  handleBackButton = () => {
    this.goBack();
    return true;
  };

  goBack = async () => {
    this.props.resetForm('formCarProduct');
    this.props.navigation.goBack();
  };

  pickerLobChange = async (data) => {
    const {resetForm, navigation} = this.props;
    this.setState({
      pickerMerkSelected: '',
      pickerDpSelected: '',
      calculateResult: false,
      showResultSimulation: false,
    });
    await resetForm('formCarProduct');
    switch (data) {
      case 'Motor Yamaha':
        navigation.replace('NmcSimulation');
        break;
      case 'Multiproduk':
        navigation.replace('MultiproductSimulation');
        break;
      case 'Mobil Baru':
        navigation.replace('CarSimulation');
        break;
      case 'Dana Syariah':
        navigation.replace('SyanaSimulation');
        break;
      default:
        navigation.replace('CarProduct');
    }
  };

  onMerkChange = (value) => {
    const {getOTR} = this.props;
    const {LOB} = this.state;
    this.setState({pickerMerkSelected: value});
    this.props.updateField('formCarProduct', 'merk', value);
    this.resetField();
    getOTR({
      LobCode: LOB,
      MerkCode: value,
      TipeCode: '',
      ManufactureYear: '',
    });
  };

  onHargaChange = (value) => {
    this.setState({showResultSimulation: false});
    this.resetField();
  };

  onTenorChange = (value) => {
    this.setState({
      pickerTenorSelected: value,
      showResultSimulation: false,
    });
    this.props.updateField('formCarProduct', 'tenor', value);
  };

  onDpChange = (value) => {
    const {formCarProduct} = this.props;

    this.setState({
      pickerDpSelected: value,
      showResultSimulation: false,
    });
    var uangMukaAmount = '';
    this.props.updateField('formCarProduct', 'uangMuka', value);

    if (!value || value == '') {
      this.props.updateField(
        'formCarProduct',
        'uangMukaAmount',
        uangMukaAmount,
      );
    } else {
      if (formCarProduct.values.hargaBarang > 0) {
        uangMukaAmount =
          parseFloat(value) * parseFloat(formCarProduct.values.hargaBarang);
        this.props.updateField(
          'formCarProduct',
          'uangMukaAmount',
          uangMukaAmount.toString(),
        );
      } else {
        this.props.updateField('formCarProduct', 'uangMukaAmount', '0');
      }
    }
  };

  resetField = () => {
    this.setState({
      pickerTenorSelected: '',
      pickerDpSelected: '',
    });
    this.props.updateField('formCarProduct', 'tenor', '');
    this.props.updateField('formCarProduct', 'uangMuka', '');
    this.props.updateField('formCarProduct', 'uangMukaAmount', '');
    this.setState({showResultSimulation: false});
  };

  submitCalculate = async (data) => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        var HargaBarang = parseFloat(data.hargaBarang);
        var UangMukaAmount = parseFloat(data.uangMukaAmount);
        var Tenor = substringCircum(data.tenor);
        var Rate = substringSecondCircum(data.tenor);
        var calculateResult =
          ((HargaBarang - UangMukaAmount) * (1 + parseFloat(Rate))) /
          parseFloat(Tenor);
        this.setState({
          showResultSimulation: true,
          calculateResult: Math.round(calculateResult),
        });
      } else {
        this.showToastNoInternet();
      }
    });
  };

  submitData = async (data) => {
    const userToken = await AsyncStorage.getItem('userToken');
    const {detailUser, LOB} = this.state;
    const {navigation, getPengajuanCar} = this.props;
    NetInfo.fetch().then(async (state) => {
      if (state.isConnected) {
        if (!detailUser) {
          navigation.navigate('Login');
          // this.handleInvalid();
          // Ini gak make sense dikasih handle invalid, tujuannya untuk user bisa simulasi walau dia gak login
          // Kalo dikasih ini, seakan user udah login dan sesinya sudah habis.
        } else {
          analytics().logEvent('eventCarSubmit', {
            item: 'user trying to submit Car simulation',
          });
          var dataMerge = {
            Source: 'BAF Mobile',
            JenisBrg: substringCircum(data.merk),
            TipePengajuan: LOB,
            Pinjaman: 'Rp. ' + data.hargaBarang,
            DP: data.uangMuka * 100 + '%',
            Tenor: substringCircum(data.tenor),
            DateReq: moment().format('YYYY-MM-DD hh:mm:ss'),
            PromoCode: '-',
          };

          getPengajuanCar(
            {
              DateReq: dataMerge.DateReq,
              DP: dataMerge.DP,
              JenisBrg: dataMerge.JenisBrg,
              Pinjaman: dataMerge.Pinjaman,
              PromoCode: dataMerge.PromoCode,
              Source: dataMerge.Source,
              Tenor: dataMerge.Tenor,
              TipePengajuan: dataMerge.TipePengajuan,
            },
            userToken,
          );
          this.setState({dataMerge: dataMerge});
        }
      } else {
        this.showToastNoInternet();
      }
    });
  };

  showAlert = () => {
    return (
      <CustomAlertComponent
        displayAlert={this.state.alertShowed}
        displayAlertIcon={true}
        alertType={this.state.alertType}
        alertTitleText={this.state.alertTitle}
        alertMessageText={this.state.alertMessage}
        displayPositiveButton={true}
        positiveButtonText={this.state.alertDoneText}
        displayNegativeButton={
          this.state.alertType === 'confirm' ? true : false
        }
        negativeButtonText={this.state.alertCancelText}
        onPressPositiveButton={this.handlePositiveButtonAlert}
        onPressNegativeButton={this.handleNegativeButtonAlert}
      />
    );
  };

  handlePositiveButtonAlert = () => {
    if (this.state.alertFor === 'showModal') {
      this.setState({alertShowed: false, alertFor: false});
      this.showModal();
    } else if (
      this.state.alertFor === 'getPengajuanCarResult' ||
      this.state.alertFor === 'getPengajuanCarError'
    ) {
      this.setState({alertShowed: false, alertFor: false});
      this.props.navigation.navigate('Home');
    } else if (this.state.alertFor === 'invalidToken') {
      this.setState({alertShowed: false, alertFor: false});
      this.props.navigation.navigate('Login');
    } else {
      this.setState({alertShowed: false, alertFor: false});
    }
  };

  handleNegativeButtonAlert = () => {
    this.setState({alertShowed: false, alertFor: false});
  };

  showIosLoadingSpinnerCar = () => {
    return (
      <View
        style={{
          backgroundColor: 'grey',
          opacity: 0.5,
          position: 'absolute',
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={require('../../../../assets/img/loading.gif')}
          style={{width: 150, height: 150}}
        />
      </View>
    );
  };

  render = () => {
    const {
      pickerLobItem,
      pickerLobSelected,
      pickerMerkSelected,
      pickerTenorSelected,
      pickerDpSelected,
      showResultSimulation,
      calculateResult,
    } = this.state;

    const {
      handleSubmit,
      submitting,
      getPengajuanCarLoading,
      getOTR_Loading,
    } = this.props;

    const carTipe = car.Tipe;
    const carDP = car.DP;
    const carTenor = car.Tenor;

    return (
      <Container>
        {Platform.OS === 'android' ? (
          <Spinner
            visible={getPengajuanCarLoading || getOTR_Loading ? true : false}
            textContent={'Sedang memproses...'}
            textStyle={{color: '#FFF'}}
          />
        ) : null}

        <Toast
          ref="toast"
          style={styles.toastContainer}
          position="top"
          positionValue={0}
          fadeInDuration={2000}
          fadeOutDuration={1000}
          opacity={0.9}
        />

        <Content disableKBDismissScroll={false}>
          <Field
            name="merk"
            component={renderFieldPicker}
            enabled={true}
            label="Tipe Mobil"
            customError={this.state.errorMerkCarProduct}
            pickerSelected={pickerMerkSelected}
            data={carTipe.map((pickerMerkItem, index) => (
              <Picker.Item
                label={pickerMerkItem.Descr}
                value={pickerMerkItem.Code}
              />
            ))}
            onValueChange={(newValue) => this.onMerkChange(newValue)}
          />
          <Field
            name="hargaBarang"
            label="Harga Mobil"
            type="text"
            placeholder="Rp. 0"
            editable={true}
            component={renderField}
            format={fdeFormatCurrency}
            normalize={normalizeCurrency}
            keyboardType={'number-pad'}
            customError={this.state.errorHargaBarang}
            onChange={(value) => this.onHargaChange(value)}
          />

          <View style={styles.pickTenorWrapper}>
            <View style={{width: '49%'}}>
              <Field
                name="tenor"
                component={renderFieldPicker}
                label="Tenor"
                enabled={true}
                pickerSelected={pickerTenorSelected}
                data={carTenor.map((pickerTenorItem, index) => (
                  <Picker.Item
                    label={pickerTenorItem.Descr}
                    value={pickerTenorItem.Code}
                  />
                ))}
                onValueChange={(newValue) => this.onTenorChange(newValue)}
              />
            </View>

            <View style={{width: '49%'}}>
              <Field
                name="uangMuka"
                component={renderFieldPicker}
                label="Uang Muka"
                enabled={true}
                pickerSelected={pickerDpSelected}
                data={carDP.map((pickerDpItem, index) => (
                  <Picker.Item
                    label={pickerDpItem.Descr}
                    value={pickerDpItem.Code}
                  />
                ))}
                onValueChange={(newValue) => this.onDpChange(newValue)}
              />
            </View>
          </View>
          <Field
            name="uangMukaAmount"
            label="Nominal Uang Muka"
            type="text"
            editable={false}
            component={renderFieldHidden}
            format={formatCurrency}
            normalize={normalizeCurrency}
          />

          <TouchableOpacity
            style={styles.btnHitung}
            onPress={handleSubmit(this.submitCalculate)}
            disabled={submitting}>
            <Text style={styles.textButton}>Hitung Angsuran</Text>
          </TouchableOpacity>
          <View style={{height: 10}} />
          {showResultSimulation ? (
            <Card
              style={{
                backgroundColor: '#f5f5f5',
                width: '85%',
                alignItems: 'center',
                alignSelf: 'center',
                borderRadius: 10,
                marginBottom: '5%',
                paddingBottom: 25,
              }}>
              <Text style={styles.textHeader}>
                Estimasi Angsuran Bulanan Anda
              </Text>
              <Text style={styles.textBelowHeader}>
                Rp.{' '}
                {calculateResult
                  ? formatCurrency(calculateResult.toString())
                  : '-'}
              </Text>
              <Text style={styles.textDesc}>/bulan</Text>
              <View style={{height: 15}}></View>
              <Text style={styles.textDesc}>
                Perhitungan hanya bersifat simulasi,
              </Text>
              <Text style={styles.textDesc}>
                dapat berubah sesuai regulasi BAF
              </Text>
              <Text style={styles.textDesc}>
                Ajukan Aplikasi pembiayaan dan
              </Text>
              <Text style={styles.textDesc}>kami akan menghubungi Anda</Text>
              <TouchableOpacity
                style={styles.btnAjukan}
                onPress={handleSubmit(this.submitData)}
                // onPress={this.AndroidBridgingExample}
              >
                <Text style={styles.textButton}>Ajukan Sekarang</Text>
              </TouchableOpacity>
            </Card>
          ) : null}
        </Content>
        {Platform.OS !== 'android'
          ? getOTR_Loading || getPengajuanCarLoading
            ? this.showIosLoadingSpinnerCar()
            : null
          : null}

        {this.showAlert()}
      </Container>
    );
  };
}

const selector = formValueSelector('formCarProduct');

function mapStateToProps(state) {
  return {
    merk: selector(state, 'merk'),
    hargaBarang: selector(state, 'hargaBarang'),
    formCarProduct: state.form.formCarProduct,
    detailUserResult: state.detailUser.result,
    getPengajuanCarResult: state.getPengajuanCar.result,
    getPengajuanCarError: state.getPengajuanCar.error,
    getPengajuanCarLoading: state.getPengajuanCar.loading,
    getOTR_Result: state.getOTR.result,
    getOTR_Error: state.getOTR.error,
    getOTR_Loading: state.getOTR.loading,
    refreshTokenResult: state.refreshToken.result,
    refreshTokenError: state.refreshToken.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getOTR,
      getPengajuanCar,
      refreshToken,
      updateField: (form, field, newValue) =>
        dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    },
    dispatch,
  );
}

CarSimulation = reduxForm({
  form: 'formCarProduct',
  enableReinitialize: true,
  validate: FormSimulationCarValidate,
})(CarSimulation);

export default connect(mapStateToProps, matchDispatchToProps)(CarSimulation);
