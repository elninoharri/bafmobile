import {StyleSheet, Dimensions, PixelRatio, Platform} from 'react-native';
import {fonts} from '../../../utils/fonts';
const {width} = Dimensions.get('window');

export const styles = StyleSheet.create({
  containerCb: {
    borderColor: '#002f5f',
    borderWidth: 1,
    height: 40,
    borderRadius: 7,
  },
  toastContainer: {
    marginTop:
      Platform.OS !== 'android'
        ? Dimensions.get('window').width * PixelRatio.get() > 750
          ? 44
          : 20
        : 0,
    backgroundColor: '#FFE6E9',
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'red',
    justifyContent: 'center',
    paddingLeft: 50,
    height: 50,
    borderRadius: 0,
  },
  symbol1: {
    marginLeft: '10%',
    // paddingLeft: '30%',
  },
  symbol2: {
    marginRight: '10%',
    paddingLeft: '-20%',
  },
  inputCb: {
    borderBottomColor: 'white',
    height: 40,
  },
  textCenter: {
    marginBottom: 5,
    fontSize: 18,
    textAlign: 'center',
    color: 'black',
    fontFamily: 'arial',
  },
  textTwin: {
    marginTop: -40,
    fontSize: 18,
    textAlign: 'center',
    color: 'black',
    fontFamily: 'arial',
  },
  textButton: {
    textAlign: 'center',
    fontSize: 14,
    color: 'white',
    fontFamily: fonts.primary.bold,
  },
  textHeader: {
    color: 'black',
    fontSize: 14,
    fontFamily: fonts.primary.bold,
    alignSelf: 'center',
    marginTop: 25,
  },
  textDesc: {
    fontSize: 12,
    color: 'black',
    fontFamily: fonts.primary.normal,
  },
  textBelowHeader: {
    color: '#1c1e61',
    fontSize: 20,
    fontFamily: fonts.primary.bold,
    alignSelf: 'center',
    marginVertical: 2,
  },
  pickTenorWrapper: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: '5%',
    alignSelf: 'center',
    justifyContent: 'space-between',
  },

  iconWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#f5f5f5',
    paddingHorizontal: '10%',
    paddingVertical: 5,
  },

  btnAjukan: {
    alignSelf: 'center',
    width: '60%',
    height: 40,
    borderRadius: 4,
    backgroundColor: '#002f5f',
    justifyContent: 'center',
    marginTop: '20%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },

  btnHitung: {
    alignSelf: 'center',
    width: '80%',
    height: 40,
    borderRadius: 5,
    backgroundColor: '#002f5f',
    justifyContent: 'center',
    marginTop: '5%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },

  btncekTenor: {
    alignSelf: 'center',
    width: '80%',
    height: 40,
    borderRadius: 5,
    backgroundColor: '#002f5f',
    justifyContent: 'center',
    marginTop: '5%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },

  centerCard: {
    alignSelf: 'center',
  },
  centerDropDown: {
    alignSelf: 'center',
  },

  imageTop: {
    height: 45,
    width: 45,
    resizeMode: 'contain',
  },
  dropDownUp: {
    width: '70%',
    borderRadius: 5,
    borderWidth: 0.5,
    backgroundColor: 'white',
    height: 40,
    paddingRight: 5,
  },
  pickerUp: {
    width: '100%',
    height: 40,
  },

  text: {
    fontSize: 50,
    textAlign: 'center',
  },

  ErrorDesc: {
    color: 'red',
    alignSelf: 'stretch',
    textAlign: 'right',
    fontSize: 10,
  },

  modalContainerLarge: {
    height: 210,
    backgroundColor: 'white',
    borderRadius: 10,
  },
  modalContainerSmall: {
    height: 185,
    backgroundColor: 'white',
    borderRadius: 10,
  },
});
