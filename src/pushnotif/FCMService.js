import messaging from '@react-native-firebase/messaging';
import {Platform} from 'react-native';

class FCMService {
  register = (onNotification, onOpenNotification) => {
    this.createNotificationListeners(onNotification, onOpenNotification);
  };

  registerToken = (onRegister) => {
    this.checkPermission(onRegister);
    this.createRegisterListeners(onRegister);
  };

  registerAppWithFCM = async () => {
    if (Platform.OS === 'android') {
      null;
    } else {
      await messaging().registerDeviceForRemoteMessages;
      await messaging().setAutoInitEnabled(true);
    }
  };

  checkPermission = (onRegister) => {
    messaging()
      .hasPermission()
      .then((enabled) => {
        if (enabled) {
          //user has permission
          this.getToken(onRegister);
        } else {
          //user doesn't have permission
          this.requestPermission(onRegister);
        }
      })
      .catch((error) => {
        console.log('[FCMService] Permission rejected ', error);
      });
  };

  getToken = (onRegister) => {
    messaging()
      .getToken()
      .then((fcmToken) => {
        if (fcmToken) {
          onRegister(fcmToken);
        } else {
          console.log('[FCMService] User does not have a device token');
        }
      })
      .catch((error) => {
        console.log('[FCMService] getToken rejected ', error);
      });
  };

  requestPermission = (onRegister) => {
    messaging()
      .requestPermission()
      .then(() => {
        this.getToken(onRegister);
      })
      .catch((error) => {
        console.log('[FCMService] Request Permission rejected ', error);
      });
  };

  deleteToken = () => {
    console.log('[FCMService] deleteToken ');
    messaging()
      .deleteToken()
      .catch((error) => {
        console.log('[FCMService] Delete token error ', error);
      });
  };

  createNotificationListeners = (onNotification, onOpenNotification) => {
    //when application running, but in the background
    messaging().onNotificationOpenedApp((remoteMessage) => {
      console.log(
        '[FCMService] onNotificationOpenedApp Notification caused app to open from background state: ',
        remoteMessage,
      );
      if (remoteMessage) {
        console.log('FCMbackground ', remoteMessage);
        const notification = remoteMessage;
        onOpenNotification(notification);
        //this.removeDeliveredNotification(notification.notificationId)
      }
    });

    // When the application is opened from  a quit state
    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        console.log(
          '[FCMService] getInitialNotification Notification caused app to open from quit state: ',
          remoteMessage,
        );

        if (remoteMessage) {
          console.log('FCM = ', remoteMessage);
          const notification = remoteMessage;
          onOpenNotification(notification);
          //this.removeDeliveredNotification(notification.notificationId)
        }
      });

    //Foreground state messages
    this.messageListener = messaging().onMessage(async (remoteMessage) => {
      if (remoteMessage) {
        // console.log(remoteMessage)
        let notification = null;
        notification = remoteMessage;

        onNotification(notification);
      }
    });
  };

  createRegisterListeners = (onRegister) => {
    //Triggered when have new token
    messaging().onTokenRefresh((fcmToken) => {
      console.log('[FCMService] new token refresh: ', fcmToken);
      onRegister(fcmToken);
    });
  };

  unRegister = () => {
    console.log('FCM unregister Sukses');
    // this.messageListener()
  };
}

export const fcmService = new FCMService();
