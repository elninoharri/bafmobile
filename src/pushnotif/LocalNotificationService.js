import {Platform} from 'react-native';
import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';

class LocalNotificationService {
  configure = (onOpenNotification) => {
    PushNotification.configure({
      onRegister: function (token) {
        console.log('[LocalNotification] onRegister:', token);
      },
      onNotification: function (notification) {
        // console.log("[LocalNotification] onNotification:", notification.data);
        if (!notification?.data) {
          return;
        }
        notification.userInteraction = true;
        console.log('Dari app : ', notification);
        onOpenNotification(
          Platform.OS === 'android'
            ? notification.data
            : notification.data.item,
        );

        //only call callback if not from foreground
        if (Platform.OS === 'android') {
          null;
        } else {
          // {required} Called when a remote is received or opened, or local notification is opened
          notification.finish(PushNotificationIOS.FetchResult.NoData);
        }
      },

      //IOS ONLY (optional): default: all - permission to register
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       * - if you are not using remote notification or do not have Firebase installed, use this:
       *     requestPermissions: Platform.OS === 'ios'
       */
      requestPermissions: true,
    });
  };

  unregister = () => {
    console.log(' Localpushnotif Unregister Sukses');
    PushNotification.unregister();
  };

  showNotification = (id, title, message, data = {}, option = {}) => {
    PushNotification.localNotification({
      //Android only properties
      ...this.buildAndroidNotification(id, title, message, data, option),
      //IOS and Android properties
      ...this.buildIOSNotification(id, title, message, data, option),
      //IOS and Android properties
      title: title || '',
      message: message || '',
      playSound: false,
      soundName: 'default',
      userInteraction: false, // Boolean :if the notification was opened  by the user from the notificat
    });
  };

  buildAndroidNotification = (id, title, message, data = {}, options = {}) => {
    return {
      id: id,
      autoCancel: true,
      largeIcon: options.largeIcon || 'ic_launcher',
      smallIcon: options.smallIcon || 'ic_notification',
      bigText: message || '',
      subText: title || '',
      vibrate: options.vibrate || true,
      vibration: options.vibration || 300,
      priority: options.priority || 'high',
      importance: options.importance || 'high', //optional set notification importance default : high
      data: data,
    };
  };

  buildIOSNotification = (id, title, message, data = {}, options = {}) => {
    return {
      alertAction: options.alertAction || 'view',
      category: options.category || '',
      userInfo: {
        id: id,
        data: data,
      },
    };
  };

  cancelAllNotifications = () => {
    if (Platform.OS === 'android') {
      PushNotification.cancelAllLocalNotifications();
    } else {
      PushNotificationIOS.removeAllDeliveredNotifications();
    }
  };

  removeDeliveredNotificationByID = (notificationId) => {
    console.log(
      '[LocalnotificationService] removeDeliveredNotificationByID: ',
      notificationId,
    );
    PushNotification.cancelLocalNotifications({id: `${notificationId}`});
  };
}

export const localNotificationService = new LocalNotificationService();
