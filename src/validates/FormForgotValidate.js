export const FormForgotValidate = (values) => {
  const errors = {};

  if (!values.phoneNo || values.phoneNo === '') {
    errors.phoneNo = 'Nomor Handphone harus diisi';
  } else {
    if (values.phoneNo.length < 9) {
      errors.phoneNo = 'No Handphone 1 diisi 9 - 15 digit';
    }
  }

  return errors;
};
