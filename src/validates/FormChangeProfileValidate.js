import {regexEmail, regexNumOnly} from '../utils/utilization';

export const FormChangeProfileValidate = (values) => {
  const errors = {};

  if (!values.NIK || values.NIK === '') {
    errors.NIK = 'NIK harus diisi';
  } else {
    if (values.NIK.length < 16) {
      errors.NIK = 'NIK diisi 16 digit';
    }
  }
  
  if (!values.gender || values.gender === '') {
    errors.gender = 'Jenis kelamin harus diisi';
  } 

  return errors;
};
