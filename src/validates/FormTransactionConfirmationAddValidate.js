import {regexJsonEscape, regexNumOnly, dateYearValidate, nextMonth } from '../utils/utilization';
import { CODE_FIELD_TIPE_KONSUMEN } from '../utils/constant';

export const FormTransactionConfirmationAddValidate = (values) => {
  const errors = {}

  if (!values.tglInvoice || values.tglInvoice === "") {
    errors.tglInvoice = 'Tanggal Invoice harus diisi'
  } else {
    if (!dateYearValidate(values.tglInvoice)) {
      errors.tglInvoice = 'Format Tanggal Invoice tidak sesuai'
    } else if (values.tglInvoice > values.tglInput ) {
        errors.tglInvoice = 'Tanggal Invoice tidak boleh lebih dari hari ini'
    } else if (values.tglInvoice < values.tglPO ) {
      errors.tglInvoice = 'Tanggal Invoice tidak boleh kurang dari Tanggal PO'
    }
  }

  if (!values.noInvoice || values.noInvoice === "") {
    errors.noInvoice = 'Nomor Invoice harus diisi'
  } else if (values.noInvoice.length > 20) {
    errors.noInvoice = 'NoInvoice harus diisi max 20 digit'
  }

  if (!values.tglTerimaInvoice || values.tglTerimaInvoice === "") {
    errors.tglTerimaInvoice = 'Tanggal Terima Invoice harus diisi'
  } else {
    if (!dateYearValidate(values.tglTerimaInvoice)) {
        errors.tglTerimaInvoice = 'Format Tanggal Terima Invoice tidak sesuai'
    }else if (values.tglTerimaInvoice < values.tglInvoice ) {
        errors.tglTerimaInvoice = 'Tanggal Terima Invoice tidak boleh kurang dari Tanggal Invoice'
    } else if (values.tglTerimaInvoice > values.tglInput ) {
        errors.tglTerimaInvoice = 'Tanggal Terima Invoice tidak boleh lebih dari hari ini'
    }
  }

  if (!values.tkInfoPengiriman || values.tkInfoPengiriman === "") {
    errors.tkInfoPengiriman = 'Info Pengiriman harus diisi'
  }

  if (!values.tkTglPengiriman || values.tkTglPengiriman === "") {
    errors.tkTglPengiriman = 'Tanggal Pengiriman harus diisi'
  } else {
    if (!dateYearValidate(values.tkTglPengiriman)) {
        errors.tkTglPengiriman = 'Format Tanggal Pengiriman tidak sesuai'
    } else if (values.tkTglPengiriman < values.tglInput ) {
        errors.tkTglPengiriman = 'Tanggal Pengiriman minimal hari ini'
    }
  }   

  if (!values.tkAlamatKirim || values.tkAlamatKirim === "") {
    errors.tkAlamatKirim = 'Dikirim ke Alamat harus diisi'
  }
  
  if (!values.tkAlamat || values.tkAlamat === "") {
    errors.tkAlamat = 'Alamat harus diisi'
  } else {
    if (regexJsonEscape(values.tkAlamat)) {
      errors.tkAlamat = 'Alamat diisi alfabet'
    }
  }

  if (!values.tkRt || values.tkRt === "") {
    errors.tkRt = 'RT harus diisi'
  } else {
    if (regexNumOnly(values.tkRt)) {
      errors.tkRt = 'RT harus diisi dengan angka'
    }else if (values.tkRt.length !== 3) {
      errors.tkRt = 'RT harus diisi 3 digit'
    }
  }

  if (!values.tkRw || values.tkRw === "") {
    errors.tkRw = 'RW harus diisi'
  } else {
    if (regexNumOnly(values.tkRw)) {
      errors.tkRw = 'RW harus diisi dengan angka'
    }else if (values.tkRw.length !== 3) {
      errors.tkRw = 'RW harus diisi 3 digit'
    }
  }

  if (!values.tkProvinsi || values.tkProvinsi === "") {
    errors.tkProvinsi = 'Provinsi harus diisi'
  }

  if (!values.tkKotaKabupaten || values.tkKotaKabupaten === "") {
    errors.tkKotaKabupaten = 'Kota/Kabupaten harus diisi'
  }

  if (!values.tkKecamatan || values.tkKecamatan === "") {
    errors.tkKecamatan = 'Kecamatan harus diisi'
  }

  if (!values.tkKelurahan || values.tkKelurahan === "") {
    errors.tkKelurahan = 'Kelurahan harus diisi'
  }

  if (!values.tkKodePos || values.tkKodePos === "") {
    errors.ktpKodePos = 'Kode Pos harus diisi'
  }

  if (!values.tglTerimaDo || values.tglTerimaDo === "") {
    errors.tglTerimaDo = 'Tanggal Terima DO harus di isi'
  } else {
    if (!dateYearValidate(values.tglTerimaDo)) {
        errors.tglTerimaDo = 'Format Tanggal Terima DO tidak sesuai'
    } else if (values.tglTerimaDo < values.tglInvoice) {
        errors.tglTerimaDo = 'Tanggal Pengiriman tidak boleh kurang dari Tanggal Invoice'
    } else if (values.tglTerimaDo > nextMonth(values.tglInvoice)) {
        errors.tglTerimaDo = 'Tanggal Pengiriman max 1 bulan dari tanggal Tanggal Invoice'
    }
  }

  if (!values.tglKonfirmasiDo || values.tglKonfirmasiDo === "") {
    errors.tglKonfirmasiDo = 'Tanggal Konfirmasi DO harus di isi'
  } else {
      if (!dateYearValidate(values.tglKonfirmasiDo)) {
        errors.tglKonfirmasiDo = 'Format Tanggal Konfirmasi DO tidak sesuai'
    } else if (values.tglKonfirmasiDo < values.tglTerimaDo) {
        errors.tglKonfirmasiDo = 'Tanggal Konfirmasi DO tidak boleh kurang dari Tanggal Terima DO'
    } else if (values.tglKonfirmasiDo > nextMonth(values.tglInvoice)) {
        errors.tglKonfirmasiDo = 'Tanggal Pengiriman max 1 bulan dari Tanggal Invoice'
    }
  } 

  if (!values.uploadPo || values.uploadPo === "") {
    errors.uploadPo = 'Foto PO dengan stempel toko harus di upload'
  }
  
  if (!values.uploadInvoice || values.uploadInvoice === "") {
    errors.uploadInvoice = 'Foto Invoice harus di upload'
  }

  if (!values.uploadDo || values.uploadDo === "") {
    errors.uploadDo = 'Foto DO (Surat Jalan) harus di upload'
  }

  if (!values.uploadFotoKonsumen || values.uploadFotoKonsumen === "") {
    errors.uploadFotoKonsumen = 'Foto Konsumen harus di upload'
  }

  if (!values.tipeKonsumen || values.tipeKonsumen === "") {
    errors.tipeKonsumen = 'TIpe Konsumen harus diisi'
  } else {
    if (values.tipeKonsumen.indexOf(CODE_FIELD_TIPE_KONSUMEN) !== -1) {
      if (!values.uploadPotongGaji || values.uploadPotongGaji === "") {
        errors.uploadPotongGaji = 'Foto Bukti Potong Gaji harus diisi'
      }
    }
  }

  return errors
}
