import {regexEmail, regexNumOnly} from '../utils/utilization';

export const FormRegisterValidate = (values) => {
  const errors = {};

  if (!values.phoneNo || values.phoneNo === '') {
    errors.phoneNo = 'Nomor Handphone harus diisi';
  } else {
    if (values.phoneNo.length < 9) {
      errors.phoneNo = 'No Handphone diisi 9 - 15 digit';
    } else if (regexNumOnly(values.phoneNo)) {
      errors.phoneNo = 'No handphone harus di isi angka';
    }
  }

  if (!values.password || values.password === '') {
    errors.password = 'Password harus diisi';
  }

  if (!values.Username || values.Username === '') {
    errors.Username = 'Nama harus diisi';
  }

  if (!values.Email) {
    errors.Email = 'Email harus diisi';
  }

  if (regexEmail(values.Email)) {
    errors.Email = 'Email minimal 9 digit (ada @ dan .)';
  }

  if (!values.BirthPlace || values.BirthPlace === '') {
    errors.BirthPlace = 'Tempat harus diisi';
  }

  if (!values.BirthDate) {
    errors.BirthDate = 'Tanggal Lahir harus diisi';
  }

  return errors;
};
