import {regexNumOnly} from '../utils/utilization';

export const FormLoginValidate = (values) => {
  const errors = {};

  if (!values.phoneNo || values.phoneNo === '') {
    errors.phoneNo = 'Nomor Handphone harus diisi';
  } else {
    if (values.phoneNo.length < 9) {
      errors.phoneNo = 'No Handphone diisi 9 - 15 digit';
    } else if (regexNumOnly(values.phoneNo)) {
      errors.phoneNo = 'No handphone harus di isi angka';
    }
  }

  if (!values.password || values.password === '') {
    errors.password = 'Password harus diisi';
  }

  return errors;
};
