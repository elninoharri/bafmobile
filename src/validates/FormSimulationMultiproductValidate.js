import {regexEmail, regexNumOnly} from '../utils/utilization';

export const FormSimulationMultiproductValidate = (values) => {
  const errors = {};

  if (!values.tipeBarang || values.tipeBarang === '') {
    errors.tipeBarang = 'Tipe Barang harus diisi';
  }

  if (!values.merkBarang || values.merkBarang === '') {
    errors.merkBarang = 'Merk Barang harus diisi';
  }

  if (!values.hargaBarang || values.hargaBarang === '') {
    errors.hargaBarang = 'Harga Barang harus diisi';
  } else {
    if (parseFloat(values.hargaBarang) < 1000000) {
      errors.hargaBarang = 'Harga Barang minimal Rp. 1.000.000';
    }
  }

  if (!values.tenor || values.tenor === '') {
    errors.tenor = 'Tenor harus diisi';
  }

  if (!values.uangMuka || values.uangMuka === '') {
    errors.uangMuka = 'Uang muka harus diisi';
  }

  return errors;
};
