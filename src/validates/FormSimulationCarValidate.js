import {regexEmail, regexNumOnly} from '../utils/utilization';

export const FormSimulationCarValidate = (values) => {
  const errors = {};

  if (!values.merk || values.merk === '') {
    errors.merk = 'Merk Mobil harus diisi';
  }

  if (!values.hargaBarang || values.hargaBarang === '') {
    errors.hargaBarang = 'Harga Barang harus diisi';
  } else {
    if (parseFloat(values.hargaBarang) < 1000000) {
      errors.hargaBarang = 'Harga harus di atas Rp.1.000.000,- Rupiah';
    }
  }

  if (!values.tenor || values.tenor === '') {
    errors.tenor = 'Tenor harus diisi';
  }

  if (!values.uangMuka || values.uangMuka === '') {
    errors.uangMuka = 'DP harus diisi';
  }

  return errors;
};
