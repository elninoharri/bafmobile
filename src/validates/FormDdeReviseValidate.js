export const FormDdeReviseValidate = (values) => {
    const errors = {}

    if (!values.uploadFotoKonsumen || values.uploadFotoKonsumen === '') {
      errors.uploadFotoKonsumen = 'Foto konsumen harus di upload'
    }
    
    if (!values.uploadKtp || values.uploadKtp === '') {
      errors.uploadKtp = 'Foto KTP harus di upload'
    }

    if (!values.uploadFap || values.uploadFap === '') {
      errors.uploadFap = 'Foto FAP harus di upload'
    }

    if (!values.uploadPpbk || values.uploadPpbk === '') {
      errors.uploadPpbk = 'Foto PPBK harus di upload'
    }

    if (!values.uploadPpbk || values.uploadPpbk === '') {
      errors.uploadPpbk = 'Foto PPBK harus di upload'
    }

    if (!values.uploadPpbk || values.uploadPpbk === '') {
      errors.uploadPpbk = 'Foto PPBK harus di upload'
    }

    if (!values.uploadSk || values.uploadSk === '') {
      errors.uploadSk = 'Foto Surat Keterangan harus di upload'
    }

    if (!values.uploadKk || values.uploadKk === '') {
      errors.uploadKk = 'Foto Kartu Keluarga harus di upload'
    }

    if (!values.uploadSim || values.uploadSim === '') {
      errors.uploadSim = 'Foto SIM harus di upload'
    }

    return errors
  }
  