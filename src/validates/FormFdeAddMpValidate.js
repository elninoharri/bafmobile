import {
  regexSymbolNum,
  regexSymbol,
  regexJsonEscape,
  regexNumOnly,
  regexSymbolExceptDot,
} from '../utils/utilization';
import {
  CODE_FIELD_JOB_CATEGORY,
  CODE_FIELD_JOB_STATUS_1,
  CODE_FIELD_JOB_STATUS_3,
} from '../utils/constant';

export const FormFdeAddMpValidate = (values) => {
  const errors = {};

  if (values.namaPanggilan) {
    if (regexSymbolNum(values.namaPanggilan)) {
      errors.namaPanggilan = 'Nama panggilan diisi huruf alfabet';
    }
  }

  if (!values.kodeArea) {
    errors.kodeArea = 'Kode area harus diisi';
  } else {
    if (regexNumOnly(values.kodeArea)) {
      errors.kodeArea = 'Kode area harus diisi angka';
    } else if (values.kodeArea.charAt(0) !== '0') {
      errors.kodeArea = 'Kode area diawali dengan 0';
    } else if (values.kodeArea.length < 2) {
      errors.kodeArea = 'Kode area diisi 2 sampai 4 digit';
    } else if (values.kodeArea.length > 4) {
      errors.kodeArea = 'Kode area diisi 2 sampai 4 digit';
    }
  }

  if (!values.telepon) {
    errors.telepon = 'No telp harus diisi';
  } else {
    if (regexNumOnly(values.telepon)) {
      errors.telepon = 'No telp harus diisi angka';
    } else if (values.telepon.charAt(0) === '0') {
      errors.telepon = 'No telp tidak boleh diawali dengan 0';
    } else if (values.telepon.length < 6) {
      errors.telepon = 'No telp diisi 6 sampai 10 digit';
    } else if (values.telepon.length > 10) {
      errors.telepon = 'No telp diisi 6 sampai 10 digit';
    }
  }

  if (values.noHandphone2) {
    if (regexNumOnly(values.noHandphone2)) {
      errors.noHandphone2 = 'No handphone 2 harus diisi angka';
    } else if (values.noHandphone2.charAt(0) !== '0') {
      errors.noHandphone2 = 'No handphone 2 diawali dengan 0';
    } else if (values.noHandphone2.length < 9) {
      errors.noHandphone2 = 'No Handphone 2 diisi 9 - 15 digit';
    } else if (values.noHandphone2.length > 15) {
      errors.noHandphone2 = 'No Handphone 2 diisi 9 - 15 digit';
    }
  }

  if (values.noHandphone3) {
    if (regexNumOnly(values.noHandphone3)) {
      errors.noHandphone3 = 'No handphone 3 harus diisi angka';
    } else if (values.noHandphone3.charAt(0) !== '0') {
      errors.noHandphone3 = 'No handphone 3 diawali dengan 0';
    } else if (values.noHandphone3.length < 9) {
      errors.noHandphone3 = 'No Handphone 3 diisi 9 - 15 digit';
    } else if (values.noHandphone3.length > 15) {
      errors.noHandphone3 = 'No Handphone 2 diisi 9 - 15 digit';
    }
  }

  if (!values.jenisKelamin || values.jenisKelamin === '') {
    errors.jenisKelamin = 'Jenis kelamin harus diisi';
  }

  if (!values.pendidikanTerakhir || values.pendidikanTerakhir === '') {
    errors.pendidikanTerakhir = 'Pendidikan terakhir harus diisi';
  }

  if (values.pendidikanTerakhir === '0107^Lainnya') {
    if (
      !values.pendidikanTerakhirLainnya ||
      values.pendidikanTerakhirLainnya === ''
    ) {
      errors.pendidikanTerakhirLainnya =
        'Pendidikan terakhir lainnya belum diisi';
    }
  }

  if (values.pendidikanTerakhirLainnya) {
    if (regexSymbol(values.pendidikanTerakhirLainnya)) {
      errors.pendidikanTerakhirLainnya =
        'Pendidikan terakhir lainya diisi dengan alfabet / angka';
    }
  }

  if (!values.statusPernikahan || values.statusPernikahan === '') {
    errors.statusPernikahan = 'Status pernikahan harus diisi';
  } else {
    if (values.statusPernikahan.indexOf('MAR') !== -1) {
      if (!values.namaPasangan || values.namaPasangan === '') {
        errors.namaPasangan = 'Nama pasangan belum diisi';
      }
      if (!values.noHandphonePasangan || values.noHandphonePasangan === '') {
        errors.noHandphonePasangan = 'No Handphone pasangan belum diisi';
      }
    }
  }

  if (values.namaPasangan) {
    if (regexSymbolNum(values.namaPasangan)) {
      errors.namaPasangan = 'Nama pasangan diisi dengan alfabet';
    }
  }

  if (values.noHandphonePasangan) {
    if (regexNumOnly(values.noHandphonePasangan)) {
      errors.noHandphonePasangan = 'No Handphone pasangan harus diisi angka';
    } else if (values.noHandphonePasangan.charAt(0) !== '0') {
      errors.noHandphonePasangan = 'No Handphone pasangan diawali dengan 0';
    } else if (values.noHandphonePasangan.length < 9) {
      errors.noHandphonePasangan = 'No Handphone pasangan diisi 9 - 15 digit';
    } else if (values.noHandphonePasangan.length > 15) {
      errors.noHandphonePasangan = 'No Handphone pasangan diisi 9 - 15 digit';
    }
  }

  if (!values.agama || values.agama === '') {
    errors.agama = 'Agama harus diisi';
  }

  if (!values.ktpAlamat || values.ktpAlamat === '') {
    errors.ktpAlamat = 'Alamat harus diisi';
  } else {
    if (regexJsonEscape(values.ktpAlamat)) {
      errors.ktpAlamat = 'Alamat diisi alfabet';
    }
  }

  if (!values.ktpRt || values.ktpRt === '') {
    errors.ktpRt = 'RT harus diisi';
  } else {
    if (regexNumOnly(values.ktpRt)) {
      errors.ktpRt = 'RT harus diisi dengan angka';
    } else if (values.ktpRt.length !== 3) {
      errors.ktpRt = 'RT harus diisi 3 digit';
    }
  }

  if (!values.ktpRw || values.ktpRw === '') {
    errors.ktpRw = 'RW harus diisi';
  } else {
    if (regexNumOnly(values.ktpRw)) {
      errors.ktpRw = 'RW harus diisi dengan angka';
    } else if (values.ktpRw.length !== 3) {
      errors.ktpRw = 'RW harus diisi 3 digit';
    }
  }

  if (!values.ktpProvinsi || values.ktpProvinsi === '') {
    errors.ktpProvinsi = 'Provinsi harus diisi';
  }

  if (!values.ktpKotaKabupaten || values.ktpKotaKabupaten === '') {
    errors.ktpKotaKabupaten = 'Kota/Kabupaten harus diisi';
  }

  if (!values.ktpKecamatan || values.ktpKecamatan === '') {
    errors.ktpKecamatan = 'Kecamatan harus diisi';
  }

  if (!values.ktpKelurahan || values.ktpKelurahan === '') {
    errors.ktpKelurahan = 'Kelurahan harus diisi';
  }

  if (!values.ktpKodePos || values.ktpKodePos === '') {
    errors.ktpKodePos = 'Kode Pos harus diisi';
  }

  if (!values.ktpStatusRumah || values.ktpStatusRumah === '') {
    errors.ktpStatusRumah = 'Status rumah harus diisi';
  } else {
    if (
      values.ktpStatusRumah.indexOf('RENT') !== -1 ||
      values.ktpStatusRumah.indexOf('KOST') !== -1
    ) {
      if (!values.ktpMasaBerlakuSewa || values.ktpMasaBerlakuSewa === '') {
        errors.ktpMasaBerlakuSewa =
          'Kolom ini harus diisi jika status rumah KONTRAK/SEWA/KOST';
      }
    }
  }

  if (!values.ktpLamaTinggalTahun || values.ktpLamaTinggalTahun === '') {
    errors.ktpLamaTinggalTahun = 'Lama tahun tinggal harus diisi';
  } else {
    if (regexNumOnly(values.ktpLamaTinggalTahun)) {
      errors.ktpLamaTinggalTahun = 'Lama tahun tinggal diisi dengan angka';
    }
  }

  if (!values.ktpLamaTinggalBulan || values.ktpLamaTinggalBulan === '') {
    errors.ktpLamaTinggalBulan = 'Lama bulan tinggal harus diisi';
  } else {
    if (regexNumOnly(values.ktpLamaTinggalBulan)) {
      errors.ktpLamaTinggalBulan = 'Lama bulan tinggal diisi dengan angka';
    } else if (Number(values.ktpLamaTinggalBulan) > 11) {
      errors.ktpLamaTinggalBulan = 'Lama bulan tinggal diisi maksimal 11';
    }
  }

  if (!values.residenceAlamat || values.residenceAlamat === '') {
    errors.residenceAlamat = 'Alamat domisili harus diisi';
  } else {
    if (regexJsonEscape(values.residenceAlamat)) {
      errors.residenceAlamat = 'Alamat domisili diisi alfabet';
    }
  }

  if (!values.residenceRt || values.residenceRw === '') {
    errors.residenceRt = 'RT domisili harus diisi';
  } else {
    if (regexNumOnly(values.residenceRt)) {
      errors.residenceRt = 'RT domisili harus diisi dengan angka';
    } else if (values.residenceRt.length !== 3) {
      errors.residenceRt = 'RT domisili harus diisi 3 digit';
    }
  }

  if (!values.residenceRw || values.residenceRw === '') {
    errors.residenceRw = 'RW domisili harus diisi';
  } else {
    if (regexNumOnly(values.residenceRw)) {
      errors.residenceRw = 'RW domisili harus diisi dengan angka';
    } else if (values.residenceRw.length !== 3) {
      errors.residenceRw = 'RW domisili harus diisi 3 digit';
    }
  }

  if (!values.residenceProvinsi || values.residenceProvinsi === '') {
    errors.residenceProvinsi = 'Provinsi domisili harus diisi';
  }
  if (!values.residenceKotaKabupaten || values.residenceKotaKabupaten === '') {
    errors.residenceKotaKabupaten = 'Kota Kabupaten domisili harus diisi';
  }
  if (!values.residenceKecamatan || values.residenceKecamatan === '') {
    errors.residenceKecamatan = 'Kecamatan domisili harus diisi';
  }
  if (!values.residenceKelurahan || values.residenceKelurahan === '') {
    errors.residenceKelurahan = 'Kelurahan/Desa domisili harus diisi';
  }
  if (!values.residenceKodePos || values.residenceKodePos === '') {
    errors.residenceKodePos = 'Kode pos domisili harus diisi';
  }

  if (!values.residenceStatusRumah || values.residenceStatusRumah === '') {
    errors.residenceStatusRumah = 'Status rumah domisili harus diisi';
  } else {
    if (
      values.residenceStatusRumah.indexOf('RENT') !== -1 ||
      values.residenceStatusRumah.indexOf('KOST') !== -1
    ) {
      if (
        !values.residenceMasaBerlakuSewa ||
        values.residenceMasaBerlakuSewa === ''
      ) {
        errors.residenceMasaBerlakuSewa =
          'Kolom ini harus diisi jika status rumah KONTRAK/SEWA/KOST';
      }
    }
  }

  if (
    !values.residenceLamaTinggalTahun ||
    values.residenceLamaTinggalTahun === ''
  ) {
    errors.residenceLamaTinggalTahun =
      'Lama tahun tinggal domisili harus diisi';
  } else {
    if (regexNumOnly(values.residenceLamaTinggalTahun)) {
      errors.residenceLamaTinggalTahun =
        'Lama tahun tinggal domisili diisi dengan angka';
    }
  }

  if (
    !values.residenceLamaTinggalBulan ||
    values.residenceLamaTinggalBulan === ''
  ) {
    errors.residenceLamaTinggalBulan =
      'Lama bulan tinggal domisili harus diisi';
  } else {
    if (regexNumOnly(values.residenceLamaTinggalBulan)) {
      errors.residenceLamaTinggalBulan =
        'Lama bulan tinggal domisili diisi dengan angka';
    } else if (Number(values.residenceLamaTinggalBulan) > 11) {
      errors.DomisiliLamaTinggalBulan =
        'Lama bulan tinggal domisili diisi maksimal 11';
    }
  }

  if (!values.jobCategory || values.jobCategory === '') {
    errors.jobCategory = 'Kategori Pekerjaan harus diisi';
  } else {
    if (values.jobCategory.indexOf(CODE_FIELD_JOB_CATEGORY) !== -1) {
      if (!values.jobStatus || values.jobStatus === '') {
        errors.jobStatus = 'Status Pekerjaan harus diisi';
      }
    }
  }

  if (
    values.jobStatus &&
    (values.jobStatus.indexOf(CODE_FIELD_JOB_STATUS_1) !== -1 ||
      values.jobStatus.indexOf(CODE_FIELD_JOB_STATUS_3) !== -1)
  ) {
    if (!values.kontrakBulan || values.kontrakBulan === '') {
      errors.kontrakBulan = 'Periode Masa Kontrak (Bulan) harus diisi';
    }
    if (!values.kontrakTahun || values.kontrakTahun === '') {
      errors.kontrakTahun = 'Periode Masa Kontrak (Tahun) harus diisi';
    }
  }

  if (!values.lamaUsahaBulan || values.lamaUsahaBulan === '') {
    errors.lamaUsahaBulan = 'Bekerja Sejak (Bulan) harus diisi';
  }

  if (!values.lamaUsahaTahun || values.lamaUsahaTahun === '') {
    errors.lamaUsahaTahun = 'Bekerja Sejak (Tahun) harus diisi';
  }

  if (!values.jobType || values.jobType === '') {
    errors.jobType = 'Tipe pekerjaan harus diisi';
  }

  if (!values.jobPosition || values.jobPosition === '') {
    errors.jobPosition = 'Jabatan harus diisi';
  }

  if (!values.industryType || values.industryType === '') {
    errors.industryType = 'Bidang perusahaan/tempat usaha harus diisi';
  }

  if (!values.jobAlamat || values.jobAlamat === '') {
    errors.jobAlamat = 'Alamat perusahaan harus diisi';
  } else {
    if (regexJsonEscape(values.jobAlamat)) {
      errors.jobAlamat = 'Alamat perusahaan diisi alfabet';
    }
  }

  if (!values.jobRt || values.jobRt === '') {
    errors.jobRt = 'RT perusahaan harus diisi';
  } else {
    if (regexNumOnly(values.jobRt)) {
      errors.jobRt = 'RT perusahaan harus diisi dengan angka';
    } else if (values.jobRt.length !== 3) {
      errors.jobRt = 'RT perusahaan harus diisi 3 digit';
    }
  }

  if (!values.jobRw || values.jobRw === '') {
    errors.jobRw = 'RW perusahaan harus diisi';
  } else {
    if (regexNumOnly(values.jobRw)) {
      errors.jobRw = 'RW perusahaan harus diisi dengan angka';
    } else if (values.jobRw.length !== 3) {
      errors.jobRw = 'RW perusahaan harus diisi 3 digit';
    }
  }

  if (!values.jobProvinsi || values.jobProvinsi === '') {
    errors.jobProvinsi = 'Provinsi perusahaan harus diisi';
  }

  if (!values.jobKotaKabupaten || values.jobKotaKabupaten === '') {
    errors.jobKotaKabupaten = 'Kota/Kabupaten perusahaan harus diisi';
  }

  if (!values.jobKecamatan || values.jobKecamatan === '') {
    errors.jobKecamatan = 'Kecamatan perusahaan harus diisi';
  }

  if (!values.jobKelurahan || values.jobKelurahan === '') {
    errors.jobKelurahan = 'Kelurahan perusahaan harus diisi';
  }

  if (!values.jobKodePos || values.jobKodePos === '') {
    errors.jobKodePos = 'Kode Pos perusahaan harus diisi';
  }

  if (!values.jobKodeArea || values.jobKodeArea === '') {
    errors.jobKodeArea = 'Kode area harus diisi';
  } else {
    if (regexNumOnly(values.jobKodeArea)) {
      errors.jobKodeArea = 'Kode area harus diisi angka';
    } else if (values.jobKodeArea.charAt(0) !== '0') {
      errors.jobKodeArea = 'Kode area diawali dengan 0';
    } else if (values.jobKodeArea.length < 2) {
      errors.jobKodeArea = 'Kode area diisi 2 sampai 4 digit';
    }
  }

  if (!values.jobTelp || values.jobTelp === '') {
    errors.jobTelp = 'No telp perusahaan harus diisi';
  } else {
    if (regexNumOnly(values.jobTelp)) {
      errors.jobTelp = 'No telp perusahaan harus diisi angka';
    } else if (values.jobTelp.charAt(0) === '0') {
      errors.jobTelp = 'No telp tidak boleh diawali dengan 0';
    } else if (values.jobTelp.length < 6) {
      errors.jobTelp = 'No telp perusahaan diisi 6 sampai 10 digit';
    }
  }

  if (values.jobTelpEkstensi) {
    if (regexNumOnly(values.jobTelpEkstensi)) {
      errors.jobTelpEkstensi = 'Kode Ekstensi perusahaan harus diisi angka';
    }
  }

  if (!values.namaPerusahaan || values.namaPerusahaan === '') {
    errors.namaPerusahaan = 'Nama perusahaan/tempat usaha harus diisi';
  } else {
    if (regexSymbol(values.namaPerusahaan)) {
      errors.namaPerusahaan = 'namaPerusahaan diisi alfabet / angka';
    }
  }

  if (!values.statusUsaha || values.statusUsaha === '') {
    errors.statusUsaha = 'Status tempat usaha harus diisi';
  }

  if (!values.penghasilanPerbulan) {
    errors.penghasilanPerbulan = 'Penghasilan perbulan harus diisi';
  } else {
    if (regexNumOnly(values.penghasilanPerbulan)) {
      errors.penghasilanPerbulan = 'Penghasilan perbulan diisi angka';
    }
  }

  if (!values.namaEmergencyKontak1 || values.namaEmergencyKontak1 === '') {
    errors.namaEmergencyKontak1 = 'Nama emergency contact 1 harus diisi';
  } else {
    if (regexSymbolNum(values.namaEmergencyKontak1)) {
      errors.namaEmergencyKontak1 =
        'Nama emergency contact 1 harus diisi dengan huruf';
    }
  }

  if (!values.noHpEmergencyKontak1 || values.noHpEmergencyKontak1 === '') {
    errors.noHpEmergencyKontak1 = 'No Hp emergency contact 1 harus diisi';
  } else {
    if (regexNumOnly(values.noHpEmergencyKontak1)) {
      errors.noHpEmergencyKontak1 =
        'No Hp emergency contact 1 harus diisi dengan angka';
    } else if (values.noHpEmergencyKontak1.charAt(0) !== '0') {
      errors.noHpEmergencyKontak1 =
        'No Hp emergency contact 1 diawali dengan 0';
    } else if (values.noHpEmergencyKontak1.length < 9) {
      errors.noHpEmergencyKontak1 = 'No Hp emergency contact 1 minimal 9 digit';
    }
  }

  if (
    !values.hubunganEmergencyKontak1 ||
    values.hubunganEmergencyKontak1 === ''
  ) {
    errors.hubunganEmergencyKontak1 =
      'Hubungan emergency contact 1 harus diisi';
  }

  if (!values.alamatEmergencyKontak1 || values.alamatEmergencyKontak1 === '') {
    errors.alamatEmergencyKontak1 = 'Alamat emergency contact 1 harus diisi';
  } else {
    if (regexJsonEscape(values.alamatEmergencyKontak1)) {
      errors.alamatEmergencyKontak1 =
        'Alamat emergency contact 1 diisi alfabet';
    }
  }

  if (
    (values.namaEmergencyKontak2 && values.namaEmergencyKontak2 !== '') ||
    (values.noHpEmergencyKontak2 && values.noHpEmergencyKontak2 !== '') ||
    (values.hubunganEmergencyKontak2 &&
      values.hubunganEmergencyKontak2 !== '') ||
    (values.alamatEmergencyKontak2 && values.alamatEmergencyKontak2 !== '')
  ) {
    if (!values.namaEmergencyKontak2 || values.namaEmergencyKontak2 === '') {
      errors.namaEmergencyKontak2 = 'Nama emergency contact 2 harus diisi';
    }

    if (!values.noHpEmergencyKontak2 || values.noHpEmergencyKontak2 === '') {
      errors.noHpEmergencyKontak2 = 'No Hp emergency contact 2 harus diisi';
    }

    if (
      !values.hubunganEmergencyKontak2 ||
      values.hubunganEmergencyKontak2 === ''
    ) {
      errors.hubunganEmergencyKontak2 =
        'Hubungan emergency contact 2 harus diisi';
    }

    if (
      !values.alamatEmergencyKontak2 ||
      values.alamatEmergencyKontak2 === ''
    ) {
      errors.alamatEmergencyKontak2 = 'Alamat emergency contact 2 harus diisi';
    }
  }

  if (values.namaEmergencyKontak2) {
    if (regexSymbolNum(values.namaEmergencyKontak2)) {
      errors.namaEmergencyKontak2 = 'Nama emergency contact 2 diisi alfabet';
    }
  }

  if (values.noHpEmergencyKontak2) {
    if (regexNumOnly(values.noHpEmergencyKontak2)) {
      errors.noHpEmergencyKontak2 =
        'No Hp emergency contact 2 harus diisi dengan angka';
    } else if (values.noHpEmergencyKontak2.charAt(0) !== '0') {
      errors.noHpEmergencyKontak2 =
        'No Hp emergency contact 2 diawali dengan 0';
    } else if (values.noHpEmergencyKontak2.length < 9) {
      errors.noHpEmergencyKontak2 = 'No Hp emergency contact 2 minimal 9 digit';
    }
  }

  if (values.alamatEmergencyKontak2) {
    if (regexJsonEscape(values.alamatEmergencyKontak2)) {
      errors.alamatEmergencyKontak2 =
        'Alamat emergency contact 2 diisi alfabet';
    }
  }

  // //remark by g5k: due to requirement changes, content of JaminanCategory move to JaminanJenis, whilst JaminanCategory is hidden
  if (!values.jaminanJenis || values.jaminanJenis === '') {
    errors.jaminanJenis = 'Tujuan Pembiayaan harus diisi';
  }

  if (!values.jenis || values.jenis === '') {
    errors.jenis = 'Jenis barang harus diisi';
  }

  if (!values.merk || values.merk === '') {
    errors.merk = 'Merk harus diisi';
  }

  if (!values.ukuran || values.ukuran === '') {
    errors.ukuran = 'Ukuran harus diisi';
  } else {
    if (regexJsonEscape(values.ukuran)) {
      errors.ukuran = 'Ukuran harus diisi alfabet';
    } else if (regexSymbolExceptDot(values.ukuran)) {
      errors.ukuran = 'Ukuran hanya boleh diisi alfabet dan/atau simbol titik';
    }
  }

  if (!values.warna || values.warna === '') {
    errors.warna = 'Warna harus diisi';
  } else {
    if (regexJsonEscape(values.warna)) {
      errors.warna = 'Warna harus diisi alfabet';
    } else if (regexSymbolNum(values.warna)) {
      errors.warna = 'Warna diisi alfabet';
    }
  }

  if (!values.tipe || values.tipe === '') {
    errors.tipe = 'Tipe barang harus diisi';
  } else {
    if (regexJsonEscape(values.tipe)) {
      errors.tipe = 'Tipe barang harus diisi alfabet';
    }
  }

  if (!values.financingPurpose || values.financingPurpose === '') {
    errors.financingPurpose = 'Peruntukan unit harus diisi';
  }

  if (!values.schemeId || values.schemeId === '') {
    errors.schemeId = 'Nama Program harus diisi';
  }

  if (!values.tipeBiayaAdmin || values.tipeBiayaAdmin === '') {
    errors.tipeBiayaAdmin = 'Tipe Biaya Admin harus diisi';
  }

  if (!values.tipePembayaran || values.tipePembayaran === '') {
    errors.tipePembayaran = 'Tipe Pembayaran harus diisi';
  }

  if (!values.jangkaWaktu || values.jangkaWaktu === '') {
    errors.jangkaWaktu = 'Tenor harus diisi';
  } else {
    if (regexNumOnly(values.jangkaWaktu)) {
      errors.jangkaWaktu = 'Tenor harus diisi angka';
    }
  }

  if (!values.uangMuka || values.uangMuka === '') {
    errors.uangMuka = 'Uang muka harus diisi';
  }

  if (!values.hargaBarang || values.hargaBarang === '') {
    errors.hargaBarang = 'Harga Barang harus diisi';
  } else {
    if (regexNumOnly(values.hargaBarang)) {
      errors.hargaBarang = 'Harga Barang diisi angka';
    } else if (values.hargaBarang === '0') {
      errors.hargaBarang = 'Harga Barang tidak boleh diawali dengan 0';
    }
  }

  if (!values.sourceFunds || values.sourceFunds === '') {
    errors.sourceFunds = 'SumberDana harus diisi';
  }

  if (!values.financialSchemeId || values.financialSchemeId === '') {
    errors.financialSchemeId = 'Hasil kalkulasi tidak valid';
  }

  if (!values.biayaAdmin || values.biayaAdmin === '') {
    errors.biayaAdmin = 'Biaya Admin tidak boleh kosong';
  }

  if (!values.jumlahPembiayaan || values.jumlahPembiayaan === '') {
    errors.jumlahPembiayaan = 'Jumlah pembiayaan tidak boleh kosong';
  }

  if (!values.bungaEfektif || values.bungaEfektif === '') {
    errors.bungaEfektif = 'Bunga Flat Perbulan tidak boleh kosong';
  }

  if (!values.angsuran || values.angsuran === '') {
    errors.angsuran = 'Angsuran tidak boleh kosong';
  } else {
    if (parseInt(values.angsuran) < 125000) {
      errors.angsuran = 'Angsuran minimal Rp. 125.000,-';
    }
  }

  if (!values.biayaAsuransi || values.biayaAsuransi === '') {
    errors.biayaAsuransi = 'Biaya asuransi tidak boleh kosong';
  }

  if (!values.totalPembayaranPertama || values.totalPembayaranPertama === '') {
    errors.totalPembayaranPertama =
      'Total pembayaran pertama tidak boleh kosong';
  }

  if (!values.uploadDiri || values.uploadDiri === '') {
    errors.uploadDiri = 'Foto konsumen harus di upload';
  }

  if (!values.verifikasiWajah || values.verifikasiWajah === '') {
    errors.verifikasiWajah = 'Lakukan Verifikasi Wajah terlebih dahulu';
  }

  if (!values.uploadKtp || values.uploadKtp === '') {
    errors.uploadKtp = 'Foto KTP harus di upload';
  }

  if (!values.uploadFap || values.uploadFap === '') {
    errors.uploadFap = 'Foto FAP harus di upload';
  }

  if (!values.uploadPpbk || values.uploadPpbk === '') {
    errors.uploadPpbk = 'Foto PPBK harus di upload';
  }

  return errors;
};
