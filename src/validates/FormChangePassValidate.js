export const FormChangePassValidate = (values) => {
  const errors = {};

  if (!values.passNow || values.passNow === '') {
    errors.passNow = 'Password harus diisi';
  } else {
    if (values.passNow.length < 8) {
      errors.passNow = 'Minimal 8 karakter';
    }
  }

  if (values.passNew !== values.passConfrim) {
    errors.passConfrim = 'Password verifikasi tidak sesuai';
  }

  if (values.passNew === values.passNow) {
    errors.passNew = 'Password baru sama dengan password yang lama';
  }

  if (!values.passNew || values.passNew === '') {
    errors.passNew = 'Password harus diisi';
  } else {
    if (values.passNew.length < 8) {
      errors.passNew = 'Minimal 8 karakter';
    }
  }

  if (!values.passConfrim || values.passConfrim === '') {
    errors.passConfrim = 'Password harus diisi';
  } else {
    if (values.passConfrim.length < 8) {
      errors.passConfrim = 'Minimal 8 karakter';
    }
  }

  return errors;
};
