import {validSearchDaysRange} from '../utils/utilization';

export const FormOrderFilterValidate = (values) => {
  const errors = {};

  if (!values.Dtmcrt) {
    errors.Dtmcrt = 'Tanggal dari harus diisi';
  }

  if (!values.password) {
    errors.password = 'Password harus diisi';
  }

  if (!values.Dtmcrtto) {
    errors.Dtmcrtto = 'Tanggal sampai harus diisi';
  }

  if (!validSearchDaysRange(values.Dtmcrt, values.Dtmcrtto)) {
    errors.Dtmcrtto = 'Maksimal range 7 hari';
  }

  if (values.Dtmcrt > values.Dtmcrtto) {
    errors.Dtmcrtto =
      'Tanggal sampai tidak boleh lebih kecil dari Tanggal dari';
  }

  if (!values.Isapproved || values.Isapproved === '') {
    errors.Isapproved = 'Status order harus diisi';
  }
  return errors;
};
