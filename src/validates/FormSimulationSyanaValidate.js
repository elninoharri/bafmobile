import {regexEmail, regexNumOnly} from '../utils/utilization';

export const FormSimulationSyanaValidate = (values) => {
  const errors = {};

  if (!values.merkMotor || values.merkMotor === '') {
    errors.merkMotor = 'Merk Motor harus diisi';
  }

  if (!values.tahunMotor || values.tahunMotor === '') {
    errors.tahunMotor = 'Tahun Motor harus diisi';
  }

  if (!values.plafonPinjaman || values.plafonPinjaman === '') {
    errors.plafonPinjaman = 'Dana yang diajukan harus diisi';
  } else {
    if (parseFloat(values.plafonPinjaman) < 1000000) {
      errors.plafonPinjaman = 'Dana yang diajukan minimal Rp. 1.000.000';
    }
  }

  if (!values.tenor || values.tenor === '') {
    errors.tenor = 'Tenor harus diisi';
  }

  return errors;
};
