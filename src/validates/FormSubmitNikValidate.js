export const FormSubmitNikValidate = (values) => {
  const errors = {};

  if (!values.NIK || values.NIK === '') {
    errors.NIK = 'NIK harus diisi';
  } else {
    if (values.NIK.length < 16) {
      errors.NIK = 'NIK harus 16 digit';
    }
  }

  return errors;
};
