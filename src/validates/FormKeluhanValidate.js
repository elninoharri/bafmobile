export const FormKeluhanValidate = (values) => {
  const errors = {};
  if (!values.keluhan || values.keluhan === '') {
    errors.keluhan = 'Keluhan harus diisi';
  }
  return errors;
};
