import moment from 'moment';

export const FormRequestBPKBValidate = (values) => {
  const errors = {};


  if (values.tanggal === values.getTanggal ) {
    errors.tanggal = 'Mohon pilih jadwal pada hari berikutnya (minimal h+1)';
  }

  if (values.tanggal === moment(new Date()).format('YYYY-MM-DD') ) {
    errors.tanggal = 'Mohon pilih jadwal pada hari berikutnya (minimal h+1)';
  }

  if (!values.jam || values.jam === '') {
    errors.jam = 'Jam harus diisi';
  }


  if (!values.tanggal || values.tanggal === '') {
    errors.tanggal = 'Tanggal harus diisi';
  }

  
  if (!values.nomorKontrak || values.nomorKontrak === '') {
    errors.nomorKontrak = 'Nomor kontrak harus diisi';
  }

  return errors;
};


