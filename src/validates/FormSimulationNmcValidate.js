import {regexEmail, regexNumOnly} from '../utils/utilization';

export const FormSimulationNmcValidate = (values) => {
  const errors = {};

  if (!values.merkMotor || values.merkMotor === '') {
    errors.merkMotor = 'Merk Motor harus diisi';
  }

  if (!values.tipeMotor || values.tipeMotor === '') {
    errors.tipeMotor = 'Tipe Motor harus diisi';
  }

  if (!values.hargaMotor || values.hargaMotor === '') {
    errors.hargaMotor = 'Harga Motor harus diisi';
  } else {
    if (parseFloat(values.hargaMotor) < 1000000) {
      errors.hargaMotor = 'Harga Motor minimal Rp. 1.000.000';
    }
  }

  if (!values.tenor || values.tenor === '') {
    errors.tenor = 'Tenor harus diisi';
  }

  if (!values.uangMuka || values.uangMuka === '') {
    errors.uangMuka = 'Uang muka harus diisi';
  }

  return errors;
};
