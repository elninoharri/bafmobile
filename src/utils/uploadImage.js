import moment from 'moment';
import AliyunOSS from 'aliyun-oss-react-native';
import {
  bucketname,
  secretKey,
  accessKey,
  endPoint,
  configuration,
  urlCdn,
} from './constant';

// / / Configure AccessKey according to AliyunOss
AliyunOSS.initWithPlainTextAccessKey(
  accessKey,
  secretKey,
  endPoint,
  configuration,
);

export const uploadOssFile = async (filepath, Userid) => {
  const filetype = filepath.substring(filepath.lastIndexOf('.')).toLowerCase();
  //   / / Get the image suffix

  const currm = moment(new Date());
  const oo = Math.random();
  const objectKey = `profile_pic/${Userid}/${currm.format(
    'YYYYMMDD',
  )}/${currm}${oo}${filetype}`;
  //   / / Generate objectKey as a custom path
  return AliyunOSS.asyncUpload(bucketname, objectKey, filepath)
    .then((result) => {
      // console.log(`${urlCdn}${objectKey}`);
      return `${urlCdn}${objectKey}`;
    })
    .catch((error) => {
      return error;
    });
};
