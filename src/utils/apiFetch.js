export async function filterFetch(url, options) {
  return await fetch(url, options)
    .then((res) => {
      if (res.status !== 401 && res.status !== 200 && res.status !== 404) {
        if (res.status == 503) {
          throw new Error(`${res.status}: Maaf, terdapat masalah pada jaringan Anda. Silahkan coba kembali.`);
        } else {
          throw new Error(`${res.status}: Maaf, terjadi gangguan pada sistem kami. Silahkan coba beberapa saat lagi.`);
        }
      } else {
        return res.json();
      }
    })
    .then((json) => {
      if (json.response == 401) {
        throw new Error(JSON.stringify(json));
      } else if (json.response == 404) {
        throw new Error(`404: ${json.message}`);
      } else if (json.response != 200) {
        throw new Error(json.message || 'Error API fetch data');
      } 
      return json.result;
    });
}
export async function filterFetchStatus(url, options) {
  return await fetch(url, options)
    .then((res) => {
      if (res.status !== 401 && res.status !== 200 && res.status !== 404) {
        if (res.status == 503) {
          throw new Error(`${res.status}: Maaf, terdapat masalah pada jaringan Anda. Silahkan coba kembali.`);
        } else {
          throw new Error(`${res.status}: Maaf, terjadi gangguan pada sistem kami. Silahkan coba beberapa saat lagi.`);
        }
      } else {
        return res.json();
      }
    })
    .then((json) => {
      if (json.status != 200) {
        throw new Error(json.message || 'Error API fetch data');
      }
      return json.result;
    });
}

export async function filterFetchToken(url, options) {
  return await fetch(url, options)
    .then((res) => {
      if (res.status !== 401 && res.status !== 200 && res.status !== 404) {
        if (res.status == 503) {
          throw new Error(`${res.status}: Maaf, terdapat masalah pada jaringan Anda. Silahkan coba kembali.`);
        } else {
          throw new Error(`${res.status}: Maaf, terjadi gangguan pada sistem kami. Silahkan coba beberapa saat lagi.`);
        }
      } else {
        return res.json();
      }
    })
    .then((json) => {
      if (json.response != 200) {
        throw new Error(json.message || 'Error API fetch data');
      }
      return json.message;
    });
}

export async function filterFetchOrder(url, options) {
  return await fetch(url, options)
    .then((res) => {
      if (res.status !== 401 && res.status !== 200 && res.status !== 404) {
        if (res.status == 503) {
          throw new Error(`${res.status}: Maaf, terdapat masalah pada jaringan Anda. Silahkan coba kembali.`);
        } else {
          throw new Error(`${res.status}: Maaf, terjadi gangguan pada sistem kami. Silahkan coba beberapa saat lagi.`);
        }
      } else {
        return res.json();
      }
    })
    .then((json) => {
      if (json.statusquery !== 200) {
        throw new Error(json.message || 'Error API fetch data');
      }
      return json;
    });
}

export async function filterFetchCore(url, options) {
  return await fetch(url, options)
    .then((res) => {
      if (res.status !== 401 && res.status !== 200 && res.status !== 404) {
        if (res.status == 503) {
          throw new Error(`${res.status}: Maaf, terdapat masalah pada jaringan Anda. Silahkan coba kembali.`);
        } else {
          throw new Error(`${res.status}: Maaf, terjadi gangguan pada sistem kami. Silahkan coba beberapa saat lagi.`);
        }
      } else {
        return res.json();
      }
    })
    .then((json) => {
      if (json == null) {
        throw new Error(json.log || 'Data tidak ditemukan');
      }
      return json;
    });
}

export async function filterFetchCommon(url, options) {
  return await fetch(url, options)
    .then((res) => {
      if (res.status !== 401 && res.status !== 200 && res.status !== 404) {
        if (res.status == 503) {
          throw new Error(`${res.status}: Maaf, terdapat masalah pada jaringan Anda. Silahkan coba kembali.`);
        } else {
          throw new Error(`${res.status}: Maaf, terjadi gangguan pada sistem kami. Silahkan coba beberapa saat lagi.`);
        }
      } else {
        return res.json();
      }
    })
    .then((json) => {
      if (json.statusquery !== 200 || json.statusdb !== 200) {
        throw new Error(json.errormsg || 'Error API fetch data');
      } else if (json.statusload !== 200) {
        throw new Error(json.result || 'Data tidak ditemukan');
      }
      return json.result;
    });
}

export async function filterFetchSubmitOrder(url, options) {
  return await fetch(url, options)
    .then((res) => {
      if (res.status !== 401 && res.status !== 200 && res.status !== 404) {
        if (res.status == 503) {
          throw new Error(`${res.status}: Maaf, terdapat masalah pada jaringan Anda. Silahkan coba kembali.`);
        } else {
          throw new Error(`${res.status}: Maaf, terjadi gangguan pada sistem kami. Silahkan coba beberapa saat lagi.`);
        }
      } else {
        return res.json();
      }
    })
    .then((json) => {
      if (!json.status) {
        throw new Error(json.message || json.result || 'Error API fetch data');
      }
      return json;
    });
}

export async function filterFetchFinancialData(url, options) {
  return await fetch(url, options)
    .then((res) => {
      if (res.status !== 401 && res.status !== 200 && res.status !== 404) {
        if (res.status == 503) {
          throw new Error(`${res.status}: Maaf, terdapat masalah pada jaringan Anda. Silahkan coba kembali.`);
        } else {
          throw new Error(`${res.status}: Maaf, terjadi gangguan pada sistem kami. Silahkan coba beberapa saat lagi.`);
        }
      } else {
        return res.json();
      }
    })
    .then((json) => {
      if (json.errormsg) {
        throw new Error(json.errormsg || 'Error API fetch data');
      }
      return json;
    });
}

export async function filterFetchStatusload(url, options) {
  return await fetch(url, options)
    .then((res) => {
      if (res.status !== 401 && res.status !== 200 && res.status !== 404) {
        if (res.status == 503) {
          throw new Error(`${res.status}: Maaf, terdapat masalah pada jaringan Anda. Silahkan coba kembali.`);
        } else {
          throw new Error(`${res.status}: Maaf, terjadi gangguan pada sistem kami. Silahkan coba beberapa saat lagi.`);
        }
      } else {
        return res.json();
      }
    })
    .then((json) => {
      if (!json.statusload) {
        throw new Error(json.errormsg || 'Error API fetch data');
      } else if (json.statusload !== 200) {
        throw new Error(json.result || 'Data tidak ditemukan');
      }
      return json.result;
    });
}

export async function filterFetchRevKT(url, options) {
  return await fetch(url, options)
    .then((res) => {
      if (res.status !== 401 && res.status !== 200 && res.status !== 404) {
        if (res.status == 503) {
          throw new Error(`${res.status}: Maaf, terdapat masalah pada jaringan Anda. Silahkan coba kembali.`);
        } else {
          throw new Error(`${res.status}: Maaf, terjadi gangguan pada sistem kami. Silahkan coba beberapa saat lagi.`);
        }
      } else {
        return res.json();
      }
    })
    .then((json) => {
      if (json.status != 13) {
        throw new Error(json.message || 'Error API fetch data');
      }
      return json;
    });
}

export async function filterFetchCancel(url, option) {
  return await fetch(url, option)
    .then((response) => {
      if (response.status !== 401 && response.status !== 200 && response.status !== 404) {
        if (response.status == 503) {
          throw new Error(`${response.status}: Maaf, terdapat masalah pada jaringan Anda. Silahkan coba kembali.`);
        } else {
          throw new Error(`${response.status}: Maaf, terjadi gangguan pada sistem kami. Silahkan coba beberapa saat lagi.`);
        }
      } else {
        return response.json();
      }
    })

    .then((json) => {
      console.log(json);
      if (json.status !== 200) {
        throw new Error(json.message || 'Error API Fetch API');
      }
      return json;
    });
}

export async function filterFetchLogout(url, options) {
  return await fetch(url, options)
    .then((res) => {
      console.log("res.status " , res.status);
      if (res.status !== 401 && res.status !== 200 && res.status !== 404) {
        if (res.status == 503) {
          throw new Error(`${res.status}: Maaf, terdapat masalah pada jaringan Anda. Silahkan coba kembali.`);
        } else {
          throw new Error(`${res.status}: Maaf, terjadi gangguan pada sistem kami. Silahkan coba beberapa saat lagi.`);
        }
      } else {
        console.log("enter else");
        return res.json();
      }
    })
    .then((json) => {
      if (json.response == 401) {
        throw new Error(JSON.stringify(json));
      } else if (json.response != 200) {
        throw new Error(json.message || 'Error API fetch data');
      }
      return json;
    });
}
