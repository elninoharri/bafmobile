export const APP_NAME = 'BAF MOBILE';

export const API_KEY = 'soon';
export const API_KEY_PRIVY = '';
export const API_TIMEOUT = 60000;
export const MESSAGE_TOKEN_EXP = 'Token is expired';
export const MESSAGE_TOKEN_INVALID = 'Token is invalid';

export const falsePositive = false;

//token expired untuk testing
export const TOKEN_EXP =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJQaG9uZW5vIjoiMDg1OTM5ODA2MjIxIiwiVG9rZW5pZCI6IjU2NTkzNkJBRk1PQklMRSIsIlVzZXJJZGVudGl0eSI6IjA2OTgyMDIxNTY1OTM2MTUwMjQ1IiwiVXNlcmlkIjoiNTY1OTM2IiwiZXhwIjoxNjEyNjMwNjc4LCJvcmlnX2lhdCI6MTYxMjYyNzA3OH0.Je7gEIKxUsAVnYWiiSpXQIJu3fOTa7aNV2DD1gt5oEc';

//add constant in here
export const Jumlah_Payment = 'Konfirmasi Pembayaran';
export const Status_Order = 'Pemberitahuan Status Order';
export const Install_Remainder = 'Pemberitahuan Jatuh Tempo';

export const Urlandroid_OrderList = 'https://bafdigital/OrderList/';
export const Urlandroid_Home = 'https://bafdigital/Home/';

export const Urlios_OrderList = 'bafdigital://OrderList/';
export const Urlios_Home = 'bafdigital://Home/';

export const BAF_COLOR_BLUE = '#002f5f';
export const BAF_COLOR_YELLOW = '#F8AF34';
export const BORDER_COLOR = '#A6AAB4';
export const BG_COLOR = 'rgba(218, 218, 218, 0.5)';

export const TYPE_BANNER_ANNOUNCEMENT = 'BANNER_TYPE_ANNOUNCEMENT';
export const ERROR_AUTH = 'Error Unauthorized or Token Expired';

export const TYPE_BANNER_PROMOTION = 'BANNER_TYPE_PROMOTION';

export const endPoint = 'https://oss-ap-southeast-5.aliyuncs.com';
export const accessKey = 'LTAI4Fvy6TdqzTaM3wK6r56o';
export const secretKey = 'lEC99SQYUkiTRpP3MJ6BZ6CRbXvDtq';
export const UsrCrtValue = 'sys';

export const configuration = {
  maxRetryCount: 3,
  timeoutIntervalForRequest: 30,
  timeoutIntervalForResource: 24 * 60 * 60,
};

//untuk menghindari false positive jailMonkey ketika debugging
//Ganti current_position menjadi DEBUGGING

// export const CURRENT_POSITION = 'DEBUGGING';
export const CURRENT_POSITION = 'RELEASE';

// development;

export const APP_VERSION = '2.2.4 UAT';

export const API_URL_NONUAT = 'https://apidemo.baf.id/SalesPortal/';

// BUAT TESTING KARENA SERVICE ERROR
// export const API_URL_NONUAT = 'https://repoleved.bussan.co.id:8065/SalesPortal/mobile/';

export const API_URL_UAT = 'https://apidemo.baf.id/SalesPortalUAT';

// untuk sprint 13 ke bawah make UAT tapi untuk Sprint14 tidak pake UAT
export const API_URL_USR_MNGMNT =
  'https://apidemo.baf.id/bafmobile/mobile/user_mgmnt/v1/';

export const API_URL_COMMON =
  'https://apidemo.baf.id/bafmobile/mobile/common/v1/';

export const API_URL_TRANSACTION =
  'https://apidemo.baf.id/bafmobile/mobile/transaction/v1/';
export const API_URL_SANDIAWEB = 'https://apidemo.baf.id/SalesPortal/';
export const API_URL_CREDOLABS =
  'https://apidemo.baf.id/bafmobile/mobile/credolabs/';

// untuk sprint 13 ke bawah make UAT tapi untuk Sprint14 tidak pake UAT

export const API_HEADERS_COMMON = {
  'Content-type': 'application/json',
  'X-Api-Key': '9515328e-d485-4d3e-b0e3-7bf20be04926',
};
export const API_HEADERS_CORE = {
  'Content-type': 'application/json',
  'X-Api-Key': '62ac2676-8e25-414b-95ba-f9b90c5bb745',
};
export const API_HEADERS_ORDER = {
  'Content-type': 'application/json',
  'X-Api-Key': '457b12ad-1baf-4545-8f0e-cdabb639c6d6',
};
export const API_HEADERS_USRMGMT = {
  'Content-type': 'application/json',
  'X-Api-Key': 'd5ba15b0-959a-4092-a7a9-66d8e7f46a08',
};
export const API_HEADERS_MASTER = {
  'Content-type': 'application/json',
  'X-Api-Key': 'b5dff617-50fd-47df-8547-e912ca9d04d4',
};

export const urlCdn = 'https://baf-mobile-dev.oss-ap-southeast-5.aliyuncs.com/';

export const bucketname = 'baf-mobile-dev';

// production
// export const APP_VERSION = '2.2.0 R';

// export const API_URL_USR_MNGMNT =
//   'https://developer.baf.id:8065/bafmobile/mobile/user_mgmnt/v1/';
// export const API_URL_COMMON =
//   'https://developer.baf.id:8065/bafmobile/mobile/common/v1/';
// export const API_URL_TRANSACTION =
//   'https://developer.baf.id:8065/bafmobile/mobile/transaction/v1/';
// export const API_URL_SANDIAWEB = 'https://developer.baf.id:8065/SalesPortal/';
// export const API_URL_NONUAT = 'https://developer.baf.id:8065/SalesPortal/';
// export const API_URL_UAT = 'https://developer.baf.id:8065/SalesPortal/';

// export const API_URL_CREDOLABS =
//   'https://developer.baf.id:8065/bafmobile/mobile/credolabs/';

// export const API_HEADERS_COMMON = {
//   'Content-type': 'application/json',
//   'X-Api-Key': '14ca7b5a-4215-4c56-bd8e-6798ebd94855',
// };
// export const API_HEADERS_CORE = {
//   'Content-type': 'application/json',
//   'X-Api-Key': 'f51ad985-2903-4adf-ac25-076648979fc6',
// };
// export const API_HEADERS_ORDER = {
//   'Content-type': 'application/json',
//   'X-Api-Key': '8da71d45-8b7e-48af-9097-cdf2219bf18e',
// };
// export const API_HEADERS_USRMGMT = {
//   'Content-type': 'application/json',
//   'X-Api-Key': '1b2349c2-82d7-4bc5-942a-fe61cb868bf8',
// };
// export const API_HEADERS_MASTER = {
//   'Content-type': 'application/json',
//   'X-Api-Key': 'c8730b88-148e-4b64-b26f-a52d2c4c764b',
// };

// export const urlCdn =
//   'https://baf-mobile-prod.oss-ap-southeast-5.aliyuncs.com/';

// export const bucketname = 'baf-mobile-prod';

export const GS_LIST_STATUS_DDE = 'dde_isapproved';
export const CODE_JOB_CATEGORY = 'JOB_CATEGORY';
export const CODE_JOB_POSITION = 'JOB_POSITION';
export const CODE_EDUCATION = 'EDUCATION';
export const CODE_RELIGION = 'RELIGION';
export const CODE_GENDER = 'GENDER';
export const CODE_RELATIONSHIP = 'RELATIONSHIP';
export const CODE_MARITAL = 'MARITAL';
export const CODE_HOUSE_OWNERSHIP = 'HOUSE_OWNERSHIP';
export const CODE_OFFICE_OWNERSHIP = 'OFFICE_OWNERSHIP';
export const CODE_ALLOCATION = 'ALLOCATION';
export const CODE_SOURCE_FUNDS = 'SOURCE_FUNDS';
export const CODE_INDUSTRY_TYPE = 'INDUSTRY_TYPE';
export const CODE_FINANCING_PURPOSE = 'FINANCING_PURPOSE';
export const CODE_COLLATERAL_TYPE = 'COLLATERAL_TYPE';
export const CODE_INSTALMENT_TYPE = 'INSTALMENT_TYPE';
export const CODE_PAYMENT_TYPE = 'PAYMENT_TYPE';
export const CODE_SOURCE_APLIKASI = 'SOURCE_APLIKASI_MP';
export const CODE_JOB_STATUS = 'JOB_STATUS';
export const CODE_YEAR = 'YEAR';
export const CODE_YEAR_PERIOD = 'YEAR_PERIOD';
export const CODE_MONTH = 'MONTH';
export const CODE_JAMINAN = 'MGB^Multiguna Barang';
export const LOB = 'MP';
export const DAYS_SEARCHING_RANGE = 7;

export const CODE_FIELD_TIPE_KONSUMEN = 'INT';
export const CODE_FIELD_JOB_CATEGORY = 'EMP';
export const CODE_FIELD_JOB_STATUS_1 = 'CONTRACT';
export const CODE_FIELD_JOB_STATUS_2 = 'PERMANENT';
export const CODE_FIELD_JOB_STATUS_3 = 'PROBATION';
