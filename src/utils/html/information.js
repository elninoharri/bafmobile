import table, {IGNORED_TAGS} from '@native-html/table-plugin';
import WebView from 'react-native-webview';

export const information = `<p>1. Jatuh Tempo Pembayaran Angsuran adalah dengan ketentuan sebagai berikut :</p>
<table style=\"width: 100%; height: 250px; border=1;\" >
	<tbody>
		<tr style=\"height: 17px;\">
			<th style=\"width: 42.7739%; height: 17px; text-align: center;\">
				<span style=\"color: #000000;\">Tanggal Terima Barang</span>
			</th>
			<th style=\"width: 46.7877%; height: 28px; text-align: center;\">
				<span style=\"color: #000000;\">Tanggal Jatuh Tempo</span>
			</th>
		</tr>
		<tr style=\"height: 17px;\">
			<td style=\"width: 42.7739%; height: 17px; text-align: center;\">1 - 5</td>
			<td style=\"width: 46.7877%; height: 17px; text-align: center;\">Tanggal 5 bulan berikutnya</td>
		</tr>
		<tr style=\"height: 17px;\">
			<td style=\"width: 42.7739%; height: 17px; text-align: center;\">6 - 10</td>
			<td style=\"width: 46.7877%; height: 17px; text-align: center;\">Tanggal 10 bulan berikutnya</td>
		</tr>
		<tr style=\"height: 17px;\">
			<td style=\"width: 42.7739%; height: 17px; text-align: center;\">11- 15</td>
			<td style=\"width: 46.7877%; height: 17px; text-align: center;\">Tanggal 15 bulan berikutnya</td>
		</tr>
		<tr style=\"height: 17px;\">
			<td style=\"width: 42.7739%; height: 17px; text-align: center;\">16 - 15</td>
			<td style=\"width: 46.7877%; height: 17px; text-align: center;\">Tanggal 20 bulan berikutnya</td>
		</tr>
		<tr style=\"height: 17px;\">
			<td style=\"width: 42.7739%; height: 17px; text-align: center;\">21 - 25</td>
			<td style=\"width: 46.7877%; height: 17px; text-align: center;\">Tanggal 25 bulan berikutnya</td>
		</tr>
		<tr style=\"height: 17px;\">
			<td style=\"width: 42.7739%; height: 17px; text-align: center;\">26 - 31</td>
			<td style=\"width: 46.7877%; height: 17px; text-align: center;\">Tanggal 1 pada 2 bulan berikutnya</td>
		</tr>
	</tbody>
</table>
<p>Jatuh Tempo untuk Debitur Additional Order (AO) akan mengikuti Tanggal Jatuh Tempo Pembiayaan yang sedang berjalan.</p>
<p>2. Kondisi Barang dan Dokumen Barang :</p>
<ol list-style-type: lower-alpha>
	<li style=\"text-align: justify;\">
		<span style=\"color: #000000;\">Kondisi dan/atau kerusakan fisik Barang sebelum hingga saat pengiriman adalah tanggung jawab Penyedia Barang/Toko Rekanan, karenanya Debitur harus memeriksa dengan seksama akan kondisi Barang pada saat diterima.</span>
	</li>
	<li style=\"text-align: justify;\">
		<span style=\"color: #000000;\">Pengurusan seluruh dokumen Barang, termasuk tidak terbatas pada antara lain faktur, dilakukan oleh dan menjadi tanggung jawab Penyedia Barang/Toko Rekanan.</span>
	</li>
</ol>
<p>3. Pembayaran angsuran dapat dilakukan dengan cara :</p>
<ol style=\"list-style-type: lower-alpha;\">
	<li>Sektor Tunai di kasir Kantor Cabang BAF terdekat akan dikenakan biaya administrasi Rp 10.000,-</li>
	<li>Penagihan angsuran di kenakan biaya penagihan Rp 20.000,-</li>
	<li>Pembayaran angsuran selain di kantor BAF&nbsp;</li>
</ol>
<ol>
	<li>Gerai Alfamart, Alfamidi, Indomaret, Dan+Dan, Tektaya, Amindo, Fastpay, Kantor Pos</li>
	<li>Pembayaran melalui fasilitas Anjungan Tunai Mandiri (ATM) BANK BCA, BRI, MANDIRI, BNI</li>
	<li>Pembayaran melalui Mobile Banking BNI</li>
	<li>Pembayaran dengan cheque (cek) atau bilyet giro atas nama BAF</li>
	<li>Online Tokopedia, Bukalapak, Shopee</li>
	<li>Virtual Account BCA, dengan Kode VA 22000 dan Virtual Account Mandiri, dengan Kode VA 21026</li>
</ol>
<p>Dapat di kenakan biaya yang besarnya sesuai ketentuan yang berlaku.</p>
<p>4. Denda keterlambatan :</p>
<ol style=\"list-style-type: lower-alpha;\">
	<li>Denda keterlambatan adalah sebesar 0,5% per hari dari total hari keterlambatan dan dari angsuran. Contoh perhitungan : 
		
		
		<p>Angsuran : Rp 300.000,-/bulan</p>
		<p>Denda 0,5% per hari dari angsuran tertunggak</p>
		<p>Jumlah hari keterlambatan : 5 hari</p>
		<p>Maka denda dalam rupiah menjadi : 0,5% x Rp 300.000,- x 5 = Rp 7.500,-</p>
	</li>
	<li>Setiap keterlambatan pembayaran angsuran pada saat jatuh tempo. Debitur dikenakan denda sebesar 0,5% per hari dari jumlah angsuran terutang sejak jatuh temponya hingga terbayarkan angsurannya tersebut.</li>
</ol>
<p>5. Pernyataan dan Jaminan Debitur :</p>
<ol style=\"list-style-type: lower-alpha;\">
	<li>Nilai utang sesuai yang dicantumkan dalam Perjanjian Pembiayaan,</li>
	<li>Debitur DILARANG untuk membebankan, menjaminkan atau menjual obyek pembiayaan dengan cara apapun kepada pihak ketiga.</li>
	<li>Cidera Janji/Wanprestasi Debitur antara lain :&nbsp; 
		
		
		<p>1. Debitur tidak membayar Utang Pembiayaan/angsuran yang telah jatuh tempo sesuai dengan Perjanjian;</p>
		<p>2. Pernyataan, jaminan atau dokumen yang diberikan oleh Debitur tidak benar;</p>
	</li>
	<li>Akibat Cidera Janji/Wanprestasi antara lain :&nbsp; 
		
		
		<p>1. BAF memberikan peringatan sebanyak 2 (sua) kali kepada Debitur,</p>
		<p>2. Debitur sepakat dan setuju menyerahkan secara sukarela Barang dan BAF akan menjual sesuai dengan proses perundang-undangan yang berlaku dan hasil penjualan tersebut akan diperhitungkan dengan seluruh sisa kewajiban Debitur kepada BAF.</p>
		<p>3. Untuk keperluan penjualan tersebut, melakukan tindakan yang dianggap perlu.</p>
		<p>4. Pemilihan domisili hukum apabila berperkara.</p>
	</li>
</ol>
<p>6. BAF berhak mendapat informasi yang berkaitan dengan calon Debitur pada saat survey berlangsung dari pihak ketiga atau pihak manapun</p>
<p>7. Debitur bersedia memberikan data dan Informasi Konsumen untuk keperluan internal BAF dan memberi persetujuan kepada BAF untuk memberikan data dan Informasi Konsumen kepada pihak ketiga terkait pemasaran produk BAF dan penagihan. Kerjasama dengan pihak ketiga dipastikan bahwa Pihak Ketiga tersebut turut menjaga kerahasiaan data dan informasi Konsumen.</p>
<p>8. BAF berhak untuk menerima, menunda atau menolak membiayai fasilitas pembiayaan yang diajukan oleh Debitur, sesuai dengan standar penilaian oleh BAF dan Debitur akan mendapatkan informasi mengenai penerimaan, penundaan atau penolakan tersebut melalui Short Messages Service (SMS) atau media lainnya.</p>
<p>9. Untuk Debitur berkebutuhan khusus dalam hal tuna aksara dan tuna netra, maka Perjanjian Pembiayaan akan dijelaskan dan dibacakan dihadapan 2 (dua) orang saksi.</p>
<p>10. BAF akan memberikan informasi kepada Debitur secara tertulis atau lisan mengenai jumlah kewajiban Konsumen yang telah dibayar dan atau yang belum dibayar oleh Debitur berdasarkan permintaan dari Debitur.</p>
<p>11. Debitur dilarang memberikan sesuatu dalam bentuk apapun kepada karyawan BAF.</p>
<p>12. Debitur dapat menyampaikan pengaduan melalui kantor cabang BAF setempat dan atau melalui call center dengan nomor 1500750&nbsp;</p>
<p>13. Hal-hal berikut dapat dikategorikan sebagai TINDAK PIDANA, jika :</p>
<ol style=\"list-style-type: lower-alpha;\">
	<li>Debitur memberikan data/dokumen/keterangan yang tidak benar yang dapat menimbulkan kerugian bagi BAF dalam proses pembiayaan (Pasal 35 UU No. 42 tahun 1999 tentang Jaminan Fidusia)</li>
	<li>Debitur mengalihkan , menggadaikan atau menyewakan Barang kepada pihak lain tanpa persetujuan BAF</li>
</ol>
<p>14. Jika Debitur membutuhkan dokumen perjanjian asli, maka Debitur dapat menghubungi cabang terkait.</p>
<p>15. Debitur tidak diperkenankan untuk memberikan imbalan/hadiah dalam bentuk apapun kepada petugas BAF. Apabila ada karyawan BAF yang meminta, mohon laporkan ke Hotline Anti-Fraud : 081514553969 (simpan nomor ini).</p>
<p>16. Reputasi riwayat Pembiayaan Debitur akan tercatat pada Sistem Layanan Informasi Keuangan (SLIK) sesuai ketentuan yang berlaku.</p>`;

export const tableConfigProps = {
  WebView,
  renderers: {
    table,
  },
  ignoredTags: IGNORED_TAGS,
  renderersProps: {
    table: {
      tableStyleSpecs: {
        cellPaddingEm: 0.01,
        outerBorderWidthPx: 1,
        rowsBorderWidthPx: 1,
        columnsBorderWidthPx: 1,
        trOddBackground: 'white',
        thOddBackground: '#e6e6e6',
        ftContainerWidth: true,
        fontSizePx: 12,
        outerBorderColor: 'black',
        tdBorderColor: 'black',
        thBorderColor: 'black',
      },
    },
  },
};
