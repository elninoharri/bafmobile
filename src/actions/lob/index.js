export const GET_PENGAJUAN_DATA_PROCESS = 'GET_PENGAJUAN_DATA_PROCESS';
export const GET_PENGAJUAN_DATA_SUCCESS = 'GET_PENGAJUAN_DATA_SUCCESS';
export const GET_PENGAJUAN_DATA_ERROR = 'GET_PENGAJUAN_DATA_ERROR';

export const GET_PENGAJUAN_MP_PROCESS = 'GET_PENGAJUAN_MP_PROCESS';
export const GET_PENGAJUAN_MP_SUCCESS = 'GET_PENGAJUAN_MP_SUCCESS';
export const GET_PENGAJUAN_MP_ERROR = 'GET_PENGAJUAN_MP_ERROR';

export const GET_PENGAJUAN_NMC_PROCESS = 'GET_PENGAJUAN_NMC_PROCESS';
export const GET_PENGAJUAN_NMC_SUCCESS = 'GET_PENGAJUAN_NMC_SUCCESS';
export const GET_PENGAJUAN_NMC_ERROR = 'GET_PENGAJUAN_NMC_ERROR';

export const GET_PENGAJUAN_SYANA_PROCESS = 'GET_PENGAJUAN_SYANA_PROCESS';
export const GET_PENGAJUAN_SYANA_SUCCESS = 'GET_PENGAJUAN_SYANA_SUCCESS';
export const GET_PENGAJUAN_SYANA_ERROR = 'GET_PENGAJUAN_SYANA_ERROR';

export const GET_PENGAJUAN_CAR_PROCESS = 'GET_PENGAJUAN_CAR_PROCESS';
export const GET_PENGAJUAN_CAR_SUCCESS = 'GET_PENGAJUAN_CAR_SUCCESS';
export const GET_PENGAJUAN_CAR_ERROR = 'GET_PENGAJUAN_CAR_ERROR';

export const GET_OTR_PROCESS = 'GET_OTR_PROCESS';
export const GET_OTR_SUCCESS = 'GET_OTR_SUCCESS';
export const GET_OTR_ERROR = 'GET_OTR_ERROR';

export function getPengajuanData(data, token) {
  return {
    type: GET_PENGAJUAN_DATA_PROCESS,
    data: data,
    token: token,
  };
}

export function getPengajuanSyana(data, token) {
  return {
    type: GET_PENGAJUAN_SYANA_PROCESS,
    data: data,
    token: token,
  };
}

export function getPengajuanNMC(data, token) {
  return {
    type: GET_PENGAJUAN_NMC_PROCESS,
    data: data,
    token: token,
  };
}

export function getPengajuanMP(data, token) {
  return {
    type: GET_PENGAJUAN_MP_PROCESS,
    data: data,
    token: token,
  };
}

export function getPengajuanCar(data, token) {
  return {
    type: GET_PENGAJUAN_CAR_PROCESS,
    data: data,
    token: token,
  };
}


export function getOTR(data) {
  return {
    type: GET_OTR_PROCESS,
    data: data,
  };
}
