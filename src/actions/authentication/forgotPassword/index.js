export const FORGOT_PASSWORD_PROCESS = 'FORGOT_PASSWORD_PROCESS';
export const FORGOT_PASSWORD_SUCCESS = 'FORGOT_PASSWORD_SUCCESS';
export const FORGOT_PASSWORD_ERROR = 'FORGOT_PASSWORD_ERROR';

export function forgotPassword(data) {
  return {
    type: FORGOT_PASSWORD_PROCESS,
    data: data,
  };
}
