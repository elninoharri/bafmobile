export const CHECK_VERSION_PROCESS = "CHECK_VERSION_PROCESS"
export const CHECK_VERSION_SUCCESS = "CHECK_VERSION_SUCCESS"
export const CHECK_VERSION_ERROR = "CHECK_VERSION_ERROR"

export const CHECK_TOKEN_PROCESS = "CHECK_TOKEN_PROCESS"
export const CHECK_TOKEN_SUCCESS = "CHECK_TOKEN_SUCCESS"
export const CHECK_TOKEN_ERROR = "CHECK_TOKEN_ERROR"

export const REFRESH_TOKEN_PROCESS = "REFRESH_TOKEN_PROCESS"
export const REFRESH_TOKEN_SUCCESS = "REFRESH_TOKEN_SUCCESS"
export const REFRESH_TOKEN_ERROR = "REFRESH_TOKEN_ERROR"

export function checkVersion(token) {
    return {
        type: CHECK_VERSION_PROCESS,
        token: token
    }
}

export function checkToken(token) {
    return {
        type: CHECK_TOKEN_PROCESS,
        token: token
    }
}

export function refreshToken(token) {
    return {
        type: REFRESH_TOKEN_PROCESS,
        token: token
    }
}