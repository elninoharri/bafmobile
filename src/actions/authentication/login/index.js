export const LOGIN_PROCESS = "LOGIN_PROCESS"
export const LOGIN_SUCCESS = "LOGIN_SUCCESS"
export const LOGIN_ERROR = "LOGIN_ERROR"

export const LOGIN_OPENID_PROCESS = "LOGIN_OPENID_PROCESS"
export const LOGIN_OPENID_SUCCESS = "LOGIN_OPENID_SUCCESS"
export const LOGIN_OPENID_ERROR = "LOGIN_OPENID_ERROR"

export const SAVE_TOKEN_PROCESS = "SAVE_TOKEN_PROCESS"
export const SAVE_TOKEN_SUCCESS = "SAVE_TOKEN_SUCCESS"
export const SAVE_TOKEN_ERROR = "SAVE_TOKEN_ERROR"



export function login(data) {
    return {
        type: LOGIN_PROCESS,
        data: data
    }
}

export function loginOpenid(data) {
    return {
        type: LOGIN_OPENID_PROCESS,
        data: data
    }
}

export function saveToken(data,token) {
    return {
        type: SAVE_TOKEN_PROCESS,
        data: data,
        token:token
    }
}