import * as actions from ".";
import { act } from "react-test-renderer";

describe('ACTIONS', () => {
    it('can bring the data', async() => {
       const expectedAction = {
               type: 'LOGIN_PROCESS',
               data: {
                Phoneno: "022202220222",
                Password: "Bussan100",
                }
              }
        const test = await actions.login({
            Phoneno: "022202220222",
            Password: "Bussan100",
        })

              
       expect(test).toEqual(expectedAction)
   })

   it('can create an action with correct type', async() => {
    const expectedAction = {
            type: 'LOGIN_PROCESS',
           }
     const test = await actions.login()

           
    expect(test).toEqual(expectedAction)
})
 })