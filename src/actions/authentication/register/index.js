export const CHECK_PHONE_NUMBER_PROCESS = 'CHECK_PHONE_NUMBER_PROCESS';
export const CHECK_PHONE_NUMBER_SUCCESS = 'CHECK_PHONE_NUMBER_SUCCESS';
export const CHECK_PHONE_NUMBER_ERROR = 'CHECK_PHONE_NUMBER_ERROR';

export const OTP_CALL_PROCESS = 'OTP_CALL_PROCESS';
export const OTP_CALL_SUCCESS = 'OTP_CALL_SUCCESS';
export const OTP_CALL_ERROR = 'OTP_CALL_ERROR';

export const OTP_SMS_PROCESS = 'OTP_SMS_PROCESS';
export const OTP_SMS_SUCCESS = 'OTP_SMS_SUCCESS';
export const OTP_SMS_ERROR = 'OTP_SMS_ERROR';

export const OTP_VALIDATE_PROCESS = 'OTP_VALIDATE_PROCESS';
export const OTP_VALIDATE_SUCCESS = 'OTP_VALIDATE_SUCCESS';
export const OTP_VALIDATE_ERROR = 'OTP_VALIDATE_ERROR';

export const REGISTER_PROCESS = 'REGISTER_PROCESS';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_ERROR = 'REGISTER_ERROR';

export function checkPhoneNumber(data) {
  return {
    type: CHECK_PHONE_NUMBER_PROCESS,
    data: data,
  };
}

export function otpCall(data) {
  return {
    type: OTP_CALL_PROCESS,
    data: data,
  };
}

export function otpSms(data) {
  return {
    type: OTP_SMS_PROCESS,
    data: data,
  };
}

export function otpValidate(data) {
  return {
    type: OTP_VALIDATE_PROCESS,
    data: data,
  };
}

export function register(data) {
  return {
    type: REGISTER_PROCESS,
    data: data,
  };
}
