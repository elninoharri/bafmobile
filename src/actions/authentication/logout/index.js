export const LOGOUT_PROCESS = "LOGOUT_PROCESS"
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS"
export const LOGOUT_ERROR = "LOGOUT_ERROR"

export function logout(token) {
    return {
        type: LOGOUT_PROCESS,
        token: token
    }
}
