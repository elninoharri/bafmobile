export const GET_AUTHKEY_PROCESS = 'GET_AUTHKEY_PROCESS';
export const GET_AUTHKEY_SUCCESS = ' GET_AUTHKEY_SUCCESS';
export const GET_AUTHKEY_ERROR = 'GET_AUTHKEY_ERROR';

export function getAuthkey(data) {
  return {
    type: GET_AUTHKEY_PROCESS,
  };
}
