export const GET_BPKB_PROCESS = 'GET_BPKB_PROCESS';
export const GET_BPKB_SUCCESS = 'GET_BPKB_SUCCESS';
export const GET_BPKB_ERROR = 'GET_BPKB_ERROR';

export const SUBMIT_BPKB_PROCESS = 'SUBMIT_BPKB_PROCESS';
export const SUBMIT_BPKB_SUCCESS = 'SUBMIT_BPKB_SUCCESS';
export const SUBMIT_BPKB_ERROR = 'SUBMIT_BPKB_ERROR';

export const TIME_SLOT_BPKB_PROCESS = 'TIME_SLOT_BPKB_PROCESS';
export const TIME_SLOT_BPKB_SUCCESS = 'TIME_SLOT_BPKB_SUCCESS';
export const TIME_SLOT_BPKB_ERROR = 'TIME_SLOT_BPKB_ERROR';

export const CHECK_AVAIL_BOOK_PROCESS = 'CHECK_AVAIL_BOOK_PROCESS';
export const CHECK_AVAIL_BOOK_SUCCESS = 'CHECK_AVAIL_BOOK_SUCCESS';
export const CHECK_AVAIL_BOOK_ERROR = 'CHECK_AVAIL_BOOK_ERROR';

export const CHECK_HOLIDAY_PROCESS = 'CHECK_HOLIDAY_PROCESS';
export const CHECK_HOLIDAY_SUCCESS = 'CHECK_HOLIDAY_SUCCESS';
export const CHECK_HOLIDAY_ERROR = 'CHECK_HOLIDAY_ERROR';

export const GET_LIST_BPKB_INPROGRESS_PROCESS = 'GET_LIST_BPKB_INPROGRESS_PROCESS';
export const GET_LIST_BPKB_INPROGRESS_SUCCESS = 'GET_LIST_BPKB_INPROGRESS_SUCCESS';
export const GET_LIST_BPKB_INPROGRESS_ERROR = 'GET_LIST_BPKB_INPROGRESS_ERROR';

export const GET_LIST_BPKB_DONE_PROCESS = 'GET_LIST_BPKB_DONE_PROCESS';
export const GET_LIST_BPKB_DONE_SUCCESS = 'GET_LIST_BPKB_DONE_SUCCESS';
export const GET_LIST_BPKB_DONE_ERROR = 'GET_LIST_BPKB_DONE_ERROR';

export const CANCEL_BPKB_PROCESS = 'CANCEL_BPKB_PROCESS';
export const CANCEL_BPKB_SUCCESS = 'CANCEL_BPKB_SUCCESS';
export const CANCEL_BPKB_ERROR = 'CANCEL_BPKB_ERROR';

export const RESCHEDULE_BPKB_PROCESS = 'RESCHEDULE_BPKB_PROCESS';
export const RESCHEDULE_BPKB_SUCCESS = 'RESCHEDULE_BPKB_SUCCESS';
export const RESCHEDULE_BPKB_ERROR = 'RESCHEDULE_BPKB_ERROR';

export const GET_DETAIL_BPKB_PROCESS = 'GET_DETAIL_BPKB_PROCESS';
export const GET_DETAIL_BPKB_SUCCESS = 'GET_DETAIL_BPKB_SUCCESS';
export const GET_DETAIL_BPKB_ERROR = 'GET_DETAIL_BPKB_ERROR';

export function getBPKB(data, token) {
  return {
    type: GET_BPKB_PROCESS,
    data: data,
    token: token,
  };
}

export function submitBPKB(data, token) {
  return {
    type: SUBMIT_BPKB_PROCESS,
    data: data,
    token: token,
  };
}

export function timeSlotBPKB(data) {
  return {
    type: TIME_SLOT_BPKB_PROCESS,
    data: data
  };
}

export function checkAvailBook(data, token) {
  return {
    type: CHECK_AVAIL_BOOK_PROCESS,
    data: data,
    token: token,
  };
}

export function checkHoliday(data, token) {
  return {
    type: CHECK_HOLIDAY_PROCESS,
    data: data,
    token: token,
  };
}

export function getListBPKBInProgress(token) {
  return {
    type: GET_LIST_BPKB_INPROGRESS_PROCESS,
    token: token,
  };
}

export function getListBPKBDone(token) {
  return {
    type: GET_LIST_BPKB_DONE_PROCESS,
    token: token,
  };
}

export function cancelBpkb(data, token) {
  return {
    type: CANCEL_BPKB_PROCESS,
    data: data,
    token: token,
  };
}

export function rescheduleBpkb(data, token) {
  return {
    type: RESCHEDULE_BPKB_PROCESS,
    data: data,
    token: token,
  };
}

export function getdetailBpkb(data, token) {
  return {
    type: GET_DETAIL_BPKB_PROCESS,
    data: data,
    token: token,
  };
}