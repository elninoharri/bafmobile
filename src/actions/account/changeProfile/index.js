export const DETAIL_USER_PROCESS = 'DETAIL_USER_PROCESS';
export const DETAIL_USER_SUCCESS = 'DETAIL_USER_SUCCESS';
export const DETAIL_USER_ERROR = 'DETAIL_USER_ERROR';

export const GET_CITY_PROCESS = 'GET_CITY_PROCESS';
export const GET_CITY_SUCCESS = 'GET_CITY_SUCCESS';
export const GET_CITY_ERROR = 'GET_CITY_ERROR';

export const EDIT_USER_PROCESS = 'EDIT_USER_PROCESS';
export const EDIT_USER_SUCCESS = 'EDIT_USER_SUCCESS';
export const EDIT_USER_ERROR = 'EDIT_USER_ERROR';

export function detailUser(data, token) {
  return {
    type: DETAIL_USER_PROCESS,
    data: data,
    token: token,
  };
}

export function getCity() {
  return {
    type: GET_CITY_PROCESS,
  };
}

export function editUser(data, token) {
  return {
    type: EDIT_USER_PROCESS,
    data: data,
    token: token,
  };
}
