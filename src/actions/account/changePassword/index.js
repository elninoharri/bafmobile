export const CHANGE_PASSWORD_PROCESS = 'CHANGE_PASSWORD_PROCESS';
export const CHANGE_PASSWORD_SUCCESS = 'CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_ERROR = 'CHANGE_PASSWORD_ERROR';

export function changePassword(data, token) {
  return {
    type: CHANGE_PASSWORD_PROCESS,
    data: data,
    token: token,
  };
}
