export const GET_BAFPOINT_VALUE_PROCESS = 'GET_BAF_POINT_VALUE_PROCESS';
export const GET_BAFPOINT_PROCESS_SUCCESS = 'GET_BAF_POINT_VALUE_SUCCESS';
export const GET_BAFPOINT_PROCESS_FAILED = 'GET_BAF_POINT_VALUE_FAILED';

export function getBafPointValueAction(data, token) {
    return {
        type: GET_BAFPOINT_VALUE_PROCESS,
        data: data,
        token: token
    }
}
