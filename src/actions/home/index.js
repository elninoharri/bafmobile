export const DATA_BANNER_PROCESS = 'DATA_BANNER_PROCESS';
export const DATA_BANNER_SUCCESS = 'DATA_BANNER_SUCCESS';
export const DATA_BANNER_ERROR = 'DATA_BANNER_ERROR';

export const GET_ACTIVE_AGREEMENT_PROCESS = 'GET_ACTIVE_AGREEMENT_PROCESS';
export const GET_ACTIVE_AGREEMENT_SUCCESS = 'GET_ACTIVE_AGREEMENT_SUCCESS';
export const GET_ACTIVE_AGREEMENT_ERROR = 'GET_ACTIVE_AGREEMENT_ERROR';

export const SUBMIT_NIK_PROCESS = 'SUBMIT_NIK_PROCESS';
export const SUBMIT_NIK_SUCCESS = 'SUBMIT_NIK_SUCCESS';
export const SUBMIT_NIK_ERROR = 'SUBMIT_NIK_ERROR';

export const DETAIL_BANNER_PROCESS = 'DETAIL_BANNER_PROCESS';
export const DETAIL_BANNER_SUCCESS = 'DETAIL_BANNER_SUCCESS';
export const DETAIL_BANNER_ERROR = 'DETAIL_BANNER_ERROR';

export const REFCONTENTD_PROCESS = 'REFCONTENTD_PROCESS';
export const REFCONTENTD_SUCCESS = 'REFCONTENTD_SUCCESS';
export const REFCONTENTD_ERROR = 'REFCONTENTD_ERROR';

export const GET_VOUCHER_CODE_PROCESS = 'GET_VOUCHER_CODE_PROCESS';
export const GET_VOUCHER_CODE_SUCCESS = 'GET_VOUCHER_CODE_SUCCESS';
export const GET_VOUCHER_CODE_ERROR = 'GET_VOUCHER_CODE_ERROR';

export const FILTER_BANNER = 'FILTER_BANNER';
export const RESET_FILTER_BANNER = 'RESET_FILTER_BANNER';

/*****START PAYMENT HISTORY ACTION*****/
export const GET_PAYMENT_HISTORY_PROCESS = 'GET_PAYMENT_HISTORY_PROCESS';
export const GET_PAYMENT_HISTORY_SUCCESS = 'GET_PAYMENT_HISTORY_SUCCESS';
export const GET_PAYMENT_HISTORY_ERROR = 'GET_PAYMENT_HISTORY_ERROR';
/*****END PAYMENT HISTORY ACTION*****/

export const EMAIL_BAF_CARE_PROCESS = 'EMAIL_BAF_CARE_PROCESS';
export const EMAIL_BAF_CARE_SUCCESS = 'EMAIL_BAF_CARE_SUCCESS';
export const EMAIL_BAF_CARE_ERROR = 'EMAIL_BAF_CARE_ERROR';

export const GET_LIST_NOTIF_PROCESS = 'GET_LIST_NOTIF_PROCESS';
export const GET_LIST_NOTIF_SUCCESS = 'GET_LIST_NOTIF_SUCCESS';
export const GET_LIST_NOTIF_ERROR = 'GET_LIST_NOTIF_ERROR';

export const DELETE_NOTIF_PROCESS = 'DELETE_NOTIF_PROCESS';
export const DELETE_NOTIF_SUCCESS = 'DELETE_NOTIF_SUCCESS';
export const DELETE_NOTIF_ERROR = 'DELETE_NOTIF_ERROR';

export const READ_NOTIF_PROCESS = 'READ_NOTIF_PROCESS';
export const READ_NOTIF_SUCCESS = 'READ_NOTIF_SUCCESS';
export const READ_NOTIF_ERROR = 'READ_NOTIF_ERROR';

export const GET_LIST_DEALER_PROCESS = 'GET_LIST_DEALER_PROCESS';
export const GET_LIST_DEALER_SUCCESS = 'GET_LIST_DEALER_SUCCESS';
export const GET_LIST_DEALER_ERROR = 'GET_LIST_DEALER_ERROR';

export const GET_LIST_MITRA_PROCESS = 'GET_LIST_MITRA_PROCESS';
export const GET_LIST_MITRA_SUCCESS = 'GET_LIST_DEALER_SUCCESS';
export const GET_LIST_MITRA_ERROR = 'GET_LIST_DEALER_ERROR';

export function filterBanner(data) {
  return {
    type: FILTER_BANNER,
    data: data,
  };
}

export function resetFilterBanner() {
  return {
    type: RESET_FILTER_BANNER,
  };
}

export function dataBanner(data) {
  return {
    type: DATA_BANNER_PROCESS,
    data: data,
  };
}

export function detailBanner(data) {
  return {
    type: DETAIL_BANNER_PROCESS,
    data: data,
  };
}

export function getVoucherCode(data, token) {
  return {
    type: GET_VOUCHER_CODE_PROCESS,
    data: data,
    token: token,
  };
}

export function refContentD(data) {
  return {
    type: REFCONTENTD_PROCESS,
    data: data,
  };
}

export function getActiveAgreement(token) {
  return {
    type: GET_ACTIVE_AGREEMENT_PROCESS,
    token: token,
  };
}

export function submitNik(data, token) {
  return {
    type: SUBMIT_NIK_PROCESS,
    data: data,
    token: token,
  };
}
/*****START PAYMENT HISTORY ACTION*****/
export function getPaymentHistory(data, token) {
  return {
    type: GET_PAYMENT_HISTORY_PROCESS,
    data: data,
    token: token,
  };
}
/*****END PAYMENT HISTORY ACTION*****/

export function emailBafCare(data, token) {
  return {
    type: EMAIL_BAF_CARE_PROCESS,
    data: data,
    token: token,
  };
}

export function getListDealer(data) {
  return {
    type: GET_LIST_DEALER_PROCESS,
    data: data,
  };
}

export function getListMitra(data) {
  return {
    type: GET_LIST_MITRA_PROCESS,
    data: data,
  };
}

export function getListNotif(data, token) {
  return {
    type: GET_LIST_NOTIF_PROCESS,
    data: data,
    token: token,
  };
}

export function deleteNotif(data, token) {
  return {
    type: DELETE_NOTIF_PROCESS,
    data: data,
    token: token,
  };
}

export function readNotif(data, token) {
  return {
    type: READ_NOTIF_PROCESS,
    data: data,
    token: token,
  };
}
