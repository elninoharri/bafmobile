export const OTP_REQUEST_PROCESS = 'OTP_REQUEST_PROCESS';
export const OTP_REQUEST_SUCCESS = ' OTP_REQUEST_SUCCESS';
export const OTP_REQUEST_ERROR = 'OTP_REQUEST_ERROR';

export const OTP_REGENERATE_PROCESS = 'OTP_REGENERATE_PROCESS';
export const OTP_REGENERATE_SUCCESS = ' OTP_REGENERATE_SUCCESS';
export const OTP_REGENERATE_ERROR = 'OTP_REGENERATE_ERROR';

export const OTP_PRIVY_VALIDATE_PROCESS = 'OTP_PRIVY_VALIDATE_PROCESS';
export const OTP_PRIVY_VALIDATE_SUCCESS = ' OTP_PRIVY_VALIDATE_SUCCESS';
export const OTP_PRIVY_VALIDATE_ERROR = 'OTP_PRIVY_VALIDATE_ERROR';

export const SUBMIT_PRIVY_PROCESS = 'SUBMIT_PRIVY_PROCESS';
export const SUBMIT_PRIVY_SUCCESS = ' SUBMIT_PRIVY_SUCCESS';
export const SUBMIT_PRIVY_ERROR = 'SUBMIT_PRIVY_ERROR';

export const CHECK_STATUS_REGISTER_PROCESS = 'CHECK_STATUS_REGISTER_PROCESS';
export const CHECK_STATUS_REGISTER_SUCCESS = ' CHECK_STATUS_REGISTER_SUCCESS';
export const CHECK_STATUS_REGISTER_ERROR = 'CHECK_STATUS_REGISTER_ERROR';

export const UPLOAD_DOC_PROCESS = 'UPLOAD_DOC_PROCESS';
export const UPLOAD_DOC_SUCCESS = ' UPLOAD_DOC_SUCCESS';
export const UPLOAD_DOC_ERROR = 'UPLOAD_DOC_ERROR';

export const CHECK_STATUS_UPLOAD_PROCESS = 'CHECK_STATUS_UPLOAD_PROCESS';
export const CHECK_STATUS_UPLOAD_SUCCESS = ' CHECK_STATUS_UPLOAD_SUCCESS';
export const CHECK_STATUS_UPLOAD_ERROR = 'CHECK_STATUS_UPLOAD_ERROR';

export const CALLBACK_DOC_SIGN_PROCESS = 'CALLBACK_DOC_SIGN_PROCESS';
export const CALLBACK_DOC_SIGN_SUCCESS = ' CALLBACK_DOC_SIGN_SUCCESS';
export const CALLBACK_DOC_SIGN_ERROR = 'CALLBACK_DOC_SIGN_ERROR';

export const GET_CABANG_STATUS_PROCESS = 'GET_CABANG_STATUS_PROCESS';
export const GET_CABANG_STATUS_SUCCESS = ' GET_CABANG_STATUS_SUCCESS';
export const GET_CABANG_STATUS_ERROR = 'GET_CABANG_STATUS_ERROR';

export const UPDATE_STATUS_ORDER_PRIVY_PROCESS =
  'UPDATE_STATUS_ORDER_PRIVY_PROCESS';
export const UPDATE_STATUS_ORDER_PRIVY_SUCCESS =
  'UPDATE_STATUS_ORDER_PRIVY_SUCCESS';
export const UPDATE_STATUS_ORDER_PRIVY_ERROR =
  'UPDATE_STATUS_ORDER_PRIVY_ERROR';

export function getCabangStatus(data) {
  return {
    type: GET_CABANG_STATUS_PROCESS,
    data: data,
  };
}

export function otpRequest(data, token) {
  return {
    type: OTP_REQUEST_PROCESS,
    data: data,
    token: token,
  };
}

export function otpRegenerate(data, token) {
  return {
    type: OTP_REGENERATE_PROCESS,
    data: data,
    token: token,
  };
}

//fix: penamaan type actionnya sama dengan otpValidate FDE bikin sagas Validate FDE sama privy jalan
export function otpValidatePrivy(data, token) {
  return {
    type: OTP_PRIVY_VALIDATE_PROCESS,
    data: data,
    token: token,
  };
}

export function submitPrivy(data, token) {
  return {
    type: SUBMIT_PRIVY_PROCESS,
    data: data,
    token: token,
  };
}

export function checkStatusRegister(data) {
  return {
    type: CHECK_STATUS_REGISTER_PROCESS,
    data: data,
  };
}

export function uploadDoc(data, token) {
  return {
    type: UPLOAD_DOC_PROCESS,
    data: data,
    token: token,
  };
}

export function checkStatusUpload(data) {
  return {
    type: CHECK_STATUS_UPLOAD_PROCESS,
    data: data,
  };
}

export function callbackDocSigning(data) {
  return {
    type: CALLBACK_DOC_SIGN_PROCESS,
    data: data,
  };
}

export function updateStatusOrderPrivy(data, target) {
  return {
    type: UPDATE_STATUS_ORDER_PRIVY_PROCESS,
    data: data,
    target: target,
  };
}
