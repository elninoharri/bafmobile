export const TOKEN_SANDIA_PROCESS = "TOKEN_SANDIA_PROCESS"
export const TOKEN_SANDIA_SUCCESS = "TOKEN_SANDIA_SUCCESS"
export const TOKEN_SANDIA_ERROR = "TOKEN_SANDIA_ERROR"

export function generateToken(token) {
    return {
        type: TOKEN_SANDIA_PROCESS,
        token: token
    }
}