export const ORDER_COUNT_PROCESS = 'ORDER_COUNT_PROCESS';
export const ORDER_COUNT_SUCCESS = 'ORDER_COUNT_SUCCESS';
export const ORDER_COUNT_ERROR = 'ORDER_COUNT_ERROR';

export const ORDER_FILTER_PROCESS = 'ORDER_FILTER_PROCESS';
export const ORDER_FILTER_SUCCESS = 'ORDER_FILTER_SUCCESS';
export const ORDER_FILTER_ERROR = 'ORDER_FILTER_ERROR';

export const QDE_DETAIL_PROCESS = 'QDE_DETAIL_PROCESS';
export const QDE_DETAIL_SUCCESS = 'QDE_DETAIL_SUCCESS';
export const QDE_DETAIL_ERROR = 'QDE_DETAIL_ERROR';

export const CANCEL_PO_PROSES = 'CANCEL_PO_PROSES';
export const CANCEL_PO_SUCCESS = 'CANCEL_PO_SUCCESS';
export const CANCEL_PO_ERROR = 'CANCEL_PO_ERROR';

export const CONFIRM_PASS_PROCESS = 'CONFIRM_PASS_PROCESS';
export const CONFIRM_PASS_SUCCESS = 'CONFIRM_PASS_SUCCESS';
export const CONFIRM_PASS_ERROR = 'CONFIRM_PASS_ERROR';

export const QDE_PERSONAL_DETAIL_DATA_PROCESS =
  'QDE_PERSONAL_DETAIL_DATA_PROCESS';
export const QDE_PERSONAL_DETAIL_DATA_SUCCESS =
  'QDE_PERSONAL_DETAIL_DATA_SUCCESS';
export const QDE_PERSONAL_DETAIL_DATA_ERROR = 'QDE_PERSONAL_DETAIL_DATA_ERROR';

export const MP_MAPPING_RISK_PROCESS = 'MP_MAPPING_RISK_PROCESS';
export const MP_MAPPING_RISK_SUCCESS = 'MP_MAPPING_RISK_SUCCESS';
export const MP_MAPPING_RISK_ERROR = 'MP_MAPPING_RISK_ERROR';

export const MP_INST_CALCULATION_PROCESS = 'MP_INST_CALCULATION_PROCESS';
export const MP_INST_CALCULATION_SUCCESS = 'MP_INST_CALCULATION_SUCCESS';
export const MP_INST_CALCULATION_ERROR = 'MP_INST_CALCULATION_ERROR';

export function orderCount(data) {
  return {
    type: ORDER_COUNT_PROCESS,
    data: data,
  };
}

export function orderFilter(data) {
  return {
    type: ORDER_FILTER_PROCESS,
    data: data,
  };
}

export function cancelPO(data) {
  return {
    type: CANCEL_PO_PROSES,
    data: data,
  };
}

export function confirmPass(data, token) {
  return {
    type: CONFIRM_PASS_PROCESS,
    data: data,
    token: token,
  };
}

export function qdeDetailData(data, token) {
  return {
    type: QDE_DETAIL_PROCESS,
    data: data,
    token: token,
  };
}

export function qdePersonalDetailData(data) {
  return {
    type: QDE_PERSONAL_DETAIL_DATA_PROCESS,
    data: data,
  };
}

export function mpMappingRisk(data) {
  return {
    type: MP_MAPPING_RISK_PROCESS,
    data: data,
  };
}

export function mpInstCalculation(data) {
  return {
    type: MP_INST_CALCULATION_PROCESS,
    data: data,
  };
}
