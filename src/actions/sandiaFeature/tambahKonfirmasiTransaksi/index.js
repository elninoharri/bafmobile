export const VALIDATION_INVOICE_PROSES = "VALIDATION_INVOICE_PROSES";
export const VALIDATION_INVOICE_SUCCESS = "VALIDATION_INVOICE_SUCCESS";
export const VALIDATION_INVOICE_ERROR = "VALIDATION_INVOICE_ERROR";
export const TANGGAL_PO_PROSES = "TANGGAL_PO_PROSES";
export const TANGGAL_PO_SUCCESS = "TANGGAL_PO_SUCCESS";
export const TANGGAL_PO_ERROR = "TANGGAL_PO_ERROR";
export const DELIVERY_ADDRESS_PROSES = "DELIVERY_ADDRESS_PROSES";
export const DELIVERY_ADDRESS_SUCCESS = "DELIVERY_ADDRESS_SUCCESS";
export const DELIVERY_ADDRESS_ERROR = "DELIVERY_ADDRESS_ERROR";
export const KONFIRMASI_TRANSAKSI_PROSES = "KONFIRMASI_TRANSAKSI_PROSES";
export const KONFIRMASI_TRANSAKSI_SUCCESS = "KONFIRMASI_TRANSAKSI_SUCCESS";
export const KONFIRMASI_TRANSAKSI_ERROR = "KONFIRMASI_TRANSAKSI_ERROR";
export const TIPE_KONSUMEN_PROSES = "TIPE_KONSUMEN_PROSES";
export const TIPE_KONSUMEN_SUCCESS = "TIPE_KONSUMEN_SUCCESS";
export const TIPE_KONSUMEN_ERROR = "TIPE_KONSUMEN_ERROR";
export const INFO_PENGIRIMAN_PROSES = "INFO_PENGIRIMAN_PROSES";
export const INFO_PENGIRIMAN_SUCCESS = "INFO_PENGIRIMAN_SUCCESS";
export const INFO_PENGIRIMAN_ERROR = "INFO_PENGIRIMAN_ERROR";

export function validationInvoice(data, token) {
    return {
        type: VALIDATION_INVOICE_PROSES,
        data: data,
        token : token,
    }
}

export function tanggalPO(data, token) {
    return {
        type: TANGGAL_PO_PROSES,
        data: data,
        token : token,
    }
}

export function deliveryAddress(data) {
    return {
        type: DELIVERY_ADDRESS_PROSES,
        data: data
    }
}

export function konfirmasiTransaksi(data) {
    return {
        type: KONFIRMASI_TRANSAKSI_PROSES,
        data: data
    }
}

export function tipeKonsumen(data) {
    return {
        type: TIPE_KONSUMEN_PROSES,
        data: data
    }
}

export function infoPengiriman(data) {
    return {
        type: INFO_PENGIRIMAN_PROSES,
        data: data
    }
}
