export const DDE_JAMINANJENIS_PROCESS = "DDE_JAMINANJENIS_PROCESS";
export const DDE_JAMINANJENIS_SUCCESS = "DDE_JAMINANJENIS_SUCCESS";
export const DDE_JAMINANJENIS_ERROR = "DDE_JAMINANJENIS_ERROR";

export const DDE_JENIS_PROCESS = "DDE_JENIS_PROCESS";
export const DDE_JENIS_SUCCESS = "DDE_JENIS_SUCCESS";
export const DDE_JENIS_ERROR = "DDE_JENIS_ERROR";

export const DDE_MERK_PROCESS = "DDE_MERK_PROCESS";
export const DDE_MERK_SUCCESS = "DDE_MERK_SUCCESS";
export const DDE_MERK_ERROR = "DDE_MERK_ERROR";

export const DDE_SCHEMEID_PROCESS = "DDE_SCHEMEID_PROCESS";
export const DDE_SCHEMEID_SUCCESS = "DDE_SCHEMEID_SUCCESS";
export const DDE_SCHEMEID_ERROR = "DDE_SCHEMEID_ERROR";

export const DDE_FINANCIALDATA_PROCESS = "DDE_FINANCIALDATA_PROCESS";
export const DDE_FINANCIALDATA_SUCCESS = "DDE_FINANCIALDATA_SUCCESS";
export const DDE_FINANCIALDATA_ERROR = "DDE_FINANCIALDATA_ERROR";

export const DDE_FINANCINGPURPOSE_PROCESS = "DDE_FINANCINGPURPOSE_PROCESS";
export const DDE_FINANCINGPURPOSE_SUCCESS = "DDE_FINANCINGPURPOSE_SUCCESS";
export const DDE_FINANCINGPURPOSE_ERROR = "DDE_FINANCINGPURPOSE_ERROR";

export const DDE_SOURCEFUNDS_PROCESS = "DDE_SOURCEFUNDS_PROCESS";
export const DDE_SOURCEFUNDS_SUCCESS = "DDE_SOURCEFUNDS_SUCCESS";
export const DDE_SOURCEFUNDS_ERROR = "DDE_SOURCEFUNDS_ERROR";


export function ddeJaminanJenis(data, token) {
    return {
        type: DDE_JAMINANJENIS_PROCESS,
        data: data,
        token: token
    };
}

export function ddeJenis(data, token) {
    return {
        type: DDE_JENIS_PROCESS,
        data: data,
        token: token
    };
}

export function ddeMerk(data, token) {
    return {
        type: DDE_MERK_PROCESS,
        data: data,
        token: token
    };
}

export function ddeSchemeId(data, token) {
    return {
        type: DDE_SCHEMEID_PROCESS,
        data: data,
        token: token
    };
}

export function ddeFinancialData(data, token) {
    return {
        type: DDE_FINANCIALDATA_PROCESS,
        data: data,
        token: token
    };
}

export function ddeFinancingPurpose(data, token) {
    return {
        type: DDE_FINANCINGPURPOSE_PROCESS,
        data: data,
        token: token
    };
}

export function ddeSouceFunds(data, token) {
    return {
        type: DDE_SOURCEFUNDS_PROCESS,
        data: data,
        token: token
    };
}