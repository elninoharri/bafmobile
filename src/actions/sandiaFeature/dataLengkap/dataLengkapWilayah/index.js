export const DDE_LEGALPROVINCE_PROCESS = "DDE_LEGALPROVINCE_PROCESS";
export const DDE_LEGALPROVINCE_SUCCESS = "DDE_LEGALPROVINCE_SUCCESS";
export const DDE_LEGALPROVINCE_ERROR = "DDE_LEGALPROVINCE_ERROR";

export const DDE_RESIDENCEPROVINCE_PROCESS = "DDE_RESIDENCEPROVINCE_PROCESS";
export const DDE_RESIDENCEPROVINCE_SUCCESS = "DDE_RESIDENCEPROVINCE_SUCCESS";
export const DDE_RESIDENCEPROVINCE_ERROR = "DDE_RESIDENCEPROVINCE_ERROR";

export const DDE_JOBPROVINCE_PROCESS = "DDE_JOBPROVINCE_PROCESS";
export const DDE_JOBPROVINCE_SUCCESS = "DDE_JOBPROVINCE_SUCCESS";
export const DDE_JOBPROVINCE_ERROR = "DDE_JOBPROVINCE_ERROR";

export const DDE_LEGALCITY_PROCESS = "DDE_LEGALCITY_PROCESS";
export const DDE_LEGALCITY_SUCCESS = "DDE_LEGALCITY_SUCCESS";
export const DDE_LEGALCITY_ERROR = "DDE_LEGALCITY_ERROR";

export const DDE_RESIDENCECITY_PROCESS = "DDE_RESIDENCECITY_PROCESS";
export const DDE_RESIDENCECITY_SUCCESS = "DDE_RESIDENCECITY_SUCCESS";
export const DDE_RESIDENCECITY_ERROR = "DDE_RESIDENCECITY_ERROR";

export const DDE_JOBCITY_PROCESS = "DDE_JOBCITY_PROCESS";
export const DDE_JOBCITY_SUCCESS = "DDE_JOBCITY_SUCCESS";
export const DDE_JOBCITY_ERROR = "DDE_JOBCITY_ERROR";

export const DDE_LEGALKECAMATAN_PROCESS = "DDE_LEGALKECAMATAN_PROCESS";
export const DDE_LEGALKECAMATAN_SUCCESS = "DDE_LEGALKECAMATAN_SUCCESS";
export const DDE_LEGALKECAMATAN_ERROR = "DDE_LEGALKECAMATAN_ERROR";

export const DDE_RESIDENCEKECAMATAN_PROCESS = "DDE_RESIDENCEKECAMATAN_PROCESS";
export const DDE_RESIDENCEKECAMATAN_SUCCESS = "DDE_RESIDENCEKECAMATAN_SUCCESS";
export const DDE_RESIDENCEKECAMATAN_ERROR = "DDE_RESIDENCEKECAMATAN_ERROR";

export const DDE_JOBKECAMATAN_PROCESS = "DDE_JOBKECAMATAN_PROCESS";
export const DDE_JOBKECAMATAN_SUCCESS = "DDE_JOBKECAMATAN_SUCCESS";
export const DDE_JOBKECAMATAN_ERROR = "DDE_JOBKECAMATAN_ERROR";

export const DDE_LEGALKELURAHAN_PROCESS = "DDE_LEGALKELURAHAN_PROCESS";
export const DDE_LEGALKELURAHAN_SUCCESS = "DDE_LEGALKELURAHAN_SUCCESS";
export const DDE_LEGALKELURAHAN_ERROR = "DDE_LEGALKELURAHAN_ERROR";

export const DDE_RESIDENCEKELURAHAN_PROCESS = "DDE_RESIDENCEKELURAHAN_PROCESS";
export const DDE_RESIDENCEKELURAHAN_SUCCESS = "DDE_RESIDENCEKELURAHAN_SUCCESS";
export const DDE_RESIDENCEKELURAHAN_ERROR = "DDE_RESIDENCEKELURAHAN_ERROR";

export const DDE_JOBKELURAHAN_PROCESS = "DDE_JOBKELURAHAN_PROCESS";
export const DDE_JOBKELURAHAN_SUCCESS = "DDE_JOBKELURAHAN_SUCCESS";
export const DDE_JOBKELURAHAN_ERROR = "DDE_JOBKELURAHAN_ERROR";

export function ddeLegalProvince(data, token) {
    return {
        type: DDE_LEGALPROVINCE_PROCESS,
        data: data,
        token: token
    };
}

export function ddeResidenceProvince(data, token) {
    return {
        type: DDE_RESIDENCEPROVINCE_PROCESS,
        data: data,
        token: token
    };
}

export function ddeJobProvince(data, token) {
    return {
        type: DDE_JOBPROVINCE_PROCESS,
        data: data,
        token: token
    };
}

export function ddeLegalCity(data, token) {
    return {
        type: DDE_LEGALCITY_PROCESS,
        data: data,
        token: token
    };
}

export function ddeResidenceCity(data, token) {
    return {
        type: DDE_RESIDENCECITY_PROCESS,
        data: data,
        token: token
    };
}

export function ddeJobCity(data, token) {
    return {
        type: DDE_JOBCITY_PROCESS,
        data: data,
        token: token
    };
}

export function ddeLegalKecamatan(data, token) {
    return {
        type: DDE_LEGALKECAMATAN_PROCESS,
        data: data,
        token: token
    };
}

export function ddeResidenceKecamatan(data, token) {
    return {
        type: DDE_RESIDENCEKECAMATAN_PROCESS,
        data: data,
        token: token
    };
}

export function ddeJobKecamatan(data, token) {
    return {
        type: DDE_JOBKECAMATAN_PROCESS,
        data: data,
        token: token
    };
}

export function ddeLegalKelurahan(data, token) {
    return {
        type: DDE_LEGALKELURAHAN_PROCESS,
        data: data,
        token: token
    };
}

export function ddeResidenceKelurahan(data, token) {
    return {
        type: DDE_RESIDENCEKELURAHAN_PROCESS,
        data: data,
        token: token
    };
}

export function ddeJobKelurahan(data, token) {
    return {
        type: DDE_JOBKELURAHAN_PROCESS,
        data: data,
        token: token
    };
}



