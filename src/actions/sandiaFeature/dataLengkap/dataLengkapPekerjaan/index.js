export const DDE_JOBCATEGORY_PROCESS = "DDE_JOBCATEGORY_PROCESS";
export const DDE_JOBCATEGORY_SUCCESS = "DDE_JOBCATEGORY_SUCCESS";
export const DDE_JOBCATEGORY_ERROR = "DDE_JOBCATEGORY_ERROR";

export const DDE_JOBPOSITION_PROCESS = "DDE_JOBPOSITION_PROCESS";
export const DDE_JOBPOSITION_SUCCESS = "DDE_JOBPOSITION_SUCCESS";
export const DDE_JOBPOSITION_ERROR = "DDE_JOBPOSITION_ERROR";

export const DDE_JOBSTATUS_PROCESS = "DDE_JOBSTATUS_PROCESS";
export const DDE_JOBSTATUS_SUCCESS = "DDE_JOBSTATUS_SUCCESS";
export const DDE_JOBSTATUS_ERROR = "DDE_JOBSTATUS_ERROR";

export const DDE_JOBTYPE_PROCESS = "DDE_JOBTYPE_PROCESS";
export const DDE_JOBTYPE_SUCCESS = "DDE_JOBTYPE_SUCCESS";
export const DDE_JOBTYPE_ERROR = "DDE_JOBTYPE_ERROR";

export const DDE_INDUSTRYTYPE_PROCESS = "DDE_INDUSTRYTYPE_PROCESS";
export const DDE_INDUSTRYTYPE_SUCCESS = "DDE_INDUSTRYTYPE_SUCCESS";
export const DDE_INDUSTRYTYPE_ERROR = "DDE_INDUSTRYTYPE_ERROR";

export const DDE_LAMAUSAHABULAN_PROCESS = "DDE_LAMAUSAHABULAN_PROCESS";
export const DDE_LAMAUSAHABULAN_SUCCESS = "DDE_LAMAUSAHABULAN_SUCCESS";
export const DDE_LAMAUSAHABULAN_ERROR = "DDE_LAMAUSAHABULAN_ERROR";

export const DDE_LAMAUSAHATAHUN_PROCESS = "DDE_LAMAUSAHATAHUN_PROCESS";
export const DDE_LAMAUSAHATAHUN_SUCCESS = "DDE_LAMAUSAHATAHUN_SUCCESS";
export const DDE_LAMAUSAHATAHUN_ERROR = "DDE_LAMAUSAHATAHUN_ERROR";

export const DDE_KONTRAKBULAN_PROCESS = "DDE_KONTRAKBULAN_PROCESS";
export const DDE_KONTRAKBULAN_SUCCESS = "DDE_KONTRAKBULAN_SUCCESS";
export const DDE_KONTRAKBULAN_ERROR = "DDE_KONTRAKBULAN_ERROR";

export const DDE_KONTRAKTAHUN_PROCESS = "DDE_KONTRAKTAHUN_PROCESS";
export const DDE_KONTRAKTAHUN_SUCCESS = "DDE_KONTRAKTAHUN_SUCCESS";
export const DDE_KONTRAKTAHUN_ERROR = "DDE_KONTRAKTAHUN_ERROR";


export function ddeJobCategory(data, token) {
    return {
        type: DDE_JOBCATEGORY_PROCESS,
        data: data,
        token: token
    };
}

export function ddeJobPosition(data, token) {
    return {
        type: DDE_JOBPOSITION_PROCESS,
        data: data,
        token: token
    };
}

export function ddeJobStatus(data, token) {
    return {
        type: DDE_JOBSTATUS_PROCESS,
        data: data,
        token: token
    };
}

export function ddeJobType(data, token) {
    return {
        type: DDE_JOBTYPE_PROCESS,
        data: data,
        token: token
    };
}

export function ddeIndustryType(data, token) {
    return {
        type: DDE_INDUSTRYTYPE_PROCESS,
        data: data,
        token: token
    };
}

export function ddeLamaUsahaBulan(data, token) {
    return {
        type: DDE_LAMAUSAHABULAN_PROCESS,
        data: data,
        token: token
    };
}

export function ddeLamaUsahaTahun(data, token) {
    return {
        type: DDE_LAMAUSAHATAHUN_PROCESS,
        data: data,
        token: token
    };
}

export function ddeKontrakBulan(data, token) {
    return {
        type: DDE_KONTRAKBULAN_PROCESS,
        data: data,
        token: token
    };
}

export function ddeKontrakTahun(data, token) {
    return {
        type: DDE_KONTRAKTAHUN_PROCESS,
        data: data,
        token: token
    };
}