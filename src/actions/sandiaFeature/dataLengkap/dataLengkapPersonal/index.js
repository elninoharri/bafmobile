export const DDE_EDUCATION_PROCESS = "DDE_EDUCATION_PROCESS";
export const DDE_EDUCATION_SUCCESS = "DDE_EDUCATION_SUCCESS";
export const DDE_EDUCATION_ERROR = "DDE_EDUCATION_ERROR";

export const DDE_GENDER_PROCESS = "DDE_GENDER_PROCESS";
export const DDE_GENDER_SUCCESS = "DDE_GENDER_SUCCESS";
export const DDE_GENDER_ERROR = "DDE_GENDER_ERROR";

export const DDE_MARITALSTATUS_PROCESS = "DDE_MARITALSTATUS_PROCESS";
export const DDE_MARITALSTATUS_SUCCESS = "DDE_MARITALSTATUS_SUCCESS";
export const DDE_MARITALSTATUS_ERROR = "DDE_MARITALSTATUS_ERROR";

export const DDE_RELATIONSHIP_PROCESS = "DDE_RELATIONSHIP_PROCESS";
export const DDE_RELATIONSHIP_SUCCESS = "DDE_RELATIONSHIP_SUCCESS";
export const DDE_RELATIONSHIP_ERROR = "DDE_RELATIONSHIP_ERROR";

export const DDE_RELIGION_PROCESS = "DDE_RELIGION_PROCESS";
export const DDE_RELIGION_SUCCESS = "DDE_RELIGION_SUCCESS";
export const DDE_RELIGION_ERROR = "DDE_RELIGION_ERROR";

export const DDE_HOUSEOWNERSHIP_PROCESS = "DDE_HOUSEOWNERSHIP_PROCESS";
export const DDE_HOUSEOWNERSHIP_SUCCESS = "DDE_HOUSEOWNERSHIP_SUCCESS";
export const DDE_HOUSEOWNERSHIP_ERROR = "DDE_HOUSEOWNERSHIP_ERROR";

export const DDE_OFFICEOWNERSHIP_PROCESS = "DDE_OFFICEOWNERSHIP_PROCESS";
export const DDE_OFFICEOWNERSHIP_SUCCESS = "DDE_OFFICEOWNERSHIP_SUCCESS";
export const DDE_OFFICEOWNERSHIP_ERROR = "DDE_OFFICEOWNERSHIP_ERROR";


export function ddeEducation(data, token) {
    return {
        type: DDE_EDUCATION_PROCESS,
        data: data,
        token: token
    };
}

export function ddeGender(data, token) {
    return {
        type: DDE_GENDER_PROCESS,
        data: data,
        token: token
    };
}

export function ddeMaritalStatus(data, token) {
    return {
        type: DDE_MARITALSTATUS_PROCESS,
        data: data,
        token: token
    };
}

export function ddeRelationship(data, token) {
    return {
        type: DDE_RELATIONSHIP_PROCESS,
        data: data,
        token: token
    };
}

export function ddeReligion(data, token) {
    return {
        type: DDE_RELIGION_PROCESS,
        data: data,
        token: token
    };
}

export function ddeHouseOwnership(data, token) {
    return {
        type: DDE_HOUSEOWNERSHIP_PROCESS,
        data: data,
        token: token
    };
}

export function ddeOfficeOwnership(data, token) {
    return {
        type: DDE_OFFICEOWNERSHIP_PROCESS,
        data: data,
        token: token
    };
}