export const DDE_FILTER_PROSES = 'DDE_FILTER_PROSES';
export const DDE_FILTER_SUCCESS = 'DDE_FILTER_SUCCESS';
export const DDE_FILTER_ERROR = 'DDE_FILTER_ERROR';

export const DDE_DETAIL_PROSES = 'DDE_DETAIL_PROSES';
export const DDE_DETAIL_SUCCESS = 'DDE_DETAIL_SUCCESS';
export const DDE_DETAIL_ERROR = 'DDE_DETAIL_ERROR';

export const DDE_INSERT_PROSES = 'DDE_INSERT_PROSES';
export const DDE_INSERT_SUCCESS = 'DDE_INSERT_SUCCESS';
export const DDE_INSERT_ERROR = 'DDE_INSERT_ERROR';

export const DDE_REVISE_PROSES = 'DDE_REVISE_PROSES';
export const DDE_REVISE_SUCCESS = 'DDE_REVISE_SUCCESS';
export const DDE_REVISE_ERROR = 'DDE_REVISE_ERROR';

export function ddeInsert(data, token) {
    return {
        type: DDE_INSERT_PROSES,
        data: data,
        token: token
    };
}

export function ddeRevise(data, token) {
  return {
    type: DDE_REVISE_PROSES,
    data: data,
    token: token,
  };
}

export function ddeFilter(data, token) {
  return {
    type: DDE_FILTER_PROSES,
    data: data,
    token: token,
  };
}

export function ddeDetail(data, token) {
  return {
    type: DDE_DETAIL_PROSES,
    data: data,
    token: token,
  };
}
