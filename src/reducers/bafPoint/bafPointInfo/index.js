import {
    GET_BAFPOINT_PROCESS_FAILED,
    GET_BAFPOINT_PROCESS_SUCCESS,
    GET_BAFPOINT_VALUE_PROCESS
} from "../../../actions/bafPoint";

const initialState = {
    bafPointValue: null,
    error: null,
    loading: false
}

export function getBafPointValueReducer(state = initialState, action) {
    switch (action.type) {
        case GET_BAFPOINT_VALUE_PROCESS :
            return {
                ...state, loading: true
            }
        case GET_BAFPOINT_PROCESS_SUCCESS :
            return  {
                ...state, bafPointValue: action.payload, loading: false
            }
        case GET_BAFPOINT_PROCESS_FAILED :
            return {
                ...state, error: action.error, loading: false
            }
        default :
            return state
    }
}
