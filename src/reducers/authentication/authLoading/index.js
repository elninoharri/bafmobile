import {
  CHECK_VERSION_PROCESS,
  CHECK_VERSION_SUCCESS,
  CHECK_VERSION_ERROR,

  CHECK_TOKEN_PROCESS,
  CHECK_TOKEN_SUCCESS,
  CHECK_TOKEN_ERROR,

  REFRESH_TOKEN_PROCESS,
  REFRESH_TOKEN_SUCCESS,
  REFRESH_TOKEN_ERROR
} from "../../../actions";

const initState = {
  result: null,
  loading: false
}

const initStates = {
  result: [],
  loading: false
}

export function checkVersion(state = initState, action) {
  switch (action.type) {
    case CHECK_VERSION_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case CHECK_VERSION_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case CHECK_VERSION_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null
      };
    default:
      return state;
  }
}

export function checkToken(state = initState, action) {
  switch (action.type) {
    case CHECK_TOKEN_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case CHECK_TOKEN_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case CHECK_TOKEN_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null
      };
    default:
      return state;
  }
}

export function refreshToken(state = initState, action) {
  switch (action.type) {
    case REFRESH_TOKEN_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case REFRESH_TOKEN_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case REFRESH_TOKEN_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null
      };
    default:
      return state;
  }
}