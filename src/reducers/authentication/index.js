import {checkVersion, checkToken, refreshToken } from './authLoading';
import {forgotPassword} from './forgotPassword';
import {login,loginOpenid,saveToken} from './login';
import {logout} from './logout';
import {
  checkPhoneNumber,
  otpCall,
  otpSms,
  otpValidate,
  register
} from './register';

export {
  checkVersion,
  checkToken,
  refreshToken,
  forgotPassword,
  login,
  loginOpenid,
  logout,
  saveToken,
  checkPhoneNumber,
  otpCall,
  otpSms,
  otpValidate,
  register,
  
};
