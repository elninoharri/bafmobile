import {
  CHECK_PHONE_NUMBER_PROCESS,
  CHECK_PHONE_NUMBER_SUCCESS,
  CHECK_PHONE_NUMBER_ERROR,
  OTP_CALL_PROCESS,
  OTP_CALL_SUCCESS,
  OTP_CALL_ERROR,
  OTP_SMS_PROCESS,
  OTP_SMS_SUCCESS,
  OTP_SMS_ERROR,
  OTP_VALIDATE_PROCESS,
  OTP_VALIDATE_SUCCESS,
  OTP_VALIDATE_ERROR,
  REGISTER_PROCESS,
  REGISTER_SUCCESS,
  REGISTER_ERROR,
} from './../../../actions/authentication';

const initState = {
  result: null,
  loading: false,
};

const initStates = {
  result: [],
  loading: false,
};

export function checkPhoneNumber(state = initState, action) {
  switch (action.type) {
    case CHECK_PHONE_NUMBER_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case CHECK_PHONE_NUMBER_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case CHECK_PHONE_NUMBER_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function otpCall(state = initState, action) {
  switch (action.type) {
    case OTP_CALL_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case OTP_CALL_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case OTP_CALL_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function otpSms(state = initState, action) {
  switch (action.type) {
    case OTP_SMS_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case OTP_SMS_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case OTP_SMS_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function otpValidate(state = initState, action) {
  switch (action.type) {
    case OTP_VALIDATE_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case OTP_VALIDATE_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case OTP_VALIDATE_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function register(state = initState, action) {
  switch (action.type) {
    case REGISTER_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case REGISTER_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}
