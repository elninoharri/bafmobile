import {
  FORGOT_PASSWORD_PROCESS,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_ERROR,
} from "../../../actions";

const initState = {
  result: null,
  loading: false
}

const initStates = {
  result: [],
  loading: false
}

export function forgotPassword(state = initState, action) {
  switch (action.type) {
    case FORGOT_PASSWORD_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case FORGOT_PASSWORD_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null
      };
    default:
      return state;
  }
}