// npm run reducerforgotPasswordTest

import {forgotPassword} from '.';

const initState = {
  result: null,
  loading: false,
};

describe('REDUCER', () => {
  it('should return the initial state', () =>
    expect(forgotPassword(undefined, {})).toEqual({...initState}));

  it('should handle "FORGOT_PASSWORD_PROCESS" action', () => {
    expect(forgotPassword({}, {type: 'FORGOT_PASSWORD_PROCESS'})).toEqual({
      ...initState,
      loading: true,
      result: null,
      error: null,
    });
  });

  it('should handle "FORGOT_PASSWORD_SUCCESS" action', () => {
    expect(forgotPassword({}, {type: 'FORGOT_PASSWORD_SUCCESS'})).toEqual({
      ...initState,
      result: undefined,
      loading: false,
      error: null,
    });
  });

  it('should handle "FORGOT_PASSWORD_ERROR" action', () => {
    expect(forgotPassword({}, {type: 'FORGOT_PASSWORD_ERROR'})).toEqual({
      ...initState,
      error: undefined,
      loading: false,
      result: null,
    });
  });
});
