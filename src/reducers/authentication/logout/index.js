import {
LOGOUT_ERROR,
LOGOUT_PROCESS,
LOGOUT_SUCCESS
} from './../../../actions/authentication/logout';

const initState = {
  result: null,
  loading: false,
};

export function logout(state = initState, action) {
  switch (action.type) {
    case LOGOUT_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case LOGOUT_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}