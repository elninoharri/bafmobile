// npm run reducerLoginTest


import {login} from ".";


const initState = {
    result: null,
    loading: false,
  };

describe('REDUCER', () => {
    it('should return the initial state', () =>
     expect(login(undefined,{})).
      toEqual({...initState})
    )

    it('should handle "LOGIN_PROCESS" action', () => {
        expect(login({}, { type: 'LOGIN_PROCESS' })).
          toEqual({ ...initState,"loading": true,"result": null,"error": null,})
    })

    it('should handle "LOGIN_SUCCESS" action', () => {
        expect(login({}, { type: 'LOGIN_SUCCESS' })).
          toEqual({...initState,result:undefined,"loading" : false,"error": null})
    })

    it('should handle "LOGIN_ERROR" action', () => {
        expect(login({}, { type: 'LOGIN_ERROR' })).
          toEqual({...initState,error: undefined,loading: false,result: null,})
    })

   
  });


