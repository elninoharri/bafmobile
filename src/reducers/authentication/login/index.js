import {
  LOGIN_PROCESS,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGIN_OPENID_PROCESS,
  LOGIN_OPENID_SUCCESS,
  LOGIN_OPENID_ERROR,
  SAVE_TOKEN_PROCESS,
  SAVE_TOKEN_SUCCESS,
  SAVE_TOKEN_ERROR,
} from './../../../actions/authentication';

const initState = {
  result: null,
  loading: false,
};

export function login(state = initState, action) {
  switch (action.type) {
    case LOGIN_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case LOGIN_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function loginOpenid(state = initState, action) {
  switch (action.type) {
    case LOGIN_OPENID_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case LOGIN_OPENID_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case LOGIN_OPENID_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function saveToken(state = initState, action) {
  switch (action.type) {
    case SAVE_TOKEN_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case SAVE_TOKEN_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case SAVE_TOKEN_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}
