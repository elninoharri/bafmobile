import {
  GET_AUTHKEY_ERROR,
  GET_AUTHKEY_PROCESS,
  GET_AUTHKEY_SUCCESS,
} from '../../actions/credoLabs';

const initState = {
  result: null,
  loading: false,
};

const initStates = {
  result: [],
  loading: false,
};

export function getAuthkey(state = initState, action) {
  switch (action.type) {
    case GET_AUTHKEY_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case GET_AUTHKEY_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_AUTHKEY_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}
