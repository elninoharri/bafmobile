import {
  OTP_REQUEST_PROCESS,
  OTP_REQUEST_SUCCESS,
  OTP_REQUEST_ERROR,
  OTP_REGENERATE_PROCESS,
  OTP_REGENERATE_SUCCESS,
  OTP_REGENERATE_ERROR,
  OTP_PRIVY_VALIDATE_ERROR,
  OTP_PRIVY_VALIDATE_PROCESS,
  OTP_PRIVY_VALIDATE_SUCCESS,
  SUBMIT_PRIVY_PROCESS,
  SUBMIT_PRIVY_ERROR,
  SUBMIT_PRIVY_SUCCESS,
  CHECK_STATUS_REGISTER_PROCESS,
  CHECK_STATUS_REGISTER_ERROR,
  CHECK_STATUS_REGISTER_SUCCESS,
  UPLOAD_DOC_PROCESS,
  UPLOAD_DOC_ERROR,
  UPLOAD_DOC_SUCCESS,
  CHECK_STATUS_UPLOAD_PROCESS,
  CHECK_STATUS_UPLOAD_ERROR,
  CHECK_STATUS_UPLOAD_SUCCESS,
  CALLBACK_DOC_SIGN_PROCESS,
  CALLBACK_DOC_SIGN_ERROR,
  CALLBACK_DOC_SIGN_SUCCESS,
  GET_CABANG_STATUS_ERROR,
  GET_CABANG_STATUS_PROCESS,
  GET_CABANG_STATUS_SUCCESS,
  UPDATE_STATUS_ORDER_PRIVY_PROCESS,
  UPDATE_STATUS_ORDER_PRIVY_SUCCESS,
  UPDATE_STATUS_ORDER_PRIVY_ERROR,
} from '../../actions/privy';

const initState = {
  result: null,
  loading: false,
};

const initStates = {
  result: [],
  loading: false,
};

export function otpRequestPrivy(state = initState, action) {
  switch (action.type) {
    case OTP_REQUEST_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case OTP_REQUEST_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case OTP_REQUEST_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function otpRegeneratePrivy(state = initState, action) {
  switch (action.type) {
    case OTP_REGENERATE_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case OTP_REGENERATE_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case OTP_REGENERATE_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function otpValidatePrivy(state = initState, action) {
  switch (action.type) {
    case OTP_PRIVY_VALIDATE_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case OTP_PRIVY_VALIDATE_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case OTP_PRIVY_VALIDATE_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function submitPrivy(state = initState, action) {
  switch (action.type) {
    case SUBMIT_PRIVY_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case SUBMIT_PRIVY_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case SUBMIT_PRIVY_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function checkStatusRegister(state = initStates, action) {
  switch (action.type) {
    case CHECK_STATUS_REGISTER_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case CHECK_STATUS_REGISTER_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case CHECK_STATUS_REGISTER_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function uploadDoc(state = initState, action) {
  switch (action.type) {
    case UPLOAD_DOC_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case UPLOAD_DOC_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case UPLOAD_DOC_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function checkStatusUpload(state = initStates, action) {
  switch (action.type) {
    case CHECK_STATUS_UPLOAD_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };

    case CHECK_STATUS_UPLOAD_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case CHECK_STATUS_UPLOAD_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function callbackDocSigning(state = initStates, action) {
  switch (action.type) {
    case CALLBACK_DOC_SIGN_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case CALLBACK_DOC_SIGN_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case CALLBACK_DOC_SIGN_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function getCabangStatus(state = initStates, action) {
  switch (action.type) {
    case GET_CABANG_STATUS_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case GET_CABANG_STATUS_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_CABANG_STATUS_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function updateStatusOrderPrivy(state = initStates, action) {
  switch (action.type) {
    case UPDATE_STATUS_ORDER_PRIVY_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case UPDATE_STATUS_ORDER_PRIVY_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
        target: action.target,
      };
    case UPDATE_STATUS_ORDER_PRIVY_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}
