import {
  DATA_BANNER_PROCESS,
  DATA_BANNER_SUCCESS,
  DATA_BANNER_ERROR,
  GET_ACTIVE_AGREEMENT_PROCESS,
  GET_ACTIVE_AGREEMENT_SUCCESS,
  GET_ACTIVE_AGREEMENT_ERROR,
  DETAIL_BANNER_PROCESS,
  DETAIL_BANNER_SUCCESS,
  DETAIL_BANNER_ERROR,
  SUBMIT_NIK_PROCESS,
  SUBMIT_NIK_SUCCESS,
  SUBMIT_NIK_ERROR,
  FILTER_BANNER,
  RESET_FILTER_BANNER,
  REFCONTENTD_ERROR,
  REFCONTENTD_PROCESS,
  REFCONTENTD_SUCCESS,

  /*****START PAYMENT HISTORY REDUCER*****/
  GET_PAYMENT_HISTORY_PROCESS,
  GET_PAYMENT_HISTORY_SUCCESS,
  GET_PAYMENT_HISTORY_ERROR,
  GET_VOUCHER_CODE_PROCESS,
  GET_VOUCHER_CODE_SUCCESS,
  GET_VOUCHER_CODE_ERROR,
  /*****END PAYMENT HISTORY REDUCER*****/
  EMAIL_BAF_CARE_PROCESS,
  EMAIL_BAF_CARE_SUCCESS,
  EMAIL_BAF_CARE_ERROR,
  GET_LIST_NOTIF_PROCESS,
  GET_LIST_NOTIF_SUCCESS,
  GET_LIST_NOTIF_ERROR,
  DELETE_NOTIF_PROCESS,
  DELETE_NOTIF_SUCCESS,
  DELETE_NOTIF_ERROR,
  READ_NOTIF_PROCESS,
  READ_NOTIF_SUCCESS,
  READ_NOTIF_ERROR,
  GET_LIST_DEALER_PROCESS,
  GET_LIST_DEALER_SUCCESS,
  GET_LIST_DEALER_ERROR,
  GET_LIST_MITRA_PROCESS,
  GET_LIST_MITRA_SUCCESS,
  GET_LIST_MITRA_ERROR,
} from '../../actions/home';

const initState = {
  result: null,
  loading: false,
};

const initStates = {
  result: [],
  loading: false,
};

export function filterBanner(state = initState, action) {
  switch (action.type) {
    case FILTER_BANNER:
      return {
        ...initState,
        result: action.data,
      };
    case RESET_FILTER_BANNER:
      return {
        ...initState,
        result: false,
      };
    default:
      return state;
  }
}

export function dataBanner(state = initStates, action) {
  switch (action.type) {
    case DATA_BANNER_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: [],
        error: null,
      };
    case DATA_BANNER_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case DATA_BANNER_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: [],
      };
    default:
      return state;
  }
}

export function detailBanner(state = initStates, action) {
  switch (action.type) {
    case DETAIL_BANNER_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: [],
        error: null,
      };
    case DETAIL_BANNER_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case DETAIL_BANNER_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: [],
      };
    default:
      return state;
  }
}

export function getVoucherCode(state = initStates, action) {
  switch (action.type) {
    case GET_VOUCHER_CODE_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case GET_VOUCHER_CODE_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_VOUCHER_CODE_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function refContentD(state = initStates, action) {
  switch (action.type) {
    case REFCONTENTD_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: [],
        error: null,
      };
    case REFCONTENTD_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case REFCONTENTD_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: [],
      };
    default:
      return state;
  }
}

export function getActiveAgreement(state = initStates, action) {
  switch (action.type) {
    case GET_ACTIVE_AGREEMENT_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: [],
        error: null,
      };
    case GET_ACTIVE_AGREEMENT_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_ACTIVE_AGREEMENT_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: [],
      };
    default:
      return state;
  }
}

export function submitNik(state = initState, action) {
  switch (action.type) {
    case SUBMIT_NIK_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case SUBMIT_NIK_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case SUBMIT_NIK_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

/*****START PAYMENT HISTORY REDUCER*****/
export function getPaymentHistory(state = initStates, action) {
  switch (action.type) {
    case GET_PAYMENT_HISTORY_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: [],
        error: null,
      };
    case GET_PAYMENT_HISTORY_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_PAYMENT_HISTORY_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: [],
      };
    default:
      return state;
  }
}
/******END PAYMENT HISTORY REDUCER******/

export function emailBafCare(state = initStates, action) {
  switch (action.type) {
    case EMAIL_BAF_CARE_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case EMAIL_BAF_CARE_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case EMAIL_BAF_CARE_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function getListDealer(state = initStates, action) {
  switch (action.type) {
    case GET_LIST_DEALER_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case GET_LIST_DEALER_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_LIST_DEALER_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function getListMitra(state = initStates, action) {
  switch (action.type) {
    case GET_LIST_MITRA_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case GET_LIST_MITRA_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_LIST_MITRA_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}


export function getListNotif(state = initStates, action) {
  switch (action.type) {
    case GET_LIST_NOTIF_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case GET_LIST_NOTIF_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_LIST_NOTIF_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function deleteNotif(state = initStates, action) {
  switch (action.type) {
    case DELETE_NOTIF_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case DELETE_NOTIF_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case DELETE_NOTIF_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function readNotif(state = initStates, action) {
  switch (action.type) {
    case READ_NOTIF_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case READ_NOTIF_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case READ_NOTIF_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}
