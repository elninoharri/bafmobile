import {
  GET_PENGAJUAN_DATA_PROCESS,
  GET_PENGAJUAN_DATA_SUCCESS,
  GET_PENGAJUAN_DATA_ERROR,

  GET_PENGAJUAN_MP_PROCESS,
  GET_PENGAJUAN_MP_SUCCESS,
  GET_PENGAJUAN_MP_ERROR,

  GET_PENGAJUAN_NMC_PROCESS,
  GET_PENGAJUAN_NMC_SUCCESS,
  GET_PENGAJUAN_NMC_ERROR,

  GET_PENGAJUAN_SYANA_PROCESS,
  GET_PENGAJUAN_SYANA_ERROR,
  GET_PENGAJUAN_SYANA_SUCCESS,

  GET_PENGAJUAN_CAR_PROCESS,
  GET_PENGAJUAN_CAR_ERROR,
  GET_PENGAJUAN_CAR_SUCCESS,

  GET_OTR_PROCESS,
  GET_OTR_SUCCESS,
  GET_OTR_ERROR,
} from '../../actions/lob/index';

const initState = {
  result: null,
  loading: false,
};

const initStates = {
  result: [],
  loading: false,
};

export function getPengajuanData(state = initState, action) {
  switch (action.type) {
    case GET_PENGAJUAN_DATA_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case GET_PENGAJUAN_DATA_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_PENGAJUAN_DATA_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}


export function getPengajuanSyana(state = initState, action) {
  switch (action.type) {
    case GET_PENGAJUAN_SYANA_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case GET_PENGAJUAN_SYANA_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_PENGAJUAN_SYANA_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function getPengajuanNMC(state = initState, action) {
  switch (action.type) {
    case GET_PENGAJUAN_NMC_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case GET_PENGAJUAN_NMC_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_PENGAJUAN_NMC_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function getPengajuanMP(state = initState, action) {
  switch (action.type) {
    case GET_PENGAJUAN_MP_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case GET_PENGAJUAN_MP_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_PENGAJUAN_MP_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function getPengajuanCar(state = initState, action) {
  switch (action.type) {
    case GET_PENGAJUAN_CAR_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case GET_PENGAJUAN_CAR_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_PENGAJUAN_CAR_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}


export function getOTR(state = initState, action) {
  switch (action.type) {
    case GET_OTR_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case GET_OTR_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_OTR_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}
