import {
TOKEN_SANDIA_ERROR,
TOKEN_SANDIA_PROCESS,
TOKEN_SANDIA_SUCCESS
} from "../../../actions/sandiaFeature/generateToken";

const initState = {
  result: null,
  loading: false
}

const initStates = {
  result: [],
  loading: false
}

export function generateToken(state = initState, action) {
  switch (action.type) {
    case TOKEN_SANDIA_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case TOKEN_SANDIA_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case TOKEN_SANDIA_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null
      };
    default:
      return state;
  }
}