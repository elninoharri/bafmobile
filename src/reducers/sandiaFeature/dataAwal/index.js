import {
  ORDER_COUNT_PROCESS,
  ORDER_COUNT_SUCCESS,
  ORDER_COUNT_ERROR,
  ORDER_FILTER_PROCESS,
  ORDER_FILTER_SUCCESS,
  ORDER_FILTER_ERROR,
  CANCEL_PO_PROSES,
  CANCEL_PO_SUCCESS,
  CANCEL_PO_ERROR,
  CONFIRM_PASS_PROCESS,
  CONFIRM_PASS_SUCCESS,
  CONFIRM_PASS_ERROR,
  QDE_DETAIL_PROCESS,
  QDE_DETAIL_SUCCESS,
  QDE_DETAIL_ERROR,
  QDE_PERSONAL_DETAIL_DATA_PROCESS,
  QDE_PERSONAL_DETAIL_DATA_SUCCESS,
  QDE_PERSONAL_DETAIL_DATA_ERROR,
  MP_MAPPING_RISK_PROCESS,
  MP_MAPPING_RISK_SUCCESS,
  MP_MAPPING_RISK_ERROR,
  MP_INST_CALCULATION_PROCESS,
  MP_INST_CALCULATION_SUCCESS,
  MP_INST_CALCULATION_ERROR,
} from '../../../actions/sandiaFeature/dataAwal';

const initState = {
  result: [],
  loading: false,
};

const initStates = {
  result: null,
  loading: false,
};

export function orderCount(state = initState, action) {
  switch (action.type) {
    case ORDER_COUNT_PROCESS:
      return {
        ...state,
        loading: true,
        error: null,
        result: [],
      };
    case ORDER_COUNT_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case ORDER_COUNT_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: [],
      };
    default:
      return state;
  }
}

export function orderFilter(state = initState, action) {
  switch (action.type) {
    case ORDER_FILTER_PROCESS:
      return {
        ...state,
        loading: true,
        error: null,
        result: [],
      };
    case ORDER_FILTER_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case ORDER_FILTER_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: [],
      };
    default:
      return state;
  }
}

export function cancelPO(state = initStates, action) {
  switch (action.type) {
    case CANCEL_PO_PROSES:
      return {
        ...state,
        loading: true,
        error: null,
        result: null,
      };
    case CANCEL_PO_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case CANCEL_PO_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function confirmPass(state = initStates, action) {
  switch (action.type) {
    case CONFIRM_PASS_PROCESS:
      return {
        ...state,
        loading: true,
        error: null,
        result: null,
      };
    case CONFIRM_PASS_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case CONFIRM_PASS_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function qdeDetailData(state = initStates, action) {
  switch (action.type) {
    case QDE_DETAIL_PROCESS:
      return {
        ...state,
        loading: true,
        error: null,
        result: null,
      };
    case QDE_DETAIL_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case QDE_DETAIL_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function qdePersonalDetailData(state = initStates, action) {
  switch (action.type) {
    case QDE_PERSONAL_DETAIL_DATA_PROCESS:
      return {
        ...state,
        loading: true,
        error: null,
        result: null,
      };
    case QDE_PERSONAL_DETAIL_DATA_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case QDE_PERSONAL_DETAIL_DATA_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function mpMappingRisk(state = initStates, action) {
  switch (action.type) {
    case MP_MAPPING_RISK_PROCESS:
      return {
        ...state,
        loading: true,
        error: null,
        result: null,
      };
    case MP_MAPPING_RISK_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case MP_MAPPING_RISK_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function mpInstCalculation(state = initStates, action) {
  switch (action.type) {
    case MP_INST_CALCULATION_PROCESS:
      return {
        ...state,
        loading: true,
        error: null,
        result: null,
      };
    case MP_INST_CALCULATION_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case MP_INST_CALCULATION_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}
