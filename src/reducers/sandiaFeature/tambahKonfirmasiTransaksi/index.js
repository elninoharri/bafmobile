import {
    VALIDATION_INVOICE_PROSES,
    VALIDATION_INVOICE_SUCCESS,
    VALIDATION_INVOICE_ERROR,
    TANGGAL_PO_PROSES,
    TANGGAL_PO_SUCCESS,
    TANGGAL_PO_ERROR,
    KONFIRMASI_TRANSAKSI_PROSES,
    KONFIRMASI_TRANSAKSI_SUCCESS,
    KONFIRMASI_TRANSAKSI_ERROR,
    TIPE_KONSUMEN_PROSES,
    TIPE_KONSUMEN_SUCCESS,
    TIPE_KONSUMEN_ERROR,
    DELIVERY_ADDRESS_PROSES,
    DELIVERY_ADDRESS_SUCCESS,
    DELIVERY_ADDRESS_ERROR,
    INFO_PENGIRIMAN_PROSES,
    INFO_PENGIRIMAN_SUCCESS,
    INFO_PENGIRIMAN_ERROR
    } from "../../../actions/sandiaFeature/tambahKonfirmasiTransaksi";
    
    const initState = {
      result: [],
      loading: false
    };
    
    export function validationInvoice(state = initState, action) {
      switch (action.type) {
        case VALIDATION_INVOICE_PROSES:
          return {
            ...state,
            loading: true,
            result: [],
            error: null
          };
        case VALIDATION_INVOICE_SUCCESS:
          return {
            ...state,
            result: action.result,
            loading: false,
            error: null
          };
        case VALIDATION_INVOICE_ERROR:
          return {
            ...state,
            error: action.error,
            result: [],
            loading: false
          };
        default:
          return state;
      }
    }

    export function tanggalPO(state = initState, action) {
      switch (action.type) {
        case TANGGAL_PO_PROSES:
          return {
            ...state,
            loading: true,
            result: [],
            error: null
          };
        case TANGGAL_PO_SUCCESS:
          return {
            ...state,
            result: action.result,
            loading: false,
            error: null
          };
        case TANGGAL_PO_ERROR:
          return {
            ...state,
            error: action.error,
            result: [],
            loading: false
          };
        default:
          return state;
      }
    }

    export function deliveryAddress(state = initState, action) {
      switch (action.type) {
        case DELIVERY_ADDRESS_PROSES:
          return {
            ...state,
            loading: true,
            result: [],
            error: null
          };
        case DELIVERY_ADDRESS_SUCCESS:
          return {
            ...state,
            result: action.result,
            loading: false,
            error: null
          };
        case DELIVERY_ADDRESS_ERROR:
          return {
            ...state,
            error: action.error,
            result: [],
            loading: false
          };
        default:
          return state;
      }
    }

    export function konfirmasiTransaksi(state = initState, action) {
      switch (action.type) {
        case KONFIRMASI_TRANSAKSI_PROSES:
          return {
            ...state,
            loading: true,
            result: [],
            error: null
          };
        case KONFIRMASI_TRANSAKSI_SUCCESS:
          return {
            ...state,
            result: action.result,
            loading: false,
            error: null
          };
        case KONFIRMASI_TRANSAKSI_ERROR:
          return {
            ...state,
            error: action.error,
            result: [],
            loading: false
          };
        default:
          return state;
      }
    }

    export function tipeKonsumen(state = initState, action) {
      switch (action.type) {
        case TIPE_KONSUMEN_PROSES:
          return {
            ...state,
            loading: true,
            result: [],
            error: null
          };
        case TIPE_KONSUMEN_SUCCESS:
          return {
            ...state,
            result: action.result,
            loading: false,
            error: null
          };
        case TIPE_KONSUMEN_ERROR:
          return {
            ...state,
            error: action.error,
            result: [],
            loading: false
          };
        default:
          return state;
      }
    }

    export function infoPengiriman(state = initState, action) {
      switch (action.type) {
        case INFO_PENGIRIMAN_PROSES:
          return {
            ...state,
            loading: true,
            result: [],
            error: null
          };
        case INFO_PENGIRIMAN_SUCCESS:
          return {
            ...state,
            result: action.result,
            loading: false,
            error: null
          };
        case INFO_PENGIRIMAN_ERROR:
          return {
            ...state,
            error: action.error,
            result: [],
            loading: false
          };
        default:
          return state;
      }
    }
  