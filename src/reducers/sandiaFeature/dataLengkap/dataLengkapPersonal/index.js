import {
    DDE_EDUCATION_PROCESS, DDE_EDUCATION_SUCCESS, DDE_EDUCATION_ERROR,
    DDE_GENDER_PROCESS, DDE_GENDER_SUCCESS, DDE_GENDER_ERROR,
    DDE_MARITALSTATUS_PROCESS, DDE_MARITALSTATUS_SUCCESS, DDE_MARITALSTATUS_ERROR,
    DDE_RELATIONSHIP_PROCESS, DDE_RELATIONSHIP_SUCCESS, DDE_RELATIONSHIP_ERROR,
    DDE_RELIGION_PROCESS, DDE_RELIGION_SUCCESS, DDE_RELIGION_ERROR,
    DDE_HOUSEOWNERSHIP_PROCESS, DDE_HOUSEOWNERSHIP_SUCCESS, DDE_HOUSEOWNERSHIP_ERROR,
    DDE_OFFICEOWNERSHIP_PROCESS, DDE_OFFICEOWNERSHIP_SUCCESS, DDE_OFFICEOWNERSHIP_ERROR
  } from "../../../../actions/sandiaFeature/dataLengkap/dataLengkapPersonal";
  
  const initState = {
    result: [],
    loading: false
  };
  
  export function ddeEducation(state = initState, action) {
    switch (action.type) {
      case DDE_EDUCATION_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_EDUCATION_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_EDUCATION_ERROR:
        return {
          ...state,
          error: action.error,
          result: [],
          loading: false
        };
      default:
        return state;
    }
  }

  export function ddeGender(state = initState, action) {
    switch (action.type) {
      case DDE_GENDER_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_GENDER_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_GENDER_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }

  export function ddeMaritalStatus(state = initState, action) {
    switch (action.type) {
      case DDE_MARITALSTATUS_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_MARITALSTATUS_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_MARITALSTATUS_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }

  export function ddeRelationship(state = initState, action) {
    switch (action.type) {
      case DDE_RELATIONSHIP_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_RELATIONSHIP_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_RELATIONSHIP_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }

  export function ddeReligion(state = initState, action) {
    switch (action.type) {
      case DDE_RELIGION_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_RELIGION_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_RELIGION_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }

  export function ddeHouseOwnership(state = initState, action) {
    switch (action.type) {
      case DDE_HOUSEOWNERSHIP_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_HOUSEOWNERSHIP_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_HOUSEOWNERSHIP_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }

  export function ddeOfficeOwnership(state = initState, action) {
    switch (action.type) {
      case DDE_OFFICEOWNERSHIP_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_OFFICEOWNERSHIP_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_OFFICEOWNERSHIP_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }