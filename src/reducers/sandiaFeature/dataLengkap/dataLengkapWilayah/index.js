import {
  DDE_LEGALPROVINCE_PROCESS, DDE_LEGALPROVINCE_SUCCESS, DDE_LEGALPROVINCE_ERROR,
  DDE_LEGALCITY_PROCESS, DDE_LEGALCITY_SUCCESS, DDE_LEGALCITY_ERROR,
  DDE_LEGALKECAMATAN_PROCESS, DDE_LEGALKECAMATAN_SUCCESS, DDE_LEGALKECAMATAN_ERROR,
  DDE_LEGALKELURAHAN_PROCESS, DDE_LEGALKELURAHAN_SUCCESS, DDE_LEGALKELURAHAN_ERROR,

  DDE_RESIDENCEPROVINCE_PROCESS, DDE_RESIDENCEPROVINCE_SUCCESS, DDE_RESIDENCEPROVINCE_ERROR,
  DDE_RESIDENCECITY_PROCESS, DDE_RESIDENCECITY_SUCCESS, DDE_RESIDENCECITY_ERROR,
  DDE_RESIDENCEKECAMATAN_PROCESS, DDE_RESIDENCEKECAMATAN_SUCCESS, DDE_RESIDENCEKECAMATAN_ERROR,
  DDE_RESIDENCEKELURAHAN_PROCESS, DDE_RESIDENCEKELURAHAN_SUCCESS, DDE_RESIDENCEKELURAHAN_ERROR,

  DDE_JOBPROVINCE_PROCESS, DDE_JOBPROVINCE_SUCCESS, DDE_JOBPROVINCE_ERROR,
  DDE_JOBCITY_PROCESS, DDE_JOBCITY_SUCCESS, DDE_JOBCITY_ERROR,
  DDE_JOBKECAMATAN_PROCESS, DDE_JOBKECAMATAN_SUCCESS, DDE_JOBKECAMATAN_ERROR,
  DDE_JOBKELURAHAN_PROCESS, DDE_JOBKELURAHAN_SUCCESS, DDE_JOBKELURAHAN_ERROR
  } from "../../../../actions/sandiaFeature/dataLengkap/dataLengkapWilayah";
  
  const initState = {
    result: [],
    loading: false
  };
  
  export function ddeLegalProvince(state = initState, action) {
    switch (action.type) {
      case DDE_LEGALPROVINCE_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_LEGALPROVINCE_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_LEGALPROVINCE_ERROR:
        return {
          ...state,
          error: action.error,
          result: [],
          loading: false
        };
      default:
        return state;
    }
  }

  export function ddeLegalCity(state = initState, action) {
    switch (action.type) {
      case DDE_LEGALCITY_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_LEGALCITY_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_LEGALCITY_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }

  export function ddeLegalKecamatan(state = initState, action) {
    switch (action.type) {
      case DDE_LEGALKECAMATAN_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_LEGALKECAMATAN_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_LEGALKECAMATAN_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }

  export function ddeLegalKelurahan(state = initState, action) {
    switch (action.type) {
      case DDE_LEGALKELURAHAN_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_LEGALKELURAHAN_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_LEGALKELURAHAN_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }

  export function ddeResidenceProvince(state = initState, action) {
    switch (action.type) {
      case DDE_RESIDENCEPROVINCE_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_RESIDENCEPROVINCE_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_RESIDENCEPROVINCE_ERROR:
        return {
          ...state,
          error: action.error,
          result: [],
          loading: false
        };
      default:
        return state;
    }
  }

  export function ddeResidenceCity(state = initState, action) {
    switch (action.type) {
      case DDE_RESIDENCECITY_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_RESIDENCECITY_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_RESIDENCECITY_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }

  export function ddeResidenceKecamatan(state = initState, action) {
    switch (action.type) {
      case DDE_RESIDENCEKECAMATAN_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_RESIDENCEKECAMATAN_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_RESIDENCEKECAMATAN_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }

  export function ddeResidenceKelurahan(state = initState, action) {
    switch (action.type) {
      case DDE_RESIDENCEKELURAHAN_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_RESIDENCEKELURAHAN_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_RESIDENCEKELURAHAN_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }

  export function ddeJobProvince(state = initState, action) {
    switch (action.type) {
      case DDE_JOBPROVINCE_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_JOBPROVINCE_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_JOBPROVINCE_ERROR:
        return {
          ...state,
          error: action.error,
          result: [],
          loading: false
        };
      default:
        return state;
    }
  }

  export function ddeJobCity(state = initState, action) {
    switch (action.type) {
      case DDE_JOBCITY_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_JOBCITY_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_JOBCITY_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }

  export function ddeJobKecamatan(state = initState, action) {
    switch (action.type) {
      case DDE_JOBKECAMATAN_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_JOBKECAMATAN_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_JOBKECAMATAN_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }

  export function ddeJobKelurahan(state = initState, action) {
    switch (action.type) {
      case DDE_JOBKELURAHAN_PROCESS:
        return {
          ...state,
          loading: true,
          result: [],
          error: null
        };
      case DDE_JOBKELURAHAN_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_JOBKELURAHAN_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: []
        };
      default:
        return state;
    }
  }