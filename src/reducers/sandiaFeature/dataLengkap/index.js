import {
    DDE_INSERT_PROSES,
    DDE_INSERT_SUCCESS,
    DDE_INSERT_ERROR,

    DDE_REVISE_PROSES,
    DDE_REVISE_SUCCESS,
    DDE_REVISE_ERROR,

    DDE_RESUBMIT_PROSES,
    DDE_RESUBMIT_SUCCESS,
    DDE_RESUBMIT_ERROR,

    DDE_DETAIL_PROSES,
    DDE_DETAIL_SUCCESS,
    DDE_DETAIL_ERROR,
  } from "../../../actions/sandiaFeature/dataLengkap";
  
  const initState = {
    result: [],
    loading: false
  };

  const initStates = {
    result: null,
    loading: false
  };
  
  
  export function ddeInsert(state = initStates, action) {
    switch (action.type) {
      case DDE_INSERT_PROSES:
        return {
          ...state,
          loading: true,
          error: null,
          result: null
        };
      case DDE_INSERT_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_INSERT_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: null
        };
      default:
        return state;
    }
  }

  export function ddeDetail(state = initStates, action) {
    switch (action.type) {
      case DDE_DETAIL_PROSES:
        return {
          ...state,
          loading: true,
          error: null,
          result: null
        };
      case DDE_DETAIL_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_DETAIL_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: null
        };
      default:
        return state;
    }
  }

  export function ddeRevise(state = initStates, action) {
    switch (action.type) {
      case DDE_REVISE_PROSES:
        return {
          ...state,
          loading: true,
          error: null,
          result: null
        };
      case DDE_REVISE_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_REVISE_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: null
        };
      default:
        return state;
    }
  }

  export function ddeResubmit(state = initStates, action) {
    switch (action.type) {
      case DDE_RESUBMIT_PROSES:
        return {
          ...state,
          loading: true,
          error: null,
          result: null
        };
      case DDE_RESUBMIT_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null
        };
      case DDE_RESUBMIT_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: null
        };
      default:
        return state;
    }
  }