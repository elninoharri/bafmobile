import {
  DDE_JAMINANJENIS_PROCESS, DDE_JAMINANJENIS_SUCCESS, DDE_JAMINANJENIS_ERROR,
  DDE_JENIS_PROCESS, DDE_JENIS_SUCCESS, DDE_JENIS_ERROR,
  DDE_MERK_PROCESS, DDE_MERK_SUCCESS, DDE_MERK_ERROR,
  DDE_SCHEMEID_PROCESS, DDE_SCHEMEID_SUCCESS, DDE_SCHEMEID_ERROR,
  DDE_FINANCIALDATA_PROCESS, DDE_FINANCIALDATA_SUCCESS, DDE_FINANCIALDATA_ERROR,
  DDE_FINANCINGPURPOSE_PROCESS, DDE_FINANCINGPURPOSE_SUCCESS, DDE_FINANCINGPURPOSE_ERROR,
  DDE_SOURCEFUNDS_PROCESS, DDE_SOURCEFUNDS_SUCCESS, DDE_SOURCEFUNDS_ERROR
} from "../../../../actions/sandiaFeature/dataLengkap/dataLengkapTransaksi";
  
const initState = {
  result: [],
  loading: false
};

const initStates = {
  result: null,
  loading: false
};
  
export function ddeJaminanJenis(state = initState, action) {
  switch (action.type) {
    case DDE_JAMINANJENIS_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_JAMINANJENIS_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_JAMINANJENIS_ERROR:
      return {
        ...state,
        error: action.error,
        result: [],
        loading: false
      };
    default:
      return state;
  }
}

export function ddeJenis(state = initState, action) {
  switch (action.type) {
    case DDE_JENIS_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_JENIS_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_JENIS_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function ddeMerk(state = initState, action) {
  switch (action.type) {
    case DDE_MERK_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_MERK_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_MERK_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function ddeSchemeId(state = initState, action) {
  switch (action.type) {
    case DDE_SCHEMEID_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_SCHEMEID_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_SCHEMEID_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function ddeFinancialData(state = initStates, action) {
  switch (action.type) {
    case DDE_FINANCIALDATA_PROCESS:
      return {
        ...state,
        loading: true,
        result: null,
        error: null
      };
    case DDE_FINANCIALDATA_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_FINANCIALDATA_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null
      };
    default:
      return state;
  }
}

export function ddeFinancingPurpose(state = initState, action) {
  switch (action.type) {
    case DDE_FINANCINGPURPOSE_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_FINANCINGPURPOSE_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_FINANCINGPURPOSE_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function ddeSourceFunds(state = initState, action) {
  switch (action.type) {
    case DDE_SOURCEFUNDS_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_SOURCEFUNDS_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_SOURCEFUNDS_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}