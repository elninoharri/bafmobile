import {
  DDE_JOBCATEGORY_PROCESS, DDE_JOBCATEGORY_SUCCESS, DDE_JOBCATEGORY_ERROR,
  DDE_JOBSTATUS_PROCESS, DDE_JOBSTATUS_SUCCESS, DDE_JOBSTATUS_ERROR,
  DDE_JOBTYPE_PROCESS, DDE_JOBTYPE_SUCCESS, DDE_JOBTYPE_ERROR,
  DDE_JOBPOSITION_PROCESS, DDE_JOBPOSITION_SUCCESS, DDE_JOBPOSITION_ERROR,
  DDE_INDUSTRYTYPE_PROCESS, DDE_INDUSTRYTYPE_SUCCESS, DDE_INDUSTRYTYPE_ERROR,
  DDE_LAMAUSAHATAHUN_PROCESS, DDE_LAMAUSAHATAHUN_SUCCESS, DDE_LAMAUSAHATAHUN_ERROR,
  DDE_LAMAUSAHABULAN_PROCESS, DDE_LAMAUSAHABULAN_SUCCESS, DDE_LAMAUSAHABULAN_ERROR,
  DDE_KONTRAKTAHUN_PROCESS, DDE_KONTRAKTAHUN_SUCCESS, DDE_KONTRAKTAHUN_ERROR,
  DDE_KONTRAKBULAN_PROCESS, DDE_KONTRAKBULAN_SUCCESS, DDE_KONTRAKBULAN_ERROR
} from "../../../../actions/sandiaFeature/dataLengkap/dataLengkapPekerjaan";
  
const initState = {
  result: [],
  loading: false
};
  
export function ddeJobCategory(state = initState, action) {
  switch (action.type) {
    case DDE_JOBCATEGORY_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_JOBCATEGORY_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_JOBCATEGORY_ERROR:
      return {
        ...state,
        error: action.error,
        result: [],
        loading: false
      };
    default:
      return state;
  }
}

export function ddeJobStatus(state = initState, action) {
  switch (action.type) {
    case DDE_JOBSTATUS_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_JOBSTATUS_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_JOBSTATUS_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function ddeJobType(state = initState, action) {
  switch (action.type) {
    case DDE_JOBTYPE_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_JOBTYPE_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_JOBTYPE_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function ddeJobPosition(state = initState, action) {
  switch (action.type) {
    case DDE_JOBPOSITION_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_JOBPOSITION_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_JOBPOSITION_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function ddeIndustryType(state = initState, action) {
  switch (action.type) {
    case DDE_INDUSTRYTYPE_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_INDUSTRYTYPE_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_INDUSTRYTYPE_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function ddeLamaUsahaTahun(state = initState, action) {
  switch (action.type) {
    case DDE_LAMAUSAHATAHUN_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_LAMAUSAHATAHUN_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_LAMAUSAHATAHUN_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function ddeLamaUsahaBulan(state = initState, action) {
  switch (action.type) {
    case DDE_LAMAUSAHABULAN_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_LAMAUSAHABULAN_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_LAMAUSAHABULAN_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function ddeKontrakTahun(state = initState, action) {
  switch (action.type) {
    case DDE_KONTRAKTAHUN_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_KONTRAKTAHUN_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_KONTRAKTAHUN_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function ddeKontrakBulan(state = initState, action) {
  switch (action.type) {
    case DDE_KONTRAKBULAN_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case DDE_KONTRAKBULAN_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case DDE_KONTRAKBULAN_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}