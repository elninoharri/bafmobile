import {
    REVISI_KONFIRMASI_TRANSAKSI_PROSES,
    REVISI_KONFIRMASI_TRANSAKSI_SUCCESS,
    REVISI_KONFIRMASI_TRANSAKSI_ERROR
    } from "../../../actions/sandiaFeature/revisiKonfirmasiTransaksi";
    
    const initState = {
      result: [],
      loading: false
    };

    export function revisiKonfirmasiTransaksi(state = initState, action) {
      switch (action.type) {
        case REVISI_KONFIRMASI_TRANSAKSI_PROSES:
          return {
            ...state,
            loading: true,
            result: [],
            error: null
          };
        case REVISI_KONFIRMASI_TRANSAKSI_SUCCESS:
          return {
            ...state,
            result: action.result,
            loading: false,
            error: null
          };
        case REVISI_KONFIRMASI_TRANSAKSI_ERROR:
          return {
            ...state,
            error: action.error,
            result: [],
            loading: false
          };
        default:
          return state;
      }
    }
