import {
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_PROCESS,
  CHANGE_PASSWORD_ERROR,
} from '../../../actions';

const initState = {
  result: null,
  loading: false,
};

const initStates = {
  result: [],
  loading: false,
};

export function changePassword(state = initState, action) {
  switch (action.type) {
    case CHANGE_PASSWORD_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case CHANGE_PASSWORD_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}
