import {changePassword} from './changePassword';
import {detailUser, getCity, editUser} from './changeProfile';
import {
  getBPKB,
  submitBPKB,
  timeSlotBPKB,
  checkAvailBook,
  checkHoliday,
  getListBPKBInProgress,
  getListBPKBDone,
  cancelBpkb,
  rescheduleBpkb,
  getdetailBpkb,
} from './bpkbRequest';

export {
  changePassword,
  detailUser,
  getCity,
  editUser,
  getBPKB,
  submitBPKB,
  timeSlotBPKB,
  checkAvailBook,
  checkHoliday,
  getListBPKBInProgress,
  getListBPKBDone,
  cancelBpkb,
  rescheduleBpkb,
  getdetailBpkb,
};
