import {
  GET_BPKB_ERROR,
  GET_BPKB_PROCESS,
  GET_BPKB_SUCCESS,
  SUBMIT_BPKB_ERROR,
  SUBMIT_BPKB_PROCESS,
  SUBMIT_BPKB_SUCCESS,
  TIME_SLOT_BPKB_PROCESS,
  TIME_SLOT_BPKB_SUCCESS,
  TIME_SLOT_BPKB_ERROR,
  CHECK_AVAIL_BOOK_ERROR,
  CHECK_AVAIL_BOOK_PROCESS,
  CHECK_AVAIL_BOOK_SUCCESS,
  CHECK_HOLIDAY_ERROR,
  CHECK_HOLIDAY_PROCESS,
  CHECK_HOLIDAY_SUCCESS,
  GET_LIST_BPKB_INPROGRESS_ERROR,
  GET_LIST_BPKB_INPROGRESS_PROCESS,
  GET_LIST_BPKB_INPROGRESS_SUCCESS,
  GET_LIST_BPKB_DONE_ERROR,
  GET_LIST_BPKB_DONE_PROCESS,
  GET_LIST_BPKB_DONE_SUCCESS,
  CANCEL_BPKB_ERROR,
  CANCEL_BPKB_PROCESS,
  CANCEL_BPKB_SUCCESS,
  RESCHEDULE_BPKB_ERROR,
  RESCHEDULE_BPKB_PROCESS,
  RESCHEDULE_BPKB_SUCCESS,
  GET_DETAIL_BPKB_PROCESS,
  GET_DETAIL_BPKB_ERROR,
  GET_DETAIL_BPKB_SUCCESS

} from '../../../actions';

const initState = {
  result: null,
  loading: false,
};

const initStates = {
  result: [],
  loading: false,
};

export function timeSlotBPKB(state = initState, action) {
  switch (action.type) {
    case TIME_SLOT_BPKB_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case TIME_SLOT_BPKB_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case TIME_SLOT_BPKB_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function getBPKB(state = initState, action) {
  switch (action.type) {
    case GET_BPKB_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case GET_BPKB_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_BPKB_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function submitBPKB(state = initState, action) {
  switch (action.type) {
    case SUBMIT_BPKB_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case SUBMIT_BPKB_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case SUBMIT_BPKB_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function checkAvailBook(state = initState, action) {
  switch (action.type) {
    case CHECK_AVAIL_BOOK_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case CHECK_AVAIL_BOOK_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case CHECK_AVAIL_BOOK_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function checkHoliday(state = initState, action) {
  switch (action.type) {
    case CHECK_HOLIDAY_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case CHECK_HOLIDAY_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case CHECK_HOLIDAY_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function getListBPKBInProgress(state = initState, action) {
  switch (action.type) {
    case GET_LIST_BPKB_INPROGRESS_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case GET_LIST_BPKB_INPROGRESS_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_LIST_BPKB_INPROGRESS_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function getListBPKBDone(state = initState, action) {
  switch (action.type) {
    case GET_LIST_BPKB_DONE_PROCESS:
      return {
        ...initStates,
        loading: true,
        result: null,
        error: null,
      };
    case GET_LIST_BPKB_DONE_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_LIST_BPKB_DONE_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function cancelBpkb(state = initState, action) {
  switch (action.type) {
    case CANCEL_BPKB_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case CANCEL_BPKB_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case CANCEL_BPKB_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function rescheduleBpkb(state = initState, action) {
  switch (action.type) {
    case RESCHEDULE_BPKB_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case RESCHEDULE_BPKB_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case RESCHEDULE_BPKB_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function getdetailBpkb(state = initState, action) {
  switch (action.type) {
    case GET_DETAIL_BPKB_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case GET_DETAIL_BPKB_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_DETAIL_BPKB_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

