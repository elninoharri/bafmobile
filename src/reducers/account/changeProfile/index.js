import {
  DETAIL_USER_PROCESS,
  DETAIL_USER_SUCCESS,
  DETAIL_USER_ERROR,
  GET_CITY_ERROR,
  GET_CITY_PROCESS,
  GET_CITY_SUCCESS,
  EDIT_USER_ERROR,
  EDIT_USER_PROCESS,
  EDIT_USER_SUCCESS,
} from '../../../actions/account/changeProfile';

const initState = {
  result: null,
  loading: false,
};

const initStates = {
  result: [],
  loading: false,
};

export function detailUser(state = initState, action) {
  switch (action.type) {
    case DETAIL_USER_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case DETAIL_USER_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case DETAIL_USER_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function getCity(state = initState, action) {
  switch (action.type) {
    case GET_CITY_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case GET_CITY_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_CITY_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function editUser(state = initState, action) {
  switch (action.type) {
    case EDIT_USER_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case EDIT_USER_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case EDIT_USER_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}
