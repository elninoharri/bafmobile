import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import {
  checkVersion,
  checkToken,
  forgotPassword,
  login,
  loginOpenid,
  logout,
  checkPhoneNumber,
  otpCall,
  otpSms,
  otpValidate,
  register,
  saveToken,
  refreshToken,
} from './../reducers/authentication';

import {
  changePassword,
  detailUser,
  editUser,
  getCity,
  getBPKB,
  submitBPKB,
  timeSlotBPKB,
  checkAvailBook,
  checkHoliday,
  getListBPKBInProgress,
  getListBPKBDone,
  cancelBpkb,
  rescheduleBpkb,
  getdetailBpkb,
} from './../reducers/account';

import {
  dataBanner,
  detailBanner,
  refContentD,
  getActiveAgreement,
  submitNik,
  filterBanner,
  getVoucherCode,
  /*****START PAYMENT HISTORY REDUCER*****/
  getPaymentHistory,
  /*****END PAYMENT HISTORY REDUCER*****/
  emailBafCare,
  getListNotif,
  deleteNotif,
  readNotif,
  getListDealer,
  getListMitra,
} from './../reducers/home';

import {
  getCabangStatus,
  submitPrivy,
  otpRegeneratePrivy,
  otpRequestPrivy,
  otpValidatePrivy,
  callbackDocSigning,
  checkStatusRegister,
  checkStatusUpload,
  uploadDoc,
  updateStatusOrderPrivy,
} from './../reducers/privy';

import {
  orderCount,
  orderFilter,
  qdeDetailData,
  qdePersonalDetailData,
  confirmPass,
  mpMappingRisk,
  mpInstCalculation,
  cancelPO,
} from './../reducers/sandiaFeature/dataAwal';
import {
  ddeDetail,
  ddeInsert,
  ddeResubmit,
  ddeRevise,
} from '../reducers/sandiaFeature/dataLengkap';
import {
  ddeEducation,
  ddeGender,
  ddeMaritalStatus,
  ddeRelationship,
  ddeReligion,
  ddeHouseOwnership,
  ddeOfficeOwnership,
} from './../reducers/sandiaFeature/dataLengkap/dataLengkapPersonal';
import {
  ddeLegalProvince,
  ddeLegalCity,
  ddeLegalKecamatan,
  ddeLegalKelurahan,
  ddeResidenceProvince,
  ddeResidenceCity,
  ddeResidenceKecamatan,
  ddeResidenceKelurahan,
  ddeJobProvince,
  ddeJobCity,
  ddeJobKecamatan,
  ddeJobKelurahan,
} from './../reducers/sandiaFeature/dataLengkap/dataLengkapWilayah';
import {
  ddeJobCategory,
  ddeJobStatus,
  ddeJobType,
  ddeJobPosition,
  ddeIndustryType,
  ddeLamaUsahaTahun,
  ddeLamaUsahaBulan,
  ddeKontrakTahun,
  ddeKontrakBulan,
} from './../reducers/sandiaFeature/dataLengkap/dataLengkapPekerjaan';
import {
  ddeJaminanJenis,
  ddeJenis,
  ddeMerk,
  ddeSchemeId,
  ddeFinancialData,
  ddeFinancingPurpose,
  ddeSourceFunds,
} from './../reducers/sandiaFeature/dataLengkap/dataLengkapTransaksi';

import {revisiKonfirmasiTransaksi} from './sandiaFeature/revisiKonfirmasiTransaksi';
import {
  validationInvoice,
  konfirmasiTransaksi,
  tipeKonsumen,
  deliveryAddress,
  infoPengiriman,
  tanggalPO,
} from './sandiaFeature/tambahKonfirmasiTransaksi';

import {generateToken} from './sandiaFeature/generateToken';

import {getAuthkey} from './../reducers/credoLabs';

import {getPengajuanData,getPengajuanCar, getPengajuanMP, getPengajuanNMC,getPengajuanSyana, getOTR} from './../reducers/lob';
import {getBafPointValueReducer} from "./bafPoint/bafPointInfo";

const allReducers = combineReducers({
  checkVersion,
  checkToken,
  getAuthkey,
  generateToken,
  login,
  loginOpenid,
  logout,
  cancelPO,
  saveToken,
  refreshToken,
  changePassword,
  forgotPassword,
  detailUser,
  checkPhoneNumber,
  otpCall,
  otpSms,
  otpValidate,
  register,
  getCity,
  editUser,
  filterBanner,
  dataBanner,
  refContentD,
  detailBanner,
  getActiveAgreement,
  submitNik,
  getVoucherCode,

  getCabangStatus,
  submitPrivy,
  otpRegeneratePrivy,
  otpRequestPrivy,
  otpValidatePrivy,
  callbackDocSigning,
  checkStatusRegister,
  checkStatusUpload,
  uploadDoc,

  orderCount,
  orderFilter,
  confirmPass,
  qdeDetailData,
  qdePersonalDetailData,
  mpMappingRisk,
  mpInstCalculation,

  ddeEducation,
  ddeGender,
  ddeMaritalStatus,
  ddeRelationship,
  ddeReligion,
  ddeHouseOwnership,
  ddeOfficeOwnership,

  ddeLegalProvince,
  ddeLegalCity,
  ddeLegalKecamatan,
  ddeLegalKelurahan,
  ddeResidenceProvince,
  ddeResidenceCity,
  ddeResidenceKecamatan,
  ddeResidenceKelurahan,
  ddeJobProvince,
  ddeJobCity,
  ddeJobKecamatan,
  ddeJobKelurahan,

  ddeJobCategory,
  ddeJobStatus,
  ddeJobType,
  ddeJobPosition,
  ddeIndustryType,
  ddeLamaUsahaTahun,
  ddeLamaUsahaBulan,
  ddeKontrakTahun,
  ddeKontrakBulan,

  updateStatusOrderPrivy,
  getListDealer,
  ddeJaminanJenis,
  ddeJenis,
  ddeMerk,
  ddeSchemeId,
  ddeFinancialData,
  ddeFinancingPurpose,
  ddeSourceFunds,
  getPengajuanData,
  getPengajuanCar,
  getPengajuanMP,
  getPengajuanNMC,
  getPengajuanSyana,
  getOTR,

  getBPKB,
  submitBPKB,
  timeSlotBPKB,
  checkAvailBook,
  checkHoliday,
  getListBPKBInProgress,
  getListBPKBDone,
  cancelBpkb,
  rescheduleBpkb,
  getdetailBpkb,

  revisiKonfirmasiTransaksi,

  validationInvoice,
  tanggalPO,
  konfirmasiTransaksi,
  tipeKonsumen,
  deliveryAddress,
  infoPengiriman,
  revisiKonfirmasiTransaksi,

  ddeDetail,
  ddeInsert,
  ddeResubmit,
  ddeRevise,

  /*****START PAYMENT HISTORY REDUCER*****/
  getPaymentHistory,
  /*****END PAYMENT HISTORY REDUCER*****/
  emailBafCare,
  getListNotif,
  deleteNotif,
  readNotif,
  getListMitra,

  /* BAF POINT */
  getBafPointValueReducer,

  form: formReducer,
});
export default allReducers;
