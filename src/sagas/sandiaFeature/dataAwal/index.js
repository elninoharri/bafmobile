import {takeLatest, put} from 'redux-saga/effects';
import {
  ORDER_COUNT_PROCESS,
  ORDER_COUNT_SUCCESS,
  ORDER_COUNT_ERROR,
  ORDER_FILTER_PROCESS,
  ORDER_FILTER_SUCCESS,
  ORDER_FILTER_ERROR,
  CANCEL_PO_PROSES,
  CANCEL_PO_SUCCESS,
  CANCEL_PO_ERROR,
  CONFIRM_PASS_PROCESS,
  CONFIRM_PASS_SUCCESS,
  CONFIRM_PASS_ERROR,
  QDE_DETAIL_PROCESS,
  QDE_DETAIL_SUCCESS,
  QDE_DETAIL_ERROR,
  QDE_PERSONAL_DETAIL_DATA_PROCESS,
  QDE_PERSONAL_DETAIL_DATA_SUCCESS,
  QDE_PERSONAL_DETAIL_DATA_ERROR,
  MP_MAPPING_RISK_PROCESS,
  MP_MAPPING_RISK_SUCCESS,
  MP_MAPPING_RISK_ERROR,
  MP_INST_CALCULATION_PROCESS,
  MP_INST_CALCULATION_SUCCESS,
  MP_INST_CALCULATION_ERROR,
} from '../../../actions/sandiaFeature/dataAwal';

import {
  filterFetch,
  filterFetchCommon,
  filterFetchStatusload,
  filterFetchStatus,
  filterFetchOrder,
  filterFetchCancel,
} from '../../../utils/apiFetch';
import {
  API_HEADERS_COMMON,
  API_HEADERS_ORDER,
  API_URL_SANDIAWEB,
  API_URL_TRANSACTION,
  API_TIMEOUT,
  API_HEADERS_CORE,
  API_URL_NONUAT,
  API_URL_UAT,
  API_KEY,
  API_HEADERS_USRMGMT,
  API_URL_USR_MNGMNT,
} from '../../../utils/constant';
import {resJsonParser} from '../../../utils/utilization';

var API_HEADERS_COMMON_MOD = API_HEADERS_COMMON;

function* orderCount(action) {
  try {
    const result = yield filterFetchCommon(
      API_URL_TRANSACTION + 'getListOrderMobile',
      {
        method: 'POST',
        body: JSON.stringify(action.data),
        timeout: API_TIMEOUT,
        headers: API_HEADERS_COMMON,
      },
    );
    result == null ? (result = []) : result;
    yield put({
      type: ORDER_COUNT_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: ORDER_COUNT_ERROR,
      error: error,
    });
  }
}

function* orderFilter(action) {
  try {
    const result = yield filterFetchCommon(
      API_URL_TRANSACTION + 'getListOrderMobile',
      {
        method: 'POST',
        body: JSON.stringify(action.data),
        timeout: API_TIMEOUT,
        headers: API_HEADERS_COMMON,
      },
    );
    result == null ? (result = []) : result;
    yield put({
      type: ORDER_FILTER_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: ORDER_FILTER_ERROR,
      error: error,
    });
  }
}

function* cancelPO(action) {
  try {
    const result = yield filterFetchCancel(API_URL_NONUAT + 'Order/CancelTrx', {
      method: 'POST',
      headers: API_HEADERS_ORDER,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: CANCEL_PO_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: CANCEL_PO_ERROR,
      error: error,
    });
  }
}

function* confirmPass(action) {
  try {
    const result = yield filterFetch(
      API_URL_USR_MNGMNT + 'KonfirmasiPassword',
      {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
          'x-api-key': API_KEY,
          Authorization: 'Bearer ' + action.token,
        },
        timeout: API_TIMEOUT,

        body: JSON.stringify(action.data),
      },
    );
    yield put({
      type: CONFIRM_PASS_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: CONFIRM_PASS_ERROR,
      error: error,
    });
  }
}

function* qdeDetailData(action) {
  //add new authorization in API_HEADERS_COMMON
  API_HEADERS_COMMON_MOD.Authorization = 'Bearer ' + action.token;
  try {
    const result = yield filterFetchCommon(
      API_URL_SANDIAWEB + 'Common/getQuickOrder',
      {
        method: 'POST',
        headers: API_HEADERS_COMMON_MOD,
        body: JSON.stringify(action.data),
        timeout: API_TIMEOUT,
      },
    );
    var resultParser = resJsonParser(result);
    yield put({
      type: QDE_DETAIL_SUCCESS,
      result: resultParser.QUESTION,
    });
  } catch (error) {
    yield put({
      type: QDE_DETAIL_ERROR,
      error: error,
    });
  }
}

function* qdePersonalDetailData(action) {
  try {
    const result = yield filterFetchCommon(
      API_URL_TRANSACTION + 'getPersonalData',
      {
        method: 'POST',
        headers: API_HEADERS_COMMON,
        body: JSON.stringify(action.data),
        timeout: API_TIMEOUT,
      },
    );
    var resultParser = resJsonParser(result);
    yield put({
      type: QDE_PERSONAL_DETAIL_DATA_SUCCESS,
      result: resultParser.QUESTION,
    });
  } catch (error) {
    yield put({
      type: QDE_PERSONAL_DETAIL_DATA_ERROR,
      error: error,
    });
  }
}

function* mpMappingRisk(action) {
  try {
    const result = yield filterFetchStatusload(
      API_URL_TRANSACTION + 'getMappingRisk',
      {
        method: 'POST',
        headers: API_HEADERS_COMMON,
        body: JSON.stringify(action.data),
        timeout: API_TIMEOUT,
      },
    );
    yield put({
      type: MP_MAPPING_RISK_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: MP_MAPPING_RISK_ERROR,
      error: error,
    });
  }
}

function* mpInstCalculation(action) {
  try {
    const result = yield filterFetchStatus(
      API_URL_TRANSACTION + 'getInstallmentCalc',
      {
        method: 'POST',
        headers: API_HEADERS_CORE,
        body: JSON.stringify(action.data),
        timeout: API_TIMEOUT,
      },
    );
    yield put({
      type: MP_INST_CALCULATION_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: MP_INST_CALCULATION_ERROR,
      error: error,
    });
  }
}

export function* watchOrderCount() {
  yield takeLatest(ORDER_COUNT_PROCESS, orderCount);
}

export function* watchOrderFilter() {
  yield takeLatest(ORDER_FILTER_PROCESS, orderFilter);
}

export function* watchQdeDetailData() {
  yield takeLatest(QDE_DETAIL_PROCESS, qdeDetailData);
}

export function* watchQdePersonalDetailData() {
  yield takeLatest(QDE_PERSONAL_DETAIL_DATA_PROCESS, qdePersonalDetailData);
}

export function* watchMpMappingRisk() {
  yield takeLatest(MP_MAPPING_RISK_PROCESS, mpMappingRisk);
}

export function* watchMpInstCalculation() {
  yield takeLatest(MP_INST_CALCULATION_PROCESS, mpInstCalculation);
}

export function* watchCancelPO() {
  yield takeLatest(CANCEL_PO_PROSES, cancelPO);
}

export function* watchConfirmPO() {
  yield takeLatest(CONFIRM_PASS_PROCESS, confirmPass);
}
