import { takeLatest, put } from "redux-saga/effects";
import {
    DDE_LEGALPROVINCE_PROCESS, DDE_LEGALPROVINCE_SUCCESS, DDE_LEGALPROVINCE_ERROR,
    DDE_LEGALCITY_PROCESS, DDE_LEGALCITY_SUCCESS, DDE_LEGALCITY_ERROR,
    DDE_LEGALKECAMATAN_PROCESS, DDE_LEGALKECAMATAN_SUCCESS, DDE_LEGALKECAMATAN_ERROR,
    DDE_LEGALKELURAHAN_PROCESS, DDE_LEGALKELURAHAN_SUCCESS, DDE_LEGALKELURAHAN_ERROR,

    DDE_RESIDENCEPROVINCE_PROCESS, DDE_RESIDENCEPROVINCE_SUCCESS, DDE_RESIDENCEPROVINCE_ERROR,
    DDE_RESIDENCECITY_PROCESS, DDE_RESIDENCECITY_SUCCESS, DDE_RESIDENCECITY_ERROR,
    DDE_RESIDENCEKECAMATAN_PROCESS, DDE_RESIDENCEKECAMATAN_SUCCESS, DDE_RESIDENCEKECAMATAN_ERROR,
    DDE_RESIDENCEKELURAHAN_PROCESS, DDE_RESIDENCEKELURAHAN_SUCCESS, DDE_RESIDENCEKELURAHAN_ERROR,

    DDE_JOBPROVINCE_PROCESS, DDE_JOBPROVINCE_SUCCESS, DDE_JOBPROVINCE_ERROR,
    DDE_JOBCITY_PROCESS, DDE_JOBCITY_SUCCESS, DDE_JOBCITY_ERROR,
    DDE_JOBKECAMATAN_PROCESS, DDE_JOBKECAMATAN_SUCCESS, DDE_JOBKECAMATAN_ERROR,
    DDE_JOBKELURAHAN_PROCESS, DDE_JOBKELURAHAN_SUCCESS, DDE_JOBKELURAHAN_ERROR
} from "../../../../actions/sandiaFeature/dataLengkap/dataLengkapWilayah";
import { filterFetchCore } from "../../../../utils/apiFetch"
import { API_HEADERS_CORE, API_TIMEOUT, API_URL_SANDIAWEB } from "../../../../utils/constant"

function* ddeLegalProvince(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getProvince', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_LEGALPROVINCE_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_LEGALPROVINCE_ERROR,
          error: error
        });
    }
}

function* ddeLegalCity(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getCity', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_LEGALCITY_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_LEGALCITY_ERROR,
          error: error
        });
    }
}

function* ddeLegalKecamatan(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getKecamatan', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_LEGALKECAMATAN_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_LEGALKECAMATAN_ERROR,
          error: error
        });
    }
}

function* ddeLegalKelurahan(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getKelurahan', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_LEGALKELURAHAN_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_LEGALKELURAHAN_ERROR,
          error: error
        });
    }
}

function* ddeResidenceProvince(action) {
  try {
      const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getProvince', {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data)
      });
      yield put({
        type: DDE_RESIDENCEPROVINCE_SUCCESS,
        result: result
      });
  } catch (error) {
      yield put({
        type: DDE_RESIDENCEPROVINCE_ERROR,
        error: error
      });
  }
}

function* ddeResidenceCity(action) {
  try {
      const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getCity', {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data)
      });
      yield put({
        type: DDE_RESIDENCECITY_SUCCESS,
        result: result
      });
  } catch (error) {
      yield put({
        type: DDE_RESIDENCECITY_ERROR,
        error: error
      });
  }
}

function* ddeResidenceKecamatan(action) {
  try {
      const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getKecamatan', {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data)
      });
      yield put({
        type: DDE_RESIDENCEKECAMATAN_SUCCESS,
        result: result
      });
  } catch (error) {
      yield put({
        type: DDE_RESIDENCEKECAMATAN_ERROR,
        error: error
      });
  }
}

function* ddeResidenceKelurahan(action) {
  try {
      const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getKelurahan', {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data)
      });
      yield put({
        type: DDE_RESIDENCEKELURAHAN_SUCCESS,
        result: result
      });
  } catch (error) {
      yield put({
        type: DDE_RESIDENCEKELURAHAN_ERROR,
        error: error
      });
  }
}

function* ddeJobProvince(action) {
  try {
      const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getProvince', {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data)
      });
      yield put({
        type: DDE_JOBPROVINCE_SUCCESS,
        result: result
      });
  } catch (error) {
      yield put({
        type: DDE_JOBPROVINCE_ERROR,
        error: error
      });
  }
}

function* ddeJobCity(action) {
  try {
      const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getCity', {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data)
      });
      yield put({
        type: DDE_JOBCITY_SUCCESS,
        result: result
      });
  } catch (error) {
      yield put({
        type: DDE_JOBCITY_ERROR,
        error: error
      });
  }
}

function* ddeJobKecamatan(action) {
  try {
      const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getKecamatan', {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data)
      });
      yield put({
        type: DDE_JOBKECAMATAN_SUCCESS,
        result: result
      });
  } catch (error) {
      yield put({
        type: DDE_JOBKECAMATAN_ERROR,
        error: error
      });
  }
}

function* ddeJobKelurahan(action) {
  try {
      const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getKelurahan', {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data)
      });
      yield put({
        type: DDE_JOBKELURAHAN_SUCCESS,
        result: result
      });
  } catch (error) {
      yield put({
        type: DDE_JOBKELURAHAN_ERROR,
        error: error
      });
  }
}


export function* watchDdeLegalProvince() {
  yield takeLatest(DDE_LEGALPROVINCE_PROCESS, ddeLegalProvince);
}

export function* watchDdeLegalCity() {
  yield takeLatest(DDE_LEGALCITY_PROCESS, ddeLegalCity);
}

export function* watchDdeLegalKecamatan() {
  yield takeLatest(DDE_LEGALKECAMATAN_PROCESS, ddeLegalKecamatan);
}

export function* watchDdeLegalKelurahan() {
  yield takeLatest(DDE_LEGALKELURAHAN_PROCESS, ddeLegalKelurahan);
}
  
export function* watchDdeResidenceProvince() {
  yield takeLatest(DDE_RESIDENCEPROVINCE_PROCESS, ddeResidenceProvince);
}

export function* watchDdeResidenceCity() {
  yield takeLatest(DDE_RESIDENCECITY_PROCESS, ddeResidenceCity);
}

export function* watchDdeResidenceKecamatan() {
  yield takeLatest(DDE_RESIDENCEKECAMATAN_PROCESS, ddeResidenceKecamatan);
}

export function* watchDdeResidenceKelurahan() {
  yield takeLatest(DDE_RESIDENCEKELURAHAN_PROCESS, ddeResidenceKelurahan);
}
    
export function* watchDdeJobProvince() {
  yield takeLatest(DDE_JOBPROVINCE_PROCESS, ddeJobProvince);
}

export function* watchDdeJobCity() {
  yield takeLatest(DDE_JOBCITY_PROCESS, ddeJobCity);
}

export function* watchDdeJobKecamatan() {
  yield takeLatest(DDE_JOBKECAMATAN_PROCESS, ddeJobKecamatan);
}

export function* watchDdeJobKelurahan() {
  yield takeLatest(DDE_JOBKELURAHAN_PROCESS, ddeJobKelurahan);
}
    