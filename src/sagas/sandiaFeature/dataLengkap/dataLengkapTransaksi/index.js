import {takeLatest, put} from 'redux-saga/effects';
import {
  DDE_JAMINANJENIS_PROCESS,
  DDE_JAMINANJENIS_SUCCESS,
  DDE_JAMINANJENIS_ERROR,
  DDE_JENIS_PROCESS,
  DDE_JENIS_SUCCESS,
  DDE_JENIS_ERROR,
  DDE_MERK_PROCESS,
  DDE_MERK_SUCCESS,
  DDE_MERK_ERROR,
  DDE_SCHEMEID_PROCESS,
  DDE_SCHEMEID_SUCCESS,
  DDE_SCHEMEID_ERROR,
  DDE_FINANCIALDATA_PROCESS,
  DDE_FINANCIALDATA_SUCCESS,
  DDE_FINANCIALDATA_ERROR,
  DDE_FINANCINGPURPOSE_PROCESS,
  DDE_FINANCINGPURPOSE_SUCCESS,
  DDE_FINANCINGPURPOSE_ERROR,
  DDE_SOURCEFUNDS_PROCESS,
  DDE_SOURCEFUNDS_SUCCESS,
  DDE_SOURCEFUNDS_ERROR,
} from '../../../../actions/sandiaFeature/dataLengkap/dataLengkapTransaksi';
import {
  filterFetchCore,
  filterFetchFinancialData,
} from '../../../../utils/apiFetch';
import {
  API_HEADERS_CORE,
  API_TIMEOUT,
  API_URL_SANDIAWEB,
  API_URL_TRANSACTION,
} from '../../../../utils/constant';

function* ddeJaminanJenis(action) {
  try {
    const result = yield filterFetchCore(API_URL_TRANSACTION + 'getJaminan', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: DDE_JAMINANJENIS_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: DDE_JAMINANJENIS_ERROR,
      error: error,
    });
  }
}

function* ddeJenis(action) {
  try {
    const result = yield filterFetchCore(API_URL_TRANSACTION + 'getJenis', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: DDE_JENIS_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: DDE_JENIS_ERROR,
      error: error,
    });
  }
}

function* ddeMerk(action) {
  try {
    const result = yield filterFetchCore(API_URL_TRANSACTION + 'getMerk', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: DDE_MERK_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: DDE_MERK_ERROR,
      error: error,
    });
  }
}

function* ddeSchemeId(action) {
  try {
    const result = yield filterFetchCore(
      API_URL_SANDIAWEB + 'core/getProgram',
      {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data),
      },
    );
    yield put({
      type: DDE_SCHEMEID_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: DDE_SCHEMEID_ERROR,
      error: error,
    });
  }
}

function* ddeFinancialData(action) {
  try {
    const result = yield filterFetchFinancialData(
      API_URL_SANDIAWEB + 'core/getFinancialData',
      {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data),
      },
    );
    yield put({
      type: DDE_FINANCIALDATA_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: DDE_FINANCIALDATA_ERROR,
      error: error,
    });
  }
}

function* ddeFinancingPurpose(action) {
  try {
    const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: DDE_FINANCINGPURPOSE_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: DDE_FINANCINGPURPOSE_ERROR,
      error: error,
    });
  }
}

function* ddeSourceFunds(action) {
  try {
    const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: DDE_SOURCEFUNDS_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: DDE_SOURCEFUNDS_ERROR,
      error: error,
    });
  }
}

export function* watchDdeJaminanJenis() {
  yield takeLatest(DDE_JAMINANJENIS_PROCESS, ddeJaminanJenis);
}

export function* watchDdeJenis() {
  yield takeLatest(DDE_JENIS_PROCESS, ddeJenis);
}

export function* watchDdeMerk() {
  yield takeLatest(DDE_MERK_PROCESS, ddeMerk);
}

export function* watchDdeSchemeId() {
  yield takeLatest(DDE_SCHEMEID_PROCESS, ddeSchemeId);
}

export function* watchDdeFinancialData() {
  yield takeLatest(DDE_FINANCIALDATA_PROCESS, ddeFinancialData);
}

export function* watchDdeFinancingPurpose() {
  yield takeLatest(DDE_FINANCINGPURPOSE_PROCESS, ddeFinancingPurpose);
}

export function* watchDdeSourceFunds() {
  yield takeLatest(DDE_SOURCEFUNDS_PROCESS, ddeSourceFunds);
}
