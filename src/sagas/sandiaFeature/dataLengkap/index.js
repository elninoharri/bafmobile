import { takeLatest, put } from "redux-saga/effects";
import {
  DDE_FILTER_PROSES,
  DDE_FILTER_SUCCESS,
  DDE_FILTER_ERROR,

  DDE_DETAIL_PROSES,
  DDE_DETAIL_SUCCESS,
  DDE_DETAIL_ERROR,

  DDE_GENERAL_SETTING_PROSES,
  DDE_GENERAL_SETTING_SUCCESS,
  DDE_GENERAL_SETTING_ERROR,

  DDE_INSERT_PROSES,
  DDE_INSERT_SUCCESS,
  DDE_INSERT_ERROR,

  DDE_REVISE_PROSES,
  DDE_REVISE_SUCCESS,
  DDE_REVISE_ERROR,

  DDE_RESUBMIT_PROSES,
  DDE_RESUBMIT_SUCCESS,
  DDE_RESUBMIT_ERROR
} from "../../../actions/sandiaFeature/dataLengkap";

import { filterFetchSubmitOrder, filterFetchCommon } from "../../../utils/apiFetch"
import { API_HEADERS_ORDER, API_URL_SANDIAWEB, API_HEADERS_COMMON } from "../../../utils/constant";
import { resJsonParser } from '../../../utils/utilization';

function* ddeInsert(action) {
  try {
    const result = yield filterFetchSubmitOrder(API_URL_SANDIAWEB + 'Order/InsertDDE', {
      method: 'POST',
      headers: API_HEADERS_ORDER,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: DDE_INSERT_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: DDE_INSERT_ERROR,
      error: error
    });
  }
}

function* ddeDetail(action) {
  try {
    const result = yield filterFetchCommon(API_URL_SANDIAWEB + 'Common/getDetailOrder', {
      method: 'POST',
      headers: API_HEADERS_COMMON,
      body: JSON.stringify(action.data)
    });
    var resultParser = resJsonParser(result)
    yield put({
      type: DDE_DETAIL_SUCCESS,
      result: resultParser.QUESTION
    });
  } catch (error) {
    yield put({
      type: DDE_DETAIL_ERROR,
      error: error
    });
  }
}

function* ddeFilter(action) {
  try {
    const result = yield filterFetchCommon(API_URL_SANDIAWEB + 'Common/getListOrder', {
      method: 'POST',
      headers: API_HEADERS_COMMON,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: DDE_FILTER_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: DDE_FILTER_ERROR,
      error: error
    });
  }
}

function* ddeRevise(action) {
  try {
    const result = yield filterFetchSubmitOrder(API_URL_SANDIAWEB + 'Order/CorrectionDetail', {
      method: 'POST',
      headers: API_HEADERS_COMMON,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: DDE_REVISE_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: DDE_REVISE_ERROR,
      error: error
    });
  }
}


export function* watchDdeFilter() {
  yield takeLatest(DDE_FILTER_PROSES, ddeFilter);
}

export function* watchDdeDetail() {
  yield takeLatest(DDE_DETAIL_PROSES, ddeDetail);
}

export function* watchDdeInsert() {
  yield takeLatest(DDE_INSERT_PROSES, ddeInsert);
}

export function* watchDdeRevise() {
  yield takeLatest(DDE_REVISE_PROSES, ddeRevise);
}