import { takeLatest, put } from "redux-saga/effects";
import {
    DDE_JOBCATEGORY_PROCESS, DDE_JOBCATEGORY_SUCCESS, DDE_JOBCATEGORY_ERROR,
    DDE_JOBSTATUS_PROCESS, DDE_JOBSTATUS_SUCCESS, DDE_JOBSTATUS_ERROR,
    DDE_JOBTYPE_PROCESS, DDE_JOBTYPE_SUCCESS, DDE_JOBTYPE_ERROR,
    DDE_JOBPOSITION_PROCESS, DDE_JOBPOSITION_SUCCESS, DDE_JOBPOSITION_ERROR,
    DDE_INDUSTRYTYPE_PROCESS, DDE_INDUSTRYTYPE_SUCCESS, DDE_INDUSTRYTYPE_ERROR,
    DDE_LAMAUSAHATAHUN_PROCESS, DDE_LAMAUSAHATAHUN_SUCCESS, DDE_LAMAUSAHATAHUN_ERROR,
    DDE_LAMAUSAHABULAN_PROCESS, DDE_LAMAUSAHABULAN_SUCCESS, DDE_LAMAUSAHABULAN_ERROR,
    DDE_KONTRAKTAHUN_PROCESS, DDE_KONTRAKTAHUN_SUCCESS, DDE_KONTRAKTAHUN_ERROR,
    DDE_KONTRAKBULAN_PROCESS, DDE_KONTRAKBULAN_SUCCESS, DDE_KONTRAKBULAN_ERROR
    
} from "../../../../actions/sandiaFeature/dataLengkap/dataLengkapPekerjaan";
import { filterFetchCore } from "../../../../utils/apiFetch"
import { API_HEADERS_CORE, API_TIMEOUT, API_URL_SANDIAWEB } from "../../../../utils/constant"

function* ddeJobCategory(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_JOBCATEGORY_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_JOBCATEGORY_ERROR,
          error: error
        });
    }
}

function* ddeJobStatus(action) {
  try {
      const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data)
      });
      yield put({
        type: DDE_JOBSTATUS_SUCCESS,
        result: result
      });
  } catch (error) {
      yield put({
        type: DDE_JOBSTATUS_ERROR,
        error: error
      });
  }
}

function* ddeJobType(action) {
  try {
      const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getJobType', {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data)
      });
      yield put({
        type: DDE_JOBTYPE_SUCCESS,
        result: result
      });
  } catch (error) {
      yield put({
        type: DDE_JOBTYPE_ERROR,
        error: error
      });
  }
}

function* ddeJobPosition(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getJabatan', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_JOBPOSITION_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_JOBPOSITION_ERROR,
          error: error
        });
    }
}

function* ddeIndustyType(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getIndustryType', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_INDUSTRYTYPE_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_INDUSTRYTYPE_ERROR,
          error: error
        });
    }
}

function* ddeLamaUsahaTahun(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_LAMAUSAHATAHUN_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_LAMAUSAHATAHUN_ERROR,
          error: error
        });
    }
}

function* ddeLamaUsahaBulan(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_LAMAUSAHABULAN_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_LAMAUSAHABULAN_ERROR,
          error: error
        });
    }
}

function* ddeKontrakTahun(action) {
  try {
      const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data)
      });
      yield put({
        type: DDE_KONTRAKTAHUN_SUCCESS,
        result: result
      });
  } catch (error) {
      yield put({
        type: DDE_KONTRAKTAHUN_ERROR,
        error: error
      });
  }
}

function* ddeKontrakBulan(action) {
  try {
      const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data)
      });
      yield put({
        type: DDE_KONTRAKBULAN_SUCCESS,
        result: result
      });
  } catch (error) {
      yield put({
        type: DDE_KONTRAKBULAN_ERROR,
        error: error
      });
  }
}



export function* watchDdeJobCategory() {
  yield takeLatest(DDE_JOBCATEGORY_PROCESS, ddeJobCategory);
}

export function* watchDdeJobStatus() {
  yield takeLatest(DDE_JOBSTATUS_PROCESS, ddeJobStatus);
}

export function* watchDdeJobType() {
    yield takeLatest(DDE_JOBTYPE_PROCESS, ddeJobType);
}

export function* watchDdeJobPosition() {
    yield takeLatest(DDE_JOBPOSITION_PROCESS, ddeJobPosition);
  }

export function* watchDdeIndustyType() {
    yield takeLatest(DDE_INDUSTRYTYPE_PROCESS, ddeIndustyType);
}

export function* watchDdeLamaUsahaTahun() {
    yield takeLatest(DDE_LAMAUSAHATAHUN_PROCESS, ddeLamaUsahaTahun);
}

export function* watchDdeLamaUsahaBulan() {
    yield takeLatest(DDE_LAMAUSAHABULAN_PROCESS, ddeLamaUsahaBulan);
}

export function* watchDdeKontrakTahun() {
  yield takeLatest(DDE_KONTRAKTAHUN_PROCESS, ddeKontrakTahun);
}

export function* watchDdeKontrakBulan() {
  yield takeLatest(DDE_KONTRAKBULAN_PROCESS, ddeKontrakBulan);
}
    