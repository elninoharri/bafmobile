import { takeLatest, put } from "redux-saga/effects";
import {
    DDE_EDUCATION_PROCESS, DDE_EDUCATION_SUCCESS, DDE_EDUCATION_ERROR,
    DDE_GENDER_PROCESS, DDE_GENDER_SUCCESS, DDE_GENDER_ERROR,
    DDE_MARITALSTATUS_PROCESS, DDE_MARITALSTATUS_SUCCESS, DDE_MARITALSTATUS_ERROR,
    DDE_RELATIONSHIP_PROCESS, DDE_RELATIONSHIP_SUCCESS, DDE_RELATIONSHIP_ERROR,
    DDE_RELIGION_PROCESS, DDE_RELIGION_SUCCESS, DDE_RELIGION_ERROR,
    DDE_HOUSEOWNERSHIP_PROCESS, DDE_HOUSEOWNERSHIP_SUCCESS, DDE_HOUSEOWNERSHIP_ERROR,
    DDE_OFFICEOWNERSHIP_PROCESS, DDE_OFFICEOWNERSHIP_SUCCESS, DDE_OFFICEOWNERSHIP_ERROR
} from "../../../../actions/sandiaFeature/dataLengkap/dataLengkapPersonal";
import { filterFetchCore } from "../../../../utils/apiFetch"
import { API_HEADERS_CORE, API_TIMEOUT, API_URL_SANDIAWEB } from "../../../../utils/constant"

function* ddeEducation(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_EDUCATION_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_EDUCATION_ERROR,
          error: error
        });
    }
}

function* ddeGender(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_GENDER_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_GENDER_ERROR,
          error: error
        });
    }
}

function* ddeMaritalStatus(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_MARITALSTATUS_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_MARITALSTATUS_ERROR,
          error: error
        });
    }
}

function* ddeRelationship(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_RELATIONSHIP_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_RELATIONSHIP_ERROR,
          error: error
        });
    }
}

function* ddeReligion(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_RELIGION_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_RELIGION_ERROR,
          error: error
        });
    }
}

function* ddeHouseOwnership(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_HOUSEOWNERSHIP_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_HOUSEOWNERSHIP_ERROR,
          error: error
        });
    }
}

function* ddeOfficeOwnership(action) {
    try {
        const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
          method: 'POST',
          headers: API_HEADERS_CORE,
          timeout: API_TIMEOUT,
          body: JSON.stringify(action.data)
        });
        yield put({
          type: DDE_OFFICEOWNERSHIP_SUCCESS,
          result: result
        });
    } catch (error) {
        yield put({
          type: DDE_OFFICEOWNERSHIP_ERROR,
          error: error
        });
    }
}



export function* watchDdeEducation() {
  yield takeLatest(DDE_EDUCATION_PROCESS, ddeEducation);
}

export function* watchDdeGender() {
    yield takeLatest(DDE_GENDER_PROCESS, ddeGender);
  }

export function* watchDdeMaritalStatus() {
  yield takeLatest(DDE_MARITALSTATUS_PROCESS, ddeMaritalStatus);
}

export function* watchDdeRelationship() {
    yield takeLatest(DDE_RELATIONSHIP_PROCESS, ddeRelationship);
}

export function* watchDdeReligion() {
    yield takeLatest(DDE_RELIGION_PROCESS, ddeReligion);
}

export function* watchDdeHouseOwnership() {
    yield takeLatest(DDE_HOUSEOWNERSHIP_PROCESS, ddeHouseOwnership);
}

export function* watchDdeOfficeOwnership() {
    yield takeLatest(DDE_OFFICEOWNERSHIP_PROCESS, ddeOfficeOwnership);
}
    