import { takeLatest, put } from "redux-saga/effects";
import {
  VALIDATION_INVOICE_PROSES,
  VALIDATION_INVOICE_SUCCESS,
  VALIDATION_INVOICE_ERROR,
  TANGGAL_PO_PROSES,
  TANGGAL_PO_SUCCESS,
  TANGGAL_PO_ERROR,
  KONFIRMASI_TRANSAKSI_PROSES,
  KONFIRMASI_TRANSAKSI_SUCCESS,
  KONFIRMASI_TRANSAKSI_ERROR,
  TIPE_KONSUMEN_PROSES,
  TIPE_KONSUMEN_SUCCESS,
  TIPE_KONSUMEN_ERROR,
  DELIVERY_ADDRESS_PROSES,
  DELIVERY_ADDRESS_SUCCESS,
  DELIVERY_ADDRESS_ERROR,
  INFO_PENGIRIMAN_PROSES,
  INFO_PENGIRIMAN_SUCCESS,
  INFO_PENGIRIMAN_ERROR
} from "../../../actions/sandiaFeature/tambahKonfirmasiTransaksi";
import { filterFetchOtp, filterFetchCore, filterFetchKT } from "../../../utils/apiFetch"
import { API_HEADERS_COMMON, API_HEADERS_ORDER, API_HEADERS_CORE, API_TIMEOUT, API_URL_NONUAT, API_URL_SANDIAWEB } from "../../../utils/constant"

var API_HEADERS_COMMON_MOD = API_HEADERS_COMMON;


function* validationInvoice(action) {
  //add new authorization in API_HEADERS_COMMON 
  API_HEADERS_COMMON_MOD.Authorization = 'Bearer ' + action.token
  try {
    
    const result = yield filterFetchOtp(API_URL_SANDIAWEB + 'Common/CheckInvoice', {
      method: 'POST',
      headers: API_HEADERS_COMMON_MOD,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: VALIDATION_INVOICE_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: VALIDATION_INVOICE_ERROR,
      error: error
    });
  }
}

function* tanggalPO(action) {
  //add new authorization in API_HEADERS_COMMON 
  API_HEADERS_COMMON_MOD.Authorization = 'Bearer ' + action.token
  try {
    const result = yield filterFetchOtp(API_URL_SANDIAWEB + 'Common/getDetailPO', {
      method: 'POST',
      headers: API_HEADERS_COMMON_MOD,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: TANGGAL_PO_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: TANGGAL_PO_ERROR,
      error: error
    });
  }
}

function* deliveryAddress(action) {
  try {
    const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: DELIVERY_ADDRESS_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: DELIVERY_ADDRESS_ERROR,
      error: error
    });
  }
}

function* konfirmasiTransaksi(action) {
  try {
    const result = yield filterFetchKT(API_URL_NONUAT + 'Order/TransactionConf', {
      method: 'POST',
      headers: API_HEADERS_ORDER,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: KONFIRMASI_TRANSAKSI_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: KONFIRMASI_TRANSAKSI_ERROR,
      error: error
    });
  }
}

function* tipeKonsumen(action) {
  try {
    const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: TIPE_KONSUMEN_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: TIPE_KONSUMEN_ERROR,
      error: error
    });
  }
}

function* infoPengiriman(action) {
  try {
    const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getMaster', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: INFO_PENGIRIMAN_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: INFO_PENGIRIMAN_ERROR,
      error: error
    });
  }
}

export function* watchValidationInvoice() {
  yield takeLatest(VALIDATION_INVOICE_PROSES, validationInvoice);
}

export function* watchTanggalPO() {
  yield takeLatest(TANGGAL_PO_PROSES, tanggalPO);
}

export function* watchDeliveryAddress() {
  yield takeLatest(DELIVERY_ADDRESS_PROSES, deliveryAddress);
}

export function* watchKonfirmasiTransaksi() {
  yield takeLatest(KONFIRMASI_TRANSAKSI_PROSES, konfirmasiTransaksi);
}

export function* watchTipeKonsumen() {
  yield takeLatest(TIPE_KONSUMEN_PROSES, tipeKonsumen);
}

export function* watchInfoPengiriman() {
  yield takeLatest(INFO_PENGIRIMAN_PROSES, infoPengiriman);
}
