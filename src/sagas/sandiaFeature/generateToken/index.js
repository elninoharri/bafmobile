import { takeLatest, put } from "redux-saga/effects";
import {
TOKEN_SANDIA_ERROR,
TOKEN_SANDIA_PROCESS,
TOKEN_SANDIA_SUCCESS
} from "../../../actions/sandiaFeature/generateToken";
import { filterFetch, filterFetchToken, filterFetchCommon } from "../../../utils/apiFetch"
import { API_URL_USR_MNGMNT, API_URL_COMMON, API_KEY, API_TIMEOUT } from "../../../utils/constant"

function* generateToken(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'GenerateTokenSANDIA', {
      method: 'GET',
      headers: {
        "Authorization": `Bearer ${action.token}`,
        "Content-type": "application/json",
      },
      timeout: API_TIMEOUT
    });
    yield put({
      type: TOKEN_SANDIA_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: TOKEN_SANDIA_ERROR,
      error: error
    });
  }
}

export function* watchGenerateToken() {
  yield takeLatest(TOKEN_SANDIA_PROCESS, generateToken);
}