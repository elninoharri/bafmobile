import { takeLatest, put } from "redux-saga/effects";
import { 
    REVISI_KONFIRMASI_TRANSAKSI_PROSES,
    REVISI_KONFIRMASI_TRANSAKSI_SUCCESS,
    REVISI_KONFIRMASI_TRANSAKSI_ERROR
} from "../../../actions/sandiaFeature/revisiKonfirmasiTransaksi";
import { filterFetchRevKT } from "../../../utils/apiFetch"
import { API_HEADERS_ORDER , API_TIMEOUT, API_URL_NONUAT } from "../../../utils/constant"

function* revisiKonfirmasiTransaksi(action) {
  try {
      const result = yield filterFetchRevKT(API_URL_NONUAT + 'Order/RevTransactionConf', {
        method: 'POST',
        headers: API_HEADERS_ORDER,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data)
      });
      yield put({
        type: REVISI_KONFIRMASI_TRANSAKSI_SUCCESS,
        result: result
      });
  } catch (error) {
      yield put({
        type: REVISI_KONFIRMASI_TRANSAKSI_ERROR,
        error: error
      });
  }
}

export function* watchRevisiKonfirmasiTransaksi() {
  yield takeLatest(REVISI_KONFIRMASI_TRANSAKSI_PROSES, revisiKonfirmasiTransaksi);
}
