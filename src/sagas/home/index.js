import {takeLatest, put} from 'redux-saga/effects';
import {
  DATA_BANNER_PROCESS,
  DATA_BANNER_SUCCESS,
  DATA_BANNER_ERROR,
  GET_ACTIVE_AGREEMENT_PROCESS,
  GET_ACTIVE_AGREEMENT_SUCCESS,
  GET_ACTIVE_AGREEMENT_ERROR,
  DETAIL_BANNER_ERROR,
  DETAIL_BANNER_PROCESS,
  DETAIL_BANNER_SUCCESS,
  REFCONTENTD_ERROR,
  REFCONTENTD_PROCESS,
  REFCONTENTD_SUCCESS,
  SUBMIT_NIK_PROCESS,
  SUBMIT_NIK_SUCCESS,
  SUBMIT_NIK_ERROR,
  /*****START PAYMENT HISTORY SAGAS*****/
  GET_PAYMENT_HISTORY_PROCESS,
  GET_PAYMENT_HISTORY_SUCCESS,
  GET_PAYMENT_HISTORY_ERROR,
  GET_VOUCHER_CODE_SUCCESS,
  GET_VOUCHER_CODE_ERROR,
  GET_VOUCHER_CODE_PROCESS,
  /*****END PAYMENT HISTORY SAGAS*****/
  EMAIL_BAF_CARE_PROCESS,
  EMAIL_BAF_CARE_SUCCESS,
  EMAIL_BAF_CARE_ERROR,
  GET_LIST_NOTIF_PROCESS,
  GET_LIST_NOTIF_SUCCESS,
  GET_LIST_NOTIF_ERROR,
  DELETE_NOTIF_PROCESS,
  DELETE_NOTIF_SUCCESS,
  DELETE_NOTIF_ERROR,
  READ_NOTIF_PROCESS,
  READ_NOTIF_SUCCESS,
  READ_NOTIF_ERROR,
  GET_LIST_DEALER_SUCCESS,
  GET_LIST_DEALER_ERROR,
  GET_LIST_DEALER_PROCESS,
  GET_LIST_MITRA_PROCESS,
} from './../../actions/home';
import {filterFetch} from '../../utils/apiFetch';
import {
  API_URL_USR_MNGMNT,
  API_URL_COMMON,
  API_KEY,
  API_TIMEOUT,
} from '../../utils/constant';

function* dataBanner(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getRefBanner', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: DATA_BANNER_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: DATA_BANNER_ERROR,
      error: error,
    });
  }
}

function* detailBanner(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getDetailBanner', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: DETAIL_BANNER_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: DETAIL_BANNER_ERROR,
      error: error,
    });
  }
}

function* getListDealer(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getListDealer', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_LIST_DEALER_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_LIST_DEALER_ERROR,
      error: error,
    });
  }
}

function* getVoucherCode(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getVoucherCode', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${action.token}`,
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_VOUCHER_CODE_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_VOUCHER_CODE_ERROR,
      error: error,
    });
  }
}

function* refContentD(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getRefContentD', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: REFCONTENTD_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: REFCONTENTD_ERROR,
      error: error,
    });
  }
}

function* getActiveAgreement(action) {
  try {
    const result = yield filterFetch(
      API_URL_USR_MNGMNT + 'getActiveAgreement',
      {
        method: 'GET',
        headers: {
          'Content-type': 'application/json',
          Authorization: 'Bearer ' + action.token,
        },
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data),
      },
    );
    yield put({
      type: GET_ACTIVE_AGREEMENT_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_ACTIVE_AGREEMENT_ERROR,
      error: error,
    });
  }
}

function* submitNik(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'submitNIK', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: SUBMIT_NIK_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: SUBMIT_NIK_ERROR,
      error: error,
    });
  }
}

/****START PAYMENT HISTORY SAGAS***/
function* getPaymentHistory(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'getPaymentHistory', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_PAYMENT_HISTORY_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_PAYMENT_HISTORY_ERROR,
      error: error,
    });
  }
}
/****END PAYMENT HISTORY SAGAS***/

function* emailBafCare(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'ProcEmailBafCare', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: EMAIL_BAF_CARE_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: EMAIL_BAF_CARE_ERROR,
      error: error,
    });
  }
}

function* getListNotif(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getListNotif', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_LIST_NOTIF_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_LIST_NOTIF_ERROR,
      error: error,
    });
  }
}

function* deleteNotif(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'UpdateNotif', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: DELETE_NOTIF_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: DELETE_NOTIF_ERROR,
      error: error,
    });
  }
}

function* readNotif(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'ReadNotif', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: READ_NOTIF_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: READ_NOTIF_ERROR,
      error: error,
    });
  }
}

function* getListMitra(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getListMitra', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_LIST_DEALER_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_LIST_DEALER_ERROR,
      error: error,
    });
  }
}

export function* watchGetVoucherCode() {
  yield takeLatest(GET_VOUCHER_CODE_PROCESS, getVoucherCode);
}

export function* watchGetListDealer() {
  yield takeLatest(GET_LIST_DEALER_PROCESS, getListDealer);
}

export function* watchDataBanner() {
  yield takeLatest(DATA_BANNER_PROCESS, dataBanner);
}

export function* watchGetActiveAgreement() {
  yield takeLatest(GET_ACTIVE_AGREEMENT_PROCESS, getActiveAgreement);
}

export function* watchSubmitNik() {
  yield takeLatest(SUBMIT_NIK_PROCESS, submitNik);
}

export function* watchDetailBanner() {
  yield takeLatest(DETAIL_BANNER_PROCESS, detailBanner);
}

export function* watchRefContentD() {
  yield takeLatest(REFCONTENTD_PROCESS, refContentD);
}

/****START PAYMENT HISTORY SAGAS***/
export function* watchGetPaymentHistory() {
  yield takeLatest(GET_PAYMENT_HISTORY_PROCESS, getPaymentHistory);
}
/****END PAYMENT HISTORY SAGAS***/

export function* watchEmailBafCare() {
  yield takeLatest(EMAIL_BAF_CARE_PROCESS, emailBafCare);
}

export function* watchGetListNotif() {
  yield takeLatest(GET_LIST_NOTIF_PROCESS, getListNotif);
}

export function* watchDeleteNotif() {
  yield takeLatest(DELETE_NOTIF_PROCESS, deleteNotif);
}

export function* watchReadNotif() {
  yield takeLatest(READ_NOTIF_PROCESS, readNotif);
}

export function* watchGetListMitra() {
  yield takeLatest(GET_LIST_MITRA_PROCESS, getListMitra);
}
