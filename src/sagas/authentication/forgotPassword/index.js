import { takeLatest, put } from "redux-saga/effects";
import {
  FORGOT_PASSWORD_PROCESS,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_ERROR,
} from "../../../actions";
import { filterFetch } from "../../../utils/apiFetch"
import { API_URL_USR_MNGMNT, API_URL_COMMON, API_KEY, API_TIMEOUT } from "../../../utils/constant"

function* forgotPassword(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'ForgotPassword', {
      method: 'POST',
      headers: {
        "Content-type" : "application/json",
        "x-api-key": API_KEY
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: FORGOT_PASSWORD_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: FORGOT_PASSWORD_ERROR,
      error: error
    });
  }
}

export function* watchForgotPassword() {
  yield takeLatest(FORGOT_PASSWORD_PROCESS, forgotPassword);
}
