import { takeLatest, put } from "redux-saga/effects";
import {
  CHECK_VERSION_PROCESS,
  CHECK_VERSION_SUCCESS,
  CHECK_VERSION_ERROR,
  CHECK_TOKEN_PROCESS,
  CHECK_TOKEN_SUCCESS,
  CHECK_TOKEN_ERROR,
  REFRESH_TOKEN_PROCESS,
  REFRESH_TOKEN_SUCCESS,
  REFRESH_TOKEN_ERROR
} from "../../../actions";
import { filterFetch, filterFetchToken } from "../../../utils/apiFetch"
import { API_URL_USR_MNGMNT, API_URL_COMMON, API_KEY, API_TIMEOUT } from "../../../utils/constant"

function* checkVersion(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'CheckVersion', {
      method: 'GET',
      headers: {
        "Authorization": `Bearer ${action.token}`,
        "Content-type": "application/json",
        "x-api-key": API_KEY
      },
      timeout: API_TIMEOUT
    });
    yield put({
      type: CHECK_VERSION_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: CHECK_VERSION_ERROR,
      error: error
    });
  }
}

function* checkToken(action) {
  try {
    const result = yield filterFetchToken(API_URL_USR_MNGMNT + 'CheckToken', {
      method: 'GET',
      headers: {
        "Authorization": `Bearer ${action.token}`,
        "Content-type": "application/json",
        "x-api-key": API_KEY
      },
      timeout: API_TIMEOUT
    });
    yield put({
      type: CHECK_TOKEN_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: CHECK_TOKEN_ERROR,
      error: error
    });
  }
}

function* refreshToken(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'GenerateRefreshToken', {
      method: 'GET',
      headers: {
        "Authorization": `Bearer ${action.token}`,
        "Content-type": "application/json",
      },
      timeout: API_TIMEOUT
    });
    yield put({
      type: REFRESH_TOKEN_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: REFRESH_TOKEN_ERROR,
      error: error
    });
  }
}

export function* watchCheckVersion() {
  yield takeLatest(CHECK_VERSION_PROCESS, checkVersion);
}

export function* watchCheckToken() {
  yield takeLatest(CHECK_TOKEN_PROCESS, checkToken);
}

export function* watchRefreshToken() {
  yield takeLatest(REFRESH_TOKEN_PROCESS, refreshToken);
}

