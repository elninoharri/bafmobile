import {watchCheckVersion, watchCheckToken,watchRefreshToken} from './authLoading';
import {watchForgotPassword} from './forgotPassword';
import {watchLogin,watchLoginOpenid,watchSaveToken} from './login';
import {watchLogout} from './logout';
import {
  watchCheckPhoneNumber,
  watchOtpCall,
  watchOtpSms,
  watchOtpValidate,
  watchRegister
} from './register';

export {
  watchCheckVersion,
  watchCheckToken,
  watchForgotPassword,
  watchLogin,
  watchLogout,
  watchLoginOpenid,
  watchCheckPhoneNumber,
  watchOtpCall,
  watchOtpSms,
  watchOtpValidate,
  watchRegister,
  watchSaveToken,
  watchRefreshToken
};
