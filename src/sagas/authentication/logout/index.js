import {takeLatest, put} from 'redux-saga/effects';
import {
 LOGOUT_ERROR,
 LOGOUT_PROCESS,
 LOGOUT_SUCCESS
} from '../../../actions/authentication/logout';
import {
  filterFetchLogout
} from '../../../utils/apiFetch';
import {
  API_URL_USR_MNGMNT,
  API_URL_COMMON,
  API_KEY,
  API_TIMEOUT,
} from '../../../utils/constant';
import AsyncStorage from '@react-native-community/async-storage';

function* logout(action) {
  try {
    const result = yield filterFetchLogout(API_URL_USR_MNGMNT + 'FuncLogout', {
      method: 'GET',
      headers: {
        "Authorization": `Bearer ${action.token}`,
        "Content-type": "application/json",
        "x-api-key": API_KEY
      },
      timeout: API_TIMEOUT
    });
    yield put({
      type: LOGOUT_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: LOGOUT_ERROR,
      error: error,
    });
  }
}

export function* watchLogout() {
  yield takeLatest(LOGOUT_PROCESS, logout);
}