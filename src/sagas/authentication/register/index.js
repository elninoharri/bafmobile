import { takeLatest, put } from 'redux-saga/effects';
import {
  CHECK_PHONE_NUMBER_PROCESS,
  CHECK_PHONE_NUMBER_SUCCESS,
  CHECK_PHONE_NUMBER_ERROR,
  OTP_CALL_PROCESS,
  OTP_CALL_SUCCESS,
  OTP_CALL_ERROR,
  OTP_SMS_PROCESS,
  OTP_SMS_SUCCESS,
  OTP_SMS_ERROR,
  OTP_VALIDATE_PROCESS,
  OTP_VALIDATE_SUCCESS,
  OTP_VALIDATE_ERROR,
  REGISTER_PROCESS,
  REGISTER_ERROR,
  REGISTER_SUCCESS,
} from '../../../actions/authentication/';
import { filterFetch } from '../../../utils/apiFetch';
import {
  API_URL_USR_MNGMNT,
  API_URL_COMMON,
  API_KEY,
  API_TIMEOUT,
} from '../../../utils/constant';

function* checkPhoneNumber(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'CheckPhoneNumber', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: CHECK_PHONE_NUMBER_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: CHECK_PHONE_NUMBER_ERROR,
      error: error,
    });
  }
}

function* otpCall(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'ChitcallOTP', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: OTP_CALL_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: OTP_CALL_ERROR,
      error: error,
    });
  }
}

function* otpSms(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'SendOTPSms', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: OTP_SMS_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: OTP_SMS_ERROR,
      error: error,
    });
  }
}

function* otpValidate(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'ValidateOTP', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: OTP_VALIDATE_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: OTP_VALIDATE_ERROR,
      error: error,
    });
  }
}

function* register(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'RegisterUser', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: REGISTER_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: REGISTER_ERROR,
      error: error,
    });
  }
}

export function* watchCheckPhoneNumber() {
  yield takeLatest(CHECK_PHONE_NUMBER_PROCESS, checkPhoneNumber);
}

export function* watchOtpCall() {
  yield takeLatest(OTP_CALL_PROCESS, otpCall);
}

export function* watchOtpSms() {
  yield takeLatest(OTP_SMS_PROCESS, otpSms);
}

export function* watchOtpValidate() {
  yield takeLatest(OTP_VALIDATE_PROCESS, otpValidate);
}

export function* watchRegister() {
  yield takeLatest(REGISTER_PROCESS, register);
}
