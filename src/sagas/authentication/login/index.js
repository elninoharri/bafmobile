import {takeLatest, put} from 'redux-saga/effects';
import {
  LOGIN_PROCESS,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGIN_OPENID_PROCESS,
  LOGIN_OPENID_SUCCESS,
  LOGIN_OPENID_ERROR,
  SAVE_TOKEN_PROCESS,
  SAVE_TOKEN_SUCCESS,
  SAVE_TOKEN_ERROR,
} from '../../../actions/authentication/';
import {
  filterFetch,
  filterFetchCommon,
  filterFetchCore,
  filterFetchFinancialData,
  filterFetchOrder,
} from '../../../utils/apiFetch';
import {
  API_URL_USR_MNGMNT,
  API_URL_COMMON,
  API_KEY,
  API_TIMEOUT,
} from '../../../utils/constant';
import AsyncStorage from '@react-native-community/async-storage';

function* login(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'CheckLogin', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: LOGIN_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: LOGIN_ERROR,
      error: error,
    });
  }
}

function* loginOpenid(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'CheckLoginAuth', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: LOGIN_OPENID_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: LOGIN_OPENID_ERROR,
      error: error,
    });
  }
}

function* saveToken(action) {
  try {
    const result = yield filterFetch(
      API_URL_USR_MNGMNT + 'RegisterTokenLogin',
      {
        method: 'POST',
        headers: {
          Authorization: 'Bearer ' + action.token,
          'Content-Type': 'application/json',
        },
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data),
      },
    );
    yield put({
      type: SAVE_TOKEN_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: SAVE_TOKEN_ERROR,
      error: error,
    });
  }
}

export function* watchLogin() {
  yield takeLatest(LOGIN_PROCESS, login);
}

export function* watchLoginOpenid() {
  yield takeLatest(LOGIN_OPENID_PROCESS, loginOpenid);
}

export function* watchSaveToken() {
  yield takeLatest(SAVE_TOKEN_PROCESS, saveToken);
}
