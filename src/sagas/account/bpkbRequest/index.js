import { takeLatest, put } from 'redux-saga/effects';
import {
  GET_BPKB_PROCESS,
  GET_BPKB_SUCCESS,
  GET_BPKB_ERROR,
  SUBMIT_BPKB_PROCESS,
  SUBMIT_BPKB_SUCCESS,
  SUBMIT_BPKB_ERROR,
  TIME_SLOT_BPKB_PROCESS,
  TIME_SLOT_BPKB_SUCCESS,
  TIME_SLOT_BPKB_ERROR,
  CHECK_AVAIL_BOOK_PROCESS,
  CHECK_AVAIL_BOOK_SUCCESS,
  CHECK_AVAIL_BOOK_ERROR,
  CHECK_HOLIDAY_ERROR,
  CHECK_HOLIDAY_SUCCESS,
  CHECK_HOLIDAY_PROCESS,
  GET_LIST_BPKB_INPROGRESS_ERROR,
  GET_LIST_BPKB_INPROGRESS_PROCESS,
  GET_LIST_BPKB_INPROGRESS_SUCCESS,
  GET_LIST_BPKB_DONE_ERROR,
  GET_LIST_BPKB_DONE_PROCESS,
  GET_LIST_BPKB_DONE_SUCCESS,
  CANCEL_BPKB_ERROR,
  CANCEL_BPKB_PROCESS,
  CANCEL_BPKB_SUCCESS,
  RESCHEDULE_BPKB_ERROR,
  RESCHEDULE_BPKB_PROCESS,
  RESCHEDULE_BPKB_SUCCESS,
  GET_DETAIL_BPKB_PROCESS,
  GET_DETAIL_BPKB_ERROR,
  GET_DETAIL_BPKB_SUCCESS
} from '../../../actions';

import { filterFetch } from '../../../utils/apiFetch';
import {
  API_URL_USR_MNGMNT,
  API_URL_COMMON,
  API_KEY,
  API_TIMEOUT,
} from '../../../utils/constant';

function* getBPKB(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'getListBPKB', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_BPKB_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_BPKB_ERROR,
      error: error,
    });
  }
}

function* submitBPKB(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'bpkb/request', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: SUBMIT_BPKB_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: SUBMIT_BPKB_ERROR,
      error: error,
    });
  }
}

function* timeSlotBPKB(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'getTimeSlot', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: TIME_SLOT_BPKB_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: TIME_SLOT_BPKB_ERROR,
      error: error,
    });
  }
}

function* checkAvailBook(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'CheckAvailBook', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: CHECK_AVAIL_BOOK_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: CHECK_AVAIL_BOOK_ERROR,
      error: error,
    });
  }
}

function* checkHoliday(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'CheckHoliday', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: CHECK_HOLIDAY_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: CHECK_HOLIDAY_ERROR,
      error: error,
    });
  }
}

function* getListBPKBInProgress(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'bpkb/inprogress', {
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
        Authorization: 'Bearer ' + action.token,
      }
    });
    yield put({
      type: GET_LIST_BPKB_INPROGRESS_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_LIST_BPKB_INPROGRESS_ERROR,
      error: error,
    });
  }
}

function* getListBPKBDone(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'bpkb/complete', {
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
        Authorization: 'Bearer ' + action.token,
      }
    });
    yield put({
      type: GET_LIST_BPKB_DONE_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_LIST_BPKB_DONE_ERROR,
      error: error,
    });
  }
}

function* cancelBpkb(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'bpkb/cancel', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: CANCEL_BPKB_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: CANCEL_BPKB_ERROR,
      error: error,
    });
  }
}


function* rescheduleBpkb(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'bpkb/reschedule', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: RESCHEDULE_BPKB_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: RESCHEDULE_BPKB_ERROR,
      error: error,
    });
  }
}


function* getdetailBpkb(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'bpkb/getdetail', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_DETAIL_BPKB_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_DETAIL_BPKB_ERROR,
      error: error,
    });
  }
}

export function* watchGetListBPKB() {
  yield takeLatest(GET_BPKB_PROCESS, getBPKB);
}

export function* watchSubmitBPKB() {
  yield takeLatest(SUBMIT_BPKB_PROCESS, submitBPKB);
}

export function* watchTimeSlotBPKB() {
  yield takeLatest(TIME_SLOT_BPKB_PROCESS, timeSlotBPKB);
}

export function* watchCheckAvailBook() {
  yield takeLatest(CHECK_AVAIL_BOOK_PROCESS, checkAvailBook);
}

export function* watchCheckHoliday() {
  yield takeLatest(CHECK_HOLIDAY_PROCESS, checkHoliday);
}

export function* watchGetListBPKBInProgress() {
  yield takeLatest(GET_LIST_BPKB_INPROGRESS_PROCESS, getListBPKBInProgress);
}

export function* watchGetListBPKBDone() {
  yield takeLatest(GET_LIST_BPKB_DONE_PROCESS, getListBPKBDone);
}

export function* watchCancelBPKB() {
  yield takeLatest(CANCEL_BPKB_PROCESS, cancelBpkb);
}

export function* watchRescheduleBpkb() {
  yield takeLatest(RESCHEDULE_BPKB_PROCESS, rescheduleBpkb);
}

export function* watchGetdetailBpkb() {
  yield takeLatest(GET_DETAIL_BPKB_PROCESS, getdetailBpkb);
}

