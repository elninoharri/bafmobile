import {takeLatest, put} from 'redux-saga/effects';
import {
  CHANGE_PASSWORD_PROCESS,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_ERROR,
} from '../../../actions';

import {filterFetch} from '../../../utils/apiFetch';
import {
  API_URL_USR_MNGMNT,
  API_URL_COMMON,
  API_KEY,
  API_TIMEOUT,
} from '../../../utils/constant';

function* changePassword(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'ChangePassword', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: CHANGE_PASSWORD_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: CHANGE_PASSWORD_ERROR,
      error: error,
    });
  }
}

export function* watchChangePassword() {
  yield takeLatest(CHANGE_PASSWORD_PROCESS, changePassword);
}
