import { takeLatest, put } from 'redux-saga/effects';
import {
  DETAIL_USER_PROCESS,
  DETAIL_USER_SUCCESS,
  DETAIL_USER_ERROR,
  EDIT_USER_PROCESS,
  EDIT_USER_SUCCESS,
  EDIT_USER_ERROR,
  GET_CITY_ERROR,
  GET_CITY_PROCESS,
  GET_CITY_SUCCESS,
} from '../../../actions';

import { filterFetch } from '../../../utils/apiFetch';
import {
  API_URL_USR_MNGMNT,
  API_URL_COMMON,
  API_KEY,
  API_TIMEOUT,
} from '../../../utils/constant';

function* detailUser(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'getDetailUser', {
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
        Authorization: 'Bearer ' + action.token,
      },
      timeout: API_TIMEOUT,
    });
    yield put({
      type: DETAIL_USER_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: DETAIL_USER_ERROR,
      error: error,
    });
  }
}

function* getCity() {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'getRegisteredCity', {
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
    });
    yield put({
      type: GET_CITY_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_CITY_ERROR,
      error: error,
    });
  }
}

function* editUser(action) {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'editUser', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
        Authorization: 'Bearer ' + action.token,
      },
      body: JSON.stringify(action.data),
      timeout: API_TIMEOUT,
    });
    yield put({
      type: EDIT_USER_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: EDIT_USER_ERROR,
      error: error,
    });
  }
}

export function* watchDetailUser() {
  yield takeLatest(DETAIL_USER_PROCESS, detailUser);
}

export function* watchGetCity() {
  yield takeLatest(GET_CITY_PROCESS, getCity);
}

export function* watchEditUser() {
  yield takeLatest(EDIT_USER_PROCESS, editUser);
}
