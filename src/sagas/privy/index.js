import {takeLatest, put} from 'redux-saga/effects';
import {
  OTP_REQUEST_PROCESS,
  OTP_REQUEST_SUCCESS,
  OTP_REQUEST_ERROR,
  OTP_REGENERATE_PROCESS,
  OTP_REGENERATE_SUCCESS,
  OTP_REGENERATE_ERROR,
  OTP_PRIVY_VALIDATE_PROCESS,
  OTP_PRIVY_VALIDATE_SUCCESS,
  OTP_PRIVY_VALIDATE_ERROR,
  SUBMIT_PRIVY_PROCESS,
  SUBMIT_PRIVY_ERROR,
  SUBMIT_PRIVY_SUCCESS,
  CHECK_STATUS_REGISTER_PROCESS,
  CHECK_STATUS_REGISTER_ERROR,
  CHECK_STATUS_REGISTER_SUCCESS,
  UPLOAD_DOC_PROCESS,
  UPLOAD_DOC_ERROR,
  UPLOAD_DOC_SUCCESS,
  CHECK_STATUS_UPLOAD_PROCESS,
  CHECK_STATUS_UPLOAD_ERROR,
  CHECK_STATUS_UPLOAD_SUCCESS,
  CALLBACK_DOC_SIGN_PROCESS,
  CALLBACK_DOC_SIGN_ERROR,
  CALLBACK_DOC_SIGN_SUCCESS,
  GET_CABANG_STATUS_ERROR,
  GET_CABANG_STATUS_PROCESS,
  GET_CABANG_STATUS_SUCCESS,
  UPDATE_STATUS_ORDER_PRIVY_PROCESS,
  UPDATE_STATUS_ORDER_PRIVY_SUCCESS,
  UPDATE_STATUS_ORDER_PRIVY_ERROR,
} from './../../actions/privy';
import {
  filterFetch,
  filterFetchCommon,
  filterFetchCore,
} from '../../utils/apiFetch';
import {
  API_URL_SANDIAWEB,
  API_TIMEOUT,
  API_URL_COMMON,
  API_HEADERS_CORE,
  API_KEY,
} from '../../utils/constant';

function* getCabangStatus(action) {
  try {
    const result = yield filterFetchCore(
      API_URL_SANDIAWEB + 'core/getCabangStatus',
      {
        method: 'POST',
        headers: API_HEADERS_CORE,
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data),
      },
    );
    yield put({
      type: GET_CABANG_STATUS_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_CABANG_STATUS_ERROR,
      error: error,
    });
  }
}

function* submitPrivy(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'SubmitPrivy', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${action.token}`,
        'Content-type': 'application/json',
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data).replace(/\\n/g, ''),
    });
    yield put({
      type: SUBMIT_PRIVY_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: SUBMIT_PRIVY_ERROR,
      error: error,
    });
  }
}

function* otpRequestPrivy(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'ReqOTPPrivy', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${action.token}`,
        'Content-type': 'application/json',
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: OTP_REQUEST_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: OTP_REQUEST_ERROR,
      error: error,
    });
  }
}

function* otpRegeneratePrivy(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'RegenerateOTPPrivy', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${action.token}`,
        'Content-type': 'application/json',
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: OTP_REGENERATE_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: OTP_REGENERATE_ERROR,
      error: error,
    });
  }
}

function* otpValidatePrivy(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'OTPPrivyValidation', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${action.token}`,
        'Content-type': 'application/json',
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: OTP_PRIVY_VALIDATE_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: OTP_PRIVY_VALIDATE_ERROR,
      error: error,
    });
  }
}

function* checkStatusRegister(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'checkStatusPrivy', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: CHECK_STATUS_REGISTER_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: CHECK_STATUS_REGISTER_ERROR,
      error: error,
    });
  }
}

function* uploadDoc(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'UploadDocPrivy', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${action.token}`,
        'Content-type': 'application/json',
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data).replace(/\\n/g, ''),
    });
    yield put({
      type: UPLOAD_DOC_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: UPLOAD_DOC_ERROR,
      error: error,
    });
  }
}

function* updateStatusOrderPrivy(action) {
  try {
    const result = yield filterFetch(
      API_URL_COMMON + 'UpdateStatusOrderPrivy',
      {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
        },
        timeout: API_TIMEOUT,
        body: JSON.stringify(action.data),
      },
    );
    yield put({
      type: UPDATE_STATUS_ORDER_PRIVY_SUCCESS,
      result: result,
      target: action.target,
    });
  } catch (error) {
    yield put({
      type: UPDATE_STATUS_ORDER_PRIVY_ERROR,
      error: error,
      target: action.target,
    });
  }
}

function* checkStatusUpload(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'UploadDocPrivy', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: CHECK_STATUS_UPLOAD_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: CHECK_STATUS_UPLOAD_ERROR,
      error: error,
    });
  }
}

export function* watchgetCabangStatus() {
  yield takeLatest(GET_CABANG_STATUS_PROCESS, getCabangStatus);
}

export function* watchSubmitPrivy() {
  yield takeLatest(SUBMIT_PRIVY_PROCESS, submitPrivy);
}

export function* watchRequestOtpPrivy() {
  yield takeLatest(OTP_REQUEST_PROCESS, otpRequestPrivy);
}

export function* watchRegenerateOtpPrivy() {
  yield takeLatest(OTP_REGENERATE_PROCESS, otpRegeneratePrivy);
}

export function* watchOtpValidatePrivy() {
  yield takeLatest(OTP_PRIVY_VALIDATE_PROCESS, otpValidatePrivy);
}

export function* watchCheckStatusRegister() {
  yield takeLatest(CHECK_STATUS_REGISTER_PROCESS, checkStatusRegister);
}

export function* watchUploadDoc() {
  yield takeLatest(UPLOAD_DOC_PROCESS, uploadDoc);
}

export function* watchCheckStatusUpload() {
  yield takeLatest(CHECK_STATUS_UPLOAD_PROCESS, checkStatusUpload);
}

export function* watchUpdateStatusOrderPrivy() {
  yield takeLatest(UPDATE_STATUS_ORDER_PRIVY_PROCESS, updateStatusOrderPrivy);
}
