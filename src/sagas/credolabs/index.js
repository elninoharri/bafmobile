import {takeLatest, put} from 'redux-saga/effects';
import {
  GET_AUTHKEY_ERROR,
  GET_AUTHKEY_PROCESS,
  GET_AUTHKEY_SUCCESS,
} from './../../actions/credoLabs';
import {filterFetch, filterFetchCore} from '../../utils/apiFetch';
import {API_KEY, API_TIMEOUT, API_URL_CREDOLABS} from '../../utils/constant';

function* getAuthkey(action) {
  try {
    const result = yield filterFetchCore(API_URL_CREDOLABS + 'AuthKey', {
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
    });
    yield put({
      type: GET_AUTHKEY_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_AUTHKEY_ERROR,
      error: error,
    });
  }
}

export function* watchGetAuthKey() {
  yield takeLatest(GET_AUTHKEY_PROCESS, getAuthkey);
}
