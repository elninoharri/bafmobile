import {takeLatest, put} from 'redux-saga/effects';
import {
  GET_PENGAJUAN_DATA_PROCESS,
  GET_PENGAJUAN_DATA_SUCCESS,
  GET_PENGAJUAN_DATA_ERROR,

  GET_PENGAJUAN_MP_PROCESS,
  GET_PENGAJUAN_MP_SUCCESS,
  GET_PENGAJUAN_MP_ERROR,

  GET_PENGAJUAN_NMC_PROCESS,
  GET_PENGAJUAN_NMC_SUCCESS,
  GET_PENGAJUAN_NMC_ERROR,

  GET_PENGAJUAN_SYANA_PROCESS,
  GET_PENGAJUAN_SYANA_ERROR,
  GET_PENGAJUAN_SYANA_SUCCESS,

  GET_PENGAJUAN_CAR_PROCESS,
  GET_PENGAJUAN_CAR_ERROR,
  GET_PENGAJUAN_CAR_SUCCESS,

  GET_OTR_PROCESS,
  GET_OTR_SUCCESS,
  GET_OTR_ERROR,
} from '../../actions/lob/index';
import {filterFetch} from '../../utils/apiFetch';
import {API_URL_COMMON, API_KEY, API_TIMEOUT} from '../../utils/constant';

function* getPengajuanData(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getPengajuanData', {
      method: 'POST',
      headers: {
        "Authorization": `Bearer ${action.token}`,
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_PENGAJUAN_DATA_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_PENGAJUAN_DATA_ERROR,
      error: error,
    });
  }
}

function* getPengajuanSyana(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getPengajuanData', {
      method: 'POST',
      headers: {
        "Authorization": `Bearer ${action.token}`,
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_PENGAJUAN_SYANA_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_PENGAJUAN_SYANA_ERROR,
      error: error,
    });
  }
}

function* getPengajuanNMC(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getPengajuanData', {
      method: 'POST',
      headers: {
        "Authorization": `Bearer ${action.token}`,
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_PENGAJUAN_NMC_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_PENGAJUAN_NMC_ERROR,
      error: error,
    });
  }
}

function* getPengajuanMP(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getPengajuanData', {
      method: 'POST',
      headers: {
        "Authorization": `Bearer ${action.token}`,
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_PENGAJUAN_MP_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_PENGAJUAN_MP_ERROR,
      error: error,
    });
  }
}

function* getPengajuanCar(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getPengajuanData', {
      method: 'POST',
      headers: {
        "Authorization": `Bearer ${action.token}`,
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_PENGAJUAN_CAR_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_PENGAJUAN_CAR_ERROR,
      error: error,
    });
  }
}


function* getOTR(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getOTRPrice', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_OTR_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_OTR_ERROR,
      error: error,
    });
  }
}

export function* watchGetPengajuanData() {
  yield takeLatest(GET_PENGAJUAN_DATA_PROCESS, getPengajuanData);
}

export function* watchGetPengajuanSyana() {
  yield takeLatest(GET_PENGAJUAN_SYANA_PROCESS, getPengajuanSyana);
}

export function* watchGetPengajuanNMC() {
  yield takeLatest(GET_PENGAJUAN_NMC_PROCESS, getPengajuanNMC);
}

export function* watchGetPengajuanMP() {
  yield takeLatest(GET_PENGAJUAN_MP_PROCESS, getPengajuanMP);
}

export function* watchGetPengajuanCar() {
  yield takeLatest(GET_PENGAJUAN_CAR_PROCESS, getPengajuanCar);
}

export function* watchGetOTR() {
  yield takeLatest(GET_OTR_PROCESS, getOTR);
}
