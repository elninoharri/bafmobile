import {all, fork} from 'redux-saga/effects';
import {
  watchCheckVersion,
  watchCheckToken,
  watchForgotPassword,
  watchLogin,
  watchLoginOpenid,
  watchLogout,
  watchCheckPhoneNumber,
  watchOtpCall,
  watchOtpSms,
  watchOtpValidate,
  watchRegister,
  watchSaveToken,
  watchRefreshToken,
} from './authentication';
import {
  watchChangePassword,
  watchDetailUser,
  watchGetCity,
  watchEditUser,
  watchGetListBPKB,
  watchSubmitBPKB,
  watchTimeSlotBPKB,
  watchCheckAvailBook,
  watchCheckHoliday,
  watchGetListBPKBInProgress,
  watchGetListBPKBDone,
  watchCancelBPKB,
  watchRescheduleBpkb,
  watchGetdetailBpkb,
} from './account';

import {
  watchDataBanner,
  watchGetActiveAgreement,
  watchSubmitNik,
  watchDetailBanner,
  watchRefContentD,
  watchGetVoucherCode,
  /*****START PAYMENT HISTORY SAGAS*****/
  watchGetPaymentHistory,
  watchGetListDealer,
  /*****END PAYMENT HISTORY SAGAS*****/
  watchEmailBafCare,
  watchGetListNotif,
  watchDeleteNotif,
  watchReadNotif,
  watchGetListMitra,
} from './home';

import {
  watchgetCabangStatus,
  watchSubmitPrivy,
  watchOtpValidatePrivy,
  watchRegenerateOtpPrivy,
  watchRequestOtpPrivy,
  watchCheckStatusRegister,
  watchUploadDoc,
  watchCheckStatusUpload,
  watchUpdateStatusOrderPrivy,
} from './privy';

import {
  watchOrderCount,
  watchOrderFilter,
  watchQdeDetailData,
  watchQdePersonalDetailData,
  watchMpMappingRisk,
  watchMpInstCalculation,
  watchCancelPO,
  watchConfirmPO,
} from './sandiaFeature/dataAwal';
import {
  watchDdeInsert,
  watchDdeDetail,
  watchDdeFilter,
  watchDdeRevise,
} from './sandiaFeature/dataLengkap';
import {
  watchDdeEducation,
  watchDdeGender,
  watchDdeMaritalStatus,
  watchDdeRelationship,
  watchDdeReligion,
  watchDdeHouseOwnership,
  watchDdeOfficeOwnership,
} from './sandiaFeature/dataLengkap/dataLengkapPersonal';
import {
  watchDdeLegalProvince,
  watchDdeLegalCity,
  watchDdeLegalKecamatan,
  watchDdeLegalKelurahan,
  watchDdeResidenceProvince,
  watchDdeResidenceCity,
  watchDdeResidenceKecamatan,
  watchDdeResidenceKelurahan,
  watchDdeJobProvince,
  watchDdeJobCity,
  watchDdeJobKecamatan,
  watchDdeJobKelurahan,
} from './sandiaFeature/dataLengkap/dataLengkapWilayah';
import {
  watchDdeJobCategory,
  watchDdeJobPosition,
  watchDdeJobStatus,
  watchDdeJobType,
  watchDdeIndustyType,
  watchDdeLamaUsahaTahun,
  watchDdeLamaUsahaBulan,
  watchDdeKontrakTahun,
  watchDdeKontrakBulan,
} from './sandiaFeature/dataLengkap/dataLengkapPekerjaan';
import {
  watchDdeJaminanJenis,
  watchDdeJenis,
  watchDdeMerk,
  watchDdeSchemeId,
  watchDdeFinancialData,
  watchDdeFinancingPurpose,
  watchDdeSourceFunds,
} from './sandiaFeature/dataLengkap/dataLengkapTransaksi';

import {watchRevisiKonfirmasiTransaksi} from './sandiaFeature/revisiKonfirmasiTransaksi';
import {
  watchValidationInvoice,
  watchKonfirmasiTransaksi,
  watchTipeKonsumen,
  watchDeliveryAddress,
  watchInfoPengiriman,
  watchTanggalPO,
} from './sandiaFeature/tambahkonfirmasiTransaksi';

import {watchGenerateToken} from './sandiaFeature/generateToken';

import {watchGetAuthKey} from './credolabs';

import {watchGetOTR, watchGetPengajuanData, watchGetPengajuanSyana , watchGetPengajuanNMC,  watchGetPengajuanMP,watchGetPengajuanCar} from './lob';

import { watchGetBafPointValueSagas } from "./bafPoint";

export default function* sagas() {
  yield all([
    fork(watchCheckVersion),
    fork(watchCheckToken),
    fork(watchChangePassword),
    fork(watchDetailUser),
    fork(watchGetCity),
    fork(watchLogin),
    fork(watchLoginOpenid),
    fork(watchLogout),
    fork(watchEditUser),
    fork(watchForgotPassword),
    fork(watchSaveToken),
    fork(watchRefreshToken),
    fork(watchGetVoucherCode),

    fork(watchUpdateStatusOrderPrivy),

    fork(watchCheckPhoneNumber),
    fork(watchOtpCall),
    fork(watchOtpSms),
    fork(watchOtpValidate),
    fork(watchRegister),

    fork(watchGetListDealer),
    fork(watchGetListMitra),

    fork(watchDataBanner),
    fork(watchGetActiveAgreement),
    fork(watchSubmitNik),
    fork(watchDetailBanner),
    fork(watchRefContentD),
    /*****START PAYMENT HISTORY SAGAS*****/
    fork(watchGetPaymentHistory),
    /*****START PAYMENT HISTORY SAGAS*****/
    fork(watchEmailBafCare),
    fork(watchGetListNotif),
    fork(watchDeleteNotif),
    fork(watchReadNotif),

    //api fork get cabang status
    fork(watchgetCabangStatus),
    fork(watchSubmitPrivy),
    fork(watchOtpValidatePrivy),
    fork(watchRegenerateOtpPrivy),
    fork(watchRequestOtpPrivy),
    fork(watchCheckStatusRegister),
    fork(watchUploadDoc),
    fork(watchCheckStatusUpload),

    fork(watchOrderCount),
    fork(watchOrderFilter),
    fork(watchCancelPO),
    fork(watchQdeDetailData),
    fork(watchQdePersonalDetailData),
    fork(watchMpMappingRisk),
    fork(watchMpInstCalculation),
    fork(watchConfirmPO),

    fork(watchDdeInsert),
    fork(watchDdeDetail),
    fork(watchDdeFilter),
    fork(watchDdeRevise),

    fork(watchDdeEducation),
    fork(watchDdeGender),
    fork(watchDdeMaritalStatus),
    fork(watchDdeRelationship),
    fork(watchDdeReligion),
    fork(watchDdeHouseOwnership),
    fork(watchDdeOfficeOwnership),

    fork(watchDdeLegalProvince),
    fork(watchDdeLegalCity),
    fork(watchDdeLegalKecamatan),
    fork(watchDdeLegalKelurahan),
    fork(watchDdeResidenceProvince),
    fork(watchDdeResidenceCity),
    fork(watchDdeResidenceKecamatan),
    fork(watchDdeResidenceKelurahan),
    fork(watchDdeJobProvince),
    fork(watchDdeJobCity),
    fork(watchDdeJobKecamatan),
    fork(watchDdeJobKelurahan),

    fork(watchDdeJobCategory),
    fork(watchDdeJobPosition),
    fork(watchDdeJobStatus),
    fork(watchDdeJobType),
    fork(watchDdeIndustyType),
    fork(watchDdeLamaUsahaTahun),
    fork(watchDdeLamaUsahaBulan),
    fork(watchDdeKontrakTahun),
    fork(watchDdeKontrakBulan),

    fork(watchDdeJaminanJenis),
    fork(watchDdeJenis),
    fork(watchDdeMerk),
    fork(watchDdeSchemeId),
    fork(watchDdeFinancialData),
    fork(watchDdeFinancingPurpose),
    fork(watchDdeSourceFunds),
    fork(watchGetAuthKey),
    fork(watchGetOTR),
    fork(watchGetPengajuanData),
    fork(watchGetPengajuanSyana),
    fork(watchGetPengajuanNMC),
    fork(watchGetPengajuanMP),
    fork(watchGetPengajuanCar),

    fork(watchGetListBPKB),
    fork(watchSubmitBPKB),
    fork(watchTimeSlotBPKB),
    fork(watchCheckAvailBook),
    fork(watchCheckHoliday),
    fork(watchGetListBPKBInProgress),
    fork(watchGetListBPKBDone),
    fork(watchCancelBPKB),
    fork(watchRescheduleBpkb),
    fork(watchGetdetailBpkb),
    fork(watchRevisiKonfirmasiTransaksi),

    fork(watchValidationInvoice),
    fork(watchTanggalPO),
    fork(watchKonfirmasiTransaksi),
    fork(watchTipeKonsumen),
    fork(watchDeliveryAddress),
    fork(watchInfoPengiriman),
    fork(watchRevisiKonfirmasiTransaksi),
    fork(watchGenerateToken),

    fork(watchGetBafPointValueSagas)
  ]);
}
