import { takeLatest, put } from 'redux-saga/effects'
import { GET_BAFPOINT_VALUE_PROCESS, GET_BAFPOINT_PROCESS_SUCCESS, GET_BAFPOINT_PROCESS_FAILED } from "../../actions/bafPoint";
import { filterFetch } from "../../utils/apiFetch";
import {API_KEY, API_TIMEOUT} from "../../utils/constant";

function* getBafPointValueSagas(action) {
    try {
        const result = yield filterFetch('', {
            method: '',
            header: {
                'Content-Type': 'application/json',
                'x-api-key': API_KEY
            },
            timeout: API_TIMEOUT,
            body: JSON.stringify(action.data),
        });
        yield put({
            type: GET_BAFPOINT_PROCESS_SUCCESS,
            payload: result
        })
    } catch (error) {
        yield put({
            type: GET_BAFPOINT_PROCESS_FAILED,
            error: error
        })
    }
}

export function* watchGetBafPointValueSagas() {
    yield takeLatest(GET_BAFPOINT_VALUE_PROCESS, getBafPointValueSagas);
}
